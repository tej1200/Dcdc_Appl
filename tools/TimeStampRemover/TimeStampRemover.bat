@echo off

if "%1" == "body" goto :body
set scriptpath=%~dp0
set projectpath=%CD%

set gendatapath=%projectpath%\Appl\GenData
set sourcepath=%projectpath%\Appl\Source
set includepath=%projectpath%\Appl\Include

set sedpath=%scriptpath%..\..\make\MakeSupport\cygwin_root\cmd
call %0 body %gendatapath%
call %0 body %sourcepath% 
call %0 body %includepath%

set scriptpath=
set projectpath=
set gendatapath=
set sourcepath=
set includepath=
set sedpath=

exit /b

:body
@echo off

if not exist "%2\" goto :errorinpath
cd %2

setlocal enabledelayedexpansion
set c_file_ext=.c
set h_file_ext=.h
set mak_file_ext=.mak

for /f tokens^=* %%i in ('where /r . *')do (
set c_or_h_file=false
set mak_file=false
if %c_file_ext%==%%~xi set c_or_h_file=true
if %h_file_ext%==%%~xi set c_or_h_file=true
if %mak_file_ext%==%%~xi set mak_file=true

if !c_or_h_file! equ true (
set fileName=%%~fxi
%sedpath%\sed -i "/Generation Time/d" !fileName!
%sedpath%\sed -i "/DATE, TIME/d" !fileName!
)

if !mak_file! equ true (
set fileName=%%~fxi
%sedpath%\sed -i "/Generation Time:/d" !fileName!
)
)

set c_file_ext=
set h_file_ext=
set mak_file_ext=
set c_or_h_file=
set mak_file=

endlocal

del sed* /s /f /q 2> nul

cd %projectpath%
exit /b

:errorinpath
echo PathNotFound %2
cd %projectpath%
exit /b