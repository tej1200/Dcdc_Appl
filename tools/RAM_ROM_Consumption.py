import xlsxwriter as xl

print('Started parse map file to get module wise memory consumption')
print('\n')

workbook = xl.Workbook('../out/RAM_ROM_Consumption.xlsx')

#Extract memory consumption of modules
worksheet = workbook.add_worksheet('Modules')
cell_format = workbook.add_format()
cell_format.set_bold()

row = 0
module_heading = ['Module Name','Text','Rodata','Data','Bss']

#set modules sheet heading
for column in range(0,5):
  worksheet.write(row,column,module_heading[column],cell_format)
  
main_dict = {}


cpyFlag = 0;
start = "| [in] File                         | [in] Section                   | [in] Size (MAU) | [out] Offset | [out] Section                                           | [out] Size (MAU) |"
end = "+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+"

try:
  file = open("../out/Appl_DCDC_Standalone.map",'r')
except FileNotFoundError:
  print('map file is NOT present')
  exit(1) 
  
while True:
  line = file.readline()

#searching for start to section 
  if line.strip('\n') == start:
    cpyFlag = 1  

#searching for end to section
  if cpyFlag == 1 and line.strip('\n') == end:
    cpyFlag = 0
    break
  
  if cpyFlag == 1:
    #searching for '.o' in string '| User_StartUp.o                    |'
    if (line[:37].find('.o')) != -1:
      #split string using char '|'  '| User_StartUp.o                    | .rodata.BMHD0_UCB (22803)      | 0x00000200      |'
      readline_list = line[2:87].split('|')
      
      module_name = "".join(str(element) for element in readline_list[0]).strip()
             
      my_string = "".join(str(element) for element in readline_list[1]).strip()
      section_size = "".join(str(element) for element in readline_list[2]).strip()

      text='0'
      rodata='0' 
      data='0'
      bss='0'

      if my_string.startswith('.text') == True:
       text = section_size
      elif my_string.startswith('.rodata') == True:
       rodata = section_size
      elif my_string.startswith('.data') == True:
       data = section_size
      elif my_string.startswith('.bss') == True:
       bss = section_size

      if module_name in main_dict:
        if text != '0':
          temp = int(main_dict[module_name][0],16) + int(text,16)
          main_dict[module_name][0] = hex(temp)
        else:
          pass        
        if rodata != '0':
          temp = int(main_dict[module_name][1],16) + int(rodata,16)
          main_dict[module_name][1] = hex(temp)
        else:
          pass
        if data != '0':
          temp = int(main_dict[module_name][2],16) + int(data,16)
          main_dict[module_name][2] = hex(temp)
        else:
          pass          
        if bss != '0':
          temp = int(main_dict[module_name][3],16) + int(bss,16)
          main_dict[module_name][3] = hex(temp)    
        else:
          pass  
      else:
        main_dict[module_name] = [text,rodata,data,bss]
        
#write extracted data from dictonary to excel sheet      
for key in main_dict:  
  row = row + 1
  worksheet.write(row,0,key)
  for column in range(1,5):
    worksheet.write(row,column,main_dict[key][column-1])

print('end of parsing of modules memory consumption')
print('\n')


#Extract Total consumption of memories

print('Started parse map file to get total memory consumption')
print('\n')

start = "| Memory                               | Code     | Data     | Reserved | Free     | Total    |"
end = "+---------------------------------------------------------------------------------------------+"

#create sheet for total memory consumption
worksheet = workbook.add_worksheet('Total')
cell_format = workbook.add_format()
cell_format.set_bold()

#reset map file read poniter
file.seek(0)
cpyFlag = 0;
row = 0

#set heading for Total memory consumption sheet
total_heading = ['Memory','Code','Data','Reserved','Free','Total']
for column in range(0,6):
  worksheet.write(row,column,total_heading[column],cell_format)
 
#clear dictionary 
main_dict.clear()

#started for parse map file line by line
while True:
  line = file.readline()

#searching for start to section 
  if line.strip('\n') == start:
    cpyFlag = 1  

#searching for end to section 
  if cpyFlag == 1 and line.strip('\n') == end:
    cpyFlag = 0
    break

  if cpyFlag == 1:
  #looking for mpe of Total in string '| mpe:RegionBlock_0_BMHD0              |'
    if (line[:41].find('mpe:')) != -1  or (line[:41].find('Total')) != -1:
      #splited string with charater '|'  '| mpe:RegionBlock_0_BMHD0              | 0x000200 |      0x0 |      0x0 |      0x0 | 0x000200 |'
      readline_list = line[2:95].split('|')
     
      memory_name = "".join(str(element) for element in readline_list[0]).strip()
             
      Code_section = "".join(str(element) for element in readline_list[1]).strip()
      memory_size = "".join(str(element) for element in readline_list[2]).strip()
      Data_section = "".join(str(element) for element in readline_list[3]).strip()
      section_Reserved = "".join(str(element) for element in readline_list[4]).strip()
      Free_section = "".join(str(element) for element in readline_list[5]).strip()
      Total_size = "".join(str(element) for element in readline_list[6]).strip()
      
      main_dict[memory_name] = [Code_section,memory_size,Data_section,section_Reserved,Free_section,Total_size]

#write extracted data from dictonary to excel sheet
for key in main_dict: 
  row = row + 1 
  worksheet.write(row,0,key)
  for column in range(1,6):
    worksheet.write(row,column,main_dict[key][column-1])
  
print('end of parsing of total memory consumption')   
#close excel sheet    
workbook.close()    

#close map file
file.close()

'''
b='0x22'
c='0x12'

a = int(b,16) + int(c,16)
print(a)
print(hex(16))

'''

