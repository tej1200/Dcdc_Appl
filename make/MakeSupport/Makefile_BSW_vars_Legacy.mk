########################################################################################################################
# File Name  : Makefile_BSW_vars_Legacy.mk
# Description: This Sub-Makefile contains variables for setting directory names in a SIP.
#
#              This file MUST NOT BE CHANGED on project integration
#
# Project    : Vector PES Build Environment
# Module     : GlobalMake 4.1
#
#-----------------------------------------------------------------------------------------------------------------------
# COPYRIGHT
#-----------------------------------------------------------------------------------------------------------------------
# Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved. 
#
#-----------------------------------------------------------------------------------------------------------------------
# AUTHOR IDENTITY
#-----------------------------------------------------------------------------------------------------------------------
# Name                          Initials      Company
# ----------------------------  ------------  --------------------------------------------------------------------------
# Clemens von Mann              vircvo        Vector Informatik GmbH
#-----------------------------------------------------------------------------------------------------------------------
# REVISION HISTORY
#-----------------------------------------------------------------------------------------------------------------------
# Refer to the file Global.Makefile.target.4.mk.
########################################################################################################################

########################################################################################################################
#    EXAMPLE CODE ONLY
#    -------------------------------------------------------------------------------------------------------------------
#    This Example Code is only intended for illustrating an example of a possible BSW integration and BSW configuration.
#    The Example Code has not passed any quality control measures and may be incomplete. The Example Code is neither
#    intended nor qualified for use in series production. The Example Code as well as any of its modifications and/or
#    implementations must be tested with diligent care and must comply with all quality requirements which are necessary
#    according to the state of the art before their use.
########################################################################################################################


## @file
#
#  Makefile_BSW_vars_Legacy.mk
#  ===========================
#
#  This Sub-Makefile contains variables for setting directory names in a SIP.
#  It is included by Global.Makefile.target.\$(VERSION).mk if modules are located in folder 'BSW' (legacy SIP structure).
#
#  @ingroup mod_Makefile
#


#
#  Set variables for legacy SIP structure
#
BSW_MAKE_DIR?=mak
GLOBAL_COMP_DIR?=BSW
BSW_DIR:=$(ROOT_U)/$(GLOBAL_COMP_DIR)
BSW_SRC_DIR:=
MSSV_DIR:=$(ROOT_U)/Generators/MSSV

#  function file2mod(filepath)
#
#  @return module of file
#
file2mod=$(notdir $(patsubst %/,%,$(dir $(1))))
