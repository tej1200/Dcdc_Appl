########################################################################################################################
# File Name  : Makefile_resource.mk
# Description: Sub-Makefile for generation of the resource information.
#
#              This file MUST NOT BE CHANGED on project integration
#
# Project    : Vector PES Build Environment
# Module     : GlobalMake 4.1
#
#-----------------------------------------------------------------------------------------------------------------------
# COPYRIGHT                                                                                                           
#-----------------------------------------------------------------------------------------------------------------------
# Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved. 
#                                                                                                                     
#-----------------------------------------------------------------------------------------------------------------------
# AUTHOR IDENTITY                                                                                                     
#-----------------------------------------------------------------------------------------------------------------------
# Name                          Initials      Company                                                                 
# ----------------------------  ------------  --------------------------------------------------------------------------
# Clemens von Mann              vircvo        Vector Informatik GmbH
# Claudia Buhl                  vircbl        Vector Informatik GmbH
#-----------------------------------------------------------------------------------------------------------------------
# REVISION HISTORY                                                                                                    
#-----------------------------------------------------------------------------------------------------------------------
# Refer to the file Global.Makefile.target.4.mk.
########################################################################################################################

########################################################################################################################
#    EXAMPLE CODE ONLY
#    -------------------------------------------------------------------------------------------------------------------
#    This Example Code is only intended for illustrating an example of a possible BSW integration and BSW configuration.
#    The Example Code has not passed any quality control measures and may be incomplete. The Example Code is neither
#    intended nor qualified for use in series production. The Example Code as well as any of its modifications and/or
#    implementations must be tested with diligent care and must comply with all quality requirements which are necessary
#    according to the state of the art before their use.
########################################################################################################################


## @file
#
#  Makefile_resource.mk
#  ====================
#
#  This file generates resource information
#
#  Delivery information is created by a generated script <b>makesupport.xml</b> and
#  an XSL script called <b>script.xslt</b> from MakeSupport.
#
#  Note: This solution is copied from MakeSupport branch 3.13.xx
#
#
#  Targets:
#  --------
#  - res, resource
#
#  @ingroup mod_Makefile
#


ifneq ($(shell if [ -f $(VERSION_INFO_FILE) ]; then $(ECHO) $(VERSION_INFO_FILE); fi),)

# VINFO - properties
# some properties are taken from Makefile_delivery.mk
get_info_from_versioninfo_file=$(shell $(XML) sel -t -v $(1) $(VERSION_INFO_FILE) 2>/dev/null)
FROM_VINFO_CBD=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/CBD)
FROM_VINFO_OEM=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/OEM)
FROM_VINFO_CONTROLLER=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/Controller)
FROM_VINFO_COMPILER=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/Compiler)
FROM_VINFO_SLP=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/SLP)
FROM_VINFO_CANCELL=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/CanCell)
FROM_VINFO_SIP=$(call get_info_from_versioninfo_file,/CANbeddedVersionInfo/Properties/SIP)

# Specifies how the file names for the release sheet and resource info files are named
RESOURCE_FILENAME_TMP     = $(FROM_VINFO_SLP).$(FROM_VINFO_OEM).$(FROM_VINFO_COMPILER).$(FROM_VINFO_CONTROLLER).$(FROM_VINFO_SIP).$(FROM_VINFO_CBD)
RESOURCE_FILENAME = $(subst $(space),_,$(RESOURCE_FILENAME_TMP))
RESOURCE_TARGETDIR_TMP    = $(subst $(_OPEN_PAREN),,$(subst $(_CLOSE_PAREN),,$(RESOURCE_FILENAME_TMP)))
RESOURCE_TARGETDIR = $(subst $(space),_,$(RESOURCE_TARGETDIR_TMP))
RESOURCE_COPYFILES    = ../CANdb/*.gny ../CANdb/*.arxml ../Config/*.gny ../Config/*.arxml
endif

CLEAN_RESOURCE_FILENAME = $(subst $(_OPEN_PAREN),,$(subst $(_CLOSE_PAREN),,$(RESOURCE_FILENAME)))


##############################################################################
# Resource information generation
##############################################################################
#-----------------------------------------------------------------------------
# List of files, which contain the resource information
#-----------------------------------------------------------------------------
LST_LIST = \
  $(if $(findstring $(RESOURCE_FILE),LIST), $(LST_PATH)/*.$(RESOURCE_FILE_EXT), \
    $(if $(findstring $(RESOURCE_FILE),MAP),                                    \
      $(PROJECT_NAME).$(RESOURCE_FILE_EXT)                                      \
    ,                                                                           \
      RESOURCE_NOT_SET                                                          \
    )                                                                           \
  )

#-----------------------------------------------------------------------------
# General storage location for resource information
#-----------------------------------------------------------------------------
RESOURCE_REPOSITORY  = \\\\vistrfs1\\vgroup\\PES\\30_ProductManagement\\_EmbeddedSwRessourceNeeds

#-----------------------------------------------------------------------------
# Resource information generation rules
#-----------------------------------------------------------------------------
RESOURCE_NOT_SET:
	@$(ECHO) "Resource is not set correctly or resource target not supported for selected platform.";

fetch-memory-info-meta.txt:
	@$(ECHO) "COMPILER_MANUFACTURER:"   '$(subst \,\\, $(COMPILER_MANUFACTURER))'  > $@; \
  $(ECHO) "PROJECT_DIR:"             '$(subst \,\\, $(subst \\,\,$(CURDIR)))'   >> $@; \
  $(ECHO) "PLATFORM:"                '$(subst \,\\, $(PLATFORM))'               >> $@; \
  $(ECHO) "EMULATOR:"                '$(subst \,\\, $(EMULATOR))'               >> $@; \
  $(ECHO) "EVA_BOARD:"               '$(subst \,\\, $(EVA_BOARD))'              >> $@; \
  $(ECHO) "DERIVATIVE:"              '$(subst \,\\, $(DERIVATIVE))'             >> $@; \
  $(ECHO) "PROJECT_NAME:"            '$(subst \,\\, $(PROJECT_NAME))'           >> $@; \
  $(ECHO) "CC_VERSION_STRING:"       '$(subst \,\\, $(CC_VERSION_STRING))'      >> $@; \
  $(ECHO) "CFLAGS_ADDITIONAL_POST:"  '$(subst \,\\, $(CFLAGS))'                 >> $@; \
  $(ECHO) "CVERSION_STRING:"         '$(subst \,\\, $(CVERSION_STRING))'        >> $@; \
  $(ECHO) "LDFLAGS_ADDITIONAL_POST:" '$(subst \,\\, $(LDFLAGS))'                >> $@; \
  $(ECHO) "LDVERSION_STRING:"        '$(subst \,\\, $(LDVERSION_STRING))'       >> $@; \
  $(ECHO) "ASFLAGS_ADDITIONAL_POST:" '$(subst \,\\, $(ASFLAGS))'                >> $@; \
  $(ECHO) "ASVERSION_STRING:"        '$(subst \,\\, $(ASVERSION_STRING))'       >> $@; \
  $(ECHO) "EXT_T1_VERSION_STRING:"   '$(subst \,\\, $(EXT_T1_VERSION_STRING))'  >> $@; \
  $(ECHO) "EXT_T1_FLAGS:"            '$(subst \,\\, $(EXT_T1_FLAGS))'           >> $@; \
  $(ECHO) "EXT_T2_VERSION_STRING:"   '$(subst \,\\, $(EXT_T2_VERSION_STRING))'  >> $@; \
  $(ECHO) "EXT_T2_FLAGS:"            '$(subst \,\\, $(EXT_T2_FLAGS))'           >> $@; \
  if [ -n "$(FROM_VINFO_CBD)" ] ; then                                                 \
  $(ECHO) "FROM_VINFO_CBD:"          '$(subst \,\\, $(FROM_VINFO_CBD))'         >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_SIP)" ] ; then                                                 \
  $(ECHO) "FROM_VINFO_SIP:"          '$(subst \,\\, $(FROM_VINFO_SIP))'         >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_OEM)" ] ; then                                                 \
  $(ECHO) "FROM_VINFO_OEM:"          '$(subst \,\\, $(FROM_VINFO_OEM))'         >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_CONTROLLER)" ] ; then                                          \
  $(ECHO) "FROM_VINFO_CONTROLLER:"   '$(subst \,\\, $(FROM_VINFO_CONTROLLER))'  >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_COMPILER)" ] ; then                                            \
  $(ECHO) "FROM_VINFO_COMPILER:"     '$(subst \,\\, $(FROM_VINFO_COMPILER))'    >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_SLP)" ] ; then                                                 \
  $(ECHO) "FROM_VINFO_SLP:"          '$(subst \,\\, $(FROM_VINFO_SLP))'         >> $@; \
  fi;                                                                                  \
  if [ -n "$(FROM_VINFO_CANCELL)" ] ; then                                             \
  $(ECHO) "FROM_VINFO_CANCELL:"      '$(subst \,\\, $(FROM_VINFO_CANCELL))'     >> $@; \
  fi;

fetch-memory-info.awk:
	@$(ECHO) '$(RESOURCE_FETCH_SCRIPT)'                                                                                > $@; \
 $(ECHO) "BEGIN {"                                                                                                  >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Print the header of the outputted file"                                                                 >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "_additional_module_lst = \"$(subst \,\\\\,$(ADDITIONAL_OBJECTS)) $(subst \,\\\\,$(APP_SOURCE_LST))\";"    >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print \"<?xml version='1.0'?>\";"                                                                     >> $@; \
 $(ECHO) "    print \"<memory-usage>\"; # root tag"                                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  while (getline < \"fetch-memory-info-meta.txt\")"                                                       >> $@; \
 $(ECHO) "  {"                                                                                                      >> $@; \
 $(ECHO) "    _colon_index = index(\$$0, \":\");"                                                                   >> $@; \
 $(ECHO) "    if (_colon_index <= 0) {"                                                                             >> $@; \
 $(ECHO) "      continue;"                                                                                          >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    _key = substr(\$$0, 0, _colon_index - 1);"                                                            >> $@; \
 $(ECHO) "    _val = substr(\$$0, _colon_index + 1);"                                                               >> $@; \
 $(ECHO) "    _val = trim(_val);"                                                                                   >> $@; \
 $(ECHO) "    if (_val ~ /^[-=]/) {"                                                                                >> $@; \
 $(ECHO) "      _val = \"'\" _val;"                                                                                 >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    if (XML) { # translate some entities"                                                                 >> $@; \
 $(ECHO) "      gsub(\">\", \"\\\\&gt;\", _val);"                                                                   >> $@; \
 $(ECHO) "      gsub(\"<\", \"\\\\&lt;\", _val);"                                                                   >> $@; \
 $(ECHO) "      gsub(\"&\", \"\\\\&amp;\", _val);"                                                                  >> $@; \
 $(ECHO) "      gsub(\"\\\"\", \"\\\\&quot;\", _val);"                                                              >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    if (XML) {"                                                                                           >> $@; \
 $(ECHO) "      printf \"  <meta name=\\\"%s\\\">%s</meta>\\n\", _key, _val;"                                       >> $@; \
 $(ECHO) "    } else {"                                                                                             >> $@; \
 $(ECHO) "      printf \"%s\\t%s\\n\", _key, _val;"                                                                 >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    if (_key == \"PROJECT_DIR\") {"                                                                       >> $@; \
 $(ECHO) "      match(_val, /^[C-Z]:\\\\usr\\\\distrib\\\\[^\\\\]+\\\\[^\\\\]+\\\\([^\\\\]+)\\\\([^\\\\]+)\\\\/,"   >> $@; \
 $(ECHO) "            _OEM_SLP);"                                                                                   >> $@; \
 $(ECHO) "      if (XML) {"                                                                                         >> $@; \
 $(ECHO) "        printf \"  <meta name=\\\"OEM\\\">%s</meta>\\n\", _OEM_SLP[1];"                                   >> $@; \
 $(ECHO) "        printf \"  <meta name=\\\"SLP\\\">%s</meta>\\n\", _OEM_SLP[2];"                                   >> $@; \
 $(ECHO) "      } else {"                                                                                           >> $@; \
 $(ECHO) "        printf \"OEM\\t%s\\n\", _OEM_SLP[1];"                                                             >> $@; \
 $(ECHO) "        printf \"SLP\\t%s\\n\", _OEM_SLP[2];"                                                             >> $@; \
 $(ECHO) "      }"                                                                                                  >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  printf \"\\n\";"                                                                                        >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This section is called at the end of the whole script."                                                 >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "END {"                                                                                                    >> $@; \
 $(ECHO) "    if (DEBUG) {"                                                                                         >> $@; \
 $(ECHO) "        printf \"\\n\\n\"; # two empty lines"                                                             >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  if (!XML) {"                                                                                            >> $@; \
 $(ECHO) "    print \"Module\\tcode\\tconst\\tRAM\\tInitRAM\";"                                                     >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  printEntries();"                                                                                        >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print \"</memory-usage>\"; # root tag"                                                                >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This API function is the most important one:"                                                           >> $@; \
 $(ECHO) "# It adds an entry to the internal list, which is printed at the end."                                    >> $@; \
 $(ECHO) "# @param string moduleName: Contains the name of the module you want to add"                              >> $@; \
 $(ECHO) "#           byteSize."                                                                                    >> $@; \
 $(ECHO) "# @param string memoryType: Contains the memory type you want to add byteSize."                           >> $@; \
 $(ECHO) "#           The memory type is mapped by the memoryTable to the according"                                >> $@; \
 $(ECHO) "#           memory section (ram, rom, const)."                                                            >> $@; \
 $(ECHO) "# @param string byteSize: Contains the size of memory in bytes. The format must"                          >> $@; \
 $(ECHO) "#           be either hexadecimal or decimal. Number with a leading '0x' or a"                            >> $@; \
 $(ECHO) "#           trailing 'h' are autodetected as a hexadecimal value."                                        >> $@; \
 $(ECHO) "# @param bool isHex: OPTIONAL: If set to 1 the param byteSize will be treated as"                         >> $@; \
 $(ECHO) "#           a hexadecimal string. If 0 (standard) the function tries to auto-"                            >> $@; \
 $(ECHO) "#           detect if byteSize is a hex or a decimal value."                                              >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function addEntry(moduleName, memoryType, byteSize, isHex) {"                                             >> $@; \
 $(ECHO) "  moduleName = trim(moduleName);"                                                                         >> $@; \
 $(ECHO) "  memoryType = trim(memoryType);"                                                                         >> $@; \
 $(ECHO) "  byteSize   = trim(byteSize);"                                                                           >> $@; \
 $(ECHO) "  # If byteSize has a trailing 'h' or a leading '0x' hexformat is autodetected"                           >> $@; \
 $(ECHO) "  if (isHex || (byteSize ~ /^0x/) || (byteSize ~ /h\$$/)) {"                                              >> $@; \
 $(ECHO) "    byteSize = hex2dec(byteSize);"                                                                        >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  "                                                                                                       >> $@; \
 $(ECHO) "  mappedMemoryType  = parseMemoryType(memoryType);"                                                       >> $@; \
 $(ECHO) "  moduleName        = pathToModule(moduleName);"                                                          >> $@; \
 $(ECHO) "  "                                                                                                       >> $@; \
 $(ECHO) "  if (DEBUG) {"                                                                                           >> $@; \
 $(ECHO) "    printf \"%5d Adding: %12s %14s -> %6s 0x%04X (%d)\\n\", FNR, moduleName, \\"                          >> $@; \
 $(ECHO) "                      memoryType, mappedMemoryType, byteSize, byteSize;"                                  >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if (mappedMemoryType == \"ignore\") {"                                                                  >> $@; \
 $(ECHO) "    return;"                                                                                              >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  _private_array_modules[moduleName, mappedMemoryType] += byteSize;"                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# The function decides, depending on the current file and the processing state"                           >> $@; \
 $(ECHO) "# (set by startProcessing() and stopProcessing()), if processing has to"                                  >> $@; \
 $(ECHO) "# continue or jump to next record."                                                                       >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function process() {"                                                                                     >> $@; \
 $(ECHO) "  if (FNR == 1 && DEBUG) {"                                                                               >> $@; \
 $(ECHO) "    print  \"\"; # print empty line"                                                                      >> $@; \
 $(ECHO) "    printf \"!Begin with File \\\"%s\\\"\\n\", FILENAME;"                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if (_private_processingState != FILENAME) { # inactive"                                                 >> $@; \
 $(ECHO) "    next;"                                                                                                >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This function starts the processing for the current file. After calling"                                >> $@; \
 $(ECHO) "# the processing will begin and no further records are skipped."                                          >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function startProcessing() {"                                                                             >> $@; \
 $(ECHO) "  if ((_private_processingState != FILENAME) && DEBUG) {"                                                 >> $@; \
 $(ECHO) "    printf \"!Start processing file \\\"%s\\\" at line %d\\n\", FILENAME, FNR;"                           >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  _private_processingState = FILENAME;"                                                                   >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This function is the complement to startProcessing() and stops the processing."                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function stopProcessing() {"                                                                              >> $@; \
 $(ECHO) "  if (_private_processingState == FILENAME) { # We started processing"                                    >> $@; \
 $(ECHO) "    if (DEBUG) {"                                                                                         >> $@; \
 $(ECHO) "      printf \"!Stopping processing file \\\"%s\\\" at line %d\\n\", FILENAME, FNR;"                      >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    _private_processingState = 0;"                                                                        >> $@; \
 $(ECHO) "    nextfile; # We're done for this file. Go to the next one"                                             >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This function converts a hexadecimal number to a decimal one."                                          >> $@; \
 $(ECHO) "# @param string hexStr: The string which has to be converted. Dont need to have"                          >> $@; \
 $(ECHO) "#             leading '0x'. Can have a trailing 'h'."                                                     >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function hex2dec(hexStr) {"                                                                               >> $@; \
 $(ECHO) "  if (hexStr !~ /^0x/) {"                                                                                 >> $@; \
 $(ECHO) "    hexStr = \"0x\" hexStr;"                                                                              >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if (hexStr ~ /h\$$/) {"                                                                                 >> $@; \
 $(ECHO) "    gsub(/h\$$/, \"\", hexStr); # Strip trailing 'h'"                                                     >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  return strtonum(hexStr);"                                                                               >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This function prints the internal array which contains the memory information"                          >> $@; \
 $(ECHO) "# The output format is a tab seperated list with the order:"                                              >> $@; \
 $(ECHO) "# moduleName, sizeCode, sizeConst, sizeRam"                                                               >> $@; \
 $(ECHO) "# @param NONE!"                                                                                           >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function printEntries(j, i, n, ind, keyArr, key, moduleName, printedModules,"                             >> $@; \
 $(ECHO) "                      ram, code, const, iram, totaliram)"                                                 >> $@; \
 $(ECHO) "{"                                                                                                        >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print \"  <modules>\";"                                                                               >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  j = 1;"                                                                                                 >> $@; \
 $(ECHO) "  for (i in _private_array_modules) {"                                                                    >> $@; \
 $(ECHO) "    ind[j] = i; # index value becomes element value"                                                      >> $@; \
 $(ECHO) "    j++;"                                                                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if( j == 1 ) {"                                                                                         >> $@; \
 $(ECHO) "    error(\"Your AWK Script never called addEntry(). Please check Makefile Script.\");"                   >> $@; \
 $(ECHO) "    n = 0;"                                                                                               >> $@; \
 $(ECHO) "  } else {"                                                                                               >> $@; \
 $(ECHO) "    n = asort(ind); # index values are now sorted"                                                        >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  gsub(/\\\\ /, \"~~~\", _additional_module_lst);"                                                        >> $@; \
 $(ECHO) "  _number_additional_modulese = split(_additional_module_lst, _tmp, \"[ ]+\");"                           >> $@; \
 $(ECHO) "  for( i = 1; i <= _number_additional_modulese; i++ ) {"                                                  >> $@; \
 $(ECHO) "    gsub(/~~~/, \" \", _tmp[i]);"                                                                         >> $@; \
 $(ECHO) "    _additional_module_array[pathToModule(_tmp[i])] = 1;"                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  for (i = 1; i <= n; i++) {"                                                                             >> $@; \
 $(ECHO) "    key = ind[i];"                                                                                        >> $@; \
 $(ECHO) "    split(key, keyArr, SUBSEP);"                                                                          >> $@; \
 $(ECHO) "    moduleName = keyArr[1];"                                                                              >> $@; \
 $(ECHO) "    if( moduleName in _additional_module_array ) {"                                                       >> $@; \
 $(ECHO) "      _private_array_modules[moduleName, \"__ISADD__\"] = 1;"                                             >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  print \"\";"                                                                                            >> $@; \
 $(ECHO) "  printModuleSummary(ind, n, 0);"                                                                         >> $@; \
 $(ECHO) "  print \"\"; print \"\";"                                                                                >> $@; \
 $(ECHO) "  printModuleSummary(ind, n, 1);"                                                                         >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print \"  </modules>\";"                                                                              >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) "function printModuleSummary(ind, n, isAdditional) {"                                                      >> $@; \
 $(ECHO) "  if( isAdditional ) {"                                                                                   >> $@; \
 $(ECHO) "    if( XML ) {"                                                                                          >> $@; \
 $(ECHO) "      print \"<additional>\";"                                                                            >> $@; \
 $(ECHO) "    } else {"                                                                                             >> $@; \
 $(ECHO) "      print \"Additonal modules:\";"                                                                      >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  } else {"                                                                                               >> $@; \
 $(ECHO) "    if( XML ) {"                                                                                          >> $@; \
 $(ECHO) "      print \"<core>\";"                                                                                  >> $@; \
 $(ECHO) "    } else {"                                                                                             >> $@; \
 $(ECHO) "      print \"Core modules:\";"                                                                           >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  totaliram = 0;"                                                                                         >> $@; \
 $(ECHO) "  totalram = 0;"                                                                                          >> $@; \
 $(ECHO) "  totalrom = 0;"                                                                                          >> $@; \
 $(ECHO) "  totalcode = 0;"                                                                                         >> $@; \
 $(ECHO) "  for (i = 1; i <= n; i++) {"                                                                             >> $@; \
 $(ECHO) "    key = ind[i];"                                                                                        >> $@; \
 $(ECHO) "    split(key, keyArr, SUBSEP);"                                                                          >> $@; \
 $(ECHO) "    moduleName = keyArr[1];"                                                                              >> $@; \
 $(ECHO) "    if( isAdditional != _private_array_modules[moduleName, \"__ISADD__\"] ) {"                            >> $@; \
 $(ECHO) "      continue;"                                                                                          >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    if ( !(moduleName in printedModules) ) {"                                                             >> $@; \
 $(ECHO) "      ram = _private_array_modules[moduleName, \"ram\"]+0;"                                               >> $@; \
 $(ECHO) "      iram = _private_array_modules[moduleName, \"iram\"]+0;"                                             >> $@; \
 $(ECHO) "      code = _private_array_modules[moduleName, \"code\"]+0;"                                             >> $@; \
 $(ECHO) "      const = _private_array_modules[moduleName, \"const\"]+0;"                                           >> $@; \
 $(ECHO) "      totaliram += iram;"                                                                                 >> $@; \
 $(ECHO) "      totalcode += code;"                                                                                 >> $@; \
 $(ECHO) "      totalram  += ram;"                                                                                  >> $@; \
 $(ECHO) "      totalrom  += const;"                                                                                >> $@; \
 $(ECHO) "      printedModules[moduleName] = 1;"                                                                    >> $@; \
 $(ECHO) "      if (XML)"                                                                                           >> $@; \
 $(ECHO) "      {"                                                                                                  >> $@; \
 $(ECHO) "        print \"    <module name=\\\"\" moduleName \"\\\">\";"                                            >> $@; \
 $(ECHO) "        print \"      <code>\" code \"</code>\";"                                                         >> $@; \
 $(ECHO) "        print \"      <const>\" const \"</const>\";"                                                      >> $@; \
 $(ECHO) "        print \"      <ram>\" ram \"</ram>\";"                                                            >> $@; \
 $(ECHO) "        print \"      <iram>\" iram \"</iram>\";"                                                         >> $@; \
 $(ECHO) "        print \"    </module>\";"                                                                         >> $@; \
 $(ECHO) "      }"                                                                                                  >> $@; \
 $(ECHO) "      else"                                                                                               >> $@; \
 $(ECHO) "      {"                                                                                                  >> $@; \
 $(ECHO) "        print moduleName \"\\t\" code \"\\t\" const \"\\t\" ram \"\\t\" iram;"                            >> $@; \
 $(ECHO) "      }"                                                                                                  >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if( !XML ) {"                                                                                           >> $@; \
 $(ECHO) "    print \"Total:\" \"\\t\" totalcode \"\\t\" totalrom \"\\t\" totalram \"\\t\" totaliram;"              >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if( totaliram > 0 ) {"                                                                                  >> $@; \
 $(ECHO) "    error(\"Warning: \" totaliram \" bytes initalized RAM is used!\");"                                   >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if( isAdditional ) {"                                                                                   >> $@; \
 $(ECHO) "    if( XML ) {"                                                                                          >> $@; \
 $(ECHO) "      print \"</additional>\";"                                                                           >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  } else {"                                                                                               >> $@; \
 $(ECHO) "    if( XML ) {"                                                                                          >> $@; \
 $(ECHO) "      print \"</core>\";"                                                                                 >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Handles input strings like \"C:\\path\\to\\file.ext\" and filters out 'file'"                           >> $@; \
 $(ECHO) "# Strips out content within parentheses () and brackets [], but only if it"                               >> $@; \
 $(ECHO) "# wouldnt result in a blank string."                                                                      >> $@; \
 $(ECHO) "# Hint: Bracket: []        Parentheses: ()"                                                               >> $@; \
 $(ECHO) "# @param string pathname: Contains the path which is analysed"                                            >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function pathToModule(pathname) {"                                                                        >> $@; \
 $(ECHO) "  pathname = trim(pathname);"                                                                             >> $@; \
 $(ECHO) "  if (pathname ~ /^\\(+.*\\)+\$$/) { # only if pathname starts AND ends with ()"                          >> $@; \
 $(ECHO) "      sub(/^\\(+/, \"\", pathname); # Strip leading ("                                                    >> $@; \
 $(ECHO) "      sub(/\\)+\$$/, \"\", pathname); # Strip trailing )"                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  if (pathname ~ /^\\[+.*\\]+\$$/) { # only if pathname starts AND ends with []"                          >> $@; \
 $(ECHO) "      sub(/^\\[+/, \"\", pathname); # Strip leading ["                                                    >> $@; \
 $(ECHO) "      sub(/\\]+\$$/, \"\", pathname); # Strip trailing ]"                                                 >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  pathname = trim(pathname);"                                                                             >> $@; \
 $(ECHO) "  sub(/\\(.*\\)/,   \"\", pathname); # Strip contents within parentheses"                                 >> $@; \
 $(ECHO) "  sub(/\\[.*\\]/,   \"\", pathname); # Strip contents within brackets"                                    >> $@; \
 $(ECHO) "  sub(/.*[\\/\\\\]/, \"\", pathname); # Strip the path"                                                   >> $@; \
 $(ECHO) "  sub(/\\.[^.]+\$$/, \"\", pathname); # Strip file extension"                                             >> $@; \
 $(ECHO) "  return pathname;"                                                                                       >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# This function takes an string which is treated as a memory information and"                             >> $@; \
 $(ECHO) "# maps it to the according memory by the memoryTable array defined at the"                                >> $@; \
 $(ECHO) "# BEGIN{} section. If no regular expression from the memoryTable matches, an"                             >> $@; \
 $(ECHO) "# error is printed and the program is shutting down."                                                     >> $@; \
 $(ECHO) "# @see memoryTable[] at BEGIN{} section"                                                                  >> $@; \
 $(ECHO) "# @param string input: The string which should be mapped."                                                >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function parseMemoryType(input,     key, keyOrder, i, numberElements) {"                                  >> $@; \
 $(ECHO) "  numberElements = split(memoryTableOrder, keyOrder, \",\");"                                             >> $@; \
 $(ECHO) "  for (i = 1; i <= numberElements; i++) {"                                                                >> $@; \
 $(ECHO) "    key = keyOrder[i];"                                                                                   >> $@; \
 $(ECHO) "    if(memoryTable[key] == \"\") {"                                                                       >> $@; \
 $(ECHO) "      continue;"                                                                                          >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "    if (input ~ memoryTable[key]) {"                                                                      >> $@; \
 $(ECHO) "      return key;"                                                                                        >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "  if( _private_detected_wrong_input[input] != 1 ) { # We did not yet print"                               >> $@; \
 $(ECHO) "    _private_detected_wrong_input[input] = 1;"                                                            >> $@; \
 $(ECHO) "    if( _private_initial_print == 1 ) {"                                                                  >> $@; \
 $(ECHO) "      error(\"!Error: Other unknown input type: '\" input \"'\");"                                        >> $@; \
 $(ECHO) "    } else {"                                                                                             >> $@; \
 $(ECHO) "      error(\"--------------------------------------------------------------------\\n\" \\"               >> $@; \
 $(ECHO) "            \"!Error: Unable to handle input type '\" input \"'\\n\" \\"                                  >> $@; \
 $(ECHO) "            \"  Filename: '\" FILENAME \"' at line \" FNR \"\\n\" \\"                                     >> $@; \
 $(ECHO) "            \"!Modify your AWK - Script in your project Makefile. The script is\\n\" \\"                  >> $@; \
 $(ECHO) "            \"   located at the Makefile variable \\\"RESOURCE_FETCH_SCRIPT\\\".\\n\" \\"                 >> $@; \
 $(ECHO) "            \"!Check your array <<memoryTable>> in the BEGIN{} section.\\n\" \\"                          >> $@; \
 $(ECHO) "            \"  E.g. add '\" input \"' to memoryTable[\\\"ram\\\"] to map it to RAM.\\n\" \\"             >> $@; \
 $(ECHO) "            \"  I.e.: memoryTable[\\\"ram\\\"] = \\\"\" memoryTable[\"ram\"] \"|\" input \"\\\";\\n\" \\" >> $@; \
 $(ECHO) "            \"  To ignore '\" input \"', add it to memoryTable[\\\"ignore\\\"]:\\n\" \\"                  >> $@; \
 $(ECHO) "            \"  memoryTable[\\\"ignore\\\"] = \\\"\" memoryTable[\"ignore\"] \"|\" input \"\\\";\\n\" \\" >> $@; \
 $(ECHO) "            \"--------------------------------------------------------------------\\n\");"                >> $@; \
 $(ECHO) "    }"                                                                                                    >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  _private_initial_print = 1;"                                                                            >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Small helper function to filter out a string enclosed in parentheses."                                  >> $@; \
 $(ECHO) "# @param string input: The input string which should be analysed"                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function getStringWithinParentheses(input,     tmpArr, tmpVar) {"                                         >> $@; \
 $(ECHO) "    if (match(input, /(\\(.+\\))/, tmpArr)) {"                                                            >> $@; \
 $(ECHO) "    tmpVar = tmpArr[1];"                                                                                  >> $@; \
 $(ECHO) "        gsub(/[\\(\\)]/, \"\", tmpVar); # strip off ( and )"                                              >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  return (tmpVar ~ /\\w/) ? tmpVar : input;"                                                              >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Small helper function to filter out a string enclosed in brackets."                                     >> $@; \
 $(ECHO) "# @param string input: The input string which should be analysed"                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function getStringWithinBrackets(input,     tmpArr, tmpVar) {"                                            >> $@; \
 $(ECHO) "    if (match(input, /(\\[.+\\])/, tmpArr)) {"                                                            >> $@; \
 $(ECHO) "    tmpVar = tmpArr[1];"                                                                                  >> $@; \
 $(ECHO) "    gsub(/[\\[\\]]/, \"\", tmpVar); # strip off [ and ]"                                                  >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  return (tmpVar ~ /\\w/) ? tmpVar : input;"                                                              >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Small helper function to trim a string. Removes trailing and leading space/tab"                         >> $@; \
 $(ECHO) "# @param string input: The input string which has to be trimmed"                                          >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function trim(str) {"                                                                                     >> $@; \
 $(ECHO) "    gsub(/^[ \\t]+/, \"\", str); # Strip leading  space/tap (ltrim)"                                      >> $@; \
 $(ECHO) "    gsub(/[ \\t]+\$$/, \"\", str); # Strip trailing space/tab (rtrim)"                                    >> $@; \
 $(ECHO) "    return str;"                                                                                          >> $@; \
 $(ECHO) "}"                                                                                                        >> $@; \
 $(ECHO) ""                                                                                                         >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "# Small helper function to print an error."                                                               >> $@; \
 $(ECHO) "# @param string input: The input string which has to printed."                                            >> $@; \
 $(ECHO) "################################################################################"                         >> $@; \
 $(ECHO) "function error(str) {"                                                                                    >> $@; \
 $(ECHO) "  if (XML) { # translate some entities"                                                                   >> $@; \
 $(ECHO) "    gsub(\">\", \"\\\\&gt;\", str);"                                                                      >> $@; \
 $(ECHO) "    gsub(\"<\", \"\\\\&lt;\", str);"                                                                      >> $@; \
 $(ECHO) "    gsub(\"&\", \"\\\\&amp;\", str);"                                                                     >> $@; \
 $(ECHO) "    gsub(\"\\\"\", \"\\\\&quot;\", str);"                                                                 >> $@; \
 $(ECHO) "    print \"<error>\";"                                                                                   >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "  print str;"                                                                                             >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print \"</error>\";"                                                                                  >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "# Additional output to console to make processing errors more obvious."                                   >> $@; \
 $(ECHO) "  if (XML) {"                                                                                             >> $@; \
 $(ECHO) "    print str > \"/dev/stderr\";"                                                                         >> $@; \
 $(ECHO) "  }"                                                                                                      >> $@; \
 $(ECHO) "}"                                                                                                        >> $@;

resource res: $(LST_LIST) $(if $(findstring $(LST_LIST),RESOURCE_NOT_SET),,fetch-memory-info-meta.txt fetch-memory-info.awk)
	@if [ "$(strip $(LST_LIST))" != "RESOURCE_NOT_SET" ] ; then                                                     \
      $(MKDIR) $(LOG_PATH); \
      $(GAWK) --re-interval -v DEBUG=1 -f fetch-memory-info.awk $(LST_LIST) > $(LOG_PATH)/$(PROJECT_NAME)_resource.xls; \
      $(GAWK) --re-interval -v   XML=1 -f fetch-memory-info.awk $(LST_LIST) > $(LOG_PATH)/$(PROJECT_NAME)_resource.xml; \
      if [ -n "$(CLEAN_RESOURCE_FILENAME)" ] ; then                                                                     \
        $(ECHO) "Generated files: $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xml";                                 \
        $(ECHO) "                 $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xls";                                 \
        $(MV) $(LOG_PATH)/$(PROJECT_NAME)_resource.xls $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xls;             \
        $(MV) $(LOG_PATH)/$(PROJECT_NAME)_resource.xml $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xml;             \
        if [ -d $(RESOURCE_REPOSITORY) ] ; then                                                            \
          if [ -d $(RESOURCE_REPOSITORY)/$(RESOURCE_TARGETDIR) ] ; then :;                                  \
          else                                                                                                          \
            $(MKDIR) $(RESOURCE_REPOSITORY)/$(RESOURCE_TARGETDIR);                                           \
          fi;                                                                                                           \
          $(CP) -r $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xml $(RESOURCE_REPOSITORY)/$(RESOURCE_TARGETDIR) 1>/dev/null;   \
          $(CP) $(RESOURCE_COPYFILES) $(RESOURCE_REPOSITORY)/$(RESOURCE_TARGETDIR) 2>/dev/null;                         \
          $(ECHO) "                 $(RESOURCE_REPOSITORY)/$(RESOURCE_TARGETDIR)/$(CLEAN_RESOURCE_FILENAME).Resource.xml"; \
        else                                                                                                            \
          $(ECHO) "Error: Repository path not correct!";                                                                \
        fi;                                                                                                             \
      else                                                                                                              \
        $(ECHO) "Generated files: $(LOG_PATH)/$(PROJECT_NAME)_resource.xml";                                            \
        $(ECHO) "                 $(LOG_PATH)/$(PROJECT_NAME)_resource.xls";                                            \
      fi;                                                                                                               \
      if [ -n "$(GAWK_DEBUG)" ] ; then                                                                                  \
          $(ECHO) "Generating resource debug file: $(LOG_PATH)/$(PROJECT_NAME)_resource_debug.txt";                     \
          $(GAWK) --re-interval -v DEBUG=1 -f fetch-memory-info.awk $(LST_LIST)                                         \
              > $(LOG_PATH)/$(PROJECT_NAME)_resource_debug.txt;                                                         \
      fi;                                                                                                               \
 fi;                                                                                                                    \
 $(RM) fetch-memory-info-meta.txt;                                                                                      \
 $(RM) fetch-memory-info.awk;

res_clean:
	@$(ECHO) "CLEAN      resource"
	$(Q)$(RM) $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xls $(LOG_PATH)/$(PROJECT_NAME)_resource.xls
	$(Q)$(RM) $(LOG_PATH)/$(CLEAN_RESOURCE_FILENAME).Resource.xml $(LOG_PATH)/$(PROJECT_NAME)_resource.xml

clean:: res_clean

distclean:: res_clean


help::
	@echo " *"
	@echo " *   Resource Information Generation - Targets:"
	@echo " *   ------------------------------------------"
	@echo " *    res,resource                     Generate resource information"
	@echo " *    res_clean                        Delete resource information files"
	@echo " *"
	@echo " ***********************************************************************************"
