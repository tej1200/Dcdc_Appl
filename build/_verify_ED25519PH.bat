@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************
rem *             Usage: Generates ED25519 signature for hex file.
rem *   Input parameter: FileName (with relative/absolute path) with .HEX file type
rem *            Output: FileName.sig contains comma seperated 64 byte ED25519 signature 
rem *****************************************************************************************

if "%1" == "" goto usage
if "%2" == "" goto setup

echo.
echo Verify signature (ED25519PH)
echo ED25519PH signature (Security model CCC) : 
echo %~d1%~p1%~n1.sig
echo.

echo Verifying ED25519PH signature...
rem %2 %1 /S -e:%4\error.txt /sv8:%3!%4\%~n1.sig
%2 %4\%~n1.hex /S -e:%4\error.txt /sv8:%3!%4\%~n1.sig

echo ...finished.

echo.
goto end

rem *****************************************************************************************
rem *  Setup environment
rem *****************************************************************************************
:setup
if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%PRJDIR%" == "" set PRJDIR=%BUILDDIR%..
if "%OUTDIR%" == "" set OUTDIR=%PRJDIR%\out
if "%TOOLDIR%" == "" set TOOLDIR=%PRJDIR%\tools
if "%SUPPORTDIR%" == "" set SUPPORTDIR=%PRJDIR%\support

echo %TOOLDIR%

if not exist %TOOLDIR% goto tools_dir_missing_err

rem *****************************************************************************************
rem *  Call Hexview
rem *****************************************************************************************
:: Hexview executable
set hexview_exe=%TOOLDIR%\HexView\hexview.exe
:: Keys
set ED25519PH_publickey=%SUPPORTDIR%\keys\ed25519\pubk.txt

call %0 %1 %hexview_exe% %ED25519PH_publickey% %OUTDIR%

set hexview_exe=
set ED25519PH_publickey=
goto end

rem *****************************************************************************************
rem *  Error Handling
rem *****************************************************************************************
:tools_dir_missing_err
echo Tool Directory does not exist in path %TOOLDIR%
goto end

:usage
echo Source Hex file path missing
goto end

rem *****************************************************************************************
rem *  Exit Handling
rem *****************************************************************************************
:end
if exist %OUTDIR%\error.txt echo Error:
if exist %OUTDIR%\error.txt type %OUTDIR%\error.txt
if exist %OUTDIR%\error.txt echo.
if exist %OUTDIR%\error.txt del %OUTDIR%\error.txt
exit /b
