@echo off
rem **********************************************************************************************************************
rem    FILE DESCRIPTION
rem  --------------------------------------------------------------------------------------------------------------------
rem    \file
rem    \brief        Hardware specific prepare updater script
rem  --------------------------------------------------------------------------------------------------------------------
rem  COPYRIGHT
rem  --------------------------------------------------------------------------------------------------------------------
rem    \par Copyright
rem    \verbatim
rem    Copyright (c) 2016 by Vector Informatik GmbH.                                                 All rights reserved.
rem 
rem                This software is copyright protected and proprietary to Vector Informatik GmbH.
rem                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
rem                All other rights remain with Vector Informatik GmbH.
rem    \endverbatim
rem
rem **********************************************************************************************************************

rem **********************************************************************************************************************
rem  AUTHOR IDENTITY
rem  --------------------------------------------------------------------------------------------------------------------
rem  Name                          Initials      Company
rem  --------------------------------------------------------------------------------------------------------------------
rem  Marco Riedl                   Rie           Vector Informatik GmbH
rem  --------------------------------------------------------------------------------------------------------------------
rem  REVISION HISTORY
rem  --------------------------------------------------------------------------------------------------------------------
rem  Version   Date        Author  Change Id          Description
rem  --------------------------------------------------------------------------------------------------------------------
rem  01.00.00  2016-08-18  Rie     -                  First implementation
rem **********************************************************************************************************************
if "%1" == "" goto end
:: setup
if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%PRJDIR%" == "" set PRJDIR=%BUILDDIR%..
if "%OUTDIR%" == "" set OUTDIR=%PRJDIR%\out
if "%TOOLDIR%" == "" set TOOLDIR=%PRJDIR%\tools

set hexview_exe=%TOOLDIR%\HexView\hexview.exe

rem echo Cut everything above and under FBL region
%hexview_exe% %OUTDIR%\%~n1%~x1 /S /CR:0x00000000-0x8001FFFF /XI -o %OUTDIR%\%~n1%~x1
%hexview_exe% %OUTDIR%\%~n1%~x1 /S /CR:0x80080000-0x80FFFFFF /XI -o %OUTDIR%\%~n1%~x1

rem echo Fill Bootloader region if necessary
%hexview_exe% %OUTDIR%\%~n1%~x1 /FP:0x00 /FR:0x80020040-0x8003FFFF  /XI /S -o %OUTDIR%\%~n1%~x1

:end
echo.
exit /b