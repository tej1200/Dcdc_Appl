@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: FBL Vector SLP3 (FBL_Vector_SLP3)
rem *          Customer: AMK Autmotive GmbH & Co. KG
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: AURIX TC275T DC-Step
rem *           Author : Melvin Richard John
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP CBD2000226_D01.
rem *                    The usage is restricted to Netwalk GmbH AMK_P319_Security Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    GmbH SIP CBD2000226_D01.
rem *             Usage: Generates ED25519 signature on SHA2-512 hashed data in hex file.
rem *   Input parameter: FileName (with relative/absolute path) with .HEX file type
rem *            Output: FileName.sig contains comma seperated 64 byte ED25519 signature on
rem *                    SHA2-512 hashed data
rem *****************************************************************************************

if "%1" == "" goto usage
if "%2" == "" goto setup

echo.
echo Generate signature (SHA256)
echo %~d1%~p1%~n1.txt
echo.

echo Calculate SHA256 signature ...
%2 %1 /S -e:%3\error.txt /cs20:%3\%~n1.txt

::To handle possible bug in Hexview 
for /f "tokens=1 delims=:" %%a IN ("%CD%") do del %%a
echo ...finished.

echo.
goto end

rem *****************************************************************************************
rem *  Setup environment
rem *****************************************************************************************
:setup
if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%OUTDIR%" == "" set OUTDIR=%BUILDDIR%..\out
if "%PRJDIR%" == "" set PRJDIR=%BUILDDIR%..
if "%TOOLDIR%" == "" set TOOLDIR=%PRJDIR%\tools
if "%SUPPORTDIR%" == "" set SUPPORTDIR=%PRJDIR%\support

if not exist %TOOLDIR% goto tools_dir_missing_err

rem *****************************************************************************************
rem *  Call Hexview
rem *****************************************************************************************
if "%HEXVIEW_EXE%" == "" set HEXVIEW_EXE=%TOOLDIR%\HexView\hexview.exe

call %0 %~1 %HEXVIEW_EXE% %OUTDIR%

goto end

rem *****************************************************************************************
rem *  Error Handling
rem *****************************************************************************************
:tools_dir_missing_err
echo Tool Directory does not exist in path %TOOLDIR%
goto end

:usage
echo Source Hex file path missing
goto end

rem *****************************************************************************************
rem *  Exit Handling
rem *****************************************************************************************
:end
if exist %OUTDIR%\error.txt echo Error:
if exist %OUTDIR%\error.txt type %OUTDIR%\error.txt
if exist %OUTDIR%\error.txt echo.
if exist %OUTDIR%\error.txt del %OUTDIR%\error.txt
exit /b
