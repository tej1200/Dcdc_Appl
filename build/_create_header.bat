@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************

set TARGET_FILENAME=%1

set HEXFILE_EXT=hex
set SIGFILE_EXT=sig

if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%MAKEDIR%" == "" set MAKEDIR=%BUILDDIR%..\make
if "%OUTDIR%" == "" set OUTDIR=%BUILDDIR%..\out
if "%SUPPORTDIR%" == "" set SUPPORTDIR=%BUILDDIR%..\support
if "%TOOLDIR%" == "" set TOOLDIR=%BUILDDIR%..\tools

set ED25519PH_GENERATOR=%BUILDDIR%.\_generate_ED25519PH.bat
set SHA256_GENERATOR=%BUILDDIR%.\_generate_SHA256.bat
set SIGNATURE_TO_HEX=%BUILDDIR%.\_generate_signature_hex.bat
set SIGNATURE_TARGET_MERGE=%BUILDDIR%.\_merge_signature_hex.bat

set HEXVIEW_EXE=%TOOLDIR%\Hexview\hexview.exe

set FLASH_START_ADDR=0x00000000
set PFLASH0_END_ADDR=0x80FFFFFF
set SWE_HASH_OFFSET=80
set SWEHEADERS_SIG_OFFSET=C0

echo Cut everything above and under FBL region
set SWE_TEMP_ADDR=%3
set SWE_HASH_START_ADDR=%SWE_TEMP_ADDR:~0,8%%SWE_HASH_OFFSET%
set SWEHEADER_SIG_START_ADDR=%SWE_TEMP_ADDR:~0,8%%SWEHEADERS_SIG_OFFSET%

:: %1 - TARGET_FILENAME
:: %2 - ADDR_BEFORE_HEADER
:: %3 - HEADER_START_ADDR
:: %4 - HEADER_END_ADDR
:: %5 - ADDR_BEFORE_SWE_START
:: %6 - SWE_START_ADDR
:: %7 - SWE_END_ADDR
:: %8 - ADDR_AFTER_SWE

::1.0 prepare header info and swe area
::1.1 remove all data before header address range
if exist %OUTDIR%\%TARGET_FILENAME%.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%.%HEXFILE_EXT% /S /CR:%FLASH_START_ADDR%-%2 /XI -o %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT%

::1.2 remove all data after header address range
if exist %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT%  %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT% /S /CR:%4-%PFLASH0_END_ADDR% /XI -o %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT%

::1.3 zerofill empty areas header address range
if exist %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT% /FP:0x00 /FR:%3-%4  /XI /S -o %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT% 

::1.4 remove all data before swe address range
if exist %OUTDIR%\%TARGET_FILENAME%.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%.%HEXFILE_EXT% /S /CR:%FLASH_START_ADDR%-%5 /XI -o %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT%

::1.5 remove all data after swe address range
if exist %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% /S /CR:%8-%PFLASH0_END_ADDR% /XI -o %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT%

::1.6 zerofill empty areas swe address range
if exist %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% /FP:0x00 /FR:%6-%7  /XI /S -o %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT%

::2.0 generate SHA256 Hash
::2.1 generate SHA256 Hash
call %SHA256_GENERATOR% %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT%

::2.2 convert SHA256 Hash to hex
if exist %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% call %SIGNATURE_TO_HEX% %TARGET_FILENAME%_wo_header.txt %SWE_HASH_START_ADDR%

::3.0 merge header info with SHA256 Hash
::3.1 zerofill empty areas swe address range
if exist %OUTDIR%\%TARGET_FILENAME%_wo_header_sig.%HEXFILE_EXT% %HEXVIEW_EXE% %OUTDIR%\%TARGET_FILENAME%_wo_header_sig.%HEXFILE_EXT% /FP:0x00 /FR:%SWE_HASH_START_ADDR%,0x40 /XI /S -o %OUTDIR%\%TARGET_FILENAME%_wo_header_sig.%HEXFILE_EXT%

::3.2 merge header info with SHA256 Hash
if exist %OUTDIR%\%TARGET_FILENAME%_wo_header_sig.%HEXFILE_EXT% call %SIGNATURE_TARGET_MERGE% %OUTDIR%\%TARGET_FILENAME%_headerinfo.%HEXFILE_EXT% %OUTDIR%\%TARGET_FILENAME%_wo_header_sig.%HEXFILE_EXT% %OUTDIR%\%TARGET_FILENAME%_header_wo_sig.%HEXFILE_EXT%

::4.0 Merge SWE header signature
::4.1 create header --> header info + SWE signature + SWE header signature
::@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
:: Adding the Header Signed Signature  
::@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if exist %OUTDIR%\%TARGET_FILENAME%_header_wo_sig.%HEXFILE_EXT% call %SIGNATURE_TARGET_MERGE% %OUTDIR%\%TARGET_FILENAME%_header_wo_sig.%HEXFILE_EXT% %SUPPORTDIR%\header\%TARGET_FILENAME%_header_wo_sig_sig.%HEXFILE_EXT% %OUTDIR%\%TARGET_FILENAME%_header.%HEXFILE_EXT%

set SWE_TEMP_ADDR=
set SWE_HASH_START_ADDR=
set SWEHEADER_SIG_START_ADDR=

set HEXFILE_EXT=
set SIGFILE_EXT=

set ED25519PH_GENERATOR=
set SIGNATURE_TO_HEX=
set SIGNATURE_TARGET_MERGE=

set HEXVIEW_EXE=

set FLASH_START_ADDR=
set PFLASH0_END_ADDR=

set BUILDDIR=
set MAKEDIR=
set OUTDIR=
set SUPPORTDIR=
set TOOLDIR=
set ED25519PH_GENERATOR=
set SHA256_GENERATOR=
set SIGNATURE_TO_HEX=