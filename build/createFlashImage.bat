@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************
rem *            Usage : Wrapper to generate SWE Header and append to SWE to create flash image.
rem *****************************************************************************************
set ADDR_BEFORE_HEADER=0x8001FFFF
set HEADER_START_ADDR=0x80020000
set HEADER_END_ADDR=0x8002007F
set ADDR_BEFORE_SWE_START=0x800200FF
set SWE_START_ADDR=0x80020100
set SWE_END_ADDR=0x8003FFFF
set ADDR_AFTER_SWE=0x80040000

call _create_header.bat Bl %ADDR_BEFORE_HEADER% %HEADER_START_ADDR% %HEADER_END_ADDR% %ADDR_BEFORE_SWE_START% %SWE_START_ADDR% %SWE_END_ADDR% %ADDR_AFTER_SWE% 

if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%MAKEDIR%" == "" set MAKEDIR=%BUILDDIR%..\make
if "%OUTDIR%" == "" set OUTDIR=%BUILDDIR%..\out
if "%SUPPORTDIR%" == "" set SUPPORTDIR=%BUILDDIR%..\support
if "%TOOLDIR%" == ""set TOOLDIR=%BUILDDIR%..\tools

set HEXFILE_EXT=hex
set SIGFILE_EXT=sig
set SIGNATURE_TARGET_MERGE=%BUILDDIR%.\_merge_signature_hex.bat

echo merge Header and SWE to generate Image

if exist %OUTDIR%\%TARGET_FILENAME%_header.%HEXFILE_EXT% call %SIGNATURE_TARGET_MERGE% %OUTDIR%\%TARGET_FILENAME%_header.%HEXFILE_EXT% %OUTDIR%\%TARGET_FILENAME%_wo_header.%HEXFILE_EXT% %OUTDIR%\%TARGET_FILENAME%_swe.%HEXFILE_EXT%
if "%1" == "" goto exithandler
exit /b

:exithandler
set HEXFILE_EXT=
set SIGFILE_EXT=
set SIGNATURE_TARGET_MERGE=

set BUILDDIR=
set MAKEDIR=
set OUTDIR=
set SUPPORTDIR=
set TOOLDIR=
exit /b
