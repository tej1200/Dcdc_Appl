@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************
rem *            Usage : Converts coma seperated bytes into INTEL-HEX file with or without offset. 
rem *   Input parameter: FileName (with relative/absolute path)
rem *                    Offset for Hex file in bytes. (Optional)
rem *            Output: FileName.hex contains Intel-HEX formatted data.
rem *****************************************************************************************

if "%1" == "" goto usage

rem *****************************************************************************************
rem *  Setup environment
rem *****************************************************************************************
:: setup
if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%PRJDIR%" == "" set PRJDIR=%BUILDDIR%..
if "%OUTDIR%" == "" set OUTDIR=%PRJDIR%\out
if "%TOOLDIR%" == "" set TOOLDIR=%PRJDIR%\tools

if not exist %TOOLDIR% goto tools_dir_missing_err

rem *****************************************************************************************
rem *  Call Hexview
rem *****************************************************************************************
if "%HEXVIEW_EXE%" == "" set HEXVIEW_EXE=%TOOLDIR%\HexView\hexview.exe

echo Convert signature/ checksum into hex file format..
:: generate signature as hex file
:: without offset
if "%2" == "" %HEXVIEW_EXE% /IA:%OUTDIR%\%~1 -e:%OUTDIR%\error.txt /s /XI:32 -o %OUTDIR%\%~n1_sig.hex
:: with offset
%HEXVIEW_EXE% /IA:%OUTDIR%\%~1;%2 -e:%OUTDIR%\error.txt /s /XI:32 -o %OUTDIR%\%~n1_sig.hex

goto end

rem *****************************************************************************************
rem *  Error Handling
rem *****************************************************************************************
:tools_dir_missing_err
echo Tool Directory does not exist in path %TOOLDIR%
goto end

:usage
echo Input Signature file path missing
goto end

rem *****************************************************************************************
rem *  Exit Handling
rem *****************************************************************************************
:end
if exist %OUTDIR%\error.txt echo Error:
if exist %OUTDIR%\error.txt type %OUTDIR%\error.txt
if exist %OUTDIR%\error.txt echo.
if exist %OUTDIR%\error.txt del %OUTDIR%\error.txt
exit /b