@echo off

rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************
rem *            Usage : Merges 2 INTEL-HEX files
rem *   Input parameter: Input FileName 1 (with relative/absolute path)
rem *                    Input FileName 2 (with relative/absolute path)
rem *                    Output FileName (with relative/absolute path)
rem *            Output: FileName.hex contains merged Intel-HEX data.
rem *****************************************************************************************

if "%1" == "" goto usage
if "%2" == "" goto usage
if "%3" == "" goto usage

:: setup
if "%BUILDDIR%" == "" set BUILDDIR=%~dp0
if "%PRJDIR%" == "" set PRJDIR=%BUILDDIR%..
if "%OUTDIR%" == "" set OUTDIR=%PRJDIR%\out
if "%TOOLDIR%" == "" set TOOLDIR=%PRJDIR%\tools

if not exist %TOOLDIR% goto tools_dir_missing_err

rem *****************************************************************************************
rem *  Call Hexview
rem *****************************************************************************************
:: Hexview executable
set hexview_exe=%TOOLDIR%\HexView\hexview.exe

echo Merging Signature to hex file..
:: generate merge file
%hexview_exe% /MT:%1+%2 -e:%OUTDIR%\error.txt /s /XI:32 -o %3

set hexview_exe=

goto end

rem *****************************************************************************************
rem *  Error Handling
rem *****************************************************************************************
:tools_dir_missing_err
echo Tool Directory does not exist in path %TOOLDIR%
goto end

:usage
echo Error^! Wrong usage^!
echo manual:
echo _merge_signature_hex.bat ^<first input file^> ^<second input file^> ^<output file^>
goto end

rem *****************************************************************************************
rem *  Exit Handling
rem *****************************************************************************************
:end
if exist %OUTDIR%\error.txt echo Error:
if exist %OUTDIR%\error.txt type %OUTDIR%\error.txt
if exist %OUTDIR%\error.txt echo.
if exist %OUTDIR%\error.txt del %OUTDIR%\error.txt
exit /b