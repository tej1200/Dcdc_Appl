@echo off

rem *****************************************************************************
rem * This is a template batch file. The path might be adapted
rem *****************************************************************************

rem *****************************************************************************
rem * MAKESUPPORT_DIR has to be set to MakeSupport root directory
rem * There MUST NOT be any blanks between the '=' sign and the path string
rem * Example: 
rem *     set MAKESUPPORT_DIR=..\..\..\MakeSupport
rem *****************************************************************************

rem *****************************************************************************************
rem * Path variables adapted to suit customization of build chain by Netwalk GmbH.
rem * Done in Calling build files build.bat and buildall.bat 
rem * Adapted for Security Project.
rem *****************************************************************************************
rem set MAKESUPPORT_DIR=..\make\MakeSupport

rem Check if SWCP was used to set MAKESUPPORT_DIR
rem   If not, its value is set to ""
set VAR_NAME=MAKESUPPORT_DIR
if %MAKESUPPORT_DIR% == $$%VAR_NAME% set MAKESUPPORT_DIR=

rem *****************************************************************************
rem * DO NOT EDIT ANYTHING BELOW THIS
rem *
rem * %~dp0 is the location where this file is started from.
rem *****************************************************************************

if "%MAKESUPPORT_DIR% "==" "   goto ErrorNotSet
if not exist %MAKESUPPORT_DIR% goto ErrorWrongPath

rem *****************************************************************************
rem * Run make and store its return value afterwards.
rem * The return value is passed to the caller of m.bat at the end of the file.
rem *****************************************************************************

cd %MAKEDIR%

make -Otarget %* 

cd %BUILDDIR%
set GNU_MAKE_RETURN_CODE=%ERRORLEVEL%

goto End

:ErrorNotSet
echo ********************************************************************************
echo Warning: MAKESUPPORT_DIR has to be set to MakeSupport\cygwin_root\cmd directory!
echo          Please correct setting in this batch file and try again!
echo ********************************************************************************
goto End

:ErrorWrongPath
echo ********************************************************************************
echo Warning: %MAKESUPPORT_DIR% does not exist
echo          Please correct setting in this batch file and try again!
echo ********************************************************************************
goto End

:End
rem *****************************************************************************************
rem * Transmit error code
rem *****************************************************************************************

echo %GNU_MAKE_RETURN_CODE% >%OUTDIR%\err.tmp

exit /b %GNU_MAKE_RETURN_CODE%
