@echo off
rem *****************************************************************************************
rem *            Module: Bootloader
rem *           Program: 
rem *          Customer: 
rem *       Expiry Date: Not restricted
rem *  Ordered Derivat.: 
rem *           Author : 
rem *     License Scope: The usage is restricted to Vector Informatik GmbH SIP .
rem *                    The usage is restricted to Netwalk GmbH  Project.
rem *       Limitations: The usage is based on underlying build environment of Vector Informatik 
rem *                    
rem *****************************************************************************************

if "%1" == "body" goto body

rem *****************************************************************************************
rem *  Set Start time
rem *****************************************************************************************
set start=%TIME%

rem *****************************************************************************************
rem *  Set build variables
rem *****************************************************************************************
set BUILDDIR=%~dp0
set MAKEDIR=%BUILDDIR%..\make
set OUTDIR=%BUILDDIR%..\out
set OLDPATH=%PATH%

rem *****************************************************************************************
rem *  Set Make Support Directory and Cygwin Path
rem *****************************************************************************************
set MAKESUPPORT_DIR=..\make\MakeSupport
rem set MAKESUPPORT_DIR=C:\Project\Git\dcdc_appl\make\MakeSupport
rem call %MAKESUPPORT_DIR%\set_cygwin_path.bat

pushd "%~dp0%MAKESUPPORT_DIR%"
rem echo %cd%
call set_cygwin_path.bat
pushd %BUILDDIR%
rem pause
rem *****************************************************************************************
rem * Clear error code
rem *****************************************************************************************
set GNU_MAKE_ERROR_CODE=

rem *****************************************************************************************
rem *  Setup output directory
rem *****************************************************************************************
rmdir %OUTDIR% /q /s 2>nul
mkdir %OUTDIR%
if exist %OUTDIR% (touch %OUTDIR%\logfile.log) else (exit \b 1)

rem *****************************************************************************************
rem *  Call build script
rem *****************************************************************************************
call %0 body | tee %OUTDIR%\logfile.log

rem *****************************************************************************************
rem * Transmit error code
rem *****************************************************************************************
set /p GNU_MAKE_ERROR_CODE=<%OUTDIR%\err.tmp
del %OUTDIR%\err.tmp 2>nul

rem *****************************************************************************************
rem *  Clear build variables
rem *****************************************************************************************
set BUILDDIR=
set MAKEDIR=
set OUTDIR=
set OLDPATH=
set MAKESUPPORT_DIR=

rem *****************************************************************************************
rem *  End
rem *****************************************************************************************
exit /b %GNU_MAKE_ERROR_CODE%

rem *****************************************************************************************
rem *  Build script - Clean Build
rem *****************************************************************************************
:body
call _b.bat 2>&1

set PATH=%OLDPATH%
cd %BUILDDIR%

rem *****************************************************************************************
rem *  Set End time
rem *****************************************************************************************
set end=%time%

rem *****************************************************************************************
rem *  Compute Build time
rem *****************************************************************************************
set options="tokens=1-4 delims=:.,"
for /f %options% %%a in ("%start%") do set start_h=%%a&set /a start_m=100%%b %% 100&set /a start_s=100%%c %% 100&set /a start_ms=100%%d %% 100
for /f %options% %%a in ("%end%") do set end_h=%%a&set /a end_m=100%%b %% 100&set /a end_s=100%%c %% 100&set /a end_ms=100%%d %% 100

set /a hours=%end_h%-%start_h%
set /a mins=%end_m%-%start_m%
set /a secs=%end_s%-%start_s%
set /a ms=%end_ms%-%start_ms%
if %ms% lss 0 set /a secs = %secs% - 1 & set /a ms = 100%ms%
if %secs% lss 0 set /a mins = %mins% - 1 & set /a secs = 60%secs%
if %mins% lss 0 set /a hours = %hours% - 1 & set /a mins = 60%mins%
if %hours% lss 0 set /a hours = 24%hours%
if 1%ms% lss 100 set ms=0%ms%

set /a totalsecs = %hours%*3600 + %mins%*60 + %secs%
set buildtime=%hours%:%mins%:%secs%.%ms% (%totalsecs%.%ms%s total) 
echo Build time %buildtime% 

pause