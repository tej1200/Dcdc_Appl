###############################################################################
#                                                                             #
#       Copyright (C) 2020 Infineon Technologies AG. All rights reserved.     #
#                                                                             #
#                                                                             #
#                              IMPORTANT NOTICE                               #
#                                                                             #
#                                                                             #
# Infineon Technologies AG (Infineon) is supplying this file for use          #
# exclusively with Infineonís microcontroller products. This file can be      #
# freely distributed within development tools that are supporting such        #
# microcontroller products.                                                   #
#                                                                             #
# THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED #
# OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF          #
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.#
# INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,#
# OR CONSEQUENTIAL DAMAGES, FOR	ANY REASON WHATSOEVER.                        #
#                                                                             #
###############################################################################

ifeq ($(COMPILER),Ghs2018)

B_GHS_TRICORE_PATH?= $(GREEN_HILLS_HOME)\comp_201815

B_GHS_TRICORE_CC_OPTIONS= -gnu99 -g -cpu=tc1v162 --xref=full -tricore_ppccompat_abi -sda=none -roxda \
		-minlib -Ogeneral -Onoprintfuncs -Onounroll -Onotailrecursion -Onoexplodejumps --unsigned_chars \
		-ffunctions -fsingle -no_fused_madd -Oinline_constant_math -no_float_associativity -fno_NaN_cmp_unordered \
		-no_precise_signed_zero -half_precision_type --no_commons --no_vla --gnu_asm -dual_debug --no_short_enum \
		--diag_error 39,549,940,1056,1780,1999 --diag_warning 193,1705,1706,1709,1710,1718,1729,1735,1777,1826,1835,2017 \
		-srec -lnk=-no_append -nostartfiles -map -Mn -Mx -nostdlib

B_GHS_TRICORE_ASM_OPTIONS= $(B_GHS_TRICORE_CC_OPTIONS)

B_GHS_TRICORE_LIB_INC= -L"$(B_GHS_TRICORE_PATH)/lib/tri162_compat"

B_GHS_TRICORE_LIBS= -lansi -lstartup -lind_sd -larch -lsys -lmath_sd -lwc_s32

B_GHS_TRICORE_LD_OPTIONS= $(B_GHS_TRICORE_CC_OPTIONS)

endif
