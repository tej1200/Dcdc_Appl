/**
 * \file
 *
 * \brief AUTOSAR ApplTemplates
 *
 * This file contains the implementation of the AUTOSAR
 * module ApplTemplates.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2019 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if (!defined MCU_STUBEXT_H)
#define MCU_STUBEXT_H

#define McuModeSettingConf_0 0U

extern CONST(Mcu_ConfigType, EBSTUBS_CONST) McuModuleConfiguration_0;

#endif /* !defined MCU_STUBEXT_H */
