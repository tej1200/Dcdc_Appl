/**
 * \file
 *
 * \brief AUTOSAR ApplTemplates
 *
 * This file contains the implementation of the AUTOSAR
 * module ApplTemplates.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2019 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#include <Mcu.h>

#define EBSTUBS_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

CONST(Mcu_ConfigType, EBSTUBS_CONST) McuModuleConfiguration_0;

#define EBSTUBS_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
