#if (!defined PLATFORMS_TSPLATFORMS_H)
#define PLATFORMS_TSPLATFORMS_H
/**
 * \file
 *
 * \brief AUTOSAR Platforms
 *
 * This file contains the implementation of the AUTOSAR
 * module Platforms.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2019 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]=============================================*/

/*==================[type definitions]=======================================*/

/*==================[macros]=================================================*/

/** \brief definition of derivate names for this architecture */
#define TS_WIN32X86 1U

#endif /* if (!defined PLATFORMS_TSPLATFORMS_H) */

