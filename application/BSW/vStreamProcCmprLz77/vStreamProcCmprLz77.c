/***********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProcCmprLz77.c
 *        \brief  vStreamProcCmprLz77 source file
 *      \details  -
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProcCmprLz77 module. >> vStreamProcCmprLz77.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 **********************************************************************************************************************/

#define VSTREAMPROCCMPRLZ77_SOURCE

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/
#include "vStreamProcCmprLz77.h"
#include "vStreamProc.h"
#include "vStreamProc_ProcessingNode.h"

#if (VSTREAMPROCCMPRLZ77_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/*********************************************************************************************************************** 
 *  VERSION CHECK
 **********************************************************************************************************************/
/* Check the version of vStreamProcCmprLz77 header file */
#if (  (VSTREAMPROCCMPRLZ77_SW_MAJOR_VERSION != (2u)) \
    || (VSTREAMPROCCMPRLZ77_SW_MINOR_VERSION != (0u)) \
    || (VSTREAMPROCCMPRLZ77_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of vStreamProcCmprLz77.c and vStreamProcCmprLz77.h are inconsistent"
#endif

/***********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 ***********************************************************************************************************************/
#define VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_POINTER_OFFSET    (0u)
#define VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_OFFSET     VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_POINTER_OFFSET
#define VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_LEN_OFFSET        (1u)
#define VSTREAMPROCCMPRLZ77_TOKEN_DISTANCE_OFFSET          (1u)
#define VSTREAMPROCCMPRLZ77_TOKEN_SIZE                     ((vStreamProc_LengthType)2u)
#define VSTREAMPROCCMPRLZ77_TOKEN_LITERAL_STRING_OFFSET    VSTREAMPROCCMPRLZ77_TOKEN_SIZE

#define VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_LENGTH     (1u)

#define VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_LEN_BIT_IDX       (4u)
#define VSTREAMPROCCMPRLZ77_TOKEN_DISTANCE_MASK         (0x0Fu)

/***********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 ***********************************************************************************************************************/
#if !defined (VSTREAMPROCCMPRLZ77_LOCAL)
# define VSTREAMPROCCMPRLZ77_LOCAL static
#endif

#if !defined (VSTREAMPROCCMPRLZ77_LOCAL_INLINE)
# define VSTREAMPROCCMPRLZ77_LOCAL_INLINE LOCAL_INLINE
#endif

#define VSTREAMPROCCMPRLZ77_GET_PHRASE_LEN(buffer) \
          ((buffer)[VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_LEN_OFFSET] >> VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_LEN_BIT_IDX)
#define VSTREAMPROCCMPRLZ77_GET_DISTANCE(buffer)   \
          ((buffer)[VSTREAMPROCCMPRLZ77_TOKEN_DISTANCE_OFFSET] & VSTREAMPROCCMPRLZ77_TOKEN_DISTANCE_MASK)

/***********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 ***********************************************************************************************************************/
typedef struct
{
  /*! Amount of data available at ReadPtr and to the right of it. */
  vStreamProc_LengthType        AvailableReadLength;
  /*! Amount of data already consumed to the left of ReadPtr. */
  vStreamProc_LengthType        ConsumedReadLength;
  /*! Amount of space available at WritePtr and to the right of it. */
  vStreamProc_LengthType        AvailableWriteLength;
  /*! Amount of data already produced to the left of WritePtr. */
  vStreamProc_LengthType        ConsumedWriteLength;
  /*! Pointer to read next data from. To be updated only together with AvailableReadLength and ConsumedReadLength. */
  P2CONST(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA)   ReadPtr;
  /*! Pointer to write next data to. To be updated only together with AvailableWriteLength and ConsumedWriteLength. */
  P2VAR(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA)     WritePtr;
} vStreamProcCmprLz77_BufferType;

/***********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 ***********************************************************************************************************************/

/***********************************************************************************************************************
 *  GLOBAL DATA
 ***********************************************************************************************************************/

/***********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 ***********************************************************************************************************************/
# define VSTREAMPROCCMPRLZ77_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

# if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON)

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateWindowPos
 ***********************************************************************************************************************/
 /*!
 * \brief         Increments and wraps a position pointer into the sliding window.
 * \details       -
 * \param[in]     WindowPos   Original window position.
 * \return        Incremented and wrapped window position.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_LengthType, AUTOMATIC) vStreamProcCmprLz77_UpdateWindowPos(
  vStreamProc_LengthType WindowPos);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_GetMaxRetVal
 ***********************************************************************************************************************/
 /*!
 * \brief         Returns the worse of two return values.
 * \details       -
 * \param[in]     RetVal1    A vStreamProc return value.
 * \param[in]     RetVal2    Another vStreamProc return value.
 * \return        MAX(RetVal1, RetVal2)
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_GetMaxRetVal(
  vStreamProc_ReturnType RetVal1,
  vStreamProc_ReturnType RetVal2);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateFromLiteral
 ***********************************************************************************************************************/
 /*!
 * \brief         Updates OutputBuffer and sliding window from a literal input stream.
 * \details       -
 * \param[out]    CmprWsPtr      The workspace containing the sliding window to update.
 * \param[out]    OutputBuffer   The output buffer to write into. Must be large enough to hold InputLength.
 * \param[in]     InputBuffer    The input buffer to copy data from.
 * \param[in]     InputLength    Number of literals to copy. Valid range: 0..15.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL FUNC(void, AUTOMATIC) vStreamProcCmprLz77_UpdateFromLiteral(
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) OutputBuffer,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) InputBuffer,
  uint8 InputLength);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateFromWindow
 ***********************************************************************************************************************/
 /*!
 * \brief         Updates OutputBuffer and sliding window from the sliding window.
 * \details       -
 * \param[in,out] CmprWsPtr           The workspace containing the sliding window to read and update.
 * \param[out]    OutputBuffer        The output buffer to write into. Must be large enough to hold PhraseLength.
 * \param[in]     PhrasePointer       The sliding window position to copy data from.
 * \param[in]     PhraseLength        Number of elements to copy. Valid range: 0..15.
 * \return        VSTREAMPROC_OK      Update successful.
 * \return        VSTREAMPROC_FAILED  PhrasePointer outside valid range of sliding window.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_UpdateFromWindow(
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) OutputBuffer,
  uint8 PhrasePointer,
  uint8 PhraseLength);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_DecompressSingleBlock
 ***********************************************************************************************************************/
 /*!
 * \brief         Decompresses a single data block from the input buffer to the output buffer.
 * \details       -
 * \param[in,out] NodeInfo            The NodeInfo structure holding port specific results to be updated.
 * \param[in,out] CmprWsPtr           The workspace containing the sliding window to read and update.
 * \param[in,out] Buffer              The Buffer to use for read and write. Read buffer must hold at least the token size of data.
 *                                    Will be updated if a block was successfully decompressed, may be modified at failure.
 * \return        VSTREAMPROC_OK                   Decompression successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT   Input data does not form a complete data block.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT  Output buffer unable to hold decompressed data.
 * \return        VSTREAMPROC_FAILED               Decompression failed.
 * \pre           Buffer holds valid length and pointer information.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_DecompressSingleBlock(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo,
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(vStreamProcCmprLz77_BufferType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) Buffer);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_PrepareDecompression
 ***********************************************************************************************************************/
 /*!
 * \brief         Prepares input and output ports and the buffer structure for decompression.
 * \details       -
 * \param[in,out] NodeInfo            The NodeInfo structure used for input and output port access.
 *                                    Port specific results will be updated on failure.
 * \param[out] InputPortInfo          The PortInfo structure used for input access.
 * \param[out] OutputPortInfo         The PortInfo structure used for output access.
 * \param[out] Buffer                 The Buffer structure to be prepared.
 * \return        VSTREAMPROC_OK                   Preparation successful.
 * \return        VSTREAMPROC_FAILED               Preparation failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_PrepareDecompression(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) OutputPortInfo,
  P2VAR(vStreamProcCmprLz77_BufferType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) Buffer);

/***********************************************************************************************************************
 *  LOCAL FUNCTIONS
 ***********************************************************************************************************************/

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateWindowPos
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_LengthType, AUTOMATIC) vStreamProcCmprLz77_UpdateWindowPos(
  vStreamProc_LengthType WindowPos)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType windowPos = WindowPos;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Increment passed WindowPos and wrap it using VSTREAMPROCCMPRLZ77_WINDOW_MASK. */
  windowPos++;
  windowPos &= VSTREAMPROCCMPRLZ77_WINDOW_MASK;

  return windowPos;
}

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_GetMaxRetVal
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_GetMaxRetVal(
  vStreamProc_ReturnType RetVal1,
  vStreamProc_ReturnType RetVal2)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = RetVal1;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the larger of the two given return values. */
  if (RetVal2 > retVal)
  {
    retVal = RetVal2;
  }

  return retVal;
} /* vStreamProcCmprLz77_GetMaxRetVal() */

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateFromWindow
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_UpdateFromWindow(
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) OutputBuffer,
  uint8 PhrasePointer,
  uint8 PhraseLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least outIdx;
  /* HINT: Calculation of the read position may underflow, as the sliding window is considered continuous.
           The subsequent mask operation turns this into a wrap-around according to the defined algorithm. */
  vStreamProc_LengthType readPos = ((CmprWsPtr->SlidingWindowPos - (vStreamProc_LengthType)PhrasePointer) & VSTREAMPROCCMPRLZ77_WINDOW_MASK);
  vStreamProc_LengthType writePos = CmprWsPtr->SlidingWindowPos;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return an error if the PhrasePointer does not point within the valid range of the sliding window. */
  if (readPos < CmprWsPtr->SlidingWindowValidRange)
  {
    /* #20 For each byte of the phrase: */
    for (outIdx = 0u; outIdx < PhraseLength; outIdx++)
    {
      /* #30 Read data from the sliding window. Start at PhrasePointer bytes before the current window position,
             then cycle through the buffer with each iteration. */
      uint8 data = CmprWsPtr->SlidingWindow[readPos];
      /* #40 Write it to the output buffer. */
      OutputBuffer[outIdx] = data;
      /* #50 Write a copy into the sliding window. Start at current window position,
             then cycle through the buffer with each iteration. */
      CmprWsPtr->SlidingWindow[writePos] = data;

      /* #60 Update the read and write position in the sliding window. */
      readPos = vStreamProcCmprLz77_UpdateWindowPos(readPos);
      writePos = vStreamProcCmprLz77_UpdateWindowPos(writePos);
    }

    /* #70 Store new window position and valid range in workspace. */
    CmprWsPtr->SlidingWindowPos = writePos;
    /* HINT: The valid range may grow beyond the size of the sliding window, but the comparison is done with 32 bit,
             so it will never overflow and we can save the effort for limiting the range. */
    CmprWsPtr->SlidingWindowValidRange += (vStreamProc_LengthType)PhraseLength;
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
} /* vStreamProcCmprLz77_UpdateFromWindow() */

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_UpdateFromLiteral
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL FUNC(void, AUTOMATIC) vStreamProcCmprLz77_UpdateFromLiteral(
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) OutputBuffer,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) InputBuffer,
  uint8 InputLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least inOutIdx;
  vStreamProc_LengthType writePos = CmprWsPtr->SlidingWindowPos;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 For each byte of the InputBuffer: */
  for (inOutIdx = 0u; inOutIdx < InputLength; inOutIdx++)
  {
    /* #20 Read data from the InputBuffer. */
    uint8 data = InputBuffer[inOutIdx];
    /* #30 Write it to the output buffer. */
    OutputBuffer[inOutIdx] = data;
    /* #40 Write a copy into the sliding window. Start at current window position,
           then cycle through the buffer with each iteration. */
    CmprWsPtr->SlidingWindow[writePos] = data;

    /* #50 Update the write position in the sliding window. */
    writePos = vStreamProcCmprLz77_UpdateWindowPos(writePos);
  }

  /* #60 Store new window position and valid range in workspace. */
  CmprWsPtr->SlidingWindowPos = writePos;
  /* HINT: The valid range may grow beyond the size of the sliding window, but the comparison is done with 32 bit,
            so it will never overflow and we can save the effort for limiting the range. */
  CmprWsPtr->SlidingWindowValidRange += (vStreamProc_LengthType)InputLength;
} /* vStreamProcCmprLz77_UpdateFromLiteral() */

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_DecompressSingleBlock
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_DecompressSingleBlock(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo,
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) CmprWsPtr,
  P2VAR(vStreamProcCmprLz77_BufferType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) Buffer)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  uint8 phraseLength  = VSTREAMPROCCMPRLZ77_GET_PHRASE_LEN(Buffer->ReadPtr); /* Length of the phrase to copy from sliding window. */
  uint8 distance      = VSTREAMPROCCMPRLZ77_GET_DISTANCE(Buffer->ReadPtr);   /* Number of bytes to copy from input stream. */
  vStreamProc_LengthType consumeLength;
  vStreamProc_LengthType produceLength;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Calculate required input and output buffer to handle the token. */
  consumeLength = VSTREAMPROCCMPRLZ77_TOKEN_SIZE + (vStreamProc_LengthType)distance;
  produceLength = (vStreamProc_LengthType)phraseLength + (vStreamProc_LengthType)distance;
  if (phraseLength == 0u)
  {
    produceLength += VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_LENGTH;
  }

  /* #20 Report INSUFFICIENT_INPUT if uncompressed literal is incomplete. */
  if (consumeLength > Buffer->AvailableReadLength)
  {
    retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
    NodeInfo->InputPortResults[vStreamProcConf_vStreamProcInputPort_vStreamProcCmprLz77_InputData] = VSTREAMPROC_INSUFFICIENT_INPUT;
  }
  /* #30 Otherwise, report INSUFFICIENT_OUTPUT if not all resulting data fits to the output buffer. */
  else if (produceLength > Buffer->AvailableWriteLength)
  {
    retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    NodeInfo->OutputPortResults[vStreamProcConf_vStreamProcOutputPort_vStreamProcCmprLz77_OutputData] = VSTREAMPROC_INSUFFICIENT_OUTPUT;
  }
  /* #40 Otherwise, decompress the block: */
  else
  {
    vStreamProc_LengthType writeIdx;

    /* #110 If phrasePointer is not needed, handle it as a literal instead. */
    if (phraseLength == 0u)
    {
      vStreamProcCmprLz77_UpdateFromLiteral(
        CmprWsPtr,
        Buffer->WritePtr,
        &Buffer->ReadPtr[VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_OFFSET],
        VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_LENGTH);
      writeIdx = VSTREAMPROCCMPRLZ77_TOKEN_EXTRA_LITERAL_LENGTH;
    }
    /* #120 Otherwise, handle the phrase. */
    else
    {
      retVal = vStreamProcCmprLz77_UpdateFromWindow(
        CmprWsPtr,
        Buffer->WritePtr,
        Buffer->ReadPtr[VSTREAMPROCCMPRLZ77_TOKEN_PHRASE_POINTER_OFFSET],
        phraseLength);
      writeIdx = phraseLength;
    }

    if (retVal == VSTREAMPROC_OK)
    {
      /* #130 Handle the literals. */
      vStreamProcCmprLz77_UpdateFromLiteral(
        CmprWsPtr,
        &Buffer->WritePtr[writeIdx],
        &Buffer->ReadPtr[VSTREAMPROCCMPRLZ77_TOKEN_LITERAL_STRING_OFFSET],
        distance);

      /* #140 Forward the buffers. */
      Buffer->WritePtr = &Buffer->WritePtr[produceLength];
      Buffer->AvailableWriteLength -= produceLength;
      Buffer->ConsumedWriteLength  += produceLength;
      Buffer->ReadPtr = &Buffer->ReadPtr[consumeLength];
      Buffer->AvailableReadLength -= consumeLength;
      Buffer->ConsumedReadLength  += consumeLength;
    }
  }

  return retVal;
} /* vStreamProcCmprLz77_DecompressSingleBlock() */

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_PrepareDecompression
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROCCMPRLZ77_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProcCmprLz77_PrepareDecompression(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) OutputPortInfo,
  P2VAR(vStreamProcCmprLz77_BufferType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) Buffer)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set SymbolicPortNames and prepare PortInfos. */
  InputPortInfo->SymbolicPortName  = vStreamProcConf_vStreamProcInputPort_vStreamProcCmprLz77_InputData;
  OutputPortInfo->SymbolicPortName = vStreamProcConf_vStreamProcOutputPort_vStreamProcCmprLz77_OutputData;

  retVal = vStreamProc_PreparePortInfos(NodeInfo, InputPortInfo, 1u, OutputPortInfo, 1u);

  /* #20 Set DataType IDs and request port data (zero length). */
  if (retVal == VSTREAMPROC_OK)
  {
    InputPortInfo->ReadRequest.StorageInfo.DataTypeInfo.Id   = vStreamProcConf_vStreamProcDataType_uint8;
    OutputPortInfo->WriteRequest.StorageInfo.DataTypeInfo.Id = vStreamProcConf_vStreamProcDataType_uint8;
    retVal = vStreamProc_RequestPortData(NodeInfo, InputPortInfo, 1u, OutputPortInfo, 1u);
  }

  /* #30 Initialize Buffer structure with the resulting pointers, available length and a consumed length of 0. */
  Buffer->ReadPtr  = vStreamProc_GetTypedReadRequestBuffer_uint8(&InputPortInfo->ReadRequest);
  Buffer->AvailableReadLength = InputPortInfo->ReadRequest.StorageInfo.AvailableLength;
  Buffer->ConsumedReadLength = 0u;

  Buffer->WritePtr = vStreamProc_GetTypedWriteRequestBuffer_uint8(&OutputPortInfo->WriteRequest);
  Buffer->AvailableWriteLength = OutputPortInfo->WriteRequest.StorageInfo.AvailableLength;
  Buffer->ConsumedWriteLength = 0u;

  return retVal;
} /* vStreamProcCmprLz77_PrepareDecompression() */

# endif /* (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON) */

/***********************************************************************************************************************
 *   GLOBAL FUNCTIONS
 ***********************************************************************************************************************/

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_GetVersionInfo
 ***********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
#if ( VSTREAMPROCCMPRLZ77_VERSION_INFO_API == STD_ON )
FUNC(void, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) VersionInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
# if ( VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON )
  uint8 errorId = VSTREAMPROCCMPRLZ77_E_NO_ERROR;
# endif /* VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON */

  /* ----- Development Error Checks ------------------------------------- */
# if ( VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of input parameters. */
  if ( VersionInfoPtr == NULL_PTR )
  {
    errorId = VSTREAMPROCCMPRLZ77_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON */
  {
  /* ----- Implementation ----------------------------------------------- */

    /* #20 Set VersionInfo structure with corresponding macros. */
    VersionInfoPtr->vendorID   = VSTREAMPROCCMPRLZ77_VENDOR_ID;
    VersionInfoPtr->moduleID   = VSTREAMPROCCMPRLZ77_MODULE_ID;
    VersionInfoPtr->sw_major_version = VSTREAMPROCCMPRLZ77_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = VSTREAMPROCCMPRLZ77_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = VSTREAMPROCCMPRLZ77_SW_PATCH_VERSION;
  }
  /* ----- Development Error Report --------------------------------------- */
# if ( VSTREAMPROCCMPRLZ77_DEV_ERROR_REPORT == STD_ON )
  /* #30 Report development errors if any occurred. */
  if ( errorId != VSTREAMPROCCMPRLZ77_E_NO_ERROR )
  {
    (void)Det_ReportError(VSTREAMPROCCMPRLZ77_MODULE_ID, VSTREAMPROCCMPRLZ77_INSTANCE_ID_DET, VSTREAMPROCCMPRLZ77_API_ID_GET_VERSION_INFO, errorId);
  }
# else /* VSTREAMPROCCMPRLZ77_DEV_ERROR_REPORT */
#  if ( VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON )
  VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT(errorId);
#  endif /* VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT == STD_ON */
# endif /* VSTREAMPROCCMPRLZ77_DEV_ERROR_REPORT */
}
#endif /* VSTREAMPROCCMPRLZ77_VERSION_INFO_API */

# if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON)

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_InitNode
 ***********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_InitNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialization is done at wave start. Nothing to do here. */
  VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT(NodeInfo);

  return retVal;
}

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_InitSlidingWindow
 ***********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_InitSlidingWindow(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType retVal = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) cmprWsPtr;

  /* ----- Implementation ----------------------------------------------- */
  /* PRQA S 0316 1 */ /* MD_vStreamProcCmprLz77_Rule11.5_0316_CastPtrVoidPtrObj */
  cmprWsPtr = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProcCmprLz77_WorkspaceType(NodeInfo);

  /* #10 Set the sliding window position to 0. The SlidingWindow itself will not be used by the algorithm
         before being updated and thus needs no initialization. */
  cmprWsPtr->SlidingWindowPos = 0u;

  /* #20 Set the sliding window valid range to 0. */
  cmprWsPtr->SlidingWindowValidRange = 0u;

  return retVal;
}

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_Decompress()
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_Decompress(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal;
  vStreamProc_InputPortInfoType       inputPortInfo;
  vStreamProc_OutputPortInfoType      outputPortInfo;
  P2VAR(vStreamProcCmprLz77_WorkspaceType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VAR) cmprWsPtr;
  vStreamProcCmprLz77_BufferType      buffer;
  vStreamProc_SignalResponseType      response = VSTREAMPROC_SIGNALRESPONSE_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* PRQA S 0316 1 */ /* MD_vStreamProcCmprLz77_Rule11.5_0316_CastPtrVoidPtrObj */
  cmprWsPtr = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProcCmprLz77_WorkspaceType(NodeInfo);

  /* #10 Prepare port info and buffer structures. */
  retVal = vStreamProcCmprLz77_PrepareDecompression(NodeInfo, &inputPortInfo, &outputPortInfo, &buffer);

  /* #20 While no error and no buffer limit occurred: */
  while (retVal == VSTREAMPROC_OK)
  {
    /* #30 If no input data is left at all, cancel the loop (processing is in a clean state). */
    if (buffer.AvailableReadLength == 0u)
    {
      break;
    }
    /* #40 Otherwise, if available data does not form a complete token: Report INSUFFICIENT_INPUT. */
    else if (buffer.AvailableReadLength < VSTREAMPROCCMPRLZ77_TOKEN_SIZE)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
      NodeInfo->InputPortResults[vStreamProcConf_vStreamProcInputPort_vStreamProcCmprLz77_InputData] = VSTREAMPROC_INSUFFICIENT_INPUT;
    }
    /* #50 Otherwise, try to decompress the block. Report which port has insufficient buffer if any. */
    else
    {
      retVal = vStreamProcCmprLz77_DecompressSingleBlock(NodeInfo, cmprWsPtr, &buffer);
    }
  }

  /* #60 If no error occurred, acknowledge consumed and produced data.
   *     Do not overwrite return value with lower severity. */
  if (retVal != VSTREAMPROC_FAILED)
  {
    if(buffer.ConsumedReadLength > 0u)
    {
      vStreamProc_SetSignalResponseFlag(response, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);
    }
    inputPortInfo.ReadRequest.StorageInfo.RequestLength = buffer.ConsumedReadLength;
    outputPortInfo.WriteRequest.StorageInfo.RequestLength = buffer.ConsumedWriteLength;
    retVal = vStreamProcCmprLz77_GetMaxRetVal(
      retVal,
      vStreamProc_AcknowledgePorts(NodeInfo, &inputPortInfo, 1u, &outputPortInfo, 1u));

    if (retVal != VSTREAMPROC_FAILED)
    {
      vStreamProc_SetSignalResponseFlag(response, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);

      /* #70 If further processing is blocked by insufficient output, switch registration states to StorageAvailable at OutputData. */
      if (retVal == VSTREAMPROC_INSUFFICIENT_OUTPUT)
      {
        vStreamProc_SetInputPortSignalHandlerState(NodeInfo->ProcessingNodeId,
                                                   vStreamProcConf_vStreamProcInputPort_vStreamProcCmprLz77_InputData,
                                                   vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
                                                   VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);
        vStreamProc_SetOutputPortSignalHandlerState(NodeInfo->ProcessingNodeId,
                                                    vStreamProcConf_vStreamProcOutputPort_vStreamProcCmprLz77_OutputData,
                                                    vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
                                                    VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
      }
      else
      {
        vStreamProc_SetSignalResponseFlag(response, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);
      }
    }
  }

  return response;
}

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_Continue()
 ***********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_Continue(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType retVal = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Switch registration states back to DataAvailable at InputData. */
  vStreamProc_SetInputPortSignalHandlerState(NodeInfo->ProcessingNodeId,
                                             vStreamProcConf_vStreamProcInputPort_vStreamProcCmprLz77_InputData,
                                             vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
                                             VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
  vStreamProc_SetOutputPortSignalHandlerState(NodeInfo->ProcessingNodeId,
                                              vStreamProcConf_vStreamProcOutputPort_vStreamProcCmprLz77_OutputData,
                                              vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
                                              VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

  return retVal;
}

# endif /* (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON) */

# define VSTREAMPROCCMPRLZ77_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/***********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 ***********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:

MD_vStreamProcCmprLz77_Rule11.5_0316_CastPtrVoidPtrObj:
  Reason:     vStreamProc provides a void pointer in the NodeInfo to obtain the workspace. This has to be cast to
              the actual workspace type of the processing node.
  Risk:       Alignment issues may occur.
  Prevention: Conversion macro is provided by vStreamProc and used for the cast to ensure only workspaces of the
              given type are converted.

*/

/***********************************************************************************************************************
 *  END OF FILE: vStreamProcCmprLz77.c
 ***********************************************************************************************************************/

