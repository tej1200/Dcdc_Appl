/***********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  _vStreamProcCmprLz77_Cfg.h
 *        \brief  vStreamProcCmprLz77 configuration header file
 *      \details  vStreamProcCmprLz77 does not have an own generator. This template file derives values from vStreamProc
 *                core module, but may be adapted to project needs.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProcCmprLz77 module. >> vStreamProcCmprLz77.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 **********************************************************************************************************************/

#if !defined (VSTREAMPROCCMPRLZ77_CFG_H)
#define VSTREAMPROCCMPRLZ77_CFG_H

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/
#include "vStreamProc_Cfg.h"

/***********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 **********************************************************************************************************************/

#define VSTREAMPROCCMPRLZ77_VERSION_INFO_API           VSTREAMPROC_VERSION_INFO_API

#define VSTREAMPROCCMPRLZ77_DEV_ERROR_DETECT           VSTREAMPROC_DEV_ERROR_DETECT
#define VSTREAMPROCCMPRLZ77_DEV_ERROR_REPORT           VSTREAMPROC_DEV_ERROR_REPORT

#ifndef VSTREAMPROCCMPRLZ77_USE_DUMMY_STATEMENT
#define VSTREAMPROCCMPRLZ77_USE_DUMMY_STATEMENT        VSTREAMPROC_USE_DUMMY_STATEMENT                                                   /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT
#define VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT(v)         VSTREAMPROC_DUMMY_STATEMENT(v)       /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/EcuC/EcucGeneral/DummyStatementKind */
#endif
#ifndef VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT_CONST
#define VSTREAMPROCCMPRLZ77_DUMMY_STATEMENT_CONST(v)   VSTREAMPROC_DUMMY_STATEMENT_CONST(v) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/EcuC/EcucGeneral/DummyStatementKind */
#endif

#endif /* VSTREAMPROCCMPRLZ77_CFG_H */
/***********************************************************************************************************************
 *  END OF FILE: vStreamProcCmprLz77_Cfg.h
 ***********************************************************************************************************************/
