/***********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProcCmprLz77.h
 *        \brief  vStreamProcCmprLz77 header file
 *      \details  vStreamProcCmprLz77 is a vStreamProc processing node able to decompress input data streams.
 *                Decompression is performed using an algorithm defined by Vector, which is derived from LZ77.
 *                This file provides the general interface of a component (here: VersionInfo) as well as the interface
 *                towards vStreamProc as an external processing node (InitNode and Signal Handlers: InitSlidingWindow,
 *                Decompress, Continue).
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2021-07-05  visshk  -             Initial release
 *  02.00.00  2022-05-02  visshk  SWUP-1771     Upgrade to vStreamProc 3.xx.xx
 *                                ESCAN00110384 Compiler error: vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProcCmprLz77_WorkspaceType undefined
 **********************************************************************************************************************/
#if !defined (VSTREAMPROCCMPRLZ77_H)
# define VSTREAMPROCCMPRLZ77_H

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/
#include "vStreamProcCmprLz77_Cfg.h"
#include "vStreamProcCmprLz77_Types.h"
#include "vStreamProc_Types.h"

/***********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 **********************************************************************************************************************/

/* Vendor and module identification */
# define VSTREAMPROCCMPRLZ77_VENDOR_ID                         (30u)
# define VSTREAMPROCCMPRLZ77_MODULE_ID                        (255u)
# define VSTREAMPROCCMPRLZ77_INSTANCE_ID_DET                    (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VSTREAMPROCCMPRLZ77_SW_MAJOR_VERSION                   (2u)
# define VSTREAMPROCCMPRLZ77_SW_MINOR_VERSION                   (0u)
# define VSTREAMPROCCMPRLZ77_SW_PATCH_VERSION                   (0u)

/* ----- API service IDs ----- */
# define VSTREAMPROCCMPRLZ77_API_ID_INIT_NODE                   (0x01u) /*!< Service ID: vStreamProcCmprLz77_InitNode() */
# define VSTREAMPROCCMPRLZ77_API_ID_GET_VERSION_INFO            (0x02u) /*!< Service ID: vStreamProcCmprLz77_GetVersionInfo() */
# define VSTREAMPROCCMPRLZ77_API_ID_DECOMPRESS                  (0x03u) /*!< Service ID: vStreamProcCmprLz77_Decompress() */
# define VSTREAMPROCCMPRLZ77_API_ID_CONTINUE                    (0x04u) /*!< Service ID: vStreamProcCmprLz77_Continue() */
# define VSTREAMPROCCMPRLZ77_API_ID_INIT_SLIDING_WINDOW         (0x05u) /*!< Service ID: vStreamProcCmprLz77_InitSlidingWindow() */

/* ----- Error codes ----- */
# define VSTREAMPROCCMPRLZ77_E_NO_ERROR                         (0x00u) /*!< Used to check if no error occurred - use a value unequal to any error code. */
# define VSTREAMPROCCMPRLZ77_E_PARAM_POINTER                    (0x01u) /*!< Error code: API service used with invalid pointer parameter (NULL). */

/***********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
# define VSTREAMPROCCMPRLZ77_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

#if ( VSTREAMPROCCMPRLZ77_VERSION_INFO_API == STD_ON )
/***********************************************************************************************************************
 *  vStreamProcCmprLz77_GetVersionInfo
 ***********************************************************************************************************************/
/*! \brief          Returns the version information.
 *  \details        vStreamProcCmprLz77_GetVersionInfo() returns version information, vendor ID and AUTOSAR module ID of the component.
 *                  The versions are BCD-coded.
 *  \param[in]      VersionInfoPtr        pointer for version info
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \config         This function is only available if VSTREAMPROCCMPRLZ77_VERSION_INFO_API is enabled.
 *  \trace          CREQ-vStreamProcCmprLz77-Service-General-VersionInfo
 ***********************************************************************************************************************/
FUNC(void, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_DATA) VersionInfoPtr);
#endif /* VSTREAMPROCCMPRLZ77_VERSION_INFO_API == STD_ON */

# if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON)
/***********************************************************************************************************************
 *  vStreamProcCmprLz77_InitNode
 ***********************************************************************************************************************/
/*!
 * \brief         Initializes the passed node.
 * \details       Must be called for the given node instance before call of the corresponding Signal Handlers.
 * \param[in]     NodeInfo        The processing node information to operate on.
 * \return        E_OK            Initialization was successful.
 * \return        E_NOT_OK        Initialization was unsuccessful.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-vStreamProcCmprLz77-Service-vStreamProc-InitNode
 ***********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_InitNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_InitSlidingWindow
 ***********************************************************************************************************************/
/*!
 * \brief         Initializes the passed node's Sliding Window.
 * \details       Must be called for the given node instance before a new data blob is about to be decompressed, i.e.
 *                when vStreamProc starts a wave of the configured type.
 * \param[in]     NodeInfo                                The processing node information to operate on.
 * \return        VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Initialization was successful.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-vStreamProcCmprLz77-Service-vStreamProc-InitSlidingWindow
 ***********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_InitSlidingWindow(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo);

/***********************************************************************************************************************
*  vStreamProcCmprLz77_Decompress
 ***********************************************************************************************************************/
/*!
 * \brief         Decompresses the passed node's input data to the output.
 * \details       If there is any data at the input, two bytes are peeked (token). If the input block is complete,
 *                and enough output memory is available, the block is decompressed. This is repeated until either no
 *                more input data is available, the input does not form a complete block any more, or the output memory
 *                is unable to hold decompressed data of next block.
 * \param[in,out] NodeInfo                                  The processing node information to operate on.
 *                - Output
 *                    - InputPortResults[0]
 *                        VSTREAMPROC_INSUFFICIENT_INPUT    Pending input data does not form a complete data block.
 *                        VSTREAMPROC_INSUFFICIENT_OUTPUT   Output memory is insufficient to decompress next data block.
 * \return        VSTREAMPROC_SIGNALRESPONSE_BUSY           No input data was decompressed, as blocked by
 *                                                          insufficient output.
 * \return        VSTREAMPROC_SIGNALRESPONSE_PENDING        At least one input block was successfully decompressed,
 *                                                          some data is still pending, but further processing is
 *                                                          blocked by insufficient output.
 * \return        VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE    Pending input data does not form a complete data block.
 *                                                          No further signalization until more data is available.
 * \return        VSTREAMPROC_SIGNALRESPONSE_CONFIRM        All pending input data was successfully decompressed.
 *                                                          No further signalization until more data is available.
 * \return        VSTREAMPROC_SIGNALRESPONSE_FAILED         Processing failed.
 * \pre           vStreamProcCmprLz77_InitSlidingWindow() must be called on the same node instance before.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 ***********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_Decompress(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo);

/***********************************************************************************************************************
 *  vStreamProcCmprLz77_Continue
 ***********************************************************************************************************************/
/*!
 * \brief         Instructs vStreamProc to continue calling a previously blocked vStreamProcCmprLz77_Decompress signal.
 * \details       Whenever vStreamProcCmprLz77_Decompress() returns VSTREAMPROC_INSUFFICIENT_OUTPUT, it sets its own
 *                registration state to BLOCKED and registers vStreamProcCmprLz77_Continue() for Storage Available
 *                signalization. As soon as this happens, the registration is switched back to default here.
 * \param[in]     NodeInfo                                The processing node information to operate on.
 * \return        VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Registration states were switched back to default.
 * \pre           Call must be requested by vStreamProcCmprLz77_Decompress().
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 ***********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROCCMPRLZ77_CODE) vStreamProcCmprLz77_Continue(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROCCMPRLZ77_APPL_VSTREAMPROC_DATA) NodeInfo);
# endif /* (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROCCMPRLZ77 == STD_ON) */

# define VSTREAMPROCCMPRLZ77_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

#endif /* VSTREAMPROCCMPRLZ77_H */
/***********************************************************************************************************************
 *  END OF FILE: vStreamProcCmprLz77.h
 **********************************************************************************************************************/
