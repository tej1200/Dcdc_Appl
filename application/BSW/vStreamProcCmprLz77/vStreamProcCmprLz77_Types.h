/***********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProcCmprLz77_Types.h
 *        \brief  vStreamProcCmprLz77 types header file
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProcCmprLz77 module. >> vStreamProcCmprLz77.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 **********************************************************************************************************************/
#if !defined (VSTREAMPROCCMPRLZ77_TYPES_H)
#define VSTREAMPROCCMPRLZ77_TYPES_H

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/
#include "Std_Types.h"
#include "vStreamProcCmprLz77_Cfg.h"

/***********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 **********************************************************************************************************************/
/* Sliding window size */
#define VSTREAMPROCCMPRLZ77_WINDOW_BITS               (8u)
#define VSTREAMPROCCMPRLZ77_WINDOW_MASK               ((vStreamProc_LengthType)VSTREAMPROCCMPRLZ77_BITS_TO_LEN(VSTREAMPROCCMPRLZ77_WINDOW_BITS))
#define VSTREAMPROCCMPRLZ77_WINDOW_SIZE               ((vStreamProc_LengthType)(VSTREAMPROCCMPRLZ77_BITS_TO_LEN(VSTREAMPROCCMPRLZ77_WINDOW_BITS) + 1u))

/***********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 **********************************************************************************************************************/
/* Bit operation macros */
#define VSTREAMPROCCMPRLZ77_BITS_TO_LEN(bits)          ((1uL << (bits)) - 1u)

/***********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

typedef struct
{
  /*! Cyclic buffer for Decompression. */
  uint8  SlidingWindow[VSTREAMPROCCMPRLZ77_WINDOW_SIZE];
  /*! Current (write) position inside the buffer. */
  vStreamProc_LengthType SlidingWindowPos;
  /*! Number of elements written into the buffer. */
  vStreamProc_LengthType SlidingWindowValidRange;
} vStreamProcCmprLz77_WorkspaceType;

/***********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 ***********************************************************************************************************************/

/***********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/

#endif  /* VSTREAMPROCCMPRLZ77_TYPES_H */
/***********************************************************************************************************************
 *  END OF FILE: vStreamProcCmprLz77_Types.h
 **********************************************************************************************************************/
