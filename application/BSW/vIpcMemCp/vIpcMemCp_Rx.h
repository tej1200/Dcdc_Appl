/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_Rx.h
 *  \brief The unit vIpcMemCp_Rx implements the user APIs for the RX communication service. Further, it provides
 *         functions to access the RX channel configuration.
 *
 *  \details
 *  The unit manages the RX communication service of the component.
 *
 *  Responsibility:
 *  During a reception, the vIpcMemCp_Rx manages the buffer index pointers "Tail" and "TailReserved" in order to
 *  ensure correct reception of messages. As "Tail" is also accessed from the TX part, it must always be stored,
 *  and consequently read, in little endian byte order, even if the platform uses big endian as default.
 *
 *  When a reception is initiated, the size of the next message is returned and TailReserved is advanced to the begin
 *  of the data buffer of the message.
 *  When a buffer is requested, TailReserved advances to the end of the requested buffer size. This can be repeated
 *  until the entire buffer of a message was requested.
 *  When a reception is confirmed, the Tail advances to the current position of TailReserved.
 *  When a reception is canceled, Tail and TailReserved both advance to the end of the message, which reception was
 *  aborted. With that, the message is discarded.
 *
 *  Following RX specific errors are detected by the unit:
 *
 *  Error                      | DevErrorCode        | APIs                       | Queue Status
 *  ------------------------------------------------------------------------------------------------------------------
 *  Queue Empty                | No Dev error        | BeginReceive               | Tail == Head
 *  ------------------------------------------------------------------------------------------------------------------
 *  Reception not pending      | RX_NOT_PENDING      | Cancel, Confirm, GetBuffer | Tail == TailReserved
 *  ------------------------------------------------------------------------------------------------------------------
 *  Reception already pending  | RX_ALREADY_PENDING  | BeginReceive               | Tail != TailReserved
 *  ------------------------------------------------------------------------------------------------------------------
 *  Inconsistent size          | INCONSISTENT_SIZE   | Confirm                    | TailReserved < Tail + TotalMsgSize
 *                             |                     | GetBuffer                  | TailReserved > Tail + TotalMsgSize
 *
 *
 *  \trace DSGN-vIpcMemCp22753, DSGN-vIpcMemCp-ChannelIndependence
 *
 *  \unit vIpcMemCp_Rx
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/

#if !defined (VIPCMEMCP_RX_H)
# define VIPCMEMCP_RX_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemCp_CfgIf.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_Init()
 *********************************************************************************************************************/
/*! \brief         Initialize the receive parts of the receiver queues.
 *  \details       -
 *  \pre           The module is uninitialized.
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Init
(
   void
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Rx_BeginReceive()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Rx_BeginReceive.
 *  \details     -
 *
 *  \param[in]   ChannelNr                      See vIpcMemCp_Rx_BeginReceive().
 *  \param[out]  DataSize                       See vIpcMemCp_Rx_BeginReceive().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_INV_PTR               (DevErrorDetect:) Invalid DataSize pointer.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_RX_ALREADY_PENDING    (DevErrorDetect:) A previous reception is still pending.
 *  \return      VIPCMEMCP_E_BUFFEREMPTY_INT       Channel is empty.
 *
 *  \pre         See vIpcMemCp_Rx_BeginReceive().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_BeginReceive(
    vIpcMemCp_ChannelIndexType ChannelNr,
    P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) DataSize
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Rx_GetBuffer()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Rx_GetBuffer.
 *  \details     -
 *
 *  \param[in]   ChannelNr               See vIpcMemCp_Rx_GetBuffer().
 *  \param[in]   DataSize                See vIpcMemCp_Rx_GetBuffer().
 *  \param[out]  BufferDesc              See vIpcMemCp_Rx_GetBuffer().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid DataSize.
 *  \return      VIPCMEMCP_E_INV_PTR               (DevErrorDetect:) Invalid BufferDesc pointer.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_RX_NOT_PENDING        (DevErrorDetect:) No pending reception.
 *  \return      VIPCMEMCP_E_INCONSISTENT_SIZE     (DevErrorDetect:) Requested buffer size exceeds message size.
 *
 *  \pre         See vIpcMemCp_Rx_GetBuffer().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Rx_Confirm()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Rx_Confirm.
 *  \details     -
 *  \param[in]   ChannelNr               See vIpcMemCp_Rx_Confirm().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_RX_NOT_PENDING        (DevErrorDetect:) No pending reception.
 *  \return      VIPCMEMCP_E_INCONSISTENT_SIZE     (DevErrorDetect:) Message was not completely received.
 *
 *  \pre         See vIpcMemCp_Rx_Confirm().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
);

/**********************************************************************************************************************
 * vIpcMemCp_Api_Rx_Cancel()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Rx_Cancel.
 *  \details     -
 *  \param[in]   ChannelNr               See vIpcMemCp_Rx_Cancel().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_RX_NOT_PENDING        (DevErrorDetect:) No pending reception.
 *
 *  \pre         See vIpcMemCp_Rx_Cancel().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
);

# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#endif /* VIPCMEMCP_RXINT_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_RxInt.h
 *********************************************************************************************************************/
