/***********************************************************************************************************************
 *  COPYRIGHT
 *  --------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  --------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  ------------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_CfgIf.h
 *  \brief  The unit vIpcMemCp_CfgIf provides utility functions to easily access the the underlying configuration.
 *  \details
 *  vIpcMemCp_CfgIf provides functions for abstraction of the configuration.
 *
 *  The unit provides abstraction functions for the following configuration switches
 *    - VIPCMEMCP_DEV_ERROR_DETECT
 *    - VIPCMEMCP_DEV_ERROR_REPORT
 *    - VIPCMEMCP_BYTEORDER_LITTLE_ENDIAN
 *    - VIPCMEMCP_USE_INIT_POINTER
 *    - VIPCMEMCP_USE_ECUM_BSW_ERROR_HOOK
 *
 *  Those functions should be used to easify decisions based on those switches.
 *
 *  \unit vIpcMemCp_CfgIf
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 **********************************************************************************************************************/

#if !defined (VIPCMEMCP_CFGIF_H)
# define VIPCMEMCP_CFGIF_H

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/
# include "vIpcMemCp_Types.h"
# include "vIpcMemCp_Cfg.h"
# include "vIpcMemCp_PBcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
# ifndef VIPCMEMCP_LOCAL                                                                                                /* COV_VIPCMEMCP_COMPILERKEYWORD */
#  define VIPCMEMCP_LOCAL static
# endif

# ifndef VIPCMEMCP_LOCAL_INLINE                                                                                         /* COV_VIPCMEMCP_COMPILERKEYWORD */
#  define VIPCMEMCP_LOCAL_INLINE LOCAL_INLINE
# endif

/*! Defines the alignment of pointers as required by the platform.
 *  As only 3 bytes may get lost, this value is currently fixed to 4 as needed by some platforms.
 */
# define VIPCMEMCP_PTR_ALIGNMENT (4u)

/**********************************************************************************************************************
 *  GLOBAL VIPCMEMCP_LOCAL_INLINE FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/
# define VIPCMEMCP_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern P2CONST(vIpcMemCp_ConfigType, VIPCMEMCP_VAR_ZERO_INIT, VIPCMEMCP_PBCFG) vIpcMemCp_ConfigDataPtr;

# define VIPCMEMCP_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL VIPCMEMCP_LOCAL_INLINE FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_DevErrorDetect()
 *********************************************************************************************************************/
/*! \brief       Returns whether DevErrorDetect is enabled or not.
 *  \details     -
 *  \return      TRUE if DevErrorDetect is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_DevErrorDetect
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_DevErrorReport()
 *********************************************************************************************************************/
/*! \brief       Returns whether DevErrorReport is enabled or not.
 *  \details     -
 *  \return      TRUE if DevErrorReport is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_DevErrorReport
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_IsByteOrderLittleEndian()
 *********************************************************************************************************************/
/*! \brief       Returns whether little endian byte order is enabled or not.
 *  \details     -
 *  \return      TRUE if ByteOrderLittleEndian is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_IsByteOrderLittleEndian
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_UseInitPointer()
 *********************************************************************************************************************/
/*! \brief       Returns whether the usage of init pointer is enabled.
 *  \details     -
 *  \return      TRUE if UseInitPointer is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_UseInitPointer
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemCp_UseEcumBswErrorHook()
 *********************************************************************************************************************/
/*! \brief       Returns whether the usage of EcuM Bsw ErrorHook is enabled.
 *  \details     -
 *  \return      TRUE if UseEcumBswErrorHook is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_UseEcumBswErrorHook
(
   void
);


# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL VIPCMEMCP_LOCAL_INLINE FUNCTIONS
 *********************************************************************************************************************/
# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_DevErrorDetect()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)                                                                    /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_CfgIf_DevErrorDetect
(
   void
)
{
   return (boolean)(VIPCMEMCP_DEV_ERROR_DETECT == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CfgIf_DevErrorDetect */


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_DevErrorReport()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_DevErrorReport                                                                                          /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   void
)
{
   return (boolean)(VIPCMEMCP_DEV_ERROR_REPORT == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CfgIf_DevErrorReport */


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_IsByteOrderLittleEndian()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_IsByteOrderLittleEndian                                                                                 /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   void
)
{
   return (boolean)(VIPCMEMCP_BYTEORDER_LITTLE_ENDIAN == STD_ON);                                                       /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CfgIf_IsByteOrderLittleEndian */


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_UseInitPointer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_UseInitPointer                                                                                          /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   void
)
{
   return (boolean)(VIPCMEMCP_USE_INIT_POINTER == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CfgIf_UseInitPointer */


/**********************************************************************************************************************
 *  vIpcMemCp_CfgIf_UseEcumBswErrorHook()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CfgIf_UseEcumBswErrorHook                                                                                     /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   void
)
{
   return (boolean)(VIPCMEMCP_USE_ECUM_BSW_ERROR_HOOK == STD_ON);                                                       /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CfgIf_UseEcumBswErrorHook */                                                                             /* COV_VIPCMEMCP_UNSUPPORTED98671 */

# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


#endif /* VIPCMEMCP_CFGIF_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_CfgIf.h
 *********************************************************************************************************************/
