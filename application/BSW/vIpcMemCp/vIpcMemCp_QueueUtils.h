/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_QueueUtils.h
 *  \brief The unit vIpcMemCp_QueueUtils provides utility functions for an easier usage of the underlying queues inside
 *         the RX and TX modules.
 *
 *  \details  -
 *
 *  \unit vIpcMemCp_QueueUtils
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/
#if !defined (VIPCMEMCP_QUEUEUTILS_H)
# define VIPCMEMCP_QUEUEUTILS_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemCp_CfgIf.h"

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_ReadQueuePointer()
 *********************************************************************************************************************/
/*! \brief         Returns the value of a queue position.
 *  \details       -
 *  \param[in]     QueuePosPtr             Pointer to the queue position. Parameter must not be NULL_PTR.
 *  \return        Queue position value.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_ReadQueuePointer
(
   P2CONST(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_CONST) QueuePosPtr
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_WriteQueuePointer()
 *********************************************************************************************************************/
/*! \brief         Stores a new value at a queue position.
 *  \details       -
 *  \param[in]     QueuePosPtr             Pointer to the queue position. Parameter must not be NULL_PTR.
 *  \param[in]     Value                   The new value.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_WriteQueuePointer
(
   P2VAR(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) QueuePosPtr,
   vIpcMemCp_QueuePosType Value
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_ReadMessageHeader()
 *********************************************************************************************************************/
/*! \brief         Retrieves the message header from the buffer.
 *  \details       -
 *  \param[in]     BufferMsgPtr            Pointer to the message header in the buffer. Parameter must not be NULL_PTR.
 *  \param[out]    MsgPtr                  Pointer the messager header to store into. Parameter must not be NULL_PTR.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_ReadMessageHeader
(
   P2CONST(uint8, AUTOMATIC, VIPCMEMCP_CONST) BufferMsgPtr,
   P2VAR(vIpcMemCp_MessageHeaderType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) MsgPtr
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_WriteMessageHeader()
 *********************************************************************************************************************/
/*! \brief         Writes the message header into the buffer.
 *  \details       -
 *  \param[in]     BufferMsgPtr            Pointer to the message header in the buffer. Parameter must not be NULL_PTR.
 *  \param[in]     MsgPtr                  Pointer the messager header to get data from. Parameter must not be NULL_PTR.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_WriteMessageHeader
(
   P2VAR(uint8, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferMsgPtr,
   P2CONST(vIpcMemCp_MessageHeaderType, AUTOMATIC, VIPCMEMCP_CONST) MsgPtr
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_QueueIsEmpty()
 *********************************************************************************************************************/
/*! \brief         Returns if the queue contains no messages.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \return        TRUE if the queue is empty, FALSE otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueIsEmpty
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_QueueGetDistance()
 *********************************************************************************************************************/
/*! \brief         Returns the distance of two positions with a optional wrap-around.
 *  \details       -
 *  \param[in]     FrontPos                The position to be considered in front.
 *  \param[in]     RearPos                 The position to be consisered in rear.
 *  \param[in]     BufferSize              The wrap-around position.
 *  \return        The distance in bytes.
 *  \pre           FrontPos and RearPos must be smaller than BufferSize.
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueGetDistance
(
   vIpcMemCp_QueuePosType FrontPos,
   vIpcMemCp_QueuePosType RearPos,
   uint32 BufferSize
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_QueueLeft()
 *********************************************************************************************************************/
/*! \brief         Returns the available free space size in the queue.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \param[in]     BufferSize              Size of the underlying buffer.
 *  \return        Free space size in bytes.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueLeft
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
   vIpcMemCp_BufferSizeType BufferSize
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_QueueGetCorrectedPos()
 *********************************************************************************************************************/
/*! \brief         Returns the value of a queue position after an optional wrap-around correction.
 *  \details       -
 *  \param[in]     QueuePosPtr             Pointer to the queue position. Parameter must not be NULL_PTR.
 *  \param[in]     BufferSize              Size of the underlying buffer.
 *  \return        Corrected queue position value.
 *  \pre           Value of *QueuePosPtr must be smaller than BufferSize.
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueGetCorrectedPos
(
   P2CONST(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_CONST) QueuePosPtr,
   vIpcMemCp_BufferSizeType BufferSize
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_Align()
 *********************************************************************************************************************/
/*! \brief         Aligns the given queue position to 4 bytes.
 *  \details       -
 *  \param[in]     unaligned               Queue position to be aligned.
 *  \return        Aligned queue position value.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_Align
(
   vIpcMemCp_QueuePosType unaligned
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_FormatInLittleEndian()
 *********************************************************************************************************************/
/*! \brief         Formats the intput value in little endian.
 *  \details       -
 *  \param[in]     Value                 Value to be formatted.
 *  \return        Value in little endian format.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_FormatInLittleEndian
(
   uint32 Value
);


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_ReadQueuePointer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)                                                     /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_QueueUtils_ReadQueuePointer
(
   P2CONST(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_CONST) QueuePosPtr
)
{
   return vIpcMemCp_QueueUtils_FormatInLittleEndian(*QueuePosPtr);                                                      /* PRQA S 2811 */ /* MD_vIpcMemCp_EmptyConfig */
} /* vIpcMemCp_QueueUtils_ReadQueuePointer */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_WriteQueuePointer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)                                                                       /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_QueueUtils_WriteQueuePointer
(
   P2VAR(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) QueuePosPtr,
   vIpcMemCp_QueuePosType Value
)
{
   *QueuePosPtr = vIpcMemCp_QueueUtils_FormatInLittleEndian(Value);
} /* vIpcMemCp_QueueUtils_WriteQueuePointer */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_ReadMessageHeader()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)                                                                       /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_QueueUtils_ReadMessageHeader
(
   P2CONST(uint8, AUTOMATIC, VIPCMEMCP_CONST) BufferMsgPtr,
   P2VAR(vIpcMemCp_MessageHeaderType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) MsgPtr
)
{
   P2CONST(uint32, AUTOMATIC, VIPCMEMCP_CONST) ptr = (P2CONST(uint32, AUTOMATIC, VIPCMEMCP_CONST))BufferMsgPtr;         /* PRQA S 0310, 3305 */ /* MD_vIpcMemCp_0310, MD_vIpcMemCp_3305 */

   MsgPtr->Size = vIpcMemCp_QueueUtils_FormatInLittleEndian(*ptr);
} /* vIpcMemCp_QueueUtils_ReadMessageHeader */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_WriteMessageHeader()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_WriteMessageHeader                                                                                 /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   P2VAR(uint8, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferMsgPtr,
   P2CONST(vIpcMemCp_MessageHeaderType, AUTOMATIC, VIPCMEMCP_CONST) MsgPtr
)
{
   P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) ptr = (P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT))BufferMsgPtr;   /* PRQA S 0310, 3305 */ /* MD_vIpcMemCp_0310, MD_vIpcMemCp_3305 */

   *ptr = vIpcMemCp_QueueUtils_FormatInLittleEndian(MsgPtr->Size);                                                      /* VCA_VIPCMEMCP_MessageHeader */
} /* vIpcMemCp_QueueUtils_WriteMessageHeader */


/**********************************************************************************************************************
 *  vIpcMemCp_QueueUtils_QueueIsEmpty()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueIsEmpty                                                                                       /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
)
{
   return (boolean)(Queue->Head == Queue->Tail);                                                                        /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_QueueUtils_QueueIsEmpty */


/**********************************************************************************************************************
 *  vIpcMemCp_QueueUtils_QueueGetDistance()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)                                                                     /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_QueueUtils_QueueGetDistance
(
   vIpcMemCp_QueuePosType FrontPos,
   vIpcMemCp_QueuePosType RearPos,
   uint32 BufferSize
)
{
   uint32 result;

   /* #10 If the front position is after the rear position, calcualte the free space between them. */
   if(FrontPos > RearPos)
   {
      result = (uint32)(FrontPos - RearPos);
   }
   /* #20 Otherwise: */
   else
   {
      /* #30 Calculate the free space between rear position and buffer's end. */
      result = (uint32)(BufferSize - RearPos);

      /* #40 Set it to zero if it is too small to fit minimal data size and the header. */
      if (result < (VIPCMEMCP_MIN_DATA_COPY_SIZE + sizeof(vIpcMemCp_MessageHeaderType)))
      {
         result = 0;
      }

      /* #50 Calculate the free space between front position and buffer's begin. */
      result += (uint32)(FrontPos);
   }
   return result;
}

/**********************************************************************************************************************
 *  vIpcMemCp_QueueUtils_QueueLeft()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_QueueLeft                                                                                          /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
   vIpcMemCp_BufferSizeType BufferSize
)
{
   uint32 result;

   vIpcMemCp_QueuePosType curHeadPos;
   vIpcMemCp_QueuePosType curTailPos;

   /* #10 Retrieve the positions from the queue management structure. */
   curHeadPos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Head));
   curTailPos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Tail));

   /* #20 Get the distance between tail and head. */
   result = vIpcMemCp_QueueUtils_QueueGetDistance(curTailPos, curHeadPos, BufferSize);

   /* #30 Subtract one byte from the result if it is greater than zero. */
   if (result > 0u)
   {
      result -= 1u;
   }

   return result;
} /* vIpcMemCp_QueueUtils_QueueLeft */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_QueueGetCorrectedPos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)                                                     /* PRQA S 3219 */ /* MD_vIpcMemCp_3219 */
vIpcMemCp_QueueUtils_QueueGetCorrectedPos
(
   P2CONST(vIpcMemCp_QueuePosType, AUTOMATIC, VIPCMEMCP_CONST) QueuePosPtr,
   vIpcMemCp_BufferSizeType BufferSize
)
{
   vIpcMemCp_QueuePosType result;

   /* #10 Read the position. */
   result = vIpcMemCp_QueueUtils_ReadQueuePointer(QueuePosPtr);

   /* The returned value needs to be checked for validity, as the management information can get corrupted
      by non ASIL software parts. */

   /* #15 Wrap-around if the result is out of bounds of the buffer or
          if the next header and minimal data size does not fit until end of the buffer. */
   if ((result > BufferSize) ||
       ((BufferSize - result) < (VIPCMEMCP_MIN_DATA_COPY_SIZE + sizeof(vIpcMemCp_MessageHeaderType))))
   {
      result = 0;
   }

   return result;
} /* vIpcMemCp_QueueUtils_QueueGetCorrectedPos */


/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_Align()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_Align
(
   vIpcMemCp_QueuePosType unaligned
)
{
   return unaligned + ((VIPCMEMCP_PTR_ALIGNMENT - (unaligned % VIPCMEMCP_PTR_ALIGNMENT)) % VIPCMEMCP_PTR_ALIGNMENT);
} /* vIpcMemCp_QueueUtils_Align */

/**********************************************************************************************************************
 * vIpcMemCp_QueueUtils_FormatInLittleEndian()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePosType, VIPCMEMCP_CODE)
vIpcMemCp_QueueUtils_FormatInLittleEndian
(
   uint32 Value
)
{
   uint32 result = Value;                                                                                               /* PRQA S 2981 */ /* MD_vIpcMemCp_2981 */

   /* #10 If byte order is not formatted in little endian: */
   if (vIpcMemCp_CfgIf_IsByteOrderLittleEndian() == FALSE){                                                             /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */

      /* #20 Invert endianess. */
      result =  (Value >> 24);
      result |= (Value >> 8) & 0x0000FF00u;
      result |= (Value << 8) & 0x00FF0000u;
      result |= (Value << 24);
   }

   return result;
} /* vIpcMemCp_QueueUtils_FormatInLittleEndian */


# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMCP_QUEUEUTILS_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_QueueUtils.h
 *********************************************************************************************************************/
