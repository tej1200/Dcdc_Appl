/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file vIpcMemCp_Types.h
 *  \brief Contains the type definitions of the module.
 *
 *  \details -
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/

#if !defined (VIPCMEMCP_TYPES_H)
# define VIPCMEMCP_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
# define VIPCMEMCP_SID_INIT                (vIpcMemCp_ServiceIdType)(0u)    /*!< Service ID: vIpcMemCp_Init() */
# define VIPCMEMCP_SID_INIT_MEMORY         (vIpcMemCp_ServiceIdType)(1u)    /*!< Service ID: vIpcMemCp_InitMemory() */
# define VIPCMEMCP_SID_GET_VERSION_INFO    (vIpcMemCp_ServiceIdType)(2u)    /*!< Service ID: vIpcMemCp_GetVersionInfo() */
# define VIPCMEMCP_SID_TX_BEGINTRANSMIT    (vIpcMemCp_ServiceIdType)(3u)    /*!< Service ID: vIpcMemCp_Tx_BeginTransmit() */
# define VIPCMEMCP_SID_TX_GETBUFFER        (vIpcMemCp_ServiceIdType)(4u)    /*!< Service ID: vIpcMemCp_Tx_GetBuffer() */
# define VIPCMEMCP_SID_TX_CONFIRM          (vIpcMemCp_ServiceIdType)(5u)    /*!< Service ID: vIpcMemCp_Tx_Confirm() */
# define VIPCMEMCP_SID_TX_CANCEL           (vIpcMemCp_ServiceIdType)(6u)    /*!< Service ID: vIpcMemCp_Tx_Cancel() */
# define VIPCMEMCP_SID_RX_BEGINRECEIVE     (vIpcMemCp_ServiceIdType)(7u)    /*!< Service ID: vIpcMemCp_Rx_BeginReceive() */
# define VIPCMEMCP_SID_RX_GETBUFFER        (vIpcMemCp_ServiceIdType)(8u)    /*!< Service ID: vIpcMemCp_Rx_GetBuffer() */
# define VIPCMEMCP_SID_RX_CONFIRM          (vIpcMemCp_ServiceIdType)(9u)    /*!< Service ID: vIpcMemCp_Rx_Confirm() */
# define VIPCMEMCP_SID_RX_CANCEL           (vIpcMemCp_ServiceIdType)(10u)   /*!< Service ID: vIpcMemCp_Rx_Cancel() */

# define VIPCMEMCP_E_NO_ERROR              (vIpcMemCp_ErrorCodeType)(0xFFu) /*!< No error. */
# define VIPCMEMCP_E_INV_PTR               (vIpcMemCp_ErrorCodeType)(1u)    /*!< Error code: Invalid pointer. */
# define VIPCMEMCP_E_PARAM                 (vIpcMemCp_ErrorCodeType)(2u)    /*!< Error code: Invalid parameter. */
# define VIPCMEMCP_E_UNINIT                (vIpcMemCp_ErrorCodeType)(3u)    /*!< Error code: API service used without module initialization. */
# define VIPCMEMCP_E_INCONSISTENT_SIZE     (vIpcMemCp_ErrorCodeType)(4u)    /*!< Error code: Size is inconsistent with previously requested. */
# define VIPCMEMCP_E_TX_ALREADY_PENDING    (vIpcMemCp_ErrorCodeType)(5u)    /*!< Error code: Transmit is not possible due to another one still pending. */
# define VIPCMEMCP_E_TX_NOT_PENDING        (vIpcMemCp_ErrorCodeType)(6u)    /*!< Error code: API service is used before transmit was started. */
# define VIPCMEMCP_E_RX_ALREADY_PENDING    (vIpcMemCp_ErrorCodeType)(7u)    /*!< Error code: Receive is not possible due to another one still pending. */
# define VIPCMEMCP_E_RX_NOT_PENDING        (vIpcMemCp_ErrorCodeType)(8u)    /*!< Error code: API service is used before receive was started. */

# define VIPCMEMCP_E_BUFFEREMPTY_INT       (vIpcMemCp_ErrorCodeType)(16u)   /*!< Internal error code: Buffer is empty. */
# define VIPCMEMCP_E_BUFFERFULL_INT        (vIpcMemCp_ErrorCodeType)(17u)   /*!< Internal error code: Buffer is full. */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/*! Type for service IDs. */
typedef uint8 vIpcMemCp_ServiceIdType;


/*! Type for the error codes. */
typedef uint8 vIpcMemCp_ErrorCodeType;


/*! Type to address a channel. */
typedef uint16 vIpcMemCp_ChannelIndexType;


/*! Type to store a position in a queue. */
typedef uint32 vIpcMemCp_QueuePosType;


/*! Type to store buffer size. */
typedef uint32 vIpcMemCp_BufferSizeType;


/*! Type to use for buffer location. */
typedef P2VAR(uint8, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) vIpcMemCp_BufferPtrType;


/*! Data structure for the management of a single queue. */
typedef struct
{
   /*! Offset to the free space for the next message. The value is used from both sides. */
   vIpcMemCp_QueuePosType Head;

   /*! Offset to the next free data block during current transmission. The value is only used by the sender. */
   vIpcMemCp_QueuePosType HeadReserved;

   /*! Offset to the next receivable message. The value is used from both sides. */
   vIpcMemCp_QueuePosType Tail;

   /*! Offset to the next receivable data block during current transmission. The value is only used by the receiver. */
   vIpcMemCp_QueuePosType TailReserved;

   /*! Flag to mark if the sender part is initialized. */
   boolean HeadInitialized;

   /*! Flag mark if the receiver part is initialized. */
   boolean TailInitialized;
}vIpcMemCp_QueueType;


/*! Pointer to the queue management structure. */
typedef P2VAR(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) vIpcMemCp_QueuePtrType;


/*! Read-only pointer to the queue management structure. */
typedef P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) vIpcMemCp_QueuePtrConstType;


/*! Header preceding each message in the queue. */
typedef struct
{
   /*! Size of the message in bytes. */
   uint32 Size;
}vIpcMemCp_MessageHeaderType;


/*! Contains the description of a single continuous data block. */
typedef struct
{
   /*! Pointer to the begin of the data block. */
   P2VAR(uint8, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) Ptr;

   /*! Data block size. */
   uint32 Size;
}vIpcMemCp_BufferDescriptionType;


#endif /* VIPCMEMCP_TYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_Types.h
 *********************************************************************************************************************/
