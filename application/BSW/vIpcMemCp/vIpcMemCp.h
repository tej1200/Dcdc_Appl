/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp.h
 *  \brief External interface of the module vIpcMemCp.
 *
 *  \details  Contains the declaration of the module's API functions.
 *
 *  \unit vIpcMemCp_Base
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2017-05-18  virbiv  -             Initial version.
 *  01.01.00  2017-08-04  virbiv  -             Tx_BeginTransmit, Rx_BeginReceive: return value condition reworked.
 *                                              Tx_Confirm, Rx_Confirm: DataSize parameter removed.
 *  01.01.01  2017-11-08  virbse  ESCAN00097083 Compiler error: 'Det_ReportError' undefined.
 *  01.02.00  2020-07-02  visdqk  -             Redesigned internal modules structure.
 *            2020-07-10  virleh  OSC-188       Updated to MISRA 2012.
 *  01.03.00  2020-09-25  visdqk  -             Minor internal improvements.
 *  01.04.00  2021-06-14  virbse  OSC-6767      Fix MISRA warning introduced by OSC-6692.
              2021-06-15  visdqk  OSC-6930      Reworked initialization checks.
              2021-07-20  visdqk  OSC-5037      Updates for SafeBSW (VCA, Coverage and CDD).
              2021-07-20  visdqk  OSC-7679      Updates for MISRA.
 *  01.05.00  2021-09-08  virsmn  OSC-7868      Updates for SafeBSW.
 *********************************************************************************************************************/
#if !defined (VIPCMEMCP_H)
# define VIPCMEMCP_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemCp_Types.h"
# include "vIpcMemCp_Cfg.h"
# include "vIpcMemCp_PBcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Vendor and module identification */
# define VIPCMEMCP_VENDOR_ID   (30u)
# define VIPCMEMCP_MODULE_ID   (255u)
# define VIPCMEMCP_INSTANCE_ID (0u)

/* AUTOSAR Software specification version information */
# define VIPCMEMCP_AR_RELEASE_MAJOR_VERSION            (4u)
# define VIPCMEMCP_AR_RELEASE_MINOR_VERSION            (3u)
# define VIPCMEMCP_AR_RELEASE_REVISION_VERSION         (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VIPCMEMCP_SW_MAJOR_VERSION                    (1u)
# define VIPCMEMCP_SW_MINOR_VERSION                    (5u)
# define VIPCMEMCP_SW_PATCH_VERSION                    (0u)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */



/**********************************************************************************************************************
 *  vIpcMemCp_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Function for *_INIT_*-variable initialization
 *  \details     Service to initialize module global variables at power up. This function initializes the
 *               variables in *_INIT_* sections. Used in case they are not initialized by the startup code.
 *  \pre         Module is uninitialized.
 *  \context     ANY
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-129713
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_InitMemory(void);


/**********************************************************************************************************************
 * vIpcMemCp_Init()
 *********************************************************************************************************************/
/*! \brief       Initialization function
 *  \details     This function initializes the module vIpcMemCp. It initializes all intern variables.
 *  \param[in]   ConfigPtr                 Pointer to the PostBuildLoadable configuration or NULL_PTR.
 *  \pre         Module is uninitialized.
 *  \context     ANY
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-129713
 *  \trace       DSGN-vIpcMemCp22763
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_Init(P2CONST(vIpcMemCp_ConfigType, AUTOMATIC, VIPCMEMCP_INIT_DATA) ConfigPtr);


/**********************************************************************************************************************
 *  vIpcMemCp_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief       Returns the version information
 *  \details     This function returns version information, vendor ID and AUTOSAR module ID of the component.
 *  \param[out]  VersionInfo             Pointer to where to store the version information. Parameter must not be NULL.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \config      VIPCMEMCP_VERSION_INFO_API
 *  \synchronous TRUE
 *  \trace       CREQ-129973
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) VersionInfo);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_BeginTransmit()
 *********************************************************************************************************************/
/*! \brief       Function to initiate a transmission.
 *  \details     This function checks if sufficient free memory is available and prepares the transmission.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \param[in]   DataSize                Length of the user's data in bytes.
 *  \return      E_OK                    Transmission request accepted.
 *  \return      E_NOT_OK                Transmission request failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-129714
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_BeginTransmit
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBuffer()
 *********************************************************************************************************************/
/*! \brief       Function to retrieve a buffer description of a single continuous data block.
 *  \details     The resulted buffer may be smaller than the requested data size. In that case, another call of the
 *               same function is expected.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \param[in]   DataSize                Length of the user's data in bytes.
 *  \param[out]  BufferDesc              Buffer description to be used for data copy. Parameter must not be NULL.
 *  \return      E_OK                    Buffer request was successful.
 *  \return      E_NOT_OK                Buffer request has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-133054
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Confirm()
 *********************************************************************************************************************/
/*! \brief       Function to mark the active transmission as finished.
 *  \details     After the call, the message can be read by the receiver.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \return      E_OK                    Confirmation was successful.
 *  \return      E_NOT_OK                Confirmation has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-133055
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Cancel()
 *********************************************************************************************************************/
/*! \brief       Function to cancel the current transmission.
 *  \details     Previously copied data is discarded.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \return      E_OK                    Cancel was successful.
 *  \return      E_NOT_OK                Cancel has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-133056
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
);

/**********************************************************************************************************************
 * vIpcMemCp_Rx_BeginReceive()
 *********************************************************************************************************************/
/*! \brief       Function to initiate a reception.
 *  \details     This functions checks if messages are available for reception.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \param[out]  DataSize                Length of the user's data in bytes. Parameter must not be NULL.
 *  \return      E_OK                    Reception request accepted.
 *  \return      E_NOT_OK                Reception request failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-129715
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_BeginReceive
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) DataSize
);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBuffer()
 *********************************************************************************************************************/
/*! \brief       Function to retrieve a buffer description of a single continuous data block.
 *  \details     The resulted buffer may be smaller than the requested data size. In that case, another call of the
 *               same function is expected.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \param[in]   DataSize                Length of the user's data in bytes.
 *  \param[out]  BufferDesc              Buffer description to be used for data copy. Parameter must not be NULL.
 *  \return      E_OK                    Buffer request was successful.
 *  \return      E_NOT_OK                Buffer request has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-129716
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_Confirm()
 *********************************************************************************************************************/
/*! \brief       Function to mark the active reception as finished.
 *  \details     After the call, the message space is freed and can be used to transmit more data.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \return      E_OK                    Confirmation was successful.
 *  \return      E_NOT_OK                Confirmation has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-133057
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_Cancel()
 *********************************************************************************************************************/
/*! \brief       Function to cancel the current reception.
 *  \details     The unreceived message is deleted from the channel.
 *  \param[in]   ChannelNr               The channel index to identify the used channel.
 *  \return      E_OK                    Cancel was successful.
 *  \return      E_NOT_OK                Cancel has failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *  \trace       CREQ-133058
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
);


# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMCP_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp.h
 *********************************************************************************************************************/
