/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_Tx.h
 *  \brief The unit vIpcMemCp_Tx implements the user APIs for the TX communication service. Further, it provides
 *         functions to access the TX channel configuration.
 *
 *  \details
 *  The unit manages the TX communication service of the component.

 *  Responsibility:
 *  During a transmission, vIpcMemCp_Tx manages the buffer index pointers "Head" and "HeadReserved" in order to
 *  ensure correct transmission of messages. As "Head" is also read from the RX part, it must always be stored, and
 *  consequently read, in little endian byte order, even if the platform uses big endian as default.
 *
 *  When a transmission is initiated, the size of the next message is stored at the current Head position and
 *  HeadReserved is advanced for the size of the message header (4 bytes).
 *  When a buffer is requested, HeadReserved advances to the end of the requested buffer size. This can be repeated
 *  until the entire buffer for a message was requested.
 *  When a transmission is confirmed, Head advances to the current position of HeadReserved.
 *  When a transmission is canceled, Head and HeadReserved are both rolled back to their position when the
 *  current transmission was initiated. With that, the message is discarded.
 *
 *  Following TX specific errors are detected by the unit:
 *
 *  Error                        | DevErrorCode        | APIs                       | Queue Status
 *  ------------------------------------------------------------------------------------------------------------------
 *  Queue Full                   | No Dev error        | BeginTransmit              | Tail > Head + TotalMsgSize
 *  ------------------------------------------------------------------------------------------------------------------
 *  Transmission not pending     | TX_NOT_PENDING      | Cancel, Confirm, GetBuffer | Head == HeadReserved
 *  ------------------------------------------------------------------------------------------------------------------
 *  Transmission already pending | TX_ALREADY_PENDING  | BeginTransmit              | Head != HeadReserved
 *  ------------------------------------------------------------------------------------------------------------------
 *  Inconsistent size            | INCONSISTENT_SIZE   | Confirm                    | HeadReserved < Head + TotalMsgSize
 *                               |                     | GetBuffer                  | HeadReserved > Head + TotalMsgSize
 *
 *  \trace DSGN-vIpcMemCp22733, DSGN-vIpcMemCp-ChannelIndependence
 *
 *  \unit vIpcMemCp_Tx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/

#if !defined (VIPCMEMCP_TX_H)
# define VIPCMEMCP_TX_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpcMemCp_CfgIf.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Init()
 *********************************************************************************************************************/
/*! \brief         Initialize the send parts of the sender queues.
 *  \details       -
 *  \pre           The module is uninitialized.
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Init
(
   void
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Tx_BeginTransmit()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Tx_BeginTransmit.
 *  \details     -
 *
 *  \param[in]   ChannelNr                      See vIpcMemCp_Tx_BeginTransmit().
 *  \param[out]  DataSize                       See vIpcMemCp_Tx_BeginTransmit().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_INV_PTR               (DevErrorDetect:) Invalid DataSize pointer.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initalized.
 *  \return      VIPCMEMCP_E_TX_ALREADY_PENDING    (DevErrorDetect:) A previous transmission is still pending.
 *  \return      VIPCMEMCP_E_BUFFERFULL_INT        Channel is full.
 *
 *  \pre         See vIpcMemCp_Tx_BeginTransmit().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_BeginTransmit(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Tx_GetBuffer()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Tx_GetBuffer.
 *  \details     -
 *
 *  \param[in]   ChannelNr               See vIpcMemCp_Tx_GetBuffer().
 *  \param[in]   DataSize                See vIpcMemCp_Tx_GetBuffer().
 *  \param[out]  BufferDesc              See vIpcMemCp_Tx_GetBuffer().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid DataSize.
 *  \return      VIPCMEMCP_E_INV_PTR               (DevErrorDetect:) Invalid BufferDesc pointer.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_TX_NOT_PENDING        (DevErrorDetect:) No pending transmission.
 *  \return      VIPCMEMCP_E_INCONSISTENT_SIZE     (DevErrorDetect:) Requested buffer size exceeds message size.
 *
 *  \pre         See vIpcMemCp_Tx_GetBuffer().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Tx_Confirm()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Tx_Confirm.
 *  \details     -
 *  \param[in]   ChannelNr               See vIpcMemCp_Tx_Confirm().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_TX_NOT_PENDING        (DevErrorDetect:) No pending transmission.
 *  \return      VIPCMEMCP_E_INCONSISTENT_SIZE     (DevErrorDetect:) Message was not completely sent.
 *
 *  \pre         See vIpcMemCp_Tx_Confirm().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
);


/**********************************************************************************************************************
 * vIpcMemCp_Api_Tx_Cancel()
 *********************************************************************************************************************/
/*! \brief       vIpcMemCp service Tx_Cancel.
 *  \details     -
 *  \param[in]   ChannelNr               See vIpcMemCp_Tx_Cancel().
 *
 *  \return      VIPCMEMCP_E_NO_ERROR              No error.
 *  \return      VIPCMEMCP_E_PARAM                 (DevErrorDetect:) Invalid channel number.
 *  \return      VIPCMEMCP_E_UNINIT                (DevErrorDetect:) Channel is not initialized.
 *  \return      VIPCMEMCP_E_TX_NOT_PENDING        (DevErrorDetect:) No pending transmission.
 *
 *  \pre         See vIpcMemCp_Tx_Cancel().
 *
 *  \context     ANY
 *  \reentrant   TRUE for different channels
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
);


# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMCP_TX_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_Tx.h
 *********************************************************************************************************************/
