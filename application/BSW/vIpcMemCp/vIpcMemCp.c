/*********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp.c
 *  \brief Implementation of the ServiceFunctions of the component.
 *
 *  \details
 *  The module vIpcMemCp provides a queued communication via shared memory. Each communication channel is unidirectional
 *  and consists of a RX and a TX part. The queued communication is realised using ring buffers.
 *
 *  Communication details
 *
 *  Each ring buffer consists of status data and storage space to store the messages.
 *  The status data is used to manage ongoing transmissions on the buffer and monitor the buffer state. The status data
 *  is stored in the first 18-20 bytes of the buffer. Actual size depends on the alignment of getsize() on the platform.
 *  During transmission and reception, the pointers of the status data are advanced respectively.
 *  See units vIpcMemCp_Rx and vIpcMemCp_Tx for details.
 *
 *
 *  Wrapper API functions
 *
 *  All API functions are implemented in the unit vIpcMemCp, but the functions implemented here only serve as wrapper
 *  functions to the actual implementation. Their job is to call the internal API function and forward the returned error
 *  code to the error handling. By that, the error handling can be encapsuled in this unit.
 *
 *
 *  Error Reporting
 *
 *  Each internal API method returns an vIpcMemCp error code. There are two types of error codes, internal errors and
 *  development errors. Internal errors occure due to runtime issues, e.g. when the buffer is full or empty. They are no
 *  real errors but are treated as errors internally to easify the call pattern of the internal APIs.
 *
 *  The error codes follow a dedicated pattern to distinguish between development errors and internal errors during error
 *  handling. The distinguishment is as follows:
 *
 *  An error code always consists of 8 bit. The lower 4 bits are only set for development errors, the higher 4 bit only
 *  for internal errors. The error code for NO_ERROR is 0xFF. During error handling, it is first checked if an error
 *  occured (compare to VIPCMEMCP_E_NO_ERROR). Afterwards it is checked whether the error code is a DET error or not.
 *  If it is a DET error, it is forwarded to the Det module.
 *
 *  \unit vIpcMemCp_Base
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemCp.h"
# include "vIpcMemCp_Rx.h"
# include "vIpcMemCp_Tx.h"
# include "vIpcMemCp_CfgIf.h"


/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

/* Check the version of vIpcMemCp header file */
#if (  (VIPCMEMCP_SW_MAJOR_VERSION != (1u)) \
    || (VIPCMEMCP_SW_MINOR_VERSION != (5u)) \
    || (VIPCMEMCP_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of vIpcMemCp.c and vIpcMemCp.h are inconsistent"
#endif

/* Check the version of the configuration header file */
#if (  (VIPCMEMCP_CFG_MAJOR_VERSION != (1u)) \
    || (VIPCMEMCP_CFG_MINOR_VERSION != (4u)) )
# error "Version numbers of vIpcMemCp.c and vIpcMemCp_Cfg.h are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
/* Size of the DET error codes */
# define VIPCMEMCP_DET_ERROR_BITS    4u

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
# if (VIPCMEMCP_DEV_ERROR_REPORT == STD_ON)
#  include "Det.h"
#  define vIpcMemCp_DetReportError(a, b, c, d) ((void) Det_ReportError(a, b, c, d))
# else
#  define vIpcMemCp_DetReportError(a, b, c, d)
# endif

# if (VIPCMEMCP_USE_INIT_POINTER == STD_ON)                                                                             /* COV_VIPCMEMCP_UNSUPPORTED98671 */
#  include "EcuM_Error.h"
# else
#  define EcuM_BswErrorHook(x, y)
# endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

# define VIPCMEMCP_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Global configuration pointer for post-build support. */
P2CONST(vIpcMemCp_ConfigType, VIPCMEMCP_VAR_ZERO_INIT, VIPCMEMCP_PBCFG) vIpcMemCp_ConfigDataPtr = NULL_PTR;             /* PRQA S 1514 */ /* MD_vIpcMemCp_1514 */

# define VIPCMEMCP_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vIpcMemCp_ReportError()
 *********************************************************************************************************************/
/*! \brief         Reports an Error to the DET module, if DET Error Reporting is enabled.
 *  \details       -
 *  \param[in]     ApiId                  Id of the API service.
 *  \param[in]     ErrorId                Error code.
 *  \return        E_OK if there was no error. E_NOT_OK otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_ReportError
(
   vIpcMemCp_ServiceIdType ApiId,
   vIpcMemCp_ErrorCodeType ErrorId
);


/**********************************************************************************************************************
 *  vIpcMemCp_CheckFinalMagicNumber()
 *********************************************************************************************************************/
/*! \brief       Checks the final magic number of the selected post build configuration for validity.
 *  \details     -
 *  \return      TRUE if FinalMagicNumber is valid, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CheckFinalMagicNumber
(
   void
);

# define VIPCMEMCP_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
# define VIPCMEMCP_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vIpcMemCp_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_InitMemory(void)
{
  /* Intentionally left empty */
} /* vIpcMemCp_InitMemory */


/**********************************************************************************************************************
 *  vIpcMemCp_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_Init(P2CONST(vIpcMemCp_ConfigType, AUTOMATIC, VIPCMEMCP_INIT_DATA) ConfigPtr)
{
   vIpcMemCp_ErrorCodeType status = VIPCMEMCP_E_NO_ERROR;

   /* #10 If UseInitPointer is on: */
   if (vIpcMemCp_CfgIf_UseInitPointer() == TRUE)                                                                        /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      /* #20 Initialize the global configuration pointer. */
      vIpcMemCp_ConfigDataPtr = ConfigPtr;

      /* #30 Check that the pointer is not null. */
      if (vIpcMemCp_ConfigDataPtr == NULL_PTR)
      {
         if (vIpcMemCp_CfgIf_UseEcumBswErrorHook() == TRUE)
         {
            EcuM_BswErrorHook(VIPCMEMCP_MODULE_ID, ECUM_BSWERROR_NULLPTR);                                              /* PRQA S 3112 */ /* MD_vIpcMemCp_3112 */
         }
         status = VIPCMEMCP_E_INV_PTR;
      }
      /* #40 Check final magic number. */
      else if (vIpcMemCp_CheckFinalMagicNumber() == FALSE)
      {
         if (vIpcMemCp_CfgIf_UseEcumBswErrorHook() == TRUE)
         {
            EcuM_BswErrorHook(VIPCMEMCP_MODULE_ID, ECUM_BSWERROR_MAGICNUMBER);                                          /* PRQA S 3112 */ /* MD_vIpcMemCp_3112 */
         }
         status = VIPCMEMCP_E_INV_PTR;
      }
      else
      {
         /* No action required in any other case. MISRA 14.10 */
      }
   }
   /* #50 Otherwise check that pointer is null. */
   else if (ConfigPtr != NULL_PTR)
   {
      status = VIPCMEMCP_E_INV_PTR;
   }
   else
   {
      /* No action required in any other case. MISRA 14.10 */
   }

   /* #60 If no error has occured: */
   if (status == VIPCMEMCP_E_NO_ERROR)
   {

      /* #70 Initialize the Tx module. */
      vIpcMemCp_Rx_Init();

      /* #80 Initialize the Rx module. */
      vIpcMemCp_Tx_Init();
   }

   (void) vIpcMemCp_ReportError(VIPCMEMCP_SID_INIT, status);
} /* vIpcMemCp_Init */                                                                                                  /* PRQA S 6050 */ /* MD_MSR_STCAL */


/**********************************************************************************************************************
 *  vIpcMemCp_GetVersionInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VIPCMEMCP_CODE) vIpcMemCp_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) VersionInfo)
{
   vIpcMemCp_ErrorCodeType status = VIPCMEMCP_E_NO_ERROR;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the pointer is valid. */
   if ((isDetEnabled == TRUE) && (VersionInfo == NULL_PTR))                                                             /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      /* #20 Set the version info to the values from the component header. */
      VersionInfo->vendorID         = (VIPCMEMCP_VENDOR_ID);
      VersionInfo->moduleID         = (VIPCMEMCP_MODULE_ID);
      VersionInfo->sw_major_version = (VIPCMEMCP_SW_MAJOR_VERSION);
      VersionInfo->sw_minor_version = (VIPCMEMCP_SW_MINOR_VERSION);
      VersionInfo->sw_patch_version = (VIPCMEMCP_SW_PATCH_VERSION);
   }

   (void) vIpcMemCp_ReportError(VIPCMEMCP_SID_GET_VERSION_INFO, status);
} /* vIpcMemCp_GetVersionInfo */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_BeginTransmit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_BeginTransmit
(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Tx_BeginTransmit(ChannelNr, DataSize);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_TX_BEGINTRANSMIT, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBuffer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Tx_GetBuffer(ChannelNr, DataSize, BufferDesc);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_TX_GETBUFFER, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Confirm()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Tx_Confirm(ChannelNr);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_TX_CONFIRM, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Cancel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Tx_Cancel(ChannelNr);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_TX_CANCEL, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Rx_BeginReceive
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_BeginReceive(
    vIpcMemCp_ChannelIndexType ChannelNr,
    P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) DataSize
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Rx_BeginReceive(ChannelNr, DataSize);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_RX_BEGINRECEIVE, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBuffer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_GetBuffer
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize,
   P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Rx_GetBuffer(ChannelNr, DataSize, BufferDesc);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_RX_GETBUFFER, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Rx_Confirm()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Confirm
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Rx_Confirm(ChannelNr);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_RX_CONFIRM, status);
}


/**********************************************************************************************************************
 * vIpcMemCp_Rx_Cancel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Cancel
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_ErrorCodeType status;

   /* #10 Call the internal API function. */
   status = vIpcMemCp_Api_Rx_Cancel(ChannelNr);

   /* #20 Report any error. */
   return vIpcMemCp_ReportError(VIPCMEMCP_SID_RX_CANCEL, status);
}


#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * LOCAL FUNCTIONS
 *********************************************************************************************************************/

#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_ReportError()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */

VIPCMEMCP_LOCAL_INLINE FUNC(Std_ReturnType, VIPCMEMCP_CODE)
vIpcMemCp_ReportError
(
   vIpcMemCp_ServiceIdType ApiId,                                                                                       /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
   vIpcMemCp_ErrorCodeType ErrorId
)
{
   Std_ReturnType result = E_NOT_OK;

   /* #10 If an error occured: */
   if (ErrorId != VIPCMEMCP_E_NO_ERROR)
   {
      /* #20 If error reporting is enabled: */
      if (vIpcMemCp_CfgIf_DevErrorReport() == TRUE){                                                                    /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */

         /* #30 If the error has to be reported: */
         if ((ErrorId >> VIPCMEMCP_DET_ERROR_BITS) == 0u){

            /* #40 Report error to the DET */
            vIpcMemCp_DetReportError(VIPCMEMCP_MODULE_ID, VIPCMEMCP_INSTANCE_ID, ApiId, ErrorId);
         }
      } else {
         VIPCMEMCP_DUMMY_STATEMENT(ApiId);
      }
   }
   /* #50 Otherwise: */
   else
   {
      /* #60 Set result to E_OK */
      result = E_OK;
   }

   return result;
} /* vIpcMemCp_ReportError */



/**********************************************************************************************************************
 *  vIpcMemCp_CheckFinalMagicNumber()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_CheckFinalMagicNumber
(
   void
)
{
   /* #10 Return if the final magic number of the post-build loaded configuration matches the precompile constant. */
   return (boolean)(vIpcMemCp_GetFinalMagicNumber() == VIPCMEMCP_FINAL_MAGIC_NUMBER);                                   /* PRQA S 2995, 4304 */ /* MD_vIpcMemCp_2995, MD_MSR_AutosarBoolean */
} /* vIpcMemCp_CheckFinalMagicNumber */

#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */


/* Justification for modules-specific MISRA deviations:

  MD_vIpcMemCp_0306:
      Reason:     In order to allow the user to use addresses as shared memory buffers, the buffer address may be
                  configured as integral type. It is necessary to cast this address to a buffer type in order to access
                  the queue meta data.
      Risk:       Casting an address to a pointer may result in unaligned memory access or out of bounds memory access.
      Prevention: Customer has to make sure that the pointer is aligned and that enough memory space is reserved for the
                  buffer (SMI-748168).

  MD_vIpcMemCp_0310:
      Reason:     To retrieve management data from a generic memory buffer
                  the casting to a different pointer type is necessary.
      Risk:       Casting to a stricter alignment can lead to an unaligned memory access.
      Prevention: Implementation ensures that the alignment requirements of the buffer are always fulfilled.
                  Compliance of this assumption is checked during review.

  MD_vIpcMemCp_3219:
      Reason:     This function is inlined and therefore it has to be implemented here. The function is not used
                  by all implementation files which include this header file.
      Risk:       None.
      Prevention: None.

  MD_vIpcMemCp_0488:
      Reason:     In order to operate on a generic memory buffer pointer arithmetic is needed.
      Risk:       Accessing the data beyond the end of the buffer.
      Prevention: Always check for the buffer boundaries.

  MD_vIpcMemCp_1514:
      Reason:     Whether the object is referenced in other translation units, depends on a configuration parameter.
      Risk:       None, because in other configurations the object is referenced.
      Prevention: None.

   MD_vIpcMemCp_2981
      Reason:     Usage of the local variable is necessary. Modification of the variable depends on the configuration.
      Risk:       None, because in other configurations the local variable is modified.
      Prevention: None.

   MD_vIpcMemCp_2983:
      Reason:     The assignment to the local variable is necessary for the next logical expression.
                  As the logical expression may be evaluated as always false due to the configuration, the assignment
                  may be redundant.
      Risk:       None, because the assignment isn't redundant and part of the next logical expression.
      Prevention: None.

   MD_vIpcMemCp_2991:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configuration it may be false.
      Prevention: None.

   MD_vIpcMemCp_2992:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configuration it may be true.
      Prevention: None.

   MD_vIpcMemCp_2994:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configurations the loop is entered.
      Prevention: None.

   MD_vIpcMemCp_2995:
      Reason:     The result of the logical operation depends on a configuration parameter.
      Risk:       None, because in other configurations it may be false.
      Prevention: None.

   MD_vIpcMemCp_2996:
      Reason:     The result of the logical operation depends on a configuration parameter.
      Risk:       None, because in other configuration it may be true.
      Prevention: None.

   MD_vIpcMemCp_3112:
      Reason:     Whether the statement has a side-effect, depends on a configuration parameter.
      Risk:       None, because in other configurations the statement has a side-effect.
      Prevention: None.

   MD_vIpcMemCp_3206:
      Reason:     It depends on the configuration if the parameter is used or not.
      Risk:       None.
      Prevention: None.

   MD_vIpcMemCp_3305:
      Reason:     See MD_vIpcMemCp_0310.
      Risk:       See MD_vIpcMemCp_0310.
      Prevention: See MD_vIpcMemCp_0310.

   MD_vIpcMemCp_3415:
      Reason:     The called function is pure reading function and has no side effects.
      Risk:       None.
      Prevention: None.

   MD_vIpcMemCp_EmptyConfig: Dir4.1, Rule1.3, Rule12.4
      Reason:     When the RX or TX side is not used in a configuration, the ComStackLib generates the access macros to
                  return a NULL_PTR for the buffer and 0 for the buffer size.
      Risk:       Null pointer dereferentiation or usage of invalid buffer size leading to out of bounds accesses.
      Prevention: A SafeBSW runtime check checks that the affected code parts are never executed if the respective side
                  is not configured. It is the customers responsibility to enable SafeBSW checks in his configuration.

*/

/* START_COVERAGE_JUSTIFICATION

\ID COV_VIPCMEMCP_COMPILERKEYWORD
   \ACCEPT TX
   \REASON [COV_MSR_COMPATIBILITY]

\ID COV_VIPCMEMCP_UNSUPPORTED98671
   \ACCEPT XX
   \ACCEPT TX
   \ACCEPT XF
   \REASON Feature is not yet supported, ESCAN00098671 prevents that the code becomes active. Unit test and code
           inspection have not found any risk in keeping this code.

END_COVERAGE_JUSTIFICATION */

/* VCA_JUSTIFICATION_BEGIN

\ID VCA_VIPCMEMCP_MessageHeader
 \DESCRIPTION Pointercast from uint8 to uint32 could lead to an out of bounds access.
              vIpcMemCp_QueueUtils_WriteMessageHeader always writes to a 4 byte aligned address. This is assured by
              usage of vIpcMemCp_QueueUtils_Align.
 \COUNTERMEASURE \S SMI-742094

\ID VCA_VIPCMEMCP_QueueMemoryRegion
 \DESCRIPTION A memory address or an external linker symbol is treated as pointer to a QueueType, but the size of the
              underlying memory region is unknown to VCA. Thus, accesses to that struct are treated as out of bounds.
              vIpcMemCp_Tx_GetQueue or vIpcMemCp_Rx_GetQueue is used to get the pointer. The index parameter (ChannelNr)
              is checked before being used. Size and alignment of the QueueType is assured by SMIs.
 \COUNTERMEASURE \S SMI-742094, SMI-748168

VCA_JUSTIFICATION_END */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp.c
 *********************************************************************************************************************/
