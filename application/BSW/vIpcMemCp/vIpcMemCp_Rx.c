/*********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_Rx.c
 *  \brief Implementation of the unit vIpcMemCp_Rx.
 *
 *  \details -
 *
 *  \unit vIpcMemCp_Rx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpcMemCp_Rx.h"
#include "vIpcMemCp_QueueUtils.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetNrOfChannels()
 *********************************************************************************************************************/
/*! \brief         Returns the number of channels from the receive configuration.
 *  \details       -
 *  \return        Number of channels.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_ChannelIndexType, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetNrOfChannels(
        void);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetQueue()
 *********************************************************************************************************************/
/*! \brief         Returns the pointer to the management data of the receive queue.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Pointer to the queue control structure.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePtrType, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetQueue(
        vIpcMemCp_ChannelIndexType Channel);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBufferStart()
 *********************************************************************************************************************/
/*! \brief         Returns the pointer to the start of the receive queue buffer.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Pointer to the start of the buffer.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_BufferPtrType, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetBufferStart(
        vIpcMemCp_ChannelIndexType Channel);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBufferSize()
 *********************************************************************************************************************/
/*! \brief         Returns the size of the receive queue buffer.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Size of the buffer.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetBufferSize(
        vIpcMemCp_ChannelIndexType Channel);

/**********************************************************************************************************************
 * vIpcMemCp_Rx_QueueIsInitialized()
 *********************************************************************************************************************/
/*! \brief         Returns if the RX parts of a queue are initialized.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \return        TRUE if the RX parts of the queue are initialized, FALSE otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_QueueIsInitialized(
        P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_QueueIsPending()
 *********************************************************************************************************************/
/*! \brief         Returns if a reception is already pending.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \return        TRUE if a reception is already pending, FALSE otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_QueueIsPending(
        P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetReservedSize()
 *********************************************************************************************************************/
/*! \brief         Returns the size of reserved reception segment.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \param[in]     BufferSize              Size of the underlying buffer.
 *  \return        Reserved size in bytes.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetReservedSize(
        P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
        vIpcMemCp_BufferSizeType BufferSize);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetPendingMessageSize()
 *********************************************************************************************************************/
/*! \brief         Returns the message size which was stored into the buffer during vIpcMemCp_Rx_BeginReceive.
 *  \details       -
 *  \param[in]     ChannelNr               Channel index. The plausibility checks must be already done before.
 *  \return        Size of the pending message.
 *
 *  \pre           The channel index was already checked to be valid.
 *
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetPendingMessageSize(
        vIpcMemCp_ChannelIndexType ChannelNr);


/**********************************************************************************************************************
 * vIpcMemCp_Rx_CheckTotalDataSize()
 *********************************************************************************************************************/
/*! \brief         Checks that the total accumulated reserved size does not exceeds the message size.
 *  \details       -
 *  \param[in]     ChannelNr               Channel index. The plausibility checks must be already done before.
 *  \param[in]     DataSize                The reserved size increment to check.
 *  \return        TRUE if the check fails, FALSE otherwise.
 *
 *  \pre           The channel index was already checked to be valid.
 *
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_CheckTotalDataSize(
        vIpcMemCp_ChannelIndexType ChannelNr,
        uint32 DataSize);

#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vIpcMemCp_Rx_Init()
 *********************************************************************************************************************/
/*!
  * Internal comment removed.
 *
 *
 *
 *********************************************************************************************************************/
FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_Rx_Init(void)
{
   vIpcMemCp_ChannelIndexType i;
   vIpcMemCp_QueuePtrType queue;

   /* #10 For each configured Rx channel: */
   for (i = 0; i < vIpcMemCp_Rx_GetNrOfChannels(); i++)                                                                 /* PRQA S 2994, 2996 */ /* MD_vIpcMemCp_2994, MD_vIpcMemCp_2996 */
   {
      /* #20 Initialize the Receive-Part of the Receiver-Queue. */
      queue = vIpcMemCp_Rx_GetQueue(i);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */
      /* @ assert vIpcMemCp_GetBufferSizeOfRxConfig(i) >= 28; */
      queue->Tail = 0;
      queue->TailReserved = 0;
      queue->TailInitialized = TRUE;
   }
}


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Rx_BeginReceive()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_BeginReceive(
    vIpcMemCp_ChannelIndexType ChannelNr,
    P2VAR(uint32, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) DataSize
)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Rx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #20 Check if the parameter pointer is valid. */
   else if ((isDetEnabled == TRUE) && (DataSize == NULL_PTR))                                                           /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_INV_PTR;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #30 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Rx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #40 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #50 Check if a receiving already in progress. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsPending(queue) == TRUE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_RX_ALREADY_PENDING;
      }
      else
      {
         /* #60 If the queue is not empty: */
         if (vIpcMemCp_QueueUtils_QueueIsEmpty(queue) == FALSE)
         {
            vIpcMemCp_MessageHeaderType header;
            vIpcMemCp_QueuePosType headerPos;

            /* #70 Get the corrected header position. */
            headerPos = vIpcMemCp_QueueUtils_QueueGetCorrectedPos(&(queue->Tail), vIpcMemCp_Rx_GetBufferSize(ChannelNr));

            /* #80 Read the header from the buffer. */
            vIpcMemCp_QueueUtils_ReadMessageHeader(vIpcMemCp_Rx_GetBufferStart(ChannelNr) + headerPos, &header);        /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

            /* #90 Store the message size. */
            *DataSize = header.Size;

            /* #100 Advance the reserved queue pointer. */
            queue->TailReserved = headerPos + sizeof(vIpcMemCp_MessageHeaderType);

         }
         else
         {
            status = VIPCMEMCP_E_BUFFEREMPTY_INT;
         }
      }
   }

   return status;
} /* vIpcMemCp_Api_Rx_BeginReceive */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Rx_GetBuffer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_GetBuffer(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize,
    P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Rx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #20 Check if the size is valid. */
   else if ((isDetEnabled == TRUE) && (DataSize == 0u))                                                                 /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #30 Check if the parameter pointer is valid. */
   else if ((isDetEnabled == TRUE) && (BufferDesc == NULL_PTR))                                                         /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_INV_PTR;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #40 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Rx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #50 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #60 Check if a reception was started already. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_RX_NOT_PENDING;
      }
      /* #70 Check if the requested size does not exceed the message size. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_CheckTotalDataSize(ChannelNr, DataSize) == TRUE))                /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_INCONSISTENT_SIZE;
      }
      else
      {
         /* #80 Calculate the size from "reserved" pointer until end of the buffer. */
         uint32 sizeUntilEnd = vIpcMemCp_Rx_GetBufferSize(ChannelNr) - queue->TailReserved;

         /* #90 Wrap-around the "reserved" pointer if it's currently at the end. */
         if (sizeUntilEnd == 0u)
         {
            queue->TailReserved = 0u;
            sizeUntilEnd = DataSize;
         }

         /* #100 Store the start address of the buffer segment. */
         BufferDesc->Ptr = vIpcMemCp_Rx_GetBufferStart(ChannelNr) + queue->TailReserved;                                /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

         /* #110 Store the size until end of the buffer if it's smaller than requested size. */
         if (DataSize > sizeUntilEnd)
         {
            BufferDesc->Size = sizeUntilEnd;
         }
         /* #120 Store the full size otherwise. */
         else
         {
            BufferDesc->Size = DataSize;
         }

         /* #130 Advance the "reserved" pointer by buffer segment's size. */
         queue->TailReserved += BufferDesc->Size;
      }
   }

   return status;
} /* vIpcMemCp_Api_Rx_GetBuffer */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Rx_Confirm()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_Confirm(
    vIpcMemCp_ChannelIndexType ChannelNr)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Rx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #20 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Rx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #30 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #40 Check if a receiving was started. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_RX_NOT_PENDING;
      }
      else
      {
         /* #50 Retrieve the pending message size from the buffer. */
         uint32 messageSize = vIpcMemCp_Rx_GetPendingMessageSize(ChannelNr);                                            /* PRQA S 2983 */ /* MD_vIpcMemCp_2983 */

         /* #60 Calculate the size of reserved data. */
         uint32 reservedSize = vIpcMemCp_Rx_GetReservedSize(queue, vIpcMemCp_Rx_GetBufferSize(ChannelNr));              /* PRQA S 2983 */ /* MD_vIpcMemCp_2983 */

         /* #70 If the calculated size doesn't correspond with the message size: */
         if ((isDetEnabled == TRUE) && (reservedSize != (messageSize + sizeof(vIpcMemCp_MessageHeaderType))))           /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
         {
            /* #80 Rollback the active receive. */
            queue->TailReserved = vIpcMemCp_QueueUtils_ReadQueuePointer(&(queue->Tail));

            status = VIPCMEMCP_E_INCONSISTENT_SIZE;
         }
         /* #90 Otherwise: */
         else
         {
            /* #100 Align the "reserved" pointer. */
            queue->TailReserved = vIpcMemCp_QueueUtils_Align(queue->TailReserved);
            /* #110 Apply the aligned "reserved" pointer into the normal. */
            vIpcMemCp_QueueUtils_WriteQueuePointer(&(queue->Tail), queue->TailReserved);
         }
      }
   }
   return status;
} /* vIpcMemCp_Api_Rx_Confirm */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 * vIpcMemCp_Api_Rx_Cancel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Rx_Cancel(
    vIpcMemCp_ChannelIndexType ChannelNr)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Rx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;
      /* #20 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Rx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #30 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #40 Check if a receiving was started. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Rx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_RX_NOT_PENDING;
      }
      else
      {
         vIpcMemCp_QueuePosType curTail;
         vIpcMemCp_QueuePosType tailAfterMsg;
         vIpcMemCp_MessageHeaderType header;
         uint32 fullSize;
         uint32 sizeUntilEnd;

         /* #50 Read current tail value. */
         curTail = vIpcMemCp_QueueUtils_QueueGetCorrectedPos(&(queue->Tail), vIpcMemCp_Rx_GetBufferSize(ChannelNr));

         /* #60 Read the header from the buffer. */
         vIpcMemCp_QueueUtils_ReadMessageHeader(vIpcMemCp_Rx_GetBufferStart(ChannelNr) + curTail, &header);             /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

         /* #70 Calculate the full data size. */
         fullSize = header.Size + sizeof(vIpcMemCp_MessageHeaderType);

         /* #80 Calculate the size until end of the buffer. */
         sizeUntilEnd = vIpcMemCp_Rx_GetBufferSize(ChannelNr) - curTail;

         /* #90 Calculate the wrap-around position if the message does not fit until end of the buffer. */
         if (sizeUntilEnd < fullSize)
         {
            tailAfterMsg = fullSize - sizeUntilEnd;
         }
         /* #100 Add to the current position otherwise. */
         else
         {
            tailAfterMsg = curTail + fullSize;
         }

         /* #110 Align the resulting pointer. */
         tailAfterMsg = vIpcMemCp_QueueUtils_Align(tailAfterMsg);

         /* #120 Change both pointers. */
         vIpcMemCp_QueueUtils_WriteQueuePointer(&(queue->Tail), tailAfterMsg);
         queue->TailReserved = tailAfterMsg;
      }
   }
   return status;
} /* vIpcMemCp_Api_Rx_Cancel */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */

#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetNrOfChannels()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_ChannelIndexType, VIPCMEMCP_CODE)
    vIpcMemCp_Rx_GetNrOfChannels(
        void)
{
   return vIpcMemCp_GetSizeOfRxConfig();
} /* vIpcMemCp_Rx_GetNrOfChannels */

/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetQueue()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePtrType, VIPCMEMCP_CODE) vIpcMemCp_Rx_GetQueue
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (vIpcMemCp_QueuePtrType)vIpcMemCp_GetQueueOfRxConfig(Channel);                                                /* PRQA S 0310, 3305 */ /* MD_vIpcMemCp_0310, MD_vIpcMemCp_3305 */
} /* vIpcMemCp_Rx_GetQueue */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBufferStart()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_BufferPtrType, VIPCMEMCP_CODE) vIpcMemCp_Rx_GetBufferStart
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (vIpcMemCp_BufferPtrType)vIpcMemCp_GetQueueOfRxConfig(Channel) + sizeof(vIpcMemCp_QueueType);                 /* PRQA S 0488, 2820 */ /* MD_vIpcMemCp_0488, MD_vIpcMemCp_EmptyConfig */
} /* vIpcMemCp_Rx_GetBufferStart */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetBufferSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Rx_GetBufferSize
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (uint32)(vIpcMemCp_GetBufferSizeOfRxConfig(Channel) - sizeof(vIpcMemCp_QueueType));                           /* PRQA S 2910 */ /* MD_vIpcMemCp_EmptyConfig */
} /* vIpcMemCp_Rx_GetBufferSize */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_QueueIsInitialized()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Rx_QueueIsInitialized
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
)
{
   return (boolean)(Queue->TailInitialized == TRUE);                                                                    /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Rx_QueueIsInitialized */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_QueueIsPending()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Rx_QueueIsPending
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
)
{
   vIpcMemCp_QueuePosType pos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Tail));

   return (boolean)(pos != Queue->TailReserved); /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Rx_QueueIsPending */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetReservedSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Rx_GetReservedSize
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
   vIpcMemCp_BufferSizeType BufferSize
)
{
   vIpcMemCp_QueuePosType pos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Tail));                                  /* PRQA S 2811 */ /* MD_vIpcMemCp_EmptyConfig */

   return vIpcMemCp_QueueUtils_QueueGetDistance(Queue->TailReserved, pos, BufferSize);
} /* vIpcMemCp_Rx_GetReservedSize */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_GetPendingMessageSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Rx_GetPendingMessageSize
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_MessageHeaderType header;

   /* #10 Get the queue. */
   vIpcMemCp_QueuePtrConstType queue = vIpcMemCp_Rx_GetQueue(ChannelNr);

   /* #20 Get the current Tail position from the queue. */
   vIpcMemCp_QueuePosType curTail = vIpcMemCp_QueueUtils_QueueGetCorrectedPos(&(queue->Tail),                           /* PRQA S 2811 */ /* MD_vIpcMemCp_EmptyConfig */
                                        vIpcMemCp_Rx_GetBufferSize(ChannelNr));

   /* #30 Read the header of the pending message. */
   vIpcMemCp_QueueUtils_ReadMessageHeader(vIpcMemCp_Rx_GetBufferStart(ChannelNr) + curTail, &header);                   /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

   return header.Size;
} /* vIpcMemCp_Rx_GetPendingMessageSize */


/**********************************************************************************************************************
 * vIpcMemCp_Rx_CheckTotalDataSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Rx_CheckTotalDataSize
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize
)
{
   /* #10 Calculate the reserved size of the current message. */
   uint32 reservedSize = vIpcMemCp_Rx_GetReservedSize(vIpcMemCp_Rx_GetQueue(ChannelNr), vIpcMemCp_Rx_GetBufferSize(ChannelNr));

   /* #20 Get the message size from the buffer. */
   uint32 messageSize = vIpcMemCp_Rx_GetPendingMessageSize(ChannelNr);

   /* #30 Return if the increment would exceed the message size. */
   return (boolean)(((reservedSize + DataSize) - sizeof(vIpcMemCp_MessageHeaderType)) > (messageSize)); /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Rx_CheckTotalDataSize */


#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_Rx.c
 *********************************************************************************************************************/
