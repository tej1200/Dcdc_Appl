/*********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemCp_Tx.c
 *  \brief Implementation of the unit vIpcMemCp_Tx.
 *
 *  \details -
 *
 *  \unit vIpcMemCp_Tx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemCp.h.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpcMemCp_Tx.h"
#include "vIpcMemCp_QueueUtils.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetNrOfChannels()
 *********************************************************************************************************************/
/*! \brief         Returns the number of channels from the transmit configuration.
 *  \details       -
 *  \return        Number of channels.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_ChannelIndexType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetNrOfChannels
(
   void
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetQueue()
 *********************************************************************************************************************/
/*! \brief         Returns the pointer to the management data of the transmit queue.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Pointer to the queue control structure.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePtrType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetQueue
(
   vIpcMemCp_ChannelIndexType Channel
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBufferStart()
 *********************************************************************************************************************/
/*! \brief         Returns the pointer to the start of the transmit queue buffer.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Pointer to the start of the buffer.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_BufferPtrType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetBufferStart
(
    vIpcMemCp_ChannelIndexType Channel
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBufferSize()
 *********************************************************************************************************************/
/*! \brief         Returns the size of the transmit queue buffer.
 *  \details       -
 *  \param[in]     Channel                  Channel to query.
 *  \return        Size of the buffer.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetBufferSize
(
    vIpcMemCp_ChannelIndexType Channel
);

/**********************************************************************************************************************
 * vIpcMemCp_Tx_QueueIsInitialized()
 *********************************************************************************************************************/
/*! \brief         Returns if the TX parts of a queue are initialized.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \return        TRUE if the TX parts of the queue are initialized, FALSE otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
    vIpcMemCp_Tx_QueueIsInitialized(
        P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_QueueIsPending()
 *********************************************************************************************************************/
/*! \brief         Returns if the transmit is already pending.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \return        TRUE if transmit is already pending, FALSE otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_Tx_QueueIsPending
(
    P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetReservedSize()
 *********************************************************************************************************************/
/*! \brief         Returns the size of reserved transmission segment.
 *  \details       -
 *  \param[in]     Queue                   Queue to check. Parameter must not be NULL_PTR.
 *  \param[in]     BufferSize              Size of the underlying buffer.
 *  \return        Reserved size in bytes.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetReservedSize
(
    P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
    vIpcMemCp_BufferSizeType BufferSize
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetPendingMessageSize()
 *********************************************************************************************************************/
/*! \brief         Returns the message size which was stored into the buffer during vIpcMemCp_Tx_BeginTransmit.
 *  \details       -
 *  \param[in]     ChannelNr               Channel index. The plausibility checks must be already done before.
 *  \return        Size of the pending message.
 *
 *  \pre           The channel index was already checked to be valid.
 *
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetPendingMessageSize
(
    vIpcMemCp_ChannelIndexType ChannelNr
);


/**********************************************************************************************************************
 * vIpcMemCp_Tx_CheckTotalDataSize()
 *********************************************************************************************************************/
/*! \brief         Checks that the total accumulated reserved size does not exceeds the message size.
 *  \details       -
 *  \param[in]     ChannelNr               Channel index. The plausibility checks must be already done before.
 *  \param[in]     DataSize                The reserved size increment to check.
 *  \return        TRUE if the check fails, FALSE otherwise.
 *
 *  \pre           The channel index was already checked to be valid.
 *
 *  \context       ANY
 *  \reentrant     TRUE for different channels
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE)
vIpcMemCp_Tx_CheckTotalDataSize
(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize
);


#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_Init()
 *********************************************************************************************************************/
 /*!
  * Internal comment removed.
 *
 *
 *
  */
FUNC(void, VIPCMEMCP_CODE)
vIpcMemCp_Tx_Init ( void )
{
   vIpcMemCp_ChannelIndexType i;
   vIpcMemCp_QueuePtrType queue;

   /* #10 For each configured Tx channel: */
   for (i = 0; i < vIpcMemCp_Tx_GetNrOfChannels(); i++)                                                                  /* PRQA S 2994, 2996 */ /* MD_vIpcMemCp_2994, MD_vIpcMemCp_2996 */
   {
      /* #20 Initialize the Send-Part of the Sender-Queue. */
      queue = vIpcMemCp_Tx_GetQueue(i);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */
      /* @ assert vIpcMemCp_GetBufferSizeOfTxConfig(i) >= 28; */
      queue->Head = 0;
      queue->HeadReserved = 0;
      queue->HeadInitialized = TRUE;
   }
}


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Tx_BeginTransmit()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_BeginTransmit(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Tx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #20 Check if the size is valid. */
   else if ((isDetEnabled == TRUE) && (DataSize == 0u))                                                                 /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #30 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Tx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #40 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #50 Check if a transmission already in progress. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsPending(queue) == TRUE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_TX_ALREADY_PENDING;
      }
      else
      {
         /* #60 Calculate the total data size. */
         uint32 totalDataSize = DataSize + sizeof(vIpcMemCp_MessageHeaderType);

         /* #70 If enough space is available: */
         if (vIpcMemCp_QueueUtils_QueueLeft(queue, vIpcMemCp_Tx_GetBufferSize(ChannelNr)) >= totalDataSize)
         {
            vIpcMemCp_MessageHeaderType header;
            vIpcMemCp_QueuePosType headerPos;

            /* #80 Prepare the header. */
            header.Size = DataSize;

            /* #90 Get the corrected header position. */
            headerPos = vIpcMemCp_QueueUtils_QueueGetCorrectedPos(&(queue->Head), vIpcMemCp_Tx_GetBufferSize(ChannelNr));

            /* #100 Write header into the buffer. */

            /*
               The overflow check is not necessary as it is always ensured that the header fits fully
               into a continuous data block.
            */
            vIpcMemCp_QueueUtils_WriteMessageHeader(vIpcMemCp_Tx_GetBufferStart(ChannelNr) + headerPos, &header);       /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

            /* #110 Advance the reserved queue pointer. */
            queue->HeadReserved = headerPos + sizeof(vIpcMemCp_MessageHeaderType);
         }
         else
         {
            status = VIPCMEMCP_E_BUFFERFULL_INT;
         }
      }
   }
   return status;
} /* vIpcMemCp_Api_Tx_BeginTransmit */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Tx_GetBuffer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_GetBuffer(
    vIpcMemCp_ChannelIndexType ChannelNr,
    uint32 DataSize,
    P2VAR(vIpcMemCp_BufferDescriptionType, AUTOMATIC, VIPCMEMCP_VAR_NOINIT) BufferDesc)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Tx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #20 Check if the size is valid. */
   else if ((isDetEnabled == TRUE) && (DataSize == 0u))                                                                 /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   /* #30 Check if the parameter pointer is valid. */
   else if ((isDetEnabled == TRUE) && (BufferDesc == NULL_PTR))                                                         /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_INV_PTR;
   }
   else
   {
      vIpcMemCp_QueuePosType headReserved;
      uint32 bufferSize;

      status = VIPCMEMCP_E_NO_ERROR;

      /* #40 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Tx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #45 Retrieve the reserved offset for the pending transmission. */
      headReserved = queue->HeadReserved;

      /* #47 Retrieve the usable size of the buffer. */
      bufferSize = vIpcMemCp_Tx_GetBufferSize(ChannelNr);

      /* #50 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #60 Check if a transmission was started. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_TX_NOT_PENDING;
      }
      /* #70 Check if the requested size does not exceeds the message size. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_CheckTotalDataSize(ChannelNr, DataSize) == TRUE))                /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_INCONSISTENT_SIZE;
      }
      /* #75 Check if the offset value for write access to the buffer is valid. */
      else if ((isDetEnabled == TRUE) && (headReserved > bufferSize))                                                   /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         /* HeadReserved needs to be checked for validity, as the management information can get corrupted
            by non ASIL software parts. */
         status = VIPCMEMCP_E_INV_PTR;
      }
      else
      {
         /* #80 Calculate the size from "reserved" pointer until end of the buffer. */
         uint32 sizeUntilEnd = bufferSize - headReserved;

         /* #90 Wrap-around the "reserved" pointer if it's currently at the end. */
         if (sizeUntilEnd == 0u)
         {
            headReserved = 0u;
            sizeUntilEnd = DataSize;
         }

         /* #100 Store the start address of the buffer segment. */
         BufferDesc->Ptr = vIpcMemCp_Tx_GetBufferStart(ChannelNr) + headReserved;                                       /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

         /* #110 Store the size until end of the buffer if it's smaller than requested size. */
         if (DataSize > sizeUntilEnd)
         {
            BufferDesc->Size = sizeUntilEnd;
         }
         /* #120 Store the full size otherwise. */
         else
         {
            BufferDesc->Size = DataSize;
         }

         /* #130 Advance the "reserved" pointer by buffer segment's size. */
         queue->HeadReserved = headReserved + BufferDesc->Size;
      }
   }

   return status;
} /* vIpcMemCp_Api_Tx_GetBuffer */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 *  vIpcMemCp_Api_Tx_Confirm()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_Confirm(
    vIpcMemCp_ChannelIndexType ChannelNr)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Tx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #20 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Tx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #30 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #40 Check if a transmission was started. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_TX_NOT_PENDING;
      }
      else
      {
         /* #50 Retrieve the pending message size from the buffer. */
         uint32 messageSize = vIpcMemCp_Tx_GetPendingMessageSize(ChannelNr);                                            /* PRQA S 2983 */ /* MD_vIpcMemCp_2983 */

         /* #60 Calculate the size of reserved data. */
         uint32 reservedSize = vIpcMemCp_Tx_GetReservedSize(queue, vIpcMemCp_Tx_GetBufferSize(ChannelNr));              /* PRQA S 2983 */ /* MD_vIpcMemCp_2983 */

         /* #70 If the calculated size doesn't correspond with the message size: */
         if ((isDetEnabled == TRUE) && (reservedSize != (messageSize + sizeof(vIpcMemCp_MessageHeaderType))))           /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
         {
            /* #80 Rollback the active transmit. */
            queue->HeadReserved = vIpcMemCp_QueueUtils_ReadQueuePointer(&(queue->Head));

            status = VIPCMEMCP_E_INCONSISTENT_SIZE;
         }
         /* #90 Otherwise: */
         else
         {
            /* #100 Align the "reserved" pointer. */
            queue->HeadReserved = vIpcMemCp_QueueUtils_Align(queue->HeadReserved);
            /* #110 Apply the aligned "reserved" pointer into the normal. */
            vIpcMemCp_QueueUtils_WriteQueuePointer(&(queue->Head), queue->HeadReserved);
         }
      }
   }
   return status;
} /* vIpcMemCp_Api_Tx_Confirm */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */


/**********************************************************************************************************************
 * vIpcMemCp_Api_Tx_Cancel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vIpcMemCp_ErrorCodeType, VIPCMEMCP_CODE)
vIpcMemCp_Api_Tx_Cancel(
    vIpcMemCp_ChannelIndexType ChannelNr)
{
   vIpcMemCp_ErrorCodeType status;
   vIpcMemCp_QueuePtrType queue;
   boolean isDetEnabled = vIpcMemCp_CfgIf_DevErrorDetect();

   /* #10 Check if the channel index is correct. */
   if ((isDetEnabled == TRUE) && (ChannelNr >= vIpcMemCp_Tx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemCp_2991, MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996 */
   {
      status = VIPCMEMCP_E_PARAM;
   }
   else
   {
      status = VIPCMEMCP_E_NO_ERROR;

      /* #20 Retrieve the pointer to the queue management data. */
      queue = vIpcMemCp_Tx_GetQueue(ChannelNr);

      /* @ assert $external(queue); */                                                                                  /* VCA_VIPCMEMCP_QueueMemoryRegion */

      /* #30 Check if the channel was fully initialized. */
      if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsInitialized(queue) == FALSE))                                  /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_UNINIT;
      }
      /* #40 Check if a transmission was started. */
      else if ((isDetEnabled == TRUE) && (vIpcMemCp_Tx_QueueIsPending(queue) == FALSE))                                 /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemCp_2992, MD_vIpcMemCp_2995, MD_vIpcMemCp_2996, MD_vIpcMemCp_3415 */
      {
         status = VIPCMEMCP_E_TX_NOT_PENDING;

      }
      else
      {
         /* #50 Rollback the active transmit. */
         queue->HeadReserved = vIpcMemCp_QueueUtils_ReadQueuePointer(&(queue->Head));
      }
   }
   return status;
} /* vIpcMemCp_Api_Tx_Cancel */

#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
#define VIPCMEMCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetNrOfChannels()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_ChannelIndexType, VIPCMEMCP_CODE)
vIpcMemCp_Tx_GetNrOfChannels(
    void)
{
   return vIpcMemCp_GetSizeOfTxConfig();
} /* vIpcMemCp_Tx_GetNrOfChannels */

/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetQueue()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_QueuePtrType, VIPCMEMCP_CODE) vIpcMemCp_Tx_GetQueue
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (vIpcMemCp_QueuePtrType)vIpcMemCp_GetQueueOfTxConfig(Channel);                                                /* PRQA S 0310, 3305 */ /* MD_vIpcMemCp_0310, MD_vIpcMemCp_3305 */
} /* vIpcMemCp_Tx_GetQueue */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBufferStart()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(vIpcMemCp_BufferPtrType, VIPCMEMCP_CODE) vIpcMemCp_Tx_GetBufferStart
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (vIpcMemCp_BufferPtrType)vIpcMemCp_GetQueueOfTxConfig(Channel) + sizeof(vIpcMemCp_QueueType);                 /* PRQA S 0488, 2820 */ /* MD_vIpcMemCp_0488, MD_vIpcMemCp_EmptyConfig */
} /* vIpcMemCp_Tx_GetBufferStart */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetBufferSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Tx_GetBufferSize
(
   vIpcMemCp_ChannelIndexType Channel                                                                                   /* PRQA S 3206 */ /* MD_vIpcMemCp_3206 */
)
{
   return (uint32)(vIpcMemCp_GetBufferSizeOfTxConfig(Channel) - sizeof(vIpcMemCp_QueueType));                           /* PRQA S 2910 */ /* MD_vIpcMemCp_EmptyConfig */
} /* vIpcMemCp_Tx_GetBufferSize */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_QueueIsInitialized()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Tx_QueueIsInitialized
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
)
{
   return (boolean)(Queue->HeadInitialized == TRUE);                                                                    /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Tx_QueueIsInitialized */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_QueueIsPending()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Tx_QueueIsPending
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue
)
{
   vIpcMemCp_QueuePosType pos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Head));

   return (boolean)(pos != Queue->HeadReserved);                                                                        /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Tx_QueueIsPending */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetReservedSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Tx_GetReservedSize
(
   P2CONST(vIpcMemCp_QueueType, AUTOMATIC, VIPCMEMCP_CONST) Queue,
   vIpcMemCp_BufferSizeType BufferSize
)
{
   vIpcMemCp_QueuePosType pos = vIpcMemCp_QueueUtils_ReadQueuePointer(&(Queue->Head));                                  /* PRQA S 2811 */ /* MD_vIpcMemCp_EmptyConfig */

   return vIpcMemCp_QueueUtils_QueueGetDistance(Queue->HeadReserved, pos, BufferSize);
} /* vIpcMemCp_Tx_GetReservedSize */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_GetPendingMessageSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(uint32, VIPCMEMCP_CODE) vIpcMemCp_Tx_GetPendingMessageSize
(
   vIpcMemCp_ChannelIndexType ChannelNr
)
{
   vIpcMemCp_MessageHeaderType header;

   /* #10 Get the queue. */
   vIpcMemCp_QueuePtrConstType queue = vIpcMemCp_Tx_GetQueue(ChannelNr);

   /* #20 Get the current Head position from the queue. */
   vIpcMemCp_QueuePosType curHead = vIpcMemCp_QueueUtils_QueueGetCorrectedPos(&(queue->Head),                           /* PRQA S 2811 */ /* MD_vIpcMemCp_EmptyConfig */
                                        vIpcMemCp_Tx_GetBufferSize(ChannelNr));

   /* #30 Read the header of the pending message. */
   vIpcMemCp_QueueUtils_ReadMessageHeader(vIpcMemCp_Tx_GetBufferStart(ChannelNr) + curHead, &header);                   /* PRQA S 0488 */ /* MD_vIpcMemCp_0488 */

   return header.Size;
} /* vIpcMemCp_Tx_GetPendingMessageSize */


/**********************************************************************************************************************
 * vIpcMemCp_Tx_CheckTotalDataSize()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPCMEMCP_LOCAL_INLINE FUNC(boolean, VIPCMEMCP_CODE) vIpcMemCp_Tx_CheckTotalDataSize
(
   vIpcMemCp_ChannelIndexType ChannelNr,
   uint32 DataSize
)
{
   /* #10 Calculate the reserved size of the current message. */
   uint32 reservedSize = vIpcMemCp_Tx_GetReservedSize(vIpcMemCp_Tx_GetQueue(ChannelNr),
                             vIpcMemCp_Tx_GetBufferSize(ChannelNr));

   /* #20 Get the message size from the buffer. */
   uint32 messageSize = vIpcMemCp_Tx_GetPendingMessageSize(ChannelNr);

   /* #30 Return if the increment would exceed the message size. */
   return (boolean)(((reservedSize + DataSize) - sizeof(vIpcMemCp_MessageHeaderType)) > (messageSize));                 /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemCp_Tx_CheckTotalDataSize */


#define VIPCMEMCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemCp_Tx.c
 *********************************************************************************************************************/
