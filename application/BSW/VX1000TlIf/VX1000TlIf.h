/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /*!       \file  VX1000TlIf.h
 *        \brief  VX1000 TL Interface header file
 *
 *      \details  Implementation of an API wrapper between XCP Protocol Layer and proprietary VX1000 Transport Layer names.
 *
 *********************************************************************************************************************/ 

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  1.00.00   2021-04-01  visore  SWAT-1471     Implement VX1000TlIf Wrapper
 *********************************************************************************************************************/

#if !defined(VX1000TLIF_H)
# define VX1000TLIF_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"
# include "VX1000Tl.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VX1000TLIF_SW_MAJOR_VERSION                    (1u)
# define VX1000TLIF_SW_MINOR_VERSION                    (0u)
# define VX1000TLIF_SW_PATCH_VERSION                    (0u)

#  if !defined(VX1000TlIf_IsAccessEnabled)
#   error "no boolean expression defined that tells the interface whether to call or to not call the actual driver"
#  else /* VX1000TlIf_IsAccessEnabled */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ------- Modes ------- */
#  define VX1000TLIF_UNINIT                              (0x00u)
#  define VX1000TLIF_INITIALIZED                         (0x01u)

/* --- Return Values --- */
#  define VX1000TLIF_RET_VOID                            (void)0
#  define VX1000TLIF_RET_E_NOT_OK                        (0xFFu)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
#  define VX1000TLIF_IMPL(functionName, retVal)      ((!VX1000TlIf_IsAccessEnabled) || (VX1000TlIf_State != VX1000TLIF_INITIALIZED))?(VX1000TlIf_ErrorCount++,retVal):functionName
#  define VX1000TLIF_IMPL_VOID(functionName)          if ((!VX1000TlIf_IsAccessEnabled) || (VX1000TlIf_State != VX1000TLIF_INITIALIZED)) { VX1000TlIf_ErrorCount++; } else functionName

/**********************************************************************************************************************
 *  VX1000TlIf_SetDeviceMetaInfo()
 *********************************************************************************************************************/
/*! \brief       Macro to set the measurement device meta information.
 *  \details     The application requests the VX1000 TL driver to set the meta information for all the queues of this
 *               process.
 *  \param[in]   measurementDeviceId:  Measurement device ID that should be used by the TL driver.
 *                                     The value will be stored only when the function returns success.
 *  \param[in]   logicalChannelNumber: Logical channel number that should be used by the TL driver.
 *                                     The value will be stored only when the function returns success.
 *  \return      0  - endpoint configured, tool connected and meta information set successfully.
 *               2  - endpoint has not been configured.
 *               3  - endpoint configured but no tool is connected yet.
 *               7  - VX1000TlIf_Init() not successfully called yet.
 *               11 - unspecified error.
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_Init must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220277
 *********************************************************************************************************************/
#  define VX1000TlIf_SetDeviceMetaInfo                    VX1000TLIF_IMPL(VX1000TL_SET_DEVICE_META_INFO, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_GetCompDaqDeviceId()
 *********************************************************************************************************************/
/*! \brief       Macro to get the unique device ID of the base module.
 *  \details     The application requests the VX1000 TL driver for the unique ID of the base module that is managing
 *               the complimentary DAQ channel.
 *  \param[out]  uniqueDeviceId:  Pointer to a 64-bit variable.
 *                                The value is overwritten with the unique device ID if successful.
 *  \return      0  - endpoint configured and unique device ID retrieved successfully.
 *               2  - endpoint has not been configured.
 *               7  - VX1000TlIf_Init() not successfully called yet.
 *               11 - unspecified error.
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_Init must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220278
 *********************************************************************************************************************/
#  define VX1000TlIf_GetCompDaqDeviceId                   VX1000TLIF_IMPL(VX1000TL_GET_DAQ_DEVICE_ID, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_RequestDaqTxQueues()
 *********************************************************************************************************************/
/*! \brief       Macro to request all queues needed for DAQ measurement.
 *  \details     The application requests queues for all XCP events at once. This API should be used for requesting
 *               TX queues needed for DAQ measurement.
 *  \param[in]     maxEvents:     Number of eventTable elements.
 *  \param[in,out] eventTable:    Pointer to the event table array.
 *                                Each element contains event specific information of the measurement infrastructure.
 *  \return      0  - queues have been allocated successfully.
 *               7  - VX1000TlIf_SetDeviceMetaInfo() not successfully called yet (or)
 *                    DAQ TX queues have been previously allocated but not yet freed.
 *               8  - data size of XCP event(s) larger than the maximum allowed size.
 *               9  - not enough memory available in the target for allocating the TX queues.
 *               10 - not enough free TX queue(s) available in the VX1000 measurement system.
 *               11 - unspecified error
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_SetDeviceMetaInfo must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220315
 *********************************************************************************************************************/
#  define VX1000TlIf_RequestDaqTxQueues                   VX1000TLIF_IMPL(VX1000TL_REQUEST_DAQ_TXQ, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_PrepareNextTxData()
 *********************************************************************************************************************/
/*! \brief       Macro to prepare the queue for data transmission.
 *  \details     The application requests the VX1000 TL driver for a pointer to the location within the queue where the
 *               next data has to be copied.
 *  \param[in]   queueId:         ID of the queue to which data has to be copied.
 *  \param[in]   dataSizeBytes:   Size of data (in bytes) that will be copied to this queue.
 *                                It must not exceed the numberOfReservedQueueBytes of this queue ID.
 *  \param[out]  addrQueueBuffer: Pointer to the address where the next data can be copied if successful.
 *                                It's a pointer that points to a byte pointer (pointer to a pointer).
 *  \return      0  - queue successfully prepared for next TX data.
 *               4  - invalid queue ID.
 *               5  - overload detected! No new data can be written to the queue.
 *               6  - overload scenario still detected.
 *               7  - VX1000TlIf_RequestDaqTxQueues() not successfully called yet (or) a transfer is still pending.
 *               11 - unspecified error
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_RequestDaqTxQueues must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220574
 *********************************************************************************************************************/
#  define VX1000TlIf_PrepareNextTxData                    VX1000TLIF_IMPL(VX1000TL_PREPARE_NEXT_TX_DATA, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_TransferPendingTxData()
 *********************************************************************************************************************/
/*! \brief       Macro to tranfer the pending data in a queue.
 *  \details     The application requests the VX1000 TL driver to transfer the pending data in a queue.
 *  \param[in]   queueId:         ID of the queue from which the data has to be transferred.
 *  \return      0  - pending data transferred successfully.
 *               4  - invalid queue ID.
 *               5  - overload detected! No new data can be written to the queue.
 *               6  - overload scenario still detected.
 *               7  - VX1000TlIf_PrepareNextTxData() not successfully called yet (or) there are no pending transfers.
 *               11 - unspecified error
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_PrepareNextTxData must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220575
 *********************************************************************************************************************/
#  define VX1000TlIf_TransferPendingData                  VX1000TLIF_IMPL(VX1000TL_TRANSFER_PENDING_DATA, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_TransferData()
 *********************************************************************************************************************/
/*! \brief       Macro to request data transfer.
 *  \details     The application requests the VX1000 TL driver to transfer the data stored in a buffer.
 *               Data is copied by the VX1000 TL driver, taking care of roll overs within the queue.
 *               The data can have any size between calls.
 *  \param[in]   queueId:         ID of the queue to which data has to be copied.
 *  \param[in]   dataToBeSent:    Pointer to the buffer that contains the data to be transferred.
 *  \param[in]   dataSizeBytes:   Size of data (in bytes). It must not exceed the numberOfReservedQueueBytes
 *                                of this queue ID.
 *  \return      0  - data transferred successfully.
 *               4  - invalid queue ID.
 *               5  - overload detected! No new data can be written to the queue.
 *               6  - overload scenario still detected.
 *               7  - VX1000TlIf_RequestDaqTxQueues() not successfully called yet.
 *               11 - unspecified error
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_RequestDaqTxQueues must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220576
 *********************************************************************************************************************/
#  define VX1000TlIf_TransferData                         VX1000TLIF_IMPL(VX1000TL_TRANSFER_DATA, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_ReleaseDaqTxQueues()
 *********************************************************************************************************************/
/*! \brief       Macro to release all allocated queues.
 *  \details     The application requests the VX1000 TL driver to release all queues that were previously allocated.
 *  \return      0  - data transferred successfully.
 *               7  - VX1000TlIf_RequestDaqTxQueues() not successfully called yet.
 *               11 - unspecified error.
 *               VX1000TLIF_RET_E_NOT_OK - while VX1000TlIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000TlIf_RequestDaqTxQueues must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220577
 *********************************************************************************************************************/
#  define VX1000TlIf_ReleaseDaqTxQueues                   VX1000TLIF_IMPL(VX1000TL_RELEASE_DAQ_TXQ, VX1000TLIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000TlIf_Cleanup()
 *********************************************************************************************************************/
/*! \brief       VX1000TlIf_Cleanup.
 *  \details     The application requests the VX1000 TL driver to start the cleanup process.
 *  \pre         VX1000TlIf_Init must have been called. 
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-282371
 *********************************************************************************************************************/
#  define VX1000TlIf_Cleanup                              VX1000TLIF_IMPL_VOID(VX1000TL_CLEANUP)

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

#  define   VX1000TLIF_START_SEC_VAR_ZERO_INIT_8BIT
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Initialization state of the module */
extern VAR(uint8, VX1000TLIF_VAR_ZERO_INIT) VX1000TlIf_State;

/*! Counter for errors that are caused by API calls when :
 *     - VX1000 Interface is in incorrect state (e.g. not initialized) 
 *     - or if VX1000If_IsVX1000DriverAccessEnabled returned FALSE 
 */
extern VAR(uint8, VX1000TLIF_VAR_ZERO_INIT) VX1000TlIf_ErrorCount;

#  define   VX1000TLIF_STOP_SEC_VAR_ZERO_INIT_8BIT
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#  define   VX1000TLIF_START_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  VX1000TlIf_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Function for *_INIT_*-variable initialization
 *  \details     Service to initialize module global variables at power up. This function initializes the
 *               variables in *_INIT_* sections. Used in case they are not initialized by the startup code.
 *  \pre         Module is uninitialized.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VX1000TLIF_CODE) VX1000TlIf_InitMemory(void);

/**********************************************************************************************************************
 * VX1000TlIf_Init()
 *********************************************************************************************************************/
/*! \brief       Initialization function
 *  \details     This function initializes the module VX1000TlIf. It initializes all variables and sets the module state
 *               to initialized.
 *  \pre         Interrupts are disabled.
 *  \pre         Module is uninitialized.
 *  \pre         VX1000TlIf_InitMemory has been called unless VX1000TlIf_State is initialized by start-up code.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220285
 *********************************************************************************************************************/
FUNC(void, VX1000TLIF_CODE) VX1000TlIf_Init(void);

#  define   VX1000TLIF_STOP_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* VX1000TlIf_IsAccessEnabled */
#endif /* !VX1000TLIF_H */

/**********************************************************************************************************************
 *  END OF FILE: VX1000TlIf.h
 *********************************************************************************************************************/
