/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  VX1000TlIf.c
 *        \brief  VX1000 TL Interface source file
 *
 *      \details  Implementation of an API wrapper between XCP Protocol Layer and proprietary VX1000 Transport Layer names.
 *
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VX1000TLIF_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "VX1000TlIf.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of VX1000If header file */
#if (  (VX1000TLIF_SW_MAJOR_VERSION != (1u)) \
    || (VX1000TLIF_SW_MINOR_VERSION != (0u)) \
    || (VX1000TLIF_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of VX1000TlIf.c and VX1000TlIf.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#define VX1000TLIF_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Initialization state of the module */
VAR(uint8, VX1000TLIF_VAR_ZERO_INIT) VX1000TlIf_State = VX1000TLIF_UNINIT; /* PRQA S 1504 */ /* MD_MSR_Rule8.7 */

/*! Counter for errors that are caused by API calls when :
 *     - VX1000 Interface is in incorrect state (e.g. not initialized) 
 *     - or if VX1000TlIf_IsVX1000DriverAccessEnabled returned FALSE 
 */
VAR(uint8, VX1000TLIF_VAR_ZERO_INIT) VX1000TlIf_ErrorCount = 0;

#define VX1000TLIF_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#define VX1000TLIF_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  VX1000TlIf_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VX1000TLIF_CODE) VX1000TlIf_InitMemory(void)
{
  /* ----- Implementation ----------------------------------------------- */
  VX1000TlIf_State = VX1000TLIF_UNINIT;

  VX1000TlIf_ErrorCount = 0;
}  /* VX1000TlIf_InitMemory() */

/**********************************************************************************************************************
 *  VX1000TlIf_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VX1000TLIF_CODE) VX1000TlIf_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  if ((VX1000TlIf_State == VX1000TLIF_UNINIT) && (VX1000TlIf_IsAccessEnabled == TRUE))
  {
    /* Check whether VX1000 TL driver intialization was successful and set component to initialized, otherwise increment error count.
     * Possible failure reasons during VX1000 TL initialization: 
     *  - Endpoint is not available (1)
     *  - Unspecified error         (11)
     */
    Std_ReturnType retVal = VX1000TL_INIT();
    if (retVal != E_OK)
    {
      VX1000TlIf_ErrorCount++;
    }
    else
    {
      VX1000TlIf_State = VX1000TLIF_INITIALIZED;
      VX1000TlIf_ErrorCount = 0;
    }
  }
} /* VX1000TlIf_Init() */

#define VX1000TLIF_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: VX1000TlIf.c
 *********************************************************************************************************************/
