/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**       \file
 *        \brief  vIpc_XCfg header file
 *
 *      \details  Auxiliary header file containing abstractions to generated data
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK in file vIpc.c
 *********************************************************************************************************************/

#ifndef VIPC_XCFG_H
# define VIPC_XCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "vIpc_Cfg.h"
#include "vIpc_Lcfg.h"
#include "vIpc_PBcfg.h"

/* PRQA S 1527 EOF */ /* MD_MSR_IdentifierLength */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 1 */ /* MD_MSR_FctLikeMacro */
#define vIpc_XCfg_GetInvalidTxConId() vIpc_XCfg_GetSizeOfTxConnections()

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

#if (VIPC_RXCONNECTIONS == STD_OFF)
/**   \brief  value based type definition for vIpc_RxConnectionsIdxOfAddress */
typedef uint8 vIpc_RxConnectionsIdxOfAddressType;
/**   \brief  value based type definition for vIpc_AddressEndIdxOfRxChannel */
typedef uint8 vIpc_AddressEndIdxOfRxChannelType;
/**   \brief  value based type definition for vIpc_AddressLengthOfRxChannel */
typedef uint8 vIpc_AddressLengthOfRxChannelType;
/**   \brief  value based type definition for vIpc_AddressStartIdxOfRxChannel */
typedef uint8 vIpc_AddressStartIdxOfRxChannelType;
/**   \brief  value based type definition for vIpc_MaxLengthOfRxChannel */
typedef uint16 vIpc_MaxLengthOfRxChannelType;
/**   \brief  value based type definition for vIpc_MaxLengthOfRxConnections */
typedef uint16 vIpc_MaxLengthOfRxConnectionsType;
/**   \brief  value based type definition for vIpc_RxAddressOfRxConnections */
typedef uint8 vIpc_RxAddressOfRxConnectionsType;
/**   \brief  value based type definition for vIpc_UpperLayerIdOfRxConnections */
typedef PduIdType vIpc_UpperLayerIdOfRxConnectionsType;
#endif

#if (VIPC_TXCONNECTIONS == STD_OFF)
/**   \brief  value based type definition for vIpc_SaturationMaskOfTxQueueElem */
typedef uint8 vIpc_SaturationMaskOfTxQueueElemType;
/**   \brief  value based type definition for vIpc_SizeOfTransmitLLFunctionTable */
typedef uint8 vIpc_SizeOfTransmitLLFunctionTableType;
/**   \brief  value based type definition for vIpc_SizeOfTxChannel */
typedef uint8 vIpc_SizeOfTxChannelType;
/**   \brief  value based type definition for vIpc_SizeOfTxConnections */
typedef uint8 vIpc_SizeOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_SizeOfTxQueueElem */
typedef uint8 vIpc_SizeOfTxQueueElemType;
/**   \brief  value based type definition for vIpc_SizeOfTxUlCallbacks */
typedef uint8 vIpc_SizeOfTxUlCallbacksType;
/**   \brief  value based type definition for vIpc_LowerLayerIdOfTxChannel */
typedef PduIdType vIpc_LowerLayerIdOfTxChannelType;
/**   \brief  value based type definition for vIpc_MaxLengthOfTxChannel */
typedef uint16 vIpc_MaxLengthOfTxChannelType;
/**   \brief  value based type definition for vIpc_NumConnectionsOfTxChannel */
typedef uint8 vIpc_NumConnectionsOfTxChannelType;
/**   \brief  value based type definition for vIpc_NumSepCyclesOfTxChannel */
typedef uint8 vIpc_NumSepCyclesOfTxChannelType;
/**   \brief  value based type definition for vIpc_MaxLengthOfTxConnections */
typedef uint16 vIpc_MaxLengthOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_PriorityOfTxConnections */
typedef uint8 vIpc_PriorityOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_TxAddressOfTxConnections */
typedef uint8 vIpc_TxAddressOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_TxChannelIdxOfTxConnections */
typedef uint8 vIpc_TxChannelIdxOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_TxQueueElemIdxOfTxConnections */
typedef uint8 vIpc_TxQueueElemIdxOfTxConnectionsType;
/**   \brief  value based type definition for vIpc_UpperLayerIdOfTxConnections */
typedef PduIdType vIpc_UpperLayerIdOfTxConnectionsType;
#endif


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vIpc_XCfg_DevErrorDetect()
 *********************************************************************************************************************/
/*! \brief       Checks if DevErrorDetect is enabled or not.
 *  \details     -
 *  \return      FALSE          The DevErrorDetect is disabled.
 *               TRUE           The DevErrorDetect is enabled.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_DevErrorDetect(void);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsVIpcInitialized()
 *********************************************************************************************************************/
/*! \brief       Checks if vIpc is initialized for all channels.
 *  \details     -
 *  \return      FALSE          The vIpc is not initialized.
 *               TRUE           The vIpc is initialized.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsVIpcInitialized(void);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsTxSduIdValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed Tx Sdu Id is valid or not.
 *  \details     -
 *  \param[in]   TxSduId The Sdu to check.
 *  \return      FALSE          The Id is invalid.
 *               TRUE           The Id is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsTxSduIdValid(PduIdType TxSduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsRxPduIdValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed Rx Pdu Id is valid or not.
 *  \details     -
 *  \param[in]   PduId The Pdu to check.
 *  \return      FALSE          The Id is invalid.
 *               TRUE           The Id is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsRxPduIdValid(PduIdType PduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsTxPduIdValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed Tx Pdu Id is valid or not.
 *  \details     -
 *  \param[in]   PduId The Pdu to check.
 *  \return      FALSE          The Id is invalid.
 *               TRUE           The Id is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsTxPduIdValid(PduIdType PduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsPduDataValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed Pdu data is valid or not.
 *  \details     -
 *  \param[in]   PduInfoPtr The pointer to check.
 *  \return      FALSE          The Pdu data is invalid.
 *               TRUE           The Pdu data is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsPduDataValid(CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsPduDataLenValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed Pdu data length is valid or not.
 *  \details     -
 *  \param[in]   PduInfoPtr The pointer to check.
 *  \return      FALSE          The Pdu data length is invalid.
 *               TRUE           The Pdu data length is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsPduDataLenValid(CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsBufSizePtrValid()
 *********************************************************************************************************************/
/*! \brief       Checks if passed buffer size pointer is valid or not.
 *  \details     -
 *  \param[in]   BufferSizePtr The pointer to check.
 *  \return      FALSE          The buffer size pointer is invalid.
 *               TRUE           The buffer size pointer is valid.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsBufSizePtrValid(CONSTP2CONST(PduLengthType, AUTOMATIC, VIPC_APPL_CONST) BufferSizePtr);

/**********************************************************************************************************************
 *  vIpc_XCfg_ReportError()
 *********************************************************************************************************************/
/*! \brief       Helper function that will report an error if Det is enabled and an error ID is passed.
 *  \details     -
 *  \param[in]   ApiId The api to use for reporting.
 *  \param[in]   ErrorId The error id to report.
 *  \return      E_OK           No error reported.
 *               E_NOT_OK       The specified error was reported.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(Std_ReturnType, VIPC_CODE) vIpc_XCfg_ReportError(vIpc_ServiceId ApiId, vIpc_ErrorCode ErrorId);

/**********************************************************************************************************************
 *  vIpc_XCfg_IsLLBusy()
 *********************************************************************************************************************/
/*! \brief       Checks if the LL of the Tx channel is currently busy.
 *  \details     -
 *  \param[in]   PduId The Pdu to check.
 *  \return      FALSE           The LL is idle.
 *               TRUE            The LL is busy.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsLLBusy(PduIdType PduId);


/*
 *  Tx Connection Data Getter and Setter
 */

/**********************************************************************************************************************
 *  vIpc_TxCon_GetState()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified Tx connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The current connection state of the specified Tx connection.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_TxConnectionStateType, VIPC_CODE) vIpc_TxCon_GetState(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxCon_SetState()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified Tx connection to the new value.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NextState The state to set.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetState(PduIdType TxConId,
                                                                  vIpc_TxConnectionStateType NextState);

/**********************************************************************************************************************
 *  vIpc_TxCon_GetRemPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Returns the remaining payload length to be transmitted of the specified Tx connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The current remaining payload length of the specified Tx connection.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxCon_GetRemPayloadLength(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxCon_SetRemPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Sets the remaining payload length of the specified Tx connection to the new value.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewLength The new length to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetRemPayloadLength(PduIdType TxConId,
                                                                             PduLengthType NewLength);

/**********************************************************************************************************************
 *  vIpc_TxCon_DecRemPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Decrements the remaining payload length of the specified Tx connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   DecLength The length of payload to decrement.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_DecRemPayloadLength(PduIdType TxConId,
                                                                             PduLengthType DecLength);

/**********************************************************************************************************************
 *  vIpc_TxCon_GetRemCompleteLength()
 *********************************************************************************************************************/
/*! \brief       Returns the remaining number of bytes to be transmitted.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The remaining number of bytes to be transmitted.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxCon_GetRemCompleteLength(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxCon_SetRemCompleteLength()
 *********************************************************************************************************************/
/*! \brief       Sets the remaining number of bytes to be transmitted.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewLengthType The number of bytes to set.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetRemCompleteLength(PduIdType TxConId,
                                                                              PduLengthType NewLengthType);

/**********************************************************************************************************************
 *  vIpc_TxCon_DecRemCompleteLength()
 *********************************************************************************************************************/
/*! \brief       Decrements the remaining number of bytes to be transmitted.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   DecLengthType The length to decrement of the remaining message.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_DecRemCompleteLength(PduIdType TxConId,
                                                                              PduLengthType DecLengthType);

/**********************************************************************************************************************
 *  vIpc_TxCon_GetMessageType()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_MessageType, VIPC_CODE) vIpc_TxCon_GetMessageType(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxCon_SetMessageType()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewMsgType The new type of message to sent.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetMessageType(PduIdType TxConId,
                                                                        vIpc_MessageType NewMsgType);

/**********************************************************************************************************************
 *  vIpc_TxCon_GetCurrentSegment()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_TxCon_GetCurrentSegment(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxCon_SetCurrentSegment()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NextSegment The next segment to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetCurrentSegment(PduIdType TxConId,
                                                                           uint8 NextSegment);

/**********************************************************************************************************************
 *  vIpc_TxCon_IncCurrentSegment()
 *********************************************************************************************************************/
/*! \brief       Increments the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   Increment The incrementation size.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_IncCurrentSegment(PduIdType TxConId,
                                                                           uint8 Increment);


/*
 *  Tx Channel Data Getter and Setter
 */

/**********************************************************************************************************************
 *  vIpc_TxChan_GetActiveTxConId()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxChan_GetActiveTxConId(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxChan_SetActiveTxConId()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   NextActiveConId The next connection to send.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *               requires NextActiveConId <= vIpc_XCfg_GetSizeOfTxConnections();
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetActiveTxConId(PduIdType TxChanId, PduIdType NextActiveConId);

/**********************************************************************************************************************
 *  vIpc_TxChan_GetIsActiveTxConAvailable()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_TxChan_GetIsActiveTxConAvailable(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxChan_SetActiveTxConAvailable()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   State The state to set.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetActiveTxConAvailable(PduIdType TxChanId, boolean State);

/**********************************************************************************************************************
 *  vIpc_TxChan_GetAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxChan_GetAlreadySentDataOfCurrSeg(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxChan_SetAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   NewAlreadySentData The length that's already been sent.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetAlreadySentDataOfCurrSeg(PduIdType TxChanId,
                                                                                      PduLengthType NewAlreadySentData);

/**********************************************************************************************************************
 *  vIpc_TxChan_IncAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*! \brief       Increments the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   Increment The length to increment.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_IncAlreadySentDataOfCurrSeg(PduIdType TxChanId,
                                                                                      PduLengthType Increment);

/**********************************************************************************************************************
 *  vIpc_TxChan_GetRemainingSeparationCycles()
 *********************************************************************************************************************/
/*! \brief       Gets the remaining number of separation cycles for the specified Tx channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The remaining number of separation cycles.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxChan_GetRemainingSeparationCycles(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxChan_SetRemainingSeparationCycles()
 *********************************************************************************************************************/
/*! \brief       Sets the remaining number of separation cycles for the specified Tx channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   NewValue The new value to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetRemainingSeparationCycles(PduIdType TxChanId, uint16 NewValue);

/**********************************************************************************************************************
 *  vIpc_TxChan_InitRemainingSeparationCycles()
 *********************************************************************************************************************/
/*! \brief       Resets the remaining number of separation cycles for the specified Tx channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_InitRemainingSeparationCycles(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxChan_DecRemainingSeparationCycles()
 *********************************************************************************************************************/
/*! \brief       Decrements the remaining number of separation cycles for the specified Tx channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_DecRemainingSeparationCycles(PduIdType TxChanId);



/*
 *  Tx Queue Data Getter and Setter
 */

/**********************************************************************************************************************
 *  vIpc_TxQ_GetConnectionId()
 *********************************************************************************************************************/
/*! \brief       Gets the queue connection id.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The queue connection id.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetConnectionId(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxQ_GetNextHigherIdx()
 *********************************************************************************************************************/
/*! \brief       Gets the next higher idx of the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The queue connection id.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetNextHigherIdx(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxQ_SetNextHigherIdx()
 *********************************************************************************************************************/
/*! \brief       Sets the next higher idx to the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewHigherIdx The index to set.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *               requires NewHigherIdx <= vIpc_XCfg_GetSizeOfTxConnections();
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetNextHigherIdx(PduIdType TxConId, PduIdType NewHigherIdx);

/**********************************************************************************************************************
 *  vIpc_TxQ_GetNextLowerIdx()
 *********************************************************************************************************************/
/*! \brief       Gets the next lower idx of the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The queue idx.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetNextLowerIdx(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxQ_SetNextLowerIdx()
 *********************************************************************************************************************/
/*! \brief       Sets the next lower idx to the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewLowerIdx The index to set.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *               requires NewLowerIdx <= vIpc_XCfg_GetSizeOfTxConnections();
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetNextLowerIdx(PduIdType TxConId, PduIdType NewLowerIdx);

/**********************************************************************************************************************
 *  vIpc_TxQ_GetPrio()
 *********************************************************************************************************************/
/*! \brief       Gets the priority of the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \return      The priority.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_PrioType, VIPC_CODE) vIpc_TxQ_GetPrio(PduIdType TxConId);

/**********************************************************************************************************************
 *  vIpc_TxQ_SetPrio()
 *********************************************************************************************************************/
/*! \brief       Sets the priority of the connection.
 *  \details     -
 *  \param[in]   TxConId The connection id to query with.
 *  \param[in]   NewPrio The new priority to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetPrio(PduIdType TxConId, vIpc_PrioType NewPrio);

/**********************************************************************************************************************
 *  vIpc_TxQ_InitData()
 *********************************************************************************************************************/
/*! \brief       Initialize all queue entries
 *  \details     -
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_InitData(void);



/*
 *  Tx Queue Info Data Getter and Setter
 */
/**********************************************************************************************************************
 *  vIpc_TxQI_GetHighPrioIdx()
 *********************************************************************************************************************/
/*! \brief       Gets the high priority index of the channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The priority.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQI_GetHighPrioIdx(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxQI_SetHighPrioIdx()
 *********************************************************************************************************************/
/*! \brief       Sets the high priority index of the channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   NewHighPrioIdx The new priority idx to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_SetHighPrioIdx(PduIdType TxChanId, PduIdType NewHighPrioIdx);

/**********************************************************************************************************************
 *  vIpc_TxQI_GetLowPrioIdx()
 *********************************************************************************************************************/
/*! \brief       Gets the low priority index of the channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \return      The priority.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQI_GetLowPrioIdx(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_TxQI_SetLowPrioIdx()
 *********************************************************************************************************************/
/*! \brief       Sets the low priority index of the channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \param[in]   NewLowPrioIdx The new priority idx to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_SetLowPrioIdx(PduIdType TxChanId, PduIdType NewLowPrioIdx);

/**********************************************************************************************************************
 *  vIpc_TxQI_InitHighAndLowPrioIdx()
 *********************************************************************************************************************/
/*! \brief       Initializes the high & low priority indices of the channel.
 *  \details     -
 *  \param[in]   TxChanId The channel id to query with.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxPduIdValid(TxChanId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_InitHighAndLowPrioIdx(PduIdType TxChanId);



/*
 *  Rx Connection Data Getter and Setter
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxCon_State()
 *********************************************************************************************************************/
/*! \brief       Returns the current connection state of the specified SDU.
 *  \details     -
 *  \param[in]   RxSduId The SDU to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_RxSduStateType, VIPC_CODE) vIpc_XCfg_GetRxCon_State(PduIdType RxSduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_SetRxCon_State()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection state of the specified SDU to the new value.
 *  \details     -
 *  \param[in]   RxSduId The SDU to query with.
 *  \param[in]   NextState The next state to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsRxPduIdValid(RxSduId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxCon_State(PduIdType RxSduId,
                                                                  vIpc_RxSduStateType NextState);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxPduIsHeaderSent()
 *********************************************************************************************************************/
/*! \brief       Checks if the header of the PDU has already been sent.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \return      true if the header of the PDU has already been sent.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_GetRxPduIsHeaderSent(PduIdType RxPduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_SetRxPduIsHeaderSent()
 *********************************************************************************************************************/
/*! \brief       Sets the value to check if the header of the PDU has already been sent.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   IsSentFlag The flat to indicate if the header has been sent.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires RxPduId < vIpc_XCfg_GetSizeOfRxChannel();
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPduIsHeaderSent(PduIdType RxPduId, boolean IsSentFlag);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxPduConnIdx()
 *********************************************************************************************************************/
/*! \brief       Returns the connection idx of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \return      The current connection state of the specified SDU.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetRxPduConnId(PduIdType RxPduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_SetRxPduConnIdx()
 *********************************************************************************************************************/
/*! \brief       Sets the current connection idx of a Rx connection.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   NewConnId The new connection ID to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires (RxPduId < vIpc_XCfg_GetSizeOfRxChannel() && vIpc_XCfg_IsRxPduIdValid(NewConnId));
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPduConnId(PduIdType RxPduId, PduIdType NewConnId);


/**********************************************************************************************************************
 *  vIpc_XCfg_SetPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Set the payload length of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   Length The length to use for the channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPayloadLength(PduIdType RxPduId, uint32 Length);


/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxRemainingPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Get the remaining payload length of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \return      The remaining payload length of a Rx channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(uint32, VIPC_CODE) vIpc_XCfg_GetRxRemainingPayloadLength(PduIdType RxPduId);


/**********************************************************************************************************************
 *  vIpc_XCfg_SetRxRemainingPayloadLength()
 *********************************************************************************************************************/
/*! \brief       Set the remaining payload length of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   Length The length to use for the payload.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxRemainingPayloadLength(PduIdType RxPduId, uint32 Length);


/**********************************************************************************************************************
 *  vIpc_XCfg_GetSeqCtr()
 *********************************************************************************************************************/
/*! \brief       Get the sequence counter of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \return      The sequence counter of a Rx channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetSeqCtr(PduIdType RxPduId);


/**********************************************************************************************************************
 *  vIpc_XCfg_SetSeqCtr()
 *********************************************************************************************************************/
/*! \brief       Sets the sequence counter of a Rx connection.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   Ctr The sequence counter to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetSeqCtr(PduIdType RxPduId,
                                                             uint8 Ctr);


/**********************************************************************************************************************
 *  vIpc_XCfg_GetFrameType()
 *********************************************************************************************************************/
/*! \brief       Get the frame type of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \return      The frame type of a Rx channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetFrameType(PduIdType RxPduId);


/**********************************************************************************************************************
 *  vIpc_XCfg_SetFrameType()
 *********************************************************************************************************************/
/*! \brief       Set the frame type of a Rx channel.
 *  \details     -
 *  \param[in]   RxPduId The PDU to query with.
 *  \param[in]   Type The frame type to use.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *  \endspec
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetFrameType(PduIdType RxPduId,
                                                                uint8 Type);

/*
 *  ComStackLib abstraction Rx Channels
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetAddressLengthOfRxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the address length of the specified Rx channel.
 *  \details     -
 *  \param[in]   Index The Rx channel index to use.
 *  \return      The address length of the specified Rx channel
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_AddressLengthOfRxChannelType, VIPC_CODE) vIpc_XCfg_GetAddressLengthOfRxChannel(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetAddressStartIdxOfRxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the address start index of the specified Rx channel.
 *  \details     -
 *  \param[in]   Index The Rx channel index to use.
 *  \return      The address start index of the specified Rx channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_AddressStartIdxOfRxChannelType, VIPC_CODE) vIpc_XCfg_GetAddressStartIdxOfRxChannel(PduIdType Index);

/*
 *  ComStackLib abstraction Rx Connections
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxAddressOfRxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the Rx address of the specified Rx channel.
 *  \details     -
 *  \param[in]   Index The Rx connection index to use.
 *  \return      The Rx address of the specified Rx channel.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_RxAddressOfRxConnectionsType, VIPC_CODE) vIpc_XCfg_GetRxAddressOfRxConnections(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetUpperLayerIdOfRxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the upper layer index of the specified Rx connection.
 *  \details     -
 *  \param[in]   Index The Rx connection index to use.
 *  \return      The upper layer index of the specified Rx connection.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_UpperLayerIdOfRxConnectionsType, VIPC_CODE) vIpc_XCfg_GetUpperLayerIdOfRxConnections(PduIdType Index);


/*
 *  ComStackLib abstraction Rx Function Pointer Tables
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks()
 *********************************************************************************************************************/
/*! \brief       Returns the CopyRxData callback for the specified upper layer.
 *  \details     -
 *  \param[in]   Index The Rx connection index to use.
 *  \return      The CopyRxData callback.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_CopyRxDataFctPtrType, VIPC_CODE) vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxIndicationOfRxUlCallbacks()
 *********************************************************************************************************************/
/*! \brief       Returns the RxIndication callback for the specified upper layer.
 *  \details     -
 *  \param[in]   Index The Rx connection index to use.
 *  \return      The RxIndication callback.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_RxIndicationFctPtrType, VIPC_CODE) vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks()
 *********************************************************************************************************************/
/*! \brief       Returns the StartOfReception callback for the specified upper layer.
 *  \details     -
 *  \param[in]   Index The Rx connection index to use.
 *  \return      The StartOfReception callback.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_StartOfReceptionFctPtrType, VIPC_CODE) vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks(PduIdType Index);


/*
 *  ComStackLib abstraction Rx Data Size
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetRxConnectionsIdxOfAddress()
 *********************************************************************************************************************/
/*! \brief       Returns the Rx connections for the specified Address.
 *  \details     -
 *  \param[in]   Index The Address index to use.
 *  \return      The Rx connection.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_RxConnectionsIdxOfAddressType, VIPC_CODE) vIpc_XCfg_GetRxConnectionsIdxOfAddress(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetSizeOfRxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the number of Rx channels.
 *  \details     -
 *  \return      The number of Rx channels.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfRxChannel(void);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetSizeOfRxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the number of Rx connections.
 *  \details     -
 *  \return      The number of Rx connections.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfRxConnections(void);


/*
 *  ComStackLib abstraction Tx Function Pointer Tables
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable()
 *********************************************************************************************************************/
/*! \brief       Returns the Transmit function for the specified lower layer.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The Transmit function.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_TransmitFctPtrType, VIPC_CODE) vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks()
 *********************************************************************************************************************/
/*! \brief       Returns the TxConfirmation function for the specified upper layer.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The TxConfirmation function.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_TxConfirmationFctPtrType, VIPC_CODE) vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks()
 *********************************************************************************************************************/
/*! \brief       Returns the CopyTxData function for the specified upper layer.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The CopyTxData function.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_CopyTxDataFctPtrType, VIPC_CODE) vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks(PduIdType Index);


/*
 *  ComStackLib abstraction Tx Connections
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetTxAddressOfTxConnection()
 *********************************************************************************************************************/
/*! \brief       Returns the address to identify the corresponding SDU ID on Tx side.
 *  \details     -
 *  \param[in]   SduId         The connection Id.
 *  \return      The calculated target address.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetTxAddressOfTxConnection(PduIdType SduId);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetUpperLayerIdOfTxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the upper layer index of the specified Tx connection.
 *  \details     -
 *  \param[in]   Index The Tx connection index to use.
 *  \return      The calculated upper layer index.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_UpperLayerIdOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetUpperLayerIdOfTxConnections(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetTxChannelIdxOfTxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the Tx channel index of the specified Tx connection.
 *  \details     -
 *  \param[in]   Index The Tx connection index to use.
 *  \return      The calculated Tx channel index.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_TxChannelIdxOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetTxChannelIdxOfTxConnections(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetAddressOfTxConnection()
 *********************************************************************************************************************/
/*! \brief       Returns the address of the specified Tx connection.
 *  \details     -
 *  \param[in]   Index The Tx connection index to use.
 *  \return      The calculated address.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_TxAddressOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetAddressOfTxConnection(PduIdType Index); /* PRQA S 0779 */ /* MD_MSR_5.1_779 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetPriorityOfTxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the priority of the specified Tx connection.
 *  \details     -
 *  \param[in]   Index The Tx connection index to use.
 *  \return      The calculated priority.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_PriorityOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetPriorityOfTxConnections(PduIdType Index);


/*
 *  ComStackLib abstraction Tx Channels
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetNumSepCyclesOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the number of separation cycles of the specified Tx channel.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The calculated number of separation cycles.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_NumSepCyclesOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetNumSepCyclesOfTxChannel(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetNumConnectionsOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the number of connections of the specified Tx channel.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The calculated number of connections.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_NumConnectionsOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetNumConnectionsOfTxChannel(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetMaxLengthOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the maximum length of the specified Tx channel.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The calculated maximum length.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_MaxLengthOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetMaxLengthOfTxChannel(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetLowerLayerIdOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the lower layer index of the specified Tx channel.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The calculated lower layer index.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_LowerLayerIdOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetLowerLayerIdOfTxChannel(PduIdType Index);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetAlgorithmOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the scheduling algorithm of the specified Tx channel.
 *  \details     -
 *  \param[in]   Index The Tx channel index to use.
 *  \return      The calculated scheduling algorithm.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(vIpc_Algorithm, VIPC_CODE) vIpc_XCfg_GetAlgorithmOfTxChannel(PduIdType Index);


/*
 *  ComStackLib abstraction Tx Data Size
 */

/**********************************************************************************************************************
 *  vIpc_XCfg_GetSizeOfTxChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the number of Tx channels.
 *  \details     -
 *  \return      The number of Tx channels.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfTxChannel(void);

/**********************************************************************************************************************
 *  vIpc_XCfg_GetSizeOfTxConnections()
 *********************************************************************************************************************/
/*! \brief       Returns the number of Tx Connections.
 *  \details     -
 *  \return      The number of Tx Connections.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfTxConnections(void);

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#ifndef VIPC_UNIT_TEST  /* COV_VIPC_EXCLUDED_FEATURES TX */
# include "vIpc_XCfgImpl.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#endif

#endif /* VIPC_XCFG_H */
