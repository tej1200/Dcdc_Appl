/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**       \file
 *        \brief  vIpc_XCfg src file
 *
 *      \details  Auxiliary src file containing abstractions to generated data
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK in file vIpc.c
 *********************************************************************************************************************/

#ifndef VIPC_XCFG_IMPL_H
# define VIPC_XCFG_IMPL_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpc_Types.h"
#include "vIpc_Priv.h"

#if (VIPC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/* PRQA S 3219 EOF */ /* MD_MSR_Unreachable */

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define VIPC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(vIpc_IsInitialized, VIPC_VAR_ZERO_INIT) vIpc_InitStatus;

#define VIPC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vIpc_XCfg_DevErrorDetect()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_DevErrorDetect(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Det detection is enabled */
  return (boolean)(VIPC_DEV_ERROR_DETECT == STD_ON); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
} /* vIpc_XCfg_DevErrorDetect */

/**********************************************************************************************************************
 * vIpc_XCfg_IsVIpcInitialized()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsVIpcInitialized(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if vIpc is initialized. */
  return (boolean)(vIpc_InitStatus == VIPC_INITIALIZED); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
} /* vIpc_XCfg_IsVIpcInitialized */

/**********************************************************************************************************************
 * vIpc_XCfg_IsTxSduIdValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsTxSduIdValid(PduIdType TxSduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Tx Connection Id is valid. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return (boolean)(TxSduId < vIpc_GetSizeOfTxConnections());
#else
  VIPC_DUMMY_STATEMENT(TxSduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (boolean)(FALSE); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_XCfg_IsTxSduIdValid */

/**********************************************************************************************************************
 * vIpc_XCfg_IsRxPduIdValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsRxPduIdValid(PduIdType PduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Rx Pdu Id is valid. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return (boolean)(PduId < vIpc_GetSizeOfRxConnections());
#else
  VIPC_DUMMY_STATEMENT(PduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (boolean)(FALSE); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_XCfg_IsRxPduIdValid */

/**********************************************************************************************************************
 * vIpc_XCfg_IsTxPduIdValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsTxPduIdValid(PduIdType PduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Tx Pdu Id is valid. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return (boolean)(PduId < vIpc_GetSizeOfTxChannel());
#else
  VIPC_DUMMY_STATEMENT(PduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (boolean)(FALSE); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_XCfg_IsTxPduIdValid */

/**********************************************************************************************************************
 * vIpc_XCfg_IsPduDataValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsPduDataValid(CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Pdu Data is valid. */
  return (boolean)((PduInfoPtr != NULL_PTR) && (PduInfoPtr->SduDataPtr != NULL_PTR));
} /* vIpc_XCfg_IsPduDataValid */

/**********************************************************************************************************************
 * vIpc_XCfg_IsPduDataLenValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsPduDataLenValid(CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Pdu Data length is valid. */
  return (boolean)((PduInfoPtr != NULL_PTR) && (PduInfoPtr->SduLength > 0u));
} /* vIpc_XCfg_IsPduDataLenValid */

/**********************************************************************************************************************
 * vIpc_XCfg_IsBufSizePtrValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsBufSizePtrValid(CONSTP2CONST(PduLengthType, AUTOMATIC, VIPC_APPL_CONST) BufferSizePtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if Buffer Size Pointer is valid. */
  return (boolean)(BufferSizePtr != NULL_PTR);
} /* vIpc_XCfg_IsBufSizePtrValid */

/**********************************************************************************************************************
 * vIpc_XCfg_ReportError()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(Std_ReturnType, VIPC_CODE) vIpc_XCfg_ReportError(vIpc_ServiceId ApiId, vIpc_ErrorCode ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;
  VIPC_DUMMY_STATEMENT(ApiId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(ErrorId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* ----- Development Error Report ------------------------------------- */
  /* #10 Reports an DET error if one is occurred. */
#if (VIPC_DEV_ERROR_REPORT == STD_ON)
  if (ErrorId != VIPC_E_NO_ERROR)
  {
    (void) Det_ReportError(VIPC_MODULE_ID, VIPC_INSTANCE_ID, ApiId, ErrorId);
    retVal = E_NOT_OK;
  }
#endif
  return retVal;
} /* vIpc_XCfg_ReportError */

/**********************************************************************************************************************
 * vIpc_XCfg_IsLLBusy()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_IsLLBusy(PduIdType PduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if the LL is busy. */
  return (boolean)(vIpc_TxChan_GetIsActiveTxConAvailable(PduId) == TRUE); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
} /* vIpc_XCfg_IsLLBusy */


/**********************************************************************************************************************
 * vIpc_XCfg_GetTxAddressOfTxConnection()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetTxAddressOfTxConnection(PduIdType SduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the address of Tx connection. */
  return vIpc_XCfg_GetAddressOfTxConnection(SduId);
} /* vIpc_XCfg_GetTxAddressOfTxConnection */


/*
 *  Tx Connection Data Getter and Setter
 */

/**********************************************************************************************************************
 * vIpc_TxCon_GetState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_TxConnectionStateType, VIPC_CODE) vIpc_TxCon_GetState(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the state of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxConnectionInfo[TxConId].SduState;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return VIPC_TX_CON_STATE_IDLE; /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_TxCon_GetState */

/**********************************************************************************************************************
 * vIpc_TxCon_SetState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetState(PduIdType TxConId,
                                                                  vIpc_TxConnectionStateType NextState)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the state of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].SduState = NextState;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NextState); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_SetState */

/**********************************************************************************************************************
 * vIpc_TxCon_GetRemPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxCon_GetRemPayloadLength(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the remaining payload length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxConnectionInfo[TxConId].RemainingPayloadLength;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return(0u);
#endif
} /* vIpc_TxCon_GetRemPayloadLength */

/**********************************************************************************************************************
 * vIpc_TxCon_SetRemPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetRemPayloadLength(PduIdType TxConId,
                                                                             PduLengthType NewLength)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the remaining payload length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].RemainingPayloadLength = NewLength;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_SetRemPayloadLength */

/**********************************************************************************************************************
 * vIpc_TxCon_DecRemPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_DecRemPayloadLength(PduIdType TxConId,
                                                                             PduLengthType DecLength)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Decrement the remaining payload length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].RemainingPayloadLength = (PduLengthType)
      (vIpc_TxConnectionInfo[TxConId].RemainingPayloadLength - DecLength);
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(DecLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_DecRemPayloadLength */

/**********************************************************************************************************************
 * vIpc_TxCon_GetRemCompleteLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxCon_GetRemCompleteLength(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the remaining complete data length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxConnectionInfo[TxConId].RemainingCompleteLength;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxCon_GetRemCompleteLength */

/**********************************************************************************************************************
 * vIpc_TxCon_SetRemCompleteLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetRemCompleteLength(PduIdType TxConId,
                                                                              PduLengthType NewLengthType)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the remaining complete data length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].RemainingCompleteLength = NewLengthType;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewLengthType); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_SetRemCompleteLength */

/**********************************************************************************************************************
 * vIpc_TxCon_DecRemCompleteLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_DecRemCompleteLength(PduIdType TxConId,
                                                                              PduLengthType DecLengthType)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Decrement the remaining complete data length of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].RemainingCompleteLength = vIpc_TxConnectionInfo[TxConId].RemainingCompleteLength
                                                           - DecLengthType;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(DecLengthType); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_DecRemCompleteLength */

/**********************************************************************************************************************
 * vIpc_TxCon_GetMessageType()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_MessageType, VIPC_CODE) vIpc_TxCon_GetMessageType(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the message type of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxConnectionInfo[TxConId].MessageType;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return VIPC_MSG_SF;
#endif
} /* vIpc_TxCon_GetMessageType */

/**********************************************************************************************************************
 * vIpc_TxCon_SetMessageType()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetMessageType(PduIdType TxConId,
                                                                       vIpc_MessageType NewMsgType)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the message type of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].MessageType = NewMsgType;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewMsgType); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_SetMessageType */

/**********************************************************************************************************************
 * vIpc_TxCon_GetCurrentSegment()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_TxCon_GetCurrentSegment(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the current active segment of transmission of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxConnectionInfo[TxConId].CurrentSegment;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxCon_GetCurrentSegment */

/**********************************************************************************************************************
 * vIpc_TxCon_SetCurrentSegment()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_SetCurrentSegment(PduIdType TxConId,
                                                                          uint8 NextSegment)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the current active segment of transmission of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].CurrentSegment = NextSegment;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NextSegment); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_SetCurrentSegment */

/**********************************************************************************************************************
 * vIpc_TxCon_IncCurrentSegment()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxCon_IncCurrentSegment(PduIdType TxConId,
                                                                           uint8 Increment)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Increment the current active segment of transmission of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxConnectionInfo[TxConId].CurrentSegment = (uint8)(vIpc_TxConnectionInfo[TxConId].CurrentSegment + Increment);
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Increment); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxCon_IncCurrentSegment */


/*
 *  Tx Channel Data Getter and Setter
 */

/**********************************************************************************************************************
 * vIpc_TxChan_GetActiveTxConId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxChan_GetActiveTxConId(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the active connection of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxChannelInfo[TxChanId].ActiveTxConId;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxChan_GetActiveTxConId */

/**********************************************************************************************************************
 * vIpc_TxChan_SetActiveTxConId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetActiveTxConId(PduIdType TxChanId, PduIdType NextActiveConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the active connection of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].ActiveTxConId = NextActiveConId;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NextActiveConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_SetActiveTxConId */

/**********************************************************************************************************************
 * vIpc_XCfg_GetTxChanIsActiveTxConAvailable()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_TxChan_GetIsActiveTxConAvailable(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if there is an active connection of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxChannelInfo[TxChanId].IsActiveTxConAvailable;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return FALSE; /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_XCfg_GetTxChanIsActiveTxConAvailable */

/**********************************************************************************************************************
 * vIpc_TxChan_SetActiveTxConAvailable()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetActiveTxConAvailable(PduIdType TxChanId, boolean State)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets if there is an active connection of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].IsActiveTxConAvailable = State;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(State); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_SetActiveTxConAvailable */

/**********************************************************************************************************************
 * vIpc_TxChan_GetAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxChan_GetAlreadySentDataOfCurrSeg(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the already transmitted data of current segment of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxChannelInfo[TxChanId].AlreadySentDataOfCurrSeg;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxChan_GetAlreadySentDataOfCurrSeg */

/**********************************************************************************************************************
 * vIpc_TxChan_SetAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetAlreadySentDataOfCurrSeg(PduIdType TxChanId,
                                                                                      PduLengthType NewAlreadySentData)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the already transmitted data of current segment of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].AlreadySentDataOfCurrSeg = NewAlreadySentData;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewAlreadySentData); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_SetAlreadySentDataOfCurrSeg */

/**********************************************************************************************************************
 * vIpc_TxChan_IncAlreadySentDataOfCurrSeg()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_IncAlreadySentDataOfCurrSeg(PduIdType TxChanId,
                                                                                      PduLengthType Increment)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Increments the already transmitted data of current segment of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].AlreadySentDataOfCurrSeg += (PduLengthType)Increment;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Increment); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_IncAlreadySentDataOfCurrSeg */

/**********************************************************************************************************************
 * vIpc_TxChan_GetRemainingSeparationCycles()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_TxChan_GetRemainingSeparationCycles(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the remaining separation cycles of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_TxChannelInfo[TxChanId].RemainingSeparationCycles;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxChan_GetRemainingSeparationCycles */

/**********************************************************************************************************************
 * vIpc_TxChan_SetRemainingSeparationCycles()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_SetRemainingSeparationCycles(PduIdType TxChanId, uint16 NewValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the remaining separation cycles of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].RemainingSeparationCycles = NewValue;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewValue); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_SetRemainingSeparationCycles */

/**********************************************************************************************************************
 * vIpc_TxChan_InitRemainingSeparationCycles()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_InitRemainingSeparationCycles(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initializes the remaining separation cycles of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].RemainingSeparationCycles = vIpc_GetNumSepCyclesOfTxChannel(TxChanId);
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_InitRemainingSeparationCycles */

/**********************************************************************************************************************
 * vIpc_TxChan_DecRemainingSeparationCycles()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxChan_DecRemainingSeparationCycles(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Decrements the remaining separation cycles of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_TxChannelInfo[TxChanId].RemainingSeparationCycles--;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxChan_DecRemainingSeparationCycles */


/*
 *  Tx Queue Data Getter and Setter
 */
/**********************************************************************************************************************
 * vIpc_TxQ_GetConnectionId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetConnectionId(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the queue connection Id of Tx connection. */  
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxQueueElem(TxConId).Q_ConnectionId;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxQ_GetConnectionId */


/**********************************************************************************************************************
 * vIpc_TxQ_GetNextHigherIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetNextHigherIdx(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the queue index to next higher priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxQueueElem(TxConId).Q_NextHigherIdx;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxQ_GetNextHigherIdx */

/**********************************************************************************************************************
 * vIpc_TxQ_SetNextHigherIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetNextHigherIdx(PduIdType TxConId, PduIdType NewHigherIdx)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the queue index to next higher priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_GetTxQueueElem(TxConId).Q_NextHigherIdx = NewHigherIdx;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewHigherIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQ_SetNextHigherIdx */

/**********************************************************************************************************************
 * vIpc_TxQ_GetNextLowerIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQ_GetNextLowerIdx(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the queue index to next lower priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxQueueElem(TxConId).Q_NextLowerIdx;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxQ_GetNextLowerIdx */

/**********************************************************************************************************************
 * vIpc_TxQ_SetNextLowerIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetNextLowerIdx(PduIdType TxConId, PduIdType NewLowerIdx)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the queue index to next lower priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_GetTxQueueElem(TxConId).Q_NextLowerIdx = NewLowerIdx;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewLowerIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQ_SetNextLowerIdx */

/**********************************************************************************************************************
 * vIpc_TxQ_GetPrio()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_PrioType, VIPC_CODE) vIpc_TxQ_GetPrio(PduIdType TxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the queue priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxQueueElem(TxConId).Q_Prio;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_TxQ_GetPrio */

/**********************************************************************************************************************
 * vIpc_TxQ_SetPrio()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_SetPrio(PduIdType TxConId, vIpc_PrioType NewPrio)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the queue priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_GetTxQueueElem(TxConId).Q_Prio = NewPrio;
#else
  VIPC_DUMMY_STATEMENT(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewPrio); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQ_SetPrio */


/**********************************************************************************************************************
 * vIpc_TxQ_InitData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQ_InitData(void)
{
#if (VIPC_TXCONNECTIONS == STD_ON)
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType overallNumberOfTxCons = vIpc_XCfg_GetSizeOfTxConnections();
  PduIdType connectionIds;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initializes queue entries of all Tx connections. */
  for (connectionIds = 0; connectionIds < overallNumberOfTxCons; connectionIds++)
  {
    vIpc_GetTxQueueElem(connectionIds).Q_ConnectionId = connectionIds; /* PRQA S 2985 3 */ /* MD_MSR_ConstantCondition */
    vIpc_GetTxQueueElem(connectionIds).Q_NextLowerIdx = overallNumberOfTxCons;
    vIpc_GetTxQueueElem(connectionIds).Q_NextHigherIdx = overallNumberOfTxCons;
    vIpc_GetTxQueueElem(connectionIds).Q_Prio = 0;
  }
#endif
} /* vIpc_TxQ_InitData */


/*
 *  Tx Queue Info Data Getter and Setter
 */
/**********************************************************************************************************************
 * vIpc_TxQI_GetHighPrioIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQI_GetHighPrioIdx(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the high priority index of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_QueueInfo[TxChanId].QI_HighPrioIdx;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (PduIdType)0u;
#endif
} /* vIpc_TxQI_GetHighPrioIdx */

/**********************************************************************************************************************
 * vIpc_TxQI_GetHighPrioIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_SetHighPrioIdx(PduIdType TxChanId, PduIdType NewHighPrioIdx)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the high priority index of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_QueueInfo[TxChanId].QI_HighPrioIdx = NewHighPrioIdx;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewHighPrioIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQI_GetHighPrioIdx */

/**********************************************************************************************************************
 * vIpc_TxQI_GetLowPrioIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_TxQI_GetLowPrioIdx(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the low priority index of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_QueueInfo[TxChanId].QI_LowPrioIdx;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (PduIdType)0u;
#endif
} /* vIpc_TxQI_GetLowPrioIdx */

/**********************************************************************************************************************
 * vIpc_TxQI_SetLowPrioIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_SetLowPrioIdx(PduIdType TxChanId, PduIdType NewLowPrioIdx)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the low priority index of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  vIpc_QueueInfo[TxChanId].QI_LowPrioIdx = NewLowPrioIdx;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewLowPrioIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQI_SetLowPrioIdx */

/**********************************************************************************************************************
 * vIpc_TxQI_InitHighAndLowPrioIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_TxQI_InitHighAndLowPrioIdx(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initializes the high and low queue pointer of all Tx channels. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  PduIdType initIdx = (vIpc_ConAddressType)vIpc_XCfg_GetSizeOfTxConnections();
  vIpc_QueueInfo[TxChanId].QI_HighPrioIdx = initIdx;
  vIpc_QueueInfo[TxChanId].QI_LowPrioIdx = initIdx;
#else
  VIPC_DUMMY_STATEMENT(TxChanId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_TxQI_SetLowPrioIdx */


/*
 *  Rx Data Getter and Setter
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRxPduIsHeaderSent()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_XCfg_GetRxPduIsHeaderSent(PduIdType RxPduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns if the header of the PDU has already been sent. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxChannelInfo[RxPduId].vIpc_RxPduHeaderIsSent;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (boolean)FALSE; /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
#endif
} /* vIpc_XCfg_GetRxPduIsHeaderSent */

/**********************************************************************************************************************
 * vIpc_XCfg_SetRxPduIsHeaderSent()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPduIsHeaderSent(PduIdType RxPduId, boolean IsSentFlag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets if the header of the PDU has already been sent. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxChannelInfo[RxPduId].vIpc_RxPduHeaderIsSent = IsSentFlag;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(IsSentFlag); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduIsHeaderSent */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRxPduConnIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetRxPduConnId(PduIdType RxPduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the active connection Id of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxChannelInfo[RxPduId].vIpc_RxPduConnId;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (PduIdType)0u;
#endif
} /* vIpc_XCfg_GetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_SetRxPduConnIdx()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPduConnId(PduIdType RxPduId, PduIdType NewConnId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the active connection Id of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxChannelInfo[RxPduId].vIpc_RxPduConnId = NewConnId;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NewConnId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_SetPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxPayloadLength(PduIdType RxPduId, uint32 Length)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the payload length of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxConnectionInfo[RxPduId].PayloadLength = Length;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Length); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRemainingPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(uint32, VIPC_CODE) vIpc_XCfg_GetRxRemainingPayloadLength(PduIdType RxPduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the remaining payload length of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxConnectionInfo[RxPduId].RemainingPayloadLength;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_XCfg_GetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_SetRemainingPayloadLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxRemainingPayloadLength(PduIdType RxPduId, uint32 Length)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the remaining payload length of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxConnectionInfo[RxPduId].RemainingPayloadLength = Length;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Length); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduConnIdx */


/**********************************************************************************************************************
 * vIpc_XCfg_GetRxCon_State()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_RxSduStateType, VIPC_CODE) vIpc_XCfg_GetRxCon_State(PduIdType RxSduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the state of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxConnectionInfo[RxSduId].SduState;
#else
  VIPC_DUMMY_STATEMENT(RxSduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return VIPC_RX_SDU_STATE_IDLE;
#endif
} /* vIpc_XCfg_GetRxCon_State */

/**********************************************************************************************************************
 * vIpc_XCfg_SetRxCon_State()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetRxCon_State(PduIdType RxSduId,
                                                                  vIpc_RxSduStateType NextState)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the state of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxConnectionInfo[RxSduId].SduState = NextState;
#else
  VIPC_DUMMY_STATEMENT(RxSduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(NextState); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetTxCon_State */



/**********************************************************************************************************************
 * vIpc_XCfg_GetSeqCtr()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetSeqCtr(PduIdType RxPduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the sequence counter of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxConnectionInfo[RxPduId].SeqCtr;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return 0u;
#endif
} /* vIpc_XCfg_GetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_SetSeqCtr()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetSeqCtr(PduIdType RxPduId,
                                                                 uint8 Ctr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Sets the sequence counter of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxConnectionInfo[RxPduId].SeqCtr = Ctr;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Ctr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduConnIdx */


/**********************************************************************************************************************
 * vIpc_XCfg_GetSeqCtr()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(uint8, VIPC_CODE) vIpc_XCfg_GetFrameType(PduIdType RxPduId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns the frame type of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_RxConnectionInfo[RxPduId].FrameType;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return VIPC_MSG_SF;
#endif
} /* vIpc_XCfg_GetRxPduConnIdx */

/**********************************************************************************************************************
 * vIpc_XCfg_SetSeqCtr()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_XCfg_SetFrameType(PduIdType RxPduId,
                                                                 uint8 Type)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the frame type of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  vIpc_RxConnectionInfo[RxPduId].FrameType = Type;
#else
  VIPC_DUMMY_STATEMENT(RxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  VIPC_DUMMY_STATEMENT(Type); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif
} /* vIpc_XCfg_SetRxPduConnIdx */



/*
 *  ComStackLib abstraction Rx Channels
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetAddressLengthOfRxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_AddressLengthOfRxChannelType, VIPC_CODE) vIpc_XCfg_GetAddressLengthOfRxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns address length of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetAddressLengthOfRxChannel(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetAddressLengthOfRxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetAddressLengthOfRxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_AddressStartIdxOfRxChannelType, VIPC_CODE) vIpc_XCfg_GetAddressStartIdxOfRxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns address start index of Rx channel. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetAddressStartIdxOfRxChannel(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetAddressStartIdxOfRxChannel */

/*
 *  ComStackLib abstraction Rx Connections
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRxAddressOfRxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_RxAddressOfRxConnectionsType, VIPC_CODE) vIpc_XCfg_GetRxAddressOfRxConnections(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the address of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetRxAddressOfRxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetRxAddressOfRxConnections */

/**********************************************************************************************************************
 * vIpc_XCfg_GetUpperLayerIdOfRxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_UpperLayerIdOfRxConnectionsType, VIPC_CODE) vIpc_XCfg_GetUpperLayerIdOfRxConnections(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the UL Id of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetUpperLayerIdOfRxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetUpperLayerIdOfRxConnections */


/*
 *  ComStackLib abstraction Rx Function Pointer Tables
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_CopyRxDataFctPtrType, VIPC_CODE) vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the CopyRxData callback of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetCopyRxDataOfRxUlCallbacks(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRxIndicationOfRxUlCallbacks()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_RxIndicationFctPtrType, VIPC_CODE) vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the RxIndication callback of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetRxIndicationOfRxUlCallbacks(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetRxIndicationOfRxUlCallbacks */

/**********************************************************************************************************************
 * vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_StartOfReceptionFctPtrType, VIPC_CODE) vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the StartOfReception callback of Rx connection. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetStartOfReceptionOfRxUlCallbacks(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks */


/*
 *  ComStackLib abstraction Rx Data Size
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetRxConnectionsIdxOfAddress()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_RxConnectionsIdxOfAddressType, VIPC_CODE) vIpc_XCfg_GetRxConnectionsIdxOfAddress(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the Rx connection Id of address. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return vIpc_GetRxConnectionsIdxOfAddress(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetRxConnectionsIdxOfAddress */

/**********************************************************************************************************************
 * vIpc_XCfg_GetSizeOfRxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfRxChannel(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns number of Rx channels. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return (PduIdType)vIpc_GetSizeOfRxChannel();
#else
  return (PduIdType)0u;
#endif
} /* vIpc_XCfg_GetSizeOfRxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetSizeOfRxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfRxConnections(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns number of Rx connections. */
#if (VIPC_RXCONNECTIONS == STD_ON)
  return (PduIdType)vIpc_GetSizeOfRxConnections();
#else
  return (PduIdType)0u;
#endif
} /* vIpc_XCfg_GetSizeOfRxConnections */

/*
 *  ComStackLib abstraction Tx Function Pointer Tables
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_TransmitFctPtrType, VIPC_CODE) vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns Transmit function of LL with Tx channel Id. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTransmitOfTransmitLLFunctionTable(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable */

/**********************************************************************************************************************
 * vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_TxConfirmationFctPtrType, VIPC_CODE) vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the TxConfirmation callback of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxConfirmationOfTxUlCallbacks(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks */

/**********************************************************************************************************************
 * vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_CopyTxDataFctPtrType, VIPC_CODE) vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the CopyTxData callback of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetCopyTxDataOfTxUlCallbacks(Index);
#else
  return NULL_PTR;
#endif
} /* vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks */


/*
 *  ComStackLib abstraction Tx Connections
 */
/**********************************************************************************************************************
 * vIpc_XCfg_GetUpperLayerIdOfTxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_UpperLayerIdOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetUpperLayerIdOfTxConnections(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the UL Id of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetUpperLayerIdOfTxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetUpperLayerIdOfTxConnections */

/**********************************************************************************************************************
 * vIpc_XCfg_GetTxChannelIdxOfTxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_TxChannelIdxOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetTxChannelIdxOfTxConnections(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns Tx channel index to which the Tx connection is assigned. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxChannelIdxOfTxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetTxChannelIdxOfTxConnections */

/**********************************************************************************************************************
 * vIpc_XCfg_GetAddressOfTxConnection()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_TxAddressOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetAddressOfTxConnection(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the address of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetTxAddressOfTxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetAddressOfTxConnection */

/**********************************************************************************************************************
 * vIpc_XCfg_GetPriorityOfTxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_PriorityOfTxConnectionsType, VIPC_CODE) vIpc_XCfg_GetPriorityOfTxConnections(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the priority of Tx connection. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetPriorityOfTxConnections(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetPriorityOfTxConnections */

/*
 *  ComStackLib abstraction Tx Channels
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetNumSepCyclesOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_NumSepCyclesOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetNumSepCyclesOfTxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the number of separation cycles of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetNumSepCyclesOfTxChannel(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetNumSepCyclesOfTxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetNumConnectionsOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_NumConnectionsOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetNumConnectionsOfTxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns number of connections assigned to Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetNumConnectionsOfTxChannel(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetNumConnectionsOfTxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetMaxLengthOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_MaxLengthOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetMaxLengthOfTxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns maximum PDU length of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetMaxLengthOfTxChannel(Index); /* PRQA S 4391 */ /* MD_vIpc_4391 */
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetMaxLengthOfTxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetLowerLayerIdOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_LowerLayerIdOfTxChannelType, VIPC_CODE) vIpc_XCfg_GetLowerLayerIdOfTxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the LL Id of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetLowerLayerIdOfTxChannel(Index);
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetLowerLayerIdOfTxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetAlgorithmOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(vIpc_Algorithm, VIPC_CODE) vIpc_XCfg_GetAlgorithmOfTxChannel(PduIdType Index)
{
  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(Index); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* #10 Returns the queue algorithm of Tx channel. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return vIpc_GetAlgorithmOfTxChannel(Index);
#else
  return VIPC_FIFO;
#endif
} /* vIpc_XCfg_GetAlgorithmOfTxChannel */


/*
 *  ComStackLib abstraction Tx Data Size
 */

/**********************************************************************************************************************
 * vIpc_XCfg_GetSizeOfTxChannel()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfTxChannel(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns number of Tx channels. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return (PduIdType)vIpc_GetSizeOfTxChannel();
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetSizeOfTxChannel */

/**********************************************************************************************************************
 * vIpc_XCfg_GetSizeOfTxConnections()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_XCfg_GetSizeOfTxConnections(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Returns number of Tx connections. */
#if (VIPC_TXCONNECTIONS == STD_ON)
  return (PduIdType)vIpc_GetSizeOfTxConnections();
#else
  return 0u;
#endif
} /* vIpc_XCfg_GetSizeOfTxConnections */

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpc_XCfgImpl.h
 *********************************************************************************************************************/

#endif /* VIPC_XCFG_IMPL_H */
