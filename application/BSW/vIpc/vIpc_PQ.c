/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector
 * Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set
 * out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  vIpc_PQ.c
 *        \brief  vIpc_PQ source file
 *
 *      \details  This file contains the functions required to queue transmission requests.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VIPC_PQ_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpc_PQ.h"
#include "SchM_vIpc.h"
#include "vstdlib.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#if (VIPC_TXCONNECTIONS == STD_ON)
  
/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
/**********************************************************************************************************************
 *  vIpc_PQ_HasFirstConHigherPrio()
 *********************************************************************************************************************/
/*! \brief       Compares two Id connection about their priority.
 *  \details     Compares two Id connection about their priority and indicates if the first connection Id has a
 *               higher priority.
 *  \param[in]   FirstTxConId      The first Tx connection Id.
 *  \param[in]   SecondTxConId     The second Tx connection Id.
 *  \return      FALSE             The second Tx connection Id has equal or higher priority.
 *               TRUE              The first Tx connection Id has higher priority.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(FirstTxConId);
 *               requires vIpc_XCfg_IsTxSduIdValid(SecondTxConId);
 *  \endspec
 *  \synchronous TRUE
 *  \pre         -
 *  \ingroup     priority
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_PQ_HasFirstConHigherPrio(PduIdType FirstTxConId,
                                                                           PduIdType SecondTxConId);

/**********************************************************************************************************************
 *  vIpc_PQ_IsQueueEmpty()
 *********************************************************************************************************************/
/*! \brief       Checks if the queue of the specified Tx channel is currently empty.
 *  \details     -
 *  \param[in]   TxChanId          The Tx channel for which the queue is checked.
 *  \return      FALSE             The queue is not empty.
 *               TRUE              The queue is empty.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires TxChanId < vIpc_XCfg_GetSizeOfTxChannel();
 *  \endspec
 *  \synchronous TRUE
 *  \pre         -
 *  \ingroup     priority
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_PQ_IsQueueEmpty(PduIdType TxChanId);

/**********************************************************************************************************************
 *  vIpc_PQ_Insert_FifoBased()
 *********************************************************************************************************************/
/*! \brief       Inserts the specified connection to FIFO queue.
 *  \details     -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(ConnectionId);
 *  \endspec
 *  \synchronous TRUE
 *  \pre         The requested TxSduId is valid.
 *  \ingroup     PQ
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_PQ_Insert_FifoBased(PduIdType ConnectionId);

/**********************************************************************************************************************
 *  vIpc_PQ_Insert_PrioBased()
 *********************************************************************************************************************/
/*! \brief       Inserts the specified connection to priority based queue.
 *  \details     -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(ConnectionId);
 *  \endspec
 *  \synchronous TRUE
 *  \pre         The requested TxSduId is valid.
 *  \ingroup     PQ
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_PQ_Insert_PrioBased(PduIdType ConnectionId);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_PQ_IsQueueEmpty()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_PQ_IsQueueEmpty(PduIdType TxChanId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Indicates if the queue of specified Tx channel is empty. */
  return (boolean)(vIpc_TxQI_GetHighPrioIdx(TxChanId) >= vIpc_XCfg_GetInvalidTxConId()); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
} /* vIpc_PQ_IsQueueEmpty */

/**********************************************************************************************************************
 * vIpc_PQ_HasFirstConHigherPrio()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_PQ_HasFirstConHigherPrio(PduIdType FirstTxConId,
                                                                           PduIdType SecondTxConId)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Indicates if the first Tx connection Id has a higher priority than the second Tx connection Id. */
  return (boolean)(vIpc_XCfg_GetPriorityOfTxConnections(FirstTxConId) > vIpc_TxQ_GetPrio(SecondTxConId)); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
} /* vIpc_PQ_HasFirstConHigherPrio */

/**********************************************************************************************************************
 * vIpc_PQ_Insert_PrioBased()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_PQ_Insert_PrioBased(PduIdType ConnectionId)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType channelId = vIpc_XCfg_GetTxChannelIdxOfTxConnections(ConnectionId);
  const vIpc_NumConnectionsOfTxChannelType numConIdsOfChan = vIpc_XCfg_GetNumConnectionsOfTxChannel(channelId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const PduIdType invalidConId = vIpc_XCfg_GetInvalidTxConId();
  const vIpc_PrioType newPrio = vIpc_XCfg_GetPriorityOfTxConnections(ConnectionId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Enter VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Enter_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();

  /* #20 If the queue is currently empty,
   *       Enqueue current connection ID and set the high and low indices to this entry. */
  if (vIpc_PQ_IsQueueEmpty(channelId) == TRUE)
  {
    vIpc_TxQ_SetNextLowerIdx(ConnectionId, invalidConId);
    vIpc_TxQ_SetNextHigherIdx(ConnectionId, invalidConId);
    vIpc_TxQ_SetPrio(ConnectionId, newPrio);
    vIpc_TxQI_SetHighPrioIdx(channelId, ConnectionId);
    vIpc_TxQI_SetLowPrioIdx(channelId, ConnectionId);
  }
  /* #30 Otherwise: */
  else
  {
    PduIdType currentQueuedConIdx = vIpc_TxQI_GetHighPrioIdx(channelId);
    PduIdType priorQueuedConIdx   = currentQueuedConIdx; /* PRQA S 2981 */ /* MD_MSR_RetVal */
    vIpc_ConAddressType i;
    
    /*@ assert vIpc_XCfg_IsTxSduIdValid(currentQueuedConIdx); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */

    /* #40 Iterate over all enqueued connections of current channel from highest to lowest priority. */
    for (i = 0u; i < numConIdsOfChan; ++i) /* PRQA S 0771 */ /* MD_vIpc_0771 */ /* COV_VIPC_VALID_NUM_CONNECTION_TX_CHAN */
    {
      /* #50 If the enqueued connection has lower priority than the new connection,
       *       enqueue prior the enqueued connection. */
      if (vIpc_PQ_HasFirstConHigherPrio(ConnectionId, (PduIdType)currentQueuedConIdx) == TRUE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
      {
        PduIdType nextHigherIdxForConId;
        /* #60 If this is the first queued connection, set higher prio index of new connection to invalid.
         *     Otherwise, set higher prio index of new connection to prior enqueued connection. */
        if (priorQueuedConIdx == currentQueuedConIdx) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
        {
          nextHigherIdxForConId = invalidConId;
        }
        else
        {
          nextHigherIdxForConId = priorQueuedConIdx;
        }

        vIpc_TxQ_SetNextHigherIdx(ConnectionId, nextHigherIdxForConId);
        vIpc_TxQ_SetNextHigherIdx((PduIdType)currentQueuedConIdx, ConnectionId);
        vIpc_TxQ_SetNextLowerIdx(ConnectionId, currentQueuedConIdx);
        vIpc_TxQ_SetPrio(ConnectionId, newPrio);

        /* #70 If the new enqueued connection has highest priority,
         *       move high prio pointer to this connection.
         *     Otherwise, set lower prio index of previous queued connection to new enqueued connection. */
        if (currentQueuedConIdx == vIpc_TxQI_GetHighPrioIdx(channelId))
        {
          vIpc_TxQI_SetHighPrioIdx(channelId, ConnectionId);
        }
        else
        {
          vIpc_TxQ_SetNextLowerIdx((PduIdType)priorQueuedConIdx, ConnectionId);
        }

        break;
      }
      /* #80 Otherwise:
       *       If the current queued connection is the last element in queue,
       *         enqueue new connection as new last element.
       *       Otherwise, remember previous enqueued connection and get next lower prior connection. */
      else
      {
        priorQueuedConIdx = currentQueuedConIdx;
        currentQueuedConIdx = vIpc_TxQ_GetNextLowerIdx((PduIdType)currentQueuedConIdx);

        if (currentQueuedConIdx >= invalidConId)
        {
          vIpc_TxQ_SetNextLowerIdx((PduIdType)priorQueuedConIdx, ConnectionId);
          vIpc_TxQ_SetNextLowerIdx((PduIdType)ConnectionId, invalidConId);
          vIpc_TxQ_SetNextHigherIdx((PduIdType)ConnectionId, priorQueuedConIdx);
          vIpc_TxQ_SetPrio((PduIdType)ConnectionId, newPrio);

          vIpc_TxQI_SetLowPrioIdx(channelId, ConnectionId);
          break;
        }
      }
    }
  }

  /* #90 Leave VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();
} /* vIpc_PQ_Insert_PrioBased */


/**********************************************************************************************************************
 * vIpc_PQ_Insert_FifoBased()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_PQ_Insert_FifoBased(PduIdType ConnectionId)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType channelId = vIpc_XCfg_GetTxChannelIdxOfTxConnections(ConnectionId);
  const PduIdType invalidConId = vIpc_XCfg_GetInvalidTxConId();
  PduIdType nextHigherIdxForConId = invalidConId;
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Enter VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Enter_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();

  /* #20 If the queue is currently empty. */
  if (vIpc_PQ_IsQueueEmpty(channelId) == TRUE)
  {
    /* #25 Then Enqueue current connection ID and set the high and low indices to this entry. */
    vIpc_TxQI_SetHighPrioIdx(channelId, ConnectionId);
  }
  else
  {
    /* #30 Otherwise add the new connection at the end of queue. */
    nextHigherIdxForConId = vIpc_TxQI_GetLowPrioIdx(channelId);
    
    /*@ assert vIpc_XCfg_IsTxSduIdValid(nextHigherIdxForConId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
    vIpc_TxQ_SetNextLowerIdx((PduIdType)nextHigherIdxForConId, ConnectionId);
  }

  vIpc_TxQ_SetNextHigherIdx(ConnectionId, nextHigherIdxForConId);
  vIpc_TxQ_SetNextLowerIdx(ConnectionId, invalidConId);
  vIpc_TxQI_SetLowPrioIdx(channelId, ConnectionId);

  /* #40 Leave VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();
} /* vIpc_PQ_Insert_FifoBased */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_PQ_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPC_CODE) vIpc_PQ_Init(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  PduIdType channelId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize all queues and their indices. */
  for (channelId = 0u; channelId < vIpc_XCfg_GetSizeOfTxChannel(); ++channelId) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */
    
    vIpc_TxQI_InitHighAndLowPrioIdx(channelId);
  }

  vIpc_TxQ_InitData(); /* PRQA S 2987 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
} /* vIpc_PQ_Init */


/**********************************************************************************************************************
 * vIpc_PQ_Insert()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPC_CODE) vIpc_PQ_Insert(PduIdType ConnectionId)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType channelId = vIpc_XCfg_GetTxChannelIdxOfTxConnections(ConnectionId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Trigger queuing of connection depending on the corresponding TxChannel's queuing algorithm. */
  if (vIpc_XCfg_GetAlgorithmOfTxChannel(channelId) == VIPC_FIFO) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    vIpc_PQ_Insert_FifoBased(ConnectionId);
  }
  else
  {
    vIpc_PQ_Insert_PrioBased(ConnectionId);
  }
} /* vIpc_PQ_Insert */

/**********************************************************************************************************************
 * vIpc_PQ_Peek()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VIPC_CODE) vIpc_PQ_Peek(PduIdType TxChanId,
                                             CONSTP2VAR(PduIdType, AUTOMATIC, VIPC_APPL_VAR) TxConIdPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If there is at least one connection enqueued, get the connection with highest priority. */
  if (vIpc_PQ_IsQueueEmpty(TxChanId) == FALSE)
  {
    *TxConIdPtr = vIpc_TxQI_GetHighPrioIdx(TxChanId);
    retVal = E_OK;
  }

  return retVal;
} /* vIpc_PQ_Peek */

/**********************************************************************************************************************
 * vIpc_PQ_Remove()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
FUNC(void, VIPC_CODE) vIpc_PQ_Remove(PduIdType TxConId)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType channelId = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId);
  const PduIdType invalidConId = vIpc_XCfg_GetInvalidTxConId();
  PduIdType nextHigherIdx;
  PduIdType nextLowerIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Enter VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Enter_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();

  nextHigherIdx = vIpc_TxQ_GetNextHigherIdx(TxConId);
  nextLowerIdx = vIpc_TxQ_GetNextLowerIdx(TxConId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */
  /*@ assert vIpc_XCfg_IsTxSduIdValid(nextLowerIdx); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
  /*@ assert vIpc_XCfg_IsTxSduIdValid(nextHigherIdx); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */

  /* #20 If there is only one enqueued connection, set queue pointers to invalid. */
  if ((nextLowerIdx == invalidConId)  /* PRQA S 2991, 2992, 2994, 2995, 2996 1 */ /* MD_MSR_ConstantCondition */
   && (nextHigherIdx == invalidConId))
  {
    vIpc_TxQI_SetHighPrioIdx(channelId, invalidConId);
    vIpc_TxQI_SetLowPrioIdx(channelId, invalidConId);
  }
  /* #30 Otherwise if it is the first enqueued connection, set next highest connection as highest. */
  else if (nextHigherIdx == invalidConId)
  {
    vIpc_TxQI_SetHighPrioIdx(channelId, nextLowerIdx);
    vIpc_TxQ_SetNextHigherIdx((PduIdType)nextLowerIdx, invalidConId);
  }
  /* #40 Otherwise if it is the last enqueued connection, set next lower connection as lowest. */
  else if (nextLowerIdx == invalidConId)
  {
    vIpc_TxQI_SetLowPrioIdx(channelId, nextHigherIdx);
    vIpc_TxQ_SetNextLowerIdx((PduIdType)nextHigherIdx, invalidConId);
  }
  /* #50 Otherwise, it is a connection in the middle of the queue, let the adjacent connection point to each other. */
  else
  {
    /* Move lower connections high pointer to higher connection. */
    vIpc_TxQ_SetNextHigherIdx((PduIdType)nextLowerIdx,
                                   nextHigherIdx);
    /* Move higher connections lower pointer to lower connection. */
    vIpc_TxQ_SetNextLowerIdx((PduIdType)nextHigherIdx,
                                  nextLowerIdx);
  }

  /* #60 Leave VIPC_EXCLUSIVE_AREA_TX_QUEUE. */
  SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_QUEUE();
} /* vIpc_PQ_Remove */

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VIPC_TXCONNECTIONS == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: vIpc_Rx.c
 *********************************************************************************************************************/
