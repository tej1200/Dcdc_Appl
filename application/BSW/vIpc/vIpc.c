/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector
 * Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set
 * out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file
 *        \brief  vIpc source file
 *
 *      \details  Main source file containing all public API implementations
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

/* Configuration, interrupt handling implementations differ from the source
 * identification define used in this example.
 * The naming schemes for those files can be taken from this list:
 *
 * vIpc_LCfg.c:    VIPC_LCFG_SOURCE
 * vIpc_PBCfg.c:   VIPC_PBCFG_SOURCE
 * vIpc_Irq.c:     VIPC_IRQ_SOURCE
 */
#define VIPC_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "vIpc.h"
#include "vIpc_PQ.h"
#include "vIpc_Tx.h"
#include "vIpc_Rx.h"
#include "SchM_vIpc.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

/* Check the version of vIpc header file */
#if ((VIPC_SW_MAJOR_VERSION != (2u)) || (VIPC_SW_MINOR_VERSION != (0u)) || (VIPC_SW_PATCH_VERSION != (3u)))
#error "Vendor specific version numbers of vIpc.c and vIpc.h are inconsistent"
#endif

/* Check the version of the configuration header file */
#if ((VIPC_CFG_MAJOR_VERSION != (2u)) || (VIPC_CFG_MINOR_VERSION != (0u)))
#error "Version numbers of vIpc.c and vIpc_Cfg.h are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#define VIPC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* clang-format off */
VAR(vIpc_IsInitialized, VIPC_VAR_ZERO_INIT) vIpc_InitStatus = VIPC_NOT_INITIALIZED;
/* clang-format on */

#define VIPC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpc_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VIPC_CODE) vIpc_InitMemory(void)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #20 Set vIpc to 'not initialized' */
  vIpc_InitStatus = VIPC_NOT_INITIALIZED;
} /* vIpc_InitMemory */


/**********************************************************************************************************************
 *  vIpc_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
FUNC(void, VIPC_CODE) vIpc_Init(P2CONST(vIpc_ConfigType, AUTOMATIC, VIPC_PBCFG) ConfigPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_ErrorCode errorId = VIPC_E_NO_ERROR;
  VIPC_DUMMY_STATEMENT_CONST(ConfigPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* #10 If Development Errors are enabled and vIpc is already initialized, report an error. */
  if ((vIpc_XCfg_DevErrorDetect() == TRUE) /* PRQA S 2995, 2996 1 */ /* MD_MSR_ConstantCondition */
      && (vIpc_XCfg_IsVIpcInitialized() == TRUE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_ALREADY_INITIALIZED;
  }
  /* #20 Otherwise:
   *       Initialize each configured Rx and Tx physical channel.
   *       Initialize each Tx connection.
   *       Initialize the queues. */
  else
  {
    PduIdType i;

    /* ----- Implementation ----------------------------------------------- */
    vIpc_InitStatus = VIPC_INITIALIZED;

    for (i = 0u; i < vIpc_XCfg_GetSizeOfRxConnections(); i++) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */ /* COV_VIPC_ONLY_RX XF */
    {
      vIpc_XCfg_SetRxCon_State(i, VIPC_RX_SDU_STATE_IDLE);
      vIpc_XCfg_SetRxPayloadLength(i, 0u);
      vIpc_XCfg_SetRxRemainingPayloadLength(i, 0u);
    }

    for (i = 0u; i < vIpc_XCfg_GetSizeOfTxChannel(); i++) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */ /* COV_VIPC_ONLY_TX XF */
    {
      vIpc_TxChan_SetActiveTxConId(i, 0u);
      vIpc_TxChan_SetAlreadySentDataOfCurrSeg(i, 0u);
      vIpc_TxChan_SetActiveTxConAvailable(i, FALSE);
      vIpc_TxChan_InitRemainingSeparationCycles(i);
    }

    for (i = 0u; i < vIpc_XCfg_GetSizeOfTxConnections(); i++) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */ /* COV_VIPC_ONLY_TX XF */
    {
      vIpc_TxCon_SetCurrentSegment(i, 0u);
      vIpc_TxCon_SetMessageType(i, VIPC_MSG_SF);
      vIpc_TxCon_SetRemCompleteLength(i, 0u);
      vIpc_TxCon_SetRemPayloadLength(i, 0u);
      vIpc_TxCon_SetState(i, VIPC_TX_CON_STATE_IDLE);
    }

#if (VIPC_TXCONNECTIONS == STD_ON)
    vIpc_PQ_Init();
#endif
  }

  (void) vIpc_XCfg_ReportError(VIPC_SID_INIT, errorId);
} /* vIpc_Init */


/**********************************************************************************************************************
 *  vIpc_GetVersionInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VIPC_CODE) vIpc_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VIPC_APPL_VAR) VersionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_ErrorCode errorId = VIPC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
  /* #10 If Dev Error Detect is enabled and the version pointer is valid. */
  if ((vIpc_XCfg_DevErrorDetect() == TRUE) && (VersionInfo == NULL_PTR)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_POINTER;
  }
  /* #20 Otherwise, set the version info the the values from the component header. */
  else
  {
    /* ----- Implementation ----------------------------------------------- */
    VersionInfo->vendorID         = (VIPC_VENDOR_ID);
    VersionInfo->moduleID         = (VIPC_MODULE_ID);
    VersionInfo->sw_major_version = (VIPC_SW_MAJOR_VERSION);
    VersionInfo->sw_minor_version = (VIPC_SW_MINOR_VERSION);
    VersionInfo->sw_patch_version = (VIPC_SW_PATCH_VERSION);
  }

  /* ----- Development Error Report --------------------------------------- */
  (void) vIpc_XCfg_ReportError(VIPC_SID_GET_VERSION_INFO, errorId);
} /* vIpc_GetVersionInfo */


/**********************************************************************************************************************
 * vIpc_MainFunction()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, VIPC_CODE) vIpc_MainFunction(void)
{
#if (VIPC_TXCONNECTIONS == STD_ON) 
  /* ----- Local Variables ---------------------------------------------- */
  PduIdType channelId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Execute main function only if vIpc is initialized. */
  if (vIpc_XCfg_IsVIpcInitialized() == TRUE)
  {
    /* #20 Iterate over all Tx channels.
     *       If the channel is idle and the delay is past, trigger next transmission.
     *       Otherwise, decrement the delay counter. */
    for (channelId = 0u; channelId < vIpc_XCfg_GetSizeOfTxChannel(); channelId++) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /*@ assert vIpc_XCfg_IsTxPduIdValid(channelId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */
      
      if ((vIpc_TxChan_GetRemainingSeparationCycles(channelId) == 0u)
       && (vIpc_XCfg_IsLLBusy(channelId) == FALSE))
      {
        vIpc_Tx_Process(channelId);
      }
      else
      {
        vIpc_TxChan_DecRemainingSeparationCycles(channelId);
      }
    }
  }
#endif  /* (VIPC_TXCONNECTIONS == STD_ON) */   
} /* vIpc_MainFunction */

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Justification for module-specific MISRA deviations:
MD_vIpc_0771:     Rule 14.6
      Reason:     For optimization reasons more than one break was used to leave the for loop.
      Risk:       Maintainability reduced due to multiple break statements.
      Prevention: Covered by code review.
      
MD_vIpc_4391:     Rule 10.8
      Reason:     A uint16 is used as offset for an uint32 operation.
      Risk:       Can be misinterpreted, difficult to understand.
      Prevention: Testet with TCASE-580526.
 */
 
/* ------------------------------------------------------------------------------------------------------------------ */
/* Coverage Justifications                                                                                            */
/* ------------------------------------------------------------------------------------------------------------------ */

/* COV_JUSTIFICATION_BEGIN
 *
--- Preprocessor Coverage Justifications ------------------------------------------------------------------------------
\ID COV_VIPC_EXCLUDED_FEATURES
  \REASON The preprocessor switch is related to a feature which is excluded for SafeBSW; it must be properly set by GenTool or UserConfig (see also [CM_VIPC_PRECOMPILEOPTIONS]).
  
\ID COV_VIPC_ONLY_RX
  \REASON This condition can only be tested with all conditions in configurations containing at least one Rx SDU.
  
\ID COV_VIPC_ONLY_TX
  \REASON This condition can only be tested with all conditions in configurations containing at least one Tx SDU.
  
\ID COV_VIPC_VALID_NUM_CONNECTION_TX_CHAN
  \ACCEPT TX
  \REASON Usage of numConIdsOfChan will always be true because the number of connections is already checked by the previous vIpc_PQ_IsQueueEmpty() call.


 * COV_JUSTIFICATION_END */
 
/* ------------------------------------------------------------------------------------------------------------------ */
/* Silent BSW Justifications                                                                                              */
/* ------------------------------------------------------------------------------------------------------------------ */

/* SBSW_JUSTIFICATION_BEGIN
 
 \ID             VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX
 \DESCRIPTION    The assertion ensures that this ID is indeed always valid:
                 The index is used to access the vIpc_RxChannelInfo[] or vIpc_TxChannelInfo[]/vIpc_QueueInfo[] arrays.
                 This access is always valid due to the countermeasure.
 \COUNTERMEASURE \M [CM_VIPC_VALID_CONFIGURED_CHANNEL_IDX]
 
 \ID             VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX
 \DESCRIPTION    The assertion ensures that this ID is indeed always valid:
                 The index is used to access the vIpc_RxConnectionInfo[] or vIpc_TxConnectionInfo[] arrays.
                 This access is always valid due to the countermeasure.
 \COUNTERMEASURE \M [CM_VIPC_VALID_CONFIGURED_CONNECTION_IDX]
 
 \ID             VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED
 \DESCRIPTION    Call to <function-name> is valid because the assertions prior the call are all passed.
                 <function-name> can be:
                 <UL>_vIpc_StartOfReception
                 <UL>_vIpc_RxIndication
                 <UL>_vIpc_CopyRxData
                 <UL>_vIpc_CopyTxData
                 <UL>_vIpc_TxConfirmation
                 <LL>_Transmit                 
 \COUNTERMEASURE \N No countermeasure required because the assertions prior the call are all passed.
 
 \ID             VCA_VIPC_FCT_CALL_UNSATISFIABLE
 \DESCRIPTION    Call to <function-name> can be unsatisfiable because there are actually no communication objects (PDUs/SDUs) configured.
                 <function-name> can be:
                 vIpc_StartOfReception
                 vIpc_RxIndication
                 vIpc_CopyRxData
                 vIpc_CopyTxData
                 vIpc_TxConfirmation
                 vIpc_Transmit
 \COUNTERMEASURE \N No countermeasure required because these APIs are not expected to be called if there are no PDUs configured. 

SBSW_JUSTIFICATION_END */

/* ------------------------------------------------------------------------------------------------------------------ */
/* Silent BSW Countermeasures                                                                                         */
/* ------------------------------------------------------------------------------------------------------------------ */
/*
--- MSSV Plugin ---
\CM CM_VIPC_PRECOMPILEOPTIONS To ensure that all mandatory / excluded features are as expected, the following must be verified by MSSV:
                               - VIPC_UNIT_TEST is undefined
                               
\CM CM_VIPC_VALID_CONFIGURED_CONNECTION_IDX To ensure that the array access to vIpc_RxConnectionInfo[] and vIpc_TxConnectionInfo[] is valid 
                              the following must be verified by MSSV:
                               - if VIPC_RXCONNECTIONS == STD_ON: AssertArraySize(vIpc_RxConnectionInfo, Equal, vIpc_GetSizeOfRxConnections)
                               - if VIPC_TXCONNECTIONS == STD_ON: AssertArraySize(vIpc_TxConnectionInfo, Equal, vIpc_GetSizeOfTxConnections)
                               
\CM CM_VIPC_VALID_CONFIGURED_CHANNEL_IDX To ensure that the array access to vIpc_RxChannelInfo[] and vIpc_TxChannelInfo[]/vIpc_QueueInfo[] is valid 
                              the following must be verified by MSSV:
                               - if VIPC_RXCONNECTIONS == STD_ON: AssertArraySize(vIpc_RxChannelInfo, Equal, vIpc_GetSizeOfRxChannel)
                               - if VIPC_TXCONNECTIONS == STD_ON: AssertArraySize(vIpc_TxChannelInfo, Equal, vIpc_GetSizeOfTxChannel)                               
                               - if VIPC_TXCONNECTIONS == STD_ON: AssertArraySize(vIpc_QueueInfo, Equal, vIpc_GetSizeOfTxChannel)                               
                               If the ChannelIdx was previously determined vIpc_GetTxChannelIdxOfTxConnections() no further MSSV check is necessary 
                               due to CSL_02.
*/

/**********************************************************************************************************************
 *  END OF FILE: vIpc.c
 *********************************************************************************************************************/
