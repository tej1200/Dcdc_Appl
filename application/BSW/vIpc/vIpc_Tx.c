/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  vIpc_Tx.c
 *        \brief  vIpc_Tx source file
 *
 *      \details  This file contains the functions required to transmit vIpc frames.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VIPC_TX_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpc_Tx.h"
#include "vIpc_PQ.h"
#include "SchM_vIpc.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#if (VIPC_TXCONNECTIONS == STD_ON)

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/
#define VIPC_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VIPC_LOCAL CONST(uint8, VIPC_CONST) vIpc_Tx_HeaderType2HeaderLen[4] = /* PRQA S 3218 */ /* MD_MSR_Rule8.7 */
{
  VIPC_MSG_HEADER_SIZE_SF,
  VIPC_MSG_HEADER_SIZE_FF,
  VIPC_MSG_HEADER_SIZE_CF,
  VIPC_MSG_HEADER_SIZE_LF
};

#define VIPC_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if (VIPC_TXCONNECTIONS == STD_ON)

/**********************************************************************************************************************
 * vIpc_Tx_GetTotalSduLength()
 *********************************************************************************************************************/
/*! \brief        Calculates the total length of requested Tx data.
 *  \details      Calculates the total length of requested Tx data (requested Tx data + all required vIpc header).
 *  \param[in]    TxConId       The connection ID for which the complete Tx length is calculated.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_Tx_GetTotalSduLength(PduIdType TxConId);

/**********************************************************************************************************************
 * vIpc_Tx_InternalTransmit()
 *********************************************************************************************************************/
/*! \brief        Handles transmission request.
 *  \details      Enques the transmission request if not already queued and forwards transmission to lower layer if
 *                lower layer not busy.
 *  \param[in]    TxConId       The connection ID for which the transmission request is handled.
 *  \param[in,out] PduInfoPtr    The pointer to the transmission data.
 *  \return       E_OK          Transmission request handling successfully.
 *  \return       E_NOT_OK      Transmission request handling failed.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *                requires PduInfoPtr != NULL_PTR;
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(Std_ReturnType, VIPC_CODE) vIpc_Tx_InternalTransmit(PduIdType TxConId,
                                                                           P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 * vIpc_Tx_GetInitialMsgEvent()
 *********************************************************************************************************************/
/*! \brief        Handles an initial message event.
 *  \details      -
 *  \param[in]    TxChannelId   The channel ID for which the transmission request is handled.
 *  \param[in]    TotalLength   The total length of the transmission
 *  \return       VIPC_MSG_EVENT_START_SF          Transmission can be handled with a single frame.
 *  \return       VIPC_MSG_EVENT_START_FF      Transmission must be handled with multiple frames.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(vIpc_MsgEventType, VIPC_CODE) vIpc_Tx_GetInitialMsgEvent(PduIdType TxChannelId, PduLengthType TotalLength);

/**********************************************************************************************************************
 * vIpc_Tx_SetNextMsgType()
 *********************************************************************************************************************/
/*! \brief        Sets message type of next vIpc frame (like SF, FF, CF or LF).
 *  \details      -
 *  \param[in]    TxConId       The connection ID for which the message type has to be changed.
 *  \param[in]    Event         The event that changes the next message type.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_SetNextMsgType(PduIdType TxConId, vIpc_MsgEventType Event);

/**********************************************************************************************************************
 * vIpc_Tx_SetNextMsgType_AfterCopyHeader()
 *********************************************************************************************************************/
/*! \brief        Prepares the change of next message type after header is copied.
 *  \details      -
 *  \param[in]    TxConId       The connection ID for which the message type has to be changed.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_SetNextMsgType_AfterCopyHeader(PduIdType TxConId);

/**********************************************************************************************************************
 * vIpc_Tx_CopyHeader()
 *********************************************************************************************************************/
/*! \brief        Copies the current vIpc header to the lower layer's transmit data.
 *  \details      Copies the current vIpc header (for SF, FF, CF or LF) to lower layer.
 *  \param[in]    TxConId       The connection ID for which the header has to be copied.
 *  \param[in,out] PduInfoPtr    The pointer to the transmission data.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxSduIdValid(TxConId);
 *                requires PduInfoPtr != NULL_PTR;
 *                requires PduInfoPtr->SduDataPtr != NULL_PTR;
 *                requires $lengthOf(PduInfoPtr->SduDataPtr) > VIPC_MSG_LENGTH_POS+3;
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          The PduInfoPtr points to first element for this segment.
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_CopyHeader(PduIdType TxConId,
                                                           P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 * vIpc_Tx_GetRemTxDataOfCurrentSegment()
 *********************************************************************************************************************/
/*! \brief        Returns the remaining data of currently active segment to be transmitted.
 *  \details      -
 *  \param[in]    TxConId       The connection ID for which the remaining data is calculated.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_Tx_GetRemTxDataOfCurrentSegment(PduIdType TxConId);

#endif /* (VIPC_TXCONNECTIONS == STD_ON) */

/**********************************************************************************************************************
 * vIpc_Tx_IsPduInfoPtrValidForCopyTxData()
 *********************************************************************************************************************/
/*! \brief        Returns if the PDU info pointer is valid.
 *  \details      -
 *  \param[in]    PduInfoPtr    The pointer to be checked for validity.
 *  \return       FALSE         The pointer is invalid.
 *                TRUE          The pointer is valid.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_Tx_IsPduInfoPtrValidForCopyTxData(
    CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

#if (VIPC_TXCONNECTIONS == STD_ON)
  
/**********************************************************************************************************************
 * vIpc_Tx_ResetConnection()
 *********************************************************************************************************************/
/*! \brief        Resets the Tx connection.
 *  \details      -
 *  \param[in]    ConId       The connection to be reset.
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxPduIdValid(ConId);
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_ResetConnection(PduIdType ConId);

/**********************************************************************************************************************
 * vIpc_Tx_ConfirmTransmission()
 *********************************************************************************************************************/
/*! \brief        Confirms the transmission to the upper layer.
 *  \details      -
 *  \param[in]    ConId       The connection on which the transmission is confirmed.
 *  \param[in]    Result      The result of transmission (success or failure).
 *  \context      TASK
 *  \reentrant    FALSE
 *  \spec         requires vIpc_XCfg_IsTxPduIdValid(ConId);
 *  \endspec
 *  \synchronous  TRUE
 *  \pre          -
 *  \ingroup      tx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_ConfirmTransmission(PduIdType ConId, Std_ReturnType Result);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_Tx_GetTotalSduLength()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_Tx_GetTotalSduLength(PduIdType TxConId)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType pduId             = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId);
  
  /*@ assert vIpc_XCfg_IsTxSduIdValid(pduId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */  
  const PduLengthType channelMaxLen = vIpc_XCfg_GetMaxLengthOfTxChannel(pduId);
  const PduLengthType payload       = vIpc_TxCon_GetRemPayloadLength(TxConId);
  PduLengthType completeLength;
  
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Given the requested length and the SF header fit in a single frame. */
  if ((payload + VIPC_MSG_HEADER_SIZE_SF) <= channelMaxLen) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /* #15 Then the message can be transmitted unsegmented. */
    completeLength = (PduLengthType)(payload + (PduLengthType) VIPC_MSG_HEADER_SIZE_SF);
  }
  /* #20 Otherwise, calculate the complete length: */
  else
  {
    const PduLengthType firstFrame = channelMaxLen;
    const PduLengthType firstFramePayload = channelMaxLen - VIPC_MSG_HEADER_SIZE_FF;
    const PduLengthType nonFirstFramePayload = payload - firstFramePayload;
    const PduLengthType maxLastFrame = channelMaxLen - VIPC_MSG_HEADER_SIZE_LF;

    /* #30 Given that the Tx data fits in first frame and last frame. */
    /* #35 Then no consecutive frame is required; calculate complete length. */
    if (nonFirstFramePayload <= maxLastFrame) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      const PduLengthType lastFrame = nonFirstFramePayload + VIPC_MSG_HEADER_SIZE_LF;
      completeLength = (PduLengthType)(firstFrame + lastFrame);
    }
    /* #40 Otherwise, calculate number of consecutive frames and length of last frame to calculate the complete length. */
    else
    {
      /* This works because LF & CF headers have the same length! 
       * If LF > CF then we would need to add an empty last frame
       * If LF < CF then we would need to remove a consecutive frame and add it to the last frame
       */
      const PduLengthType lastFramePayload = nonFirstFramePayload % (channelMaxLen - VIPC_MSG_HEADER_SIZE_CF);
      const PduLengthType lastFrame = (lastFramePayload > 0u) ? (lastFramePayload + VIPC_MSG_HEADER_SIZE_LF) : 0u;
      const PduLengthType consecutiveFrameNum = nonFirstFramePayload / (channelMaxLen - VIPC_MSG_HEADER_SIZE_CF);
      const PduLengthType consecutiveFrame = consecutiveFrameNum * (channelMaxLen);

      completeLength = (PduLengthType)(firstFrame + consecutiveFrame + lastFrame);
    }
  }

  return completeLength;
} /* vIpc_Tx_GetTotalSduLength */

/**********************************************************************************************************************
 * vIpc_Tx_InternalTransmit()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(Std_ReturnType, VIPC_CODE) vIpc_Tx_InternalTransmit(PduIdType TxConId,
                                                                           P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  const PduIdType chanId = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId);
  
  /*@ assert vIpc_XCfg_IsTxSduIdValid(chanId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  vIpc_TxCon_SetRemPayloadLength(TxConId, PduInfoPtr->SduLength);
  vIpc_TxCon_SetCurrentSegment(TxConId, 0u);

  /* #10 Calculate the full length of data including vIpc headers. */
  {
    const PduLengthType totalLength = vIpc_Tx_GetTotalSduLength(TxConId);
    vIpc_TxCon_SetRemCompleteLength(TxConId, totalLength);
    {
      const vIpc_MsgEventType msgEvent = vIpc_Tx_GetInitialMsgEvent(chanId, totalLength);
      vIpc_Tx_SetNextMsgType(TxConId, msgEvent);
    }
  }
  /* #20 Enqueue the requested transmission with SDU ID. */
  vIpc_PQ_Insert(TxConId);
  vIpc_TxCon_SetState(TxConId, VIPC_TX_CON_STATE_QUEUED);

  /* #30 If the lower layer is not busy, trigger transmission on lower layer. */
  if (vIpc_XCfg_IsLLBusy(chanId) == FALSE)
  {
    vIpc_Tx_Process(chanId);
  }

  return E_OK;
} /* vIpc_Tx_InternalTransmit */


/**********************************************************************************************************************
 * vIpc_Tx_GetInitialMsgEvent()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(vIpc_MsgEventType, VIPC_CODE) vIpc_Tx_GetInitialMsgEvent(PduIdType TxChannelId, PduLengthType TotalLength)
{
  const PduLengthType maxChnLen = vIpc_XCfg_GetMaxLengthOfTxChannel(TxChannelId);

  return (TotalLength <= maxChnLen) ? VIPC_MSG_EVENT_START_SF : VIPC_MSG_EVENT_START_FF;
}


/**********************************************************************************************************************
 * vIpc_Tx_SetNextMsgType()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_SetNextMsgType(PduIdType TxConId, vIpc_MsgEventType Event)
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_MessageType newMsgType;
  const vIpc_MessageType currentMsgType = vIpc_TxCon_GetMessageType(TxConId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If the message type has to be reset, set it to SF. */
  if (Event == VIPC_MSG_EVENT_RESET)
  {
    newMsgType = VIPC_MSG_SF;
  }
  /* #20 Otherwise determine new msg type based on current msg type and event: */
  else
  {
    switch (currentMsgType)
    {
      /* #30 If the current message type is SF:
       *       If the event start SF occurs, the message type remains.
       *       Otherwise if the event start FF occurs, set the message type to FF.
       *       Otherwise, reset the message type to SF and indicate an error. */
      case (VIPC_MSG_SF):
        if (Event == VIPC_MSG_EVENT_START_FF)
        {
          newMsgType = VIPC_MSG_FF;
        }
        else
        {
          newMsgType = VIPC_MSG_SF;
        }
        break;

      /* #40 If the current message type is FF or CF:
       *       If the event start CF occurs, set the message type to CF.
       *       Otherwise if the event start LF occurs, set the message type to LF.
       *       Otherwise, reset the message type to SF and indicate an error. */
      case (VIPC_MSG_FF):
      case (VIPC_MSG_CF):
        if (Event == VIPC_MSG_EVENT_START_CF)
        {
          newMsgType = VIPC_MSG_CF;
        }
        else if (Event == VIPC_MSG_EVENT_START_LF)
        {
          newMsgType = VIPC_MSG_LF;
        }
        else
        {
          newMsgType = VIPC_MSG_SF;
        }
        break;

      /* #50 If the current message type is LF or invalid:
       *       Reset the message type to SF and indicate an error. */
      default:
        newMsgType = VIPC_MSG_SF;
        break;
    }
  }

  vIpc_TxCon_SetMessageType(TxConId, newMsgType);
} /* vIpc_Tx_SetNextMsgType */


/**********************************************************************************************************************
 * vIpc_Tx_SetNextMsgType_AfterCopyHeader()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_SetNextMsgType_AfterCopyHeader(PduIdType TxConId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_MsgEventType eventMsg;
  const PduIdType chanId                = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const vIpc_MessageType currentMsgType = vIpc_TxCon_GetMessageType(TxConId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(chanId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Implementation ----------------------------------------------- */
  switch (currentMsgType)
  {
    /* #10 If this is a FF or CF:
     *       And the remaining Tx data does not fit in a LF, set type of next message to CF.
     *       Otherwise, set type of next message to LF. */
    case (VIPC_MSG_FF):
    case (VIPC_MSG_CF):
      if (vIpc_TxCon_GetRemCompleteLength(TxConId)
          > (PduLengthType)(2u * vIpc_XCfg_GetMaxLengthOfTxChannel(chanId)))
      {
        eventMsg = VIPC_MSG_EVENT_START_CF;
      }
      else
      {
        eventMsg = VIPC_MSG_EVENT_START_LF;
      }
      break;

    /* #20 Otherwise, reset message type to SF. */
    default:
      eventMsg = VIPC_MSG_EVENT_RESET;
      break;
  }

  vIpc_Tx_SetNextMsgType(TxConId, eventMsg);
} /* vIpc_Tx_SetNextMsgType_AfterCopyHeader */


/**********************************************************************************************************************
 * vIpc_Tx_CopyHeader()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_CopyHeader(PduIdType TxConId,
                                                           P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  const PduIdType chanId                = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const vIpc_MessageType currentMsgType = vIpc_TxCon_GetMessageType(TxConId);
  const uint8 address                   = vIpc_XCfg_GetAddressOfTxConnection(TxConId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(chanId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */
  
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy address and message type to lower layer's transmit buffer. */
  vIpc_Msg_SetAddress(PduInfoPtr, address);
  vIpc_Msg_SetType(PduInfoPtr, currentMsgType);

  switch (currentMsgType)
  {
    /* #20 If this is the first frame:
     *       Copy the overall payload length of this SDU to lower layer's transmit buffer. */
    case (VIPC_MSG_FF):
      vIpc_Msg_SetLength(PduInfoPtr, vIpc_TxCon_GetRemPayloadLength(TxConId));
      break;

    /* #30 Otherwise, if this is a consecutive frame:
      *       Copy sequence counter to lower layer's transmit buffer. */
    case (VIPC_MSG_CF):
      vIpc_Msg_SetSeqCtr(PduInfoPtr, vIpc_TxCon_GetCurrentSegment(TxConId));

      if (vIpc_TxCon_GetRemCompleteLength(TxConId)
          > (PduLengthType)(2u * vIpc_XCfg_GetMaxLengthOfTxChannel(chanId)))
      {
        vIpc_TxCon_IncCurrentSegment(TxConId, 1u);
      }
      break;

    /* #40 Otherwise, do nothing. */
    default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
      break;
  }
} /* vIpc_Tx_CopyHeader */

/**********************************************************************************************************************
 * vIpc_Tx_GetRemTxDataOfCurrentSegment()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(PduLengthType, VIPC_CODE) vIpc_Tx_GetRemTxDataOfCurrentSegment(PduIdType TxConId)
{
  /* ----- Local Variables ---------------------------------------------- */
  PduLengthType remTxDataOfSegment;
  const PduLengthType remCompleteLength  = vIpc_TxCon_GetRemCompleteLength(TxConId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const PduIdType chanId                 = vIpc_XCfg_GetTxChannelIdxOfTxConnections(TxConId);
  const PduLengthType maxLengthOfChannel = vIpc_XCfg_GetMaxLengthOfTxChannel(chanId);
  
  /*@ assert vIpc_XCfg_IsTxPduIdValid(chanId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Given that a whole segment has already been sent */
  if (vIpc_TxChan_GetAlreadySentDataOfCurrSeg(chanId) >= maxLengthOfChannel) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /* #11 Then the remaining data is zero */
    remTxDataOfSegment = 0u;
  }
  /* #20 Or given that the remaining length of the connection is at least as large as the max size of a channel */
  else if (remCompleteLength >= maxLengthOfChannel)
  {
    /* #21 Then the remaining data in the segment is the size of the channel without the already sent data */
    remTxDataOfSegment = maxLengthOfChannel - vIpc_TxChan_GetAlreadySentDataOfCurrSeg(chanId);
  }
  else
  {
    /* #30 Else the remaining segment length is equal to the remaining complete length */
    remTxDataOfSegment = remCompleteLength;
  }

  return remTxDataOfSegment;
} /* vIpc_Tx_GetRemTxDataOfCurrentSegment */

#endif /* (VIPC_TXCONNECTIONS == STD_ON) */

/**********************************************************************************************************************
 * vIpc_Tx_IsPduInfoPtrValidForCopyTxData()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(boolean, VIPC_CODE) vIpc_Tx_IsPduInfoPtrValidForCopyTxData(
    CONSTP2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If the PDU pointer is no NULL pointer:
   *       If the LL request number of available transmit data, PDU pointer is valid.
   *       Otherwise, if the data pointer points to valid data, PDU pointer is valid.
   *     Otherwise, the PDU pointer is invalid. */
  if (PduInfoPtr != NULL_PTR)
  {
    if (PduInfoPtr->SduLength == 0u)
    {
      retVal = TRUE;
    }
    else if (PduInfoPtr->SduDataPtr != NULL_PTR)
    {
      retVal = TRUE;
    }
    else
    {
      retVal = FALSE;
    }
  }

  return retVal;
} /* vIpc_Tx_IsPduInfoPtrValidForCopyTxData */

#if (VIPC_TXCONNECTIONS == STD_ON)

/**********************************************************************************************************************
 * vIpc_Tx_ResetConnection()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_ResetConnection(PduIdType ConId)
{
  /* #10 Remove connection from queue
   *     Reset connection to idle state
   *     Set payload etc. to 0 */
  vIpc_PQ_Remove(ConId);
  vIpc_Tx_SetNextMsgType(ConId, VIPC_MSG_EVENT_RESET);
  vIpc_TxCon_SetState(ConId, VIPC_TX_CON_STATE_IDLE);
  vIpc_TxCon_SetCurrentSegment(ConId, 0u);
  vIpc_TxCon_SetRemCompleteLength(ConId, (PduLengthType) 0u);
  vIpc_TxCon_SetRemPayloadLength(ConId, 0u);
} /* vIpc_Tx_ResetConnection */

/**********************************************************************************************************************
 * vIpc_Tx_ConfirmTransmission()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(void, VIPC_CODE) vIpc_Tx_ConfirmTransmission(PduIdType ConId, Std_ReturnType Result)
{
  vIpc_XCfg_GetTxConfirmationOfTxUlCallbacks(ConId)(vIpc_XCfg_GetUpperLayerIdOfTxConnections(ConId), Result);
} /* vIpc_Tx_ConfirmTransmission */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_Tx_Process()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
FUNC(void, VIPC_CODE) vIpc_Tx_Process(PduIdType TxChannel)
{
  /* ----- Local Variables ---------------------------------------------- */
  PduInfoType pdu;
  PduIdType activeConnection;
  Std_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Enter VIPC_EXCLUSIVE_AREA_TX_PROCESS. */
  SchM_Enter_vIpc_VIPC_EXCLUSIVE_AREA_TX_PROCESS();

  /* #20 Get the queued connection with the highest priority. */
  retVal = vIpc_PQ_Peek(TxChannel, &activeConnection);
  
  /* #30 If there was a queued transmission request and the Tx channel is idle. */
  if ((retVal == E_OK)
      && (vIpc_TxChan_GetIsActiveTxConAvailable(TxChannel) == FALSE))
  {
    /*@ assert vIpc_XCfg_IsTxSduIdValid(activeConnection); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
    
    /* Only length is relevant for transmit */
    pdu.SduDataPtr = NULL_PTR; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

    /* #40 Set new connection active. */
    vIpc_TxChan_SetAlreadySentDataOfCurrSeg(TxChannel, 0u);
    vIpc_TxChan_SetActiveTxConId(TxChannel, activeConnection);
    vIpc_TxChan_SetActiveTxConAvailable(TxChannel, TRUE);

    /* #50 Leave critical section. */
    SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_PROCESS();

    /* #60 If the SDU length fits in a single frame, request sending this data. */
    if (vIpc_TxCon_GetRemCompleteLength(activeConnection) <= vIpc_XCfg_GetMaxLengthOfTxChannel(TxChannel)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      pdu.SduLength = vIpc_TxCon_GetRemCompleteLength(activeConnection);
    }
    /* #70 Otherwise:
     *       Set the transmission length to the max. length of lower layer.
     *       Trigger transmission and re-enqueue the connection transmission request. */
    else
    {
      pdu.SduLength = vIpc_XCfg_GetMaxLengthOfTxChannel(TxChannel);
    }
    
    if (pdu.SduLength == 0u) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      retVal = E_NOT_OK;
    }
    else
    {
      /*@ assert vIpc_XCfg_IsTxSduIdValid(activeConnection); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
      
      vIpc_TxCon_SetState(activeConnection, VIPC_TX_CON_STATE_ACTIVE_TX);
      retVal = vIpc_XCfg_GetTransmitOfTransmitLLFunctionTable(TxChannel)( /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
        vIpc_XCfg_GetLowerLayerIdOfTxChannel(TxChannel), &pdu);
    }

    if (retVal == E_NOT_OK) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /*@ assert vIpc_XCfg_IsTxSduIdValid(activeConnection); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
      vIpc_TxChan_SetActiveTxConAvailable(TxChannel, FALSE);
      vIpc_TxCon_SetState(activeConnection, VIPC_TX_CON_STATE_QUEUED);
    }
  }
  /* #80 Otherwise, leave VIPC_EXCLUSIVE_AREA_TX_PROCESS. */
  else
  {
    SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_PROCESS();
  }
} /* vIpc_Tx_Process */

#endif  /* (VIPC_TXCONNECTIONS == STD_ON) */ 

/**********************************************************************************************************************
 * vIpc_Transmit()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
/* PRQA S 6080 1 */ /* MD_MSR_STMIF */
FUNC(Std_ReturnType, VIPC_CODE) vIpc_Transmit(PduIdType TxSduId, /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal  = E_NOT_OK;
  vIpc_ErrorCode errorId = VIPC_E_NO_ERROR; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  const boolean isDetEnabled = vIpc_XCfg_DevErrorDetect();
  const boolean isPduDataValid = vIpc_XCfg_IsPduDataValid(PduInfoPtr); /* PRQA S 1338, 2983, 3112 2 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const boolean isPduDataLenValid = vIpc_XCfg_IsPduDataLenValid(PduInfoPtr);
  
  /* ----- Development Error Checks ------------------------------------- */
  /* #10 If vIpc is not initialized or an invalid parameter is given, report an error. */
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  }
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsTxSduIdValid(TxSduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_SDU_ID;
  }
  else if ((isDetEnabled == TRUE) && (isPduDataValid == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_POINTER;
  }
  else if ((isDetEnabled == TRUE) && (isPduDataLenValid == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_CONFIG;
  }
  /* #20 If the connection is idle, forward the transmission request. */
  else if (vIpc_TxCon_GetState(TxSduId) == VIPC_TX_CON_STATE_IDLE) 
  {
#if (VIPC_TXCONNECTIONS == STD_ON) 
    /* ----- Implementation ----------------------------------------------- */
    retVal = vIpc_Tx_InternalTransmit(TxSduId, PduInfoPtr);
#endif  /* (VIPC_TXCONNECTIONS == STD_ON) */ 
  }
  else
  {
    /* #30 Otherwise indicate an error to the upperlayer */
    errorId = VIPC_E_TX_PENDING;
  }

  /* ----- Development Error Report --------------------------------------- */
  retVal |= vIpc_XCfg_ReportError(VIPC_SID_TRANSMIT, errorId); /* PRQA S 2985 */ /* MD_MSR_ConstantCondition */

  return retVal;
} /* vIpc_Transmit */

/**********************************************************************************************************************
 * vIpc_CopyTxData()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6030 3 */ /* MD_MSR_STCYC */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
/* PRQA S 6080 1 */ /* MD_MSR_STMIF */
FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_CopyTxData(PduIdType TxPduId, /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
                                                   P2VAR(PduInfoType, AUTOMATIC, VIPC_APPL_VAR) PduInfoPtr, /* PRQA S 3673 2 */ /* MD_MSR_Rule8.13 */
                                                   P2VAR(RetryInfoType, AUTOMATIC, VIPC_APPL_VAR) RetryPtr,
                                                   P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) AvailableDataPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;
  vIpc_ErrorCode errorId   = VIPC_E_NO_ERROR; /* PRQA S 2981  */ /* MD_MSR_RetVal */
  const boolean isDetEnabled     = vIpc_XCfg_DevErrorDetect();
  const boolean isPduInfoPtrValidForCopyTxData = vIpc_Tx_IsPduInfoPtrValidForCopyTxData(PduInfoPtr); /* PRQA S 1338, 2983, 3112 2 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* ----- Development Error Checks ------------------------------------- */
  /* #10 If vIpc is not initialized or an invalid parameter is given, report an error. */
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  }
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsTxPduIdValid(TxPduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_PDU_ID;
  }
  else if ((isDetEnabled == TRUE) && (isPduInfoPtrValidForCopyTxData == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_POINTER;
  }
  /* #20 Otherwise determine correct parameters and perform copy operation if applicable: */
  else
  {
#if (VIPC_TXCONNECTIONS == STD_ON)     
    /* ----- Implementation ----------------------------------------------- */
    /* #30 If there is an active connection for transmission: */
    const boolean isActiveTxConAvailable = vIpc_TxChan_GetIsActiveTxConAvailable(TxPduId);
    if (isActiveTxConAvailable == TRUE)
    {
      const PduIdType conId = vIpc_TxChan_GetActiveTxConId(TxPduId);
      
      /* @ assert vIpc_XCfg_IsTxSduIdValid(conId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */

      /* #40 If the LL indicates currently no buffer available:
       *       Forward CopyTxData to UL and indicate LL how much data can be copied. */
      if (PduInfoPtr->SduLength == 0u)
      {
        retVal = vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks(conId)(vIpc_XCfg_GetUpperLayerIdOfTxConnections(conId), /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
                                                               PduInfoPtr,
                                                               RetryPtr,
                                                               AvailableDataPtr);

        AvailableDataPtr[0] = vIpc_Tx_GetRemTxDataOfCurrentSegment(conId);
      }
      /* #50 Otherwise continue checks: */
      else
      {
        const PduLengthType alreadySentData = vIpc_TxChan_GetAlreadySentDataOfCurrSeg(TxPduId);
        PduLengthType headerLength = 0u;

        /* #60 If there is a new segment to be sent, first reserve the place for the vIpc header. */
        if (alreadySentData == 0u)
        {
          headerLength = vIpc_Tx_HeaderType2HeaderLen[(uint8)(vIpc_TxCon_GetMessageType(conId))];
        }

        /* #70 If there is not enough Tx buffer provided by the lower layer for the header, indicate an error. */
        if (PduInfoPtr->SduLength < headerLength)
        {
          errorId = VIPC_E_INV_CONFIG;
        }
        /* #80 Otherwise forward call to perform copy operation: */
        else
        {
          PduInfoType locPduInfoPtr;
          locPduInfoPtr.SduDataPtr = &PduInfoPtr->SduDataPtr[headerLength];
          locPduInfoPtr.SduLength  = PduInfoPtr->SduLength - headerLength;

          /* #90 Forward the remaining Tx buffer space for copying data by upper layer. */
          retVal = vIpc_XCfg_GetCopyTxDataOfTxUlCallbacks(conId)(vIpc_XCfg_GetUpperLayerIdOfTxConnections(conId), /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
                                                                 &locPduInfoPtr,
                                                                 RetryPtr,
                                                                 AvailableDataPtr);

          /* #100 If the upper layer succeeded:
           *        If there is a new segment to be sent, first copy vIpc header.
           *        Calculate remaining data to be copied.
           *        If complete data is copied, set the connection state to VIPC_TX_CON_STATE_WAIT_FOR_TX_CONF. */
          if (retVal == BUFREQ_OK)
          {
            if (alreadySentData == 0u)
            {
              vIpc_Tx_CopyHeader(conId, PduInfoPtr);
              vIpc_Tx_SetNextMsgType_AfterCopyHeader(conId);
            }

            vIpc_TxChan_IncAlreadySentDataOfCurrSeg(TxPduId, PduInfoPtr->SduLength);
            vIpc_TxCon_DecRemCompleteLength(conId, PduInfoPtr->SduLength);
            vIpc_TxCon_DecRemPayloadLength(conId, locPduInfoPtr.SduLength);

            {
              PduLengthType remainingData = vIpc_Tx_GetRemTxDataOfCurrentSegment(conId);
              AvailableDataPtr[0] = remainingData;

              if (remainingData == 0u)
              {
                vIpc_TxCon_SetState(conId, VIPC_TX_CON_STATE_WAIT_FOR_TX_CONF);
              }
            }
          }
        }
      }
    }
#else 
    VIPC_DUMMY_STATEMENT(PduInfoPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    VIPC_DUMMY_STATEMENT(RetryPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    VIPC_DUMMY_STATEMENT(AvailableDataPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif /* (VIPC_TXCONNECTIONS == STD_ON) */ 
  }

  /* ----- Development Error Report --------------------------------------- */
  (void) vIpc_XCfg_ReportError(VIPC_SID_COPYTXDATA, errorId);

  VIPC_DUMMY_STATEMENT(RetryPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  return retVal;  
} /* vIpc_CopyTxData */

/**********************************************************************************************************************
 * vIpc_TxConfirmation()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 1 */ /* MD_MSR_STCAL */
FUNC(void, VIPC_CODE) vIpc_TxConfirmation(PduIdType TxPduId, Std_ReturnType Result) /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_ErrorCode errorId = VIPC_E_NO_ERROR; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  const boolean isDetEnabled   = vIpc_XCfg_DevErrorDetect();
  PduIdType conId        = vIpc_TxChan_GetActiveTxConId(TxPduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  
  /* @ assert vIpc_XCfg_IsTxPduIdValid(conId); */ /* VCA_VIPC_VALID_CONFIGURED_CHANNEL_IDX */

  /* ----- Development Error Checks ------------------------------------- */
  /* #10 Given Development Errors are enabled 
   *       And vIpc is not initialized
   *       Or an invalid parameter is given
   *         Then report an error. */
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  } 
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsTxPduIdValid(TxPduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_PDU_ID;
  }
  else
  {
#if (VIPC_TXCONNECTIONS == STD_ON)     
    /* ----- Implementation ----------------------------------------------- */
    
    /* #20 Otherwise enter VIPC_EXCLUSIVE_AREA_TX_PROCESS. */
    SchM_Enter_vIpc_VIPC_EXCLUSIVE_AREA_TX_PROCESS();
    
    /* #30 Determine correct parameters and perform TxConfirmation if applicable: */
    vIpc_TxChan_InitRemainingSeparationCycles(TxPduId);
    vIpc_TxChan_SetAlreadySentDataOfCurrSeg(TxPduId, 0u);
    vIpc_TxChan_SetActiveTxConId(TxPduId, 0u); 
    vIpc_TxChan_SetActiveTxConAvailable(TxPduId, FALSE);

    /* #40 Check if we expected a Tx Confirmation */
    if (vIpc_TxCon_GetState(conId) != VIPC_TX_CON_STATE_WAIT_FOR_TX_CONF)
    {
      Result = E_NOT_OK; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    }

    /* #50 Given the lower layer successfully transmitted a segment
           And there is still data to be sent */
    if ((Result == E_OK) && (vIpc_TxCon_GetRemPayloadLength(conId) > 0u))
    {
      /* #60 Then re-schedule this connection. */
      vIpc_TxCon_SetState(conId, VIPC_TX_CON_STATE_QUEUED);
    }
    else
    {
      /* #70 Otherwise, reset connection data and forward the TxConfirmation to indicate the UL that the
       *     transmission has terminated. */
      vIpc_Tx_ResetConnection(conId);

      vIpc_Tx_ConfirmTransmission(conId, Result);
    }

    /* #80 Exit VIPC_EXCLUSIVE_AREA_TX_PROCESS. */
    SchM_Exit_vIpc_VIPC_EXCLUSIVE_AREA_TX_PROCESS();

    /* #90 Given the channel is allowed to transmit immediately */
    if (vIpc_XCfg_GetNumSepCyclesOfTxChannel(TxPduId) == 0u) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /* #91 Then trigger the next transmission */
      vIpc_Tx_Process(TxPduId);
    }
#else 
    VIPC_DUMMY_STATEMENT(Result); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    VIPC_DUMMY_STATEMENT(conId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif /* (VIPC_TXCONNECTIONS == STD_ON) */ 
  }

  /* ----- Development Error Report --------------------------------------- */
  (void) vIpc_XCfg_ReportError(VIPC_SID_TXCONFIRMATION, errorId);
} /* vIpc_TxConfirmation */

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpc_Tx.c
 *********************************************************************************************************************/
