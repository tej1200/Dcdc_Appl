/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  vIpc_Rx.c
 *        \brief  vIpc_Rx source file
 *
 *      \details  This file contains the functions required to receive vIpc frames.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VIPC_RX_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpc_Rx.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#if (VIPC_RXCONNECTIONS == STD_ON)
/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/
#define VIPC_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VIPC_LOCAL CONST(uint8, VIPC_CONST) vIpc_Rx_HeaderType2HeaderLen[4] =
{
  VIPC_MSG_HEADER_SIZE_SF,
  VIPC_MSG_HEADER_SIZE_FF,
  VIPC_MSG_HEADER_SIZE_CF,
  VIPC_MSG_HEADER_SIZE_LF
};

#define VIPC_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if (VIPC_RXCONNECTIONS == STD_ON)

/**********************************************************************************************************************
 * vIpc_Rx_GetSduIdOfPduIdAndAddress()
 *********************************************************************************************************************/
/*! \brief         Returns the Rx Connection Id mapped to the couple of Rx Channel and Address.
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(PduIdType, VIPC_CODE) vIpc_Rx_GetSduIdOfPduIdAndAddress(PduIdType RxPduId,
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 * vIpc_Rx_Process()
 *********************************************************************************************************************/
/*! \brief         Processes the start of reception.
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \param[in]     TpSduLength   The length of received data.
 *  \param[out]    BufferSizePtr The pointer to the available buffer.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \spec          requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *                 requires PduInfoPtr != NULL_PTR;
 *                 requires BufferSizePtr != NULL_PTR;
 *                 requires TpSduLength >= 1;
 *  \endspec
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE)
  vIpc_Rx_Process(PduIdType RxPduId,
                  P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                  PduLengthType TpSduLength,
                  P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr);

/**********************************************************************************************************************
 * vIpc_Rx_Process_SF()
 *********************************************************************************************************************/
/*! \brief         Processes the start of reception for single frames (SF).
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \param[in]     TpSduLength   The length of received data.
 *  \param[out]    BufferSizePtr The pointer to the available buffer.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \spec          requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *                 requires PduInfoPtr != NULL_PTR;
 *                 requires BufferSizePtr != NULL_PTR;
 *                 requires TpSduLength >= 1;
 *  \endspec
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_Rx_Process_SF(PduIdType RxPduId,
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                                              PduLengthType TpSduLength,
                                              P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr);

/**********************************************************************************************************************
 * vIpc_Rx_Process_FF()
 *********************************************************************************************************************/
/*! \brief         Processes the start of reception for first frames (FF).
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \param[in]     TpSduLength   The length of received data.
 *  \param[out]    BufferSizePtr The pointer to the available buffer.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \spec          requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *                 requires PduInfoPtr != NULL_PTR;
 *                 requires BufferSizePtr != NULL_PTR;
 *                 requires TpSduLength >= 1;
 *  \endspec
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_Rx_Process_FF(PduIdType RxPduId,
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                                              PduLengthType TpSduLength,
                                              P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr);

/**********************************************************************************************************************
 * vIpc_Rx_Process_CF()
 *********************************************************************************************************************/
/*! \brief         Processes the start of reception for consecutive frames (CF).
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \param[in]     TpSduLength   The length of received data.
 *  \param[out]    BufferSizePtr The pointer to the available buffer.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \spec          requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *                 requires PduInfoPtr != NULL_PTR;
 *                 requires BufferSizePtr != NULL_PTR;
 *                 requires TpSduLength >= 1;
 *  \endspec
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_Rx_Process_CF(PduIdType RxPduId,
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                                              PduLengthType TpSduLength,
                                              P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr);

/**********************************************************************************************************************
 * vIpc_Rx_Process_LF()
 *********************************************************************************************************************/
/*! \brief         Processes the start of reception for last frames (LF).
 *  \details       -
 *  \param[in]     RxPduId       The Rx channel on which the data was received.
 *  \param[in,out] PduInfoPtr    The pointer to the received data.
 *  \param[in]     TpSduLength   The length of received data.
 *  \param[out]    BufferSizePtr The pointer to the available buffer.
 *  \context       TASK
 *  \reentrant     FALSE
 *  \spec          requires vIpc_XCfg_IsRxPduIdValid(RxPduId);
 *                 requires PduInfoPtr != NULL_PTR;
 *                 requires BufferSizePtr != NULL_PTR;
 *                 requires TpSduLength >= 1;
 *  \endspec
 *  \synchronous   TRUE
 *  \pre           -
 *  \ingroup       rx
 *********************************************************************************************************************/
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_Rx_Process_LF(PduIdType RxPduId,
                                              P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                                              PduLengthType TpSduLength,
                                              P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_Rx_GetSduIdOfPduIdAndAddress()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(PduIdType, VIPC_CODE)
  vIpc_Rx_GetSduIdOfPduIdAndAddress(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr)
{
  /* #10 Calculate the upper layer sduId based on the RxPduId and the protocoll address. */
  /* PRQA S 2986 1 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  return (PduIdType)((PduIdType) vIpc_XCfg_GetAddressStartIdxOfRxChannel(RxPduId) + vIpc_Msg_GetAddress(PduInfoPtr)); 
} /* vIpc_Rx_GetSduIdOfPduIdAndAddress */


/**********************************************************************************************************************
 * vIpc_Rx_Process()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE) vIpc_Rx_Process(PduIdType RxPduId,
                                                                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                                                                     PduLengthType TpSduLength,
                                                                     P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  BufReq_ReturnType retVal;
  const uint8 frameType = vIpc_Msg_GetType(PduInfoPtr);

  /* #10 Process frame according to frametype */
  switch (frameType)
  {
    case VIPC_MSG_SF:
      retVal = vIpc_Rx_Process_SF(RxPduId, PduInfoPtr, TpSduLength, BufferSizePtr);
      break;
    case VIPC_MSG_FF:
      retVal = vIpc_Rx_Process_FF(RxPduId, PduInfoPtr, TpSduLength, BufferSizePtr);
      break;
    case VIPC_MSG_CF:
      retVal = vIpc_Rx_Process_CF(RxPduId, PduInfoPtr, TpSduLength, BufferSizePtr);
      break;
    case VIPC_MSG_LF:
      retVal = vIpc_Rx_Process_LF(RxPduId, PduInfoPtr, TpSduLength, BufferSizePtr);
      break;
    default:
      /* No valid frame type? */
      retVal = BUFREQ_E_NOT_OK;
      break;
  }

  return retVal;
} /* vIpc_Rx_Process */

/**********************************************************************************************************************
 * vIpc_Rx_Process_SF()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE)
  vIpc_Rx_Process_SF(PduIdType RxPduId,
                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                     PduLengthType TpSduLength,
                     P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  PduInfoType ULInfoPtr;
  BufReq_ReturnType retVal;

  /* #10 Received address is in valid range, store intermediate sdu id for later use in CopyRxData and RxIndication. */
  const PduIdType sduId   = vIpc_Rx_GetSduIdOfPduIdAndAddress(RxPduId, PduInfoPtr);
  
  /*@ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
  
  const PduIdType ulPduId = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId);
  const uint8 headerLen   = vIpc_Rx_HeaderType2HeaderLen[VIPC_MSG_SF];

  /* ----- Implementation ----------------------------------------------- */

  vIpc_XCfg_SetRxPduConnId(RxPduId, sduId);

  /* #20 First check if we are in the correct state */
  if (vIpc_XCfg_GetRxCon_State(sduId) != VIPC_RX_SDU_STATE_IDLE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /* #30 We received an incomplete sequence. Give RxIndication with E_NOT_OK to close it. */
    vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, E_NOT_OK); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
    vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
  }

  /* #40 Forward start of reception minus vIpc header information. */
  ULInfoPtr.SduDataPtr = &PduInfoPtr->SduDataPtr[headerLen];
  ULInfoPtr.SduLength  = PduInfoPtr->SduLength - headerLen;

  /* #50 Memorize TP API params for later use in CopyRxData and RxIndication. */
  vIpc_XCfg_SetSeqCtr(sduId, 0u);
  vIpc_XCfg_SetRxPduIsHeaderSent(RxPduId, FALSE);
  vIpc_XCfg_SetRxPayloadLength(sduId, ULInfoPtr.SduLength);
  vIpc_XCfg_SetRxRemainingPayloadLength(sduId, ULInfoPtr.SduLength);

  retVal =
    vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks(sduId)(ulPduId, &ULInfoPtr, TpSduLength - headerLen, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
  /* #60 Multiple headerLen might be required. This can not be calculated here as the number of segments is missing.
   *     A single header will probably sufficient to satisfy the lower layer.
   *     If the upper layer returned a size of 0 we do not add the vIpc header information, but return the zero unmodified.
   */
  if (*BufferSizePtr > 0u)
  {
    *BufferSizePtr += headerLen;
  }

  if (retVal == BUFREQ_OK)
  {
    vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_WAIT_FOR_RX_IND);
    vIpc_XCfg_SetFrameType(sduId, VIPC_MSG_SF);
  }

  return retVal;
} /* vIpc_Rx_Process_SF */

/**********************************************************************************************************************
 * vIpc_Rx_Process_FF()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE)
  vIpc_Rx_Process_FF(PduIdType RxPduId,
                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                     PduLengthType TpSduLength,
                     P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  PduInfoType ULInfoPtr;
  BufReq_ReturnType retVal;
  
  /* #10 Received address is in valid range, store intermediate sdu id for later use in CopyRxData and RxIndication. */
  const PduIdType sduId   = vIpc_Rx_GetSduIdOfPduIdAndAddress(RxPduId, PduInfoPtr);
  
  /* @ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
  const PduIdType ulPduId = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId);
  const uint8 headerLen   = vIpc_Rx_HeaderType2HeaderLen[VIPC_MSG_FF];
  const uint32 length = vIpc_Msg_GetLength(PduInfoPtr);
  
  VIPC_DUMMY_STATEMENT(TpSduLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* ----- Implementation ----------------------------------------------- */

  vIpc_XCfg_SetRxPduConnId(RxPduId, sduId);

  /* #20 First check if we are in the correct state. */
  if (vIpc_XCfg_GetRxCon_State(sduId) != VIPC_RX_SDU_STATE_IDLE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /* #30 We received an incomplete sequence. Give RxIndication with E_NOT_OK to close it. */
    vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, E_NOT_OK); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
    vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
  }

  /* #40 Forward start of reception minus vIpc header information. */
  ULInfoPtr.SduDataPtr = &PduInfoPtr->SduDataPtr[headerLen];
  ULInfoPtr.SduLength  = PduInfoPtr->SduLength - headerLen;

  /* #50 Memorize TP API params for later use in CopyRxData and RxIndication. */
  vIpc_XCfg_SetSeqCtr(sduId, 0u);
  vIpc_XCfg_SetRxPduIsHeaderSent(RxPduId, FALSE);
  vIpc_XCfg_SetRxPayloadLength(sduId, length);
  vIpc_XCfg_SetRxRemainingPayloadLength(sduId, length);

  retVal =
    vIpc_XCfg_GetStartOfReceptionOfRxUlCallbacks(sduId)(ulPduId, &ULInfoPtr, (PduLengthType)length, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
  /* #60 Multiple headerLen might be required. This can not be calculated here as the number of segments is missing.
   *     A single header will probably sufficient to satisfy the lower layer.
   *     If the upper layer returned a size of 0 we do not add the vIpc header information, but return the zero unmodified.
   */
  if (*BufferSizePtr > 0u)
  {
    *BufferSizePtr += headerLen;
  }

  if (retVal == BUFREQ_OK)
  {
    vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_WAIT_FOR_CF);
    vIpc_XCfg_SetFrameType(sduId, VIPC_MSG_FF);
  }

  return retVal;
} /* vIpc_Rx_Process_FF */

/**********************************************************************************************************************
 * vIpc_Rx_Process_CF()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE)
  vIpc_Rx_Process_CF(PduIdType RxPduId,
                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                     PduLengthType TpSduLength,
                     P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;

  /* #10 Received address is in valid range, store intermediate sdu id for later use in CopyRxData and RxIndication. */
  const PduIdType sduId   = vIpc_Rx_GetSduIdOfPduIdAndAddress(RxPduId, PduInfoPtr);
  
  /* @ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
  const PduIdType ulPduId = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const uint8 headerLen   = vIpc_Rx_HeaderType2HeaderLen[VIPC_MSG_CF]; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(TpSduLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  vIpc_XCfg_SetRxPduConnId(RxPduId, sduId);

  /* #20 Are we in the correct state to receive a CF. */
  if (vIpc_XCfg_GetRxCon_State(sduId) == VIPC_RX_SDU_STATE_WAIT_FOR_CF) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    /* #30 Is the sequence counter correct? */
    if (vIpc_XCfg_GetSeqCtr(sduId) == vIpc_Msg_GetSeqCtr(PduInfoPtr))
    {
      PduInfoType ULInfoPtr = {NULL_PTR, 0u};

      /* #40 Yes, the sequence is correct, prepare for CopyRxData. */
      vIpc_XCfg_SetSeqCtr(sduId, (vIpc_XCfg_GetSeqCtr(sduId) + 1u) & VIPC_MSG_SEQCTR_MASK);
      vIpc_XCfg_SetRxPduIsHeaderSent(RxPduId, FALSE);

      (void) vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(sduId)(ulPduId, &ULInfoPtr, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
      vIpc_XCfg_SetFrameType(sduId, VIPC_MSG_CF);

      *BufferSizePtr += headerLen;
    }
    else
    {
      /* #50 We received a wrong sequence. Give RxIndication with E_NOT_OK to close it. */
      vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, E_NOT_OK); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
      vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
      vIpc_XCfg_SetRxRemainingPayloadLength(sduId, 0u);

      *BufferSizePtr = 0u;
    }

    retVal = BUFREQ_OK;
  }
  else
  {
    if (vIpc_XCfg_GetRxCon_State(sduId) != VIPC_RX_SDU_STATE_IDLE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /* #60 We received an incomplete sequence. Give RxIndication with E_NOT_OK to close it. */
      vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, E_NOT_OK); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
      vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
      vIpc_XCfg_SetRxRemainingPayloadLength(sduId, 0u);
    }

    *BufferSizePtr = 0u;
  }

  return retVal;
} /* vIpc_Rx_Process_CF */


/**********************************************************************************************************************
 * vIpc_Rx_Process_LF()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
VIPC_LOCAL_INLINE FUNC(BufReq_ReturnType, VIPC_CODE)
  vIpc_Rx_Process_LF(PduIdType RxPduId,
                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,
                     PduLengthType TpSduLength,
                     P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal;

  /* #10 Received address is in valid range, store intermediate sdu id for later use in CopyRxData and RxIndication. */
  const PduIdType sduId   = vIpc_Rx_GetSduIdOfPduIdAndAddress(RxPduId, PduInfoPtr);
  
  /* @ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
  const PduIdType ulPduId = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const uint8 headerLen   = vIpc_Rx_HeaderType2HeaderLen[VIPC_MSG_LF]; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* ----- Implementation ----------------------------------------------- */
  VIPC_DUMMY_STATEMENT(TpSduLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  vIpc_XCfg_SetRxPduConnId(RxPduId, sduId);

  vIpc_XCfg_SetRxPduIsHeaderSent(RxPduId, FALSE);
  /* #20 Are we in the correct state to process the LF? */
  if (vIpc_XCfg_GetRxCon_State(sduId) == VIPC_RX_SDU_STATE_WAIT_FOR_CF) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    PduInfoType ULInfoPtr = {NULL_PTR, 0u};

    vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_WAIT_FOR_RX_IND);

    /* #30 Request pending buffer size and forward it to lower layer */
    (void) vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(sduId)(ulPduId, &ULInfoPtr, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
    vIpc_XCfg_SetFrameType(sduId, VIPC_MSG_LF);

    *BufferSizePtr += headerLen;
    retVal = BUFREQ_OK;
  }
  else
  {
    if (vIpc_XCfg_GetRxCon_State(sduId) != VIPC_RX_SDU_STATE_IDLE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /* #40 We received an incomplete sequence. Give RxIndication with E_NOT_OK to close it. */
      vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, E_NOT_OK); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
      vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
    }

    *BufferSizePtr = 0u;
    retVal         = BUFREQ_E_NOT_OK;
  }

  return retVal;
} /* vIpc_Rx_Process_LF */

#endif /* (VIPC_RXCONNECTIONS == STD_ON) */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vIpc_RxIndication()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
/* PRQA S 6080 1 */ /* MD_MSR_STMIF */
FUNC(void, VIPC_CODE) vIpc_RxIndication(PduIdType RxPduId, Std_ReturnType Result) /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
{
  /* ----- Local Variables ---------------------------------------------- */
  vIpc_ErrorCode errorId     = VIPC_E_NO_ERROR; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  const boolean isDetEnabled = vIpc_XCfg_DevErrorDetect();

  /* #10 If vIpc is not initialized or an invalid parameter is given, report an error. */
  
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  }
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsRxPduIdValid(RxPduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_PDU_ID;
  }
  else
  {
#if (VIPC_RXCONNECTIONS == STD_ON)    
    /* ----- Implementation ----------------------------------------------- */
    /* #20 When no errors have been detected, process the incoming Pdu. */
    const PduIdType sduId      = vIpc_XCfg_GetRxPduConnId(RxPduId);
    
    /*@ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */
    const PduIdType ulPduId    = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId);
    const boolean inStateRxInd = (boolean)(vIpc_XCfg_GetRxCon_State(sduId) == VIPC_RX_SDU_STATE_WAIT_FOR_RX_IND); /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    
    /* #30 If the reception failed or succeeded */
    if ((Result != E_OK) || (inStateRxInd == TRUE))
    {
      /* #40 If not all data bytes were received */
      if ((inStateRxInd == TRUE) && (vIpc_XCfg_GetRxRemainingPayloadLength(sduId) > 0u))
      {
        /* #50 Then give RxIndication with E_NOT_OK to close it. */
        Result = E_NOT_OK; /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
      }
      vIpc_XCfg_GetRxIndicationOfRxUlCallbacks(sduId)(ulPduId, Result); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
      vIpc_XCfg_SetRxCon_State(sduId, VIPC_RX_SDU_STATE_IDLE);
    }
#else
  VIPC_DUMMY_STATEMENT(Result); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif /* (VIPC_RXCONNECTIONS == STD_ON) */
  }

  (void) vIpc_XCfg_ReportError(VIPC_SID_RXINDICATION, errorId);
} /* vIpc_RxIndication */


/**********************************************************************************************************************
 * vIpc_CopyRxData()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6080 3 */ /* MD_MSR_STMIF */
/* PRQA S 6050 2 */ /* MD_MSR_STCAL */
FUNC(BufReq_ReturnType, VIPC_CODE)
vIpc_CopyRxData(PduIdType RxPduId, /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
                P2VAR(PduInfoType, AUTOMATIC, VIPC_APPL_VAR) PduInfoPtr, /* PRQA S 3673 1 */ /* MD_MSR_Rule8.13 */
                P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr) 
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal   = BUFREQ_E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vIpc_ErrorCode errorId     = VIPC_E_NO_ERROR; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  const boolean isDetEnabled = vIpc_XCfg_DevErrorDetect();
  const boolean isBufSizePtrValid = vIpc_XCfg_IsBufSizePtrValid(BufferSizePtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */

  /* #10 If vIpc is not initialized or an invalid parameter is given, report an error. */
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  }
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsRxPduIdValid(RxPduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_PDU_ID;
  }
  else if ((isDetEnabled == TRUE) && (isBufSizePtrValid == FALSE))  /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_POINTER; 
  }
  else
  {
#if (VIPC_RXCONNECTIONS == STD_ON)    
    /* ----- Implementation ----------------------------------------------- */
    /* #20 When no errors have been detected, process the incoming Pdu. */
    const PduIdType sduId = vIpc_XCfg_GetRxPduConnId(RxPduId);
    
    /*@ assert vIpc_XCfg_IsRxPduIdValid(sduId); */ /* VCA_VIPC_VALID_CONFIGURED_CONNECTION_IDX */

    /* #30 If we are in the correct state, we process and forward the call to the upper layer. */
    if (vIpc_XCfg_GetRxCon_State(sduId) != VIPC_RX_SDU_STATE_IDLE) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      const PduIdType ulPduId = vIpc_XCfg_GetUpperLayerIdOfRxConnections(sduId);

      /* #40 Query buffer size information use case. */
      if (PduInfoPtr->SduLength == 0u)
      {
        retVal = vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(sduId)(ulPduId, PduInfoPtr, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
        /* #50 If the upper layer returned a size of 0 we do not add the vIpc header information. */
        if ((*BufferSizePtr > 0u)
            && (vIpc_XCfg_GetRxPduIsHeaderSent(RxPduId) == FALSE))
        {
          *BufferSizePtr += vIpc_Rx_HeaderType2HeaderLen[vIpc_XCfg_GetFrameType(sduId)];
        }
      }
      else
      {
        PduInfoType ULInfoPtr;
        PduLengthType remLen = PduInfoPtr->SduLength;

        /* #60 Regular use case. Is this the beginning of a segment or are we copying a fragment? */
        if (vIpc_XCfg_GetRxPduIsHeaderSent(RxPduId) == FALSE)
        {
          const vIpc_MessageType frameType = vIpc_Msg_GetType(PduInfoPtr);
          const uint8 headerLen            = vIpc_Rx_HeaderType2HeaderLen[frameType];

          vIpc_XCfg_SetRxPduIsHeaderSent(RxPduId, TRUE);

          /* #70 Copy data minus the vIpc header information. */
          ULInfoPtr.SduDataPtr = &PduInfoPtr->SduDataPtr[headerLen];
          ULInfoPtr.SduLength  = PduInfoPtr->SduLength - headerLen;
          remLen -= headerLen;
        }
        else
        {
          /* #80 Copy full header as we don't have a vIpc header we have to subtract. */
          ULInfoPtr = *PduInfoPtr;
        }

        retVal = vIpc_XCfg_GetCopyRxDataOfRxUlCallbacks(sduId)(ulPduId, &ULInfoPtr, BufferSizePtr); /* VCA_VIPC_FCT_CALL_WITH_ALL_PRIOR_ASSERTIONS_PASSED */
        vIpc_XCfg_SetRxRemainingPayloadLength(sduId, (uint32)(vIpc_XCfg_GetRxRemainingPayloadLength(sduId) - remLen));
      }
    }
    else
    {
      /* #90 We return BUFREQ_E_NOT_OK here as the transmission is idle, i.e. no StartOfReception. */
      *BufferSizePtr = 0u;
    }
#else 
    VIPC_DUMMY_STATEMENT(PduInfoPtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    VIPC_DUMMY_STATEMENT(BufferSizePtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif /* (VIPC_RXCONNECTIONS == STD_ON) */
  }

  (void) vIpc_XCfg_ReportError(VIPC_SID_COPYRXDATA, errorId);

  return retVal;
} /* vIpc_CopyRxData */


/**********************************************************************************************************************
 * vIpc_StartOfReception()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
/* PRQA S 6080 4 */ /* MD_MSR_STMIF */
/* PRQA S 6050 3 */ /* MD_MSR_STCAL */
/* PRQA S 6030 2 */ /* MD_MSR_STCYC */
FUNC(BufReq_ReturnType, VIPC_CODE)
vIpc_StartOfReception(PduIdType RxPduId, /* VCA_VIPC_FCT_CALL_UNSATISFIABLE */
                      P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr,  /* PRQA S 3673 2 */ /* MD_MSR_Rule8.13 */
                      PduLengthType TpSduLength,
                      P2VAR(PduLengthType, AUTOMATIC, VIPC_APPL_VAR) BufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal   = BUFREQ_E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vIpc_ErrorCode errorId     = VIPC_E_NO_ERROR; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  const boolean isDetEnabled = vIpc_XCfg_DevErrorDetect();
  const boolean isPduDataValid = vIpc_XCfg_IsPduDataValid(PduInfoPtr); /* PRQA S 1338, 2983, 3112 2 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
  const boolean isPduDataLenValid = vIpc_XCfg_IsPduDataLenValid(PduInfoPtr);
  const boolean isBufSizePtrValid = vIpc_XCfg_IsBufSizePtrValid(BufferSizePtr);

  /* #10 If vIpc is not initialized or an invalid parameter is given, report an error. */
  if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsVIpcInitialized() == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_NOT_INITIALIZED;
  }
  else if ((isDetEnabled == TRUE) && (vIpc_XCfg_IsRxPduIdValid(RxPduId) == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_PDU_ID;
  }
  else if ((isDetEnabled == TRUE) && (isPduDataValid == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_POINTER;
  }
  else if ((isDetEnabled == TRUE) && (isPduDataLenValid == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_CONFIG;
  }
  else if ((isDetEnabled == TRUE) && (isBufSizePtrValid == FALSE)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */ 
  {
    errorId = VIPC_E_INV_POINTER;
  }
  else if ((isDetEnabled == TRUE) && (PduInfoPtr->SduLength < 2u)) /* PRQA S 2991, 2992, 2994, 2995, 2996 */ /* MD_MSR_ConstantCondition */
  {
    errorId = VIPC_E_INV_CONFIG;
  }
  else
  {
#if (VIPC_RXCONNECTIONS == STD_ON)    
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Given no errors have been detected, process the incoming Pdu. */
    const uint8 vIpcAddress = vIpc_Msg_GetAddress(PduInfoPtr);

    /* #30 If received address valid for this channel. */
    if (vIpcAddress < vIpc_XCfg_GetAddressLengthOfRxChannel(RxPduId)) /* PRQA S 2991, 2992, 2994, 2995, 2996  */ /* MD_MSR_ConstantCondition */
    {
      /* #40 Then process the frame. */
      retVal = vIpc_Rx_Process(RxPduId, PduInfoPtr, TpSduLength, BufferSizePtr);
    }
#else 
    VIPC_DUMMY_STATEMENT(TpSduLength); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
    VIPC_DUMMY_STATEMENT(BufferSizePtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
#endif /* (VIPC_RXCONNECTIONS == STD_ON) */
  }


  (void) vIpc_XCfg_ReportError(VIPC_SID_STARTOFRECEPTION, errorId);

  return retVal;
} /* vIpc_StartOfReception */

#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpc_Rx.c
 *********************************************************************************************************************/
