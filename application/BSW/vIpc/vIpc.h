/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  vIpc.h
 *        \brief  VIPC Template header file
 *
 *      \details 
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Patrick Junger                vispju        Vector Informatik GmbH
 *  Oliver Reineke                visore        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  00.00.00  2017-05-19  vispju                Create boilerplate module vIpc
 *  00.01.00  2017-06-12  vispju  STORYC-1404   Usecase: Last-is-best with unsegmented PDUs
 *  00.02.00  2017-07-10  vispju  STORYC-1453   Usecase: Queued transmissions with unsegmented PDUs and equal priority
 *  00.03.00  2017-08-09  vispju  STORYC-1454   Usecase: Queued transmissions with un-/segmented prioritized PDUs
 *  01.00.00  2017-09-27  vispju  STORYC-1667   Bring vIpc to release state
 *            2017-09-22  vispju  ESCAN00096628 vIpc copies incomplete data to UL
 *  01.01.00  2018-01-24  vispju  ESCAN00098121 Removed compiler warnings
 *                                STORYC-4028   Refactorings
 *  01.01.01  2019-07-22  vispju  ESCAN00103484 vIpc_CopyTxData somtimes does not copy segments
 *  02.00.00  2021-02-22  vispju  ESCAN00108628 vIpc confuses Source & Destination Pdus for Rx Connections.
 *            2021-02-24  vispju  ESCAN00103486 vIpc_TxConfirmation returns E_NOT_OK
 *            2021-05-12  visore  PERMIL-880    MISRA-C:2012: Ipc_vIpc
 *  02.00.01  2021-10-01  visore  ESCAN00110267 vIpc does not indicate the complete SduLength when calling 
 *                                              UL_StartOfReception for an incoming segmented transfer.
 *  02.00.02  2021-10-14  visore  ESCAN00110465 Compiler error: conversion from 'const uint32' to 'PduLengthType', possible loss of data
 *  02.00.03  2022-05-25  visore  PERMIL-4001   Change reported DET error in vIpc_Transmit to VIPC_E_TX_PENDING in case the connection is not idle yet.
 *********************************************************************************************************************/

#ifndef VIPC_H
# define VIPC_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vIpc_Cbk.h"
#include "vIpc_Cfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Vendor and module identification */
#define VIPC_VENDOR_ID                    (uint8) (30u)
#define VIPC_MODULE_ID                    (uint16)(0xFFu)
#define VIPC_INSTANCE_ID                  (uint8) (0x00u)

/* AUTOSAR Software specification version information */
#define VIPC_AR_RELEASE_MAJOR_VERSION     (4u)
#define VIPC_AR_RELEASE_MINOR_VERSION     (3u)
#define VIPC_AR_RELEASE_REVISION_VERSION  (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
#define VIPC_SW_MAJOR_VERSION (2u)
#define VIPC_SW_MINOR_VERSION (0u)
#define VIPC_SW_PATCH_VERSION (3u)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VIPC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vIpc_Transmit()
 *********************************************************************************************************************/
/*! \brief       Receives and enqueues transmission requests.
 *  \details     If the Tx channel is idle, it forwards the transmission request to the lower layer.
 *  \param[in]   TxSduId                 Id of the L-SDU for which a transmit is wanted.
 *  \param[in]   PduInfoPtr              Data to transmit including length.
 *  \return      E_OK                    Transmission request accepted.
 *  \return      E_NOT_OK                Transmission request failed.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires vIpc_XCfg_IsTxSduIdValid(TxSduId);
 *               requires PduInfoPtr != NULL_PTR;
 *               requires $lengthOf(PduInfoPtr->SduDataPtr) >= PduInfoPtr->SduLength;
 *  \endspec
 *  \synchronous TRUE
 *  \pre         -
 *  \ingroup     tx
 *********************************************************************************************************************/
extern FUNC(Std_ReturnType, VIPC_CODE) vIpc_Transmit(PduIdType TxSduId,
                                                     P2CONST(PduInfoType, AUTOMATIC, VIPC_APPL_CONST) PduInfoPtr);

/**********************************************************************************************************************
 *  vIpc_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Function for *_INIT_*-variable initialization
 *  \details     Service to initialize module global variables at power up. This function initializes the
 *               variables in *_INIT_* sections. Used in case they are not initialized by the startup code.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \pre         Module is uninitialized.
 *  \ingroup     general
 *********************************************************************************************************************/
extern FUNC(void, VIPC_CODE) vIpc_InitMemory(void);

/**********************************************************************************************************************
 * vIpc_Init()
 *********************************************************************************************************************/
/*! \brief       Initializes the vIpc.
 *  \details     This function initializes the module vIpc. It initializes all variables and sets the module state to
 *               initialized.
 *  \param[in]   ConfigPtr               Configuration structure for initializing the module
 *  \context     TASK
 *  \reentrant   FALSE
 *  \spec        requires ConfigPtr != NULL_PTR;
 *  \endspec
 *  \synchronous TRUE
 *  \pre         -
 *  \trace       CREQ-129569
 *  \ingroup     general
 *********************************************************************************************************************/
extern FUNC(void, VIPC_CODE) vIpc_Init(P2CONST(vIpc_ConfigType, AUTOMATIC, VIPC_PBCFG) ConfigPtr);

/**********************************************************************************************************************
 *  vIpc_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief       Returns the version information
 *  \details     vIpc_GetVersionInfo() returns version information, vendor ID and AUTOSAR module ID of the component.
 *  \param[out]  VersionInfo             Pointer to where to store the version information. Parameter must not be NULL.
 *  \context     TASK|ISR
 *  \reentrant   FALSE
 *  \spec        requires VersionInfo != NULL_PTR;
 *  \endspec
 *  \synchronous TRUE
 *  \trace       CREQ-129568
 *  \pre         -
 *  \ingroup     general
 *********************************************************************************************************************/
extern FUNC(void, VIPC_CODE) vIpc_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VIPC_APPL_VAR) VersionInfo);

/**********************************************************************************************************************
  vIpc_MainFunction
**********************************************************************************************************************/
/*!
 * \fn           void vIpc_MainFunction(void)
 * \brief        Monitors and controls the continuous execution of the vIpc Transmission & Reception.
 * \details      -
 * \pre          -
 * \context      TASK|ISR
 * \reentrant    FALSE
 * \synchronous  TRUE
 * \trace        CREQ-130437
 * \ingroup      general
 *********************************************************************************************************************/
/* extern FUNC(void, VIPC_CODE) vIpc_MainFunction(void); */


#define VIPC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*!
 * \exclusivearea VIPC_EXCLUSIVE_AREA_TX_PROCESS
 * Ensures that multiple LL transmits on one Tx channel are avoided.
 * \protects IsActiveTxConAvailable, indication if Tx channel is idle
 * \usedin vIpc_Tx_Process
 * \exclude vIpc_MainFunction, vIpc_Transmit, vIpc_TxConfirmation
 * \length SHORT The IsActiveTxConAvailable is read and written.
 * \endexclusivearea
 *
 * \exclusivearea VIPC_EXCLUSIVE_AREA_TX_QUEUE
 * Ensures consistency of all Tx queues.
 * \protects Write access to vIpc_GetTxQueueElem and vIpc_QueueInfo
 * \usedin vIpc_PQ_Insert_PrioBased, vIpc_PQ_Insert_FifoBased, vIpc_PQ_Remove
 * \exclude vIpc_PQ_Insert_PrioBased, vIpc_PQ_Insert_FifoBased, vIpc_PQ_Remove
 * \length MEDIUM For vIpc_PQ_Insert_PrioBased and high number of Tx Connections. In case of a few Tx Connections,
 *                vIpc_PQ_Insert_FifoBased and vIpc_PQ_Remove it is SHORT.
 * \endexclusivearea
 */

#endif /* VIPC_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpc.h
 *********************************************************************************************************************/
