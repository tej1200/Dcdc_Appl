/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /*!       \file  VX1000HookIf.h
 *        \brief  VX1000 Hook Interface header file
 *
 *      \details  Implementation of an API wrapper between VX1000 Hook API and VX1000 Hook driver.
 *
 *********************************************************************************************************************/ 

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Oliver Reineke                visore        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  1.00.00   2019-10-16  visore  -             First implementation of VX1000 Hook Interface
 *********************************************************************************************************************/

/** 
* \defgroup IF Embedded Interfaces
* \{
*   \defgroup Generic Generic
*   \defgroup HookIf HookIf
** \} */

#if !defined(VX1000HOOKIF_H)
# define VX1000HOOKIF_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"
# include "VX1000HookIf_Cfg.h"
# include "VX1000_Hook.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VX1000HOOKIF_SW_MAJOR_VERSION                    (1u)
# define VX1000HOOKIF_SW_MINOR_VERSION                    (0u)
# define VX1000HOOKIF_SW_PATCH_VERSION                    (0u)

# if !defined(VX1000HookIf_IsAccessEnabled)
#  error "No boolean expression defined that tells the interface whether to call or to not call the actual hook driver"
# else /* VX1000HookIf_IsAccessEnabled */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ------- Modes ------- */
#  define VX1000HOOKIF_UNINIT                            (0x00u)
#  define VX1000HOOKIF_INITIALIZED                       (0x01u)

/* --- Return Values --- */
#  define VX1000HOOKIF_RET_VOID                          (void)0
#  define VX1000HOOKIF_RET_E_NOT_OK                      (0xFFu)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 3 */ /* MD_MSR_FctLikeMacro */
/* PRQA S 0342 2 */ /* MD_MSR_Rule20.10_0342 */
#  define VX1000HOOKIF_IMPL(functionName, retVal)   ((!VX1000HookIf_IsAccessEnabled) || (VX1000HookIf_State != VX1000HOOKIF_INITIALIZED)) ? (VX1000HookIf_ErrorCount++, retVal) : functionName
#  define VX1000HOOKIF_IMPL_VOID(functionName)      if (!(VX1000HookIf_IsAccessEnabled) || (VX1000HookIf_State != VX1000HOOKIF_INITIALIZED)) {VX1000HookIf_ErrorCount++;} else functionName

/**********************************************************************************************************************
 *  VX1000HookIf_Hook()
 *********************************************************************************************************************/
/*! \brief       Macro to trigger a generic bypass and wait until the data request is finished.
 *  \details     Make the VX1000 Hook driver trigger a generic bypass for STIM events with VX1000 Hooks.
 *               If given STIM event is active:
 *                 1.	Trigger the generic bypass for the STIM event 
 *                 2.	Execute the original code (optional if configured to execute always)
 *                 3.	Trigger an additional DAQ event (optional configured)
 *                 4.	Wait until the data set requests of this generic bypass are finished
 *                 5.	Execute the original code if stimulation has failed because of timeout (optional configured)
 *               If given STIM event is inactive:
 *                 1.	Trigger an additional event (optional configured)
 *               Execute the original user code.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \param[in]   timeout:             Timeout in microseconds, starting with the function call. 
 *  \param[in]   function:            User function to be executed in case of failed stimulation or inactive hook. 
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220315
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_Hook                         VX1000HOOKIF_IMPL_VOID(VX1000_HOOK)

/**********************************************************************************************************************
 *  VX1000HookIf_HookWait()
 *********************************************************************************************************************/
/*! \brief       Macro to stimulate with timeout for a generic bypass.
 *  \details     Makes the VX1000 Hook driver stimulate with timeout for a generic bypass.
 *               If STIM event is active:
 *                 1.	Trigger an additional DAQ event (optional configured)
 *                 2.	Wait until the data set requests of this generic bypass are finished
 *                 3.	Return the result.
 *               If STIM event is inactive:
 *                 1.	Trigger an additional event (optional configured)
 *               Return the result.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \param[in]   timeout:             Timeout in microseconds, starting with the function call. 
 *  \return      0 - bypass inactive 
 *               1 - stimulation done, no timeout, OK
 *               2 - stimulation not done, timeout
 *               3 - stimulation not done, timeout, execute original code
 *               VX1000HOOKIF_RET_E_NOT_OK while VX1000_HookIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000HookIf_Init and VX1000HookIf_HookTrigger must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220278
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_HookWait                     VX1000HOOKIF_IMPL(VX1000_HOOK_WAIT, VX1000HOOKIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000HookIf_HookTrigger()
 *********************************************************************************************************************/
/*! \brief       Macro to trigger a generic bypass.
 *  \details     Make the VX1000 Hook driver trigger a generic bypass for STIM events with VX1000 Hooks.
 *               If given STIM event is active:
 *                 1.	Trigger the generic bypass for the STIM event
 *                 2.	Return the result
 *               If given STIM event is inactive:
 *                 1.	Trigger an additional event (optional configured)
 *               Return the result.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \return      0 - inactive bypass or active bypass and original code enabled
 *               1 - bypass active and original code disabled
 *               VX1000HOOKIF_RET_E_NOT_OK while VX1000HookIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220277
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_HookTrigger                  VX1000HOOKIF_IMPL(VX1000_HOOK_TRIGGER, VX1000HOOKIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  VX1000HookIf_GenericEvent()
 *********************************************************************************************************************/
/*! \brief       Macro to trigger a generic event.
 *  \details     Makes the VX1000 Hook driver trigger a generic event whose event ID is related to the Hook ID.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220574
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_GenericEvent                 VX1000HOOKIF_IMPL_VOID(VX1000_HOOK_GENERIC_EVENT)

/**********************************************************************************************************************
 *  VX1000HookIf_DefineBypass()
 *********************************************************************************************************************/
/*! \brief       Macro to set the initial bypass configuration.
 *  \details     Makes the VX1000 Hook driver set the initial bypass configuration in the hook control structure.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \param[in]   trigger_event:       Trigger event index.
 *  \param[in]   stim_event:          STIM event index.
 *  \param[in]   daq_event:           DAQ event index.
 *  \param[in]   mode:                Enable or disable copy of STIM data.
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220575
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_DefineBypass                 VX1000HOOKIF_IMPL_VOID(VX1000_HOOK_DEFINE_BYPASS)

/**********************************************************************************************************************
 *  VX1000HookIf_DefineEvent()
 *********************************************************************************************************************/
/*! \brief       Macro to set the initial generic event configuration.
 *  \details     Makes the VX1000 Hook driver set the initial generic event configuration in the hook control structure.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \param[in]   daq_event:           DAQ event index.
 *  \param[in]   mode:                Enable or disable copy of STIM data.
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220576
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_DefineEvent                  VX1000HOOKIF_IMPL_VOID(VX1000_HOOK_DEFINE_EVENT)

/**********************************************************************************************************************
 *  VX1000HookIf_HookConfigured()
 *********************************************************************************************************************/
/*! \brief       Macro to check the validity for the given hook ID.
 *  \details     Makes the VX1000 Hook driver check whether the given hook ID is a valid index.
 *  \param[in]   hook_id:             Hook ID range as configured in the VX1000 Hook driver.
 *  \return      0 - hook ID is an invalid index
 *               1 - hook ID is a valid index
 *               VX1000HOOKIF_RET_E_NOT_OK while VX1000HookIf_IsAccessEnabled returned FALSE
 *  \pre         VX1000HookIf_Init must have been called.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220577
 *  \ingroup     HookIf
 *********************************************************************************************************************/
#  define VX1000HookIf_HookConfigured               VX1000HOOKIF_IMPL(VX1000_HOOK_CONFIGURED, VX1000HOOKIF_RET_E_NOT_OK)

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

#   define   VX1000HOOKIF_START_SEC_VAR_ZERO_INIT_8BIT
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Initialization state of the module */
extern VAR(uint8, VX1000HOOKIF_VAR_ZERO_INIT) VX1000HookIf_State;

/*! Counter for errors that are caused by API calls when :
 *     - VX1000 Hook Interface is in incorrect state (e.g. not initialized) 
 *     - or if VX1000HookIf_IsAccessEnabled returned FALSE 
 */
extern VAR(uint8, VX1000HOOKIF_VAR_ZERO_INIT) VX1000HookIf_ErrorCount;

#   define   VX1000HOOKIF_STOP_SEC_VAR_ZERO_INIT_8BIT
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#  define   VX1000HOOKIF_START_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  VX1000HookIf_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Function for *_INIT_*-variable initialization
 *  \details     Service to initialize module global variables at power up. This function initializes the
 *               variables in *_INIT_* sections. Used in case they are not initialized by the startup code.
 *  \pre         Module is uninitialized.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \ingroup     Generic
 *********************************************************************************************************************/
FUNC(void, VX1000HOOKIF_CODE) VX1000HookIf_InitMemory(void);

/**********************************************************************************************************************
 * VX1000HookIf_Init()
 *********************************************************************************************************************/
/*! \brief       Initialization function
 *  \details     This function initializes the module VX1000HookIf. It initializes all variables and sets the module
 *               state to initialized.
 *  \pre         Interrupts are disabled.
 *  \pre         Module is uninitialized.
 *  \pre         VX1000HookIf_InitMemory has been called unless VX1000HookIf_State is initialized by start-up code.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-220285
 *  \ingroup     Generic
 *********************************************************************************************************************/
FUNC(void, VX1000HOOKIF_CODE) VX1000HookIf_Init(void);

#  define   VX1000HOOKIF_STOP_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* VX1000HookIf_IsAccessEnabled */

#endif /* !VX1000HOOKIF_H */

/**********************************************************************************************************************
 *  END OF FILE: VX1000HookIf.h
 *********************************************************************************************************************/
