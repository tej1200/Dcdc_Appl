/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  VX1000HookIf.c
 *        \brief  VX1000 Hook Interface source file
 *
 *      \details  Implementation of an API wrapper between VX1000 Hook API and VX1000 Hook driver.
 *
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VX1000HOOKIF_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "VX1000HookIf.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of VX1000HookIf header file */
#if (  (VX1000HOOKIF_SW_MAJOR_VERSION != (1u)) \
    || (VX1000HOOKIF_SW_MINOR_VERSION != (0u)) \
    || (VX1000HOOKIF_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of VX1000HookIf.c and VX1000HookIf.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#define VX1000HOOKIF_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Initialization state of the module */
VAR(uint8, VX1000HOOKIF_VAR_ZERO_INIT) VX1000HookIf_State = VX1000HOOKIF_UNINIT; /* PRQA S 1504 */ /* MD_MSR_Rule8.7 */

/*! Counter for errors that are caused by API calls when :
 *     - VX1000 Hook Interface is in incorrect state (e.g. not initialized) 
 *     - or if VX1000HookIf_IsAccessEnabled returned FALSE 
 */
VAR(uint8, VX1000HOOKIF_VAR_ZERO_INIT) VX1000HookIf_ErrorCount = 0; /* PRQA S 1504 */ /* MD_MSR_Rule8.7 */

#define VX1000HOOKIF_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#define VX1000HOOKIF_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  VX1000HookIf_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VX1000HOOKIF_CODE) VX1000HookIf_InitMemory(void)
{
  /* ----- Implementation ----------------------------------------------- */
  VX1000HookIf_State = VX1000HOOKIF_UNINIT;

  VX1000HookIf_ErrorCount = 0;
}  /* VX1000HookIf_InitMemory() */

/**********************************************************************************************************************
 *  VX1000HookIf_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VX1000HOOKIF_CODE) VX1000HookIf_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #-- Set component to initialized */
  if ((VX1000HookIf_State == VX1000HOOKIF_UNINIT) && (VX1000HookIf_IsAccessEnabled == TRUE))
  {
    VX1000_HOOK_INIT();

    VX1000HookIf_State = VX1000HOOKIF_INITIALIZED;

    VX1000HookIf_ErrorCount = 0;
  }
} /* VX1000HookIf_Init() */

#define VX1000HOOKIF_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: VX1000HookIf.c
 *********************************************************************************************************************/
