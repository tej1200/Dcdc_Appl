/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Scheduler.h
 *        \brief  vStreamProc Scheduler Sub Module Header File
 *
 *      \details  Header file of the vStreamProc Scheduler sub module.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_SCHEDULER_H)
# define VSTREAMPROC_SCHEDULER_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "vStreamProc_Types.h"
# include "vStreamProc_Lcfg.h"
# include "vStreamProc_CfgTypes.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_Init()
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the scheduler of the passed pipe.
 * \details       -
 * \param[in]     PipeId        Id of the pipe.
 * \return        E_OK          Initialization was successful.
 * \return        E_NOT_OK      Initialization failed.
 * \pre           Must be called before call of any vStreamProc_Scheduler function.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_Init(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_Process()
 *********************************************************************************************************************/
/*!
 * \brief         Starts the executing of the given scheduler.
 * \details       Loops over the scheduler entries and process the ProcessingNodes.
 * \param[in]     SchedulerId                       Id of the scheduler workspace. Parameter must not be NULL
 * \return        VSTREAMPROC_OK                    The scheduler is idle.
 * \return        VSTREAMPROC_PENDING               The scheduler is pending and needs to be called again.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    The scheduler was disturbed due to insufficient input data 
 *                                                  of an entry point.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   The scheduler was disturbed due to insufficient output data 
 *                                                  of an exit point.
 * \return        VSTREAMPROC_FAILED   The scheduler failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_Process(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetNodeSignal
 *********************************************************************************************************************/
/*!
 * \brief         Sets a node signal and adds the corresponding signal handlers to the schedule table.
 * \details       -
 * \param[in]     ProcessingNodeId  Id of the node that should be added to the schedule table.
 * \param[in]     SignalId          Id of the signal that should be added to the schedule table.
 * \return        E_OK              Signal handler is queued.
 * \return        E_NOT_OK          Signal handler is not queued. This might be the case if no signal handler is defined
 *                                  for this signal or the signal handler is unregistered.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetNodeSignal(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetInputPortSignal
 *********************************************************************************************************************/
/*!
 * \brief         Sets an input port signal and adds the corresponding signal handlers to the schedule table.
 * \details       -
 * \param[in]     NamedInputPortId  Id of the target input port of the signal.
 * \param[in]     SignalId          Id of the signal that should be added to the schedule table.
 * \return        E_OK              Signal handler is queued.
 * \return        E_NOT_OK          Signal handler is not queued. This might be the case if no signal handler is defined
 *                                  for this signal or the signal handler is unregistered.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetInputPortSignal(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetOutputPortSignal
 *********************************************************************************************************************/
/*!
 * \brief         Sets an output port signal and adds the corresponding signal handlers to the schedule table.
 * \details       -
 * \param[in]     NamedOutputPortId   Id of the target output port of the signal
 * \param[in]     SignalId            Id of the signal that should be added to the schedule table.
 * \return        E_OK                Signal handler is queued.
 * \return        E_NOT_OK            Signal handler is not queued. This might be the case if no signal handler is defined
 *                                    for this signal or the signal handler is unregistered.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetOutputPortSignal(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_QueueSignalHandler
 *********************************************************************************************************************/
/*!
 * \brief         Adds the provided signal handler to the schedule table queue.
 * \details       -
 * \param[in]     SignalHandlerId   Id of the signal handler which shall be added to the schedule table.
 * \return        E_OK              Signal handler is queued.
 * \return        E_NOT_OK          Signal handler is not queued. This might be the case if the signal handler is unregistered.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_QueueSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetNodeSignalHandlerState
 *********************************************************************************************************************/
/*!
 * \brief         Sets the signal handler state to the provided value.
 * \details       -
 * \param[in]     ProcessingNodeId            The id of processing node the signal handler belongs to.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful. 
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_Scheduler_SetNodeSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetInputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * \brief         Sets the signal handler state to the provided value.
 * \details       -
 * \param[in]     NamedInputPortId            The id of named input port the signal handler belongs to.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful.
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_Scheduler_SetInputPortSignalHandlerState(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetOutputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * \brief         Sets the signal handler state to the provided value.
 * \details       -
 * \param[in]     NamedOutputPortId           The id of named output port the signal handler belongs to.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful.
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_Scheduler_SetOutputPortSignalHandlerState(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_DiscardInputPortSignal
 *********************************************************************************************************************/
/*!
 * \brief         Discards pending signal handlers for the provided signal at the provided input port from the 
 *                scheduling queue.
 * \details       -
 * \param[in]     NamedInputPortId            The id of named input port.
 * \param[in]     SignalId                    The id of signal.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Scheduler_DiscardInputPortSignal(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId);

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* VSTREAMPROC_SCHEDULER_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Scheduler.h
 *********************************************************************************************************************/
