/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_AuthGeneration.h
 *        \brief  vStreamProc Authentication Generation Processing Node Header File
 *
 *      \details  Header file of the Authentication Generation Processing Node.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_PROCESSINGNODE_AUTH_GENERATION_H)
# define VSTREAMPROC_PROCESSINGNODE_AUTH_GENERATION_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_Init()
 *********************************************************************************************************************/
/*!
 *  \brief          Initializes the passed node.
 *  \details        -
 *  \param[in]      ProcNodeInfo              The processing node information to operate on.
 *  \return         E_OK                      Initialization was successful
 *  \return         E_NOT_OK                  Initialization was unsuccessful
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-211279
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StartWave()
 *********************************************************************************************************************/
/*!
 *  \brief          Initializes the passed node.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ProcessingNode_AuthGeneration_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-313395
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_ProcessInputData()
 *********************************************************************************************************************/
/*!
 *  \brief          Processes the passed node.
 *  \details        This node generation an authentication value based on the input data It does not write the
                    authentication tag until the wave end signal is received.
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ProcessingNode_AuthGeneration_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-313395
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_ProcessInputData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_WriteAuthenticationTag()
 *********************************************************************************************************************/
/*!
 *  \brief          Handler for wave end signal.
 *  \details        The handler will finalize the CSM verification value calculation, compare the resulting value
 *                  with the provided CSM reference data and produce the result.
 *                  All input data has to be consumed and processed without errors when this function is called.
 *                  Reference or compare value have to be available at this point because no further input data is
 *                  allowed to be provided when wave end is triggered. This will lead to a reduced set of responses.
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ProcessingNode_AuthGeneration_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-313395
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_WriteAuthenticationTag(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 *  \brief          Stores the CSM context in the workspace.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ProcessingNode_AuthGeneration_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 *  \brief          Cancels any action and close CSM job if necessary.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ProcessingNode_Cipher_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 *  \brief          Restores the CSM context to the workspace.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            A CSM job must have been successfully started beforehand.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_PROCESSINGNODE_AUTH_GENERATION_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_AuthGeneration.h
 *********************************************************************************************************************/
