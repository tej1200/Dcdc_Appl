/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Mode.h
 *        \brief  vStreamProc Mode Sub Module Header File
 *
 *      \details  Header file of the vStreamProc Mode sub module.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_MODE_H)
# define VSTREAMPROC_MODE_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */ 

/**********************************************************************************************************************
 * vStreamProc_Mode_ApplyMode()
 *********************************************************************************************************************/
/*! \brief        Apply the given mode to the pipe.
 *  \details      Switches base states configured for given mode.
 *  \param[in]    PipeId            The pipe to operate on.
 *  \param[in]    PipeModeId        The mode to apply.
 *                                  Use VSTREAMPROC_NO_MODE to set all base states to their default values.
 *  \return       E_OK - success
 *  \return       E_NOT_OK - error
 *  \context      TASK
 *  \reentrant    TRUE
 *  \pre          -
 *  \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_ApplyMode(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType PipeModeId);

/**********************************************************************************************************************
 * vStreamProc_Mode_HandleMetaStateSwitching()
 *********************************************************************************************************************/
/*! \brief        Switch active ports, based on given mode.
 *  \details      Applies mode, evaluates active meta states and switches port activations.
 *  \param[in]    PipeId            The pipe to operate on.
 *  \return       E_OK - success
 *  \return       E_NOT_OK - error
 *  \context      TASK
 *  \reentrant    TRUE
 *  \pre          -
 *  \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_HandleMetaStateSwitching(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 * vStreamProc_Mode_IsActivationStateChanged()
 *********************************************************************************************************************/
/*! \brief        Checks if the provided mode would lead to any activation states changes of the base states.
 *  \details      -
 *  \param[in]    PipeId  Id of the pipe.
 *  \param[in]    ModeId  Id of the mode.
 *  \return       TRUE    The mode would change the activation state of at least on base state.
 *  \return       FALSE   The mode would not lead to any activation state changes.
 *  \pre          -
 *  \context      TASK
 *  \reentrant    TRUE
 *  \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Mode_IsActivationStateChanged(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType ModeId);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_MODE_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Mode.h
 *********************************************************************************************************************/
