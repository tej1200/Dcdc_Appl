/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_Session.c
 *        \brief  vStreamProc source code file for the session handling
 *
 *      \details  Implementation of the session related functionality.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_SESSION_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Mode.h"
#include "vStreamProc_Queue.h"
#include "vStreamProc_Scheduler.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_Session.h"
#include "vStreamProc_Snapshot.h"
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 **********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

 /*! Session data format version */
#define VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MAJOR         (0x01u)
#define VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MINOR         (0x00u)

#define VSTREAMPROC_SESSION_SECTION_MARKER_SIZE     4u

#define VSTREAMPROC_NO_SNAPSHOTWAVE       VSTREAMPROC_UNSIGNED_MAX(vStreamProc_SnapshotWaveIdType)

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_Session_DataPtr;
typedef P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_Session_ConstDataPtr;
typedef uint8 vStreamProc_Session_SectionMarkerType[VSTREAMPROC_SESSION_SECTION_MARKER_SIZE + 1u];

typedef enum
{
  VSTREAMPROC_SESSION_STATE_SET_STARTMARKER,
  VSTREAMPROC_SESSION_STATE_SET_VERSION,
  VSTREAMPROC_SESSION_STATE_RESERVE_LENGTH,
  VSTREAMPROC_SESSION_STATE_SET_UUID,
  VSTREAMPROC_SESSION_STATE_SET_STATEMARKER,
  VSTREAMPROC_SESSION_STATE_SET_BASESTATES,
  VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTMARKER,
  VSTREAMPROC_SESSION_STATE_RESERVE_SNAPSHOTCOUNT,
  VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTDATA,
  VSTREAMPROC_SESSION_STATE_SET_ENDMARKER,
  VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTCOUNT,
  VSTREAMPROC_SESSION_STATE_SET_LENGTH,
  VSTREAMPROC_SESSION_STATE_FRAMECOUNT
} vStreamProc_Session_State_DataFrameType;

typedef enum
{
  VSTREAMPROC_SESSION_STATE_SET_NODEMARKER,
  VSTREAMPROC_SESSION_STATE_SET_RELNODEIDX,
  VSTREAMPROC_SESSION_STATE_SET_INPUTDATA,
  VSTREAMPROC_SESSION_STATE_SET_OUTPUTDATA,
  VSTREAMPROC_SESSION_STATE_SET_WORKSPACEMARKER,
  VSTREAMPROC_SESSION_STATE_SET_WORKSPACE,
  VSTREAMPROC_SESSION_STATE_SNAP_FRAMECOUNT
} vStreamProc_Session_State_SnapshotType;

/* Hint: Would be vStreamProc_Session_SectionType, but type is never used directly. */
typedef enum
{
  VSTREAMPROC_SESSION_SECTION_PIPE_START,
  VSTREAMPROC_SESSION_SECTION_BASESTATE,
  VSTREAMPROC_SESSION_SECTION_SNAPSHOT,
  VSTREAMPROC_SESSION_SECTION_NODE,
  VSTREAMPROC_SESSION_SECTION_INPUTPORT,
  VSTREAMPROC_SESSION_SECTION_OUTPUTPORT,
  VSTREAMPROC_SESSION_SECTION_WAVE,
  VSTREAMPROC_SESSION_SECTION_METADATA,
  VSTREAMPROC_SESSION_SECTION_WORKSPACE,
  VSTREAMPROC_SESSION_SECTION_PIPE_END
};

typedef struct
{
  uint8 Version[2];
  vStreamProc_LengthType TotalSize;
  uint8 Uuid[8];
  uint8 SnapshotCount;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) BaseStateStart;
  vStreamProc_BaseStateIdType BaseStateCount;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) SnapshotDataStart;
  vStreamProc_LengthType SnapshotDataLength;
} vStreamProc_SessionHeaderDataType;

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */  /* MD_MSR_MemMap */

VSTREAMPROC_LOCAL CONST(vStreamProc_Session_SectionMarkerType, VSTREAMPROC_CONST) vStreamProc_Session_SectionMarkers[] =
{
  "Pipe", /* Pipe start */
  "Base", /* Base state */
  "Snap", /* Snapshot */
  "Node", /* Node */
  "IPrt", /* Input port */
  "OPrt", /* Output port */
  "Wave", /* Wave */
  "Meta", /* Meta data */
  "Wksp", /* Workspace */
  "Plug"  /* Pipe end (plug) */
};

#define VSTREAMPROC_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */  /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

 /*********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSessionEntryIdByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * \brief         Returns the corresponding session entry index of a processing node index.
 * \details       The session index can be calculated because the offsets are the same in both arrays.
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \return        The id of the session entry.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SessionEntryIdType, VSTREAMPROC_CODE) vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSessionQueueHandleByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * \brief         Returns the corresponding session queue handle of a processing node index.
 * \details       The session queue handle can be calculated because the offsets are the same in both arrays.
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \return        The session queue handle of the processing node.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Session_GetSessionQueueHandleByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * \brief         Returns a pointer to the snapshot queue of the provided processing node.
 * \details       -
 * \param[in]     ProcessingNodeId        Id of the processing node.
 * \return        Pointer to the snapshot set queue of the processing node.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueuePtrType, AUTOMATIC) vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateRestoreSessionData()
 *********************************************************************************************************************/
/*!
 * \brief         Increments the provided session data pointer and decrements the provided session data length with
 *                the provided length.
 * \details       -
 * \param[in,out] SessionData             Pointer to the session data.
 * \param[in,out] SessionDataLength       Pointer to the length of the session data.
 * \param[in]     Length                  The requested length .
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_UpdateRestoreSessionData(
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength,
  vStreamProc_LengthType Length);

/**********************************************************************************************************************
 *  vStreamProc_Session_IsMarkerValid()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the section marker of the provided section marker id is found at the start of the provided
 *                session data.
 * \details       The pointer and the length are updated based on the length of the section marker.
 * \param[in]     SectionMarkerId        Id of the data frame state.
 * \param[in,out] SessionData             Pointer to the session data.
 * \param[in,out] SessionDataLength       Pointer to the length of the session data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Session_IsMarkerValid(
  uint8 SectionMarkerId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_CheckDataFrameStateMarker()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the marker of the provided data frame state id is found at the start of the provided
 *                session data.
 * \details       The pointer and the length are updated based on the length of the corresponding section marker.
 * \param[in]     DataFrameStateId        Id of the data frame state.
 * \param[in,out] SessionData             Pointer to the session data.
 * \param[in,out] SessionDataLength       Pointer to the length of the session data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CheckDataFrameStateMarker(
  vStreamProc_Session_State_DataFrameType DataFrameStateId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_PrepareSession()
 *********************************************************************************************************************/
/*!
 * \brief         Prepares the session queue and the session entries of the provided pipe for the session creation.
 * \details       Fills the session queue and sorts the entries ascending based on the consumer count of
 *                the corresponding processing nodes. All session entries are invalidated.
 * \param[in]     PipeId                  Id of the pipe.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_PrepareSession(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_PrepareSessionEntries()
 *********************************************************************************************************************/
/*!
 * \brief         Sets each session entry to the latest available snapshot slot of the corresponding processing node.
 * \details       -
 * \param[in]     PipeId                  Id of the pipe.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_PrepareSessionEntries(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_ProcessSessionQueue()
 *********************************************************************************************************************/
/*!
 * \brief         Iterates over the session queue and processes the session queue entries.
 * \details       The entry is removed from the queue afterwards.
 * \param[in]     PipeId                  Id of the pipe.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_ProcessSessionQueue(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_ProcessSessionQueueHandle()
 *********************************************************************************************************************/
/*!
 * \brief         Iterates over the input ports of the corresponding processing node of the session queue handle and
 *                evaluates the snapshots of the producer node.
 * \details       Additionally, the consumer count of the producer nodes are decremented and the
 *                corresponding session queue entries are reordered based on the new priority.
 * \param[in]     PipeId                  Id of the pipe the session belongs to.
 * \param[in]     SessionQueue            Pointer to the session queue.
 * \param[in]     SessionQueueHandle      The session queue handle.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_ProcessSessionQueueHandle(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_QueueConstPtrType SessionQueue,
  vStreamProc_QueueHandleType SessionQueueHandle);

/**********************************************************************************************************************
 *  vStreamProc_Session_EvaluateProducerSnapshots()
 *********************************************************************************************************************/
 /*!
 * \brief         Iterates over the snapshots of the provided processing node from old to new and checks for
 *                each snapshot slot if the produce position of the provided output port is covered by the provided
 *                consumer snapshot slot.
 * \details       The latest snapshot which in covered by the consumer snapshot is set in the session entry of
 *                the producer node.
 * \param[in]     ProducerNodeId                    Id of the producer node.
 * \param[in]     ProducerOutputPortSymbolicName    Output Port of the producer node.
 * \param[in]     ConsumerNodeId                    Id of the consumer node.
 * \param[in]     ConsumerInputPortSymbolicName     Input Port of the consumer node.
 * \param[in]     ConsumerSnapshotSlotId            Snapshot slot of the consumer node.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_EvaluateProducerSnapshots(
  vStreamProc_ProcessingNodeIdType ProducerNodeId,
  vStreamProc_OutputPortSymbolicNameType ProducerOutputPortSymbolicName,
  vStreamProc_ProcessingNodeIdType ConsumerNodeId,
  vStreamProc_InputPortSymbolicNameType ConsumerInputPortSymbolicName,
  vStreamProc_SnapshotSlotIdType ConsumerSnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Session_CheckSessionValidity()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if at least one snapshot is referenced in the session entries and sets
 *                the session valid if true.
 * \details       -
 * \param[in]     PipeId                  Id of the pipe the session belongs to.
 * \pre           The pipe is open.
 * \return        VSTREAMPROC_OK                    The session is valid.
 * \return        VSTREAMPORC_INSUFFICIENT_INPUT    The session is invalid because there are no snapshots
 *                                                  referenced in session.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CheckSessionValidity(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateEntryNodeSnapshot
 *********************************************************************************************************************/
/*!
 * \brief         Updates the produce position of the provided entry node snapshot slot based on the
 *                minimum consume position of consumer node snapshots of the session.
 * \details       -
 * \param[in]     EntryNodeId              Id of the entry processing node.
 * \param[in]     SnapshotSlotId           Id of the snapshot slot.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_UpdateEntryNodeSnapshot(
  vStreamProc_ProcessingNodeIdType EntryNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateExitNodeSnapshot
 *********************************************************************************************************************/
/*!
 * \brief         Updates the consume position and wave handle of the provided exit node snapshot slot to the
 *                produce position and wave handle of the producer node snapshot of the session.
 * \details       -
 * \param[in]     ExitNodeId              Id of the entry processing node.
 * \param[in]     SnapshotSlotId           Id of the snapshot slot.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_UpdateExitNodeSnapshot(
  vStreamProc_ProcessingNodeIdType ExitNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetProducerSnapshotWaveStartId()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the snapshot wave id of the lowest wave level of the corresponding producer snapshot of the
 *                provided named input port.
 * \details       -
 * \param[in]     NamedInputPortId         Id of the named input port.
 * \return        If the producer is found, is active and has a snapshot slot: The producer snapshot wave id.
 * \return        Otherwise: VSTREAMPROC_NO_SNAPSHOTWAVE.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SnapshotWaveIdType, VSTREAMPROC_CODE) vStreamProc_Session_GetProducerSnapshotWaveStartId(
  vStreamProc_NamedInputPortIdType NamedInputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSnapshotDataOfSession
 *********************************************************************************************************************/
/*!
 * \brief         Serialize snapshot related data of session.
 * \details       -
 * \param[in]     Destination              Pointer to target buffer.
 * \param[in,out] AvailableLength          Available size of target buffer.
 * \param[in]     PipeId                   Id of the desired pipe.
 * \param[in]     SessionEntryIdx          Index to session, which shall be serialized.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetSnapshotDataOfSession(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SessionEntryIterType SessionEntryIdx);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetInputDataOfSession
 *********************************************************************************************************************/
/*!
 * \brief         Serialize input port related data of session.
 * \details       -
 * \param[in]     Destination              Pointer to target buffer.
 * \param[in,out] AvailableLength          Available size of target buffer.
 * \param[in]     SnapShotInpuPortInfoIdx  Index to snapshot, which shall be serialized.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetInputDataOfSession(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_SnapshotInputPortInfoIterType SnapShotInpuPortInfoIdx);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetOutputDataOfSession
 *********************************************************************************************************************/
/*!
 * \brief         Serialize output port related data of session.
 * \details       -
 * \param[in]     Destination              Pointer to target buffer.
 * \param[in,out] AvailableLength          Available size of target buffer.
 * \param[in]     PipeId                   Id of the desired pipe.
 * \param[in]     SnapShotInpuPortInfoIdx  Index to snapshot, which shall be serialized.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetOutputDataOfSession(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SnapshotOutputPortIterType SnapShotOutputPortIdx);

/**********************************************************************************************************************
 *  vStreamProc_Session_CopyAndCheck
 *********************************************************************************************************************/
/*!
 * \brief         Copy source data to destination with the given length, performs necessary checks and update .
 * \details       -
 * \param[in,out] Destination              Pointer to target buffer.
 * \param[in]     Source                   Pointer to source buffer.
 * \param[in]     Length                   Amount of data that shall be copied.
 * \param[in,out] AvailableLength          Available size of target buffer.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CopyAndCheck(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) Source,
  vStreamProc_LengthType Length,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreAndCheck
 *********************************************************************************************************************/
/*!
 * \brief         Writes the source data to the destination buffer and decrements the available length.
 * \details       -
 * \param[out]    Destination      Pointer to the destination of the source data.
 * \param[in]     Source           Pointer to the source data.
 * \param[in]     Length           Length which shall be written.
 * \param[in,out] AvailableLength  The available source data length. Is decremented by the written length after
                                   the source data is written to the destination.
 * \return        VSTREAMPROC_OK                  Restore successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT  Available length is smaller than source length.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreAndCheck(
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Source,
  vStreamProc_LengthType Length,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_EvaluateSessionHeader()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluates the provided session data and writes the derived header data to the provided session
 *                header data structure.
 * \details       -
 * \param[in]     PipeId              Id of the pipe.
 * \param[in]     SessionData         Pointer to the session data.
 * \param[in]     SessionDataLength   Length of the provided session data.
 * \param[out]    SessionHeaderData   Pointer to the session header data structure.
 * \return        VSTREAMPROC_OK      Session header data successfully written.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_EvaluateSessionHeader(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_Session_ConstDataPtr SessionData,
  vStreamProc_LengthType SessionDataLength,
  P2VAR(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreBaseStates()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the base states of the provided pipe based on the provided session header data.
 * \details       After restoration, the meta state switching is triggered.
 * \param[in]     PipeId              Id of the pipe.
 * \param[in]     SessionHeaderData   Pointer to the session header data structure.
 * \return        VSTREAMPROC_OK      Restoration successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreBaseStates(
  vStreamProc_PipeIdType PipeId,
  P2CONST(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSnapshots()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the snapshots of the provided pipe from the provided session data.
 * \details       -
 * \param[in]     PipeId              Id of the pipe.
 * \param[in,out] SnapshotData        Input: Pointer to the start address of the pipe snapshot data.
 *                                    Output: Pointer to the first byte after the pipe snapshot data.
 * \param[in,out] SnapshotDataLength  Input: Length of the provided snapshot data.
 *                                    Output: Length of the remaining snapshot data.
 * \return        VSTREAMPROC_OK      Restoration successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSnapshots(
  vStreamProc_PipeIdType PipeId,
  P2CONST(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreProcessingNodeSnapshot()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the snapshot of the provided processing node from the provided session data.
 * \details       -
 * \param[in]     ProcessingNodeId    Id of the processing node.
 * \param[in,out] SnapshotData        Input: Pointer to the start address of the processing node snapshot data.
 *                                    Output: Pointer to the first byte after the processing node snapshot data.
 * \param[in,out] SnapshotDataLength  Input: Length of the provided snapshot data.
 *                                    Output: Length of the remaining snapshot data.
 * \return        VSTREAMPROC_OK      Restoration successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreProcessingNodeSnapshot(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreInputPortSnapshots()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the input port snapshot of the provided processing node from the provided session data.
 * \details       -
 * \param[in]     ProcessingNodeId    Id of the processing node.
 * \param[in,out] SnapshotData        Input: Pointer to the start address of the input port snapshot data.
 *                                    Output: Pointer to the first byte after the snapshot input port data.
 * \param[in,out] SnapshotDataLength  Input: Length of the provided snapshot data.
 *                                    Output: Length of the remaining snapshot data.
 * \return        VSTREAMPROC_OK      Restoration successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreInputPortSnapshots(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreOutputPortSnapshots()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the output port snapshot of the provided processing node from the provided session data.
 * \details       -
 * \param[in]     ProcessingNodeId    Id of the processing node.
 * \param[in,out] SnapshotData        Input: Pointer to the start address of the output port snapshot data.
 *                                    Output: Pointer to the first byte after the snapshot output port data.
 * \param[in,out] SnapshotDataLength  Input: Length of the provided snapshot data.
 *                                    Output: Length of the remaining snapshot data.
 * \return        VSTREAMPROC_OK      Restoration successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreOutputPortSnapshots(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the runtime variables of the provided pipe from the snapshot data.
 * \details       -
 * \param[in]     PipeId    Id of the pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreRuntimeVariables(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreStreamRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the stream runtime variables of the provided output port with the data of the
 *                provided snapshot output port id.
 * \details       -
 * \param[in]     OutputPortHandle       Pointer to the output port handle.
 * \param[in]     SnapshotOutputPortId   Id of the snapshot output port info
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreStreamRuntimeVariables(
  P2CONST(vStreamProc_OutputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortHandle,
  vStreamProc_SnapshotOutputPortIdType SnapshotOutputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreStreamOutputPortRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the stream output port runtime variables of the provided input port with the data of the
 *                provided snapshot input port info id.
 * \details       -
 * \param[in]     InputPortHandle           Pointer to the input port handle.
 * \param[in]     SnapshotInputPortInfoId   Id of the snapshot input port info
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreStreamOutputPortRuntimeVariables(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle,
  vStreamProc_SnapshotInputPortIdType SnapshotInputPortInfoId);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreProcessingNodeWorkspace()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the processing node workspace of the provided processing node with the data of the
 *                provided snapshot slot.
 * \details       -
 * \param[in]     SnapshotSlotId          Id of the snapshot slot.
 * \param[in]     ProcessingNodeId        Id of the processing node.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreProcessingNodeWorkspace(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSignals()
 *********************************************************************************************************************/
/*!
 * \brief         Iterates over all nodes and triggers the required signals.
 * \details       The wave state flags for STARTING/ENDING are updated as well based on the pending WaveStart/WaveEnd
 *                signals.
 * \param[in]     PipeId                  Id of the pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSignals(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreInputPortSignals()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluates the producer wave and the consumer wave and sets the required input ports signals for the
 *                provided input port.
 * \details       -
 * \param[in]     InputPortHandle          Pointer to handle of the input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreInputPortSignals(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Session_RebuildConsumerWave()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluates the producer wave and sets the consumer wave runtime parameters of the provided input port
 *                accordingly.
 * \details       This is only necessary if there is no consumer wave, but a producer wave included in the snapshot.
 *                This might be possible if the producer started and possibly ended the wave without producing
 *                any data.
 * \param[in]     InputPortHandle          Pointer to handle of the input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RebuildConsumerWave(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Session_GetSessionEntryIdByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SessionEntryIdType, VSTREAMPROC_CODE) vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_ProcessingNodeIterType processingNodeOffset =
    (vStreamProc_ProcessingNodeIterType)ProcessingNodeId - vStreamProc_GetProcessingNodeStartIdxOfPipe(pipeId);
  vStreamProc_SessionEntryIdType sessionEntryId =
    vStreamProc_GetSessionEntryStartIdxOfSession(vStreamProc_GetSessionIdxOfPipe(pipeId))
    + (vStreamProc_SessionEntryIdType)processingNodeOffset;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the session entry id */
  return sessionEntryId;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSessionQueueHandleByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Session_GetSessionQueueHandleByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_ProcessingNodeIterType processingNodeOffset =
    (vStreamProc_ProcessingNodeIterType)ProcessingNodeId - vStreamProc_GetProcessingNodeStartIdxOfPipe(pipeId);
  /* Hint: The session queue handle has the same offset as the processing node.
           The offset is due to the special handles for USED/FREE in the queue. */
  vStreamProc_QueueHandleType sessionQueueHandle = (vStreamProc_QueueHandleType)processingNodeOffset + VSTREAMPROC_QUEUE_HANDLE_ENTRY_OFFSET;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the session queue handle. */
  return sessionQueueHandle;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueuePtrType, AUTOMATIC) vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* #10 Return the snapshot set queue address. */
  return vStreamProc_GetAddrSnapshotSetQueue(
    vStreamProc_GetSnapshotSetQueueIdxOfSnapshotSet(
      vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId)));
}

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateRestoreSessionData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_UpdateRestoreSessionData(
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength,
  vStreamProc_LengthType Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_Session_ConstDataPtr sessionData = *SessionData;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is enough buffer available. */
  if (Length <= *SessionDataLength)
  {
    /* #20 Update the session data pointer and length. */
    *SessionData = &sessionData[Length];
    *SessionDataLength -= Length;
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_IsMarkerValid()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Session_IsMarkerValid(
  uint8 SectionMarkerId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  uint8_least markerIdx;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) sectionMarker =
    vStreamProc_Session_SectionMarkers[SectionMarkerId];
  vStreamProc_Session_ConstDataPtr sessionDataPtr = *SessionData;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is enough data available. */
  if (*SessionDataLength >= VSTREAMPROC_SESSION_SECTION_MARKER_SIZE)
  {
    retVal = TRUE;

    /* #20 Check if the expected marker is set */
    for (markerIdx = 0u; markerIdx < VSTREAMPROC_SESSION_SECTION_MARKER_SIZE; markerIdx++)
    {
      if (sessionDataPtr[markerIdx] != sectionMarker[markerIdx])
      {
        retVal = FALSE;
        break;
      }
    }
  }

  /* #30 Update the session data parameters.*/
  *SessionDataLength -= VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
  *SessionData = &sessionDataPtr[VSTREAMPROC_SESSION_SECTION_MARKER_SIZE];

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_CheckDataFrameStateMarker()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CheckDataFrameStateMarker(
  vStreamProc_Session_State_DataFrameType DataFrameStateId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  uint8 sectionMarkerId = 0xFF;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the section marker id of the data from state. */
  switch (DataFrameStateId)
  {
    case VSTREAMPROC_SESSION_STATE_SET_STARTMARKER:
    {
      sectionMarkerId = VSTREAMPROC_SESSION_SECTION_PIPE_START;
      break;
    }
    case VSTREAMPROC_SESSION_STATE_SET_STATEMARKER:
    {
      sectionMarkerId = VSTREAMPROC_SESSION_SECTION_BASESTATE;
      break;
    }
    case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTMARKER:
    {
      sectionMarkerId = VSTREAMPROC_SESSION_SECTION_SNAPSHOT;
      break;
    }
    case VSTREAMPROC_SESSION_STATE_SET_ENDMARKER:
    {
      sectionMarkerId = VSTREAMPROC_SESSION_SECTION_PIPE_END;
      break;
    }
    default:
    {
      /* Nothing to do */
      break;
    }
  }

  /* #20 Check if the marker is valid. */
  if (sectionMarkerId != 0xFFu)
  {
    if (vStreamProc_Session_IsMarkerValid(sectionMarkerId, SessionData, SessionDataLength) == TRUE)
    {
      retVal = VSTREAMPROC_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_PrepareSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_PrepareSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_QueuePtrType sessionQueue =
    vStreamProc_GetAddrSessionQueue(vStreamProc_GetSessionQueueIdxOfSession(sessionId));
  vStreamProc_SessionEntryIterType sessionEntryIdx = vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
  vStreamProc_ProcessingNodeIterType processingNodeIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Invalidate old session. */
  vStreamProc_SetValidOfSessionInfo(sessionId, FALSE);

  /* #20 Prepare the queue. */
  vStreamProc_Queue_Init(
    sessionQueue,
    vStreamProc_GetAddrSessionQueueEntry(vStreamProc_GetSessionQueueEntryStartIdxOfSession(sessionId)),
    vStreamProc_GetSessionQueueEntryLengthOfSession(sessionId),
    VSTREAMPROC_QUEUE_PRIO_ORDER_ASC,
    0u, /* This is the offset of the session entry and the processing node. The start index is always known. */
    1u);

  /* #30 Fill the queue. */
  /* Hint: The session entry offset is the same as the corresponding processing node offset.
           Therefore, we can just iterate over the processing nodes directly. */
  for (processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
       processingNodeIdx < vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
       processingNodeIdx++)
  {
    vStreamProc_StreamOutputPortIterType consumerCount = 0u;
    vStreamProc_NamedOutputPortIterType namedOutputPortIdx;
    vStreamProc_NamedOutputPortIterType namedOutputPortOffset = 0u;

    /* #40 Compute the consumer count of the processing node. */
    for (namedOutputPortIdx = vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(processingNodeIdx);
         namedOutputPortIdx < vStreamProc_GetNamedOutputPortEndIdxOfProcessingNode(processingNodeIdx);
         namedOutputPortIdx++)
    {
      vStreamProc_StreamIterType streamIdx = vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortIdx);
      vStreamProc_OutputPortHandleType outputPortHandle;

      /* #50 Check if the port is connected and active. */
      (void)vStreamProc_Stream_GetOutputPortHandle(
            (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
            (vStreamProc_OutputPortSymbolicNameType)namedOutputPortOffset,
            &outputPortHandle);

      if(outputPortHandle.IsConnected == TRUE)
      {
        consumerCount +=
          (vStreamProc_StreamOutputPortIterType)vStreamProc_GetStreamOutputPortEndIdxOfStream(streamIdx)
            - (vStreamProc_StreamOutputPortIterType)vStreamProc_GetStreamOutputPortStartIdxOfStream(streamIdx);
      }

      namedOutputPortOffset++;
    }

    /* #60 Insert the processing node to the queue, based on the consumer count. */
    (void)vStreamProc_Queue_PrioInsert(sessionQueue, (vStreamProc_QueuePrioType)consumerCount);

    /* #70 Init the session entry. */
    vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
      sessionEntryIdx,
      (vStreamProc_SnapshotSlotIdxOfSessionEntryType)VSTREAMPROC_NO_SNAPSHOTSLOT);
    sessionEntryIdx++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_PrepareSessionEntries()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_PrepareSessionEntries(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ProcessingNodeIterType processingNodeIdx;
  vStreamProc_SessionEntryIterType sessionEntryIdx =
    vStreamProc_GetSessionEntryStartIdxOfSession(vStreamProc_GetSessionIdxOfPipe(PipeId));

  /* ----- Implementation ----------------------------------------------- */
  for (processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
       processingNodeIdx < vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
       processingNodeIdx++)
  {
    vStreamProc_QueueConstPtrType snapshotSetQueue =
      vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId((vStreamProc_ProcessingNodeIdType)processingNodeIdx);
    vStreamProc_QueueHandleType snapshotSetQueueHandle =vStreamProc_Queue_GetLastUsedHandle(snapshotSetQueue);

    /* #10 Check if at least one snapshot is available */
    if (snapshotSetQueueHandle != VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
    {
      /* #20 Set the latest snapshot in the session entry. */
      vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
        sessionEntryIdx,
        (vStreamProc_SnapshotSlotIdxOfSessionEntryType)vStreamProc_Queue_GetEntityHandle(
          snapshotSetQueue,
          snapshotSetQueueHandle));
    }

    sessionEntryIdx++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_ProcessSessionQueue()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_ProcessSessionQueue(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_QueuePtrType sessionQueue =
    vStreamProc_GetAddrSessionQueue(vStreamProc_GetSessionQueueIdxOfSession(vStreamProc_GetSessionIdxOfPipe(PipeId)));

  /* ----- Implementation ----------------------------------------------- */
    /* #10 Loop the session queue. */
  while (!vStreamProc_Queue_IsEmpty(sessionQueue))
  {
    vStreamProc_QueueHandleType sessionQueueHandle = vStreamProc_Queue_GetFirstUsedHandle(sessionQueue);

    vStreamProc_Session_ProcessSessionQueueHandle(PipeId, sessionQueue, sessionQueueHandle);

    /* #20 Remove the session queue entry */
    (void)vStreamProc_Queue_Remove(sessionQueue, sessionQueueHandle);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_ProcessSessionQueueHandle()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_ProcessSessionQueueHandle(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_QueueConstPtrType SessionQueue,
  vStreamProc_QueueHandleType SessionQueueHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_EntityHandleType offset = vStreamProc_Queue_GetEntityHandle(SessionQueue, SessionQueueHandle);
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId)+ (vStreamProc_ProcessingNodeIdType)offset;
  vStreamProc_SessionEntryIdType sessionEntryId =
    vStreamProc_GetSessionEntryStartIdxOfSession(
      vStreamProc_GetSessionIdxOfPipe(PipeId)) + (vStreamProc_SessionEntryIdType)offset;
  vStreamProc_SnapshotSlotIdType snapshotSlotId = vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryId);
  vStreamProc_NamedInputPortIterType inputPortSymbolicNameIdx;
  vStreamProc_NamedInputPortIterType namedInputPortIdx =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(processingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Loop over input ports of the processing node. */
  for (inputPortSymbolicNameIdx = 0;
       inputPortSymbolicNameIdx < vStreamProc_GetNamedInputPortLengthOfProcessingNode(processingNodeId);
       inputPortSymbolicNameIdx++)
  {
    vStreamProc_InputPortHandleType inputPortHandle;

    /* Get the input port handle */
    (void)vStreamProc_Stream_GetInputPortHandle(
      processingNodeId,
      (vStreamProc_InputPortSymbolicNameType)inputPortSymbolicNameIdx,
      &inputPortHandle);

    /* #20 Check if the input port is connected. */
    /* Hint: If the input port is not connected or not active,
              the consumer node was already skipped as consumer for producer node during preparation. */
    if ((inputPortHandle.IsConnected == TRUE) && (inputPortHandle.HasActiveProducer == TRUE))
    {
      /* Producer Node */
      vStreamProc_NamedOutputPortIdType producerNamedOutputPortId =
        vStreamProc_GetNamedOutputPortIdxOfStreamInfo(inputPortHandle.StreamId);
      vStreamProc_ProcessingNodeIdType producerNodeId =
        vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(producerNamedOutputPortId);
      vStreamProc_QueueHandleType producerSessionQueueHandle =
        vStreamProc_Session_GetSessionQueueHandleByProcessingNodeId(producerNodeId);

      /* #30 Evaluate the producer snapshots.*/
      vStreamProc_Session_EvaluateProducerSnapshots(
        producerNodeId,
        vStreamProc_GetOutputPortSymbolicNameOfPortDef(
          vStreamProc_GetPortDefIdxOfNamedOutputPort(producerNamedOutputPortId)),
        processingNodeId,
        (vStreamProc_InputPortSymbolicNameType)inputPortSymbolicNameIdx,
        snapshotSlotId);

      /* #40 Decrease the consumer count (priority) of the producer node session queue entry. */
      (void)vStreamProc_Queue_PrioUpdate(
        SessionQueue,
        producerSessionQueueHandle,
        vStreamProc_Queue_GetPriority(SessionQueue, producerSessionQueueHandle) - 1u);
    }

    namedInputPortIdx++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_EvaluateProducerSnapshots()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_EvaluateProducerSnapshots(
  vStreamProc_ProcessingNodeIdType ProducerNodeId,
  vStreamProc_OutputPortSymbolicNameType ProducerOutputPortSymbolicName,
  vStreamProc_ProcessingNodeIdType ConsumerNodeId,
  vStreamProc_InputPortSymbolicNameType ConsumerInputPortSymbolicName,
  vStreamProc_SnapshotSlotIdType ConsumerSnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SnapshotSlotIterType selectedProducerSnapshotSlotIdx =
    (vStreamProc_SnapshotSlotIterType)vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
      vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(ProducerNodeId));
  vStreamProc_SnapshotSlotIterType newProducerSnapshotSlotIdx =
    (vStreamProc_SnapshotSlotIterType)VSTREAMPROC_NO_SNAPSHOTSLOT;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the producer has a valid session entry. */
  if (selectedProducerSnapshotSlotIdx != VSTREAMPROC_NO_SNAPSHOTSLOT)
  {
    /* Producer */
    vStreamProc_QueueConstPtrType producerSnapshotSetQueue =
      vStreamProc_Session_GetSnapshotSetQueueByProcessingNodeId(ProducerNodeId);
    vStreamProc_QueueHandleType currentQueueHandle = vStreamProc_Queue_GetFirstUsedHandle(producerSnapshotSetQueue);

    /* Consumer */
    vStreamProc_NamedInputPortIdType consumerNamedInputPortId =
      (vStreamProc_NamedInputPortIdType)vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ConsumerNodeId)
      + (vStreamProc_NamedInputPortIdType)ConsumerInputPortSymbolicName;
    boolean isVirtual =
      vStreamProc_Stream_IsInputPortVirtual(consumerNamedInputPortId);
    boolean isEntryNode = vStreamProc_AccessNode_IsEntryNode(ProducerNodeId);
    vStreamProc_StreamPositionType consumePosition;
    vStreamProc_WaveHandleType consumeWaveHandle;

    /* #20 Get consumer reference values from the snapshot, if valid. */
    if (ConsumerSnapshotSlotId != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      vStreamProc_SnapshotInputPortIdType consumerSnapshotInputPortId =
        vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(ConsumerSnapshotSlotId)
        + ConsumerInputPortSymbolicName;
      consumePosition = vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(consumerSnapshotInputPortId);
      consumeWaveHandle = vStreamProc_GetWaveHandleOfSnapshotInputPortInfo(consumerSnapshotInputPortId);
    }
    /* #30 Otherwise, set reference values manually */
    /* Hint: There might be still a suitable producer snapshot which consumed data but did not produce any data yet. */
    else
    {
      consumePosition = 0u;
      consumeWaveHandle = 1u;
    }

    /* #40 Loop the available producer snapshots. */
    while (currentQueueHandle != VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
    {
      boolean breakLoop = TRUE;
      vStreamProc_SnapshotSlotIterType producerSnapshotSlotIdx =
        (vStreamProc_SnapshotSlotIterType)vStreamProc_Queue_GetEntityHandle(producerSnapshotSetQueue, currentQueueHandle);
      vStreamProc_SnapshotOutputPortIterType producerSnapshotOutputPortIdx =
        vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(producerSnapshotSlotIdx)
        + (vStreamProc_SnapshotOutputPortIterType)ProducerOutputPortSymbolicName;
      vStreamProc_SnapshotWaveInfoIterType snapshotWaveIdx =
        vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(producerSnapshotOutputPortIdx)
        + (vStreamProc_SnapshotWaveInfoIterType)vStreamProc_Stream_GetWaveLevelOfProcessingNode(ConsumerNodeId);
      vStreamProc_WaveHandleType produceWaveHandle = vStreamProc_GetHandleOfSnapshotWaveInfo(snapshotWaveIdx);
      vStreamProc_StreamPositionType producePosition =
        vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveIdx);

      /* #50 Check if the producer snapshot is behind or equal the consumer snapshot. */
      /* Hint: If the consumer is virtual , a produce position greater than the consume position is allowed because
               there is no real data involved. If the producer is an entry node, the produce position will be patched
               at the end of the session creation. */
      if ( (produceWaveHandle < consumeWaveHandle)
        || ( (produceWaveHandle == consumeWaveHandle)
          && ( (  (producePosition <= consumePosition)
              ||  (isVirtual == TRUE))
              ||  (isEntryNode == TRUE))))
      {
        /* #60 Update the latest possible snapshot slot. */
        newProducerSnapshotSlotIdx = producerSnapshotSlotIdx;

        /* #70 Check if the new producer snapshot slot is not equal to the snapshot referenced in the session. */
        /* Hint: If the snapshot is already selected in the session, it means another consumer selected it as
                  latest possible snapshot or it is the latest snapshot for the node.
                  Therefore we must not continue in this case. */
        if (newProducerSnapshotSlotIdx != selectedProducerSnapshotSlotIdx)
        {
          /* #80 Check the next producer snapshot.*/
          currentQueueHandle = vStreamProc_Queue_GetNextHandle(producerSnapshotSetQueue, currentQueueHandle);
          breakLoop = FALSE;
        }
      }

      if (breakLoop == TRUE)
      {
        break;
      }
    }

    /* #90 Check if the producer session entry has to be updated. */
    if (newProducerSnapshotSlotIdx != selectedProducerSnapshotSlotIdx)
    {
      vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
        vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(ProducerNodeId),
        (vStreamProc_SnapshotSlotIdxOfSessionEntryType)newProducerSnapshotSlotIdx);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_CheckSessionValidity()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CheckSessionValidity(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_SessionEntryIterType sessionEntryIdx;
  vStreamProc_SessionEntryIterType sessionEntryCount = 0u;
  boolean sessionValid = FALSE;
  boolean onlyExitNodes = TRUE;
  vStreamProc_ProcessingNodeIterType processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the session entries */
  for (sessionEntryIdx =vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
       sessionEntryIdx < vStreamProc_GetSessionEntryEndIdxOfSession(sessionId);
       sessionEntryIdx++)
  {
    vStreamProc_SnapshotSlotIterType snapshotSlotIdx =
      vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryIdx);

    /*  #20 Update the session entry count and update the produce position for entry nodes. */
    if (snapshotSlotIdx != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      sessionEntryCount++;

      /* #30 If the corresponding node is an entry node, update the produce position. */
      if (vStreamProc_AccessNode_IsEntryNode((vStreamProc_ProcessingNodeIdType)processingNodeIdx) == TRUE)
      {
        vStreamProc_Session_UpdateEntryNodeSnapshot(
          (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
          (vStreamProc_SnapshotSlotIdType)snapshotSlotIdx);
      }

      /* #40 If the corresponding node is an exit node, update the consume position. */
      if (vStreamProc_AccessNode_IsExitNode((vStreamProc_ProcessingNodeIdType)processingNodeIdx) == TRUE)
      {
        vStreamProc_Session_UpdateExitNodeSnapshot(
          (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
          (vStreamProc_SnapshotSlotIdType)snapshotSlotIdx);
      }
      else
      {
        onlyExitNodes = FALSE;
      }
    }

    processingNodeIdx++;
  }

  /* #50 Check if at least one snapshot is referenced and at least one of the snapshots is not from an exit node. */
  /* Hint: As the exit nodes snapshots are created during CreateSession(), and because they don't have any
           consumer nodes, the exit node snapshots are never deleted by the algorithm.
           However, a session with only exit nodes does not provide any benefit and is therefore invalid. */
  if ((sessionEntryCount != 0u) && (onlyExitNodes == FALSE))
  {
    /* #60 Set session valid */
    sessionValid = TRUE;
    retVal = VSTREAMPROC_OK;
  }

  vStreamProc_SetValidOfSessionInfo(sessionId, sessionValid);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateEntryNodeSnapshot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_UpdateEntryNodeSnapshot(                             /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType EntryNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* Producer */
  vStreamProc_StreamIdType streamId =
    vStreamProc_GetStreamIdxOfNamedOutputPort(
      vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(EntryNodeId));
  /* Hint: Entry nodes always have exactly one output port. */
  vStreamProc_SnapshotOutputPortIdType snapshotOutputPortId =
    vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(SnapshotSlotId);
  vStreamProc_SnapshotWaveIdType snapshotWaveStartId =
    vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(snapshotOutputPortId);
  vStreamProc_StreamOutputPortIterType streamOutputPortIdx;
  vStreamProc_SnapshotWaveIterType snapshotWaveIdx;
  vStreamProc_WaveLevelType waveLevel = 0u;
  boolean anyConsumerSnapshotsAvailable = FALSE;

  /* Consumer */
  /* On higher level waves, the produce position might not start with 0, so the offset has to be used to compare the
     consume position over different wave levels. */
  vStreamProc_StreamPositionType maxConsumePositionOffset = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the stream output ports. */
  for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(streamId);
       streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(streamId);
       streamOutputPortIdx++)
  {
    vStreamProc_InputPortHandleType inputPortHandle;
    vStreamProc_NamedInputPortIterType namedInputPortIdx =
      vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(streamOutputPortIdx);
    vStreamProc_ProcessingNodeIterType consumerNodeIdx =
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortIdx);
    vStreamProc_InputPortSymbolicNameType inputPortSymbolicName =
      vStreamProc_GetInputPortSymbolicNameOfPortDef(vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortIdx));

    (void)vStreamProc_Stream_GetInputPortHandle(
      (vStreamProc_ProcessingNodeIdType)consumerNodeIdx,
      inputPortSymbolicName,
      &inputPortHandle);

    /* #20 Check if the consumer is active and registered on the current wave level. */
    if (inputPortHandle.IsConnected == TRUE)
    {
      /* Consumer */
      vStreamProc_SnapshotSlotIdType consumerSessionSnapshotSlotId =
        vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
          vStreamProc_Session_GetSessionEntryIdByProcessingNodeId((vStreamProc_ProcessingNodeIdType)consumerNodeIdx));

      /* #30 Check if a snapshot is available for the consumer. */
      if (consumerSessionSnapshotSlotId != VSTREAMPROC_NO_SNAPSHOTSLOT)
      {
        /* Producer */
        vStreamProc_SnapshotWaveIdType snapshotWaveInfoId = snapshotWaveStartId + inputPortHandle.WaveLevel;
        vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfSnapshotWaveInfo(snapshotWaveInfoId);

        /* Consumer */
        vStreamProc_SnapshotInputPortIdType snapshotInputPortInfoId =
          vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(consumerSessionSnapshotSlotId)
          + (vStreamProc_SnapshotInputPortIdType)inputPortHandle.InputPortSymbolicName;
        vStreamProc_WaveHandleType consumerWaveHandle =
          vStreamProc_GetWaveHandleOfSnapshotInputPortInfo(snapshotInputPortInfoId);

        anyConsumerSnapshotsAvailable = TRUE;

        /* #40 Check if the consumer is on the same wave handle. */
        /* Hint: If the consumer has a snapshot, the ConsumerWaveHandle is always equal or higher than the
                 ProducerWaveHandle. As there is always one consumer snapshot on the same wave handle, the consumers
                 with higher wave handles can be ignored. */
        if (consumerWaveHandle == producerWaveHandle)
        {
          /* Producer */
          vStreamProc_StreamPositionType producePosition =
            vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveInfoId);

          /* Consumer */
          vStreamProc_StreamPositionType consumePosition =
            vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(snapshotInputPortInfoId);

          if (producePosition > consumePosition)
          {
            vStreamProc_StreamPositionType currentPositionOffset = producePosition - consumePosition;

            if (maxConsumePositionOffset < currentPositionOffset)
            {
              maxConsumePositionOffset = currentPositionOffset;
            }
          }
        }
      }
      else
      {
        /* #50 Set the overall consume position to the produce position of the lowest wave level and leave the loop. */
        /* Hint: If the consumer node has no snapshot, all data has to be provided again. */
        maxConsumePositionOffset = vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveStartId);
        break;
      }
    }
  }

  /* #60 Iterate over all waves and update the snapshot data. */
  for (snapshotWaveIdx = (vStreamProc_SnapshotWaveIterType)snapshotWaveStartId;
       snapshotWaveIdx < vStreamProc_GetSnapshotWaveEndIdxOfSnapshotOutputPort(snapshotOutputPortId);
       snapshotWaveIdx++)
  {
    /* #70 Check if at least one consumer snapshot is available. */
    if (anyConsumerSnapshotsAvailable == TRUE)
    {
      vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfSnapshotWaveInfo(snapshotWaveIdx);
    /* #80 Decrease the produce position with the calculated offset.*/
      vStreamProc_SetProducePositionOfSnapshotWaveInfo(
        snapshotWaveIdx,
        vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveIdx) - maxConsumePositionOffset);

      /* #90 Clear the ENDING flag and set the ACTIVE flag if necessary. */
      /* Hint: If the offset is 0, all consumers have consumed the data of this wave, and the ENDING/IDLE state
               can be kept. */
      if ( (maxConsumePositionOffset > 0u)
        && ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING)
          || (waveState == VSTREAMPROC_WAVESTATE_IDLE))))
      {
        vStreamProc_SetWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);
        vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
        vStreamProc_SetStateOfSnapshotWaveInfo(snapshotWaveIdx, waveState);
      }
    }
    /* #100 Otherwise: */
    else
    {
      /* #110 Remove the entry node snapshot from session. */
      vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
        vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(EntryNodeId),
        (vStreamProc_SnapshotSlotIdxOfSessionEntryType)VSTREAMPROC_NO_SNAPSHOTSLOT);
    }

    waveLevel++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_UpdateExitNodeSnapshot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_UpdateExitNodeSnapshot(
  vStreamProc_ProcessingNodeIdType ExitNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* Hint: Exit nodes always have exactly one input port. */
  vStreamProc_InputPortHandleType inputPortHandle;
  vStreamProc_ReturnType localRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the exit node is connected. */
  localRetVal = vStreamProc_Stream_GetInputPortHandle(
    ExitNodeId,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
    &inputPortHandle);

  if (localRetVal == VSTREAMPROC_OK)
  {
    vStreamProc_SnapshotWaveIdType producerSnapshotWaveId =
      vStreamProc_Session_GetProducerSnapshotWaveStartId(inputPortHandle.NamedInputPortId);

    /* #20 Check if there is a valid producer snapshot available. */
    if (producerSnapshotWaveId != VSTREAMPROC_NO_SNAPSHOTWAVE)
    {
      vStreamProc_SnapshotInputPortIdType consumerSnapshotInputPortId =
        vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(SnapshotSlotId);
      vStreamProc_WaveHandleType producerWaveHandle =
        vStreamProc_GetHandleOfSnapshotWaveInfo(producerSnapshotWaveId);
      vStreamProc_WaveHandleType consumerWaveHandle =
        vStreamProc_GetWaveHandleOfSnapshotInputPortInfo(consumerSnapshotInputPortId);
      vStreamProc_StreamPositionType producePosition =
        vStreamProc_GetProducePositionOfSnapshotWaveInfo(producerSnapshotWaveId);

      /* #30 Check if the consumer snapshot already contains a newer wave. */
      if (consumerWaveHandle > producerWaveHandle)
      {
        /* #40 Update the exit node snapshot with the produce position and wave handle/state of the producer node. */
        /* Hint: This is necessary to have access to meta data for the GetExitPoint*InfoOfSession() API */
        vStreamProc_SetWaveHandleOfSnapshotInputPortInfo(
          consumerSnapshotInputPortId,
          vStreamProc_GetHandleOfSnapshotWaveInfo(producerSnapshotWaveId));

        vStreamProc_SetAbsolutePositionStartOfSnapshotInputPortInfo(
          consumerSnapshotInputPortId,
          vStreamProc_GetAbsolutePositionStartOfSnapshotWaveInfo(producerSnapshotWaveId));

        vStreamProc_SetConsumePositionOfSnapshotInputPortInfo(consumerSnapshotInputPortId, producePosition);

        vStreamProc_SetWaveStateOfSnapshotInputPortInfo(
          consumerSnapshotInputPortId,
          vStreamProc_GetStateOfSnapshotWaveInfo(producerSnapshotWaveId));
      }
      /* #50 Otherwise (Same wave handle):*/
      else
      {
        vStreamProc_StreamPositionType consumePosition =
          vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(consumerSnapshotInputPortId);

        /* #60 Set the consume position back to the produce position if the consume position is higher. */
        if (consumePosition > producePosition)
        {
          vStreamProc_SetConsumePositionOfSnapshotInputPortInfo(consumerSnapshotInputPortId, producePosition);
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetProducerSnapshotWaveStartId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SnapshotWaveIdType, VSTREAMPROC_CODE) vStreamProc_Session_GetProducerSnapshotWaveStartId(
  vStreamProc_NamedInputPortIdType NamedInputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SnapshotWaveIdType snapshotWaveId = VSTREAMPROC_NO_SNAPSHOTWAVE;
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_Stream_GetInputPortHandle(
    vStreamProc_GetProcessingNodeIdxOfNamedInputPort(NamedInputPortId),
    vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
    &inputPortHandle);

  /* #10 Check if the input port is connected and if producer is active. */
  if ((inputPortHandle.IsConnected == TRUE) && (inputPortHandle.HasActiveProducer == TRUE))
  {
    vStreamProc_NamedOutputPortIdType producerNamedOutputPortId =
      vStreamProc_GetNamedOutputPortIdxOfStreamInfo(inputPortHandle.StreamId);

    vStreamProc_SnapshotSlotIdType producerSnapshotSlotId =
      vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
        vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(
          vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(producerNamedOutputPortId)));

    /* #20 Check if the producer has a valid snapshot in session. */
    if (producerSnapshotSlotId != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      vStreamProc_SnapshotOutputPortIdType snapshotOutputPortId =
        vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(producerSnapshotSlotId)
        + (vStreamProc_SnapshotOutputPortIdType)vStreamProc_GetOutputPortSymbolicNameOfPortDef(
            vStreamProc_GetPortDefIdxOfNamedOutputPort(producerNamedOutputPortId));

      snapshotWaveId = vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(snapshotOutputPortId);
    }
  }
  return snapshotWaveId;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetSnapshotDataOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetSnapshotDataOfSession(          /* PRQA S 6030 */ /* MD_MSR_STCYC */
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SessionEntryIterType SessionEntryIdx)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;

  vStreamProc_Session_DataPtr sessionData     = *Destination;
  vStreamProc_LengthType      availableLength = *AvailableLength;

  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_SnapshotSlotIterType snapShotSlotIdx = vStreamProc_GetSnapshotSlotIdxOfSessionEntry(SessionEntryIdx);
  uint8 procNodeOffset = (uint8)(SessionEntryIdx - vStreamProc_GetSessionEntryStartIdxOfSession(sessionId));
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) snapshotWorkspace =
    (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))vStreamProc_GetWorkspaceOfSnapshotSlot(snapShotSlotIdx);             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  vStreamProc_Session_State_SnapshotType state = VSTREAMPROC_SESSION_STATE_SET_NODEMARKER;
  const vStreamProc_ProcessingNodeIdType processingNodeId = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId) +
    (vStreamProc_ProcessingNodeIdType)procNodeOffset;

  /* #10 Serialize section by section until error occur or end is reached. */
  do
  {
    P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) sourceDataPtr = NULL_PTR;
    vStreamProc_LengthType sourceLength = 0u;

    switch (state)
    {
      case VSTREAMPROC_SESSION_STATE_SET_NODEMARKER:
      {
        sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_NODE];
        sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_RELNODEIDX:
      {
        sourceDataPtr = &procNodeOffset;
        sourceLength  = sizeof(procNodeOffset);
        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_INPUTDATA:
      {
        vStreamProc_SnapshotInputPortInfoIterType snapShotInpuPortInfoIdx;

        for (snapShotInpuPortInfoIdx = vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(snapShotSlotIdx);
             snapShotInpuPortInfoIdx < vStreamProc_GetSnapshotInputPortInfoEndIdxOfSnapshotSlot(snapShotSlotIdx);
             snapShotInpuPortInfoIdx++)
        {
          retVal = vStreamProc_Session_GetInputDataOfSession(&sessionData, &availableLength, snapShotInpuPortInfoIdx);
        }

        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_OUTPUTDATA:
      {
        vStreamProc_SnapshotOutputPortIterType snapShotOutputPortIdx;

        for (snapShotOutputPortIdx = vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(snapShotSlotIdx);
             snapShotOutputPortIdx < vStreamProc_GetSnapshotOutputPortEndIdxOfSnapshotSlot(snapShotSlotIdx);
             snapShotOutputPortIdx++)
        {
          retVal =
            vStreamProc_Session_GetOutputDataOfSession(&sessionData, &availableLength, PipeId, snapShotOutputPortIdx);
        }

        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_WORKSPACEMARKER:
      {
        if (snapshotWorkspace != NULL_PTR)
        {
          sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_WORKSPACE];
          sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
        }

        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_WORKSPACE:
      {
        if (snapshotWorkspace != NULL_PTR)
        {
          sourceDataPtr = snapshotWorkspace;
          sourceLength  = vStreamProc_GetWorkspaceSizeOfProcessingNodeClassDef(
            vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
            vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(processingNodeId)));
        }

        break;
      }
      default:
      {
        /* This state shouldn't be reachable - force leaving loop with error. */
        retVal = VSTREAMPROC_FAILED;
        break;
      }
    };

    /* #20 Serialize data if necessary in this step. */
    if (sourceLength > 0u)
    {
      retVal = vStreamProc_Session_CopyAndCheck(&sessionData, sourceDataPtr, sourceLength, &availableLength);
    }

    /* #30 Switch to next state. */
    state++;                                                                                                            /* PRQA S 4527 1 */ /* MD_vStreamProc_Rule10.1_4527_EnumInc */
  }
  while ((retVal == VSTREAMPROC_OK) && (state < VSTREAMPROC_SESSION_STATE_SNAP_FRAMECOUNT));

  /* #40 Update return values */
  *Destination      = sessionData;
  *AvailableLength  = availableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetInputDataOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetInputDataOfSession(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_SnapshotInputPortInfoIterType SnapShotInpuPortInfoIdx)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;

  vStreamProc_Session_DataPtr sessionData     = *Destination;
  vStreamProc_LengthType      availableLength = *AvailableLength;

  /* #10 Set magic value for input port section. */
  retVal = vStreamProc_Session_CopyAndCheck(
    &sessionData,
    vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_INPUTPORT],
    VSTREAMPROC_SESSION_SECTION_MARKER_SIZE,
    &availableLength);

  /* #20 Set input port info. */
  if ((vStreamProc_HasSnapshotInputPortInfo()) && (retVal == VSTREAMPROC_OK))
  {
    retVal = vStreamProc_Session_CopyAndCheck(
      &sessionData,
      (vStreamProc_Session_DataPtr)vStreamProc_GetAddrSnapshotInputPortInfo(SnapShotInpuPortInfoIdx),
      sizeof(vStreamProc_SnapshotInputPortInfoType),
      &availableLength);
  }

  /* Update return values */
  *Destination      = sessionData;
  *AvailableLength  = availableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetOutputDataOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetOutputDataOfSession(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength,
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SnapshotOutputPortIterType SnapShotOutputPortIdx)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;

  vStreamProc_Session_DataPtr sessionData     = *Destination;
  vStreamProc_LengthType      availableLength = *AvailableLength;

  vStreamProc_WaveLevelType         waveLevel = 0u;
  vStreamProc_SnapshotWaveIterType  snapShotWaveIdx;

  /* #10 Set magic value for output port section. */
  retVal = vStreamProc_Session_CopyAndCheck(
    &sessionData,
    vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_OUTPUTPORT],
    VSTREAMPROC_SESSION_SECTION_MARKER_SIZE,
    &availableLength);

  /* #20 For each wave level. */
  for (snapShotWaveIdx = vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(SnapShotOutputPortIdx);
       snapShotWaveIdx < vStreamProc_GetSnapshotWaveEndIdxOfSnapshotOutputPort(SnapShotOutputPortIdx);
       snapShotWaveIdx++)
  {
    P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer =
      vStreamProc_GetMetaDataPointerOfSnapshotWave(snapShotWaveIdx);

    /* #30 Set magic value for wave section. */
    if (retVal == VSTREAMPROC_OK)
    {
      retVal = vStreamProc_Session_CopyAndCheck(
        &sessionData,
        vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_WAVE],
        VSTREAMPROC_SESSION_SECTION_MARKER_SIZE,
        &availableLength);
    }

    /* #40 Set SnapshotWaveInfo content. */
    if ((vStreamProc_HasSnapshotWaveInfo()) && (retVal == VSTREAMPROC_OK))
    {
      retVal = vStreamProc_Session_CopyAndCheck(
        &sessionData,
        (vStreamProc_Session_DataPtr)vStreamProc_GetAddrSnapshotWaveInfo(snapShotWaveIdx),
        sizeof(vStreamProc_SnapshotWaveInfoType),
        &availableLength);
    }

    /* #50 If meta data are available. */
    if (metaDataBuffer != NULL_PTR)
    {
      /* #60 Set magic value for meta section. */
      if (retVal == VSTREAMPROC_OK)
      {
        retVal = vStreamProc_Session_CopyAndCheck(
          &sessionData,
          vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_METADATA],
          VSTREAMPROC_SESSION_SECTION_MARKER_SIZE,
          &availableLength);
      }
      /* #70 Set meta data. */
      if (retVal == VSTREAMPROC_OK)
      {
        vStreamProc_WaveTypeDefIdType waveTypeDefId =
          vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
            vStreamProc_GetWaveLevelDefStartIdxOfPipe(PipeId) + waveLevel);
        vStreamProc_LengthType metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefId);

        retVal = vStreamProc_Session_CopyAndCheck(&sessionData, metaDataBuffer, metaDataSize, &availableLength);
      }
    }

    waveLevel++;
  }

  /* Update return values */
  *Destination      = sessionData;
  *AvailableLength  = availableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_CopyAndCheck
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CopyAndCheck(
  P2VAR(vStreamProc_Session_DataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) Source,
  vStreamProc_LengthType Length,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_INSUFFICIENT_INPUT;

  /* #10 If there is enough free space in buffer. */
  if (*AvailableLength >= Length)
  {
    vStreamProc_LengthType idx;

    /* #20 Copy data. */
    for (idx = 0u; idx < Length; idx++)
    {
      (*Destination)[idx] = Source[idx];
    }

    /* #30 Update return values. */
    *AvailableLength -= Length;
    (*Destination)    = &((*Destination)[Length]);
    retVal            = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreAndCheck
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreAndCheck(
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) Destination,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) Source,
  vStreamProc_LengthType Length,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
  vStreamProc_Session_ConstDataPtr sourceData = *Source;

  /* #10 If there is enough free space in buffer. */
  if (*AvailableLength >= Length)
  {
    vStreamProc_LengthType idx;

    /* #20 Copy data. */
    for (idx = 0u; idx < Length; idx++)
    {
      Destination[idx] = sourceData[idx];
    }

    /* #30 Update return values. */
    *AvailableLength -= Length;
    *Source = &sourceData[Length];
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_EvaluateSessionHeader()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_EvaluateSessionHeader(             /* PRQA S 6030 */ /* MD_MSR_STCYC */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_Session_ConstDataPtr SessionData,
  vStreamProc_LengthType SessionDataLength,
  P2VAR(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_Session_State_DataFrameType state = VSTREAMPROC_SESSION_STATE_SET_STARTMARKER;
  vStreamProc_Session_ConstDataPtr sessionData = SessionData;
  vStreamProc_LengthType sessionDataLength = SessionDataLength;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the provided session data. */
  do
  {
    /* #20 Evaluate state. */
    switch (state)
    {
      /* #30 The next element is a marker. */
      case VSTREAMPROC_SESSION_STATE_SET_STARTMARKER:
      case VSTREAMPROC_SESSION_STATE_SET_STATEMARKER:
      case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTMARKER:
      case VSTREAMPROC_SESSION_STATE_SET_ENDMARKER:
      {
        /* #40 Check the validity of the marker. */
        retVal = vStreamProc_Session_CheckDataFrameStateMarker(state, &sessionData, &sessionDataLength);

        break;
      }
      /* #50 The next element is the format version. */
      case VSTREAMPROC_SESSION_STATE_SET_VERSION:
      {
        retVal = vStreamProc_Session_RestoreAndCheck(SessionHeaderData->Version, &sessionData, 2u, &sessionDataLength);

        if (retVal == VSTREAMPROC_OK)
        {
          retVal = VSTREAMPROC_FAILED;

          /* #60 Check the version. */
          if ((SessionHeaderData->Version[0] == VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MAJOR)
            && (SessionHeaderData->Version[1] <= VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MINOR))
          {
            retVal = VSTREAMPROC_OK;
          }
        }
        break;
      }
      /* #70 The next element is the length of the session.*/
      case VSTREAMPROC_SESSION_STATE_RESERVE_LENGTH:
      {
        /* #80 Read the session size. */
        retVal = vStreamProc_Session_RestoreAndCheck(
          (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))&SessionHeaderData->TotalSize,
          &sessionData,
          sizeof(vStreamProc_LengthType),
          &sessionDataLength);

        /* #90 Check if total size matches provided size. */
        if (SessionHeaderData->TotalSize != SessionDataLength)
        {
          retVal = VSTREAMPROC_FAILED;
        }
        break;
      }
      /* #100 The next element is the UUID: */
      case VSTREAMPROC_SESSION_STATE_SET_UUID:
      {
        vStreamProc_LengthType uuidIdx;
        P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) uuid = sessionData;
        P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) refUuid =
          (P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))vStreamProc_GetAddrUuidOfPipe(PipeId);

        /* #110 Validate the UUID */
        for (uuidIdx = 0u; uuidIdx < 8u; uuidIdx++)
        {
          retVal = VSTREAMPROC_OK;;
          if (uuid[uuidIdx] != refUuid[uuidIdx])
          {
            retVal = VSTREAMPROC_FAILED;
            break;
          }
        }

        sessionData = &sessionData[uuidIdx];
        sessionDataLength -= uuidIdx;
        break;
      }
      /* #120 If the next elements are the base states: */
      case VSTREAMPROC_SESSION_STATE_SET_BASESTATES:
      {
        vStreamProc_BaseStateIdType baseStateCount =
          (vStreamProc_BaseStateIdType)vStreamProc_GetBaseStateEndIdxOfPipe(PipeId)
          - (vStreamProc_BaseStateIdType)vStreamProc_GetBaseStateStartIdxOfPipe(PipeId);

        retVal = VSTREAMPROC_OK;

        /* #130 Check if mode handling is used in the pipe. */
        if (baseStateCount > 0u)
        {
          /* #140 Save the start index in the session header data */
          SessionHeaderData->BaseStateStart = sessionData;
          retVal =
            vStreamProc_Session_UpdateRestoreSessionData(
              &sessionData,
              &sessionDataLength,
              (vStreamProc_LengthType)baseStateCount);
        }

        SessionHeaderData->BaseStateCount = baseStateCount;
        break;
      }
      /* #150 If the next element is the snapshot count: */
      case VSTREAMPROC_SESSION_STATE_RESERVE_SNAPSHOTCOUNT:
      {
        /* #160 Save the snapshot count to the session header data. */
        retVal = vStreamProc_Session_RestoreAndCheck(
          &SessionHeaderData->SnapshotCount,
          &sessionData,
          1u,
          &sessionDataLength);
        break;
      }
      /* #170 If the next elements are the snapshots: */
      case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTDATA:
      {
        /* #180 Save the start index in the session header data. */
        SessionHeaderData->SnapshotDataStart = sessionData;
        SessionHeaderData->SnapshotDataLength = sessionDataLength - VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;

        /* Hint: The position is set to the last marker to check if the session is complete in the next step. */
        retVal = vStreamProc_Session_UpdateRestoreSessionData(
          &sessionData,
          &sessionDataLength,
          SessionHeaderData->SnapshotDataLength);
        break;
      }
      default:
      {
        /* This state shouldn't be reachable - force leaving loop with error. */
        retVal = VSTREAMPROC_FAILED;
        break;
      }
   }

   /* #190 Switch to next state. */
   state++;                                                                                                             /* PRQA S 4527 1 */ /* MD_vStreamProc_Rule10.1_4527_EnumInc */

  } while ((retVal == VSTREAMPROC_OK) && (state < VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTCOUNT));

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreBaseStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreBaseStates(
  vStreamProc_PipeIdType PipeId,
  P2CONST(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_BaseStateIterType baseStateIdx;
  vStreamProc_BaseStateIterType baseStateSessionDataIdx = 0u;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) baseStateData = SessionHeaderData->BaseStateStart;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Restore the base state activation states. */
  for (baseStateIdx = vStreamProc_GetBaseStateStartIdxOfPipe(PipeId);
       baseStateIdx < vStreamProc_GetBaseStateEndIdxOfPipe(PipeId);
       baseStateIdx++)
  {
    boolean activationState = FALSE;

    if (baseStateData[baseStateSessionDataIdx] == 1u)
    {
      activationState = TRUE;
    }

    vStreamProc_SetActiveOfBaseStateInfo(baseStateIdx, activationState);
    baseStateSessionDataIdx++;
  }

  /* #20 Perform the meta state switching. */
  if (vStreamProc_Mode_HandleMetaStateSwitching(PipeId) == E_OK)
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSnapshots()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSnapshots(
  vStreamProc_PipeIdType PipeId,
  P2CONST(vStreamProc_SessionHeaderDataType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionHeaderData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIterType processingNodeStartIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
  vStreamProc_ProcessingNodeIterType processingNodeEndIdx = vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
  vStreamProc_ProcessingNodeIterType processingNodeCount = processingNodeEndIdx - processingNodeStartIdx;
  vStreamProc_ProcessingNodeIterType processingNodeIdx = processingNodeStartIdx;
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_SessionEntryIdType sessionEntryStartId = vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
  vStreamProc_Session_ConstDataPtr snapshotData = SessionHeaderData->SnapshotDataStart;
  vStreamProc_LengthType snapshotDataLength = SessionHeaderData->SnapshotDataLength;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the snapshot data. */
  do
  {
    retVal = VSTREAMPROC_FAILED;

    /* #20 Check if the node marker is valid. */
    if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_NODE, &snapshotData, &snapshotDataLength) == TRUE)
    {
      uint8 relativeProcessingNodeIndex;

      /* #30 Read and check relative node index. */
      retVal = vStreamProc_Session_RestoreAndCheck(
        &relativeProcessingNodeIndex,
        &snapshotData,
        1u,
        &snapshotDataLength);

      /* #40 Check if relative node index is out of range. */
      if (relativeProcessingNodeIndex >= processingNodeCount)
      {
        retVal = VSTREAMPROC_FAILED;
      }

      if (retVal == VSTREAMPROC_OK)
      {
        processingNodeIdx = processingNodeStartIdx + relativeProcessingNodeIndex;

        /* #50 Restore the processing node snapshot. */
        retVal =
          vStreamProc_Session_RestoreProcessingNodeSnapshot(
            (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
            &snapshotData,
            &snapshotDataLength);
      }

      /* #60 Add the restored snapshot to the session. */
      if (retVal == VSTREAMPROC_OK)
      {
        vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
          sessionEntryStartId + relativeProcessingNodeIndex,
          vStreamProc_GetSnapshotSlotStartIdxOfSnapshotSet(
            vStreamProc_GetSnapshotSetIdxOfProcessingNode(processingNodeIdx)));
      }
    }
  } while ((retVal == VSTREAMPROC_OK) && (snapshotDataLength > 0u));

  if (retVal == VSTREAMPROC_OK)
  {
    /* #70 Set Session valid. */
    vStreamProc_SetValidOfSessionInfo(sessionId, TRUE);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreProcessingNodeSnapshot()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreProcessingNodeSnapshot(     /* PRQA S 6030 */ /* MD_MSR_STCYC */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_Session_State_SnapshotType snapshotFrameState = VSTREAMPROC_SESSION_STATE_SET_INPUTDATA;
  vStreamProc_SnapshotSlotIdType snapshotSlotId =
    vStreamProc_GetSnapshotSlotStartIdxOfSnapshotSet(
      vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId));
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) snapshotWorkspace =
    (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))vStreamProc_GetWorkspaceOfSnapshotSlot(snapshotSlotId);              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the processing node snapshot data. */
  do
  {
    switch (snapshotFrameState)
    {
      case VSTREAMPROC_SESSION_STATE_SET_INPUTDATA:
      {
        retVal = VSTREAMPROC_OK;

        /* #20 Check if the processing node has input ports.*/
        if (vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId) != VSTREAMPROC_NO_NAMEDINPUTPORT)
        {
          retVal =
            vStreamProc_Session_RestoreInputPortSnapshots(
              ProcessingNodeId,
              SnapshotData,
              SnapshotDataLength);
        }
        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_OUTPUTDATA:
      {
        retVal = VSTREAMPROC_OK;

        /* #30 Check if the processing node has input ports.*/
        if (vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId) != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
        {
          retVal =
            vStreamProc_Session_RestoreOutputPortSnapshots(
              ProcessingNodeId,
              SnapshotData,
              SnapshotDataLength);
        }
        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_WORKSPACEMARKER:
      {
        retVal = VSTREAMPROC_OK;

        if (snapshotWorkspace != NULL_PTR)
        {
          /* #40 Check if the workspace marker is valid. */
          if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_WORKSPACE, SnapshotData, SnapshotDataLength) == FALSE)
          {
            retVal = VSTREAMPROC_FAILED;
          }
        }
        break;
      }
      case VSTREAMPROC_SESSION_STATE_SET_WORKSPACE:
      {
        retVal = VSTREAMPROC_OK;

        if (snapshotWorkspace != NULL_PTR)
        {
          vStreamProc_LengthType workspaceLength = vStreamProc_GetWorkspaceSizeOfProcessingNodeClassDef(
            vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
              vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)));

          retVal = vStreamProc_Session_RestoreAndCheck(
            snapshotWorkspace,
            SnapshotData,
            workspaceLength,
            SnapshotDataLength);
        }
        break;
      }

      default:
      {
        /* This state shouldn't be reachable - force leaving loop with error. */
        retVal = VSTREAMPROC_FAILED;
        break;
      }
    };

    /* #50 Switch to next state. */
    snapshotFrameState++;                                                                                               /* PRQA S 4527 1 */ /* MD_vStreamProc_Rule10.1_4527_EnumInc */
  } while ((retVal == VSTREAMPROC_OK) && (snapshotFrameState < VSTREAMPROC_SESSION_STATE_SNAP_FRAMECOUNT));

  /* #60 Add the restored snapshot to the snapshot queue. */
  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_QueuePtrType queue =
      vStreamProc_GetAddrSnapshotSetQueue(
        vStreamProc_GetSnapshotSetQueueIdxOfSnapshotSet(
          vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId)));

    /* Hint: This will implicitly add the first snapshot entry to the queue, as the queue is empty in this context. */
    (void)vStreamProc_Queue_PrioInsert(queue, 0u);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreInputPortSnapshots()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreInputPortSnapshots(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SnapshotSlotIdType snapshotSlotId =
    vStreamProc_GetSnapshotSlotStartIdxOfSnapshotSet(
      vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId));
  vStreamProc_LengthType inputPortInfoLength = sizeof(vStreamProc_SnapshotInputPortInfoType);
  vStreamProc_SnapshotInputPortInfoIterType snapShotInputPortInfoIdx;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasSnapshotInputPortInfo())
  {
    /* #10 Iterate over all snapshot input ports. */
    for (snapShotInputPortInfoIdx = vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(snapshotSlotId);
         snapShotInputPortInfoIdx < vStreamProc_GetSnapshotInputPortInfoEndIdxOfSnapshotSlot(snapshotSlotId);
         snapShotInputPortInfoIdx++)
    {
      retVal = VSTREAMPROC_FAILED;

      /* #20 Check if the marker is valid. */
      if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_INPUTPORT, SnapshotData, SnapshotDataLength))
      {
        vStreamProc_SnapshotInputPortInfoPtrType snapshotInputPortInfoPtr =
          vStreamProc_GetAddrSnapshotInputPortInfo(snapShotInputPortInfoIdx);

        /* #30 Restore input port info. */
        retVal = vStreamProc_Session_RestoreAndCheck(
          (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))snapshotInputPortInfoPtr,
          SnapshotData,
          inputPortInfoLength,
          SnapshotDataLength);
      }

      if (retVal == VSTREAMPROC_FAILED)
      {
        break;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreOutputPortSnapshots()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreOutputPortSnapshots(        /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_Session_ConstDataPtr, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SnapshotDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SnapshotSlotIdType snapshotSlotId =
    vStreamProc_GetSnapshotSlotStartIdxOfSnapshotSet(
      vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId));
  vStreamProc_LengthType snapshotWaveInfoLength = sizeof(vStreamProc_SnapshotWaveInfoType);
  vStreamProc_SnapshotOutputPortIterType snapshotOuputPortInfoIdx;
  vStreamProc_WaveLevelDefIterType waveLeveDefStartIdx =
    vStreamProc_GetWaveLevelDefStartIdxOfPipe(
      vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId));
  vStreamProc_Session_ConstDataPtr snapshotData = *SnapshotData;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all snapshot output ports. */
  for (snapshotOuputPortInfoIdx = vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(snapshotSlotId);
       snapshotOuputPortInfoIdx < vStreamProc_GetSnapshotOutputPortEndIdxOfSnapshotSlot(snapshotSlotId);
       snapshotOuputPortInfoIdx++)
  {
    retVal = VSTREAMPROC_FAILED;

    /* #20 Check if the marker is valid. */
    if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_OUTPUTPORT, &snapshotData, SnapshotDataLength))
    {
      vStreamProc_WaveLevelDefIterType waveLevelDefIdx = waveLeveDefStartIdx;
      vStreamProc_SnapshotWaveIterType snapShotWaveIdx;

      /* #30 For each wave level. */
      for (snapShotWaveIdx = vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(snapshotOuputPortInfoIdx);
           snapShotWaveIdx < vStreamProc_GetSnapshotWaveEndIdxOfSnapshotOutputPort(snapshotOuputPortInfoIdx);
           snapShotWaveIdx++)
      {
        /* #40 Check if the marker is valid. */
        if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_WAVE, &snapshotData, SnapshotDataLength) == TRUE)
        {
          vStreamProc_StorageNodeBufferType metaDataBuffer =
            vStreamProc_GetMetaDataPointerOfSnapshotWave(snapShotWaveIdx);
          vStreamProc_SnapshotWaveInfoPtrType snapshotWaveInfoPtr =
            vStreamProc_GetAddrSnapshotWaveInfo(snapShotWaveIdx);

          /* #50 Set SnapshotWaveInfo content. */
          retVal = vStreamProc_Session_RestoreAndCheck(
            (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))snapshotWaveInfoPtr,
            &snapshotData,
            snapshotWaveInfoLength,
            SnapshotDataLength);

          /* #60 If meta data is available. */
          if ((retVal == VSTREAMPROC_OK) && (metaDataBuffer != NULL_PTR))
          {
            vStreamProc_WaveTypeDefIterType waveTypeDefIdx =
              vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(waveLevelDefIdx);

            retVal = VSTREAMPROC_FAILED;

            /* #70 Check if the meta data marker is valid. */
            if (vStreamProc_Session_IsMarkerValid(VSTREAMPROC_SESSION_SECTION_METADATA, &snapshotData, SnapshotDataLength) == TRUE)
            {
              vStreamProc_LengthType metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefIdx);

              /* #80 Restore the meta data. */
              retVal = vStreamProc_Session_RestoreAndCheck(
                (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))metaDataBuffer,
                &snapshotData,
                metaDataSize,
                SnapshotDataLength);

            }
          }
        }

        if (retVal == VSTREAMPROC_FAILED)
        {
          break;
        }

        waveLevelDefIdx++;
      }
    }

    if (retVal == VSTREAMPROC_FAILED)
    {
      break;
    }
  }

  *SnapshotData = snapshotData;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreRuntimeVariables(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_SessionEntryIterType sessionEntryStartIdx = vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
  vStreamProc_SessionEntryIterType sessionEntryEndIdx = vStreamProc_GetSessionEntryEndIdxOfSession(sessionId);
  vStreamProc_ProcessingNodeIdType processingNodeId = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
  vStreamProc_SessionEntryIterType sessionEntryIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all session entries of the session. */
  for (sessionEntryIdx = sessionEntryStartIdx; sessionEntryIdx < sessionEntryEndIdx; sessionEntryIdx++)
  {
    vStreamProc_SnapshotSlotIterType snapshotSlotIdx = vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryIdx);

    if (snapshotSlotIdx != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      vStreamProc_SnapshotInputPortInfoIterType snapshotInputPortInfoIdx;
      vStreamProc_SnapshotOutputPortIterType snapshotOutputPortIdx;
      vStreamProc_InputPortSymbolicNameType inputPortSymbolicName = 0u;
      vStreamProc_OutputPortSymbolicNameType outputPortSymbolicName = 0u;

      /* #20 Iterate over the input ports and restore the stream output port variables. */
      for (snapshotInputPortInfoIdx = vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(snapshotSlotIdx);
           snapshotInputPortInfoIdx < vStreamProc_GetSnapshotInputPortInfoEndIdxOfSnapshotSlot(snapshotSlotIdx);
           snapshotInputPortInfoIdx++)
      {
        vStreamProc_InputPortHandleType inputPortHandle;

        (void)vStreamProc_Stream_GetInputPortHandle(
          (vStreamProc_ProcessingNodeIdType)processingNodeId,
          inputPortSymbolicName,
          &inputPortHandle);

        if (inputPortHandle.IsConnected == TRUE)
        {
          vStreamProc_Session_RestoreStreamOutputPortRuntimeVariables(
            &inputPortHandle,
            (vStreamProc_SnapshotInputPortIdType)snapshotInputPortInfoIdx);
        }

        inputPortSymbolicName++;
      }

      /* #30 Iterate over the output ports and restore the stream runtime variables.*/
      for (snapshotOutputPortIdx = vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(snapshotSlotIdx);
           snapshotOutputPortIdx < vStreamProc_GetSnapshotOutputPortEndIdxOfSnapshotSlot(snapshotSlotIdx);
           snapshotOutputPortIdx++)
      {
        vStreamProc_OutputPortHandleType outputPortHandle;

        (void)vStreamProc_Stream_GetOutputPortHandle(
          (vStreamProc_ProcessingNodeIdType)processingNodeId,
          outputPortSymbolicName,
          &outputPortHandle);

        if (outputPortHandle.IsConnected == TRUE)
        {
          vStreamProc_Session_RestoreStreamRuntimeVariables(
            &outputPortHandle,
            (vStreamProc_SnapshotOutputPortIdType)snapshotOutputPortIdx);
        }

        outputPortSymbolicName++;
      }

      /* #40 Restore the workspace of the processing node */
      vStreamProc_Session_RestoreProcessingNodeWorkspace(
        (vStreamProc_ProcessingNodeIdType)processingNodeId,
        (vStreamProc_SnapshotSlotIdType)snapshotSlotIdx);
    }

    processingNodeId++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreStreamRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreStreamRuntimeVariables(
  P2CONST(vStreamProc_OutputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortHandle,
  vStreamProc_SnapshotOutputPortIdType SnapshotOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamIdType streamId = OutputPortHandle->StreamId;
  vStreamProc_WaveIterType waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamId);
  vStreamProc_NamedOutputPortIdType namedOutputPortId = OutputPortHandle->NamedOutputPortId;
  vStreamProc_SnapshotWaveIterType snapshotWaveIdx;
  vStreamProc_SnapshotIntervalOfPortDefType snapshotInterval =
    vStreamProc_GetSnapshotIntervalOfPortDef(
      vStreamProc_GetPortDefIdxOfNamedOutputPort(namedOutputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Restore the wave runtime variables. */
  for (snapshotWaveIdx = vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(SnapshotOutputPortId);
       snapshotWaveIdx < vStreamProc_GetSnapshotWaveEndIdxOfSnapshotOutputPort(SnapshotOutputPortId);
       snapshotWaveIdx++)
  {
    P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer = vStreamProc_GetMetaDataPointerOfWave(waveIdx);

    vStreamProc_SetHandleOfWaveVarInfo(waveIdx, vStreamProc_GetHandleOfSnapshotWaveInfo(snapshotWaveIdx));
    vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(
      waveIdx,
      vStreamProc_GetAbsolutePositionStartOfSnapshotWaveInfo(snapshotWaveIdx));
    vStreamProc_SetProducePositionOfWaveVarInfo(
      waveIdx,
      vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveIdx));
    vStreamProc_SetStateOfWaveVarInfo(waveIdx, vStreamProc_GetStateOfSnapshotWaveInfo(snapshotWaveIdx));

    /* #20 Restore the meta data from snapshot, if required. */
    if (metaDataBuffer != NULL_PTR)
    {
      vStreamProc_LengthIterType metaDataSize =
        vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(
          vStreamProc_GetWaveTypeDefIdxOfWave(waveIdx));
      P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) snapshotMetaDataBuffer =
        vStreamProc_GetMetaDataPointerOfSnapshotWave(snapshotWaveIdx);
      vStreamProc_LengthIterType metaDataIdx;

      for (metaDataIdx = 0u; metaDataIdx < metaDataSize; metaDataIdx++)
      {
        metaDataBuffer[metaDataIdx] = snapshotMetaDataBuffer[metaDataIdx];
      }
    }

    waveIdx++;
  }

  /* #30 Init the snapshot trigger position. */
  if (snapshotInterval != VSTREAMPROC_NO_SNAPSHOTINTERVALOFPORTDEF)
  {
    vStreamProc_LengthType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(OutputPortHandle->WaveId);
    vStreamProc_LengthType triggerPosition;

    /* Hint: Disable snapshot trigger, if new trigger position exceeds maximum stream length. */
    if (snapshotInterval <= (VSTREAMPROC_MAX_LENGTH - producePosition))
    {
      triggerPosition = producePosition + snapshotInterval;
    }
    else
    {
      triggerPosition = 0u;
    }

    vStreamProc_SetSnapshotTriggerPositionOfStreamInfo(
      streamId,
      triggerPosition);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreStreamOutputPortRuntimeVariables()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreStreamOutputPortRuntimeVariables(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle,
  vStreamProc_SnapshotInputPortIdType SnapshotInputPortInfoId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_SnapshotIntervalOfPortDefType snapshotInterval =
    vStreamProc_GetSnapshotIntervalOfPortDef(
      vStreamProc_GetPortDefIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(streamOutputPortId)));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Restore the stream output port variables. */
  vStreamProc_SetWaveHandleOfStreamOutputPortInfo(
    streamOutputPortId,
    vStreamProc_GetWaveHandleOfSnapshotInputPortInfo(SnapshotInputPortInfoId));

  vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(
    streamOutputPortId,
    vStreamProc_GetAbsolutePositionStartOfSnapshotInputPortInfo(SnapshotInputPortInfoId));

  vStreamProc_SetConsumePositionOfStreamOutputPortInfo(
    streamOutputPortId,
    vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(SnapshotInputPortInfoId));

  vStreamProc_SetWaveStateOfStreamOutputPortInfo(
    streamOutputPortId,
    vStreamProc_GetWaveStateOfSnapshotInputPortInfo(SnapshotInputPortInfoId));

  /* #20 Init the snapshot trigger position. */
  if (snapshotInterval != VSTREAMPROC_NO_SNAPSHOTINTERVALOFPORTDEF)
  {
    vStreamProc_LengthType consumePosition =
      vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);
    vStreamProc_LengthType triggerPosition;

    /* Hint: Disable snapshot trigger, if new trigger position exceeds maximum stream length. */
    if (snapshotInterval <= (VSTREAMPROC_MAX_LENGTH - consumePosition))
    {
      triggerPosition = consumePosition + snapshotInterval;
    }
    else
    {
      triggerPosition = 0u;
    }

    vStreamProc_SetSnapshotTriggerPositionOfStreamOutputPortInfo(
      streamOutputPortId,
      triggerPosition);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreProcessingNodeWorkspace()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreProcessingNodeWorkspace(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType workspaceSize =
    vStreamProc_GetWorkspaceSizeOfProcessingNodeClassDef(
      vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
        vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)));
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) snapshotWorkspaceData =
    (P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))vStreamProc_GetWorkspaceOfSnapshotSlot(SnapshotSlotId);           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) liveWorkspaceData =
    (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))vStreamProc_GetWorkspaceOfProcessingNode(ProcessingNodeId);         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  uint32_least dataIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Restore the workspace data. */
  for (dataIdx = 0; dataIdx < workspaceSize; dataIdx++)
  {
    liveWorkspaceData[dataIdx] = snapshotWorkspaceData[dataIdx];
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSignals()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSignals(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SessionEntryIterType sessionEntryIdx =
    vStreamProc_GetSessionEntryStartIdxOfSession(
      vStreamProc_GetSessionIdxOfPipe(PipeId));
  vStreamProc_ProcessingNodeIterType processingNodeIdx;
  vStreamProc_StreamIterType streamIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all processing nodes. */
  for (processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
       processingNodeIdx < vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
       processingNodeIdx++)
  {
    vStreamProc_InputPortSymbolicNameType inputPortIdx;

    /* #20 Issue the Resume signal if the processing node has a snapshot. */
    if (vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryIdx) != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      (void)vStreamProc_Scheduler_SetNodeSignal(
        (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
        vStreamProcConf_vStreamProcSignal_vStreamProc_Resume);
    }

    /* #30 Iterate over the input ports. */
    for (inputPortIdx = 0u;
         inputPortIdx < vStreamProc_GetNamedInputPortLengthOfProcessingNode(processingNodeIdx);
         inputPortIdx++)
    {
      vStreamProc_InputPortHandleType inputPortHandle;

      (void)vStreamProc_Stream_GetInputPortHandle(
        (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
        inputPortIdx,
        &inputPortHandle);

      /* #40 Check if the input port is connected. */
      if (inputPortHandle.IsConnected == TRUE)
      {
        /* #50 Restore the input port signals */
        vStreamProc_Session_RestoreInputPortSignals(&inputPortHandle);
      }
    }

    sessionEntryIdx++;
  }

  /*#60 Iterate over all streams. */
  for (streamIdx = vStreamProc_GetStreamStartIdxOfPipe(PipeId);
       streamIdx < vStreamProc_GetStreamEndIdxOfPipe(PipeId);
       streamIdx++)
  {
    vStreamProc_WaveIterType waveIdx;

    /*#70 Iterate over all waves of the stream. */
    for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamIdx);
         waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamIdx);
         waveIdx++)
    {
      vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

      /* #80 Update the wave states based on the pending wave start/end count. */
      if (vStreamProc_GetPendingWaveStartCountOfWaveVarInfo(waveIdx) > 0u)
      {
        vStreamProc_SetWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING);
      }
      else
      {
        vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING);
      }

      if (vStreamProc_GetPendingWaveEndCountOfWaveVarInfo(waveIdx) > 0u)
      {
        vStreamProc_SetWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
      }
      else
      {
        vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
      }

      vStreamProc_SetStateOfWaveVarInfo(waveIdx, waveState);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreInputPortSignals()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RestoreInputPortSignals(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
  vStreamProc_NamedInputPortIdType namedInputPortId = InputPortHandle->NamedInputPortId;
  vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(waveId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Rebuild the consumer wave state if necessary. */
  /* Hint: This is only necessary if there is a producer snapshot but no consumer snapshot, which is only possible if
           the producer snapshot has started the first wave but did not produce any data yet.
           In this case, the consumer wave state and handle has to be rebuild based on the producer. */
  vStreamProc_Session_RebuildConsumerWave(InputPortHandle);

  /* #20 Check if the wave handles are equal. */
  /* Hint: If the producer wave handle is lower, the gap has to be filled first by reproducing the data,
           which is out of scope here. */
  if (producerWaveHandle == vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId))
  {
    vStreamProc_LengthType consumePosition =
      vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);
    vStreamProc_LengthType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);
    vStreamProc_WaveStateType consumerWaveState =
      vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId);
    vStreamProc_WaveStateType producerWaveState = vStreamProc_GetStateOfWaveVarInfo(waveId);

    /* #30 Check if WaveStart has to be set. */
    if (vStreamProc_IsWaveStateFlagSet(consumerWaveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING))
    {
      vStreamProc_SnapshotIntervalOfPortDefType snapshotInterval =
        vStreamProc_GetSnapshotIntervalOfPortDef(
          vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortId));

      /* #40 WaveStart not yet acknowledged, issue WaveStart again. */
      (void)vStreamProc_Scheduler_SetInputPortSignal(
        namedInputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart);

      /* #50 Trigger snapshot if snapshot interval is defined. */
      if (snapshotInterval != VSTREAMPROC_NO_SNAPSHOTINTERVALOFPORTDEF)
      {
        /* #60 If true, set the snapshot signal at the input port. */
        if (vStreamProc_Scheduler_SetInputPortSignal(
          namedInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_InputSnapshotRequested) == E_NOT_OK)
        {
          /* #70 If there is no signal handler or the signal handler is unregistered,
                 start snapshot creation directly. */
          vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(InputPortHandle->ProcessingNodeId, namedInputPortId);
        }
      }

      /* #80 Set the STARTING flag in producer wave state. */
      vStreamProc_SetWaveStateFlag(producerWaveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING);
      vStreamProc_IncPendingWaveStartCountOfWaveVarInfo(waveId);
    }
    /* #90 Otherwise: */
    else
    {
      /* #100 Check if WaveResume has to be set. */
      if ( (consumerWaveState == VSTREAMPROC_WAVESTATE_ACTIVE)
        || (consumerWaveState == VSTREAMPROC_WAVESTATE_ENDING))
      {
        /* #110 WaveStart already acknowledged, issue WaveResume. */
        (void)vStreamProc_Scheduler_SetInputPortSignal(
          namedInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveResume);
      }
    }

    /* #120 Check if WaveEnd has to be set */
    /* Hint: If the WaveEnd has to be set depends mainly on the producer wave state. If the producer snapshot
             wave state reflects that the WaveEnd was already propagated, the WaveEnd has to be set for consumer,
             unless the consumer already acknowledged it and is IDLE now. */
    if ((vStreamProc_IsWaveStateFlagSet(producerWaveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING)
          || (producerWaveState == VSTREAMPROC_WAVESTATE_IDLE))
        && (consumerWaveState != VSTREAMPROC_WAVESTATE_IDLE))
    {
      /* #130 Issue WaveEnd. */
      (void)vStreamProc_Scheduler_SetInputPortSignal(
        namedInputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd);

      /*#140 Set ENDING and clear ACTIVE flag in consumer wave state. */
      vStreamProc_SetWaveStateFlag(consumerWaveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
      vStreamProc_ClrWaveStateFlag(consumerWaveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);
      vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, consumerWaveState);

      /* Update the pending wave end count. */
      vStreamProc_IncPendingWaveEndCountOfWaveVarInfo(waveId);
    }

    /* #150 Check if the ENDING flag has to be cleared on consumer side. */
    /* Hint: The ending flag has to be cleared if the WaveEnd was not propagated in the producer snapshot yet.
             The WaveEnd will be set during regular processing in this case. */
    if (vStreamProc_IsWaveStateFlagSet(consumerWaveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING)
      && ((producerWaveState == VSTREAMPROC_WAVESTATE_STARTING_ACTIVE)
        || (producerWaveState == VSTREAMPROC_WAVESTATE_ACTIVE)))
    {
      vStreamProc_SetWaveStateFlagOfStreamOutputPortId(
        streamOutputPortId,
        VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);

      vStreamProc_ClrWaveStateFlagOfStreamOutputPortId(
        streamOutputPortId,
        VSTREAMPROC_WAVESTATE_FLAG_ENDING);
    }

    /* #160 Check if the produce position is higher than the consume position (only possible for virtual ports). */
    if (producePosition > consumePosition)
    {
      /* #170 Issue DataAvailable. */
      (void)vStreamProc_Scheduler_SetInputPortSignal(
        namedInputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RebuildConsumerWave()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_RebuildConsumerWave(
  P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
  vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(waveId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the producer wave handle is higher than the consumer wave handle. */
  if (producerWaveHandle > vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId))
  {
    vStreamProc_WaveStateType producerWaveState = vStreamProc_GetStateOfWaveVarInfo(waveId);

    /* #20 Analyze the producer wave state and derive the consumer wave state. */
    switch (producerWaveState)
    {
      case VSTREAMPROC_WAVESTATE_STARTING:
      {
        /* Hint: Wave was started but not propagated. Only possible if wave propagation was not possible
                 -> Set wave blocked, no need to update consumer wave state and handle */
        vStreamProc_SetWaveBlockedOfStreamInfo(vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId), TRUE);
        break;
      }
      case VSTREAMPROC_WAVESTATE_STARTING_ACTIVE:
      case VSTREAMPROC_WAVESTATE_ACTIVE:
      {
        /* Producer started and propagated the wave, set consumer to STARTING_ACTIVE */
        /* Hint: The STARTING flag will be added to the producer wave state in the next step if necessary. */
        vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, VSTREAMPROC_WAVESTATE_STARTING_ACTIVE);
        vStreamProc_SetWaveHandleOfStreamOutputPortInfo(streamOutputPortId, producerWaveHandle);
        vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(
          streamOutputPortId,
          vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId));
        break;
      }
      case VSTREAMPROC_WAVESTATE_STARTING_ENDING:
      case VSTREAMPROC_WAVESTATE_ENDING:
      case VSTREAMPROC_WAVESTATE_IDLE:
      {
        /* Producer started, propagated and ended the wave, set consumer to STARTING_ENDING. */
        /* Hint: The STARTING/ENDING flag will be added to the producer wave state in the next step if necessary. */
        vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, VSTREAMPROC_WAVESTATE_STARTING_ENDING);
        vStreamProc_SetWaveHandleOfStreamOutputPortInfo(streamOutputPortId, producerWaveHandle);
        vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(
          streamOutputPortId,
          vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId));
        break;
      }
      default:
      {
        /* Should never occur. */
        break;
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Session_TriggerPostRestoreActions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_TriggerPostRestoreActions(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_EntryPointIterType entryPointIdx;
  vStreamProc_ExitPointIterType exitPointIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Reset pending input streams. */
  for (entryPointIdx = vStreamProc_GetEntryPointStartIdxOfPipe(PipeId);
       entryPointIdx < vStreamProc_GetEntryPointEndIdxOfPipe(PipeId);
       entryPointIdx++)
  {
    vStreamProc_AccessNode_ResetPendingInputStream((vStreamProc_EntryPointIdType)entryPointIdx, TRUE);
  }

  /* #20 Reset pending output streams. */
  for (exitPointIdx = vStreamProc_GetExitPointStartIdxOfPipe(PipeId);
       exitPointIdx < vStreamProc_GetExitPointEndIdxOfPipe(PipeId);
       exitPointIdx++)
  {
    vStreamProc_AccessNode_ResetPendingOutputStream((vStreamProc_EntryPointIdType)exitPointIdx, TRUE);
  }
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Session_InitSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_InitSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SessionIterType sessionIdx = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_SessionEntryIterType sessionEntryEndIdx = vStreamProc_GetSessionEntryEndIdxOfSession(sessionIdx);
  vStreamProc_SessionEntryIterType sessionEntryIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* Hint: The session queue are initialized every time CreateSession() is called. */

  /* #10 Init the session entries. */
  for (sessionEntryIdx = 0u; sessionEntryIdx < sessionEntryEndIdx; sessionEntryIdx++)
  {
    vStreamProc_SetSnapshotSlotIdxOfSessionEntry(
      sessionEntryIdx,
      (vStreamProc_SnapshotSlotIdxOfSessionEntryType)VSTREAMPROC_NO_SNAPSHOTSLOT);
  }

  /* #20 Set Session invalid. */
  vStreamProc_SetValidOfSessionInfo(sessionIdx, FALSE);
}

 /*********************************************************************************************************************
 *  vStreamProc_Session_CreateSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CreateSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare the session */
  vStreamProc_Session_PrepareSession(PipeId);

  /* #20 Create snapshots for entry nodes. */
  vStreamProc_Snapshot_CreateEntryNodeSnapshots(PipeId);

  /* #30 Create snapshots for exit nodes. */
  vStreamProc_Snapshot_CreateExitNodeSnapshots(PipeId);

  /* #40 Tag the latest snapshots of the processing nodes.*/
  vStreamProc_Session_PrepareSessionEntries(PipeId);

  /* #50 Process the session queue. */
  vStreamProc_Session_ProcessSessionQueue(PipeId);

  /* #60 Check validity */
  retVal = vStreamProc_Session_CheckSessionValidity(PipeId);

  /* #70 Check if the session is valid and remove obsolete snapshots if true. */
  if (vStreamProc_IsValidOfSessionInfo(sessionId))
  {
    vStreamProc_SessionEntryIterType sessionEntryIdx;
    vStreamProc_ProcessingNodeIterType processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);

    /* #80 Iterate over the session entries.*/
    for (sessionEntryIdx = vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
         sessionEntryIdx < vStreamProc_GetSessionEntryEndIdxOfSession(sessionId);
         sessionEntryIdx++)
    {
      vStreamProc_SnapshotSlotIterType snapshotSlotIdx = vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryIdx);

      /* #90 Remove the obsolete snapshots of the processing node if a snapshot is referenced in session. */
      if (snapshotSlotIdx != VSTREAMPROC_NO_SNAPSHOTSLOT)
      {
        vStreamProc_Snapshot_RemoveObsoleteSnapshots(
          (vStreamProc_ProcessingNodeIdType)processingNodeIdx,
          (vStreamProc_SnapshotSlotIdType)snapshotSlotIdx);
      }
      processingNodeIdx++;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetEntryPointInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetEntryPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo,
  vStreamProc_ReadRequestPtrType MetaData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SessionIterType sessionIdx = vStreamProc_GetSessionIdxOfPipe(PipeId);

  /* Output Data */
  vStreamProc_StreamPositionType absolutePositionStart = 0u;
  vStreamProc_StreamPositionType relativePosition = 0u;

  vStreamProc_DataTypeIdType metaDataTypeId = VSTREAMPROC_NO_DATATYPE;
  vStreamProc_LengthType metaDataSize = 0u;
  vStreamProc_LengthType metaDataAvailableLength = 0u;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer = NULL_PTR;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if session is valid */
  if (vStreamProc_IsValidOfSessionInfo(sessionIdx))
  {
    vStreamProc_ProcessingNodeIdType processingNodeId =
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId));

    vStreamProc_SnapshotSlotIdType entryNodeSnapshotSlotId =
      vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
        vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(processingNodeId));

    retVal = VSTREAMPROC_INSUFFICIENT_INPUT;

    if (entryNodeSnapshotSlotId != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      vStreamProc_SnapshotWaveIdType snapshotWaveId =
        vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(
          vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(entryNodeSnapshotSlotId));
      vStreamProc_WaveTypeDefIdType waveTypeDefId =
        vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
          vStreamProc_GetWaveLevelDefStartIdxOfPipe(PipeId));

      /* #20 Set info data */
      absolutePositionStart = vStreamProc_GetAbsolutePositionStartOfSnapshotWaveInfo(snapshotWaveId);
      relativePosition = vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveId);
      metaDataTypeId = vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(waveTypeDefId);

      /* #30 Check if meta data is available. */
      if (metaDataTypeId != VSTREAMPROC_NO_DATATYPE)
      {
        metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefId);
        metaDataBuffer = vStreamProc_GetMetaDataPointerOfSnapshotWave(snapshotWaveId);
        metaDataAvailableLength = 1u;
      }

      retVal = VSTREAMPROC_OK;
    }
  }

  /* #40 Write the stream position info. */
  StreamPositionInfo->AbsolutePositionStart = absolutePositionStart;
  StreamPositionInfo->RelativePosition = relativePosition;
  StreamPositionInfo->TotalLength = (vStreamProc_LengthType)relativePosition;

  /* #50 Write the meta data . */
  MetaData->Buffer = metaDataBuffer;
  MetaData->StorageInfo.DataTypeInfo.Id = metaDataTypeId;
  MetaData->StorageInfo.DataTypeInfo.Size = metaDataSize;
  MetaData->StorageInfo.AvailableLength = metaDataAvailableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetExitPointInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetExitPointInfoOfSession(                           /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointId,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo,
  vStreamProc_ReadRequestPtrType MetaData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SessionIterType sessionIdx = vStreamProc_GetSessionIdxOfPipe(PipeId);

  /* Output Data */
  vStreamProc_StreamPositionType absolutePositionStart = 0u;
  vStreamProc_StreamPositionType consumePosition = 0u;
  vStreamProc_LengthType totalLength = 0u;

  vStreamProc_DataTypeIdType metaDataTypeId = VSTREAMPROC_NO_DATATYPE;
  vStreamProc_LengthType metaDataSize = 0u;
  vStreamProc_LengthType metaDataAvailableLength = 0u;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer = NULL_PTR;

  /* ----- Implementation ----------------------------------------------- */
  /*#10 Check if session is valid */
  if (vStreamProc_IsValidOfSessionInfo(sessionIdx))
  {
    vStreamProc_NamedInputPortIdType namedInputPortId = vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId);
    vStreamProc_InputPortHandleType inputPortHandle;

    retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;

    /* #20 Check if the exit node is connected. */
    (void)vStreamProc_Stream_GetInputPortHandle(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortId),
      vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
      &inputPortHandle);

    if (inputPortHandle.IsConnected == TRUE)
    {
      vStreamProc_SnapshotInputPortIdType snapshotInputPortId =
        vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(
          vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
            vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(inputPortHandle.ProcessingNodeId)));

      /* #30 Check if the exit node snapshot already received a wave. */
      if (vStreamProc_GetWaveStateOfSnapshotInputPortInfo(snapshotInputPortId) != VSTREAMPROC_WAVESTATE_UNINIT)
      {
        vStreamProc_SnapshotWaveIdType producerSnapshotWaveId =
          vStreamProc_Session_GetProducerSnapshotWaveStartId(inputPortHandle.NamedInputPortId);

        absolutePositionStart = vStreamProc_GetAbsolutePositionStartOfSnapshotInputPortInfo(snapshotInputPortId);
        consumePosition = vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(snapshotInputPortId);
        totalLength = consumePosition;

        if ((inputPortHandle.HasActiveProducer == TRUE) && (producerSnapshotWaveId != VSTREAMPROC_NO_SNAPSHOTWAVE))
        {
          vStreamProc_WaveLevelType waveLevelDef = vStreamProc_GetWaveLevelDefStartIdxOfPipe(PipeId);
          vStreamProc_WaveTypeDefIdType waveTypeDefId = vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(waveLevelDef);
          vStreamProc_StreamPositionType producePosition =
            vStreamProc_GetProducePositionOfSnapshotWaveInfo(producerSnapshotWaveId);


          /* #40 Check if the total length needs to be updated. */
          /* Hint: If the produce position is lower, the consume position is reported as the gap will be reproduced
                   and discarded after restore. */
          if (producePosition > consumePosition)
          {
            totalLength = (vStreamProc_LengthType)producePosition;
          }

          /* #50 Check if there is meta data to provide. */
          metaDataTypeId = vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(waveTypeDefId);

          if (metaDataTypeId != VSTREAMPROC_NO_DATATYPE)
          {
            /* #60 Provide the meta data. */
            metaDataBuffer = vStreamProc_GetMetaDataPointerOfSnapshotWave(producerSnapshotWaveId);
            metaDataAvailableLength = 1u;
            metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefId);
          }

          retVal = VSTREAMPROC_OK;
        }
      }
    }
  }

  /* #70 Write the stream position info. */
  StreamPositionInfo->AbsolutePositionStart = absolutePositionStart;
  StreamPositionInfo->RelativePosition = consumePosition;
  StreamPositionInfo->TotalLength = totalLength;

  /* #80 Write the meta data . */
  MetaData->StorageInfo.DataTypeInfo.Id = metaDataTypeId;
  MetaData->StorageInfo.DataTypeInfo.Size = metaDataSize;
  MetaData->StorageInfo.AvailableLength = metaDataAvailableLength;
  MetaData->Buffer = metaDataBuffer;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetEntryPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetEntryPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);

  /* Output Data */
  vStreamProc_WaveHandleType waveHandle = 0u;
  vStreamProc_WaveStateType waveState = VSTREAMPROC_WAVESTATE_UNINIT;
  vStreamProc_StreamPositionType absoluteStreamPositionStart = 0u;
  vStreamProc_StreamPositionType relativeStreamPosition = 0u;

  vStreamProc_DataTypeIdType metaDataTypeId = VSTREAMPROC_NO_DATATYPE;
  vStreamProc_LengthType metaDataSize = 0u;
  vStreamProc_LengthType metaDataAvailableLength = 0u;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer = NULL_PTR;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if session is valid. */
  if (vStreamProc_IsValidOfSessionInfo(sessionId))
  {
    vStreamProc_ProcessingNodeIdType processingNodeId =
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId));
    vStreamProc_SnapshotSlotIdType entryNodeSnapshotSlotId =
      vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
        vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(processingNodeId));

    retVal = VSTREAMPROC_INSUFFICIENT_INPUT;

    if (entryNodeSnapshotSlotId != VSTREAMPROC_NO_SNAPSHOTSLOT)
    {
      vStreamProc_WaveTypeDefIdType waveTypeDefId = WaveInfo->WaveTypeSymbolicName;
      vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, waveTypeDefId);
      vStreamProc_SnapshotWaveIdType snapshotWaveId =
        vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(
          vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(entryNodeSnapshotSlotId))
        + (vStreamProc_SnapshotWaveIdType)waveLevel;

      /* #20 Provide wave data and stream positions.*/
      waveHandle = vStreamProc_GetHandleOfSnapshotWaveInfo(snapshotWaveId);
      waveState = vStreamProc_GetStateOfSnapshotWaveInfo(snapshotWaveId);
      absoluteStreamPositionStart = vStreamProc_GetAbsolutePositionStartOfSnapshotWaveInfo(snapshotWaveId);
      relativeStreamPosition = vStreamProc_GetProducePositionOfSnapshotWaveInfo(snapshotWaveId);

      /* #30 Provide the meta data. */
      metaDataTypeId = vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(waveTypeDefId);

      if (metaDataTypeId != VSTREAMPROC_NO_DATATYPE)
      {
        metaDataBuffer = vStreamProc_GetMetaDataPointerOfSnapshotWave(snapshotWaveId);
        metaDataAvailableLength = 1u;
        metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefId);
      }

      retVal = VSTREAMPROC_OK;
    }
  }

  /* #40 Write the wave info. */
  WaveInfo->Handle = waveHandle;
  WaveInfo->State = waveState;
  WaveInfo->AbsoluteStreamPosition = absoluteStreamPositionStart;
  WaveInfo->RelativeStreamPosition = relativeStreamPosition;
  WaveInfo->MetaData.Buffer = metaDataBuffer;
  WaveInfo->MetaData.StorageInfo.DataTypeInfo.Id = metaDataTypeId;
  WaveInfo->MetaData.StorageInfo.DataTypeInfo.Size = metaDataSize;
  WaveInfo->MetaData.StorageInfo.AvailableLength = metaDataAvailableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetExitPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetExitPointWaveInfoOfSession(                       /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType ExitPointId,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);

  /* Output Data */
  vStreamProc_WaveHandleType waveHandle = 0u;
  vStreamProc_WaveStateType waveState = VSTREAMPROC_WAVESTATE_UNINIT;
  vStreamProc_StreamPositionType absoluteStreamPositionStart = 0u;
  vStreamProc_StreamPositionType relativeStreamPosition = 0u;

  vStreamProc_DataTypeIdType metaDataTypeId = VSTREAMPROC_NO_DATATYPE;
  vStreamProc_LengthType metaDataSize = 0u;
  vStreamProc_LengthType metaDataAvailableLength = 0u;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaDataBuffer = NULL_PTR;

  /* ----- Implementation ----------------------------------------------- */
  /*#10 Check if session is valid */
  if (vStreamProc_IsValidOfSessionInfo(sessionId))
  {
    vStreamProc_NamedInputPortIdType namedInputPortId = vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId);
    vStreamProc_InputPortHandleType inputPortHandle;

    retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;

    /* #20 Check if the exit node is connected. */
    (void)vStreamProc_Stream_GetInputPortHandle(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortId),
      vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
      &inputPortHandle);

    if ((inputPortHandle.IsConnected == TRUE)  && (inputPortHandle.HasActiveProducer == TRUE))
    {
      vStreamProc_SnapshotSlotIdType exitNodeSnapshotSlotId =
        vStreamProc_GetSnapshotSlotIdxOfSessionEntry(
          vStreamProc_Session_GetSessionEntryIdByProcessingNodeId(inputPortHandle.ProcessingNodeId));
      vStreamProc_SnapshotInputPortIdType exitNodeSnapshotInputPortId =
        vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(exitNodeSnapshotSlotId);

      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;

      /* #30 Check if the exit node snapshot already received a wave. */
      if (vStreamProc_GetWaveStateOfSnapshotInputPortInfo(exitNodeSnapshotInputPortId) != VSTREAMPROC_WAVESTATE_UNINIT)
      {
        vStreamProc_WaveTypeDefIdType waveTypeDefId = WaveInfo->WaveTypeSymbolicName;
        vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, waveTypeDefId);
        vStreamProc_SnapshotWaveIdType producerSnapshotWaveStartId =
          vStreamProc_Session_GetProducerSnapshotWaveStartId(inputPortHandle.NamedInputPortId);

        /* #40 Check if a producer snapshot is set in session. */
        if (producerSnapshotWaveStartId != VSTREAMPROC_NO_SNAPSHOTWAVE)
        {
          vStreamProc_SnapshotWaveIdType producerSnapshotWaveId =
            producerSnapshotWaveStartId + (vStreamProc_SnapshotWaveIdType)waveLevel;
          /* Hint: Consume Position Offset is always calculated on wave level 0, as the exit node always operates on
                   wave level 0. There is no need to consider the wave handle as the exit node snapshot is always
                   patched to the wave handle of the producer during CreateSession() if necessary. */
          vStreamProc_StreamPositionType consumePositionWaveLevel0 =
            vStreamProc_GetConsumePositionOfSnapshotInputPortInfo(exitNodeSnapshotInputPortId);
          vStreamProc_StreamPositionType producerPositionWaveLevel0 =
            vStreamProc_GetProducePositionOfSnapshotWaveInfo(producerSnapshotWaveStartId);
          vStreamProc_StreamPositionType positionOffset;

          /* #50 Set the wave data and stream positions. */
          waveHandle = vStreamProc_GetHandleOfSnapshotWaveInfo(producerSnapshotWaveId);
          waveState = vStreamProc_GetStateOfSnapshotWaveInfo(producerSnapshotWaveId);
          absoluteStreamPositionStart = vStreamProc_GetAbsolutePositionStartOfSnapshotWaveInfo(producerSnapshotWaveId);

          /* #60 Set the consume position offset. */
          if (consumePositionWaveLevel0 >= producerPositionWaveLevel0)
          {
            /* Hint: For non-virtual ports, the consume position is never lower than the produce position.
                     The offset describes the already consumed data which is not contained in the producer snapshot. */
            positionOffset = consumePositionWaveLevel0 - producerPositionWaveLevel0;
            relativeStreamPosition = vStreamProc_GetProducePositionOfSnapshotWaveInfo(producerSnapshotWaveId) + positionOffset;
          }
          else
          {
            /* Hint: This is only possible for virtual ports. The offset describes the not yet consumed (virtual) data. */
            positionOffset = producerPositionWaveLevel0 - consumePositionWaveLevel0;
            relativeStreamPosition = vStreamProc_GetProducePositionOfSnapshotWaveInfo(producerSnapshotWaveId) - positionOffset;
          }

          /* #70 Check if there is meta data to provide. */
          metaDataTypeId = vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(waveTypeDefId);

          if (metaDataTypeId != VSTREAMPROC_NO_DATATYPE)
          {
            /* #80 Provide the meta data. */
            metaDataBuffer = vStreamProc_GetMetaDataPointerOfSnapshotWave(producerSnapshotWaveId);
            metaDataAvailableLength = 1u;
            metaDataSize = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefId);
          }

          retVal = VSTREAMPROC_OK;
        }
        /* #90 Otherwise: Report insufficient input. */
        else
        {
          retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
        }
      }
    }
  }

  /* #100 Write the wave info. */
  WaveInfo->Handle = waveHandle;
  WaveInfo->State = waveState;
  WaveInfo->AbsoluteStreamPosition = absoluteStreamPositionStart;
  WaveInfo->RelativeStreamPosition = relativeStreamPosition;
  WaveInfo->MetaData.StorageInfo.DataTypeInfo.Id = metaDataTypeId;
  WaveInfo->MetaData.StorageInfo.DataTypeInfo.Size = metaDataSize;
  WaveInfo->MetaData.StorageInfo.AvailableLength = metaDataAvailableLength;
  WaveInfo->MetaData.Buffer = metaDataBuffer;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetMaxDataSizeOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_Session_GetMaxDataSizeOfSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SessionIdType sessionId = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_LengthType length = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Read max size from configuration. */
  length = vStreamProc_GetMaxSizeOfSession(sessionId);

  return length;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_GetDataOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetDataOfSession(                                    /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC , MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType    retVal          = VSTREAMPROC_FAILED;
  vStreamProc_SessionIdType sessionId       = vStreamProc_GetSessionIdxOfPipe(PipeId);
  vStreamProc_LengthType    processedLength = 0u;

  CONST(uint8, VSTREAMPROC_CONST) vStreamProc_Session_SessionDataFormat_Version[] = {
    VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MAJOR,
    VSTREAMPROC_SESSION_DATA_FORMAT_VERSION_MINOR
  };

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if session is valid. */
  if (vStreamProc_IsValidOfSessionInfo(sessionId))
  {
    uint8                                   snapCount             = 0u;
    uint8                                   *sessionData          = SessionData;
    vStreamProc_LengthType                  availableLength       = *Length;
    vStreamProc_LengthType                  totalLengthPosition   = 0u;
    vStreamProc_LengthType                  snapshotCountPosition = 0u;
    vStreamProc_Session_State_DataFrameType state                 = VSTREAMPROC_SESSION_STATE_SET_STARTMARKER;

    retVal = VSTREAMPROC_OK;

    do
    {
      P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) sourceDataPtr = NULL_PTR;
      vStreamProc_LengthType sourceLength = 0u;

      /* #20 Evaluate state. */
      switch (state)
      {
        case VSTREAMPROC_SESSION_STATE_SET_STARTMARKER:
        {
          sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_PIPE_START];
          sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_VERSION:
        {
          sourceDataPtr = vStreamProc_Session_SessionDataFormat_Version;
          sourceLength  = sizeof(vStreamProc_Session_SessionDataFormat_Version);
          break;
        }
        case VSTREAMPROC_SESSION_STATE_RESERVE_LENGTH:
        {
          totalLengthPosition = *Length - availableLength;
          sourceDataPtr = (vStreamProc_Session_DataPtr)&processedLength;
          sourceLength  = sizeof(processedLength);
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_UUID:
        {
          sourceDataPtr = (P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA))vStreamProc_GetAddrUuidOfPipe(PipeId);
          sourceLength  = sizeof(vStreamProc_UuidOfPipeType);
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_STATEMARKER:
        {
          sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_BASESTATE];
          sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_BASESTATES:
        {
          vStreamProc_LengthType baseStateCount = (vStreamProc_LengthType)vStreamProc_GetBaseStateEndIdxOfPipe(PipeId) -
            (vStreamProc_LengthType)vStreamProc_GetBaseStateStartIdxOfPipe(PipeId);

          if (availableLength < baseStateCount)
          {
            retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
          }
          else
          {
            vStreamProc_BaseStateIterType baseIdx;

            for (baseIdx = 0u; baseIdx < baseStateCount; baseIdx++)
            {
              if (vStreamProc_IsActiveOfBaseStateInfo(baseIdx + vStreamProc_GetBaseStateStartIdxOfPipe(PipeId)))
              {
                sessionData[baseIdx] = 0x01u;
              }
              else
              {
                sessionData[baseIdx] = 0x00u;
              }
            }

            sessionData = &sessionData[baseIdx];
            availableLength -= (vStreamProc_LengthType)baseIdx;
          }
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTMARKER:
        {
          sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_SNAPSHOT];
          sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
          break;
        }
        case VSTREAMPROC_SESSION_STATE_RESERVE_SNAPSHOTCOUNT:
        {
          snapshotCountPosition = *Length - availableLength;
          sourceDataPtr = &snapCount;
          sourceLength  = sizeof(snapCount);
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTDATA:
        {
          vStreamProc_SessionEntryIterType sessionEntryIdx;

          /* #30 For all entries in current session. */
          for (sessionEntryIdx = vStreamProc_GetSessionEntryStartIdxOfSession(sessionId);
               sessionEntryIdx < vStreamProc_GetSessionEntryEndIdxOfSession(sessionId);
               sessionEntryIdx++)
          {
            vStreamProc_SnapshotSlotIterType snapShotSlotIdx =
              vStreamProc_GetSnapshotSlotIdxOfSessionEntry(sessionEntryIdx);

            if (snapShotSlotIdx != VSTREAMPROC_NO_SNAPSHOTSLOT)
            {
              retVal =
                vStreamProc_Session_GetSnapshotDataOfSession(&sessionData, &availableLength, PipeId, sessionEntryIdx);

              if (retVal == VSTREAMPROC_OK)
              {
                snapCount++;
              }
              else
              {
                break;
              }
            }
          }

          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_ENDMARKER:
        {
          sourceDataPtr = vStreamProc_Session_SectionMarkers[VSTREAMPROC_SESSION_SECTION_PIPE_END];
          sourceLength  = VSTREAMPROC_SESSION_SECTION_MARKER_SIZE;
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_SNAPSHOTCOUNT:
        {
          sourceDataPtr = &snapCount;
          sessionData   = &SessionData[snapshotCountPosition];
          sourceLength  = sizeof(snapCount);
          availableLength += sizeof(snapCount); /* Length was already taken into account. */
          break;
        }
        case VSTREAMPROC_SESSION_STATE_SET_LENGTH:
        {
          processedLength  = *Length - availableLength;
          sourceDataPtr    = (vStreamProc_Session_DataPtr)&processedLength;
          sessionData      = &SessionData[totalLengthPosition];
          sourceLength     = sizeof(processedLength);
          availableLength += sizeof(processedLength); /* Length was already taken into account. */
          break;
        }
        default:
        {
          /* This state shouldn't be reachable - force leaving loop with error. */
          retVal = VSTREAMPROC_FAILED;
          break;
        }
      }

      /* #40 Serialize data if necessary in this step. */
      if (sourceLength > 0u)
      {
        retVal = vStreamProc_Session_CopyAndCheck(&sessionData, sourceDataPtr, sourceLength, &availableLength);
      }

      /* #50 Switch to next state. */
      state++;                                                                                                          /* PRQA S 4527 1 */ /* MD_vStreamProc_Rule10.1_4527_EnumInc */
    }
    while ((retVal == VSTREAMPROC_OK) && (state < VSTREAMPROC_SESSION_STATE_FRAMECOUNT));
  }

  /* #60 Set output length. */
  *Length = processedLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSession(
  vStreamProc_PipeIdType PipeId,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  vStreamProc_LengthType SessionDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType    retVal = VSTREAMPROC_FAILED;
  vStreamProc_SessionHeaderDataType sessionHeaderData;
  vStreamProc_LengthType sessionDataLength = SessionDataLength;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Evaluate header. */
  retVal = vStreamProc_Session_EvaluateSessionHeader(PipeId, SessionData, sessionDataLength, &sessionHeaderData);

  if ((retVal == VSTREAMPROC_OK) && (sessionHeaderData.SnapshotCount > 0u))
  {
    /* #20 Restore snapshots. */
    retVal = vStreamProc_Session_RestoreSnapshots(
      PipeId,
      &sessionHeaderData);
  }

  /* #30 Restore base states if mode handling is used in the pipe. */
  if ((retVal == VSTREAMPROC_OK) && (sessionHeaderData.BaseStateCount > 0u))
  {
    retVal = vStreamProc_Session_RestoreBaseStates(PipeId, &sessionHeaderData);
  }

  /* #40 Restore the runtime variables based on the snapshot data. */
  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_Session_RestoreRuntimeVariables(PipeId);
  }

  /* #50 Restore pending signals. */
  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_Session_RestoreSignals(PipeId);
  }

  /* #60 Finalize restore. */
  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_Session_TriggerPostRestoreActions(PipeId);
  }

  return retVal;
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Session.c
 *********************************************************************************************************************/
