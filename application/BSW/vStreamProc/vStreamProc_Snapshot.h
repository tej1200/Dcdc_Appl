/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Snapshot.h
 *        \brief  vStreamProc header file for the snapshot handling.
 *
 *      \details  Declaration of the snapshot related functionality.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_SNAPSHOT_H
#define VSTREAMPROC_SNAPSHOT_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot of the provided processing node.
 * \details       -
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \param[in]     NamedOutputPortId                 Id of the named output port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_NamedOutputPortIdType NamedOutputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshotWithInputTrigger
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot of the provided processing node.
 * \details       -
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \param[in]     NamedInputPortId                  Id of the named input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_NamedInputPortIdType NamedInputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateEntryNodeSnapshot
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot of the provided entry node.
 * \details       -
 * \param[in]     EntryNodeId           Id of the processing node.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateEntryNodeSnapshot(
  vStreamProc_ProcessingNodeIdType EntryNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateExitNodeSnapshots
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot for all entry nodes of the provided pipe.
 * \details       -
 * \param[in]     PipeId           Id of the pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateEntryNodeSnapshots(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateExitNodeSnapshots
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot for all exit nodes of the provided pipe.
 * \details       -
 * \param[in]     PipeId           Id of the pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateExitNodeSnapshots(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_RemoveObsoleteSnapshots
 *********************************************************************************************************************/
/*!
 * \brief         Removes all snapshots of the processing node before the provided reference snapshot.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     ReferenceSnapshotSlotId   Id of the reference snapshot.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_RemoveObsoleteSnapshots(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType ReferenceSnapshotSlotId);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_SNAPSHOT_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Snapshot.h
 *********************************************************************************************************************/

