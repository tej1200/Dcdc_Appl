/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_Cipher.c
 *        \brief  vStreamProc Cipher Sub Module Source Code File
 *      \details  Implementation of the cipher sub module for the vStreamProc framework
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_CIPHER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_Cipher.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_CIPHER_CONFIG == STD_ON)
# include "Csm.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_InitLocal()
 *********************************************************************************************************************/
/*!
 * \brief           Initialize CSM cipher job.
 * \details         -
 * \param[in]       ProcNodeInfo                The processing node information to operate on.
 * \pre           -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_InitLocal(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_QueryPorts
 *********************************************************************************************************************/
/*!
 * \brief           Queries the available input and output lengths of the provided ProcNode.
 * \details         The queried data is saved to the InputPortInfo and OutputPortInfo
 * \param[in]       ProcNodeInfo            Pointer to the Processing Node info of the current node.
 * \param[out]      InputPortInfo           Pointer to the input port info structure for the query.
 * \param[out]      OutputPortInfo          Pointer to the output port info structure for the query.
 * \param[in]       MinOutputLength         The minimum required buffer length at the output port.
 * \return          VSTREAMPROC_OK                    Operation was successful.
 * \return          VSTREAMPROC_INSUFFICIENT_INPUT    There is no data available at the input port.
 * \return          VSTREAMPROC_INSUFFICIENT_OUTPUT   There is less than the requested minimum output buffer length available.
 * \return          VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfo,
  vStreamProc_LengthType MinOutputLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * \brief           Changes the signal handler states based on the provided trigger signals.
 * \details         The signal handler states are only changed if the current signal is different to the new trigger signal.
 *                  If the trigger signal is changed, the corresponding signal handler state will be set to
 *                  REGISTERED and the other signal handler state will be set to UNREGISTERED.
 * \param[in]       ProcessingNodeId        ID of the ProcessingNode for which the signal handler shall be set.
 * \param[in]       CurrentTriggerSignalId  ID of the current trigger signal.
 * \param[in]       NewTriggerSignalId      ID of the new trigger signal.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType CurrentTriggerSignalId,
  vStreamProc_SignalIdType NewTriggerSignalId);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_UpdateSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * \brief           Updates the signal handler states based on the input and output lengths.
 * \details         -
 * \param[in]       ProcNodeInfo            Pointer to the Processing Node info of the current node.
 * \param[in]       RemainingInputLength    The remaining data length at the input port.
 * \param[in]       RemainingOutputLength   The remaining buffer length at the output port.
 * \param[in]       MinOutputLength         The minimum required buffer length at the output port.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_UpdateSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType RemainingInputLength,
  vStreamProc_LengthType RemainingOutputLength,
  vStreamProc_LengthType MinOutputLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_PerformCipherOperation()
 *********************************************************************************************************************/
/*!
 * \brief           Performs the provided cipher operation and evaluates the CSM return value.
 * \details         -
 * \param[in]       CipherConfigPtr       Pointer to the cipher configuration.
 * \param[in]       OpMode                The operation mode which shall be used.
 * \param[in,out]   ReadRequestPtr        Pointer to the ReadRequest. The Buffer and AvailableLength are used
 *                                        as input parameters for the CSM operation.
 *                                        The RequestLength is updated with the actually used buffer length.
 * \param[in,out]   WriteRequestPtr       Pointer to the WriteRequest. The Buffer and the AvailableLength are used
 *                                        as output parameters for the CSM operation.
 *                                        The RequestLength is updated with the actually used buffer length.
 * \param[in,out]   SignalResponsePtr     Pointer to the signal response. The ACK flag is set based on the result of
 *                                        the CSM operation.
 * \return          VSTREAMPROC_OK        Operation was successful.
 * \return          VSTREAMPROC_PENDING   The CSM is busy.
 * \return          VSTREAMPROC_FAILED    Operation was unsuccessful.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_PerformCipherOperation(
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CipherConfigPtr,
  Crypto_OperationModeType OpMode,
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ReadRequestPtr,
  P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) WriteRequestPtr,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponsePtr);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_InitLocal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_InitLocal(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST)  specializedCfgPtr;
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR)                  cipherWorkspace;
  Std_ReturnType csmResult;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  cipherWorkspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize the encryption/decryption CSM Job */
  if (specializedCfgPtr->CipherModeOfProcessingNode_Cipher_Config == VSTREAMPROC_DECRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHER_CONFIG)
  {
    csmResult = Csm_Decrypt(specializedCfgPtr->CsmJobIdOfProcessingNode_Cipher_Config,
                          CRYPTO_OPERATIONMODE_START, NULL_PTR, 0u,
                          NULL_PTR, NULL_PTR);
  }
  else /* == VSTREAMPROC_ENCRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHER_CONFIG */
  {
    csmResult = Csm_Encrypt(specializedCfgPtr->CsmJobIdOfProcessingNode_Cipher_Config,
                          CRYPTO_OPERATIONMODE_START, NULL_PTR, 0u,
                          NULL_PTR, NULL_PTR);
  }

  /* In case everything went fine fall through to next state. */
  if (csmResult == E_OK)
  {
    /* Initialize the encryption/decryption CSM Job */
    cipherWorkspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_QueryPorts
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfo,
  vStreamProc_LengthType MinOutputLength)
{
  vStreamProc_ReturnType retVal;

  /* #10 Prepare Port infos. */
  retVal = vStreamProc_PrepareAllPortInfos(ProcNodeInfo, InputPortInfo, 1u, OutputPortInfo, 1u);

  /* #20 Check available input data. */
  if (retVal == VSTREAMPROC_OK)
  {
    /* Request at least one byte. */
    InputPortInfo->ReadRequest.StorageInfo.RequestLength = 1u;

    retVal = vStreamProc_GetInputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, InputPortInfo);
  }

  /* #30 Check available output buffer. */
  if (retVal == VSTREAMPROC_OK)
  {
    /* Request minimum output length. */
    OutputPortInfo->WriteRequest.StorageInfo.RequestLength = MinOutputLength;

    retVal = vStreamProc_GetOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, OutputPortInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType CurrentTriggerSignalId,
  vStreamProc_SignalIdType NewTriggerSignalId)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */

  /* #10 Check if signal handler is switched from DataAvailable to StorageAvailable. */
  if ((CurrentTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable)
    &&(NewTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable))
  {
    /* Block DataAvailable signal handler and register StorageAvailable signal handler. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcInputPort_vStreamProc_Cipher_InputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    (void)vStreamProc_SetOutputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Cipher_OutputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

  }

  /* #20 Check if signal handler is switched from StorageAvailable to DataAvailable. */
  if ((CurrentTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable)
    && (NewTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable))
  {
    /* Deregister StorageAvailable signal handler and register DataAvailable signal handler. */
    (void)vStreamProc_SetOutputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Cipher_OutputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcInputPort_vStreamProc_Cipher_InputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_UpdateSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_UpdateSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType RemainingInputLength,
  vStreamProc_LengthType RemainingOutputLength,
  vStreamProc_LengthType MinOutputLength)
{
  /* #10 Update signal handler registration. */
  if ( (RemainingOutputLength < MinOutputLength)
    && (RemainingInputLength > 0u))
  {
    /* #20 If there is still data, but not enough output buffer available, register StorageAvailable signal handler. */
    vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates(
      ProcNodeInfo->ProcessingNodeId,
      ProcNodeInfo->SignalInfo.SignalId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable);
  }
  else
  {
    /* #30 Otherwise, register DataAvailable signal handler. */
    vStreamProc_ProcessingNode_Cipher_SwitchSignalHandlerStates(
      ProcNodeInfo->ProcessingNodeId,
      ProcNodeInfo->SignalInfo.SignalId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable);
  }
}
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_PerformCipherOperation()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_PerformCipherOperation(
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CipherConfigPtr,
  Crypto_OperationModeType OpMode,
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ReadRequestPtr,
  P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) WriteRequestPtr,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponsePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType            csmResult;
  vStreamProc_ReturnType    retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare RequestLength of WriteRequest. */
  WriteRequestPtr->StorageInfo.RequestLength = WriteRequestPtr->StorageInfo.AvailableLength;

  /* #20 Call CSM based on the provided config. */
  if (CipherConfigPtr->CipherModeOfProcessingNode_Cipher_Config == VSTREAMPROC_DECRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHER_CONFIG)
  {
    /* Perform decryption. */
    csmResult = Csm_Decrypt(
      CipherConfigPtr->CsmJobIdOfProcessingNode_Cipher_Config,
      OpMode,
      ReadRequestPtr->Buffer, ReadRequestPtr->StorageInfo.RequestLength,
      WriteRequestPtr->Buffer, &WriteRequestPtr->StorageInfo.RequestLength);
  }
  else /* == VSTREAMPROC_ENCRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHER_CONFIG */
  {
    /* Perform encryption. */
    csmResult = Csm_Encrypt(
      CipherConfigPtr->CsmJobIdOfProcessingNode_Cipher_Config,
      OpMode,
      ReadRequestPtr->Buffer, ReadRequestPtr->StorageInfo.RequestLength,
      WriteRequestPtr->Buffer, &WriteRequestPtr->StorageInfo.RequestLength);
  }

  /* #30 Handle CSM results. */
  switch (csmResult)
  {
    /* #40 If CSM returned E_OK, set the WORKDONE flag in the signal response. */
    case E_OK:
    {
      /* All usable input data was consumed, read request length is up to date */
      /* Write request length is already set by CSM */

      /* Update signal response */
      vStreamProc_SetSignalResponseFlag(*SignalResponsePtr, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);

      retVal = VSTREAMPROC_OK;
      break;
    }
    /* #50 If CSM is busy or queue is full: Reset ACK flag to try again in next cycle. */
    case CRYPTO_E_BUSY:
    case CRYPTO_E_QUEUE_FULL:
    {
      /* No data was used, reset request lengths */
      ReadRequestPtr->StorageInfo.RequestLength = 0u;
      WriteRequestPtr->StorageInfo.RequestLength = 0u;

      /* Stay scheduled, because the pending reason is external. */
      vStreamProc_ClrSignalResponseFlag(*SignalResponsePtr, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

      retVal = VSTREAMPROC_PENDING;

      break;
    }
    /* #60 Otherwise: Treat all other return values as error. */
    default:
    {
      /* No data was used, reset request lengths */
      ReadRequestPtr->StorageInfo.RequestLength = 0u;
      WriteRequestPtr->StorageInfo.RequestLength = 0u;

      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  VAR(Std_ReturnType, AUTOMATIC) retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's workspace */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize state value of workspace. */
  workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_UpdateCipher
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_UpdateCipher(    /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult = VSTREAMPROC_FAILED;

  vStreamProc_InputPortInfoType   inputPortInfo;
  vStreamProc_OutputPortInfoType  outputPortInfo;

  vStreamProc_StorageInfoPtrType  inputStorageInfo = &inputPortInfo.ReadRequest.StorageInfo;
  vStreamProc_StorageInfoPtrType  outputStorageInfo = &outputPortInfo.WriteRequest.StorageInfo;

  vStreamProc_LengthType          availableInputLength = 0u;
  vStreamProc_LengthType          processedInputLength = 0u;
  vStreamProc_LengthType          minOutputLength;
  vStreamProc_LengthType          availableOutputLength = 0u;
  vStreamProc_LengthType          writtenOutputLength = 0u;

  P2CONST(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Set output length to default value. */
  minOutputLength = specializedCfgPtr->MinOutputLengthOfProcessingNode_Cipher_Config;

  /* #10 Initialize the encryption/decryption CSM Job if necessary. */
  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    vStreamProc_ProcessingNode_Cipher_InitLocal(ProcNodeInfo);
  }

  /* #20 Start cipher update process. */
  if(workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* Signal is acknowledged by default. Flag is later cleared if necessary. */
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

    /* #30 Query the input and output ports. */
    currentResult = vStreamProc_ProcessingNode_Cipher_QueryPorts(ProcNodeInfo, &inputPortInfo, &outputPortInfo, minOutputLength);

    availableInputLength = inputStorageInfo->AvailableLength;
    availableOutputLength = outputStorageInfo->AvailableLength;

    /* #40 Get the buffers. */
    if (currentResult == VSTREAMPROC_OK)
    {
      vStreamProc_LengthType  maxInputLength;

      inputStorageInfo->RequestLength = availableInputLength;
      outputStorageInfo->RequestLength = availableOutputLength;

      /* #50 Calculate max input length based on the available output buffer and limit the requested length if necessary. */
      /* Max input length is always capped to a multiple of the minimum output length due to the unknown number of
         buffered bytes in the crypto driver. */
      maxInputLength = (availableOutputLength / minOutputLength) * minOutputLength;

      if (maxInputLength < availableInputLength)
      {
        /* Not all available data can be consumed, limit to max input length.*/
        inputStorageInfo->RequestLength = maxInputLength;
      }

      currentResult = vStreamProc_RequestPortData(ProcNodeInfo, &inputPortInfo, 1u, &outputPortInfo, 1u);
    }

    /* #60 Process input data. */
    if (currentResult == VSTREAMPROC_OK)
    {
      /* Call the CSM. The Request lengths from the ReadRequest/Write Request will be used as input. */
      /* The Request lengths are updated depending on the CSM operation result. */
      currentResult = vStreamProc_ProcessingNode_Cipher_PerformCipherOperation(
        specializedCfgPtr,
        CRYPTO_OPERATIONMODE_UPDATE,
        &inputPortInfo.ReadRequest,
        &outputPortInfo.WriteRequest,
        &signalResponse);

      processedInputLength = inputStorageInfo->RequestLength;
      writtenOutputLength = outputStorageInfo->RequestLength;

      /* Ports are always acknowledged to unlock buffers. */
      (void)vStreamProc_AcknowledgePorts(ProcNodeInfo, &inputPortInfo, 1u, &outputPortInfo, 1u);
    }
  }

  /* #70 Update signal handler registration states and set SUCCESS flag. */
  if (currentResult != VSTREAMPROC_FAILED)
  {
    /* The Signal Handler are only updated if the current signal is acknowledged. */
    if (vStreamProc_IsSignalResponseFlagSet(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK))
    {
      vStreamProc_ProcessingNode_Cipher_UpdateSignalHandlerStates(
        ProcNodeInfo,
        (availableInputLength - processedInputLength),
        (availableOutputLength - writtenOutputLength),
        minOutputLength);
    }

    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_FinishCipher
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_FinishCipher(    /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult = VSTREAMPROC_FAILED;

  vStreamProc_InputPortInfoType   inputPortInfo;
  vStreamProc_OutputPortInfoType  outputPortInfo;
  vStreamProc_LengthType          minOutputLength;

  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Check state of cipher operation. */
  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    /* #20 If state is initialized, signal is only acknowledged */
    /* Only possible no data was provided before. Set signal response simply to ACKNOWLEDGE as there is nothing to do. */
    /* No output will be written */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }
  /* #30 Otherwise, finish the cipher operation */
  else
  {
    /* Set the output length to 2x minimum output due to the unknown number of buffered bytes in CSM. */
    minOutputLength = (vStreamProc_LengthType)specializedCfgPtr->MinOutputLengthOfProcessingNode_Cipher_Config * 2u;

    /* #40 Prepare Port infos. */
    /* Input port structure is initialized because it is later used to finish the CSM operation */
    currentResult = vStreamProc_PrepareAllPortInfos(ProcNodeInfo, &inputPortInfo, 1u, &outputPortInfo, 1u);

    /* #50 Check available output buffer. */
    if (currentResult == VSTREAMPROC_OK)
    {
      /* Request minimum output length. */
      outputPortInfo.WriteRequest.StorageInfo.RequestLength = minOutputLength;

      currentResult = vStreamProc_GetOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, &outputPortInfo);
    }

    /* #60 Check available output buffer. */
    if (currentResult == VSTREAMPROC_OK)
    {
      currentResult = vStreamProc_RequestOutputPortData(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, minOutputLength, &outputPortInfo);
    }

    /* #70 Finalize Cipher. */
    if (currentResult == VSTREAMPROC_OK)
    {
      /* Hint: inputPortInfo is used on purpose without previous port request.
               The structure was initialized to zero values by vStreamProc_PrepareAllPortInfos(),
               which means the CSM will not use any input data. */
      currentResult = vStreamProc_ProcessingNode_Cipher_PerformCipherOperation(
        specializedCfgPtr,
        CRYPTO_OPERATIONMODE_FINISH,
        &inputPortInfo.ReadRequest,
        &outputPortInfo.WriteRequest,
        &signalResponse);

      /* #80 Acknowledge the output port to unlock the buffer. */
      (void)vStreamProc_AcknowledgePorts(ProcNodeInfo, NULL_PTR, 0u, &outputPortInfo, 1u);

      workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;
      vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);
    }

    /* #90 Check overall result and set signal response accordingly */
    if (currentResult != VSTREAMPROC_FAILED)
    {
      vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

#if  (VSTREAMPROC_PROCESSINGNODETYPE_CSMVERIFICATION_STORE_CSM_CONTEXT == STD_ON)
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Hint: Actual size of context is written by CSM. */
  workspace->SizeOfStoredCsmContext = specializedCfgPtr->SizeOfContextOfProcessingNode_Cipher_Config;

  /* #10 Check if there is a context buffer. */
  if (specializedCfgPtr->SizeOfContextOfProcessingNode_Cipher_Config > 0u)
  {
    /* #20 Check if the node is in PROCESSING state. */
    if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
    {
      Std_ReturnType csmRetVal;

      /* #30 Store the CSM context in the workspace via the CSM. */
      csmRetVal = Csm_SaveContextJob(
        specializedCfgPtr->CsmJobIdOfProcessingNode_Cipher_Config,
        workspace->CsmContextBuffer,
        &workspace->SizeOfStoredCsmContext);

      switch (csmRetVal)
      {
        /* #40 If the storing was successful, confirm the signal. */
        case E_OK:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
          break;
        }
        /* #50 If the CSM is busy, return pending to try again in the next cycle. */
        case CRYPTO_E_BUSY:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
          break;
        }
        /* #60 If any other return value: Consider as error. */
        default:
        {
          /* Either E_NOT_OK or an unexpected return value was received. */
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
          break;
        }
      }
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
#endif

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Cancel job. */
    (void) Csm_CancelJob(specializedCfgPtr->CsmJobIdOfProcessingNode_Cipher_Config, CRYPTO_OPERATIONMODE_FINISH);

    /* #30 Reset state. */
    workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;

    /* #40 Set signal response to confirm. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Cipher_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Cipher_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
#if  (VSTREAMPROC_PROCESSINGNODETYPE_CIPHER_STORE_CSM_CONTEXT == STD_ON)
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_Cipher_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Cipher_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  Std_ReturnType csmRetVal;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Cipher_Config(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Cipher_WorkspaceType(ProcNodeInfo);                         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Restore the context. */
    csmRetVal = Csm_RestoreContextJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_Cipher_Config,
      workspace->CsmContextBuffer,
      workspace->SizeOfStoredCsmContext);

    switch (csmRetVal)
    {
      /* #30 If the storing was successful, confirm the signal. */
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      /* #40 If the CSM is busy, return pending to try again in the next cycle. */
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      /* #50 If any other return value: Consider as error. */
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received. */
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
        break;
      }
    }
  }
  /* #60 Otherwise: Nothing to do, acknowledge the signal. */
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#endif
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_CIPHER_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
*/

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_Cipher.c
 *********************************************************************************************************************/
