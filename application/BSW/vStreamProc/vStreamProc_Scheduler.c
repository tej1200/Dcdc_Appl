/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Scheduler.c
 *        \brief  vStreamProc Scheduler Sub Module Source Code File
 *
 *      \details  Implementation of the vStreamProc Scheduler sub module.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_SCHEDULER_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Scheduler.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_Queue.h"
#include "vStreamProc_Snapshot.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

 /*********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetQueueIdxOfSignalHandler
 *********************************************************************************************************************/
/*!
 * \brief         Returns the index of the queue the provided signal handler belongs to.
 * \details       -
 * \param[in]     SignalHandlerId     The signal handler id.
 * \return        The index of the queue.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SchedulingQueueIterType, AUTOMATIC) vStreamProc_Scheduler_GetQueueIdxOfSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsSignalHandlerStateAllowed
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the requested new state is allowed for the provided signal.
 * \details       -
 * \param[in]     SignalId                Id of the signal.
 * \param[in]     NewSignalHandlerState   The requested signal handler state.
 * \return        TRUE                    The signal handler state is allowed for the signal.
 * \return        FALSE                   The signal handler state is not allowed for the signal.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsSignalHandlerStateAllowed(
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsSignalHandlerScheduled
 *********************************************************************************************************************/
/*!
 * \brief         Checks if a passed signal is contained in a schedule table
 * \details       -
 * \param[in]     SignalHandlerDefId  The signal handler  id which shall be matched with the schedule table entries.
 * \param[out]    QueueHandle         Id of the queue entry. Only set if fitting signal is found.
 *                                    Can be set to NULL_PTR if not required.
 * \return        TRUE           Node is in schedule table.
 * \return        FALSE          Node is not in schedule table.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsSignalHandlerScheduled(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(vStreamProc_QueueHandleType, AUTOMATIC, VSTREAMPROC_APPL_VAR) QueueHandle);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsStarvedInputPortConnectedToEntryPoint
 *********************************************************************************************************************/
/*!
 * \brief         Check if a starved input port of the passed node is connected to an entry point.
 * \details       Port is starved, if the specific result is VSTREAMPROC_INSUFFICIENT_INPUT.
 * \param[in]     ProcNodeInfo    Information structure of the processing node.
 * \return        TRUE   Starved input port of the passed node is connected to an entry point
 * \return        FALSE  No starved input port of the passed node is connected to an entry point.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsStarvedInputPortConnectedToEntryPoint(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsStarvedOutputPortConnectedToExitPoint
 *********************************************************************************************************************/
/*!
 * \brief         Check if a starved output port of the passed node is connected to an exit point.
 * \details       Port is starved, if the specific result is VSTREAMPROC_INSUFFICIENT_OUTPUT.
 * \param[in]     ProcNodeInfo    Information structure of the processing node.
 * \return        TRUE   Starved output port of the passed node is connected to an exit point.
 * \return        FALSE  No starved output port of the passed node is connected to an exit point.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsStarvedOutputPortConnectedToExitPoint(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetRetValBySignalResponse
 *********************************************************************************************************************/
/*!
 * \brief         Translate a signal response and node info to a vStreamProc_ReturnType.
 * \details       Evaluates the port results and the signal response based on the return type severity.
 * \param[in]     SignalResponse   The signal response of the signal handler.
 * \param[in]     ProcNodeInfo     The node info response of the signal handler.
 * \return        The resulting vStreamProc_ReturnType.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_GetRetValBySignalResponse(
  P2CONST(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse,
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetSignalHandlerId
 *********************************************************************************************************************/
/*!
 * \brief         Searches in the provided signal handler range for a signal handler for the provided signal.
 * \details       -
 * \param[in]     SignalHandlerRangeStartId   Start of the signal handler range.
 * \param[in]     SignalHandlerRangeEndId     End of the signal handler range.
 * \param[in]     SignalId                    ID of signal.
 * \return        Returns SignalHandler index of signal if signal was found.
 * \return        VSTREAMPROC_NO_SIGNALHANDLER if signal was not found.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SignalHandlerIdType, AUTOMATIC) vStreamProc_Scheduler_GetSignalHandlerId(
  vStreamProc_SignalHandlerIdType SignalHandlerRangeStartId,
  vStreamProc_SignalHandlerIdType SignalHandlerRangeEndId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_PrepareSignalInfo
 *********************************************************************************************************************/
/*!
 * \brief         Writes the signal parameters to the given signal info pointer.
 * \details       -
 * \param[in]     SignalHandlerId     The id of the signal handler on which the signal info shall be based on.
 * \param[out]    SignalInfo          The signal info which shall be prepared.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Scheduler_PrepareSignalInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(vStreamProc_SignalInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalInfo);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_InitSignalHandlerInfo
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the signal handler info of the given signal handler range with the default value.
 * \details       -
 * \param[in]     SignalHandlerStartId   The start index in the signal handler info array.
 * \param[in]     SignalHandlerEndId     The start index in the signal handler array.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Scheduler_InitSignalHandlerInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerStartId,
  vStreamProc_SignalHandlerIdType SignalHandlerEndId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_UpdateExternalSignalInfo
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the signal handler belongs to a broadcast signal and updates the status if true.
 * \details       -
 * \param[in]     SignalHandlerId       The signal handler id.
 * \param[in]     Result                The result of the signal handler execution.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_Scheduler_UpdateExternalSignalInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  vStreamProc_ReturnType Result);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetSignalHandlerState
 *********************************************************************************************************************/
/*!
 * \brief         Sets the registration state of the provided signal handler to the provided new state.
 * \details       If the new state is UNREGISTERED, pending signal handlers will be removed from the scheduling queue.
 * \param[in]     SignalHandlerId         The signal handler id.
 * \param[in]     NewSignalHandlerState   The new signal handler registration state.
 * \return        VSTREAMPROC_OK          Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED      Signal handler state change not successful.
 *                                        Might be the case if the new state is not allowed for the signal of the
 *                                        signal handler.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetSignalHandlerState(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_DiscardSignalHandler
 *********************************************************************************************************************/
/*!
 * \brief         Discards the provided signal handler from the scheduling queue.
 * \details       -
 * \param[in]     SignalHandlerId       The signal handler id.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Scheduler_DiscardSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_EvaluateSignalHandler
 *********************************************************************************************************************/
/*!
 * \brief         Evaluates the registration state of the signal handler.
 * \details       If the registration state is REGISTERED, the signal handler is executed and the result is returned.
 * \param[in]     SignalHandlerId           Pointer to the queue.
 * \param[out]    WorkDone                  Set to true if work was performed.
 * \param[out]    RemoveFromSchedule        Set to true if the signal handler shall be removed from the queue.
 * \param[out]    AllSignalHandlersBlocked  Set to false if the signal handler is registered.
 * \return        VSTREAMPROC_OK                    Processing of signal finished successfully or
 *                                                  the signal handler was not registered.
 * \return        VSTREAMPROC_PENDING               Processing of signal is not yet finished.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Processing cannot continue due to missing input data.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Processing cannot continue due to missing output buffer.
 * \return        VSTREAMPROC_FAILED                Processing failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_EvaluateSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) WorkDone,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) RemoveFromSchedule,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) AllSignalHandlersBlocked);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_UpdateSignalHandlerQueue
 *********************************************************************************************************************/
/*!
 * \brief         Updates the queue based on the provided queue entry handle.
 * \details       If RemoveFromSchedule is true, the entry will be removed.
 *                Otherwise, it will be rescheduled based on its priority.
 * \param[in]     Queue                 Pointer to the queue.
 * \param[in]     QueueEntryHandle      The queue entry handle.
 * \param[in]     RemoveFromSchedule    Defines if the entry is removed or rescheduled.
 * \param[in,out] FirstRescheduledQueueEntryHandle  The first rescheduled queue entry.
 *                                                  Will be updated if the provided entry is the new first rescheduled entry.
 * \pre           The queue is initialized.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Scheduler_UpdateSignalHandlerQueue(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueueHandleType QueueEntryHandle,
  boolean RemoveFromSchedule,
  P2VAR(vStreamProc_QueueHandleType, AUTOMATIC, VSTREAMPROC_APPL_VAR) FirstRescheduledQueueEntryHandle);

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_ProcessSignalHandler
 *********************************************************************************************************************/
/*!
 * \brief         Calls the signal handler for the provided schedule entry.
 * \details       -
 * \param[in]     SignalHandlerId       The signal handler which shall be processed.
 * \param[out]    WorkDone              TRUE if signal handler performed some actual work.
 *                                      FALSE otherwise.
 * \param[out]    RemoveFromSchedule    TRUE if the signal shall be removed from the signal table.
 *                                      FALSE otherwise.
 * \return        VSTREAMPROC_OK                    Processing of signal finished successfully.
 * \return        VSTREAMPROC_PENDING               Processing of signal is not yet finished.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Processing cannot continue due to missing input data.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Processing cannot continue due to missing output buffer.
 * \return        VSTREAMPROC_FAILED                Processing failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_ProcessSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) WorkDone,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) RemoveFromSchedule);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetQueueIdxOfSignalHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SchedulingQueueIterType, AUTOMATIC) vStreamProc_Scheduler_GetQueueIdxOfSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId)
{
  /* #10 Return the index of the queue the signal handler belongs to. */
  return vStreamProc_GetSchedulingQueueIdxOfPipe(
    vStreamProc_GetPipeIdxOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfSignalHandler(SignalHandlerId)));
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsSignalHandlerStateAllowed
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsSignalHandlerStateAllowed(
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check the allowed state flags of the provided signal. */
  switch (NewSignalHandlerState)
  {
    case(VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED):
    {
      if (vStreamProc_IsStateUnregisteredAllowedOfSignal(SignalId))
      {
        retVal = TRUE;
      }
      break;
    }
    case(VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED):
    {
      if (vStreamProc_IsStateBlockAllowedOfSignal(SignalId))
      {
        retVal = TRUE;
      }
      break;
    }
    default:
    {
      retVal = TRUE;
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsSignalHandlerScheduled
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsSignalHandlerScheduled(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(vStreamProc_QueueHandleType, AUTOMATIC, VSTREAMPROC_APPL_VAR) QueueHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_QueueConstPtrType queue =
    vStreamProc_GetAddrSchedulingQueue(vStreamProc_Scheduler_GetQueueIdxOfSignalHandler(SignalHandlerId));
  vStreamProc_QueueHandleType queueEntryHandle = vStreamProc_Queue_GetFirstUsedHandle(queue);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the queue and check if the requested signal is found. */
  /* If the queue is empty, the loop will not be executed */
  while (queueEntryHandle != VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
  {
    /* Check if the target entry is found */
    if (vStreamProc_Queue_GetEntityHandle(queue, queueEntryHandle) == SignalHandlerId)
    {
      retVal = TRUE;
    }

    /* #20 If the target entry is found, provide the entry handle */
    if (retVal == TRUE)
    {
      if (QueueHandle != NULL_PTR)
      {
        *QueueHandle = queueEntryHandle;
      }
      break;
    }

    /* Continue with next entry */
    queueEntryHandle = vStreamProc_Queue_GetNextHandle(queue, queueEntryHandle);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsStarvedInputPortConnectedToEntryPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsStarvedInputPortConnectedToEntryPoint(               /* PRQA S 6080 */ /* MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_NamedInputPortIterType procInputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If any of the provided input port specific results signals an insufficient input: */
  for (procInputPortIdx = 0u; procInputPortIdx < ProcNodeInfo->InputPortCount; procInputPortIdx++)
  {
    if (ProcNodeInfo->InputPortResults[procInputPortIdx] == VSTREAMPROC_INSUFFICIENT_INPUT)
    {
      vStreamProc_InputPortHandleType inputPortHandle;

      /* #20 Check if the input port is connected. */
      (void)vStreamProc_Stream_GetInputPortHandle(
        ProcNodeInfo->ProcessingNodeId,
        (vStreamProc_InputPortSymbolicNameType)procInputPortIdx,
        &inputPortHandle);

      /* #30 Check if there is an active producer. */
      if ((inputPortHandle.IsConnected) && (inputPortHandle.HasActiveProducer == TRUE))
      {
        vStreamProc_ProcessingNodeIdType producerNodeId =
          vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
            vStreamProc_GetNamedOutputPortIdxOfStreamInfo(inputPortHandle.StreamId));

        /* #40 Check whether the producer node is an access node. */
        if (vStreamProc_AccessNode_IsEntryNode(producerNodeId) == TRUE)
        {
          vStreamProc_EntryPointIdType entryPointId =
            vStreamProc_AccessNode_GetEntryPointIdOfEntryNode(producerNodeId);

          if (vStreamProc_AccessNode_IsInputStreamPending(entryPointId) == FALSE)
          {
            retVal = TRUE;
            break;
          }
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsStarvedOutputPortConnectedToExitPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsStarvedOutputPortConnectedToExitPoint(               /* PRQA S 6080 */ /* MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_NamedOutputPortIterType procOutputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If any of the provided output port specific results signals an insufficient output: */
  for (procOutputPortIdx = 0u; procOutputPortIdx < ProcNodeInfo->OutputPortCount; procOutputPortIdx++)
  {
    if (ProcNodeInfo->OutputPortResults[procOutputPortIdx] == VSTREAMPROC_INSUFFICIENT_OUTPUT)
    {
      vStreamProc_OutputPortHandleType outputPortHandle;

      /* #20 Check whether the associated storage node is connected to an (active) exit point. */
      if (vStreamProc_Stream_GetOutputPortHandle(
            ProcNodeInfo->ProcessingNodeId,
            (vStreamProc_OutputPortSymbolicNameType)procOutputPortIdx,
            &outputPortHandle) == VSTREAMPROC_OK)
      {
        vStreamProc_StreamOutputPortIterType streamOutputPortIdx;

        for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(outputPortHandle.StreamId);
             streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(outputPortHandle.StreamId);
             streamOutputPortIdx++)
        {
          vStreamProc_NamedInputPortIdType namedInputPortId =
            vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(streamOutputPortIdx);
          vStreamProc_ProcessingNodeIdType consumerNodeId =
            vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortId);

          if (vStreamProc_AccessNode_IsExitNode(consumerNodeId) == TRUE)
          {
            vStreamProc_ExitPointIdType exitPointId =
              vStreamProc_AccessNode_GetExitPointIdOfExitNode(consumerNodeId);

            vStreamProc_InputPortHandleType inputPortHandle;

            /* #30 Check if the exit node is connected to this stream. */
            (void)vStreamProc_Stream_GetInputPortHandle(
              consumerNodeId,
              vStreamProc_GetInputPortSymbolicNameOfPortDef(vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortId)),
              &inputPortHandle);

            if (inputPortHandle.StreamOutputPortId == streamOutputPortIdx)
            {
              vStreamProc_StreamPositionType producePosition =
                vStreamProc_GetProducePositionOfWaveVarInfo(inputPortHandle.WaveId);

              /* #40 Check if there is no output stream pending and if there is unconsumed data. */
              /* Hint: This check is necessary to avoid reporting a INSUFFICIENT_OUTPUT although there is no data
                         to consume. This might be the case if a second, internal consumer is responsible. */
              if ( (vStreamProc_AccessNode_IsOutputStreamPending(exitPointId) == FALSE)
                && (vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortIdx) != producePosition))
              {
                retVal = TRUE;
                break;
              }
            }
          }
        }
      }

      if (retVal == TRUE)
      {
        break;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_IsExitNode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Scheduler_IsExitNode(                                            /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_ExitPointIterType exitPointId;


  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all exit points of the pipe and check if the provided processing node is a exit node, */
  for ( exitPointId = vStreamProc_GetExitPointStartIdxOfPipe(pipeId);
        exitPointId < vStreamProc_GetExitPointEndIdxOfPipe(pipeId);
        exitPointId++)
  {
    vStreamProc_ProcessingNodeIterType exitNodeIdx =
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(vStreamProc_GetNamedInputPortIdxOfExitPoint(exitPointId));

    if (exitNodeIdx == ProcessingNodeId)
    {
      retVal = TRUE;
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetRetValBySignalResponse
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_GetRetValBySignalResponse(
  P2CONST(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse,
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_InputPortIterType inputPortIdx;
  vStreamProc_OutputPortIterType outputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if failed bit of the signal response is set. */
  if (!vStreamProc_IsSignalResponseFlagSet(*SignalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS))
  {
    retVal = VSTREAMPROC_FAILED;
  }
  else
  {
    /* #20 If not: Iterate over the output port results and save the return value with highest severity. */
    for ( outputPortIdx = 0u;
          outputPortIdx < ProcNodeInfo->OutputPortCount;
          outputPortIdx++)
    {
      if (ProcNodeInfo->OutputPortResults[outputPortIdx] > retVal)
      {
        retVal = ProcNodeInfo->OutputPortResults[outputPortIdx];
      }
    }

    /* #30 Iterate over the input port results.
           If a return value with a higher severity is found, the return value is updated. */
    for ( inputPortIdx = 0u;
          inputPortIdx < ProcNodeInfo->InputPortCount;
          inputPortIdx++)
    {
      if (ProcNodeInfo->InputPortResults[inputPortIdx] > retVal)
      {
        retVal = ProcNodeInfo->InputPortResults[inputPortIdx];
      }
    }

    /* #40 Evaluate the acknowledge bit of the signal response and update the return value to pending if necessary. */
    if ((retVal < VSTREAMPROC_PENDING)
      && (!vStreamProc_IsSignalResponseFlagSet(*SignalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK)))
    {
      retVal = VSTREAMPROC_PENDING;
    }
  }
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_GetSignalHandlerId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_SignalHandlerIdType, AUTOMATIC) vStreamProc_Scheduler_GetSignalHandlerId(
  vStreamProc_SignalHandlerIdType SignalHandlerRangeStartId,
  vStreamProc_SignalHandlerIdType SignalHandlerRangeEndId,
  vStreamProc_SignalIdType SignalId)
{
  vStreamProc_SignalHandlerIdType   retVal = VSTREAMPROC_NO_SIGNALHANDLER;
  vStreamProc_SignalHandlerIterType signalHandlerIdx;

  /* #10 Iterate over the signal handler range until an entry for the provided signal is found. */
  for (signalHandlerIdx = (vStreamProc_SignalHandlerIterType)SignalHandlerRangeStartId;
       signalHandlerIdx < (vStreamProc_SignalHandlerIterType)SignalHandlerRangeEndId;
       signalHandlerIdx++)
  {
    if (vStreamProc_GetSignalIdxOfSignalHandlerDef(vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(signalHandlerIdx)) == SignalId)
    {
      retVal = (vStreamProc_SignalHandlerIdType)signalHandlerIdx;
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_PrepareSignalInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Scheduler_PrepareSignalInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(vStreamProc_SignalInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalInfo)
{
  vStreamProc_SignalHandlerDefIdType signalHandlerDefId = vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(SignalHandlerId);
  vStreamProc_PortDefIterType portDefIdx = vStreamProc_GetPortDefIdxOfSignalHandlerDef(signalHandlerDefId);

  SignalInfo->SignalId = vStreamProc_GetSignalIdxOfSignalHandlerDef(signalHandlerDefId);

 /* #10 Write the signal parameters to the provided signal info. */
  if (portDefIdx == VSTREAMPROC_NO_PORTDEFIDXOFSIGNALHANDLERDEF)
  {
    /* Node Signal. */
    SignalInfo->InputPortId = VSTREAMPROC_NO_NAMEDINPUTPORT;
    SignalInfo->OutputPortId = VSTREAMPROC_NO_NAMEDOUTPUTPORT;
  }
  else
  {
    /* Port Signal. */
    SignalInfo->InputPortId = vStreamProc_GetInputPortSymbolicNameOfPortDef(portDefIdx);
    SignalInfo->OutputPortId = vStreamProc_GetOutputPortSymbolicNameOfPortDef(portDefIdx);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_InitSignalHandlerInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Scheduler_InitSignalHandlerInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerStartId,
  vStreamProc_SignalHandlerIdType SignalHandlerEndId)
{
  vStreamProc_SignalHandlerIterType signalHandlerIdx;

  /* #10 Iterate over the signal handlers and initialize its state with the default value. */
  for (signalHandlerIdx = (vStreamProc_SignalHandlerIterType)SignalHandlerStartId;
       signalHandlerIdx < (vStreamProc_SignalHandlerIterType)SignalHandlerEndId;
       signalHandlerIdx++)
  {
    vStreamProc_SetStateOfSignalHandlerInfo(
      (vStreamProc_SignalHandlerInfoIdType)signalHandlerIdx,
      vStreamProc_GetInitStateOfSignalHandlerDef(vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(signalHandlerIdx)));
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_UpdateExternalSignalInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_Scheduler_UpdateExternalSignalInfo(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  vStreamProc_ReturnType Result)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasExternalSignalDef())
  {
    vStreamProc_ExternalSignalDefIdType  externalSignalDefId = vStreamProc_GetExternalSignalDefIdxOfSignalHandler(SignalHandlerId);

    /* #10 If an entry was found and external signal processing is pending, update the status of the external signal. */
    /* Hint: Some signals used externally may also be issued by internal events, e.g. OutputSnapshotRequested. */
    if ( (externalSignalDefId != VSTREAMPROC_NO_EXTERNALSIGNALDEFIDXOFSIGNALHANDLER)
      && (vStreamProc_GetPendingCountOfExternalSignalInfo(externalSignalDefId) > 0u) )
    {
      /* #20 Update the external signal info. */
      switch (Result)
      {
        case(VSTREAMPROC_OK):
        {
          vStreamProc_DecPendingCountOfExternalSignalInfo(externalSignalDefId);
          break;
        }
        case(VSTREAMPROC_PENDING):
        {
          /* Nothing to do */
          break;
        }
        default:
        {
          /* Report FAILED in all other cases */
          vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_FAILED);
          vStreamProc_SetPendingCountOfExternalSignalInfo(externalSignalDefId, 0u);
          break;
        }
      }

      /* #30 Check if the external signal processing is completed. */
      if (vStreamProc_GetPendingCountOfExternalSignalInfo(externalSignalDefId) == 0u)
      {
        vStreamProc_SignalCallbackType signalCallbackFct =
          vStreamProc_GetSignalCallbackOfExternalSignalInfo(externalSignalDefId);

        /* #40 If there were no errors, set the result to VSTREAMPROC_OK. */
        if (vStreamProc_GetResultOfExternalSignalInfo(externalSignalDefId) == VSTREAMPROC_PENDING)
        {
          vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_OK);
        }

        /* #50 Call the signal callback if available. */
        if (signalCallbackFct != (vStreamProc_SignalCallbackType)0)
        {
          signalCallbackFct(
            vStreamProc_GetPipeIdxOfProcessingNode(
              vStreamProc_GetProcessingNodeIdxOfSignalHandler(SignalHandlerId)),
              vStreamProc_GetSignalIdxOfExternalSignalDef(externalSignalDefId),
              vStreamProc_GetResultOfExternalSignalInfo(externalSignalDefId),
              vStreamProc_GetSignalCallbackHandleOfExternalSignalInfo(externalSignalDefId));
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_EvaluateSignalHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_EvaluateSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) WorkDone,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) RemoveFromSchedule,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) AllSignalHandlersBlocked)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check the registration state of the signal handler. */
  switch (vStreamProc_GetStateOfSignalHandlerInfo(SignalHandlerId))
  {
    case VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED:
    {
      /* #20 If registered, call the signal handler of the queued signal. */
      retVal = vStreamProc_Scheduler_ProcessSignalHandler(
        SignalHandlerId,
        WorkDone,
        RemoveFromSchedule);

      *AllSignalHandlersBlocked = FALSE;

      vStreamProc_Scheduler_UpdateExternalSignalInfo(SignalHandlerId, retVal);

      break;
    }
    case VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED:
    {
      /* #30 If blocked, just reschedule */
      *RemoveFromSchedule = FALSE;
      break;
    }
    default:
    {
      /* #40 Otherwise: do nothing */
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_UpdateSignalHandlerQueue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Scheduler_UpdateSignalHandlerQueue(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueueHandleType QueueEntryHandle,
  boolean RemoveFromSchedule,
  P2VAR(vStreamProc_QueueHandleType, AUTOMATIC, VSTREAMPROC_APPL_VAR) FirstRescheduledQueueEntryHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalHandlerIdType signalHandlerId =
    (vStreamProc_SignalHandlerIdType)vStreamProc_Queue_GetEntityHandle(Queue, QueueEntryHandle);
  vStreamProc_SignalHandlerStateType signalHandlerState = vStreamProc_GetStateOfSignalHandlerInfo(signalHandlerId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the signal handler is still registered. */
  /* Hint: If the signal handler state is now unregistered, the state was changed during execution
           and the signal handler was already removed from queue. Therefore, it must not be removed or re-queued. */
  if (signalHandlerState != VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED)
  {
    /* #20 Remove the signal from the queue if requested. */
    if (RemoveFromSchedule == TRUE)
    {
      (void)vStreamProc_Queue_Remove(Queue, QueueEntryHandle);
    }
    /* #30 Otherwise queue the signal again. */
    else
    {
      vStreamProc_QueuePrioType entryPriority = vStreamProc_Queue_GetPriority(Queue, QueueEntryHandle);

      (void)vStreamProc_Queue_PrioUpdate(Queue, QueueEntryHandle, entryPriority);

      /* #40 Update the first rescheduled queue entry. This is necessary if:
              1. No signal has been rescheduled in this cycle yet.
              2. The rescheduled signal has a higher priority as the previous first rescheduled signal
                (might happen if new high priority signals are scheduled during Scheduler_Process()). */
      if ((*FirstRescheduledQueueEntryHandle == VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
        || (entryPriority < (vStreamProc_Queue_GetPriority(Queue, *FirstRescheduledQueueEntryHandle))))
      {
        *FirstRescheduledQueueEntryHandle = QueueEntryHandle;
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_ProcessSignalHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_ProcessSignalHandler(            /* PRQA S 6030, 6050 */ /* MD_MSR_STCYC, MD_MSR_STCAL */
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) WorkDone,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_VAR) RemoveFromSchedule)
{
  vStreamProc_ReturnType      retVal                = VSTREAMPROC_OK;                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  boolean                     removeFromSchedule    = TRUE;
  boolean                     workDone              = TRUE;
  vStreamProc_SignalIdType    signalId =
    vStreamProc_GetSignalIdxOfSignalHandlerDef(vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(SignalHandlerId));
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfSignalHandler(SignalHandlerId);

  vStreamProc_ProcessingNodeInfoType  processingNodeInfo;
  vStreamProc_ReturnType              inputPortResults[vStreamProcConf_MaxInputPortCount];
  vStreamProc_ReturnType              outputPortResults[vStreamProcConf_MaxOutputPortCount];
  vStreamProc_SignalResponseType      signalHandlerResponse;

  vStreamProc_Stream_InitProcessingNodeInfo(
    processingNodeId,
    inputPortResults,
    outputPortResults,
    &processingNodeInfo);

  /* #10 Prepare SignalInfo. */
  vStreamProc_Scheduler_PrepareSignalInfo(SignalHandlerId, &processingNodeInfo.SignalInfo);

  /* #20 Call signal handler function. */
  signalHandlerResponse =
    vStreamProc_GetSignalHandlerFctOfSignalHandlerFctDef(
      vStreamProc_GetSignalHandlerFctDefIdxOfSignalHandlerDef(
        vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(SignalHandlerId)))(&processingNodeInfo);

  /* #30 Evaluate signal response and port specific results. */
  /* Set output parameter. */
  workDone = vStreamProc_GetSignalResponseFlagValue(signalHandlerResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);
  removeFromSchedule = vStreamProc_GetSignalResponseFlagValue(signalHandlerResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

  /* #40 Trigger actions on acknowledge if needed. */
  if (removeFromSchedule == TRUE)
  {
    switch (processingNodeInfo.SignalInfo.SignalId)
    {
      case (vStreamProcConf_vStreamProcSignal_vStreamProc_InputSnapshotRequested):
      {
        vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(
          processingNodeInfo.ProcessingNodeId,
          (vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(processingNodeInfo.ProcessingNodeId)
            + processingNodeInfo.SignalInfo.InputPortId));
        break;
      }
      case (vStreamProcConf_vStreamProcSignal_vStreamProc_OutputSnapshotRequested):
      {
        vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(
          processingNodeInfo.ProcessingNodeId,
          (vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(processingNodeInfo.ProcessingNodeId)
            + processingNodeInfo.SignalInfo.OutputPortId));
        break;
      }
      case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart):
      case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd):
      {
        retVal = vStreamProc_Stream_DecreasePendingWaveSignalCount(
          &processingNodeInfo);

        if (retVal == VSTREAMPROC_OK)
        {
          vStreamProc_Stream_PerformWaveRouting(
            processingNodeId,
            processingNodeInfo.SignalInfo.InputPortId,
            processingNodeInfo.SignalInfo.SignalId);
        }

        break;
      }
      default:
      {
        /* No snapshot required */
        break;
      }
    }
  }

  /* Translate signal response to vStreamProc_RetVal. */
  if (retVal != VSTREAMPROC_FAILED)
  {
    retVal = vStreamProc_Scheduler_GetRetValBySignalResponse(&signalHandlerResponse, &processingNodeInfo);
  }

  /* #50 Update the return value based on the node context. */
  switch (retVal)
  {
    case VSTREAMPROC_INSUFFICIENT_INPUT:
    {
      /* #60 Check if the signal was WaveEnd and insufficient input was reported at the target port of the signal. */
      if ( (signalId == vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd)
        && (processingNodeInfo.InputPortResults[processingNodeInfo.SignalInfo.InputPortId]
              == VSTREAMPROC_INSUFFICIENT_INPUT))
      {
        /* #70 Set FAILED */
        /* Hint: If a node reports INSUFFICIENT_INPUT during WaveEnd,
                 the result is failed because no data can be provided anymore. */
        retVal = VSTREAMPROC_FAILED;
      }
      /* #80 Check if the node is not directly connected to an entry point. */
      else if (vStreamProc_Scheduler_IsStarvedInputPortConnectedToEntryPoint(&processingNodeInfo) == FALSE)
      {
        /*#90 Set result to OK. */
        /* Hint:Only nodes at the border of the pipe should trigger this return value. Remap to OK result for others.
                If there are other node left in the queue, PENDING will be set as overall result anyway. */
        retVal = VSTREAMPROC_OK;
      }
      else
      {
        /* Nothing to do. */
      }
      break;
    }
    case VSTREAMPROC_INSUFFICIENT_OUTPUT:
    {
      /* Only nodes at the border of the pipe or exit nodes should trigger this return value.
         Remap to OK result for others. */
      if ( (vStreamProc_Scheduler_IsStarvedOutputPortConnectedToExitPoint(&processingNodeInfo) == FALSE)
        && (vStreamProc_Scheduler_IsExitNode(processingNodeId) == FALSE))
      {
        retVal = VSTREAMPROC_OK;
      }
      break;
    }
    default:
    {
      /* Values remain unchanged. */
      break;
    }
  }

  /* Update the output parameter. */
  *WorkDone = workDone;
  *RemoveFromSchedule = removeFromSchedule;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetSignalHandlerState(
  vStreamProc_SignalHandlerIdType SignalHandlerId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SignalIdType signalId =
    vStreamProc_GetSignalIdxOfSignalHandlerDef(
      vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(SignalHandlerId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the new state is allowed for signal. */
  if (vStreamProc_Scheduler_IsSignalHandlerStateAllowed(signalId, NewSignalHandlerState) == TRUE)
  {
    /* #20 Set the new signal handler state if a signal handler was found. */
    if (SignalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
    {
      vStreamProc_SetStateOfSignalHandlerInfo(
        SignalHandlerId,
        NewSignalHandlerState);

      if (NewSignalHandlerState == VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED)
      {
        vStreamProc_Scheduler_DiscardSignalHandler(SignalHandlerId);
      }
    }

    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_DiscardSignalHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Scheduler_DiscardSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_QueueHandleType queueEntryHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if signal handler is already queued. */
  if (vStreamProc_Scheduler_IsSignalHandlerScheduled(SignalHandlerId, &queueEntryHandle) == TRUE)
  {
    /* #20 Remove signal handler from queue. */
    (void)vStreamProc_Queue_Remove(
      vStreamProc_GetAddrSchedulingQueue(vStreamProc_Scheduler_GetQueueIdxOfSignalHandler(SignalHandlerId)),
      queueEntryHandle);

    /* #30 Update external signal info. */
    /* Hint: The deregistered signal handler is treated as if it finished successfully. */
    vStreamProc_Scheduler_UpdateExternalSignalInfo(
      SignalHandlerId,
      VSTREAMPROC_OK);
  }
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_Init(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;
  vStreamProc_ProcessingNodeIterType processingNodeIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the queue of the scheduler. */
  vStreamProc_Queue_Init(
    vStreamProc_GetAddrSchedulingQueue(vStreamProc_GetSchedulingQueueIdxOfPipe(PipeId)),
    vStreamProc_GetAddrSchedulingQueueEntry(vStreamProc_GetSchedulingQueueEntryStartIdxOfPipe(PipeId)),
    vStreamProc_GetSchedulingQueueEntryLengthOfPipe(PipeId),
    VSTREAMPROC_QUEUE_PRIO_ORDER_ASC,
    0u,
    0u);

  /* #20 Initialize the signal info. */
  for ( processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
        processingNodeIdx < vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
        processingNodeIdx++)
  {
    /* #30 Iterate over the processing nodes. */
    vStreamProc_NamedInputPortIterType namedInputPortIdx;
    vStreamProc_NamedOutputPortIterType namedOutputPortIdx;

    /* #40 Init node signals of Processing Node. */
    vStreamProc_Scheduler_InitSignalHandlerInfo(
      vStreamProc_GetSignalHandlerStartIdxOfProcessingNode(processingNodeIdx),
      vStreamProc_GetSignalHandlerEndIdxOfProcessingNode(processingNodeIdx));

    /* #50 Init input port signals. */
    for ( namedInputPortIdx = vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(processingNodeIdx);
          namedInputPortIdx < vStreamProc_GetNamedInputPortEndIdxOfProcessingNode(processingNodeIdx);
          namedInputPortIdx++)
    {
      vStreamProc_Scheduler_InitSignalHandlerInfo(
        vStreamProc_GetSignalHandlerStartIdxOfNamedInputPort(namedInputPortIdx),
        vStreamProc_GetSignalHandlerEndIdxOfNamedInputPort(namedInputPortIdx));
    }

    /* #60 Init output port signals. */
    for ( namedOutputPortIdx = vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(processingNodeIdx);
          namedOutputPortIdx < vStreamProc_GetNamedOutputPortEndIdxOfProcessingNode(processingNodeIdx);
          namedOutputPortIdx++)
    {
      vStreamProc_Scheduler_InitSignalHandlerInfo(
        vStreamProc_GetSignalHandlerStartIdxOfNamedOutputPort(namedOutputPortIdx),
        vStreamProc_GetSignalHandlerEndIdxOfNamedOutputPort(namedOutputPortIdx));
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_Process
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Scheduler_Process(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType                           retVal = VSTREAMPROC_OK;
  vStreamProc_MaxNumberOfNodesToProcessOfPipeType  maxNumOfSignalHandlersToProcess =
    vStreamProc_GetMaxNumberOfNodesToProcessOfPipe(PipeId);
  vStreamProc_QueuePtrType queue =
    vStreamProc_GetAddrSchedulingQueue(vStreamProc_GetSchedulingQueueIdxOfPipe(PipeId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is at least one signal queued. */
  if (!vStreamProc_Queue_IsEmpty(queue))
  {
    vStreamProc_QueueHandleType     queueEntryHandle                  = vStreamProc_Queue_GetFirstUsedHandle(queue);
    vStreamProc_QueueHandleType     firstRescheduledQueueEntryHandle  = VSTREAMPROC_QUEUE_HANDLE_HEAD_USED;
    boolean                         allSignalHandlersBlocked = TRUE;

    /* #20 If yes, iterate over the queue limited by the max number of nodes to process. */
    while (maxNumOfSignalHandlersToProcess > 0u)
    {
      boolean                           workDone = FALSE;
      boolean                           removeFromSchedule = FALSE;
      vStreamProc_ReturnType            currentRetVal;
      vStreamProc_SignalHandlerIdType   signalHandlerId =
        (vStreamProc_SignalHandlerIdType)vStreamProc_Queue_GetEntityHandle(queue, queueEntryHandle);

      /* #30 Evaluate the signal handler. */
      currentRetVal = vStreamProc_Scheduler_EvaluateSignalHandler(signalHandlerId, &workDone, &removeFromSchedule, &allSignalHandlersBlocked);

      /* #40 Update the overall return value if the current return value is worse. */
      if (retVal < currentRetVal)
      {
        retVal = currentRetVal;
      }

      /* #50 Decrease signal handler counter if work was done. */
      if (workDone == TRUE)
      {
        maxNumOfSignalHandlersToProcess--;
      }

      /* #60 Update the signal handler queue. */
      vStreamProc_Scheduler_UpdateSignalHandlerQueue(queue, queueEntryHandle, removeFromSchedule, &firstRescheduledQueueEntryHandle);

      /* #70 Sync Consumers. */
      /* HINT: Must be done after removing current signal from scheduling.
       *       Otherwise the StorageAvailable triggered by the synchronization may get lost.
       */
      vStreamProc_Stream_SyncOutputPortsWaves(vStreamProc_GetProcessingNodeIdxOfSignalHandler(signalHandlerId));

      /* #80 Get first queue entry as next entry. */
      queueEntryHandle = vStreamProc_Queue_GetFirstUsedHandle(queue);

      /* #90 Check if the loop has to be aborted. Possible reasons are:
             1. The queue is empty.
             2. The first rescheduled signal is reached.
             3. A fatal error occurred. */
      if (  (queueEntryHandle == VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
         || (queueEntryHandle == firstRescheduledQueueEntryHandle)
         || (retVal == VSTREAMPROC_FAILED))
      {
        break;
      }
    }

    /* #100 Check if the highest priority in queue consists only of blocked signal handlers. */
    if (allSignalHandlersBlocked == TRUE)
    {
      /* Report blocked state, because there is external interaction required to unblock these signal handlers. */
      retVal = VSTREAMPROC_BLOCKED;
    }
  }

  /* #110 Return pending if there are still queued signals. */
  if (retVal == VSTREAMPROC_OK)
  {
    if (!vStreamProc_Queue_IsEmpty(queue))
    {
      /* If there are still nodes to process return pending. */
      retVal = VSTREAMPROC_PENDING;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetNodeSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetNodeSignal(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  vStreamProc_SignalHandlerIdType signalHandlerId;
  vStreamProc_SignalHandlerIdType signalHandlerStartId =
    (vStreamProc_SignalHandlerIdType)vStreamProc_GetSignalHandlerStartIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_SignalHandlerIdType signalHandlerEndId =
    (vStreamProc_SignalHandlerIdType)vStreamProc_GetSignalHandlerEndIdxOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  signalHandlerId = vStreamProc_Scheduler_GetSignalHandlerId(
      signalHandlerStartId,
      signalHandlerEndId,
      SignalId);

  /* #10 Check if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    /* #20 If yes, queue the signal handler. */
    retVal = vStreamProc_Scheduler_QueueSignalHandler(signalHandlerId);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetInputPortSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetInputPortSignal(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  vStreamProc_SignalHandlerIdType signalHandlerId;
  vStreamProc_SignalHandlerIdType signalHandlerStartId =
    (vStreamProc_SignalHandlerIdType)vStreamProc_GetSignalHandlerStartIdxOfNamedInputPort(NamedInputPortId);
  vStreamProc_SignalHandlerIdType signalHandlerEndId =
    (vStreamProc_SignalHandlerIdType)vStreamProc_GetSignalHandlerEndIdxOfNamedInputPort(NamedInputPortId);

  /* ----- Implementation ----------------------------------------------- */
  signalHandlerId = vStreamProc_Scheduler_GetSignalHandlerId(
    signalHandlerStartId,
    signalHandlerEndId,
    SignalId);

  /* #10 Check if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    /* #20 If yes, queue the signal handler. */
    retVal = vStreamProc_Scheduler_QueueSignalHandler(signalHandlerId);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetOutputPortSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetOutputPortSignal(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  vStreamProc_SignalHandlerIdType signalHandlerId;
  vStreamProc_SignalHandlerIdType signalHandlerStartId =
    vStreamProc_GetSignalHandlerStartIdxOfNamedOutputPort(NamedOutputPortId);
  vStreamProc_SignalHandlerIdType signalHandlerEndId =
    vStreamProc_GetSignalHandlerEndIdxOfNamedOutputPort(NamedOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  signalHandlerId = vStreamProc_Scheduler_GetSignalHandlerId(
    signalHandlerStartId,
    signalHandlerEndId,
    SignalId);

  /* #10 Check if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    /* #20 If yes, queue the signal handler. */
    retVal = vStreamProc_Scheduler_QueueSignalHandler(signalHandlerId);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_QueueSignalHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, AUTOMATIC) vStreamProc_Scheduler_QueueSignalHandler(
  vStreamProc_SignalHandlerIdType SignalHandlerId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(
    vStreamProc_GetProcessingNodeIdxOfSignalHandler(SignalHandlerId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ignore any signal except cancel in cancel phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(pipeId) == VSTREAMPROC_CANCEL_PHASEOFPIPEINFO)
    && (SignalHandlerId != vStreamProcConf_vStreamProcSignal_vStreamProc_Cancel))
  {
    retVal = E_OK;
  }
  else
  {
    /* #20 Check if signal handler can and has to be queued. */
    if (vStreamProc_GetStateOfSignalHandlerInfo(SignalHandlerId) != VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED)
    {
      if (vStreamProc_Scheduler_IsSignalHandlerScheduled(SignalHandlerId, NULL_PTR) == FALSE)
      {
        /* #30 If yes, add signal entry. */
        vStreamProc_QueuePtrType queue =
          vStreamProc_GetAddrSchedulingQueue(vStreamProc_GetSchedulingQueueIdxOfPipe(pipeId));

        vStreamProc_QueueHandleType firstFreeHandle = vStreamProc_Queue_GetFirstFreeHandle(queue);

        /* #40 Check if there is space left in queue. */
        if (!vStreamProc_Queue_IsFull(queue))
        {
          /* #50 Add signal handler to queue. */
          vStreamProc_Queue_SetEntityHandle(queue, firstFreeHandle, SignalHandlerId);

          /* #60 Add the signal entry to the queue.*/
          (void)vStreamProc_Queue_PrioInsert(
            queue,
            vStreamProc_GetSignalPriorityOfSignal(vStreamProc_GetSignalIdxOfSignalHandlerDef(vStreamProc_GetSignalHandlerDefIdxOfSignalHandler(SignalHandlerId))));
        }
      }

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetNodeSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetNodeSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SignalHandlerIdType signalHandlerId =
    vStreamProc_Scheduler_GetSignalHandlerId(
      vStreamProc_GetSignalHandlerStartIdxOfProcessingNode(ProcessingNodeId),
      vStreamProc_GetSignalHandlerEndIdxOfProcessingNode(ProcessingNodeId),
      SignalId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the new signal handler state if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    retVal = vStreamProc_Scheduler_SetSignalHandlerState(signalHandlerId, NewSignalHandlerState);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetInputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetInputPortSignalHandlerState(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SignalHandlerIdType signalHandlerId =
    vStreamProc_Scheduler_GetSignalHandlerId(
      vStreamProc_GetSignalHandlerStartIdxOfNamedInputPort(NamedInputPortId),
      vStreamProc_GetSignalHandlerEndIdxOfNamedInputPort(NamedInputPortId),
      SignalId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the new signal handler state if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    retVal = vStreamProc_Scheduler_SetSignalHandlerState(signalHandlerId, NewSignalHandlerState);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_SetOutputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Scheduler_SetOutputPortSignalHandlerState(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_SignalHandlerIdType signalHandlerId =
    vStreamProc_Scheduler_GetSignalHandlerId(
      vStreamProc_GetSignalHandlerStartIdxOfNamedOutputPort(NamedOutputPortId),
      vStreamProc_GetSignalHandlerEndIdxOfNamedOutputPort(NamedOutputPortId),
      SignalId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the new signal handler state if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    retVal = vStreamProc_Scheduler_SetSignalHandlerState(signalHandlerId, NewSignalHandlerState);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Scheduler_DiscardInputPortSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Scheduler_DiscardInputPortSignal(
  vStreamProc_NamedInputPortIdType NamedInputPortId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalHandlerIdType signalHandlerId =
    vStreamProc_Scheduler_GetSignalHandlerId(
      vStreamProc_GetSignalHandlerStartIdxOfNamedInputPort(NamedInputPortId),
      vStreamProc_GetSignalHandlerEndIdxOfNamedInputPort(NamedInputPortId),
      SignalId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Discard the signal handler if a signal handler was found. */
  if (signalHandlerId != VSTREAMPROC_NO_SIGNALHANDLER)
  {
    vStreamProc_Scheduler_DiscardSignalHandler(signalHandlerId);
  }
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Scheduler.c
 *********************************************************************************************************************/
