/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_AccessNode.h
 *        \brief  vStreamProc access node header file
 *
 *      \details  Definition of all relevant interfaces for vStreamProc access nodes
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_ACCESSNODE_H
#define VSTREAMPROC_ACCESSNODE_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef struct
{
  vStreamProc_ProduceDataFctPtrType     ProduceCbk;
  boolean                               IsLocked;
}vStreamProc_EntryNode_WorkspaceType;

typedef struct
{
  vStreamProc_ConsumeDataFctPtrType     ConsumeCbk;
  boolean                               IsLocked;
}vStreamProc_ExitNode_WorkspaceType;

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
   GENERIC API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_AnnounceStream()
 *********************************************************************************************************************/
/*!
 *  \brief          This function prepares the entry node for an input stream.
 *  \details        -
 *  \param[in]      EntryPointId              Id of the entry point.
 *  \param[in]      ProduceCbk                Callback function which shall be called when input buffer is available.
 *  \return         E_OK                      The stream was set up successfully.
 *  \return         E_NOT_OK                  The stream was not set due to an error.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_AccessNode_AnnounceStream(
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_ProduceCallbackType ProduceCbk);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_ResetPendingInputStream
 *********************************************************************************************************************/
/*!
 *  \brief          Resets a pending input stream at the provided entry point if set.
 *  \details        -
 *  \param[in]      EntryPointId        Id of the entry point.
 *  \param[in]      ForceUnlock         Defines if the pending stream is reset even if the entry point is locked.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_ResetPendingInputStream(
  vStreamProc_EntryPointIdType EntryPointId,
  boolean ForceUnlock);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsInputStreamPending()
 *********************************************************************************************************************/
/*!
 *  \brief          This function provides the info if an input stream is active at the provided entry point.
 *  \details        -
 *  \param[in]      EntryPointId      Id of the entry point.
 *  \return         TRUE              There is an active stream at this access node.
 *  \return         FALSE             There is no active stream at this access node.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsInputStreamPending(
  vStreamProc_EntryPointIdType EntryPointId);


/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetEntryNodeLock()
 *********************************************************************************************************************/
/*!
 *  \brief          Sets the lock for the entry access node of the entry point to the provided value.
 *  \details        -
 *  \param[in]      EntryPointId      Id of the entry point.
 *  \param[in]      IsLocked          Value of the lock.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetEntryNodeLock(
  vStreamProc_EntryPointIdType EntryPointId,
  boolean IsLocked);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsEntryNodeLocked()
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if the lock for the entry access node of the entry point is set.
 *  \details        -
 *  \param[in]      EntryPointId      Id of the entry point.
 *  \return         TRUE    Lock is set
 *  \return         FALSE   Lock is not set
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsEntryNodeLocked(
  vStreamProc_EntryPointIdType EntryPointId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsEntryNode()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided processing node is an entry node.
 * \details       -
 * \param[in]     ProcessingNodeId        Id of the processing node.
 * \return        TRUE                    The processing node is an entry node.
 * \return        TRUE                    The processing node is not an entry node.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsEntryNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_GetEntryPointIdOfEntryNode()
 *********************************************************************************************************************/
/*! \brief        Returns the entry point id of the provided entry node.
 *  \details    -
 *  \param[in]    ProcessingNodeId        Id of the entry processing node.
 *  \return       The Id of the entry point which references the entry node.
 *                If no entry point was found, VSTREAMPROC_NO_ENTRYPOINT is returned.
 *  \pre        -
 *  \context      TASK
 *  \reentrant    TRUE
 *  \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_EntryPointIdType, VSTREAMPROC_CODE) vStreamProc_AccessNode_GetEntryPointIdOfEntryNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_RequestStream()
 *********************************************************************************************************************/
/*!
 *  \brief          This function prepares the exit node for an output stream.
 *  \details        -
 *  \param[in]      ExitPointId               Id of the exit point where the stream is requested.
 *  \param[in]      ConsumeCbk                Callback function which shall be called when output data is available.
 *  \return         E_OK                      The stream was set up successfully.
 *  \return         E_NOT_OK                  The stream was not set due to an error.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_AccessNode_RequestStream(
  vStreamProc_ExitPointIdType ExitPointId,
  vStreamProc_ConsumeCallbackType ConsumeCbk);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_ResetPendingOutputStream
 *********************************************************************************************************************/
/*!
 *  \brief          Resets a pending output stream at the provided exit point if set.
 *  \details        -
 *  \param[in]      ExitPointId         Id of the exit point.
 *  \param[in]      ForceUnlock         Defines if the pending stream is reset even if the entry point is locked.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_ResetPendingOutputStream(
  vStreamProc_ExitPointIdType ExitPointId,
  boolean ForceUnlock);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsOutputStreamPending()
 *********************************************************************************************************************/
/*!
 *  \brief          This function provides the info if an output stream is active at the provided exit point.
 *  \details        -
 *  \param[in]      ExitPointId       Id of the exit point.
 *  \return         TRUE              There is an active stream at this access node.
 *  \return         FALSE             There is no active stream at this access node.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsOutputStreamPending(
  vStreamProc_ExitPointIdType ExitPointId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetExitNodeLock()
 *********************************************************************************************************************/
/*!
 *  \brief          Sets the lock for the exit access node of the exit point to the provided value.
 *  \details        -
 *  \param[in]      ExitPointId       Id of the exit point.
 *  \param[in]      IsLocked          Value of the lock.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetExitNodeLock(
  vStreamProc_ExitPointIdType ExitPointId,
  boolean IsLocked);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsExitNodeLocked()
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if the lock for the exit access node of the exit point is set.
 *  \details        -
 *  \param[in]      ExitPointId       Id of the exit point.
 *  \return         TRUE    Lock is set
 *  \return         FALSE   Lock is not set
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsExitNodeLocked(
  vStreamProc_ExitPointIdType ExitPointId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsExitNode()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided processing node is an exit node.
 * \details       -
 * \param[in]     ProcessingNodeId        Id of the processing node.
 * \return        TRUE                    The processing node is an exit node.
 * \return        TRUE                    The processing node is not an exit node.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsExitNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_GetExitPointIdOfExitNode()
 *********************************************************************************************************************/
/*! \brief        Returns the exit point id of the provided exit node.
 *  \details    -
 *  \param[in]    ProcessingNodeId        Id of the exit processing node.
 *  \return       The Id of the exit point which references the exit node.
 *                If no exit point was found, VSTREAMPROC_NO_EXITPOINT is returned.
 *  \pre        -
 *  \context      TASK
 *  \reentrant    TRUE
 *  \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ExitPointIdType, VSTREAMPROC_CODE) vStreamProc_AccessNode_GetExitPointIdOfExitNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
   ENTRY POINT NODE API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_EntryNode_Init()
 *********************************************************************************************************************/
/*!
 *  \brief          Initializes the passed node.
 *  \details        -
 *  \param[in]      ProcNodeInfo              The processing node information to operate on.
 *  \return         E_OK                      Initialization was successful
 *  \return         E_NOT_OK                  Initialization was unsuccessful
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-211271
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_EntryNode_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_EntryNode_SignalHandler_HandleInputStream()
 *********************************************************************************************************************/
/*!
 *  \brief          Checks for available buffer and handles the pending input stream.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_EntryNode_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_EntryNode_SignalHandler_HandleInputStream(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
   EXIT POINT NODE API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ExitNode_Init()
 *********************************************************************************************************************/
/*!
 *  \brief          Initializes the passed node.
 *  \details        -
 *  \param[in]      ProcNodeInfo              The processing node information to operate on.
 *  \return         E_OK                      Initialization was successful
 *  \return         E_NOT_OK                  Initialization was unsuccessful
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-211271
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ExitNode_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ExitNode_SignalHandler_HandleOutputStream()
 *********************************************************************************************************************/
/*!
 *  \brief          Checks for available data and handles the pending output stream.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ExitNode_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ExitNode_SignalHandler_HandleOutputStream(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);


/**********************************************************************************************************************
 *  vStreamProc_ExitNode_SignalHandler_WaveEnd
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if there is still data available at the exit point.
 *  \details        If there is still data, INSUFFICIENT_OUTPUT is returned at the input port and the signal is
 *                  not acknowledged until all data is consumed. Otherwise, the signal is acknowledged.
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            vStreamProc_ExitNode_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ExitNode_SignalHandler_WaveEnd(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_ACCESSNODE_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_AccessNode.h
 *********************************************************************************************************************/

