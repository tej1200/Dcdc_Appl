/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_AccessNode.c
 *        \brief  vStreamProc access node specific source code file
 *
 *      \details  Implementation of the special access processing nodes at the entry and exit points of the pipe.
 *                The access nodes are used to handle input and output data streams to the pipe.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_ACCESSNODE_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_AccessNode.h"
#include "vStreamProc.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc_Pipe.h"
#include "vStreamProc_Stream.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef enum
{
  VSTREAMPROC_ACCESSNODE_ENTRY_STATE_PENDING,
  VSTREAMPROC_ACCESSNODE_ENTRY_STATE_STORAGE_AVAILABLE_AWAIT_DATA,
  VSTREAMPROC_ACCESSNODE_ENTRY_STATE_DATA_AVAILABLE_AWAIT_STORAGE,
  VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM
}vStreamProc_AccessNode_EntryStateType;

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
typedef enum
{
  VSTREAMPROC_ACCESSNODE_EXIT_STATE_PENDING,
  VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_AVAILABLE_AWAIT_CONSUME,
  VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_CONSUMED_AWAIT_DATA,
  VSTREAMPROC_ACCESSNODE_EXIT_STATE_FINALIZE_STREAM
}vStreamProc_AccessNode_ExitStateType;
#endif

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetNextEntryState()
 *********************************************************************************************************************/
/*! \brief      Helper function, which sets the next entry state.
 *  \details    -
 *  \param[in]  ProcNodeInfo            The processing node information to operate on.
 *  \param[in]  NextState               State, which shall be set.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetNextEntryState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_AccessNode_EntryStateType NextState);

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetNextExitState()
 *********************************************************************************************************************/
/*! \brief      Helper function, which sets the next exit state.
 *  \details    -
 *  \param[in]  ProcNodeInfo            The processing node information to operate on.
 *  \param[in]  NextState               State, which shall be set.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetNextExitState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_AccessNode_ExitStateType NextState);
#endif

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_CallProduceCallback()
 *********************************************************************************************************************/
/*! \brief      Calls the produce callback of the entry node and evaluates the return value.
 *  \details    The signal response is calculated based on the produce callback return value.
 *  \param[in,out]  EntryPointInfo    Pointer to the entry point info structure which shall be provided to
 *                                    the produce callback.
 *  \param[in,out]  Workspace         Pointer to the workspace of the entry node.
 *  \param[out]     OutputPortResult  Pointer to the output port result of the entry node.
 *  \param[out]     NextState         Pointer to the next state of the entry node .
 *  \return         The calculated signal response.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_AccessNode_CallProduceCallback(
  P2VAR(vStreamProc_EntryPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) EntryPointInfo,
  P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Workspace,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortResult,
  P2VAR(vStreamProc_AccessNode_EntryStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) NextState);

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_CallConsumeCallback()
 *********************************************************************************************************************/
/*! \brief      Calls the consume callback of the exit node and evaluates the return value.
 *  \details    The signal response is calculated based on the consume callback return value.
 *  \param[in,out]  ExitPointInfo     Pointer to the exit point info structure which shall be provided to
 *                                    the consume callback.
 *  \param[in,out]  Workspace         Pointer to the workspace of the exit node.
 *  \param[out]     InputPortResult   Pointer to the input port result of the exit node.
 *  \param[out]     NextState         Pointer to the next state of the exit node .
 *  \return         The calculated signal response.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_AccessNode_CallConsumeCallback(
  P2VAR(vStreamProc_ExitPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ExitPointInfo,
  P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Workspace,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortResult,
  P2VAR(vStreamProc_AccessNode_ExitStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) NextState);
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetNextEntryState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetNextEntryState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_AccessNode_EntryStateType NextState)
{
  vStreamProc_SignalHandlerStateType portSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED;
  vStreamProc_SignalHandlerStateType nodeSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED;
  boolean updateSignalHandlerConfig = TRUE;

  /* #10 Calculate the signal handler states based on the next state. */
  switch (NextState)
  {
    case VSTREAMPROC_ACCESSNODE_ENTRY_STATE_PENDING:
    {
      /* If next state is pending, the signal handler is queued again,
         independently which one is currently executed.
         No change of signal handler configuration necessary. */
      updateSignalHandlerConfig = FALSE;
      break;
    }
    case VSTREAMPROC_ACCESSNODE_ENTRY_STATE_STORAGE_AVAILABLE_AWAIT_DATA:
    {
      /* Register the StreamAnnounced signal handler. */
      nodeSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
      break;
    }
    case VSTREAMPROC_ACCESSNODE_ENTRY_STATE_DATA_AVAILABLE_AWAIT_STORAGE:
    {
      /* Register the StorageAvailable signal handler. */
      portSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
      break;
    }
    default:
    {
      /* VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM */
      /* Unregister both signal handlers. */
      break;
    }
  }

  /* #20 Set the signal handler states if necessary. */
  if (updateSignalHandlerConfig == TRUE)
  {
    (void)vStreamProc_SetOutputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      portSignalHandlerState);
    (void)vStreamProc_SetNodeSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StreamAnnounced,
      nodeSignalHandlerState);
  }
}

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetNextExitState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetNextExitState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_AccessNode_ExitStateType NextState)
{
  vStreamProc_SignalHandlerStateType portSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED;
  vStreamProc_SignalHandlerStateType nodeSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED;
  boolean updateSignalHandlerConfig = TRUE;

  /* #10 Calculate the signal handler states based on the next state. */
  switch (NextState)
  {
    case VSTREAMPROC_ACCESSNODE_EXIT_STATE_PENDING:
    { /* If next state is pending, the signal handler is queued again,
         independently which one is currently executed.
         No change of signal handler configuration necessary.*/
      updateSignalHandlerConfig = FALSE;
      break;
    }
    case VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_AVAILABLE_AWAIT_CONSUME:
    {
      /* Register the StreamRequested signal handler. */
      nodeSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
      break;
    }
    case VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_CONSUMED_AWAIT_DATA:
    {
      /* Register the DataAvailable signal handler. */
      portSignalHandlerState = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
      break;
    }
    default:
    {
      /* VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM */
      /* Unregister both signal handlers. */
      break;
    }
  }

  /* #20 Set the signal handler states. */
  if (updateSignalHandlerConfig == TRUE)
  {
    (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      portSignalHandlerState);
    (void)vStreamProc_SetNodeSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StreamRequested,
      nodeSignalHandlerState);
  }
}
#endif

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_CallProduceCallback()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_AccessNode_CallProduceCallback(
  P2VAR(vStreamProc_EntryPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) EntryPointInfo,
  P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Workspace,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortResult,
  P2VAR(vStreamProc_AccessNode_EntryStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) NextState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_CONST) storageInfo =
    &EntryPointInfo->WriteRequest.StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Execute user callback and evaluate return value. */
  switch (Workspace->ProduceCbk(EntryPointInfo))
  {
    case VSTREAMPROC_OK:
    {
      /* #20 If the stream is finished: Switch off the signal handlers. */
      Workspace->ProduceCbk = NULL_PTR;
      *NextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM;

      /* Hint: It is not allowed to keep the buffer when the stream is finished. */
      if (storageInfo->ReleaseFlag == TRUE)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    case VSTREAMPROC_INSUFFICIENT_OUTPUT:
    {
      /* #30 If the stream needs more buffer: Wait for a StorageAvailable signal. */
      *NextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_DATA_AVAILABLE_AWAIT_STORAGE;
      *OutputPortResult = VSTREAMPROC_INSUFFICIENT_OUTPUT;

      /* Hint: It is not allowed to keep the buffer when the signal handler is unqueued. */
      if (storageInfo->ReleaseFlag == TRUE)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    case VSTREAMPROC_PENDING:
    {
      /* #40 If the stream did not produce all data: Queue the signal handler again. */
      if (storageInfo->RequestLength != storageInfo->AvailableLength)
      {
        /* There is still buffer available, just report busy to queue the signal handler again. */
        *NextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_PENDING;
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_BUSY;
      }
      else
      {
        /* Buffer is used completely, acknowledge the signal and wait for a storage available signal. */
        *NextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_DATA_AVAILABLE_AWAIT_STORAGE;
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    default:
    {
      /* #50 Otherwise return failed. */
      /* retVal already set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
      break;
    }
  }

  /* #60 If data was produced, set the work done flag. */
  if (storageInfo->RequestLength > 0u)
  {
    if (storageInfo->ReleaseFlag == FALSE)
    {
      /* Release flag FALSE with produced data is currently not supported */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
    }
    else
    {
      /* Ensure that workdone flag is set. */
      vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);
    }
  }

  return signalResponse;
}

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_CallConsumeCallback()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_AccessNode_CallConsumeCallback(
  P2VAR(vStreamProc_ExitPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ExitPointInfo,
  P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Workspace,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortResult,
  P2VAR(vStreamProc_AccessNode_ExitStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) NextState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_CONST) storageInfo = &ExitPointInfo->ReadRequest.StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Execute the user callback and evaluate return value. */
  switch (Workspace->ConsumeCbk(ExitPointInfo))
  {
    /* #20 If the stream is finished: Switch off the signal handlers. */
    case VSTREAMPROC_OK:
    {
      Workspace->ConsumeCbk = NULL_PTR;
      *NextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_FINALIZE_STREAM;

      /* Hint: It is not allowed to keep the buffer when the stream is finished. */
      if (storageInfo->ReleaseFlag == TRUE)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    /* #30 If the stream needs more data: Wait for a DataAvailable signal. */
    case VSTREAMPROC_INSUFFICIENT_INPUT:
    {
      *NextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_CONSUMED_AWAIT_DATA;
      *InputPortResult = VSTREAMPROC_INSUFFICIENT_INPUT;

      /* Hint: It is not allowed to keep the buffer when the signal handler is unqueued. */
      if (storageInfo->ReleaseFlag == TRUE)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    /* #40 If the stream did not consume all data: Queue the signal handler again. */
    case VSTREAMPROC_PENDING:
    {
      if (storageInfo->RequestLength != storageInfo->AvailableLength)
      {
        /* There is data left, just report busy to queue the signal handler again. */
        *NextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_PENDING;
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_BUSY;
      }
      else
      {
        /* All data is consumed. Acknowledge the signal and wait for a data available signal. */
        *NextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_CONSUMED_AWAIT_DATA;
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
      break;
    }
    /* #50 Otherwise return failed. */
    default:
    {
      /* retVal already set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
      break;
    }
  }

  /* #60 If data was consumed, set the work done flag. */
  if (storageInfo->RequestLength > 0u)
  {
    if (storageInfo->ReleaseFlag == FALSE)
    {
      /* Release flag FALSE with produced data is currently not supported */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
    }
    else
    {
      /* Ensure that workdone flag is set. */
      vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);
    }
  }

  return signalResponse;
}
#endif

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
   GENERIC API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_AccessNode_AnnounceStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_AccessNode_AnnounceStream(
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_ProduceCallbackType ProduceCbk)
{
  Std_ReturnType retVal = E_NOT_OK;
  vStreamProc_EntryNode_WorkspaceType* workspace;
  vStreamProc_ProcessingNodeInfoType procNodeInfo;
  vStreamProc_ReturnType outputPortResults;

  vStreamProc_Stream_InitProcessingNodeInfo(
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId)),
    NULL_PTR,
    &outputPortResults,
    &procNodeInfo);

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_EntryNode_WorkspaceType(&procNodeInfo);         /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  if ( (workspace->ProduceCbk == NULL_PTR)
    && (workspace->IsLocked == FALSE))
  {
    vStreamProc_ReturnType                currentResult   = VSTREAMPROC_FAILED;
    vStreamProc_OutputPortInfoType        outputPortInfo;
    vStreamProc_AccessNode_EntryStateType nextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM;

    /* #10 Store produce callback if no other stream is active. */
    workspace->ProduceCbk   = ProduceCbk;

    /* #20 Check for available buffer. Request at least one data element. */
    /* HINT: Return value check omitted, because it may never fail here. */
    (void)vStreamProc_PrepareOutputPortInfo(&procNodeInfo, vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output, &outputPortInfo);
    outputPortInfo.WriteRequest.StorageInfo.RequestLength = 1;
    currentResult = vStreamProc_GetOutputPortInfo(&procNodeInfo, vStreamProcConf_vStreamProcDataType_Undefined, &outputPortInfo);

    /* #30 Handle the result. */
    switch (currentResult)
    {
      /* #40 If a buffer for at least one element is available, wait for the stream announced signal. */
      case VSTREAMPROC_OK:
      {
        nextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_STORAGE_AVAILABLE_AWAIT_DATA;
        retVal = E_OK;
        break;
      }
      /* #50 If there is no free buffer, wait for the storage available signal. */
      case VSTREAMPROC_INSUFFICIENT_OUTPUT:
      {
        nextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_DATA_AVAILABLE_AWAIT_STORAGE;
        retVal = E_OK;
        break;
      }
      /* #60 Otherwise, return E_NOT_OK. */
      default:
      {
        /* VSTREAMPROC_PENDING, VSTREAMPROC_INSUFFICIENT_INPUT, VSTREAMPROC_FAILED */
        /* retVal already set to E_NOT_OK. */
        break;
      }
    }

    vStreamProc_AccessNode_SetNextEntryState(&procNodeInfo, nextState);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_ResetPendingInputStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_ResetPendingInputStream(
  vStreamProc_EntryPointIdType EntryPointId,
  boolean ForceUnlock)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId)));

  /* #10 Reset the produce callback. */
  if ((workspace->IsLocked == FALSE) || (ForceUnlock == TRUE))
  {
    workspace->ProduceCbk = NULL_PTR;
  }
}


/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsInputStreamPending()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsInputStreamPending(
  vStreamProc_EntryPointIdType EntryPointId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  P2CONST(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2CONST(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId)));

   /* #10 Check if a produce callback is set. */
  if (workspace->ProduceCbk != NULL_PTR)
  {
    retVal = TRUE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetEntryNodeLock()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetEntryNodeLock(
  vStreamProc_EntryPointIdType EntryPointId,
  boolean IsLocked)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId)));

  /* #10 Set the lock value. */
  workspace->IsLocked = IsLocked;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsEntryNodeLocked()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsEntryNodeLocked(
  vStreamProc_EntryPointIdType EntryPointId)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2CONST(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(EntryPointId)));

  /* #10 Return the lock value. */
  return workspace->IsLocked;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsEntryNode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsEntryNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
    /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_ScopeOfProcessingNodeClassDefType processingNodeScope =
    vStreamProc_GetScopeOfProcessingNodeClassDef(
      vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
        vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)));
  vStreamProc_NamedInputPortIdType namedInputPortCount =
    vStreamProc_GetNamedInputPortLengthOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the node is an access node and if it has no input ports. */
  if ((processingNodeScope == VSTREAMPROC_EXTERNAL_SCOPE_SCOPEOFPROCESSINGNODECLASSDEF)
    && (namedInputPortCount == 0u))
  {
    retVal = TRUE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_GetEntryPointIdOfEntryNode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_EntryPointIdType, VSTREAMPROC_CODE) vStreamProc_AccessNode_GetEntryPointIdOfEntryNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  vStreamProc_EntryPointIdType entryPointId = VSTREAMPROC_NO_ENTRYPOINT;
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_EntryPointIterType entryPointIdx;

  /* #10 Iterate over the entry points and check the referenced processing node. */
  for (entryPointIdx = vStreamProc_GetEntryPointStartIdxOfPipe(pipeId);
       entryPointIdx < vStreamProc_GetEntryPointEndIdxOfPipe(pipeId);
       entryPointIdx++)
  {
    if (vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
         vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointIdx)) == ProcessingNodeId)
    {
      /* #20 If the entry point was found, break the loop. */
      entryPointId = (vStreamProc_EntryPointIdType)entryPointIdx;
      break;
    }
  }

  return entryPointId;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_RequestStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_AccessNode_RequestStream(
  vStreamProc_ExitPointIdType ExitPointId,
  vStreamProc_ConsumeCallbackType ConsumeCbk)
{
  Std_ReturnType retVal = E_NOT_OK;

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_ExitNode_WorkspaceType *workspace;
  vStreamProc_ProcessingNodeInfoType procNodeInfo;
  vStreamProc_ReturnType inputPortResults;

  vStreamProc_Stream_InitProcessingNodeInfo(
    vStreamProc_GetProcessingNodeIdxOfNamedInputPort(vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId)),
      &inputPortResults,
      NULL_PTR,
      &procNodeInfo);

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ExitNode_WorkspaceType(&procNodeInfo);          /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Ensure that callback buffer is free to use. */
  if ( (workspace->ConsumeCbk == NULL_PTR)
    && (workspace->IsLocked == FALSE))
  {
    vStreamProc_ReturnType                currentResult = VSTREAMPROC_FAILED;
    vStreamProc_InputPortInfoType         inputPortInfo;
    vStreamProc_AccessNode_ExitStateType  nextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_FINALIZE_STREAM;

    /* #10 Store consume callback if no other stream is active. */
    workspace->ConsumeCbk   = ConsumeCbk;

    /* #20 Check for available buffer. Request at least one data element.*/
    /* HINT: This may fail when the input port is currently not connected. */
    if (vStreamProc_PrepareInputPortInfo(&procNodeInfo, vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input, &inputPortInfo) == VSTREAMPROC_OK)
    {
      if (inputPortInfo.IsConnected == TRUE)
      {
        inputPortInfo.ReadRequest.StorageInfo.RequestLength = 1;
        currentResult = vStreamProc_GetInputPortInfo(&procNodeInfo, vStreamProcConf_vStreamProcDataType_Undefined, &inputPortInfo);
      }
    }

    /* #30 Handle the result. */
    switch (currentResult)
    {
      /* #40 If at least one data element is available, wait for the stream requested signal. */
      case VSTREAMPROC_OK:
      {
        nextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_AVAILABLE_AWAIT_CONSUME;
        retVal = E_OK;
        break;
      }
      /* #50 If there is no data, wait for the data available signal. */
      case VSTREAMPROC_INSUFFICIENT_INPUT:
      {
        nextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_DATA_CONSUMED_AWAIT_DATA;
        retVal = E_OK;
        break;
      }
      /* #60 Otherwise return E_NOT_OK. */
      default:
      {
        /* VSTREAMPROC_PENDING, VSTREAMPROC_INSUFFICIENT_OUTPUT, VSTREAMPROC_FAILED */
        /* retVal already set to E_NOT_OK. */
        break;
      }
    }

    vStreamProc_AccessNode_SetNextExitState(&procNodeInfo, nextState);
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointId);
  VSTREAMPROC_DUMMY_STATEMENT(ConsumeCbk);
#endif

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_ResetPendingOutputStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_ResetPendingOutputStream(
  vStreamProc_ExitPointIdType ExitPointId,
  boolean ForceUnlock)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId)));

  /* #10 Reset the produce callback. */
  if ((workspace->IsLocked == FALSE) || (ForceUnlock == TRUE))
  {
    workspace->ConsumeCbk = NULL_PTR;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsOutputStreamPending()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsOutputStreamPending(
  vStreamProc_ExitPointIdType ExitPointId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  P2CONST(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2CONST(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                            /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId)));

  /* #10 Check if a consume callback is set. */
  if (workspace->ConsumeCbk != NULL_PTR)
  {
    retVal = TRUE;
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointId);
#endif
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_SetExitNodeLock()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_AccessNode_SetExitNodeLock(
  vStreamProc_ExitPointIdType ExitPointId,
  boolean IsLocked)
{
  /* ----- Local Variables ---------------------------------------------- */
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId)));

  /* #10 Set the lock value. */
  workspace->IsLocked = IsLocked;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointId);
  VSTREAMPROC_DUMMY_STATEMENT(IsLocked);
#endif
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsExitNodeLocked()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsExitNodeLocked(
  vStreamProc_ExitPointIdType ExitPointId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  P2CONST(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  workspace = (P2CONST(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR))                            /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_GetWorkspaceOfProcessingNode(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId)));

  retVal = workspace->IsLocked;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointId);
#endif

  /* #10 Return the lock value. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AccessNode_IsExitNode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_AccessNode_IsExitNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
    /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_ScopeOfProcessingNodeClassDefType processingNodeScope =
    vStreamProc_GetScopeOfProcessingNodeClassDef(
      vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
        vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)));
  vStreamProc_NamedOutputPortIdType namedOutputPortCount =
    vStreamProc_GetNamedOutputPortLengthOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the node is an access node and if it has no output ports. */
  if ( (processingNodeScope == VSTREAMPROC_EXTERNAL_SCOPE_SCOPEOFPROCESSINGNODECLASSDEF)
    && (namedOutputPortCount == 0u))
  {
    retVal = TRUE;
  }

  return retVal;
}


/**********************************************************************************************************************
 *  vStreamProc_AccessNode_GetExitPointIdOfExitNode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ExitPointIdType, VSTREAMPROC_CODE) vStreamProc_AccessNode_GetExitPointIdOfExitNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  vStreamProc_ExitPointIdType exitPointId = VSTREAMPROC_NO_EXITPOINT;

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_ExitPointIterType exitPointIdx;

  /* #10 Iterate over the exit points and check the referenced processing node. */
  for (exitPointIdx = vStreamProc_GetExitPointStartIdxOfPipe(pipeId);
       exitPointIdx < vStreamProc_GetExitPointEndIdxOfPipe(pipeId);
       exitPointIdx++)
  {
    if (vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
      vStreamProc_GetNamedInputPortIdxOfExitPoint(exitPointIdx)) == ProcessingNodeId)
    {
      /* #20 If the exit point was found, break the loop. */
      exitPointId = (vStreamProc_EntryPointIdType)exitPointIdx;
      break;
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcessingNodeId);
#endif

  return exitPointId;
}

/**********************************************************************************************************************
   ENTRY NODE API
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_EntryNode_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_EntryNode_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  vStreamProc_EntryNode_WorkspaceType *workspace;

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_EntryNode_WorkspaceType(ProcNodeInfo);          /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize callback to "unused". */
  workspace->ProduceCbk = NULL_PTR;

  /* #20 Initialize the locked state. */
  workspace->IsLocked = FALSE;

  return E_OK;
}

/**********************************************************************************************************************
 *  vStreamProc_EntryNode_SignalHandler_HandleInputStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_EntryNode_SignalHandler_HandleInputStream(           /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType currentResult;

  vStreamProc_PipeIdType pipeId = (vStreamProc_PipeIdType)vStreamProc_GetPipeIdxOfProcessingNode(ProcNodeInfo->ProcessingNodeId);
  vStreamProc_EntryPointIdType  entryPointId = vStreamProc_AccessNode_GetEntryPointIdOfEntryNode(ProcNodeInfo->ProcessingNodeId);
  vStreamProc_EntryPointInfoType entryPointInfo;
  vStreamProc_OutputPortInfoType outPortInfo;
  P2VAR(vStreamProc_EntryNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  vStreamProc_AccessNode_EntryStateType nextState = VSTREAMPROC_ACCESSNODE_ENTRY_STATE_FINALIZE_STREAM;

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_EntryNode_WorkspaceType(ProcNodeInfo);          /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Get output port information */
  (void)vStreamProc_PrepareOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output, &outPortInfo);

  /* #10 Request a buffer for at least 1 element. */
  currentResult = vStreamProc_RequestOutputPortData(
    ProcNodeInfo,
    vStreamProc_GetDataTypeOfEntryPoint(entryPointId),
    1u,
    &outPortInfo);

  /* #20 Handle the result. */
  switch (currentResult)
  {
    /* #30 If buffer for at least one element is available: */
    case VSTREAMPROC_OK:
    {
      /* #40 Prepare entry point info. */
      entryPointInfo.EntryPointSymbolicName = (vStreamProc_EntryPointSymbolicNameType)(entryPointId - vStreamProc_GetEntryPointStartIdxOfPipe(pipeId));

      if (vStreamProc_Pipe_PrepareEntryPointInfo(pipeId, &entryPointInfo) == VSTREAMPROC_OK)
      {
        vStreamProc_NamedOutputPortIdxOfEntryPointType namedOutputPort = vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointId);
        vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPort);
        vStreamProc_WaveTypeDefIdType waveTypeDefId =
          vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
            vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcNodeInfo->ProcessingNodeId));

        /* Copy write request from output port info to entry point structure. */
        entryPointInfo.WriteRequest = outPortInfo.WriteRequest;
        entryPointInfo.WriteRequest.StorageInfo.RequestLength = 0u;
        entryPointInfo.WriteRequest.StorageInfo.ReleaseFlag = TRUE;

        /* Initialize StreamPositionInfo and meta data. */
        vStreamProc_Stream_GetProducePosition(streamId, 0u, entryPointInfo.WriteRequest.StorageInfo.AvailableLength, &entryPointInfo.StreamPositionInfo);
        vStreamProc_Stream_GetMetaData(pipeId, streamId, waveTypeDefId, &entryPointInfo.MetaData);

        /* #50 Call the produce callback. */
        signalResponse = vStreamProc_AccessNode_CallProduceCallback(
          &entryPointInfo,
          workspace,
          &ProcNodeInfo->OutputPortResults[outPortInfo.SymbolicPortName],
          &nextState);

        /* #60 Acknowledge data and release buffer. */
        if (vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, entryPointInfo.WriteRequest.StorageInfo.RequestLength, entryPointInfo.WriteRequest.StorageInfo.ReleaseFlag, &outPortInfo) != VSTREAMPROC_OK)
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
        }
      }
      break;
    }
    /* #70 Otherwise return failed. */
    default:
    {
      /* VSTREAMPROC_PENDING, VSTREAMPROC_INSUFFICIENT_*, VSTREAMPROC_FAILED, .. */
      /* signalResponse is already set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
      break;
    }
  }

  vStreamProc_AccessNode_SetNextEntryState(ProcNodeInfo, nextState);

  return signalResponse;
}

/**********************************************************************************************************************
   EXIT NODE API
 *********************************************************************************************************************/
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_ExitNode_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ExitNode_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  vStreamProc_ExitNode_WorkspaceType* workspace;

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ExitNode_WorkspaceType(ProcNodeInfo);           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize callback to "unused". */
  workspace->ConsumeCbk = NULL_PTR;

  /* #20 Initialize the locked state. */
  workspace->IsLocked = FALSE;

  return E_OK;
}

/**********************************************************************************************************************
 *  vStreamProc_ExitNode_SignalHandler_HandleOutputStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ExitNode_SignalHandler_HandleOutputStream(           /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;

  vStreamProc_ReturnType currentResult;

  P2VAR(vStreamProc_ExitNode_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  vStreamProc_ExitPointInfoType exitPointInfo;
  vStreamProc_InputPortInfoType inputPortInfo;

  vStreamProc_PipeIdType pipeId = (vStreamProc_PipeIdType)vStreamProc_GetPipeIdxOfProcessingNode(ProcNodeInfo->ProcessingNodeId);
  vStreamProc_ExitPointIdType exitPointId = vStreamProc_AccessNode_GetExitPointIdOfExitNode(ProcNodeInfo->ProcessingNodeId);

  vStreamProc_AccessNode_ExitStateType  nextState = VSTREAMPROC_ACCESSNODE_EXIT_STATE_FINALIZE_STREAM;

  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ExitNode_WorkspaceType(ProcNodeInfo);           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Get output port information */
  (void)vStreamProc_PrepareInputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input, &inputPortInfo);

  /* #10 Request at least one data element. */
  currentResult = vStreamProc_RequestInputPortData(
    ProcNodeInfo,
    vStreamProc_GetDataTypeOfExitPoint(exitPointId),
    1u,
    &inputPortInfo);

  /* #20 Handle the result. */
  switch (currentResult)
  {
    /* #30 If at least 1 data element is available: */
    case VSTREAMPROC_OK:
    {
      /* #40 Prepare the exit point info. */
      exitPointInfo.ExitPointSymbolicName = (vStreamProc_ExitPointSymbolicNameType)(exitPointId - vStreamProc_GetExitPointStartIdxOfPipe(pipeId));

      if (vStreamProc_Pipe_PrepareExitPointInfo(pipeId, &exitPointInfo) == VSTREAMPROC_OK)
      {
        vStreamProc_NamedInputPortIdxOfExitPointType namedInputPortId = vStreamProc_GetNamedInputPortIdxOfExitPoint(exitPointId);
        vStreamProc_OutputPortIdType outputPortId = vStreamProc_GetStreamOutputPortMapping(
          vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortId));
        vStreamProc_WaveTypeDefIdType waveTypeDefId =
          vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
            vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcNodeInfo->ProcessingNodeId));

        /* Copy write request from input port info to exit point structure. */
        exitPointInfo.ReadRequest = inputPortInfo.ReadRequest;
        exitPointInfo.ReadRequest.StorageInfo.RequestLength = 0u;
        exitPointInfo.ReadRequest.StorageInfo.ReleaseFlag = TRUE;

        /* Initialize StreamPositionInfo and MetaData. */
        vStreamProc_Stream_GetConsumePosition(
          outputPortId,
          exitPointInfo.ReadRequest.StorageInfo.AvailableLength,
          &exitPointInfo.StreamPositionInfo);
        vStreamProc_Stream_GetMetaData(
          pipeId,
          vStreamProc_GetStreamIdxOfStreamOutputPort(outputPortId),
          waveTypeDefId,
          &exitPointInfo.MetaData);

        /* #50 Call the consume callback. */
        signalResponse = vStreamProc_AccessNode_CallConsumeCallback(
          &exitPointInfo,
          workspace,
          &ProcNodeInfo->InputPortResults[inputPortInfo.SymbolicPortName],
          &nextState);

        /* #60 Acknowledge data and release buffer. */
        if (vStreamProc_AcknowledgeInputPort(ProcNodeInfo, exitPointInfo.ReadRequest.StorageInfo.RequestLength, exitPointInfo.ReadRequest.StorageInfo.ReleaseFlag, &inputPortInfo) != VSTREAMPROC_OK)
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
        }
      }
      break;
    }
    /* #70 Otherwise return failed. */
    default:
    {
      /* VSTREAMPROC_PENDING, VSTREAMPROC_INSUFFICIENT_*, VSTREAMPROC_FAILED, .. */
      /* signalResponse is  already set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
      break;
    }
  }

  vStreamProc_AccessNode_SetNextExitState(ProcNodeInfo, nextState);

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ExitNode_SignalHandler_WaveEnd
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ExitNode_SignalHandler_WaveEnd(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ExitPointIdType exitPointId =
    vStreamProc_AccessNode_GetExitPointIdOfExitNode(ProcNodeInfo->ProcessingNodeId);
  vStreamProc_InputPortInfoType inputPortInfo;
  vStreamProc_ReturnType currentResult;

  /* ----- Implementation ----------------------------------------------- */
   /* #10 Query the input port. */
  (void)vStreamProc_PrepareInputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
    &inputPortInfo);

  currentResult = vStreamProc_GetInputPortInfo(
    ProcNodeInfo,
    vStreamProc_GetDataTypeOfExitPoint(exitPointId),
    &inputPortInfo);

  /* #20 Check if there is still data available. */
  if (currentResult == VSTREAMPROC_OK)
  {
    if (inputPortInfo.ReadRequest.StorageInfo.AvailableLength > 0u)
    {
      /* #30 If true, set the input port result to INSUFFICIENT_OUTPUT. */
      /* Hint: The WaveEnd signal will not be acknowledged as long as data is available.
               This is necessary to avoid that meta data is overwritten by a new wave before all data is consumed.
               The input port has to be used for reporting as an exit node does not have any output ports.*/
      ProcNodeInfo->InputPortResults[vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input] =
        VSTREAMPROC_INSUFFICIENT_OUTPUT;

      signalResponse = VSTREAMPROC_SIGNALRESPONSE_BUSY;
    }
    else
    {
      /* #40 Otherwise, acknowledge the wave end signal. */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
    }
  }

  return signalResponse;
}
#endif

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_AccessNode.c
 *********************************************************************************************************************/
