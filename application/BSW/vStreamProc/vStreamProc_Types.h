/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Types.h
 *        \brief  vStreamProc types header file
 *
 *      \details  Definition of all relevant types for vStreamProc
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_TYPES_H
#define VSTREAMPROC_TYPES_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/* State value for workspaces with state */
#define VSTREAMPROC_WS_STATE_UNDEFINED            0xFFu
#define VSTREAMPROC_WS_STATE_INITIALIZED          0x01u /**< Shall be set at end of <ProcessingNode>_Init function */
#define VSTREAMPROC_WS_STATE_PROCESSING           0x02u /**< Shall be set in <ProcessingNode>_SignalHandler_* function, after all INIT/FIRST CALL actions are done. */

#define VSTREAMPROC_MAX_LENGTH                    0xFFFFFFFFu

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

#define VSTREAMPROC_UNSIGNED_MAX(type)    (type)(~(type)0u)

#define VSTREAMPROC_NO_MODE               (vStreamProc_ModeIdType)VSTREAMPROC_NO_MODESTARTIDXOFPIPE
#define VSTREAMPROC_NO_NAMEDINPUTPORT     (vStreamProc_NamedInputPortIdType)VSTREAMPROC_NO_NAMEDINPUTPORTSTARTIDXOFPROCESSINGNODE
#define VSTREAMPROC_NO_NAMEDOUTPUTPORT    (vStreamProc_NamedOutputPortIdType)VSTREAMPROC_NO_NAMEDOUTPUTPORTSTARTIDXOFPROCESSINGNODE
#define VSTREAMPROC_NO_PORTDEF            (vStreamProc_PortDefIdType)VSTREAMPROC_NO_PORTDEFIDXOFSIGNALHANDLERDEF
#define VSTREAMPROC_NO_EXTERNALSIGNAL     (vStreamProc_ExternalSignalDefIdType)VSTREAMPROC_NO_EXTERNALSIGNALDEFSTARTIDXOFPIPE
#define VSTREAMPROC_NO_PROCESSINGNODE     (vStreamProc_ProcessingNodeIdType)VSTREAMPROC_NO_PROCESSINGNODESTARTIDXOFPIPE
#define VSTREAMPROC_NO_STORAGENODE        (vStreamProc_StorageNodeIdType)VSTREAMPROC_NO_STORAGENODEIDXOFSTREAM
#define VSTREAMPROC_NO_STORAGEOUTPUTPORT  (vStreamProc_OutputPortIdType)VSTREAMPROC_NO_STORAGEOUTPUTPORTSTARTIDXOFSTORAGENODE
#define VSTREAMPROC_NO_STREAMOUTPUTPORT   (vStreamProc_StreamOutputPortIdType)VSTREAMPROC_NO_STREAMOUTPUTPORTSTARTIDXOFSTREAM
#define VSTREAMPROC_NO_STREAM             (vStreamProc_StreamIdType)VSTREAMPROC_NO_STREAMIDXOFNAMEDOUTPUTPORT
#define VSTREAMPROC_NO_STREAMLIMIT        (vStreamProc_StreamLimitIdType)VSTREAMPROC_NO_STREAMLIMITIDXOFEXITPOINT
#define VSTREAMPROC_NO_ENTRYPOINT         (vStreamProc_EntryPointIdType)VSTREAMPROC_NO_ENTRYPOINTSTARTIDXOFPIPE
#define VSTREAMPROC_NO_EXITPOINT          (vStreamProc_ExitPointIdType)VSTREAMPROC_NO_EXITPOINTSTARTIDXOFPIPE
#define VSTREAMPROC_NO_SIGNALHANDLER      (vStreamProc_SignalHandlerIdType)VSTREAMPROC_NO_SIGNALHANDLERSTARTIDXOFPROCESSINGNODE
#define VSTREAMPROC_NO_SNAPSHOTSLOT       (vStreamProc_SnapshotSlotIdType)VSTREAMPROC_MAX_SNAPSHOTSLOTIDXOFSESSIONENTRY
#define VSTREAMPROC_NO_WAVELEVEL          VSTREAMPROC_UNSIGNED_MAX(vStreamProc_WaveLevelType)
#define VSTREAMPROC_NO_WAVE               VSTREAMPROC_UNSIGNED_MAX(vStreamProc_WaveIdType)

#define VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED   (vStreamProc_SignalHandlerStateType)VSTREAMPROC_UNREGISTERED_STATEOFSIGNALHANDLERINFO
#define VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED        (vStreamProc_SignalHandlerStateType)VSTREAMPROC_BLOCKED_STATEOFSIGNALHANDLERINFO
#define VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED     (vStreamProc_SignalHandlerStateType)VSTREAMPROC_REGISTERED_STATEOFSIGNALHANDLERINFO

/* Generic bit access access macros: */
#define vStreamProc_GetSetBitMaskResult(var, bitmask)   ((var) | (bitmask))
#define vStreamProc_GetClrBitMaskResult(var, bitmask)   (((var) | (bitmask)) ^ (bitmask))
#define vStreamProc_SetBitMask(var, bitmask)            var = vStreamProc_GetSetBitMaskResult(var, bitmask)
#define vStreamProc_ClrBitMask(var, bitmask)            var = vStreamProc_GetClrBitMaskResult(var, bitmask)
#define vStreamProc_IsBitMaskSet(var, bitmask)          (((var) & (bitmask)) == (bitmask))
#define vStreamProc_GetBitFlagValue(var, bitflag)       ((((var) & (bitflag)) == (bitflag)) ? TRUE:FALSE)

/* Wave state access macros */
/* Reuse signal response access macros */
#define vStreamProc_GetSetWaveStateFlagResult(waveState, waveStateFlag)   vStreamProc_GetSetBitMaskResult(waveState, waveStateFlag)
#define vStreamProc_GetClrWaveStateFlagResult(waveState, waveStateFlag)   vStreamProc_GetClrBitMaskResult(waveState, waveStateFlag)

#define vStreamProc_SetWaveStateFlag(waveState, waveStateFlag)            vStreamProc_SetBitMask(waveState, waveStateFlag) 
#define vStreamProc_SetWaveStateFlagOfWaveId(waveId, waveStateFlag)       vStreamProc_SetStateOfWaveVarInfo(waveId, (vStreamProc_GetSetWaveStateFlagResult((vStreamProc_GetStateOfWaveVarInfo(waveId)), waveStateFlag)))
#define vStreamProc_SetWaveStateFlagOfStreamOutputPortId(streamOutputPortId, waveStateFlag)       vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, (vStreamProc_GetSetWaveStateFlagResult((vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId)), waveStateFlag)))

#define vStreamProc_ClrWaveStateFlag(waveState, waveStateFlag)            vStreamProc_ClrBitMask(waveState, waveStateFlag)
#define vStreamProc_ClrWaveStateFlagOfWaveId(waveId, waveStateFlag)       vStreamProc_SetStateOfWaveVarInfo(waveId, (vStreamProc_GetClrWaveStateFlagResult((vStreamProc_GetStateOfWaveVarInfo(waveId)), waveStateFlag)))
#define vStreamProc_ClrWaveStateFlagOfStreamOutputPortId(streamOutputPortId, waveStateFlag)       vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, (vStreamProc_GetClrWaveStateFlagResult((vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId)), waveStateFlag)))

#define vStreamProc_IsWaveStateFlagSet(waveState, waveStateFlag)          vStreamProc_IsBitMaskSet(waveState, waveStateFlag) 
#define vStreamProc_IsWaveStateFlagSetOfWaveId(waveId, waveStateFlag)     vStreamProc_IsWaveStateFlagSet(vStreamProc_GetStateOfWaveVarInfo(waveId), waveStateFlag)
#define vStreamProc_IsWaveStateFlagSetOfStreamOutputPortId(streamOutputPortId, waveStateFlag)     vStreamProc_IsWaveStateFlagSet(vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId), waveStateFlag)

#define vStreamProc_GetWaveStateFlag(waveState, waveStateFlag)            vStreamProc_GetBitFlagValue(waveState, waveStateFlag)
#define vStreamProc_GetWaveStateFlagOfWaveId(waveId, waveStateFlag)       vStreamProc_GetWaveStateFlag(vStreamProc_GetStateOfWaveVarInfo(waveId), waveStateFlag)
#define vStreamProc_GetWaveStateFlagOfStreamOutputPortId(streamOutputPortId, waveStateFlag)       vStreamProc_GetWaveStateFlag(vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId), waveStateFlag)

/* Wave state flags */
#define VSTREAMPROC_WAVESTATE_FLAG_INIT           (vStreamProc_WaveStateType)(0x10u)
#define VSTREAMPROC_WAVESTATE_FLAG_STARTING       (vStreamProc_WaveStateType)(0x01u)
#define VSTREAMPROC_WAVESTATE_FLAG_ACTIVE         (vStreamProc_WaveStateType)(0x02u)
#define VSTREAMPROC_WAVESTATE_FLAG_ENDING         (vStreamProc_WaveStateType)(0x04u)

/* Valid wave states */
#define VSTREAMPROC_WAVESTATE_UNINIT              (vStreamProc_WaveStateType)(0x00u)
#define VSTREAMPROC_WAVESTATE_IDLE                (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT)
#define VSTREAMPROC_WAVESTATE_STARTING            (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT | VSTREAMPROC_WAVESTATE_FLAG_STARTING)
#define VSTREAMPROC_WAVESTATE_ACTIVE              (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT | VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)
#define VSTREAMPROC_WAVESTATE_ENDING              (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT | VSTREAMPROC_WAVESTATE_FLAG_ENDING)

#define VSTREAMPROC_WAVESTATE_STARTING_ACTIVE     (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT | VSTREAMPROC_WAVESTATE_FLAG_STARTING | VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)
#define VSTREAMPROC_WAVESTATE_STARTING_ENDING     (vStreamProc_WaveStateType)(VSTREAMPROC_WAVESTATE_FLAG_INIT | VSTREAMPROC_WAVESTATE_FLAG_STARTING | VSTREAMPROC_WAVESTATE_FLAG_ENDING)

/* vStreamProc_SignalResponseType access macros: */
#define vStreamProc_GetSetSignalResponseFlagResult(signalResponse, signalResponeFlag)   vStreamProc_GetSetBitMaskResult(signalResponse, signalResponeFlag)
#define vStreamProc_GetClrSignalResponseFlagResult(signalResponse, signalResponeFlag)   vStreamProc_GetClrBitMaskResult(signalResponse, signalResponeFlag)
#define vStreamProc_SetSignalResponseFlag(signalResponse, signalResponeFlag)            vStreamProc_SetBitMask(signalResponse, signalResponeFlag)
#define vStreamProc_ClrSignalResponseFlag(signalResponse, signalResponeFlag)            vStreamProc_ClrBitMask(signalResponse, signalResponeFlag)
#define vStreamProc_IsSignalResponseFlagSet(signalResponse, signalResponeFlag)          vStreamProc_IsBitMaskSet(signalResponse, signalResponeFlag)
#define vStreamProc_GetSignalResponseFlagValue(signalResponse, signalResponeFlag)       vStreamProc_GetBitFlagValue(signalResponse, signalResponeFlag)

/* vStreamProc_SignalResponseType bit flags: */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE  (vStreamProc_SignalResponseType)(0x01u)  /*!< Workload shall consume a schedule cycle if set. */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK       (vStreamProc_SignalResponseType)(0x02u)  /*!< Signal successfully consumed if set. */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS   (vStreamProc_SignalResponseType)(0x80u)  /*!< Result successful if set. */

/* vStreamProc_SignalResponseType supported values: */
#define VSTREAMPROC_SIGNALRESPONSE_BUSY           (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS)
#define VSTREAMPROC_SIGNALRESPONSE_PENDING        (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE)
#define VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE    (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK)
#define VSTREAMPROC_SIGNALRESPONSE_CONFIRM        (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK | VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE)
#define VSTREAMPROC_SIGNALRESPONSE_FAILED         (vStreamProc_SignalResponseType)(0x00u)

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/****************************
 *  Generic external Types  *
 ****************************/
typedef uint32 vStreamProc_PipeIdType;
typedef uint32 vStreamProc_ModeIdType;
typedef uint32 vStreamProc_ModeHandleIdType;
typedef uint32 vStreamProc_DataTypeIdType;
typedef uint32 vStreamProc_StreamPositionType;
typedef uint32 vStreamProc_LengthType;
typedef uint32_least vStreamProc_LengthIterType;
typedef uint32 vStreamProc_SignalIdType;
typedef uint32 vStreamProc_WaveTypeSymbolicNameType;
typedef uint8 vStreamProc_WaveStateType;
typedef uint32 vStreamProc_WaveHandleType;

/* Note: Sorted by increasing "severity". */
typedef enum
{
  VSTREAMPROC_OK,
  VSTREAMPROC_PENDING,
  VSTREAMPROC_INSUFFICIENT_INPUT,
  VSTREAMPROC_INSUFFICIENT_OUTPUT,
  VSTREAMPROC_BLOCKED,
  VSTREAMPROC_FAILED
} vStreamProc_ReturnType;

typedef struct
{
  vStreamProc_LengthType      Size;
  vStreamProc_DataTypeIdType  Id;
} vStreamProc_DataTypeInfoType;

typedef struct /*<! Holds stream related position information. */
{
  vStreamProc_StreamPositionType AbsolutePositionStart; /*!< Points to absolute start position [element]. */
  vStreamProc_StreamPositionType RelativePosition; /*!< Points to current relative start position (related to currently requested buffer) [element]. */
  vStreamProc_LengthType         TotalLength;      /*!< RelativePosition + size of currently requested buffer [element]. */
}vStreamProc_StreamPositionInfoType;

typedef P2VAR(vStreamProc_StreamPositionInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)     vStreamProc_StreamPositionInfoPtrType;
typedef P2CONST(vStreamProc_StreamPositionInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   vStreamProc_StreamPositionInfoConstPtrType;

typedef struct
{
  vStreamProc_DataTypeInfoType  DataTypeInfo;
  vStreamProc_LengthType        AvailableLength;
  vStreamProc_LengthType        RequestLength;
  boolean                       ReleaseFlag;
} vStreamProc_StorageInfoType;

typedef P2VAR(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_StorageInfoPtrType;
typedef P2CONST(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  vStreamProc_StorageInfoConstPtrType;


typedef struct
{
  vStreamProc_StorageInfoType                       StorageInfo;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR)   Buffer;
} vStreamProc_ReadRequestType;

typedef P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_ReadRequestPtrType;
typedef P2CONST(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  vStreamProc_ReadRequestConstPtrType;

typedef struct
{
  vStreamProc_StorageInfoType                       StorageInfo;
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR)     Buffer;
} vStreamProc_WriteRequestType;

typedef P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA)     vStreamProc_WriteRequestPtrType;
typedef P2CONST(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   vStreamProc_WriteRequestConstPtrType;

typedef struct
{
  vStreamProc_StreamPositionType          AbsoluteStreamPosition; /*!< Used to provide absolute stream start position from upper layer. */
  vStreamProc_StreamPositionType          RelativeStreamPosition;
  vStreamProc_ReadRequestType             MetaData;
  vStreamProc_WaveTypeSymbolicNameType    WaveTypeSymbolicName;
  vStreamProc_WaveStateType               State;
  vStreamProc_WaveHandleType              Handle;
} vStreamProc_WaveInfoType;

typedef P2VAR(vStreamProc_WaveInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_WaveInfoPtrType;
typedef P2CONST(vStreamProc_WaveInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_WaveInfoConstPtrType;


/****************************
 *  Access Point API Types  *
 ****************************/
typedef uint32 vStreamProc_EntryPointSymbolicNameType;
typedef uint32 vStreamProc_ExitPointSymbolicNameType;
typedef uint32 vStreamProc_EntryPointIdType;
typedef uint32 vStreamProc_ExitPointIdType;
typedef uint32 vStreamProc_CallbackHandleType;

typedef struct
{
  vStreamProc_WriteRequestType            WriteRequest;   /*!< The write request. */
  vStreamProc_ReadRequestType             MetaData;
  vStreamProc_StreamPositionInfoType      StreamPositionInfo;
  vStreamProc_EntryPointSymbolicNameType  EntryPointSymbolicName;   /*!< The symbolic entry point name. */
} vStreamProc_EntryPointInfoType;

typedef P2VAR(vStreamProc_EntryPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)     vStreamProc_EntryPointInfoPtrType;
typedef P2CONST(vStreamProc_EntryPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   vStreamProc_EntryPointInfoConstPtrType;

typedef struct
{
  vStreamProc_ReadRequestType             ReadRequest;
  vStreamProc_ReadRequestType             MetaData;
  vStreamProc_StreamPositionInfoType      StreamPositionInfo;
  vStreamProc_ExitPointSymbolicNameType   ExitPointSymbolicName;
} vStreamProc_ExitPointInfoType;

typedef P2VAR(vStreamProc_ExitPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_ExitPointInfoPtrType;
typedef P2CONST(vStreamProc_ExitPointInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_ExitPointInfoConstPtrType;


/*******************************
 *  Processing Node API Types  *
 ******************************/
typedef uint32 vStreamProc_ProcessingNodeIdType;
typedef uint32 vStreamProc_InputPortSymbolicNameType;
typedef uint32 vStreamProc_InputPortIdType;
typedef uint32_least vStreamProc_InputPortIterType;
typedef uint32 vStreamProc_OutputPortSymbolicNameType;
typedef uint32 vStreamProc_OutputPortIdType;
typedef uint32_least vStreamProc_OutputPortIterType;
typedef uint8 vStreamProc_SignalResponseType;
typedef uint8 vStreamProc_SignalHandlerStateType;
typedef uint32 vStreamProc_WorkspaceTypeIdType;
typedef uint32 vStreamProc_ConfigTypeIdType;
typedef uint32 vStreamProc_SliceHandleType;

typedef void * vStreamProc_GenericNodeWorkspaceType;
typedef const void * vStreamProc_GenericNodeConfigType;

typedef enum
{
  VSTREAMPROC_LIMIT_SOFT,
  VSTREAMPROC_LIMIT_HARD,
  VSTREAMPROC_LIMIT_PROMPT,

  VSTREAMPROC_LIMIT_COUNT   /* Count of limit types, used for validity checks. */
} vStreamProc_LimitType;

typedef struct
{
  vStreamProc_GenericNodeWorkspaceType  Pointer;
  vStreamProc_WorkspaceTypeIdType       TypeId;
} vStreamProc_WorkspaceInfoType;

typedef struct
{
  vStreamProc_GenericNodeConfigType   Pointer;
  vStreamProc_ConfigTypeIdType        TypeId;
} vStreamProc_ConfigInfoType;

typedef struct
{
  vStreamProc_SignalIdType                SignalId;
  vStreamProc_InputPortSymbolicNameType   InputPortId;
  vStreamProc_OutputPortSymbolicNameType  OutputPortId;
} vStreamProc_SignalInfoType;

typedef struct
{
  vStreamProc_ReadRequestType             ReadRequest;
  vStreamProc_ReadRequestType             MetaData;
  vStreamProc_StreamPositionInfoType      StreamPositionInfo;
  vStreamProc_InputPortSymbolicNameType   SymbolicPortName;
  boolean                                 IsConnected;
  boolean                                 IsVirtual;
} vStreamProc_InputPortInfoType;

typedef P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_InputPortInfoPtrType;
typedef P2CONST(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_InputPortInfoConstPtrType;

typedef struct
{
  vStreamProc_WriteRequestType            WriteRequest;
  vStreamProc_ReadRequestType             MetaData;
  vStreamProc_StreamPositionInfoType      StreamPositionInfo;
  vStreamProc_OutputPortSymbolicNameType  SymbolicPortName;
  boolean                                 IsConnected;
  boolean                                 IsVirtual;
} vStreamProc_OutputPortInfoType;

typedef P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)     vStreamProc_OutputPortInfoPtrType;
typedef P2CONST(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   vStreamProc_OutputPortInfoConstPtrType;

typedef struct
{
  vStreamProc_WorkspaceInfoType     WorkspaceInfo;
  vStreamProc_ConfigInfoType        ConfigInfo;
  vStreamProc_SignalInfoType        SignalInfo;
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR)  InputPortResults;
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR)  OutputPortResults;
  vStreamProc_ProcessingNodeIdType  ProcessingNodeId;
  vStreamProc_SliceHandleType       SliceHandle;
  vStreamProc_InputPortIdType       InputPortCount;
  vStreamProc_OutputPortIdType      OutputPortCount;
} vStreamProc_ProcessingNodeInfoType;


/****************************
 *  Storage Node API Types  *
 ****************************/
typedef uint32 vStreamProc_StorageNodeIdType;
typedef uint32 vStreamProc_StorageOutputPortIdType;
typedef boolean vStreamProc_OutputActivationType;


/********************
 *  Internal Types  *
 ********************/
typedef uint32 vStreamProc_ProcessingNodeTypeDefIdType;
typedef uint32 vStreamProc_ProcessingNodeClassDefIdType;
typedef uint32 vStreamProc_StreamIdType;
typedef uint32 vStreamProc_StreamInputPortIdType;
typedef uint32 vStreamProc_StreamOutputPortIdType;
typedef uint32 vStreamProc_NamedInputPortIdType;
typedef uint32 vStreamProc_StreamOutputPortMappingIdType;
typedef uint32 vStreamProc_NamedOutputPortIdType;
typedef uint32 vStreamProc_PortIdType;
typedef uint32 vStreamProc_PortDefIdType;
typedef uint32 vStreamProc_BaseStateIdType;
typedef uint32 vStreamProc_EdgeIdType;
typedef uint32 vStreamProc_SignalHandlerIdType;
typedef uint32 vStreamProc_SignalHandlerInfoIdType;
typedef uint32 vStreamProc_SignalHandlerDefIdType;
typedef uint32 vStreamProc_ExternalSignalDefIdType;
typedef uint32 vStreamProc_SnapshotSetIdType;
typedef uint32 vStreamProc_SnapshotSlotIdType;
typedef uint32 vStreamProc_SnapshotInputPortIdType;
typedef uint32 vStreamProc_SnapshotOutputPortIdType;
typedef uint32 vStreamProc_SnapshotWaveIdType;
typedef uint32 vStreamProc_WaveIdType;
typedef uint32 vStreamProc_WaveTypeDefIdType;
typedef uint32 vStreamProc_WaveLevelType;
typedef uint32 vStreamProc_WaveRoutingIdType;
typedef uint32 vStreamProc_SessionIdType;
typedef uint32 vStreamProc_SessionEntryIdType;
typedef uint32 vStreamProc_StreamLimitIdType;
typedef uint32 vStreamProc_SchedulingQueueIdType;


typedef struct
{
  vStreamProc_ProcessingNodeIdType        ProcessingNodeId;
  vStreamProc_InputPortSymbolicNameType   InputPortSymbolicName;
  vStreamProc_NamedInputPortIdType        NamedInputPortId;
  vStreamProc_PortDefIdType               PortDefId;
  vStreamProc_StorageOutputPortIdType     StorageOutputPortId;
  vStreamProc_StorageNodeIdType           StorageNodeId;
  vStreamProc_StreamOutputPortIdType      StreamOutputPortId;
  vStreamProc_StreamIdType                StreamId;
  vStreamProc_WaveIdType                  WaveId;
  vStreamProc_WaveLevelType               WaveLevel;
  boolean                                 IsVirtual;
  boolean                                 IsConnected;
  boolean                                 HasActiveProducer;
} vStreamProc_InputPortHandleType;

typedef P2VAR(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_InputPortHandlePtrType;
typedef P2CONST(vStreamProc_InputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  vStreamProc_InputPortHandleConstPtrType;

typedef struct
{
  vStreamProc_ProcessingNodeIdType        ProcessingNodeId;
  vStreamProc_OutputPortSymbolicNameType  OutputPortSymbolicName;
  vStreamProc_NamedOutputPortIdType       NamedOutputPortId;
  vStreamProc_PortDefIdType               PortDefId;
  vStreamProc_StorageNodeIdType           StorageNodeId;
  vStreamProc_StreamIdType                StreamId;
  vStreamProc_WaveIdType                  WaveId;
  vStreamProc_WaveLevelType               WaveLevel;
  boolean                                 IsVirtual;
  boolean                                 IsConnected;
} vStreamProc_OutputPortHandleType;

typedef P2VAR(vStreamProc_OutputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_OutputPortHandlePtrType;
typedef P2CONST(vStreamProc_OutputPortHandleType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  vStreamProc_OutputPortHandleConstPtrType;

typedef struct
{
  vStreamProc_StreamPositionType  Position;
  vStreamProc_StreamPositionType  Offset;
  vStreamProc_LimitType           Type;
} vStreamProc_LimitInfoType;

typedef P2VAR(vStreamProc_LimitInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_LimitInfoPtrType;
typedef P2CONST(vStreamProc_LimitInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  vStreamProc_LimitInfoConstPtrType;

/**********************************************************************************************************************
 * FUNCTION POINTER TYPES
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * PROCESSING NODE FUNCTION POINTERS
 *********************************************************************************************************************/
typedef P2FUNC(Std_ReturnType, VSTREAMPROC_CODE, vStreamProc_ProcessingNode_InitFctPtrType)(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

typedef P2FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE, vStreamProc_SignalHandlerFctPtrType)(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 * STORAGE NODE FUNCTION POINTERS
 *********************************************************************************************************************/
typedef P2FUNC(Std_ReturnType, VSTREAMPROC_CODE, vStreamProc_InitFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId);
typedef P2FUNC(void, VSTREAMPROC_CODE, vStreamProc_DiscardAllDataFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId);

/* Storage node data provisioning function pointer */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_InputPort_WriteInfoFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) WriteInfoPtr);
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_InputPort_WriteRequestFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) RequestBufferPtr);
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_InputPort_WriteAckFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_LengthType ProducedLen);

/* Storage node data consumption function pointer */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_OutputPort_ReadInfoFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ReadInfoPtr,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId);
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_OutputPort_ReadRequestFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) RequestBufferPtr,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId);
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_OutputPort_ReadAckFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_LengthType ConsumedLen,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId);

/* Storage node info function pointer */
typedef P2FUNC(boolean, VSTREAMPROC_CODE, vStreamProc_StorageInfo_IsEmptyFctPtr)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId);

/* Storage node output port function pointer */
typedef P2FUNC(void, VSTREAMPROC_CODE, vStreamProc_OutputPort_SetActivationSignalFctPtrType)(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId,
  vStreamProc_OutputActivationType ActivationSignalValue);

/**********************************************************************************************************************
 * DATA ACCESS CALLBACK FUNCTION POINTERS
 *********************************************************************************************************************/
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_ProduceDataFctPtrType)(
  vStreamProc_EntryPointInfoPtrType EntryPointInfo);
typedef vStreamProc_ProduceDataFctPtrType vStreamProc_ProduceCallbackType;

typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_ConsumeDataFctPtrType)(
  vStreamProc_ExitPointInfoPtrType ExitPointInfo);
typedef vStreamProc_ConsumeDataFctPtrType vStreamProc_ConsumeCallbackType;

/**********************************************************************************************************************
 * SIGNAL RESPONSE CALLBACK FUNCTION POINTERS
 *********************************************************************************************************************/
typedef P2FUNC(void, VSTREAMPROC_CODE, vStreamProc_SignalResponseFctPtrType)(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_ReturnType  SignalResult,
  vStreamProc_CallbackHandleType CallbackHandle);
typedef vStreamProc_SignalResponseFctPtrType vStreamProc_SignalCallbackType;

#endif /* VSTREAMPROC_TYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Types.h
 *********************************************************************************************************************/

