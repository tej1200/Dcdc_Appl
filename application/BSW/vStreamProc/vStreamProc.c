/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc.c
 *        \brief  vStreamProc Source Code File
 *
 *      \details  Implementation of the vStreamProc module.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/
#define VSTREAMPROC_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Pipe.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_AccessNode.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of vStreamProc header file */
#if (  (VSTREAMPROC_SW_MAJOR_VERSION != (3u)) \
    || (VSTREAMPROC_SW_MINOR_VERSION != (3u)) \
    || (VSTREAMPROC_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of vStreamProc.c and vStreamProc.h are inconsistent"
#endif

/* Check the version of the configuration header file */
#if (  (VSTREAMPROC_CFG_MAJOR_VERSION != (3u)) \
    || (VSTREAMPROC_CFG_MINOR_VERSION != (3u)) )
# error "Version numbers of vStreamProc.c and vStreamProc_Cfg.h are inconsistent!"
#endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

#define VSTREAMPROC_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Initialization state of the module */
#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
VSTREAMPROC_LOCAL VAR(uint8, VSTREAMPROC_VAR_ZERO_INIT) vStreamProc_ModuleInitialized = VSTREAMPROC_UNINIT;
#endif

#define VSTREAMPROC_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_DetChecksPipe
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for pipe APIs.
 * \details       Includes check for opened pipe.
 *                Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksPipe(
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksEntryPoint
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on an entry point.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                    Id of the desired pipe.
 * \param[in]     EntryPointSymbolicName    Symbolic name of the entry point.
 * \param[out]    ErrorId                   VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                          DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksEntryPointInfo
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on an entry point info structures.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     EntryPointInfo  Pointer to entry point information structure.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoConstPtrType EntryPointInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksExitPoint
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on an exit point.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  Id of the desired pipe.
 * \param[in]     ExitPointSymbolicName   Symbolic name of the exit point.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksExitPointInfo
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on an exit point info structures.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     ExitPointInfo   Pointer to exit point information structure.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoConstPtrType ExitPointInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksAccessPointInfos
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on multiple access point info structures.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     EntryPointInfos Entry point information structures. NULL_PTR if not used.
 * \param[in]     EntryPointCount Number of entry point information structures.
 * \param[in]     ExitPointInfos  Exit point information structures. NULL_PTR if not used.
 * \param[in]     ExitPointCount  Number of exit point information structures.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksAccessPointInfos(
  vStreamProc_EntryPointInfoConstPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoConstPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksAccessPointIds
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on multiple access point info structures.
 * \details       Includes check of access point IDs.
 *                Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     EntryPointInfos Entry point information structures. NULL_PTR if not used.
 * \param[in]     EntryPointCount Number of entry point information structures.
 * \param[in]     ExitPointInfos  Exit point information structures. NULL_PTR if not used.
 * \param[in]     ExitPointCount  Number of exit point information structures.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksAccessPointIds(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksBroadcastSignal
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for broadcast signal APIs.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     SignalId        Id of the signal which shall broadcasted.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksBroadcastSignal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksLimit
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for limit APIs.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  Id of the desired pipe.
 * \param[in]     ExitPointSymbolicName   Zero based ID of exit point.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksLimit(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksLimitType
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for limit APIs where type is passed.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  Id of the desired pipe.
 * \param[in]     ExitPointSymbolicName   Zero based ID of exit point.
 * \param[in]     LimitType               Type of limit.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksLimitType(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_LimitType LimitType,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveType
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for the wave type depending on the provided API.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                Id of the desired pipe.
 * \param[in]     WaveTypeSymbolicName  The symbolic name of the wave type.
 * \param[out]    ErrorId               VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                      DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveType(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveInfo
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for APIs operating on wave info structures.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[in]     WaveInfo        Pointer to the wave info structure.
 * \param[out]    ErrorId         VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveInfoConstPtrType WaveInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState_StartWave
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for the wave state for the StartEntryPointWave() API.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  The id of the pipe.
 * \param[in]     EntryPointSymbolicName  The symbolic name of the entry point.
 * \param[in]     WaveTypeSymbolicName    The symbolic name of the target wave type.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_StartWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState_EndWave
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for the wave state for the EndEntryPointWave() API.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  The id of the pipe.
 * \param[in]     EntryPointSymbolicName  The symbolic name of the entry point.
 * \param[in]     WaveTypeSymbolicName    The symbolic name of the target wave type.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_EndWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState
 *********************************************************************************************************************/
/*!
 * \brief         Performs common DET checks for the wave state for the AnnounceStream() API.
 * \details       Return code and ErrorId output parameter contain the same value.
 * \param[in]     PipeId                  The id of the pipe.
 * \param[in]     EntryPointSymbolicName  The symbolic name of the entry point.
 * \param[out]    ErrorId                 VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                                        DET error code          Otherwise.
 * \return        VSTREAMPROC_E_NO_ERROR  No error, all preconditions are fulfilled.
 *                DET error code          Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_AnnounceStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId);

/**********************************************************************************************************************
 *  vStreamProc_GetStreamIdOfEntryPoint
 *********************************************************************************************************************/
/*!
 * \brief         Returns the stream id of an entry point (via entry node).
 * \details       -
 * \param[in]     PipeId                  Id of the desired pipe.
 * \param[in]     EntryPointSymbolicName  Symbolic name of the entry point.
 * \return        Id of the stream.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamIdType, VSTREAMPROC_CODE) vStreamProc_GetStreamIdOfEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName);
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_DetChecksPipe
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksPipe(
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Check initialization state of the component. */
  if (vStreamProc_ModuleInitialized != (uint8)VSTREAMPROC_INIT)
  {
    errorID = VSTREAMPROC_E_UNINIT;
  }
  /* Check if pipe is unavailable. */
  else if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Ensure that the pipe is open. */
  else if (vStreamProc_GetPipeState(PipeId) != VSTREAMPROC_OPENED_PIPESTATE)
  {
    errorID = VSTREAMPROC_E_PIPE_STATE;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksEntryPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if EntryPointSymbolicName is an invalid ID. */
  else if (EntryPointSymbolicName >= vStreamProc_GetEntryPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksEntryPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoConstPtrType EntryPointInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if EntryPointInfo is a null pointer. */
  else if (EntryPointInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Check if EntryPointSymbolicName is an invalid ID. */
  else if (EntryPointInfo->EntryPointSymbolicName >= vStreamProc_GetEntryPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksExitPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if ExitPointId is an invalid ID. */
  else if (ExitPointSymbolicName >= vStreamProc_GetExitPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksExitPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoConstPtrType ExitPointInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if ExitPointInfo is a null pointer. */
  else if (ExitPointInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Check if ExitPointId is an invalid ID. */
  else if (ExitPointInfo->ExitPointSymbolicName >= vStreamProc_GetExitPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksAccessPointInfos
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksAccessPointInfos(
  vStreamProc_EntryPointInfoConstPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoConstPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Check if EntryPointInfo is a null pointer, while count is greater than zero. */
  if ( (EntryPointInfos == NULL_PTR) && (EntryPointCount > 0u) )
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Check if ExitPointInfo is a null pointer, while count is greater than zero. */
  else if ( (ExitPointInfos == NULL_PTR) && (ExitPointCount > 0u) )
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksAccessPointIds
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
/* PRQA S 6060 1 */ /* MD_MSR_STPAR */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksAccessPointIds(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Perform common DET checks for APIs operating on multiple access point info structures. */
  else if (vStreamProc_DetChecksAccessPointInfos(EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check access point IDs for invalid values. */
  else if (vStreamProc_Pipe_DetChecksAccessPointInfos(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksBroadcastSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksBroadcastSignal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* #20 Check if the provided signal is valid */
  else if (SignalId >= vStreamProc_GetSizeOfSignal())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* #30 Check if the provided signal is a broadcast signal */
  else if (!vStreamProc_IsExternalOfSignal(SignalId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksLimit(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* #10 Check whether stream limit is configured at exit point. */
  if (!vStreamProc_HasStreamLimit())
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else
  {
    vStreamProc_ExitPointIdType       exitPointId         = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamLimitIdType     streamLimitIdx      = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);

    if (streamLimitIdx == VSTREAMPROC_NO_STREAMLIMITIDXOFEXITPOINT)
    {
      errorID = VSTREAMPROC_E_ID_PARAM;
    }
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksLimitType
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksLimitType(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_LimitType LimitType,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* #10 Check plausibility of limit type parameter. */
  if (LimitType >= VSTREAMPROC_LIMIT_COUNT)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* #20 Check whether stream limit is configured at exit point. */
  else if (vStreamProc_DetChecksLimit(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
  {
    /* #30 Check provided limit type against configured one. */
    vStreamProc_ExitPointIdType       exitPointId         = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamLimitIdType     streamLimitIdx      = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);
    vStreamProc_TypeOfStreamLimitType configuredLimitType = vStreamProc_GetTypeOfStreamLimit(streamLimitIdx);

    if (LimitType == VSTREAMPROC_LIMIT_HARD)
    {
      if (configuredLimitType == VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT)
      {
        errorID = VSTREAMPROC_E_ID_PARAM;
      }
    }
    else
    {
      if (configuredLimitType == VSTREAMPROC_HARD_TYPEOFSTREAMLIMIT)
      {
        errorID = VSTREAMPROC_E_ID_PARAM;
      }
    }
  }

  *ErrorId = errorID;

  return errorID;
}


/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveType
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveType(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  boolean waveTypeUsed = FALSE;
  vStreamProc_WaveLevelDefIterType waveLevelDefIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Search wave type in pipe wave levels. */
  for (waveLevelDefIdx = vStreamProc_GetWaveLevelDefStartIdxOfPipe(PipeId);
       waveLevelDefIdx < vStreamProc_GetWaveLevelDefEndIdxOfPipe(PipeId);
       waveLevelDefIdx++)
  {
    if (vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(waveLevelDefIdx) ==
          (vStreamProc_WaveTypeDefIterType)WaveTypeSymbolicName)
    {
      waveTypeUsed = TRUE;
      break;
    }
  }

  /* #20 Check if the provided wave type is valid. */
  if (WaveTypeSymbolicName >= vStreamProc_GetSizeOfWaveTypeDef())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* #30 Check if the wave type is used in the pipe wave levels. */
  else if (waveTypeUsed == FALSE)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveInfo(                                          /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveInfoConstPtrType WaveInfo,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if wave info pointer is valid. */
  if (WaveInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* #20 Check if the provided wave type is valid. */
  else if (vStreamProc_DetChecksWaveType(PipeId, WaveInfo->WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* #30 Check if the meta data requests is valid. */
  else if (WaveInfo->MetaData.StorageInfo.RequestLength > 1u)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else if ((WaveInfo->MetaData.StorageInfo.RequestLength == 1u)
        && (WaveInfo->MetaData.StorageInfo.DataTypeInfo.Id !=
              vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(WaveInfo->WaveTypeSymbolicName)))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_StartWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_WaveLevelType waveLevel = 0u;
  vStreamProc_WaveLevelType targetWaveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, WaveTypeSymbolicName);
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdOfEntryPoint(PipeId, EntryPointSymbolicName);
  boolean breakLoop = FALSE;
  vStreamProc_WaveIterType waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all wave levels. */
  for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamId);
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamId);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Check if all wave levels have the expected state. */
    /* Hint: StartWave has to be called from top level to bottom level:
              - Wave levels below or equal the target level must not have the active flag set.
              - Wave levels above the target level need to have the active or starting flag set. */
    if ( (waveLevel <= targetWaveLevel)
      && (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)))
    {
      errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
      breakLoop = TRUE;
    }

    if ((waveLevel > targetWaveLevel)
          && (!vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)
          && (!vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING))))
    {
      errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
      breakLoop = TRUE;
    }

    if (breakLoop == TRUE)
    {
      break;
    }

    waveLevel++;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState_EndWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_EndWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_WaveLevelType waveLevel = 0u;
  vStreamProc_WaveLevelType targetWaveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, WaveTypeSymbolicName);
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdOfEntryPoint(PipeId, EntryPointSymbolicName);
  boolean breakLoop = FALSE;
  vStreamProc_WaveIterType waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all wave levels. */
  for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamId);
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamId);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Check if all wave levels have the expected state. */
    /* Hint: EndWave has to be called from bottom level to top level:
              - Wave levels below the target level must not have the active flag set.
              - Wave levels above or equal the target level need to have the active flag set. */
    if ((waveLevel < targetWaveLevel)
      && (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)))
    {
      errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
      breakLoop = TRUE;
    }

    if ((waveLevel >= targetWaveLevel)
      && (!vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)))
    {
      errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
      breakLoop = TRUE;
    }

    if (breakLoop == TRUE)
    {
      break;
    }

    waveLevel++;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_DetChecksWaveState_AnnounceStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_DetChecksWaveState_AnnounceStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdOfEntryPoint(PipeId, EntryPointSymbolicName);
  vStreamProc_WaveIterType waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all wave levels. */
  for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamId);
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamId);
       waveIdx++)
  {
    /* #20 Check if the wave level is ACTIVE. */
    if (!vStreamProc_IsWaveStateFlagSetOfWaveId(waveIdx, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
    {
      errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
      break;
    }
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_GetStreamIdOfEntryPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamIdType, VSTREAMPROC_CODE) vStreamProc_GetStreamIdOfEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_EntryPointIdType entryPointId = vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName;
  vStreamProc_StreamIdType streamId =
    vStreamProc_GetStreamIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the stream id. */
  return streamId;
}
#endif

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vStreamProc_InitMemory
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_InitMemory(void)
{
  /* ----- Implementation ------------------------------------------------- */
  /* #10 Initialize global state variables. */
#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  vStreamProc_ModuleInitialized = VSTREAMPROC_UNINIT;
#endif
}

/**********************************************************************************************************************
 * vStreamProc_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Init(
  P2CONST(vStreamProc_ConfigType, AUTOMATIC, VSTREAMPROC_PBCFG) ConfigPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  VSTREAMPROC_DUMMY_STATEMENT(ConfigPtr);                                                                               /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check initialization state of the component. */
  if (vStreamProc_ModuleInitialized == (uint8)VSTREAMPROC_INIT)
  {
    errorID = VSTREAMPROC_E_ALREADY_INITIALIZED;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    vStreamProc_PipeIterType pipeId;

    /* #20 Initialize pipe state to default value ("uninitialized"). */
    for (pipeId = 0u; pipeId < vStreamProc_GetSizeOfPipeState(); pipeId++)
    {
      vStreamProc_SetPipeState(pipeId, VSTREAMPROC_UNINIT_PIPESTATE);
    }

# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
    /* #30 Module is now initialized. */
    vStreamProc_ModuleInitialized = (uint8)VSTREAMPROC_INIT;
# endif
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_INIT, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */
}

/**********************************************************************************************************************
 *  vStreamProc_InitPipe
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_InitPipe(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check initialization state of the component. */
  if (vStreamProc_ModuleInitialized != (uint8)VSTREAMPROC_INIT)
  {
    errorID = VSTREAMPROC_E_UNINIT;
  }
  /* Check if pipe is unavailable. */
  else if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Initialize the pipe. */
    retVal = vStreamProc_Pipe_Init(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_INIT_PIPE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

#if (VSTREAMPROC_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_GetVersionInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) VersionInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if VersionInfoPtr is a NullPointer. */
  if (VersionInfoPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Copy version information. */
    VersionInfoPtr->vendorID = (uint16)VSTREAMPROC_VENDOR_ID;
    VersionInfoPtr->moduleID = (uint16)VSTREAMPROC_MODULE_ID;
    VersionInfoPtr->sw_major_version = (uint8)VSTREAMPROC_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = (uint8)VSTREAMPROC_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = (uint8)VSTREAMPROC_SW_PATCH_VERSION;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_VERSION_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */
}
#endif /* (VSTREAMPROC_VERSION_INFO_API == STD_ON) */

/**********************************************************************************************************************
 *  vStreamProc_Open
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Open(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check initialization state of the component. */
  if (vStreamProc_ModuleInitialized != (uint8)VSTREAMPROC_INIT)
  {
    errorID = VSTREAMPROC_E_UNINIT;
  }
  /* Check if pipe is unavailable. */
  else if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check that the pipe is not opened yet. */
  else if (vStreamProc_GetPipeState(PipeId) != VSTREAMPROC_CLOSED_PIPESTATE)
  {
    errorID = VSTREAMPROC_E_PIPE_STATE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Open the passed pipe. */
    retVal = vStreamProc_Pipe_Open(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_OPEN, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Close
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Close(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Close the passed pipe. */
    retVal = vStreamProc_Pipe_Close(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_CLOSE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Process
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Process(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Route process request to passed pipe. */
    retVal = vStreamProc_Pipe_Process(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_PROCESS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Cancel(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                   errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_ReturnType  retVal  = VSTREAMPROC_FAILED;                                                                 /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Route cancel request to passed pipe. */
    retVal = vStreamProc_Pipe_Cancel(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_CANCEL, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SetMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_SetMode(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType PipeModeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if passed mode is invalid */
  else if ( (PipeModeId >= vStreamProc_GetSizeOfMode()) && (PipeModeId != VSTREAMPROC_NO_MODE) )
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check that the passed mode belongs to this pipe. */
  else if ( (PipeModeId != VSTREAMPROC_NO_MODE) && (vStreamProc_GetPipeIdxOfMode(PipeModeId) != PipeId) )
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue set mode request to passed pipe. */
    retVal = vStreamProc_Pipe_SetMode(PipeId, PipeModeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_SET_MODE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetModeByHandle
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ModeIdType, VSTREAMPROC_CODE) vStreamProc_GetModeByHandle(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeHandleIdType HandleId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_ModeIdType    modeId = VSTREAMPROC_NO_MODE;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Search for the matching mode handle. */
    if ( vStreamProc_HasMode()
      && (HandleId != VSTREAMPROC_NO_HANDLEOFMODE) )
    {
      vStreamProc_ModeIterType  modeIdx;

      for ( modeIdx = vStreamProc_GetModeStartIdxOfPipe(PipeId);
            modeIdx < vStreamProc_GetModeEndIdxOfPipe(PipeId);
            modeIdx++ )
      {
        if (vStreamProc_GetHandleOfMode(modeIdx) == HandleId)
        {
          modeId = (vStreamProc_ModeIdType)modeIdx;

          break;
        }
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_MODE_BY_HANDLE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return modeId;
}

/**********************************************************************************************************************
 *  vStreamProc_AnnounceStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_AnnounceStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_ProduceCallbackType ProduceCbk)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksEntryPoint(PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetDataTypeOfEntryPoint(
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName) != DataTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check passed callback is a null pointer.
   * Hint: NULL_PTR cannot be used here, as data pointer shouldn't be casted to function pointer. */
  else if (ProduceCbk == (vStreamProc_ProduceCallbackType)0)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else if (vStreamProc_DetChecksWaveState_AnnounceStream(
             PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# else
  VSTREAMPROC_DUMMY_STATEMENT(DataTypeId);                                                                              /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue announce stream request to passed pipe. */
    retVal = vStreamProc_Pipe_AnnounceStream(PipeId, EntryPointSymbolicName, ProduceCbk);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_ANNOUNCE_STREAM, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareEntryPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable. */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if EntryPointSymbolicName is an invalid ID. */
  else if (EntryPointSymbolicName >= vStreamProc_GetEntryPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if EntryPointInfo is a null pointer. */
  else if (EntryPointInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set ID in info structure. */
    EntryPointInfo->EntryPointSymbolicName = EntryPointSymbolicName;

    /* #30 Issue prepare request to passed pipe. */
    retVal = vStreamProc_Pipe_PrepareEntryPointInfo(PipeId, EntryPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_PREPARE_ENTRY_POINT_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetEntryPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksEntryPointInfo(PipeId, EntryPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if DataTypeId is an invalid ID. */
  else if (DataTypeId >= vStreamProcConf_DataTypeCount)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set data type in info structure. */
    EntryPointInfo->WriteRequest.StorageInfo.DataTypeInfo.Id = DataTypeId;

    /* #30 Issue getter request to passed pipe. */
    retVal = vStreamProc_Pipe_GetEntryPointInfo(PipeId, EntryPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_ENTRY_POINT_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestEntryPointData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestEntryPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksEntryPointInfo(PipeId, EntryPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if DataTypeId is an invalid ID. */
  else if (DataTypeId >= vStreamProcConf_DataTypeCount)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set data type and request length in info structure. */
    EntryPointInfo->WriteRequest.StorageInfo.DataTypeInfo.Id  = DataTypeId;
    EntryPointInfo->WriteRequest.StorageInfo.RequestLength    = RequestLength;

    /* #30 Issue request to passed pipe. */
    retVal = vStreamProc_Pipe_RequestEntryPointData(PipeId, EntryPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REQUEST_ENTRY_POINT_DATA, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetWriteRequestBuffer
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR), VSTREAMPROC_CODE) vStreamProc_GetWriteRequestBuffer(
  P2CONST(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_CONST) RequestBufferPtr,
  vStreamProc_DataTypeIdType DataTypeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                                         errorID = VSTREAMPROC_E_NO_ERROR;
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) buffer  = NULL_PTR;                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if RequestBufferPtr is a null pointer. */
  if (RequestBufferPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Compare data type of request against expected one. */
  else if (RequestBufferPtr->StorageInfo.DataTypeInfo.Id != DataTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Return buffer pointer of request. */
    buffer = RequestBufferPtr->Buffer;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_WRITE_REQUEST_BUFFER, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(DataTypeId);                                                                              /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return buffer;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeEntryPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_LengthType ProducedLength,
  boolean ReleaseFlag,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksEntryPointInfo(PipeId, EntryPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if ( (vStreamProc_AccessNode_IsEntryNodeLocked(
               vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointInfo->EntryPointSymbolicName) == FALSE)
         && (EntryPointInfo->WriteRequest.StorageInfo.RequestLength > 0u))
  {
    errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set request length and release flag in info structure. */
    EntryPointInfo->WriteRequest.StorageInfo.RequestLength  = ProducedLength;
    EntryPointInfo->WriteRequest.StorageInfo.ReleaseFlag    = ReleaseFlag;

    /* #30 Issue acknowledge request to passed pipe. */
    retVal = vStreamProc_Pipe_AcknowledgeEntryPoint(PipeId, EntryPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_ACKNOWLEDGE_ENTRY_POINT, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_ConsumeCallbackType ConsumeCbk)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetDataTypeOfExitPoint(
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName) != DataTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check passed callback is a null pointer.
   * Hint: NULL_PTR cannot be used here, as data pointer shouldn't be casted to function pointer. */
  else if (ConsumeCbk == (vStreamProc_ConsumeCallbackType)0)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# else
  VSTREAMPROC_DUMMY_STATEMENT(DataTypeId);                                                                              /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue stream request to passed pipe. */
    retVal = vStreamProc_Pipe_RequestStream(PipeId, ExitPointSymbolicName, ConsumeCbk);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REQUEST_STREAM, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareExitPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable. */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if ExitPointId is an invalid ID. */
  else if (ExitPointSymbolicName >= vStreamProc_GetExitPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if ExitPointInfo is a null pointer. */
  else if (ExitPointInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set ID in info structure. */
    ExitPointInfo->ExitPointSymbolicName = ExitPointSymbolicName;

    /* #30 Issue prepare request to passed pipe. */
    retVal = vStreamProc_Pipe_PrepareExitPointInfo(PipeId, ExitPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_PREPARE_EXIT_POINT_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetExitPointInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksExitPointInfo(PipeId, ExitPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if DataTypeId is an invalid ID. */
  else if (DataTypeId >= vStreamProcConf_DataTypeCount)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set data type in info structure. */
    ExitPointInfo->ReadRequest.StorageInfo.DataTypeInfo.Id = DataTypeId;

    /* #30 Issue getter request to passed pipe. */
    retVal = vStreamProc_Pipe_GetExitPointInfo(PipeId, ExitPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_EXIT_POINT_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestExitPointData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestExitPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksExitPointInfo(PipeId, ExitPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if DataTypeId is an invalid ID. */
  else if (DataTypeId >= vStreamProcConf_DataTypeCount)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set data type and request length in info structure. */
    ExitPointInfo->ReadRequest.StorageInfo.DataTypeInfo.Id  = DataTypeId;
    ExitPointInfo->ReadRequest.StorageInfo.RequestLength    = RequestLength;

    /* #30 Issue request to passed pipe. */
    retVal = vStreamProc_Pipe_RequestExitPointData(PipeId, ExitPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REQUEST_EXIT_POINT_DATA, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetReadRequestBuffer
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR), VSTREAMPROC_CODE) vStreamProc_GetReadRequestBuffer(
  P2CONST(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) RequestBufferPtr,
  vStreamProc_DataTypeIdType DataTypeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                                           errorID = VSTREAMPROC_E_NO_ERROR;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) buffer  = NULL_PTR;                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if RequestBufferPtr is a null pointer. */
  if (RequestBufferPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Compare data type of request against expected one. */
  else if (RequestBufferPtr->StorageInfo.DataTypeInfo.Id != DataTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Return buffer pointer of request. */
    buffer = RequestBufferPtr->Buffer;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_READ_REQUEST_BUFFER, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(DataTypeId);                                                                              /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return buffer;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeExitPoint
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_LengthType ConsumedLength,
  boolean ReleaseFlag,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksExitPointInfo(PipeId, ExitPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if ( (vStreamProc_AccessNode_IsExitNodeLocked(
               vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointInfo->ExitPointSymbolicName) == FALSE)
         && (ExitPointInfo->ReadRequest.StorageInfo.RequestLength > 0u))
  {
    errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Set request length and release flag in info structure. */
    ExitPointInfo->ReadRequest.StorageInfo.RequestLength  = ConsumedLength;
    ExitPointInfo->ReadRequest.StorageInfo.ReleaseFlag    = ReleaseFlag;

    /* #30 Issue acknowledge request to passed pipe. */
    retVal = vStreamProc_Pipe_AcknowledgeExitPoint(PipeId, ExitPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_ACKNOWLEDGE_EXIT_POINT, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareAccessPointInfos
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable. */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check access point info structures. */
  else if (vStreamProc_DetChecksAccessPointInfos(EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue prepare request to passed pipe. */
    retVal = vStreamProc_Pipe_PrepareAccessPointInfos(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_PREPARE_ACCESS_POINT_INFOS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareAllAccessPointInfos
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareAllAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check that entry point count matches actual count of pipe. */
  else if (EntryPointCount != vStreamProc_GetEntryPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER_SIZE;
  }
  /* Check that exit point count matches actual count of pipe. */
  else if (ExitPointCount != vStreamProc_GetExitPointLengthOfPipe(PipeId))
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER_SIZE;
  }
  /* Check access point info structures. */
  else if (vStreamProc_DetChecksAccessPointInfos(EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue prepare request to passed pipe. */
    retVal = vStreamProc_Pipe_PrepareAllAccessPointInfos(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_PREPARE_ALL_ACCESS_POINT_INFOS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetAccessPointInfos
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksAccessPointIds(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue getter request to passed pipe. */
    retVal = vStreamProc_Pipe_GetAccessPointInfos(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_ACCESS_POINT_INFOS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestAccessPointData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestAccessPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksAccessPointIds(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue request to passed pipe. */
    retVal = vStreamProc_Pipe_RequestAccessPointData(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REQUEST_ACCESS_POINT_DATA, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeAccessPoints
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeAccessPoints(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_ExitPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_EntryPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksAccessPointIds(
    PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_Pipe_DetChecksAccessPointIsLocked(
    PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue acknowledge request to passed pipe. */
    retVal = vStreamProc_Pipe_AcknowledgeAccessPoints(PipeId, EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_ACKNOWLEDGE_ACCESS_POINTS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ReleaseAllAccessPoints
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ReleaseAllAccessPoints(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue release request to passed pipe. */
    retVal = vStreamProc_Pipe_ReleaseAllAccessPoints(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_RELEASE_ALL_ACCESS_POINTS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_BroadcastSignal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_BroadcastSignal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksBroadcastSignal(PipeId, SignalId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue broadcast signal request to passed pipe. */
    retVal = vStreamProc_Pipe_BroadcastSignal(PipeId, SignalId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_BROADCAST_SIGNAL, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetSignalStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetSignalStatus(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
      /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksBroadcastSignal(PipeId, SignalId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue signal request to passed pipe. */
    retVal = vStreamProc_Pipe_GetSignalStatus(PipeId, SignalId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_SIGNAL_STATUS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RegisterSignalCallback
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RegisterSignalCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalCallbackType SignalCbk,
  vStreamProc_CallbackHandleType CallbackHandle)
{
      /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  if (vStreamProc_DetChecksBroadcastSignal(PipeId, SignalId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (SignalCbk == (vStreamProc_SignalCallbackType)0)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue signal request to passed pipe. */
    retVal = vStreamProc_Pipe_RegisterSignalCallback(PipeId, SignalId, SignalCbk, CallbackHandle);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REGISTER_SIGNAL_CALLBACK, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SetExitPointLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SetExitPointLimit(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_LimitType LimitType,
  vStreamProc_StreamPositionType AbsoluteLimitPosition,
  vStreamProc_StreamPositionType LimitOffset)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;
  uint8                             errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksLimitType(PipeId, ExitPointSymbolicName, LimitType, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue set limit request to passed pipe. */
    retVal = vStreamProc_Pipe_SetExitPointLimit(PipeId, ExitPointSymbolicName, LimitType, AbsoluteLimitPosition, LimitOffset);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_SET_EXITPOINT_LIMIT, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}


/**********************************************************************************************************************
 *  vStreamProc_ClearExitPointLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ClearExitPointLimit(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;
  uint8                             errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksLimit(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue clear limit request to passed pipe. */
    retVal = vStreamProc_Pipe_ClearExitPointLimit(PipeId, ExitPointSymbolicName);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_CLEAR_EXITPOINT_LIMIT, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetExitPointLimitStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetExitPointLimitStatus(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;
  uint8                             errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksLimit(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue limit status request to passed pipe. */
    retVal = vStreamProc_Pipe_GetExitPointLimitStatus(PipeId, ExitPointSymbolicName);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_EXITPOINT_LIMITSTATUS, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}


/**********************************************************************************************************************
 *  vStreamProc_RegisterExitPointLimitCallback
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RegisterExitPointLimitCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_SignalCallbackType LimitCbk,
  vStreamProc_CallbackHandleType CallbackHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;
  uint8                             errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksLimit(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (LimitCbk == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue limit callback registration request to passed pipe. */
    retVal = vStreamProc_Pipe_RegisterExitPointLimitCallback(PipeId, ExitPointSymbolicName, LimitCbk, CallbackHandle);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_REGISTER_EXITPOINT_LIMIT_CALLBACK, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  uint8                 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable. */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if wave type is used in pipe. */
  else if (vStreamProc_DetChecksWaveType(PipeId, WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if wave info pointer is valid. */
  else if (WaveInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Prepare the WaveInfo. */
    vStreamProc_Stream_PrepareWaveInfo(WaveTypeSymbolicName, WaveInfo);

    retVal = VSTREAMPROC_OK;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_PREPARE_WAVE_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);                                                                                  /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_StartEntryPointWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_StartEntryPointWave(                                         /* PRQA S 6080 1 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal = VSTREAMPROC_FAILED;
  uint8                              errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksEntryPoint(PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (WaveInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else if ( (WaveInfo->MetaData.Buffer == NULL_PTR)
         && (WaveInfo->MetaData.StorageInfo.RequestLength > 0u) )
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else if (vStreamProc_DetChecksWaveType(PipeId, WaveInfo->WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksWaveState_StartWave(
             PipeId, EntryPointSymbolicName, WaveInfo->WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Start wave with given information. */
    retVal = vStreamProc_Pipe_StartEntryPointWave(PipeId, EntryPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_START_ENTRY_POINT_WAVE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_EndEntryPointWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_EndEntryPointWave(                                           /* PRQA S 6080 1 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;
  uint8                             errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksEntryPoint(PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (WaveInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else if ((WaveInfo->MetaData.Buffer == NULL_PTR)
    && (WaveInfo->MetaData.StorageInfo.RequestLength > 0u))
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Check if wave type is used in pipe. */
  else if (vStreamProc_DetChecksWaveType(PipeId, WaveInfo->WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_DetChecksWaveState_EndWave(
             PipeId, EntryPointSymbolicName, WaveInfo->WaveTypeSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_AccessNode_IsEntryNodeLocked(
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName) == TRUE)
  {
    errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
  }
  else if (vStreamProc_AccessNode_IsInputStreamPending(
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName) == TRUE)
  {
    errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 End wave with given information. */
    retVal = vStreamProc_Pipe_EndEntryPointWave(PipeId, EntryPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_END_ENTRY_POINT_WAVE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetEntryPointWaveInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetEntryPointWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
/* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if entry point is valid. */
  if (vStreamProc_DetChecksEntryPoint(PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if wave info is valid. */
  else if (vStreamProc_DetChecksWaveInfo(PipeId, WaveInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue request to pipe. */
    retVal = vStreamProc_Pipe_GetEntryPointWaveInfo(PipeId, EntryPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_ENTRY_POINT_WAVE_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetExitPointWaveInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetExitPointWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if exit point is valid. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if wave info is valid. */
  else if (vStreamProc_DetChecksWaveInfo(PipeId, WaveInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue request to pipe. */
    retVal = vStreamProc_Pipe_GetExitPointWaveInfo(PipeId, ExitPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_EXIT_POINT_WAVE_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_CreateSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_CreateSession(
  vStreamProc_PipeIdType PipeId)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal = VSTREAMPROC_FAILED;
  uint8                              errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)

  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for pipe APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId) == VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Create session. */
    retVal = vStreamProc_Pipe_CreateSession(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_CREATE_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetEntryPointInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetEntryPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal = VSTREAMPROC_FAILED;
  uint8                              errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for entry point APIs. */
  if (vStreamProc_DetChecksEntryPointInfo(PipeId, EntryPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId) == VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Get entry point session info. */
    retVal = vStreamProc_Pipe_GetEntryPointInfoOfSession(PipeId, EntryPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_ENTRY_POINT_INFO_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetExitPointInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetExitPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
    /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal = VSTREAMPROC_FAILED;
  uint8                              errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for exit point APIs. */
  if (vStreamProc_DetChecksExitPointInfo(PipeId, ExitPointInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId)== VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Get entry point session info. */
    retVal = vStreamProc_Pipe_GetExitPointInfoOfSession(PipeId, ExitPointInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_EXIT_POINT_INFO_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetEntryPointWaveInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetEntryPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if entry point is valid. */
  if (vStreamProc_DetChecksEntryPoint(PipeId, EntryPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if wave info is valid. */
  else if (vStreamProc_DetChecksWaveInfo(PipeId, WaveInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue request to pipe. */
    retVal = vStreamProc_Pipe_GetEntryPointWaveInfoOfSession(PipeId, EntryPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_ENTRY_POINT_WAVE_INFO_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetExitPointWaveInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetExitPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if exit point is valid. */
  if (vStreamProc_DetChecksExitPoint(PipeId, ExitPointSymbolicName, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  /* Check if wave info is valid. */
  else if (vStreamProc_DetChecksWaveInfo(PipeId, WaveInfo, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Issue request to pipe. */
    retVal = vStreamProc_Pipe_GetExitPointWaveInfoOfSession(PipeId, ExitPointSymbolicName, WaveInfo);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_EXIT_POINT_WAVE_INFO_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetMaxDataSizeOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_GetMaxDataSizeOfSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType returnedLength = 0u;
  uint8                         errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check if pipe is unavailable. */
  if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId) == VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Get maximum session data length. */
    returnedLength = vStreamProc_Pipe_GetMaxDataSizeOfSession(PipeId);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_MAX_DATA_SIZE_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return returnedLength;
}

/**********************************************************************************************************************
 *  vStreamProc_GetDataOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetDataOfSession(
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal  = VSTREAMPROC_FAILED;
  uint8                         errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Perform common DET checks for exit point APIs. */
  if (vStreamProc_DetChecksPipe(PipeId, &errorID) != VSTREAMPROC_E_NO_ERROR)
  {
    /* Error ID already set by check function. */
  }
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId) == VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  else if (SessionData == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else if (*Length == 0u)
  {
    errorID = VSTREAMPROC_E_PARAM_SIZE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Get session data. */
    retVal = vStreamProc_Pipe_GetDataOfSession(PipeId, SessionData, Length);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_GET_DATA_OF_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RestoreSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RestoreSession(                                              /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  vStreamProc_LengthType SessionDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_FAILED;
  uint8                         errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters. */
  /* Check initialization state of the component. */
  if (vStreamProc_ModuleInitialized != (uint8)VSTREAMPROC_INIT)
  {
    errorID = VSTREAMPROC_E_UNINIT;
  }
  /* Check if the pipe id is valid. */
  else if (PipeId >= vStreamProc_GetSizeOfPipe())
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  /* Check if session handling is enabled for pipe. */
  else if (vStreamProc_GetSessionIdxOfPipe(PipeId) == VSTREAMPROC_NO_SESSIONIDXOFPIPE)
  {
    errorID = VSTREAMPROC_E_FEATURE_NOT_ENABLED;
  }
  /* Check if the pointer to the session data is valid. */
  else if (SessionData == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Check if the session data length is > 0. */
  else if (SessionDataLength == 0u)
  {
    errorID = VSTREAMPROC_E_PARAM_SIZE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Restore session data. */
    retVal = vStreamProc_Pipe_RestoreSession(PipeId, SessionData, SessionDataLength);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(
      VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET,
      VSTREAMPROC_API_ID_RESTORE_SESSION, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}


#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* Justification for module specific MISRA deviations:

MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj:
Reason:     Cast is necessary to convert generic workspace/configuration to specific workspace/configuration.
Risk:       Alignment issues may occur.
Prevention: Covered by code review.

MD_vStreamProc_Dir1.1_0314_CastPtrObjPtrVoid:
Reason:     Cast is necessary to convert generic workspace/configuration to specific workspace/configuration.
Risk:       Alignment issues may occur.
Prevention: Covered by code review.

MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature:
Reason:     Function shares it signature with a set of similar functions. All of those are accessed through a function
            pointer look-up table.
            As some of those functions modify the value, the parameter cannot be a 'pointer to const'.
Risk:       No risk.
Prevention: No prevention necessary.

MD_vStreamProc_Rule10.1_4527_EnumInc:
Reason:     Enum values increase the readability and maintainability of state machines.
Risk:       Values outside the enum scope are calculated.
Prevention: The enumerated is checked against its upper border right after the increment operation.

*/

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc.c
 *********************************************************************************************************************/
