/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_Limit.c
 *        \brief  vStreamProc source code file for limit handling
 *
 *      \details  Implementation of the limit related functionality.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_LIMIT_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Limit.h"
#include "vStreamProc_Scheduler.h"
#include "vStreamProc_Snapshot.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 **********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

 /*********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetRemainingLength
 *********************************************************************************************************************/
/*!
 * \brief         Get the length remaining from the available length.
 * \details       Additionally checks whether the requested length exceeds the available length.
 * \param[in]     AvailableLength                     Length which is available.
 * \param[in]     RequestedLength                     Length which is requested.
 * \param[out]    RemainingLength                     Length which remains from the available length.
 * \return        VSTREAMPROC_LIMIT_RESULT_NONE       Requested length doesn't exceed the available length.
 *                VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Requested length exceeds the available length.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetRemainingLength(
  vStreamProc_StreamPositionType AvailableLength,
  vStreamProc_StreamPositionType RequestedLength,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) RemainingLength);

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetAvailableLength
 *********************************************************************************************************************/
/*!
 * \brief         Get the length available behind the end of the stream.
 * \details       The length is either limited by an active limit, or truncated to the maximum stream length.
 *                Additionally checks whether the stream end reached or exceeds the limit.
 * \param[in]     LimitStart                          Absolute start position of the limit.
 * \param[in]     LimitOffset                         Relative offset of the effective limit position.
 * \param[in]     StreamStart                         Absolute start position of the stream.
 * \param[in]     StreamOffset                        Relative offset of the stream end.
 * \param[out]    AvailableLength                     Length which is available.
 * \return        VSTREAMPROC_LIMIT_RESULT_NONE       Stream doesn't exceed the limit.
 *                VSTREAMPROC_LIMIT_RESULT_TRUNCATED  Stream doesn't exceed the limit, but length was truncated
 *                                                    to the maximum stream length.
 *                VSTREAMPROC_LIMIT_RESULT_REACHED    Stream reached the limit.
 *                VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Stream exceeds the limit.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetAvailableLength(
  vStreamProc_StreamPositionType LimitStart,
  vStreamProc_StreamPositionType LimitOffset,
  vStreamProc_StreamPositionType StreamStart,
  vStreamProc_StreamPositionType StreamOffset,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetLimitedLength
 *********************************************************************************************************************/
/*!
 * \brief         Get the length available behind the end of the stream.
 * \details       The length is either limited by an active limit, or truncated to the maximum stream length.
 *                Additionally checks whether the stream end exceeds the limit.
 *                Stream range is passed as parameters.
 * \param[in]     StreamId                            ID of the stream to which a potential limit is applied.
 * \param[in]     StreamStart                         Absolute start position of the stream.
 * \param[in]     StreamOffset                        Relative offset of the stream end.
 * \param[out]    AvailableLength                     Length which is available.
 * \return        VSTREAMPROC_LIMIT_RESULT_NONE       Stream doesn't exceed the limit.
 *                VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Stream exceeds the limit.
 *                VSTREAMPROC_LIMIT_RESULT_TRUNCATED  Stream doesn't exceed the limit, but length was truncated to
 *                                                    maximum stream length.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetLimitedLength(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamPositionType StreamStart,
  vStreamProc_StreamPositionType StreamOffset,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetLimitedLengthOfStream
 *********************************************************************************************************************/
/*!
 * \brief         Get the length available behind the end of the stream.
 * \details       The length is either limited by an active limit, or truncated to the maximum stream length.
 *                Additionally checks whether the stream end exceeds the limit.
 *                Stream range is extracted from data model, based on stream ID.
 * \param[in]     StreamId                            ID of the stream.
 * \param[out]    AvailableLength                     Length which is available.
 * \return        VSTREAMPROC_LIMIT_RESULT_NONE       Stream doesn't exceed the limit.
 *                VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Stream exceeds the limit.
 *                VSTREAMPROC_LIMIT_RESULT_TRUNCATED  Stream doesn't exceed the limit, but length was truncated to
 *                                                    maximum stream length.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetLimitedLengthOfStream(
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_Limit_CheckLimitPreconditions()
 *********************************************************************************************************************/
/*!
 * \brief         Checks limit preconditions for set limit API.
 * \details       -
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \param[in]     LimitInfo               Description of the new limit.
 *                - Position              Absolute start position of the limit.
 *                - Offset                Relative offset of the effective limit position.
 *                - Type                  Type of limit (VSTREAMPROC_LIMIT_SOFT or VSTREAMPROC_LIMIT_HARD)
 * \return        VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Preconditions are not met.
 *                VSTREAMPROC_LIMIT_RESULT_REACHED    Preconditions are met, limit is already reached by stream.
 *                VSTREAMPROC_LIMIT_RESULT_TRUNCATED  Preconditions are met, limit can never be reached by stream,
 *                                                    as beyond maximum stream length.
 *                VSTREAMPROC_LIMIT_RESULT_NONE       Preconditions are met, limit can be set.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_LimitResultType, VSTREAMPROC_CODE) vStreamProc_Limit_CheckLimitPreconditions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo);

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetPromptLimit
 *********************************************************************************************************************/
/*!
 * \brief         Set a prompt limit on the stream.
 * \details       A prompt limit becomes effective as soon as the associated signal (typically OutputSnapshotRequested)
 *                is acknowledged.
 *                An existing soft or hard limit remains active until then.
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \return        VSTREAMPROC_FAILED      Limit could not be set.
 * \return        VSTREAMPROC_OK          Limit was set successfully.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetPromptLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId);

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetPositionLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Set a limit to a specific position in the stream.
 * \details       -
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \param[in]     LimitInfo               Description of the new limit.
 *                - Position              Absolute start position of the limit.
 *                - Offset                Relative offset of the effective limit position.
 *                - Type                  Type of limit (VSTREAMPROC_LIMIT_SOFT or VSTREAMPROC_LIMIT_HARD)
 * \return        VSTREAMPROC_FAILED      Limit could not be set.
 * \return        VSTREAMPROC_OK          Limit was set successfully.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetPositionLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo);

/**********************************************************************************************************************
 *  vStreamProc_Limit_TriggerStreamLimit
 *********************************************************************************************************************/
/*!
 * \brief         Issues the signal associated with a limit, if the limit applied to the stream is of the specific type.
 * \details       -
 * \param[in]     StreamId            ID of the stream.
 * \param[in]     StreamLimitId       Index of the stream limit structure.
 * \param[in]     LimitType           Limit type for which the signal shall be issued.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_TriggerStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_TypeOfStreamLimitType LimitType );

/**********************************************************************************************************************
 *  vStreamProc_Limit_TriggerStorageAvailable()
 *********************************************************************************************************************/
/*!
 * \brief         Trigger storage available signal at stream producers output port.
 * \details       -
 * \param[in]     StreamId            Id of stream.
 * \param[in]     PrevLimitedLength   The previously limited length (before applying the new limit).
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_TriggerStorageAvailable(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_LengthType PrevLimitedLength );

/**********************************************************************************************************************
 *  vStreamProc_Limit_ConvertLimitType
 *********************************************************************************************************************/
/*!
 * \brief         Convert external limit type to internal limit type.
 * \details       -
 * \param[in]     LimitType                             External limit type.
 * \return        VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT    If external type is VSTREAMPROC_LIMIT_SOFT.
 *                VSTREAMPROC_HARD_TYPEOFSTREAMLIMIT    Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_TypeOfStreamLimitType, VSTREAMPROC_CODE) vStreamProc_Limit_ConvertLimitType(
  vStreamProc_LimitType LimitType);

/**********************************************************************************************************************
 *  vStreamProc_Limit_SignalCallback()
 *********************************************************************************************************************/
/*!
 * \brief         Internal limit signal handler.
 * \details       -
 * \param[in]     PipeId                ID of the pipe.
 * \param[in]     SignalId              ID of the signal.
 * \param[in]     SignalResult          Result of the signal handler execution.
 * \param[in]     CallbackHandle        Stream ID associated with limit.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_SignalCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_ReturnType  SignalResult,
  vStreamProc_CallbackHandleType CallbackHandle);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetRemainingLength
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetRemainingLength(
  vStreamProc_StreamPositionType AvailableLength,
  vStreamProc_StreamPositionType RequestedLength,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) RemainingLength)
{
  vStreamProc_LimitResultType     retVal          = VSTREAMPROC_LIMIT_RESULT_NONE;
  vStreamProc_LengthType          remainingLength = 0u;

  /* #10 Calculate the length which remains from the available length. */
  if (RequestedLength < AvailableLength)
  {
    remainingLength = AvailableLength - RequestedLength;
  }
  else
  {
    /* Hint: remainingLength = 0u */
    /* #20 Check whether the requested length exceeds the available length. */
    if (RequestedLength > AvailableLength)
    {
      retVal = VSTREAMPROC_LIMIT_RESULT_EXCEEDED;
    }
  }

  *RemainingLength = remainingLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetAvailableLength
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetAvailableLength(
  vStreamProc_StreamPositionType LimitStart,
  vStreamProc_StreamPositionType LimitOffset,
  vStreamProc_StreamPositionType StreamStart,
  vStreamProc_StreamPositionType StreamOffset,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength)
{
  vStreamProc_LimitResultType     retVal          = VSTREAMPROC_LIMIT_RESULT_NONE;
  vStreamProc_StreamPositionType  availableFront  = 0u;
  vStreamProc_StreamPositionType  availableInside = 0u;
  vStreamProc_LengthType          availableLength;
  vStreamProc_LengthType          streamRemainder;

  /* #10 If stream start position lies in front of limit start position */
  if (StreamStart < LimitStart)
  {
    vStreamProc_StreamPositionType distanceStart = LimitStart - StreamStart;

    /* #20 If stream ends before limit start position */
    if (StreamOffset < distanceStart)
    {
      /* #30 Calculate length available before limit start position. */
      availableFront   = distanceStart - StreamOffset;
      /* #40 Complete length inside of limit range is available as well. */
      availableInside  = LimitOffset;
    }
    /* #50 Otherwise: */
    else
    {
      /* #60 Nothing is available before limit start position. */
      /* Hint: availableFront = 0u */

      /* #70 Determine length available inside of limit range (after stream end)
       *     or whether stream exceeds limit.
       */
      vStreamProc_StreamPositionType distanceOffset = StreamOffset - distanceStart;

      retVal = vStreamProc_Limit_GetRemainingLength(LimitOffset, distanceOffset, &availableInside);
    }
  }
  /* #80 Otherwise: */
  else
  {
    /* #90 Nothing is available before limit start position. */
    /* Hint: availableFront = 0u */

    /* #100 Determine length available inside of limit range (after stream start)
     *     or whether stream start already exceeds limit.
     */
    /* Hint: Stream offset isn't considered yet to avoid potential range overflow,
     *       when adding directly to the distance.
     */
    vStreamProc_StreamPositionType distanceStart = StreamStart - LimitStart;

    retVal = vStreamProc_Limit_GetRemainingLength(LimitOffset, distanceStart, &availableInside);

    /* #110 If there is length available inside of limit range (after stream start) */
    if (availableInside > 0u)
    {
      /* #120 Determine length available inside of limit range (after stream end)
       *     or whether stream exceeds limit.
       */
      vStreamProc_StreamPositionType remainderOffset = LimitOffset - distanceStart;

      retVal = vStreamProc_Limit_GetRemainingLength(remainderOffset, StreamOffset, &availableInside);
    }
    /* #130 Otherwise: Stream start either lies exactly on effective limit position, or already exceeded the limit. */
    else
    {
      /* #140 If stream has any length: Stream exceeds limit. */
      if (StreamOffset > 0u)
      {
        retVal = VSTREAMPROC_LIMIT_RESULT_EXCEEDED;
      }
    }
  }

  /* Hint: Do not add availableFront and availableInside directly to prevent potential range overflow.
   *       Maximum effective stream position may lie in between start and effective position of limit.
   */
  streamRemainder = VSTREAMPROC_MAX_LENGTH - (StreamOffset + availableFront);

  /* #150 If available length exceeds maximum stream length */
  if (streamRemainder < availableInside)
  {
    /* #160 Truncate to maximum stream length. */
    availableLength = availableFront + streamRemainder;

    retVal = VSTREAMPROC_LIMIT_RESULT_TRUNCATED;
  }
  else
  {
    availableLength = availableFront + availableInside;

    /* #170 If limit isn't exceeded and available length is zero: Stream reached limit. */
    if ( (retVal != VSTREAMPROC_LIMIT_RESULT_EXCEEDED )
      && (availableLength == 0u) )
    {
      retVal = VSTREAMPROC_LIMIT_RESULT_REACHED;
    }
  }

  *AvailableLength = availableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetLimitedLength
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetLimitedLength(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamPositionType StreamStart,
  vStreamProc_StreamPositionType StreamOffset,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength)
{
  vStreamProc_LimitResultType retVal          = VSTREAMPROC_LIMIT_RESULT_NONE;
  vStreamProc_LengthType      availableLength = VSTREAMPROC_MAX_LENGTH;
  vStreamProc_LengthType      streamRemainder = VSTREAMPROC_MAX_LENGTH - StreamOffset;

  if (vStreamProc_HasStreamLimit())
  {
    vStreamProc_StreamLimitIdxOfStreamInfoType streamLimitIdx = vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);

    /* #10 If active stream limit is linked with stream. */
    if ( (streamLimitIdx != VSTREAMPROC_NO_STREAMLIMITIDXOFSTREAMINFO)
      && (vStreamProc_GetTypeOfStreamLimitInfo(streamLimitIdx) != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT) )
    {
      /* #20 Get the length available behind the end of the stream.*/
      vStreamProc_StreamPositionType  limitStart  = vStreamProc_GetPositionOfStreamLimitInfo(streamLimitIdx);
      vStreamProc_StreamPositionType  limitOffset = vStreamProc_GetOffsetOfStreamLimitInfo(streamLimitIdx);

      retVal = vStreamProc_Limit_GetAvailableLength(limitStart, limitOffset, StreamStart, StreamOffset, &availableLength);
    }
    /* #30 Otherwise: Report the length truncated to the maximum stream length. */
  }

  if (availableLength > streamRemainder)
  {
    availableLength = streamRemainder;

    retVal = VSTREAMPROC_LIMIT_RESULT_TRUNCATED;
  }

  *AvailableLength = availableLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetLimitedLengthOfStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_LimitResultType, AUTOMATIC) vStreamProc_Limit_GetLimitedLengthOfStream(
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) AvailableLength)
{
  vStreamProc_LimitResultType         retVal = VSTREAMPROC_LIMIT_RESULT_NONE;
  vStreamProc_StreamPositionInfoType  streamPositionInfo;
  vStreamProc_StreamPositionType      streamStart;
  vStreamProc_StreamPositionType      streamOffset;

  /* #10 Get current produce position. */
  vStreamProc_Stream_GetProducePosition(StreamId, 0u, 0u, &streamPositionInfo);

  streamStart   = streamPositionInfo.AbsolutePositionStart;
  streamOffset  = streamPositionInfo.RelativePosition;

  /* #20 Get the length available behind the end of the stream. */
  retVal = vStreamProc_Limit_GetLimitedLength(StreamId, streamStart, streamOffset, AvailableLength);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_CheckLimitPreconditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
/* PRQA S 6080 1 */ /* MD_MSR_STMIF */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_LimitResultType, VSTREAMPROC_CODE) vStreamProc_Limit_CheckLimitPreconditions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo)
{
  vStreamProc_LimitResultType       retVal            = VSTREAMPROC_LIMIT_RESULT_EXCEEDED;
  vStreamProc_WaveIdType            waveId            = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveStateType         waveState         = vStreamProc_GetStateOfWaveVarInfo(waveId);
  vStreamProc_TypeOfStreamLimitType currentLimitType  = vStreamProc_GetTypeOfStreamLimitInfo(StreamLimitId);
  boolean                           writelockPending;

  if (vStreamProc_IsWriteRequestLockOfStreamInfo(StreamId))
  {
    writelockPending = TRUE;
  }
  else
  {
    writelockPending = FALSE;
  }

  /* #10 Accept any limit if wave state is idle. */
  if ((waveState == VSTREAMPROC_WAVESTATE_UNINIT) || (waveState == VSTREAMPROC_WAVESTATE_IDLE))
  {
    retVal = VSTREAMPROC_LIMIT_RESULT_NONE;
  }
  /* #20 Accept any limits greater or equal than existing limit if wave is active and write buffer is locked. */
  else if ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
         && (currentLimitType != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT)
         && (writelockPending == TRUE) )
  {
    vStreamProc_LengthType distanceFront;

    /* Hint: Check whether new limit reached or exceeded the existing limit,
     *       by treating the new limit as it would be a stream.
     */
    (void)vStreamProc_Limit_GetLimitedLength(StreamId, LimitInfo->Position, LimitInfo->Offset, &distanceFront);

    if (distanceFront == 0u)
    {
      retVal = VSTREAMPROC_LIMIT_RESULT_NONE;
    }
  }
  /* #30 Check new limit against current producing position if wave is ending OR active with unlocked write buffer. */
  else if ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING))
         || ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
           && (writelockPending == FALSE) ) )
  {
    vStreamProc_StreamPositionType  streamStart   = vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId);
    vStreamProc_StreamPositionType  streamOffset  = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);
    vStreamProc_LengthType          limitedLength;

    /* Hint: Will return REACHED or EXCEEDED, depending on effective stream position in relation to new limit. */
    retVal = vStreamProc_Limit_GetAvailableLength(
      LimitInfo->Position, LimitInfo->Offset, streamStart, streamOffset, &limitedLength);
  }
  else
  {
    /* EXCEEDED already set. */
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetPromptLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetPromptLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasStreamLimit())
  {
    vStreamProc_TypeOfStreamLimitType configuredLimitType = vStreamProc_GetTypeOfStreamLimit(StreamLimitId);

    /* #10 Check whether soft limit is configured. */
    if ( (configuredLimitType == VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT)
      || (configuredLimitType == VSTREAMPROC_SOFTORHARD_TYPEOFSTREAMLIMIT) )
    {
      /* #20 Apply prompt limit. */
      vStreamProc_SetStreamLimitIdxOfStreamInfo(StreamId, (vStreamProc_StreamLimitIdxOfStreamInfoType)StreamLimitId);
      vStreamProc_SetPromptActiveOfStreamLimitInfo(StreamLimitId, TRUE);

      /* #30 Issue signal associated with limit. */
      vStreamProc_Limit_TriggerStreamLimit(StreamId, vStreamProc_GetTypeOfStreamLimitInfo(StreamLimitId));

      retVal = VSTREAMPROC_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetPositionLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetPositionLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if ( (vStreamProc_HasStreamLimit())
    && (vStreamProc_HasExternalSignalDef()) )
  {
    vStreamProc_TypeOfStreamLimitType configuredLimitType = vStreamProc_GetTypeOfStreamLimit(StreamLimitId);
    vStreamProc_LimitResultType       limitResult         = VSTREAMPROC_LIMIT_RESULT_EXCEEDED;
    vStreamProc_TypeOfStreamLimitType requestedLimitType;

    /* #10 Convert limit type. */
    requestedLimitType = vStreamProc_Limit_ConvertLimitType(LimitInfo->Type);

    /* #20 Check requested against configured limit type. */
    if ( (requestedLimitType == configuredLimitType)
      || (configuredLimitType == VSTREAMPROC_SOFTORHARD_TYPEOFSTREAMLIMIT) )
    {
      /* #30 Check preconditions for setting the limit. */
      limitResult = vStreamProc_Limit_CheckLimitPreconditions(StreamId, StreamLimitId, LimitInfo);
    }

    if (limitResult != VSTREAMPROC_LIMIT_RESULT_EXCEEDED)
    {
      vStreamProc_LengthType              currentLimitedLength  = 0u;
      vStreamProc_TypeOfStreamLimitType   currentLimitType      =
        vStreamProc_GetTypeOfStreamLimitInfo(StreamLimitId);
      vStreamProc_ExternalSignalDefIdType externalSignalDefId   =
        vStreamProc_GetExternalSignalDefIdxOfStreamLimit(StreamLimitId);
      vStreamProc_WaveIdType              waveId                = vStreamProc_GetWaveStartIdxOfStream(StreamId);
      vStreamProc_WaveStateType           waveState             = vStreamProc_GetStateOfWaveVarInfo(waveId);

      /* #40 Overwrite the current limit type based on the wave state. */
      /* Hint: If the wave is uninit/idle, the current limit is considered as no limit to avoid potential
               StorageAvailable signals. */
      if ((waveState == VSTREAMPROC_WAVESTATE_UNINIT) || (waveState == VSTREAMPROC_WAVESTATE_IDLE))
      {
        currentLimitType = VSTREAMPROC_NO_TYPEOFSTREAMLIMIT;
      }

      if (currentLimitType != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT)
      {
        (void)vStreamProc_Limit_GetLimitedLengthOfStream(StreamId, &currentLimitedLength);
      }

      /* #50 Apply limit. */
      vStreamProc_SetStreamLimitIdxOfStreamInfo(StreamId, (vStreamProc_StreamLimitIdxOfStreamInfoType)StreamLimitId);
      vStreamProc_SetTypeOfStreamLimitInfo(StreamLimitId, requestedLimitType);
      vStreamProc_SetPositionOfStreamLimitInfo(StreamLimitId, LimitInfo->Position);
      vStreamProc_SetOffsetOfStreamLimitInfo(StreamLimitId, LimitInfo->Offset);

      /* #60 Initialize signal result. */
      vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_PENDING);

      /* #70 Issue storage available when limit is updated and additional free space becomes available. */
      if (currentLimitType != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT)
      {
        vStreamProc_Limit_TriggerStorageAvailable(StreamId, currentLimitedLength);
      }

      retVal = VSTREAMPROC_OK;
    }

    /* #80 Trigger soft limit in case that new limit is on current write position. */
    if (limitResult == VSTREAMPROC_LIMIT_RESULT_REACHED)
    {
      vStreamProc_Limit_TriggerStreamLimit(StreamId, VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_TriggerStreamLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_TriggerStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_TypeOfStreamLimitType LimitType )
{
  if ( (vStreamProc_HasStreamLimit())
    && (vStreamProc_HasExternalSignalDef()) )
  {
    vStreamProc_StreamLimitIdxOfStreamInfoType  streamLimitIdx    =
      vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);
    vStreamProc_TypeOfStreamLimitType           referenceType     =
      vStreamProc_GetTypeOfStreamLimitInfo(streamLimitIdx);
    vStreamProc_NamedOutputPortIdType           namedOutputPortId =
      vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);

    /* #10 Trigger registered limit signal if necessary, and handle callback, if configured. */
    if ( (namedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
      && (referenceType == LimitType) )
    {
      vStreamProc_ExternalSignalDefIdType externalSignalDefId =
        vStreamProc_GetExternalSignalDefIdxOfStreamLimit(streamLimitIdx);
      vStreamProc_SignalIdType            signalId            =
        vStreamProc_GetSignalIdxOfExternalSignalDef(externalSignalDefId);

      if (vStreamProc_Scheduler_SetOutputPortSignal(namedOutputPortId, signalId) == E_OK)
      /* Signal Handler available. Signal queued. */
      {
        if (vStreamProc_GetPendingCountOfExternalSignalInfo(externalSignalDefId) == 0u)
        {
          vStreamProc_IncPendingCountOfExternalSignalInfo(externalSignalDefId);
        }
        vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_PENDING);
      }
      else
      /* No signal handler available or the signal handler is unregistered. */
      {
        vStreamProc_ProcessingNodeIdType  processingNodeId  =
          vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(namedOutputPortId);
        vStreamProc_SignalCallbackType    signalCallbackFct =
          vStreamProc_GetSignalCallbackOfExternalSignalInfo(externalSignalDefId);

        /* #20 If configured signal is OutputSnapshotRequested and there is no signal handler
               or the signal handler is unregistered, start snapshot creation directly. */
        if (signalId == vStreamProcConf_vStreamProcSignal_vStreamProc_OutputSnapshotRequested)
        {
          vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(processingNodeId, namedOutputPortId);
        }

        vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_OK);

        /* Call signal callback if available. */
        if (signalCallbackFct != (vStreamProc_SignalCallbackType)0)
        {
          vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(processingNodeId);

          signalCallbackFct(
            pipeId,
            signalId,
            vStreamProc_GetResultOfExternalSignalInfo(externalSignalDefId),
            vStreamProc_GetSignalCallbackHandleOfExternalSignalInfo(externalSignalDefId));
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_TriggerStorageAvailable
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_TriggerStorageAvailable(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_LengthType PrevLimitedLength )
{
  vStreamProc_NamedOutputPortIdType namedOutputPortId = vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);

  /* #10 Issue storage available when there is an active producer for the stream
   *      and additional free space becomes available.
   */
  if (namedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
  {
    vStreamProc_LengthType limitedLength;

    (void)vStreamProc_Limit_GetLimitedLengthOfStream(StreamId, &limitedLength);

    if (!vStreamProc_Stream_IsOutputPortVirtual(namedOutputPortId))
    {
      vStreamProc_LengthType availableLength = vStreamProc_Stream_GetAvailableStorageLength(StreamId);

      if (availableLength < limitedLength)
      {
        limitedLength = availableLength;
      }
    }

    if (limitedLength > PrevLimitedLength)
    {
      (void)vStreamProc_Scheduler_SetOutputPortSignal(namedOutputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_ConvertLimitType
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_TypeOfStreamLimitType, VSTREAMPROC_CODE) vStreamProc_Limit_ConvertLimitType(
  vStreamProc_LimitType LimitType)
{
  vStreamProc_TypeOfStreamLimitType retVal;

  /* #10 Convert limit type. */
  if (LimitType == VSTREAMPROC_LIMIT_SOFT)
  {
    retVal = VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT;
  }
  else
  {
    retVal = VSTREAMPROC_HARD_TYPEOFSTREAMLIMIT;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_SignalCallback
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_SignalCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_ReturnType  SignalResult,
  vStreamProc_CallbackHandleType CallbackHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamIdType                    streamId           = CallbackHandle;
  vStreamProc_StreamLimitIdxOfStreamInfoType  streamLimitId      = vStreamProc_GetStreamLimitIdxOfStreamInfo(streamId);
  vStreamProc_SignalCallbackType              signalCallbackFct  = vStreamProc_GetCallbackOfStreamLimitInfo(streamLimitId);

  /* Hint: Since the signal callback was called, there must be a valid entry for streamLimitId. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if we are called in prompt mode or not. */
  if (vStreamProc_IsPromptActiveOfStreamLimitInfo(streamLimitId) == TRUE)
  {
    /* #20 Get produce position (off lowest level). */
    vStreamProc_WaveIdType          waveId        = vStreamProc_GetWaveStartIdxOfStream(streamId);
    vStreamProc_StreamPositionType  streamStart   = vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId);
    vStreamProc_StreamPositionType  streamOffset  = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);

    /* #30 Reset prompt flag. */
    vStreamProc_SetPromptActiveOfStreamLimitInfo(streamLimitId, FALSE);

    /* #40 Apply soft limit to current effective produce position. */
    vStreamProc_SetTypeOfStreamLimitInfo(streamLimitId, VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT);
    vStreamProc_SetPositionOfStreamLimitInfo(streamLimitId, streamStart);
    vStreamProc_SetOffsetOfStreamLimitInfo(streamLimitId, streamOffset);
  }

  /* #50 Execute limit callback if set. */
  if (signalCallbackFct != NULL_PTR)
  {
    signalCallbackFct(
      PipeId,
      SignalId,
      SignalResult,
      vStreamProc_GetCallbackHandleOfStreamLimitInfo(streamLimitId));
  }
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Limit_InitStreamLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_InitStreamLimit(
  vStreamProc_PipeIdType PipeId)
{
  vStreamProc_ExitPointIterType exitPointIdx;

  /* #10 Iterate over the exit points and init referenced stream limits. */
  for ( exitPointIdx = vStreamProc_GetExitPointStartIdxOfPipe(PipeId);
        exitPointIdx < vStreamProc_GetExitPointEndIdxOfPipe(PipeId);
        exitPointIdx++)
  {
    vStreamProc_StreamLimitIdxOfExitPointType streamLimitId = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointIdx);

    if (streamLimitId != VSTREAMPROC_NO_STREAMLIMITIDXOFEXITPOINT)
    {
      vStreamProc_ExternalSignalDefIdType externalSignalDefId =
        vStreamProc_GetExternalSignalDefIdxOfStreamLimit(streamLimitId);

      vStreamProc_SetCallbackOfStreamLimitInfo(streamLimitId, NULL_PTR);
      vStreamProc_SetCallbackHandleOfStreamLimitInfo(streamLimitId, 0u);
      vStreamProc_SetPromptActiveOfStreamLimitInfo(streamLimitId, FALSE);
      vStreamProc_SetTypeOfStreamLimitInfo(streamLimitId, VSTREAMPROC_NO_TYPEOFSTREAMLIMIT);
      vStreamProc_SetPositionOfStreamLimitInfo(streamLimitId, 0u);
      vStreamProc_SetOffsetOfStreamLimitInfo(streamLimitId, 0u);

      vStreamProc_SetSignalCallbackOfExternalSignalInfo(externalSignalDefId, vStreamProc_Limit_SignalCallback);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetStreamLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType            retVal            = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasStreamLimit())
  {
    /* #10 Check if a limit needs to be set or just the prompt limit type needs to be evaluated. */
    if (LimitInfo->Type == VSTREAMPROC_LIMIT_PROMPT)
    {
      retVal = vStreamProc_Limit_SetPromptLimit(StreamId, StreamLimitId);
    }
    else
    {
      retVal = vStreamProc_Limit_SetPositionLimit(StreamId, StreamLimitId, LimitInfo);
    }

    if (retVal == VSTREAMPROC_OK)
    {
      vStreamProc_ExternalSignalDefIdType externalSignalDefId = vStreamProc_GetExternalSignalDefIdxOfStreamLimit(StreamLimitId);

      /* #20 Provide stream ID to signal callback via signal callback handle. */
      vStreamProc_SetSignalCallbackHandleOfExternalSignalInfo(externalSignalDefId, StreamId);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_ClearStreamLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 6080 1 */ /* MD_MSR_STMIF */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_ClearStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasStreamLimit())
  {
    vStreamProc_LengthType                      currentLimitedLength  = 0u;
    vStreamProc_StreamLimitIdxOfStreamInfoType  activeStreamLimitId   =
      vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);

    /* #10 Check if limit was configured and can be applied. */
    boolean lastActiveState = FALSE;

    retVal = VSTREAMPROC_OK;

    if (vStreamProc_GetTypeOfStreamLimitInfo(StreamLimitId) != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT)
    {
      (void)vStreamProc_Limit_GetLimitedLengthOfStream(StreamId, &currentLimitedLength);
      lastActiveState = TRUE;
    }

    /* #20 Disable limit. */
    vStreamProc_SetTypeOfStreamLimitInfo(StreamLimitId, VSTREAMPROC_NO_TYPEOFSTREAMLIMIT);
    vStreamProc_SetPositionOfStreamLimitInfo(StreamLimitId, 0u);

    /* #30 Remove link to limit from stream if requested for active limit. */
    if (StreamLimitId == activeStreamLimitId)
    {
      vStreamProc_SetStreamLimitIdxOfStreamInfo(StreamId, VSTREAMPROC_NO_STREAMLIMITIDXOFSTREAMINFO);
    }

    /* #40 Reinitialize signal result. */
    if (vStreamProc_HasExternalSignalDef())
    {
      vStreamProc_ExternalSignalDefIdType externalSignalDefId = vStreamProc_GetExternalSignalDefIdxOfStreamLimit(StreamLimitId);

      vStreamProc_SetResultOfExternalSignalInfo(externalSignalDefId, VSTREAMPROC_OK);
    }

    /* #50 Issue storage available when limit is updated and additional free space becomes available. */
    if (lastActiveState == TRUE)
    {
      vStreamProc_Limit_TriggerStorageAvailable(StreamId, currentLimitedLength);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_GetStatus(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if ( (vStreamProc_HasStreamLimit())
    && (vStreamProc_HasExternalSignalDef()) )
  {
    vStreamProc_StreamLimitIdxOfExitPointType activeStreamLimitId = vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);

    /* #10 If requested limit is active */
    if (StreamLimitId == activeStreamLimitId)
    {
      vStreamProc_ExternalSignalDefIdType externalSignalDefId =
        vStreamProc_GetExternalSignalDefIdxOfStreamLimit(StreamLimitId);

      /* #20 Return the current result of signal handler. */
      retVal = vStreamProc_GetResultOfExternalSignalInfo(externalSignalDefId);
    }
    /* #30 Otherwise: Return okay. */
    else
    {
      retVal = VSTREAMPROC_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_RegisterCallback
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_RegisterCallback(
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_SignalCallbackType LimitCbk,
  vStreamProc_CallbackHandleType CallbackHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasStreamLimit())
  {
    /* #10 Register limit callback. */
    vStreamProc_SetCallbackOfStreamLimitInfo(StreamLimitId, LimitCbk);
    vStreamProc_SetCallbackHandleOfStreamLimitInfo(StreamLimitId, CallbackHandle);

    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateRequestLimit()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Limit_EvaluateRequestLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StorageInfoPtrType StorageInfo,
  boolean WriteBufferRequested)
{
  vStreamProc_ReturnType  retVal = VSTREAMPROC_OK;
  vStreamProc_LengthType  limitedLength;

  /* #10 Get the length available behind the end of the stream. */
  vStreamProc_LimitResultType limitResult = vStreamProc_Limit_GetLimitedLengthOfStream(StreamId, &limitedLength);

  /* #20 Check if request is out of range (independent of available storage). */
  if (StorageInfo->RequestLength > limitedLength)
  {
    /* #30 Request exceeds limit, unless the available length was truncated to the maximum stream length. */
    if (limitResult != VSTREAMPROC_LIMIT_RESULT_TRUNCATED)
    {
      limitResult = VSTREAMPROC_LIMIT_RESULT_EXCEEDED;
    }

    /* #40 If requested data exceeds available length, return INSUFFICIENT_OUTPUT. */
    retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
  }

  /* #50 If hard limit is exceeded: Trigger limit. */
  /* Ignore info requests with size of zero, because no intention to request write buffer can be seen so far. */
  if ( (limitResult == VSTREAMPROC_LIMIT_RESULT_EXCEEDED)
    && ( (WriteBufferRequested == TRUE) || (StorageInfo->RequestLength > 0u) ) )
  {
    vStreamProc_Limit_TriggerStreamLimit(StreamId, VSTREAMPROC_HARD_TYPEOFSTREAMLIMIT);
  }

  /* #60 Set return length. */
  StorageInfo->AvailableLength = limitedLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateAcknowledgeLimit()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Limit_EvaluateAcknowledgeLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StorageInfoConstPtrType StorageInfo,
  P2VAR(vStreamProc_LimitResultType, AUTOMATIC, VSTREAMPROC_APPL_DATA) LimitResult)
{
  vStreamProc_ReturnType  retVal = VSTREAMPROC_OK;
  vStreamProc_LengthType  limitedLength;
  vStreamProc_StreamLimitIdType streamLimitId = vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);

  /* #10 Get the length available behind the end of the stream. */
  vStreamProc_LimitResultType limitResult = vStreamProc_Limit_GetLimitedLengthOfStream(StreamId, &limitedLength);

  /* #20 Check if request is out of range. */
  if (StorageInfo->RequestLength > limitedLength)
  {
    /* #30 If requested data exceeds available length, return FAILED. */
    retVal = VSTREAMPROC_FAILED;
  }
  else
  {
    /* #40 If soft limit is reached: Trigger limit. */
    /* Hint: Do not trigger limit if there is no limit or if the length was just truncated by maximum stream length. */
    if ( (streamLimitId != VSTREAMPROC_NO_STREAMLIMIT)
      && (limitResult != VSTREAMPROC_LIMIT_RESULT_TRUNCATED)
      && (StorageInfo->RequestLength == limitedLength) )
    {
      limitResult = VSTREAMPROC_LIMIT_RESULT_REACHED;
    }
  }

  *LimitResult = limitResult;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateAcknowledgeLimit()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Limit_TriggerAcknowledgeLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_LimitResultType LimitResult)
{
  /* #10 If soft limit is reached or exceeded: Trigger limit. */
  if ( (LimitResult == VSTREAMPROC_LIMIT_RESULT_REACHED)
    || (LimitResult == VSTREAMPROC_LIMIT_RESULT_EXCEEDED) )
  {
    vStreamProc_Limit_TriggerStreamLimit(StreamId, VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateWaveStartLimit()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Limit_EvaluateWaveStartLimit(
  vStreamProc_StreamIdType StreamId)
{
  if (vStreamProc_HasStreamLimit())
  {
    vStreamProc_StreamLimitIdxOfStreamInfoType streamLimitIdx = vStreamProc_GetStreamLimitIdxOfStreamInfo(StreamId);

    /* #10 Trigger registered soft limit signal if stream start reached or exceeded limit. */
    if ( (streamLimitIdx != VSTREAMPROC_NO_STREAMLIMITIDXOFSTREAMINFO)
      && (vStreamProc_GetTypeOfStreamLimitInfo(streamLimitIdx) == VSTREAMPROC_SOFT_TYPEOFSTREAMLIMIT) )
    {
      vStreamProc_StorageInfoType storageInfo;
      vStreamProc_LimitResultType limitResult;

      vStreamProc_Stream_InitStorageInfo(&storageInfo);
      (void)vStreamProc_Limit_EvaluateAcknowledgeLimit(StreamId, &storageInfo, &limitResult);
      vStreamProc_Limit_TriggerAcknowledgeLimit(StreamId, limitResult);
    }
  }
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Limit.c
 *********************************************************************************************************************/
