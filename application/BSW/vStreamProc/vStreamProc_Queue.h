/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Queue.h
 *        \brief  vStreamProc queue implementation header file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#ifndef VSTREAMPROC_QUEUE_H
#define VSTREAMPROC_QUEUE_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**************************************************************
 *  Queue handling
 **************************************************************/

/*! Reserve two entries in data structure for explicit free and used head. */
#define VSTREAMPROC_RESERVED_QUEUE_ENTRIES                2u

/* Handles for free and used head. */
#define VSTREAMPROC_QUEUE_HANDLE_HEAD_USED                0u
#define VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE                1u
/* Start handle for actual entries. */
#define VSTREAMPROC_QUEUE_HANDLE_ENTRY_OFFSET             2u

#define VSTREAMPROC_QUEUE_HANDLE_NONE                     0xFFFFFFFFu

/* Default priorities for queue entries. */
#define VSTREAMPROC_QUEUE_PRIO_LOWEST                     0x00u
#define VSTREAMPROC_QUEUE_PRIO_HIGHEST                    0xFFFFFFFFu

#define VSTREAMPROC_QUEUE_ENTITY_HANDLE_NONE              0xFFFFFFFFu

#define VSTREAMPROC_QUEUE_SIZE(count)                     ((uint32)(count) + VSTREAMPROC_RESERVED_QUEUE_ENTRIES)

/**************************************************************
 *  Queue handling
 **************************************************************/

/* Access macros to check for empty queues.
 * A queue is empty if the head references itself (cyclic double linked list). */
#define vStreamProc_Queue_IsEmpty(queue)                   (VSTREAMPROC_QUEUE_HANDLE_HEAD_USED == (queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_USED].Next)
#define vStreamProc_Queue_IsFull(queue)                    (VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE == (queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE].Next)

/* Access macros to get handle of first entry. */
#define vStreamProc_Queue_GetFirstUsedHandle(queue)        ((queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_USED].Next)
#define vStreamProc_Queue_GetLastUsedHandle(queue)         ((queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_USED].Prev)
#define vStreamProc_Queue_GetFirstFreeHandle(queue)        ((queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE].Next)
#define vStreamProc_Queue_GetLastFreeHandle(queue)         ((queue)->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE].Prev)

/*! Access macro to entry using handle. */
#define vStreamProc_Queue_GetEntry(queue, handle)          ((queue)->Entries[handle])

#define vStreamProc_Queue_GetNextHandle(queue, handle)     (vStreamProc_Queue_GetEntry((queue), (handle)).Next)
#define vStreamProc_Queue_GetPreviousHandle(queue, handle) (vStreamProc_Queue_GetEntry((queue), (handle)).Prev)

#define vStreamProc_Queue_GetPriority(queue, handle)       (vStreamProc_Queue_GetEntry((queue), (handle)).Prio)

#define vStreamProc_Queue_GetEntityHandle(queue, handle)                 (vStreamProc_Queue_GetEntry((queue), (handle)).Entity)
#define vStreamProc_Queue_SetEntityHandle(queue, handle, entityHandle)   { vStreamProc_Queue_GetEntry((queue), (handle)).Entity = (entityHandle); }

/* Access macros to get first or last entry. */
#define vStreamProc_Queue_GetFirstUsedEntry(queue)         (vStreamProc_Queue_GetEntry((queue), vStreamProc_Queue_GetFirstUsedHandle(queue)))
#define vStreamProc_Queue_GetLastUsedEntry(queue)          (vStreamProc_Queue_GetEntry((queue), vStreamProc_Queue_GetLastUsedHandle(queue)))
#define vStreamProc_Queue_GetFirstFreeEntry(queue)         (vStreamProc_Queue_GetEntry((queue), vStreamProc_Queue_GetFirstFreeHandle(queue)))
#define vStreamProc_Queue_GetLastFreeEntry(queue)          (vStreamProc_Queue_GetEntry((queue), vStreamProc_Queue_GetLastFreeHandle(queue)))

/* Access macros to get fill level of queue. */
#define vStreamProc_Queue_GetNumberOfUsedEntries(queue)    ((queue)->UsedCount)

/* HINT: The cast to vStreamProc_QueueHandleType is required to avoid an undesired integer promotion to signed (!) integer (see C99 6.3.1.1). */
#define vStreamProc_Queue_GetNumberOfFreeEntries(queue)    ((vStreamProc_QueueHandleType)((queue)->Length - (queue)->UsedCount))

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
 /*! Handle for queue entry. */
typedef uint32      vStreamProc_QueueHandleType;
typedef uint32      vStreamProc_QueuePrioType;
typedef uint32      vStreamProc_EntityHandleType;

typedef enum
{
  VSTREAMPROC_QUEUE_ENTRY_STATE_FREE,
  VSTREAMPROC_QUEUE_ENTRY_STATE_USED
} vStreamProc_QueueEntryStateType;

typedef enum
{
  VSTREAMPROC_QUEUE_PRIO_ORDER_DESC,  /**< Descending order: Higher values result in higher priority */
  VSTREAMPROC_QUEUE_PRIO_ORDER_ASC    /**< Ascending order: Lower values result in higher priority */
} vStreamProc_QueuePrioOrderType;

/*! Queue entry. */
typedef struct
{
  vStreamProc_QueuePrioType         Prio;    /**< Entry priority */
  vStreamProc_QueueEntryStateType   State;
  vStreamProc_QueueHandleType       Prev;    /**< Handle of previous queue entry */
  vStreamProc_QueueHandleType       Next;    /**< Handle of next queue entry */
  vStreamProc_EntityHandleType      Entity;  /**< Associated entity handle / index */
} vStreamProc_QueueEntryType;

typedef struct
{
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR)  Entries;
  vStreamProc_QueueHandleType       Length;
  vStreamProc_QueueHandleType       UsedCount;
  vStreamProc_QueuePrioOrderType    PrioOrder;
} vStreamProc_QueueType;

typedef P2VAR(vStreamProc_QueueType, AUTOMATIC, VSTREAMPROC_APPL_DATA)      vStreamProc_QueuePtrType;
typedef P2CONST(vStreamProc_QueueType, AUTOMATIC, VSTREAMPROC_APPL_DATA)    vStreamProc_QueueConstPtrType;

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**************************************************************
 *  Queue handling
 **************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_Queue_Init
 *********************************************************************************************************************/
/*! \brief         Initialize queue structure.
 *  \details       Implements two double linked lists, representing a used and a free queue.
 *  \param[in,out] Queue            Pointer to queue object.
 *  \param[in,out] Entries          Pointer to array of queue entries.
 *  \param[in]     Length           Total length of queue, including entries for used and free head.
 *  \param[in]     PrioOrder        VSTREAMPROC_QUEUE_PRIO_ORDER_DESC   Sort in descending order: Higher values result in higher priority.
 *                                  VSTREAMPROC_QUEUE_PRIO_ORDER_ASC    Sort in ascending order: Lower values result in higher priority.
 *  \param[in]     EntityStart      Entity handle to be assigned to first entry (excluding head entries).
 *  \param[in]     EntityIncrement  Increment applied to entity handle when assigned to next entry.
 *  \context       TASK|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           -
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Queue_Init(
  vStreamProc_QueuePtrType Queue,
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Entries,
  vStreamProc_QueueHandleType Length,
  vStreamProc_QueuePrioOrderType PrioOrder,
  vStreamProc_EntityHandleType EntityStart,
  vStreamProc_EntityHandleType EntityIncrement);

/**********************************************************************************************************************
 *  vStreamProc_Queue_Remove
 *********************************************************************************************************************/
/*! \brief         Remove specific entry from queue.
 *  \details       Take specific entry, remove it from queue and insert it as last entry into free queue.
 *  \param[in,out] Queue            Pointer to queue object.
 *  \param[in]     Handle           Queue entry handle.
 *  \return        Handle of removed entry.
 *  \context       TASK|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           vStreamProc_Queue_Init executed before, queue not empty.
 *********************************************************************************************************************/
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_Remove(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueueHandleType Handle);

/**********************************************************************************************************************
 *  vStreamProc_Queue_PrioUpdate
 *********************************************************************************************************************/
/*! \brief         Update priority of a queue entry.
 *  \details       Take specific entry, remove it from queue and re-insert it according to the updated priority.
 *  \param[in,out] Queue            Pointer to queue object.
 *  \param[in]     Handle           Queue entry handle.
 *  \param[in]     Prio             New priority value.
 *  \return        Handle of updated entry.
 *  \context       TASK|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           vStreamProc_Queue_Init executed before, queue not empty.
 *********************************************************************************************************************/
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_PrioUpdate(
  vStreamProc_QueueConstPtrType Queue,
  vStreamProc_QueueHandleType Handle,
  vStreamProc_QueuePrioType Prio);

/**********************************************************************************************************************
 *  vStreamProc_Queue_PrioInsert
 *********************************************************************************************************************/
/*! \brief         Insert entry into queue using given priority.
 *  \details       Take first free entry, remove it from free queue and insert it according to the given priority.
 *                 Additionally set job associated with queue entry.
 *  \param[in,out] Queue            Pointer to queue object.
 *  \param[in]     Prio             Priority value.
 *  \return        Handle of inserted entry.
 *  \context       TASK|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           vStreamProc_Queue_Init executed before, queue not full.
 *********************************************************************************************************************/
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_PrioInsert(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueuePrioType Prio);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_QUEUE_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Queue.h
 *********************************************************************************************************************/
