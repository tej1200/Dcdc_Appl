/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode.h
 *        \brief  vStreamProc processing node interfaces header file
 *
 *      \details  Definition of interfaces for vStreamProc processing nodes
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_PROCESSING_NODE_H
#define VSTREAMPROC_PROCESSING_NODE_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
#define vStreamProc_ProcessingNode_SignalHandler_HandleWaveStartEnd         vStreamProc_ProcessingNode_SignalHandler_DefaultHandler
#define vStreamProc_ProcessingNode_SignalHandler_HandleOutputPortSnapshot   vStreamProc_ProcessingNode_SignalHandler_DefaultHandler
#define vStreamProc_ProcessingNode_SignalHandler_HandleInputPortSnapshot    vStreamProc_ProcessingNode_SignalHandler_DefaultHandler

/* vStreamProc_SignalResponseType access macros: */
#define vStreamProc_GetSetSignalResponseFlagResult(signalResponse, signalResponeFlag)   vStreamProc_GetSetBitMaskResult(signalResponse, signalResponeFlag)
#define vStreamProc_GetClrSignalResponseFlagResult(signalResponse, signalResponeFlag)   vStreamProc_GetClrBitMaskResult(signalResponse, signalResponeFlag)
#define vStreamProc_SetSignalResponseFlag(signalResponse, signalResponeFlag)            vStreamProc_SetBitMask(signalResponse, signalResponeFlag)
#define vStreamProc_ClrSignalResponseFlag(signalResponse, signalResponeFlag)            vStreamProc_ClrBitMask(signalResponse, signalResponeFlag)
#define vStreamProc_IsSignalResponseFlagSet(signalResponse, signalResponeFlag)          vStreamProc_IsBitMaskSet(signalResponse, signalResponeFlag)
#define vStreamProc_GetSignalResponseFlagValue(signalResponse, signalResponeFlag)       vStreamProc_GetBitFlagValue(signalResponse, signalResponeFlag)

/* vStreamProc_SignalResponseType bit flags: */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE  (vStreamProc_SignalResponseType)(0x01u)  /*!< Workload shall consume a schedule cycle if set. */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK       (vStreamProc_SignalResponseType)(0x02u)  /*!< Signal successfully consumed if set. */
#define VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS   (vStreamProc_SignalResponseType)(0x80u)  /*!< Result successful if set. */

/* vStreamProc_SignalResponseType supported values: */
#define VSTREAMPROC_SIGNALRESPONSE_BUSY           (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS)
#define VSTREAMPROC_SIGNALRESPONSE_PENDING        (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE)
#define VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE    (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK)
#define VSTREAMPROC_SIGNALRESPONSE_CONFIRM        (vStreamProc_SignalResponseType)(VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK | VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE)
#define VSTREAMPROC_SIGNALRESPONSE_FAILED         (vStreamProc_SignalResponseType)(0x00u)

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_GetTypedWorkspaceOfProcessingNode()
 *********************************************************************************************************************/
/*!
 * \brief         Get the pointer to the workspace of a processing node.
 * \details       Returns the pointer to the workspace, while performing a DET check against the
 *                expected workspace type.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     WorkspaceTypeId   Id of the expected workspace type.
 * \return        Pointer to workspace  DET check enabled and workspace type matches or DET checks disabled.
 * \return        NULL_PTR              DET check enabled and workspace type doesn't match.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_GenericNodeWorkspaceType, VSTREAMPROC_CODE) vStreamProc_GetTypedWorkspaceOfProcessingNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_WorkspaceTypeIdType WorkspaceTypeId);

/**********************************************************************************************************************
 *  vStreamProc_GetTypedConfigOfProcessingNode()
 *********************************************************************************************************************/
/*!
 * \brief         Get the pointer to the configuration of a processing node.
 * \details       Returns the pointer to the configuration, while performing a DET check against the
 *                expected configuration type.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     ConfigTypeId      Id of the expected configuration type.
 * \return        Pointer to configuration  DET check enabled and configuration type matches or DET checks disabled.
 * \return        NULL_PTR                  DET check enabled and configuration type doesn't match.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_GenericNodeConfigType, VSTREAMPROC_CODE) vStreamProc_GetTypedConfigOfProcessingNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_ConfigTypeIdType ConfigTypeId);

/**********************************************************************************************************************
 *  vStreamProc_PrepareInputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize input port information structure.
 * \details       -
 * \param[in]     ProcNodeInfo              The processing node information to operate on.
 * \param[in]     InputPortSymbolicName     A named input port of the processing node.
 * \param[out]    InputPortInfo             Pointer to input port information structure.
 *                - ReadRequest.StorageInfo.DataTypeInfo.Id     Set to vStreamProcConf_vStreamProcDataType_Undefined.
 *                - ReadRequest.StorageInfo.DataTypeInfo.Size   Set to zero.
 *                - ReadRequest.StorageInfo.AvailableLength     Set to zero.
 *                - ReadRequest.StorageInfo.RequestLength       Set to zero.
 *                - ReadRequest.StorageInfo.ReleaseFlag         Set to TRUE.
 *                - ReadRequest.Buffer                          Set to NULL_PTR.
 *                - PortHandle                                  Set to handle matching the named input port.
 *                - SymbolicPortName                            Set to passed symbolic name value.
 *                - IsConnected                                 TRUE if port is connected, FALSE otherwise.
 * \return        VSTREAMPROC_OK      Operation was successful.
 * \return        VSTREAMPROC_FAILED  Operation was unsuccessful.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareInputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_InputPortInfoPtrType InputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_GetInputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Get information about a pipe input port.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     DataTypeId        Id of the expected data type of the storage node connected to the access point.
 *                                  vStreamProcConf_vStreamProcDataType_Undefined if no specific data type is expected.
 * \param[in,out] InputPortInfo     Pointer to input port information structure.
 *                - Input
 *                  - ReadRequest.StorageInfo.RequestLength       Requested minimum buffer length.
 *                  - PortHandle                                  Handle of an input port of the processing node.
 *                  - SymbolicPortName                            Named input port of the processing node.
 *                - Output
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                                connected to the port.
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                                connected to the port.
 *                  - ReadRequest.StorageInfo.AvailableLength     Available buffer length.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetInputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_InputPortInfoPtrType InputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_RequestInputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Requests a read action.
 * \details       This call does not read any data from the buffer. It rather provides pointer and length information. 
 *                The application then has to do the data reading based on this information. If data is available, the 
 *                provided pointer does not change until a read acknowledge is issued. Defragmentation may be blocked 
 *                due to a pending read request. A read request that indicates data must always be acknowledged to release
 *                the defragmentation lock.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     DataTypeId        Id of the expected data type of the storage node connected to the access point.
 * \param[in]     RequestLength     Requested minimum buffer length.
 * \param[in,out] InputPortInfo     Pointer to input port information structure.
 *                - Input
 *                  - PortHandle                                  Handle of an input port of the processing node.
 *                  - SymbolicPortName                            Named input port of the processing node.
 *                - Output
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                                connected to the port.
 *                  - ReadRequest.StorageInfo.AvailableLength     Available buffer length.
 *                  - ReadRequest.StorageInfo.RequestLength       Requested minimum buffer length.
 *                  - ReadRequest.Buffer                          Pointer to available buffer.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestInputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_InputPortInfoPtrType InputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeInputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Acknowledges a read action.
 * \details       The processing node shall have issued a read request which then can be acknowledge by calling this 
 *                function. Therefore, the amount of consumed data must be provided. By acknowledging a read request
 *                a possible defragmentation lock is being released.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     ConsumedLength    The amount of consumed data which needs to be acknowledged.
 * \param[in]     ReleaseFlag       Flag to control release of storage lock.
 *                - TRUE: Release storage lock.
 *                - FALSE: Keep storage locked.
 * \param[in,out] InputPortInfo     Pointer to input port information structure.
 *                - Input
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only required when storage lock is kept.
 *                  - PortHandle                                  Handle of an input port of the processing node.
 *                  - SymbolicPortName                            Named input port of the processing node.
 *                - Output
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only set when storage lock is kept.
 *                  - ReadRequest.StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only set when storage lock is kept.
 *                  - ReadRequest.StorageInfo.AvailableLength     Updated available buffer length.
 *                                                                Zero if storage lock was released.
 *                  - ReadRequest.StorageInfo.RequestLength       Passed produced length value.
 *                                                                Zero if storage lock was released.
 *                  - ReadRequest.StorageInfo.ReleaseFlag         Passed release flag value.
 *                  - ReadRequest.Buffer                          Updated pointer to available buffer.
 *                                                                NULL_PTR if storage lock was released.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeInputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ConsumedLength,
  boolean ReleaseFlag,
  vStreamProc_InputPortInfoPtrType InputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_PrepareOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize output port information structure.
 * \details       -
 * \param[in]     ProcNodeInfo            The processing node information to operate on.
 * \param[in]     OutputPortSymbolicName  A named output port of the processing node.
 * \param[out]    OutputPortInfo            Pointer to output port information structure.
 *                - WriteRequest.StorageInfo.DataTypeInfo.Id    Set to vStreamProcConf_vStreamProcDataType_Undefined.
 *                - WriteRequest.StorageInfo.DataTypeInfo.Size  Set to zero.
 *                - WriteRequest.StorageInfo.AvailableLength    Set to zero.
 *                - WriteRequest.StorageInfo.RequestLength      Set to zero.
 *                - WriteRequest.StorageInfo.ReleaseFlag        Set to TRUE.
 *                - WriteRequest.Buffer                         Set to NULL_PTR.
 *                - PortHandle                                  Set to handle matching the named output port.
 *                - SymbolicPortName                            Set to passed symbolic name value.
 *                - IsConnected                                 TRUE if port is connected, FALSE otherwise.
 * \return        VSTREAMPROC_OK      Operation was successful.
 * \return        VSTREAMPROC_FAILED  Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareOutputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_GetOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Get information about a pipe output port.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     DataTypeId        Id of the expected data type of the storage node connected to the access point.
 *                                  vStreamProcConf_vStreamProcDataType_Undefined if no specific data type is expected.
 * \param[in,out] OutputPortInfo    Pointer to output port information structure.
 *                - Input
 *                  - WriteRequest.StorageInfo.RequestLength      Requested minimum buffer length.
 *                  - PortHandle                                  Handle of an output port of the processing node.
 *                  - SymbolicPortName                            Named output port of the processing node.
 *                - Output
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Id    Id of the actual data type of the storage node
 *                                                                connected to the port.
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Size  Byte size of data type of the storage node
 *                                                                connected to the port.
 *                  - WriteRequest.StorageInfo.AvailableLength    Available buffer length.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetOutputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_RequestOutputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Requests a write action.
 * \details       This call does not write any data to a buffer. It rather provides the pointer and length information
 *                to the application. The application can then write its data to the provided pointer while the 
 *                maximum data amount is limited by the provided available length.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     DataTypeId        Id of the expected data type of the storage node connected to the access point.
 * \param[in]     RequestLength     Requested minimum buffer length.
 * \param[in,out] OutputPortInfo    Pointer to output port information structure.
 *                - Input
 *                  - PortHandle                                  Handle of an output port of the processing node.
 *                  - SymbolicPortName                            Named output port of the processing node.
 *                - Output
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Id    Id of the actual data type of the storage node
 *                                                                connected to the access point.
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Size  Byte size of data type of the storage node
 *                                                                connected to the port.
 *                  - WriteRequest.StorageInfo.AvailableLength    Available buffer length.
 *                  - WriteRequest.StorageInfo.RequestLength      Requested minimum buffer length.
 *                  - WriteRequest.Buffer                         Pointer to available buffer.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestOutputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Acknowledges a write action.
 * \details       The processing node shall have requested a write request which then can be acknowledge by calling 
 *                this function. The write request only provides a maximum amount of data to be produced, but it does
 *                not specify a precise amount of data which shall be produced. Therefore, the amount of produced
 *                data must be provided by calling this function.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     ProducedLength    The amount of produced data which needs to be acknowledged.
 * \param[in]     ReleaseFlag       Flag to control release of storage lock.
 *                - TRUE: Release storage lock.
 *                - FALSE: Keep storage locked.
 * \param[in,out] OutputPortInfo    Pointer to output port information structure.
 *                - Input
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Id    Id of the expected data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only required when storage lock is kept.
 *                  - PortHandle                                  Handle of an output port of the processing node.
 *                  - SymbolicPortName                            Named output port of the processing node.
 *                - Output
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Id    Id of the actual data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only set when storage lock is kept.
 *                  - WriteRequest.StorageInfo.DataTypeInfo.Size  Byte size of data type of the storage node
 *                                                                connected to the access point.
 *                                                                Only set when storage lock is kept.
 *                  - WriteRequest.StorageInfo.AvailableLength  Updated available buffer length.
 *                                                              Zero if storage lock was released.
 *                  - WriteRequest.StorageInfo.RequestLength      Passed produced length value.
 *                                                                Zero if storage lock was released.
 *                  - WriteRequest.StorageInfo.ReleaseFlag        Passed release flag value.
 *                  - WriteRequest.Buffer                       Updated pointer to available buffer.
 *                                                              NULL_PTR if storage lock was released.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProducedLength,
  boolean ReleaseFlag,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_PreparePortInfos()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize multiple port information structures.
 * \details       See #vStreamProc_PrepareInputPortInfo and #vStreamProc_PrepareOutputPortInfo for details.
 * \param[in]     ProcNodeInfo          The processing node information to operate on.
 * \param[in,out] InputPortInfo         Input port information structures. NULL_PTR if not used.
 *                - Input
 *                  - SymbolicPortName  A named input port of the processing node.
 *                - Output              See #vStreamProc_PrepareInputPortInfo.
 * \param[in]     InputPortCount        Number of input port information structures.
 * \param[in,out] OutputPortInfo        Output port information structures. NULL_PTR if not used.
 *                - Input
 *                  - SymbolicPortName  A named output port of the processing node.
 *                - Output              See #vStreamProc_PrepareOutputPortInfo.
 * \param[in]     OutputPortCount       Number of output port information structures.
 * \return        VSTREAMPROC_FAILED    Operation failed for at least one port.
 * \return        VSTREAMPROC_OK        Operation was successful for all ports.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PreparePortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_PrepareAllPortInfos()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize information structures for all ports of a pipe.
 * \details       See #vStreamProc_PrepareInputPortInfo and #vStreamProc_PrepareOutputPortInfo for details.
 * \param[in]     ProcNodeInfo          The processing node information to operate on.
 * \param[out]    InputPortInfo         Input port information structures. NULL_PTR if not used.
 *                - SymbolicPortName    Set to value matching the index.
 *                - <Other members>     See #vStreamProc_PrepareInputPortInfo.
 * \param[in]     InputPortCount        Number of input port information structures.
 * \param[out]    OutputPortInfo        Output port information structures. NULL_PTR if not used.
 *                - SymbolicPortName    Set to value matching the index.
 *                - <Other members>     See #vStreamProc_PrepareOutputPortInfo.
 * \param[in]     OutputPortCount       Number of output port information structures.
 * \return        VSTREAMPROC_FAILED    Operation failed for at least one port.
 * \return        VSTREAMPROC_OK        Operation was successful for all ports.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareAllPortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_GetPortInfos()
 *********************************************************************************************************************/
/*!
 * \brief         Get information about multiple pipe ports.
 * \details       See #vStreamProc_GetInputPortInfo and #vStreamProc_GetOutputPortInfo for details.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in,out] InputPortInfos    Input port information structures. NULL_PTR if not used.
 * \param[in]     InputPortCount    Number of input port information structures.
 * \param[in,out] OutputPortInfos   Output port information structures. NULL_PTR if not used.
 * \param[in]     OutputPortCount   Number of output port information structures.
 * \return        VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   At least one port cannot provide the requested minimum storage.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    At least one port cannot provide the requested minimum data.
 * \return        VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetPortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_RequestPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Requests multiple read and write actions.
 * \details       See #vStreamProc_RequestOutputPortData and #vStreamProc_RequestInputPortData for details.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in,out] InputPortInfos    Input port information structures. NULL_PTR if not used.
 * \param[in]     InputPortCount    Number of input port information structures.
 * \param[in,out] OutputPortInfos   Output port information structures. NULL_PTR if not used.
 * \param[in]     OutputPortCount   Number of output port information structures.
 * \return        VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   At least one port cannot provide the requested minimum storage.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    At least one port cannot provide the requested minimum data.
 * \return        VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgePorts()
 *********************************************************************************************************************/
/*!
 * \brief         Acknowledges multiple read and write actions.
 * \details       See #vStreamProc_AcknowledgeInputPort and #vStreamProc_AcknowledgeOutputPort for details.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in,out] InputPortInfos   Input port information structures. NULL_PTR if not used.
 * \param[in]     InputPortCount    Number of input port information structures.
 * \param[in,out] OutputPortInfos   Output port information structures. NULL_PTR if not used.
 * \param[in]     OutputPortCount   Number of output port information structures.
 * \return        VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return        VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgePorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_ReleaseAllPorts()
 *********************************************************************************************************************/
/*!
 * \brief         Releases storage lock of all ports.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \return        VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return        VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ReleaseAllPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/***********************************************************************************************************************
 *  vStreamProc_SetNodeSignalHandlerState
 **********************************************************************************************************************/
/*!
 * \brief         Sets the node signal handler state of the node signal to the given state.
 * \details       -
 * \param[in]     ProcessingNodeId            The target node of the signal.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful.
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-298123
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetNodeSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/***********************************************************************************************************************
 *  vStreamProc_SetInputPortSignalHandlerState
 **********************************************************************************************************************/
/*!
 * \brief         Sets the node signal handler state of the input port signal to the given state.
 * \details       -
 * \param[in]     ProcessingNodeId            The target node of the signal.
 * \param[in]     InputPortSymbolicName       The target input port symbolic name of the signal.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful.
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-298123
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetInputPortSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/***********************************************************************************************************************
 *  vStreamProc_SetOutputPortSignalHandlerState
 **********************************************************************************************************************/
/*!
 * \brief         Sets the node signal handler state of the output port signal to the given state.
 * \details       -
 * \param[in]     ProcessingNodeId            The target node of the signal.
 * \param[in]     OutputPortSymbolicName      The target output port symbolic name of the signal.
 * \param[in]     SignalId                    The id of signal.
 * \param[in]     NewSignalHandlerState       The new signal handler state.
 * \return        VSTREAMPROC_OK              Signal handler state change successful.
 * \return        VSTREAMPROC_FAILED          Signal handler state change not successful.
 *                                            Might be the case if no signal handler was found or the new state is
 *                                            not allowed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-298123
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetOutputPortSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_GetInputPortWaveInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize WaveInfo with values from input port.
 * \details       -
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     InputPortSymbolicName     A named input port of the processing node.
 * \param[out]    WaveInfo                  Reference to wave info structure.
 * \return        VSTREAMPROC_OK
 * \return        VSTREAMPROC_FAILED
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-304742
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_GetInputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo);

/***********************************************************************************************************************
 *  vStreamProc_GetOutputPortWaveInfo
 **********************************************************************************************************************/
/*!
 * \brief         Initialize WaveInfo with values from output port.
 * \details       -
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     OutputPortSymbolicName    A named output port of the processing node.
 * \param[out]    WaveInfo                  Reference to wave info structure.
 * \return        VSTREAMPROC_OK
 * \return        VSTREAMPROC_FAILED
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-304742
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_GetOutputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_StartOutputPortWave()
 *********************************************************************************************************************/
/*!
 * \brief         Signals start of wave at output port.
 * \details       -
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     OutputPortSymbolicName    A named input port of the processing node.
 * \param[out]    WaveInfo                  Reference to wave info structure.
 * \return        VSTREAMPROC_OK
 * \return        VSTREAMPROC_FAILED
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-304742
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_StartOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_EndOutputPortWave()
 *********************************************************************************************************************/
/*!
 * \brief         Signals end of wave at output port.
 * \details       -
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     SymbolicNameOfInputPort   A named input port of the processing node.
 * \param[out]    WaveInfo                  Reference to wave info structure.
 * \return        VSTREAMPROC_OK
 * \return        VSTREAMPROC_FAILED
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-304742
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_EndOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_DiscardInputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Discards existing and future data at the provided input port until the provided consume position.
 * \details       Pending DataAvailable signal will be discarded as well.
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     InputPortSymbolicName     Symbolic name of the input port.
 * \param[in]     NewConsumePosition        The consume position until the data shall be discarded.
 * \return        VSTREAMPROC_OK            Request was successful.
 * \return        VSTREAMPROC_FAILED        Request failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-312777
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_DiscardInputPortData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_StreamPositionType NewConsumePosition);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_SignalHandler_DefaultHandler
 *********************************************************************************************************************/
/*!
 *  \brief          Default signal handler.
 *  \details        The signal handler simply acknowledges the signal to trigger further actions.
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted and no work was performed.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SignalHandler_DefaultHandler(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_SignalHandler_PropagateHighLevelWaveEnd
 *********************************************************************************************************************/
/*!
 *  \brief          Internal signal handler for the WaveEndUpperLevel signal. Used for the routing of waves on
 *                  higher levels than the registered level of the processing node.
 *  \details        The signal handler will query which (higher) wave levels at the input port have to be routed and 
 *                  calls the EndOutputPortWave() API for all mapped output ports.
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted and wave routing was performed.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SignalHandler_PropagateHighLevelWaveEnd(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_PROCESSING_NODE_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode.h
 *********************************************************************************************************************/

