/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Queue.c
 *        \brief  vStreamProc queue implementation source file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VSTREAMPROC_QUEUE_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Queue.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Queue_Move
 *********************************************************************************************************************/
/*! \brief         Move queue entry in the same or other queue.
 *  \details       Cut queue entry from current position and place it at a different location (either in same or other queue).
 *  \param[in,out] Queue            Pointer to queue object.
 *  \param[in]     Handle           Handle of queue entry to be moved.
 *  \param[in]     NewPrev          Handle of new predecessor.
 *  \return        Handle of affected entry.
 *  \context       TASK|ISR2
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *  \pre           vStreamProc_Queue_Init executed before, source queue not empty.
 *                 Passed handles have to represent actual predecessor and successor relationship.
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_Move(
  vStreamProc_QueueConstPtrType Queue,
  vStreamProc_QueueHandleType Handle,
  vStreamProc_QueueHandleType NewPrev);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vStreamProc_Queue_Move
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_Move(
  vStreamProc_QueueConstPtrType Queue,
  vStreamProc_QueueHandleType Handle,
  vStreamProc_QueueHandleType NewPrev )
{
  vStreamProc_QueueHandleType oldPrev;
  vStreamProc_QueueHandleType oldNext;
  vStreamProc_QueueHandleType newNext;
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR) entries;

  entries = Queue->Entries;

  /* #10 Check for matching handles. */
  if (Handle == NewPrev)
  {
    /* Entry requested to be placed behind itself.
     * Can e.g. occur when priority of existing entry is updated.
     * This effectively results in it placed at exact same location.
     * No need to change anything. */
  }
  else
  {
    /* #20 Get current predecessor and successor. */
    oldPrev = entries[Handle].Prev;
    oldNext = entries[Handle].Next;

    /* #30 Remove entry from old queue. */
    entries[oldPrev].Next = oldNext;
    entries[oldNext].Prev = oldPrev;

    /* #40 Get new predecessor. */
    newNext = entries[NewPrev].Next;

    /* #50 Insert entry into new queue. */
    entries[Handle].Prev  = NewPrev;
    entries[NewPrev].Next = Handle;
    entries[Handle].Next  = newNext;
    entries[newNext].Prev = Handle;

    /* #60 Update state of entry to match current queue. */
    entries[Handle].State = entries[NewPrev].State;
  }

  return Handle;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**************************************************************
 *  Queue handling
 **************************************************************/

/**********************************************************************************************************************
 * vStreamProc_Queue_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Queue_Init(                                                                    /* PRQA S 6060 */ /* MD_MSR_STPAR */
  vStreamProc_QueuePtrType Queue,
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR) Entries,
  vStreamProc_QueueHandleType Length,
  vStreamProc_QueuePrioOrderType PrioOrder,
  vStreamProc_EntityHandleType EntityStart,
  vStreamProc_EntityHandleType EntityIncrement)
{
  vStreamProc_QueueHandleType handle;
  vStreamProc_QueueHandleType prevHandle;
  vStreamProc_QueueHandleType nextHandle;
  vStreamProc_EntityHandleType entityHandle;
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR) entry;
  vStreamProc_QueuePrioType entryPrio;

  /* #10 Start with empty queue by self-reference. */
  entry = &Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_USED];
  entry->State  = VSTREAMPROC_QUEUE_ENTRY_STATE_USED;
  entry->Next   = VSTREAMPROC_QUEUE_HANDLE_HEAD_USED;
  entry->Prev   = VSTREAMPROC_QUEUE_HANDLE_HEAD_USED;

  /* #20 Set default values. */
  if (PrioOrder == VSTREAMPROC_QUEUE_PRIO_ORDER_DESC)
  {
    entry->Prio = VSTREAMPROC_QUEUE_PRIO_HIGHEST;
  }
  else
  {
    entry->Prio = VSTREAMPROC_QUEUE_PRIO_LOWEST;
  }
  entry->Entity = VSTREAMPROC_QUEUE_ENTITY_HANDLE_NONE;

  /* #30 Setup double linked list of empty queue entries.
   * Head references last entry as predecessor. */
  prevHandle = Length - 1u;
  nextHandle = VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE;

  entityHandle = EntityStart;

  if (PrioOrder == VSTREAMPROC_QUEUE_PRIO_ORDER_DESC)
  {
    entryPrio = VSTREAMPROC_QUEUE_PRIO_LOWEST;
  }
  else
  {
    entryPrio = VSTREAMPROC_QUEUE_PRIO_HIGHEST;
  }

  /* #40 Append available entries to free queue. */
  for (handle = VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE; handle < Length; handle++)
  {
    nextHandle++;
    entry = &Entries[handle];

    /* #50 Set predecessor and successor. */
    entry->Prev   = prevHandle;
    entry->Next   = nextHandle;
    /* #60 Set default values. */
    entry->Prio   = entryPrio;
    entry->State  = VSTREAMPROC_QUEUE_ENTRY_STATE_FREE;

    if (handle == VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE)
    {
      entry->Entity = VSTREAMPROC_QUEUE_ENTITY_HANDLE_NONE;
    }
    else
    {
      entry->Entity = entityHandle;
      entityHandle += EntityIncrement;
    }

    prevHandle = handle;
  }

  /* #70 Last entry references head as successor. */
  Entries[Length - 1u].Next = VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE;

  Queue->Entries    = Entries;
  Queue->Length     = Length - 2u;
  Queue->PrioOrder  = PrioOrder;
  Queue->UsedCount  = 0u;
}

/**********************************************************************************************************************
 * vStreamProc_Queue_Remove
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_Remove(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueueHandleType Handle )
{
  Queue->UsedCount--;

  /* #10 Insert at end of free queue.
     Place between current last entry and tail (== head). */
  return vStreamProc_Queue_Move(Queue, Handle, Queue->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE].Prev);
}

/**********************************************************************************************************************
 * vStreamProc_Queue_PrioUpdate
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_PrioUpdate(
  vStreamProc_QueueConstPtrType Queue,
  vStreamProc_QueueHandleType Handle,
  vStreamProc_QueuePrioType Prio)
{
  vStreamProc_QueueHandleType prevHandle;
  P2VAR(vStreamProc_QueueEntryType, AUTOMATIC, VSTREAMPROC_APPL_VAR) entries;

  entries = Queue->Entries;

  /* #10 Start search at last entry. */
  prevHandle = entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_USED].Prev;

  /* #20 Skip all entries with lower priority.
      Remark: Search is assured to stop at head because it has the highest possible priority. */
  if (Queue->PrioOrder == VSTREAMPROC_QUEUE_PRIO_ORDER_DESC)
  {
    while (entries[prevHandle].Prio < Prio)
    {
      prevHandle = entries[prevHandle].Prev;
    }
  }
  else
  {
    while (entries[prevHandle].Prio > Prio)
    {
      prevHandle = entries[prevHandle].Prev;
    }
  }

  /* #30 Append after first entry with higher or equal priority. */
  (void)vStreamProc_Queue_Move(Queue, Handle, prevHandle);

  /* #40 Update priority of inserted entry. */
  entries[Handle].Prio = Prio;

  return Handle;
}

/**********************************************************************************************************************
 * vStreamProc_Queue_PrioInsert
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_QueueHandleType, VSTREAMPROC_CODE) vStreamProc_Queue_PrioInsert(
  vStreamProc_QueuePtrType Queue,
  vStreamProc_QueuePrioType Prio)
{
  vStreamProc_QueueHandleType handle;

  Queue->UsedCount++;

   /* #10 Relocate first free entry according to given priority. */
  handle = vStreamProc_Queue_PrioUpdate(Queue, Queue->Entries[VSTREAMPROC_QUEUE_HANDLE_HEAD_FREE].Next, Prio);

  return handle;
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Queue.c
 *********************************************************************************************************************/
