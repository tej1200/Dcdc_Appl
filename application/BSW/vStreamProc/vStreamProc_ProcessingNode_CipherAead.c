/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_CipherAead.c
 *        \brief  vStreamProc CipherAead Sub Module Source Code File
 *      \details  Implementation of the CipherAead sub module for the vStreamProc framework
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_CIPHERAEAD_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_CipherAead.h"
#include "vStreamProc.h"
#include "vStreamProc_ProcessingNode.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_CIPHERAEAD_CONFIG == STD_ON)
# include "Csm_Types.h"
# include "Csm.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_InitLocal()
 *********************************************************************************************************************/
/*!
 * \brief           Initializes the CSM CipherAead job.
 * \details         -
 * \param[in]       ProcNodeInfo                 The processing node information to operate on.
 * \return          E_OK                         The call to start the decryption succeeded.
 * \return          E_NOT_OK                     The call to start the decryption failed.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_InitLocal(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_QueryPorts
 *********************************************************************************************************************/
/*!
 * \brief           Queries the available input and output lengths of the provided ProcNode.
 * \details         The queried data is saved to the InputPortInfo and OutputPortInfo
 * \param[in]       ProcNodeInfo            Pointer to the Processing Node info of the current node.
 * \param[out]      InputPortInfo           Pointer to the input port info structure for the query.
 * \param[out]      OutputPortInfo          Pointer to the output port info structure for the query.
 * \param[in]       MinOutputLength         The minimum required buffer length at the output port.
 * \return          VSTREAMPROC_OK                    Operation was successful.
 * \return          VSTREAMPROC_INSUFFICIENT_INPUT    There is no data available at the input port.
 * \return          VSTREAMPROC_INSUFFICIENT_OUTPUT   There is less than the requested minimum output buffer length available.
 * \return          VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfos,
  uint32 OutputPortCount,
  vStreamProc_LengthType MinOutputLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * \brief           Changes the signal handler states based on the provided trigger signals.
 * \details         The signal handler states are only changed if the current signal is different to the new trigger
 *                  signal.
 *                  If the trigger signal is changed, the corresponding signal handler state will be set to
 *                  REGISTERED and the other signal handler state will be set to UNREGISTERED.
 * \param[in]       ProcessingNodeId        ID of the ProcessingNode for which the signal handler shall be set.
 * \param[in]       CurrentTriggerSignalId  ID of the current trigger signal.
 * \param[in]       NewTriggerSignalId      ID of the new trigger signal.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType CurrentTriggerSignalId,
  vStreamProc_SignalIdType NewTriggerSignalId);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_UpdateSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * \brief           Updates the signal handler states based on the input and output lengths.
 * \details         -
 * \param[in]       ProcNodeInfo            Pointer to the Processing Node info of the current node.
 * \param[in]       RemainingInputLength    The remaining data length at the input port.
 * \param[in]       RemainingOutputLength   The remaining buffer length at the output port.
 * \param[in]       MinOutputLength         The minimum required buffer length at the output port.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_UpdateSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType RemainingInputLength,
  vStreamProc_LengthType RemainingOutputLength,
  vStreamProc_LengthType MinOutputLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation()
 *********************************************************************************************************************/
/*!
 * \brief           Performs the provided CipherAead operation and evaluates the CSM return value.
 * \details         -
 * \param[in]       CipherConfigPtr       Pointer to the CipherAead configuration.
 * \param[in]       OpMode                The operation mode which shall be used.
 * \param[in,out]   ReadRequestBuffer     Pointer to the read data.
 * \param[in,out]   ReadRequestLength     Length of the read data.
 *                                        The RequestLength is updated with the actually used buffer length.
 * \param[in]       AssociatedDataBuffer  Pointer to the associated data.
 * \param[in]       AssociatedDataLength  Length of the associated data.
 * \param[in]       TagBuffer             Pointer to the authentication tag data. Only needed in the last call.
 * \param[in]       TagLength             Length of the authentication tag. Only needed in the last call.
 * \param[out]      OutputBuffer          Pointer where the output data shall be stored.
 * \param[in,out]   OutputLength          Pointer which contains the length of the output buffer.
 *                                        After the function call, the actual written length is stored here.
 * \param[out]      VerifyPtr             Pointer which contains the verify result. Will be set in the last call.
 * \param[in,out]   SignalResponsePtr     Pointer to the signal response. The ACK flag is set based on the result of
 *                                        the CSM operation.
 * \return          VSTREAMPROC_OK        Operation was successful.
 * \return          VSTREAMPROC_PENDING   The CSM is busy.
 * \return          VSTREAMPROC_FAILED    Operation was unsuccessful.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation(
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CipherConfigPtr,
  Crypto_OperationModeType OpMode,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) ReadRequestBuffer,
  vStreamProc_LengthType ReadRequestLength,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) AssociatedDataBuffer,
  vStreamProc_LengthType AssociatedDataLength,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) TagBuffer,
  vStreamProc_LengthType TagLength,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputBuffer,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputLength,
  P2VAR(Crypto_VerifyResultType, AUTOMATIC, VSTREAMPROC_APPL_VAR) VerifyPtr,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponsePtr);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleDataReception
 *********************************************************************************************************************/
/*!
 * \brief           Responsible for handling the actual plain text data of the stream and forwarding the data to the
 *                   CSM.
 * \details         Cuts off the tag length of the data stream and processes all available input data with the CSM.
 * \param[in]       ProcNodeInfo         Pointer to the Processing Node info of the current node.
 * \param[in,out]   SignalResponsePtr    Pointer to the signal response. The ACK flag is set based on the result of
 *                                       the CSM operation.
 * \param[in,out]   InputPortInfo        Pointer to the input port information.
 * \param[out]      OutputPortInfo       Pointer to the output port information.
 * \return          VSTREAMPROC_OK       Operation was successful.
 * \return          VSTREAMPROC_PENDING  The CSM is busy.
 * \return          VSTREAMPROC_FAILED   Operation was unsuccessful.
 * \pre             -
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleDataReception(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponsePtr,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleIvReception
 *********************************************************************************************************************/
/*!
 * \brief           Responsible for handling the reception of the IV data via the data stream.
 * \details         Receives data with the length of the IV, sets the configured key element and validates the key.
 * \param[in]       ProcNodeInfo         Pointer to the Processing Node info of the current node.
 * \param[in,out]   SignalResponsePtr    Pointer to the signal response. The ACK flag is set based on the result of
 *                                       the CSM operation.
 * \param[in,out]   InputPortInfo        Pointer to the input port information.
 * \param[in]       IvLength             Length of the initialization vector.
 * \return          VSTREAMPROC_OK       Operation was successful.
 * \return          VSTREAMPROC_PENDING  The CSM is busy.
 * \return          VSTREAMPROC_FAILED   Operation was unsuccessful.
 * \pre             SignalResponsePtr is assumed to have value VSTREAMPROC_SIGNALRESPONSE_FAILED on call.
 * \context         TASK|ISR
 * \reentrant       TRUE
 * \synchronous     TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleIvReception(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponsePtr,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortInfo,
  vStreamProc_SizeOfInitializationVectorOfProcessingNode_CipherAead_ConfigType IvLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_PropagateResult
 *********************************************************************************************************************/
/*!
 * \brief         Propagate result of cipher operation.
 * \details       -
 * \param[in]     ProcNodeInfo                      The processing node information to operate on.
 * \param[in]     Result                            The result to be reported.
 * \return        VSTREAMPROC_OK                    Operation succeeded.
 * \return        VSTREAMPROC_FAILED                Operation failed.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Not enough buffer available to report result.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_PropagateResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  Std_ReturnType Result);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleRemainingData
 *********************************************************************************************************************/
/*!
 * \brief         If necessary, copies data from the given local buffer to the output port.
 * \details       -
 * \param[in]     ProcNodeInfo                      The processing node information to operate on.
 * \param[in]     OutputLength                      The amount of bytes to be written.
 * \param[in]     OutputPortInfo                    The output port to write the data to.
 * \param[in]     LocalBuffer                       Pointer to remaining data.
 * \return        VSTREAMPROC_OK                    Operation succeeded.
 * \return        VSTREAMPROC_FAILED                Operation failed.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Not enough buffer available to report result.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleRemainingData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType OutputLength,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfo,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) LocalBuffer
);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleResult
 *********************************************************************************************************************/
/*!
 * \brief         Writes data to the optional output result port if necessary and sets the signal response.
 * \details       -
 * \param[in]     ProcNodeInfo                      The processing node information to operate on.
 * \param[in]     IsConnected                       Value whether the output port is connected.
 * \param[in]     SignalResponse                    The signal response to set.
 * \param[in]     CsmResult                         The result returned by CSM.
 * \return        VSTREAMPROC_OK                    Operation succeeded.
 * \return        VSTREAMPROC_FAILED                Operation failed.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Not enough buffer available to report result.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ProcNodeInfo,
  boolean IsConnected,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponse,
  Crypto_VerifyResultType CsmResult
);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_InitLocal
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_InitLocal(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST)  specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  Std_ReturnType csmResult = E_NOT_OK;
  Std_ReturnType retVal = E_NOT_OK;

  /* Hint: Associated data is currently never used, but it is required to be a valid pointer for the crypto stack. => Use stack address. */
  uint8 associatedBuffer = 0u;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  if (specializedCfgPtr->CipherAeadModeOfProcessingNode_CipherAead_Config == VSTREAMPROC_CIPHERAEADMODEOFPROCESSINGNODE_CIPHERAEAD_CONFIG)
  {
    /* #10 Signal decryption start within CSM. */
    csmResult = Csm_AEADDecrypt(
      specializedCfgPtr->CsmJobIdOfProcessingNode_CipherAead_Config,
      CRYPTO_OPERATIONMODE_START,
      (uint8*)NULL_PTR,
      0u,
      &associatedBuffer,
      0u,
      (uint8*)NULL_PTR,
      0u,
      (uint8*)NULL_PTR,
      (uint32*)NULL_PTR,
      (uint8*)NULL_PTR
    );
    /* Map CSM result, errors either cannot happen in synchronous case or are critical => exit necessary. */
    if (csmResult == E_OK)
    {
      retVal = E_OK;
      /* #20 Set workspace to state PROCESSING. */
      workspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
    }
  }
  else /* == VSTREAMPROC_ENCRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHERAEAD_CONFIG */
  {
    /* Hint: Encryption not yet supported. */
  }
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_QueryPorts
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfos,
  uint32 OutputPortCount,
  vStreamProc_LengthType MinOutputLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare port infos. */
  retVal = vStreamProc_PrepareAllPortInfos(
    ProcNodeInfo,
    InputPortInfo,
    1u,
    OutputPortInfos,
    OutputPortCount
  );

  if ((InputPortInfo != NULL_PTR) && (retVal == VSTREAMPROC_OK))
  {
    /* #20 Get available input data. */
    InputPortInfo->ReadRequest.StorageInfo.RequestLength = 0u;

    retVal = vStreamProc_GetInputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, InputPortInfo);
  }

  if ((OutputPortCount >= 1u)
    && (OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData].IsConnected == TRUE)
    && (retVal == VSTREAMPROC_OK))
  {
    /* #30 If requested: Get output port info. */
    OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData].WriteRequest.StorageInfo.RequestLength = MinOutputLength;

    retVal = vStreamProc_GetOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, &OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData]);
  }

  if ((OutputPortCount >= 2u)
    && (OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_ResultData].IsConnected == TRUE)
    && (retVal == VSTREAMPROC_OK))
  {
    /* #40 If requested: Get optional port info. */
    OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_ResultData].WriteRequest.StorageInfo.RequestLength = 0u;

    retVal = vStreamProc_GetOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_Std_ReturnType, &OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_ResultData]);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType CurrentTriggerSignalId,
  vStreamProc_SignalIdType NewTriggerSignalId)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if signal handler is switched from DataAvailable to StorageAvailable. */
  if ((CurrentTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable)
    && (NewTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable))
  {
    /* Block DataAvailable signal handler and register StorageAvailable signal handler. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcInputPort_vStreamProc_CipherAead_InputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    (void)vStreamProc_SetOutputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
  }

  /* #20 Check if signal handler is switched from StorageAvailable to DataAvailable. */
  if ((CurrentTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable)
    && (NewTriggerSignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable))
  {
    /* Deregister StorageAvailable signal handler and register DataAvailable signal handler. */
    (void)vStreamProc_SetOutputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcessingNodeId,
      vStreamProcConf_vStreamProcInputPort_vStreamProc_CipherAead_InputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_UpdateSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_UpdateSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType RemainingInputLength,
  vStreamProc_LengthType RemainingOutputLength,
  vStreamProc_LengthType MinOutputLength)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If there is still data, but not enough output buffer available, register StorageAvailable signal handler. */
  if ((RemainingOutputLength < MinOutputLength)
    && (RemainingInputLength > 0u))
  {
    vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates(
      ProcNodeInfo->ProcessingNodeId,
      ProcNodeInfo->SignalInfo.SignalId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable);
  }
  /* #20 Otherwise, register DataAvailable signal handler. */
  else
  {
    vStreamProc_ProcessingNode_CipherAead_SwitchSignalHandlerStates(
      ProcNodeInfo->ProcessingNodeId,
      ProcNodeInfo->SignalInfo.SignalId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable);
  }
}
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation(/* PRQA S 6060 */ /* MD_MSR_STPAR */
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CipherConfigPtr,
  Crypto_OperationModeType OpMode,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) ReadRequestBuffer,
  vStreamProc_LengthType ReadRequestLength,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) AssociatedDataBuffer,
  vStreamProc_LengthType AssociatedDataLength,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) TagBuffer,
  vStreamProc_LengthType TagLength,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputBuffer,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputLength,
  P2VAR(Crypto_VerifyResultType, AUTOMATIC, VSTREAMPROC_APPL_VAR) VerifyPtr,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponsePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType            csmResult = E_NOT_OK;
  vStreamProc_ReturnType    retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Perform requested CSM action. */
  if (CipherConfigPtr->CipherAeadModeOfProcessingNode_CipherAead_Config == VSTREAMPROC_CIPHERAEADMODEOFPROCESSINGNODE_CIPHERAEAD_CONFIG)
  {
    csmResult = Csm_AEADDecrypt(
      CipherConfigPtr->CsmJobIdOfProcessingNode_CipherAead_Config,
      OpMode,
      ReadRequestBuffer,
      ReadRequestLength,
      AssociatedDataBuffer,
      AssociatedDataLength,
      TagBuffer,
      TagLength,
      OutputBuffer,
      OutputLength,
      VerifyPtr
    );
  }
  else /* == VSTREAMPROC_ENCRYPTION_CIPHERMODEOFPROCESSINGNODE_CIPHERAEAD_CONFIG */
  {
    /* Hint: Not currently supported. */
  }

  /* #20 Handle CSM result. Set signal response accordingly. */
  switch (csmResult)
  {
    case E_OK:
    {
      /* All usable input data was consumed, read request length is up to date */
      /* Write request length is already set by CSM */
      *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
      retVal = VSTREAMPROC_OK;
      break;
    }
    case CRYPTO_E_BUSY: /* Intentional fall through */
    case CRYPTO_E_QUEUE_FULL:
    {
      /* Stay scheduled, because the pending reason is external. */
      *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_PENDING;
      retVal = VSTREAMPROC_PENDING;
      break;
    }
    default:
    {
      retVal = VSTREAMPROC_FAILED;
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleDataReception
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleDataReception(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponsePtr,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   InputPortInfo,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)  OutputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* Hint: Associated data is currently never used, but it is required to be a valid pointer for the crypto stack.
  => Use stack address. */
  uint8 associatedDataBuffer = 0u;
  vStreamProc_ReturnType retVal;
  vStreamProc_LengthType  maxInputLength;
  vStreamProc_SizeOfAuthenticationTagOfProcessingNode_CipherAead_ConfigType tagLength;
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  vStreamProc_LengthType availableInputLength = 0u;
  vStreamProc_LengthType minOutputLength = 0u;
  vStreamProc_LengthType availableOutputLength = 0u;
  vStreamProc_LengthType outputLength = 0;

  vStreamProc_StorageInfoPtrType  inputStorageInfo = &InputPortInfo->ReadRequest.StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  minOutputLength = specializedCfgPtr->MinOutputLengthOfProcessingNode_CipherAead_Config;
  tagLength = specializedCfgPtr->SizeOfAuthenticationTagOfProcessingNode_CipherAead_Config;

  /* Signal is acknowledged by default. Flag is later cleared if necessary. */
  vStreamProc_SetSignalResponseFlag(*SignalResponsePtr, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

  availableInputLength = inputStorageInfo->AvailableLength;
  availableOutputLength = OutputPortInfo->WriteRequest.StorageInfo.AvailableLength;

  /* #20 Always cut off the configured tag length so the tag can be handled within FinishCipher. */
  if (availableInputLength > tagLength)
  {
    availableInputLength -= tagLength;
  }
  else
  {
    availableInputLength = 0u;
  }

  /* #30 Update request length. */
  inputStorageInfo->RequestLength = availableInputLength;

  /* Max input length is always capped to a multiple of the minimum output length due to the unknown number of
      buffered bytes in the crypto driver. */
  maxInputLength = (availableOutputLength / minOutputLength) * minOutputLength;

  if (maxInputLength < availableInputLength)
  {
    /* Not all available data can be consumed, limit to max input length.*/
    inputStorageInfo->RequestLength = maxInputLength;
  }

  outputLength = maxInputLength;

  if (maxInputLength > 0u)
  {
    OutputPortInfo->WriteRequest.StorageInfo.RequestLength = outputLength;

    /* #10 Get stream data. */
    retVal = vStreamProc_RequestPortData(
      ProcNodeInfo,
      InputPortInfo,
      1u,
      OutputPortInfo,
      1u
    );

    if (retVal == VSTREAMPROC_OK)
    {
      /* #40 Hand plain text data over to CSM. */
      /* Call the CSM. The Request lengths from the ReadRequest/Write Request will be used as input. */
      /* The Request lengths are updated depending on the CSM operation result. */
      retVal = vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation(
        specializedCfgPtr,
        CRYPTO_OPERATIONMODE_UPDATE,
        InputPortInfo->ReadRequest.Buffer,
        InputPortInfo->ReadRequest.StorageInfo.RequestLength,
        &associatedDataBuffer,
        0u,
        (uint8*)NULL_PTR, /* Tag is not actually consumed within update, handled within FinishCipher! */
        0u, /* Tag is not actually consumed within update, handled within FinishCipher! */
        OutputPortInfo->WriteRequest.Buffer,
        &outputLength,
        (Crypto_VerifyResultType*)NULL_PTR, /* Verify pointer is not handled within update */
        SignalResponsePtr
      );

      /* #50 Save how much data was actually produced. */
      OutputPortInfo->WriteRequest.StorageInfo.RequestLength = outputLength;
    }
  }
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleIvReception
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleIvReception(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponsePtr,
  P2VAR(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA)   InputPortInfo,
  vStreamProc_SizeOfInitializationVectorOfProcessingNode_CipherAead_ConfigType IvLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  vStreamProc_StorageInfoPtrType inputStorageInfo = &InputPortInfo->ReadRequest.StorageInfo;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Compare available length with configured IV length. */
  if (inputStorageInfo->AvailableLength >= IvLength)
  {
    /* #20 Set the key element. */
    Std_ReturnType csmResult = Csm_KeyElementSet(
      specializedCfgPtr->CsmKeyIdOfProcessingNodeOfProcessingNode_CipherAead_Config,
      CRYPTO_KE_CIPHER_IV,
      InputPortInfo->ReadRequest.Buffer,
      IvLength
    );

    if (csmResult == E_OK)
    {
      /* #30 Validate the key. */
      csmResult = Csm_KeySetValid(specializedCfgPtr->CsmKeyIdOfProcessingNodeOfProcessingNode_CipherAead_Config);
    }

    /* #40 Handle the CSM result. Set the signal response accordingly. */
    if (csmResult == E_OK)
    {
      inputStorageInfo->RequestLength = IvLength;
      retVal = VSTREAMPROC_OK;
      if (inputStorageInfo->AvailableLength == IvLength)
      {
        *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
      }
      else
      {
        *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_PENDING;
      }
    }
    else if (csmResult == CRYPTO_E_BUSY)
    {
      retVal = VSTREAMPROC_PENDING;
      *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_PENDING;
    }
    else
    {
      /* SignalResponse is initialized with FAILED, so nothing to do here. */
    }
  }
  else
  {
    /* #50 If necessary: Wait for more data. */
    ProcNodeInfo->InputPortResults[InputPortInfo->SymbolicPortName] = VSTREAMPROC_INSUFFICIENT_INPUT;
    *SignalResponsePtr = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_PropagateResult
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_PropagateResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  Std_ReturnType Result)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_OutputPortInfoType resultPortInfo;
  vStreamProc_WaveInfoType waveInfo;

  retVal = vStreamProc_GetInputPortWaveInfo(
    ProcNodeInfo->ProcessingNodeId,
    0u,
    &waveInfo
  );

  if (retVal == VSTREAMPROC_OK)
  {
    /* #10 Check if output port is used. */
    retVal = vStreamProc_PrepareOutputPortInfo(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_ResultData,
      &resultPortInfo
    );
  }
  if (retVal == VSTREAMPROC_OK)
  {
    /* #20 Check if result can be stored. */
    /* Report "insufficient data" when no data is available by requesting a single entry. */
    retVal = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_Std_ReturnType,
      1u,
      &resultPortInfo
    );
  }

  if (retVal == VSTREAMPROC_OK)
  {
    /* #30 Write result to output port */
    P2VAR(Std_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) resultBuffer =
      vStreamProc_GetTypedWriteRequestBuffer_Std_ReturnType(&resultPortInfo.WriteRequest);

    resultBuffer[0u] = Result;

    retVal = vStreamProc_AcknowledgeOutputPort(
      ProcNodeInfo,
      1u,
      TRUE,
      &resultPortInfo
    );
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleRemainingData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleRemainingData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType OutputLength,
  P2VAR(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputPortInfo,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) LocalBuffer
)
{
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;

  /* #10 Check if data copying is necessary. */
  if (OutputLength > 0u)
  {
    uint8 index;

    /* #20 Request port data. */
    retVal = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      OutputLength,
      OutputPortInfo);

    /* #30 If operation succeeded: */
    if (retVal == VSTREAMPROC_OK)
    {
      /* #40 Copy data. */
      for (index = 0u; index < OutputLength; index++)
      {
        OutputPortInfo->WriteRequest.Buffer[index] = LocalBuffer[index];
      }

      /* #50 Acknowledge port. */
      retVal = vStreamProc_AcknowledgeOutputPort(
        ProcNodeInfo,
        OutputLength,
        TRUE,
        OutputPortInfo);
    }
    /* #60 Otherwise, return FAILED. */
    /* Hint: This is not solvable, as there is currently no suitable buffer to store the csm output data until
       storage is available again. */
    else
    {
      retVal = VSTREAMPROC_FAILED;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_HandleResult
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_HandleResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_VAR) ProcNodeInfo,
  boolean IsConnected,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_VAR) SignalResponse,
  Crypto_VerifyResultType CsmResult
)
{
  vStreamProc_ReturnType currentResult = VSTREAMPROC_OK;
  vStreamProc_SignalResponseType signalResponse = *SignalResponse;

  /* #10 If optional port is used: Propagate verification result here. */
  if (IsConnected == TRUE)
  {
    Std_ReturnType propagatedResult = E_OK;

    if (CsmResult == CSM_E_VER_NOT_OK)
    {
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
      propagatedResult = E_NOT_OK;
    }

    currentResult = vStreamProc_ProcessingNode_CipherAead_PropagateResult(ProcNodeInfo, propagatedResult);

    if (currentResult != VSTREAMPROC_OK)
    {
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
    }

    /* #20 In case anything goes wrong, propagate the result only through the optional port -> make internal values OK. */
    ProcNodeInfo->InputPortResults[vStreamProcConf_vStreamProcInputPort_vStreamProc_CipherAead_InputData] = VSTREAMPROC_OK;
  }
  else
  {
    /* #30 Handle CSM verification result. */
    if (CsmResult == CSM_E_VER_OK)
    {
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
    }
    else
    {
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
    }
  }

  *SignalResponse = signalResponse;

  return currentResult;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* Hint: User must successfully call Csm_KeyElementSet beforehand.*/
  /* #10 Simply return E_OK. */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return E_OK;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  VAR(vStreamProc_SignalResponseType, AUTOMATIC) signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  vStreamProc_SizeOfInitializationVectorOfProcessingNode_CipherAead_ConfigType ivLength;
  Std_ReturnType csmResult;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  ivLength = specializedCfgPtr->SizeOfInitializationVectorOfProcessingNode_CipherAead_Config;

  /* #10 Set the workspace state to UNDEFINED. */
  workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;
  if (ivLength == 0u)
  {
    /* Hint: If the IV length is 0, the IV must have previously been set by the user. */
    /* #20 If no initialization vector is configured (i.e. ivLength == 0) prepare the CSM at this point. */
    csmResult = vStreamProc_ProcessingNode_CipherAead_InitLocal(ProcNodeInfo);
    if (csmResult == E_OK)
    {
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
    }
    else
    {
      /* Leave as signalResponse value failed. */
    }
  }
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_UpdateCipher
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_UpdateCipher(/* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* Hint: Associated data is currently never used, but it is required to be a valid pointer for the crypto stack. => Use stack address. */
  vStreamProc_InputPortInfoType   inputPortInfo;
  vStreamProc_OutputPortInfoType  outputPortInfo;
  vStreamProc_LengthType          minOutputLength;
  vStreamProc_SizeOfAuthenticationTagOfProcessingNode_CipherAead_ConfigType tagLength;
  vStreamProc_SizeOfInitializationVectorOfProcessingNode_CipherAead_ConfigType ivLength;
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult = VSTREAMPROC_FAILED;
  Std_ReturnType csmResult;

  vStreamProc_LengthType          availableInputLength = 0u;
  vStreamProc_LengthType          processedInputLength = 0u;
  vStreamProc_LengthType          availableOutputLength = 0u;
  vStreamProc_LengthType          writtenOutputLength = 0u;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  ivLength = specializedCfgPtr->SizeOfInitializationVectorOfProcessingNode_CipherAead_Config;
  minOutputLength = specializedCfgPtr->MinOutputLengthOfProcessingNode_CipherAead_Config;
  tagLength = specializedCfgPtr->SizeOfAuthenticationTagOfProcessingNode_CipherAead_Config;

  currentResult = vStreamProc_ProcessingNode_CipherAead_QueryPorts(
    ProcNodeInfo,
    &inputPortInfo,
    &outputPortInfo,
    1u,
    0u
  );

  availableInputLength = inputPortInfo.ReadRequest.StorageInfo.AvailableLength;
  availableOutputLength = outputPortInfo.WriteRequest.StorageInfo.AvailableLength;

  if (currentResult == VSTREAMPROC_OK)
  {
    /* #10 Get the available port input data. */
    currentResult = vStreamProc_RequestInputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      0u,
      &inputPortInfo
    );

    if (currentResult == VSTREAMPROC_OK)
    {
      /* #20 If the IV is given through the data stream: Handle reception and setup CSM. */
      if (inputPortInfo.StreamPositionInfo.RelativePosition < ivLength)
      {
        currentResult = vStreamProc_ProcessingNode_CipherAead_HandleIvReception(
          ProcNodeInfo,
          &signalResponse,
          &inputPortInfo,
          ivLength
        );

        if (currentResult == VSTREAMPROC_OK)
        {
          csmResult = vStreamProc_ProcessingNode_CipherAead_InitLocal(ProcNodeInfo);
          if (csmResult == E_NOT_OK)
          {
            /* Leave as VSTREAMPROC_SIGNALRESPONSE_FAILED */
          }
        }

        (void)vStreamProc_AcknowledgeInputPort(
          ProcNodeInfo,
          inputPortInfo.ReadRequest.StorageInfo.RequestLength,
          TRUE,
          &inputPortInfo
        );
      }
      /* Everything that happens here is behind the IV, will also be hit in case no IV is configured (ivLength == 0) */
      /* #30 Otherwise handle all incoming encrypted data, excluding the tag. */
      else
      {
        currentResult = vStreamProc_ProcessingNode_CipherAead_HandleDataReception(
          ProcNodeInfo,
          &signalResponse,
          &inputPortInfo,
          &outputPortInfo
        );

        /* Ports are always acknowledged to unlock buffers. */
        (void)vStreamProc_AcknowledgePorts(
          ProcNodeInfo,
          &inputPortInfo,
          1u,
          &outputPortInfo,
          1u
        );
      }
    }
    processedInputLength = inputPortInfo.ReadRequest.StorageInfo.RequestLength;
    writtenOutputLength = outputPortInfo.WriteRequest.StorageInfo.RequestLength;
  }
  else
  {
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);
  }

  if (currentResult != VSTREAMPROC_FAILED)
  {
    /* The Signal Handler are only updated if the current signal is acknowledged. */
    /* #40 Update signal handlers if necessary. */
    if (vStreamProc_IsSignalResponseFlagSet(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK))
    {
      vStreamProc_ProcessingNode_CipherAead_UpdateSignalHandlerStates(
        ProcNodeInfo,
        (availableInputLength - processedInputLength - tagLength),
        (availableOutputLength - writtenOutputLength),
        minOutputLength
      );
    }

    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_FinishCipher
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_FinishCipher(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_InputPortInfoType   inputPortInfo;
  vStreamProc_OutputPortInfoType  outputPortInfos[vStreamProcConf_OutputPortCount_Internal_CipherAeadNode];
  vStreamProc_SizeOfInitializationVectorOfProcessingNode_CipherAead_ConfigType ivLength;
  Crypto_VerifyResultType verifyResult = CSM_E_VER_NOT_OK;

  /* Hint: Associated data is currently never used, but it is required to be a valid pointer for the crypto stack.
     => Use stack. */
  uint8 associatedDataBuffer = 0u;
  vStreamProc_LengthType          outputLength = 0u;
  vStreamProc_SizeOfAuthenticationTagOfProcessingNode_CipherAead_ConfigType tagLength;
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult;
  vStreamProc_LengthType dataRemainder = 0u;
  uint8 localBuffer[vStreamProcConf_MaxCipherAeadMinOutputLength] = { 0u };

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  tagLength = specializedCfgPtr->SizeOfAuthenticationTagOfProcessingNode_CipherAead_Config;
  inputPortInfo.ReadRequest.StorageInfo.RequestLength = tagLength;

  ivLength = specializedCfgPtr->SizeOfInitializationVectorOfProcessingNode_CipherAead_Config;

  currentResult = vStreamProc_ProcessingNode_CipherAead_QueryPorts(
    ProcNodeInfo,
    &inputPortInfo,
    outputPortInfos,
    vStreamProcConf_OutputPortCount_Internal_CipherAeadNode,
    0u);

  /* #10 Check that the actual processing is past the IV and if at least the tag length is available. */
  if ( (currentResult == VSTREAMPROC_OK)
    && (inputPortInfo.StreamPositionInfo.RelativePosition >= ivLength)
    && (inputPortInfo.ReadRequest.StorageInfo.AvailableLength >= tagLength))
  {
    /* #20 Get the input data. */
    currentResult = vStreamProc_RequestInputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      tagLength,
      &inputPortInfo);

    /* #30 Process additional data before the tag if necessary. */
    if ((currentResult == VSTREAMPROC_OK) && (inputPortInfo.ReadRequest.StorageInfo.AvailableLength > tagLength))
    {
      outputLength = vStreamProcConf_MaxCipherAeadMinOutputLength;
      dataRemainder = inputPortInfo.ReadRequest.StorageInfo.AvailableLength - tagLength;

      currentResult = vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation(
        specializedCfgPtr,
        CRYPTO_OPERATIONMODE_UPDATE,
        inputPortInfo.ReadRequest.Buffer,
        dataRemainder,
        &associatedDataBuffer,
        0u,
        (uint8 *)NULL_PTR, /* Tag is not actually consumed within update, handled within FinishCipher! */
        0u, /* Tag is not actually consumed within update, handled within FinishCipher! */
        localBuffer,
        &outputLength,
        (Crypto_VerifyResultType *)NULL_PTR, /* Verify pointer is not handled within update */
        &signalResponse);

      if (currentResult == VSTREAMPROC_OK)
      {
        currentResult = vStreamProc_ProcessingNode_CipherAead_HandleRemainingData(
          ProcNodeInfo,
          outputLength,
          &outputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData],
          localBuffer);
      }
    }

    /* #40 Perform the authentication. */
    if (currentResult == VSTREAMPROC_OK)
    {
      /* Only the tag is read within FinishCipher, everything else was handled within UpdateCipher! */
      outputLength = vStreamProcConf_MaxCipherAeadMinOutputLength;

      /* Hint: inputPortInfo is used on purpose without previous port request.
         The structure was initialized to zero values by vStreamProc_PrepareAllPortInfos(),
         which means the CSM will not use any input data. */
      /* #50 Provide the tag data to the CSM and finish the operation. */
      currentResult = vStreamProc_ProcessingNode_CipherAead_PerformCipherOperation(
        specializedCfgPtr,
        CRYPTO_OPERATIONMODE_FINISH,
        NULL_PTR,
        0u,
        &associatedDataBuffer, /* Not actually handled here. */
        0u,
        &inputPortInfo.ReadRequest.Buffer[dataRemainder], /* Input is used as tag */
        tagLength,
        localBuffer,
        &outputLength,
        &verifyResult,
        &signalResponse);

      /* #60 Acknowledge the input data. */
      if (currentResult == VSTREAMPROC_OK)
      {
        (void)vStreamProc_AcknowledgeInputPort(
          ProcNodeInfo,
          inputPortInfo.ReadRequest.StorageInfo.AvailableLength,
          TRUE,
          &inputPortInfo);
      }

      if (currentResult == VSTREAMPROC_OK)
      {
        currentResult = vStreamProc_ProcessingNode_CipherAead_HandleRemainingData(
          ProcNodeInfo,
          outputLength,
          &outputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_OutputData],
          localBuffer);
      }

      /* #70 Handle the result. */
      if (currentResult == VSTREAMPROC_OK)
      {
        currentResult = vStreamProc_ProcessingNode_CipherAead_HandleResult(
          ProcNodeInfo,
          outputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_CipherAead_ResultData].IsConnected,
          &signalResponse,
          verifyResult);
      }

      /* #80 Reset the workspace state. */
      workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;
    }
  }

  /* #90 Report FAILED as signal response if an internal error occurred. */
  if (currentResult == VSTREAMPROC_FAILED)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

#if  (VSTREAMPROC_PROCESSINGNODETYPE_CIPHERAEAD_STORE_CSM_CONTEXT == STD_ON)
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Hint: Actual size of context is written by CSM. */
  workspace->SizeOfStoredCsmContext = specializedCfgPtr->SizeOfContextOfProcessingNode_CipherAead_Config;

  /* #10 Check if there is a context buffer. */
  if (specializedCfgPtr->SizeOfContextOfProcessingNode_CipherAead_Config > 0u)
  {
    workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

    /* #20 Check if the node is in PROCESSING state. */
    if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
    {
      Std_ReturnType csmRetVal;

      /* #30 Save the CSM context. */
      csmRetVal = Csm_SaveContextJob(
        specializedCfgPtr->CsmJobIdOfProcessingNode_CipherAead_Config,
        workspace->CsmContextBuffer,
        &workspace->SizeOfStoredCsmContext);

      /* #40 Handle the CSM result. */
      switch (csmRetVal)
      {
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received,. */
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
        break;
      }
      }
    }
  }

#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
#endif

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If the node is currently processing, cancel the job. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    (void)Csm_CancelJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_CipherAead_Config,
      CRYPTO_OPERATIONMODE_FINISH
    );
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;

    /* #20 Reset workspace state to UNDEFINED. */
    workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CipherAead_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CipherAead_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
#if  (VSTREAMPROC_PROCESSINGNODETYPE_CIPHERAEAD_STORE_CSM_CONTEXT == STD_ON)
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_CipherAead_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CipherAead_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  Std_ReturnType csmRetVal;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CipherAead_Config(ProcNodeInfo);                       /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CipherAead_WorkspaceType(ProcNodeInfo);                     /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Restore the context. */
    csmRetVal = Csm_RestoreContextJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_CipherAead_Config,
      workspace->CsmContextBuffer,
      workspace->SizeOfStoredCsmContext);

    switch (csmRetVal)
    {
      /* #30 If the storing was successful, confirm the signal. */
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      /* #40 If the CSM is busy, return pending to try again in the next cycle. */
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      /* #50 If any other return value: Consider as error. */
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received. Leave signal response as failed.*/
        break;
      }
    }
  }
  /* #60 Otherwise: Nothing to do, acknowledge the signal. */
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#endif
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_CIPHERAEAD_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_CipherAead.c
 *********************************************************************************************************************/
