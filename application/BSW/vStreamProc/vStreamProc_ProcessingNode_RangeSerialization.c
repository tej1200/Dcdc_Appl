/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!       \file   vStreamProc_ProcessingNode_RangeSerialization.c
 *        \brief  vStreamProc Range Serialization Processing Node Source Code File
 *
 *      \details  Implementation of the vStreamProc range serialization processing node
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_RANGESERIALIZATION_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_RangeSerialization.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc_Cfg.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_RANGESERIALIZATION == STD_ON)
/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

#define VSTREAMPROC_RANGESERIALIZATION_LENGTHSIZE 4u
#define VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE 4u

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_RangeSerialization_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_RangeSerialization_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_RangeSerialization_SignalHandler_DataAvailable
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_RangeSerialization_SignalHandler_DataAvailable(/* PRQA S 6010 */ /* MD_MSR_STPTH */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType retVal;
  vStreamProc_InputPortInfoType inputPortInfo;
  vStreamProc_OutputPortInfoType outputPortInfo;
  vStreamProc_WaveInfoType waveInfo;

  /* ----- Implementation ----------------------------------------------- */
  inputPortInfo.SymbolicPortName = vStreamProcConf_vStreamProcInputPort_vStreamProc_RangeSerialization_Range;
  outputPortInfo.SymbolicPortName = vStreamProcConf_vStreamProcOutputPort_vStreamProc_RangeSerialization_OutputData;

  retVal = vStreamProc_GetInputPortWaveInfo(
    ProcNodeInfo->ProcessingNodeId,
    inputPortInfo.SymbolicPortName,
    &waveInfo);

  if (retVal == VSTREAMPROC_OK)
  {
    if (waveInfo.State == VSTREAMPROC_WAVESTATE_ENDING)
    {
      (void)vStreamProc_PreparePortInfos(
        ProcNodeInfo,
        &inputPortInfo,
        1u,
        &outputPortInfo,
        1u
      );

      /* #10 Start the output port wave. */
      (void)vStreamProc_GetInputPortWaveInfo(ProcNodeInfo->ProcessingNodeId,
        inputPortInfo.SymbolicPortName,
        &waveInfo);

      retVal = vStreamProc_StartOutputPortWave(
        ProcNodeInfo->ProcessingNodeId,
        outputPortInfo.SymbolicPortName,
        &waveInfo);

      /* #20 Get port infos. */
      if (retVal == VSTREAMPROC_OK)
      {
        retVal = vStreamProc_GetInputPortInfo(
          ProcNodeInfo,
          VSTREAMPROC_NO_DATATYPE,
          &inputPortInfo);
      }

      /* #30 Request serialized data size for output data stream. */
      if (retVal == VSTREAMPROC_OK)
      {
        retVal = vStreamProc_RequestOutputPortData(
          ProcNodeInfo,
          vStreamProcConf_vStreamProcDataType_uint8,
          VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE + VSTREAMPROC_RANGESERIALIZATION_LENGTHSIZE,
          &outputPortInfo
        );
      }

      /* #40 If data could successfully be retrieved: */
      if (retVal == VSTREAMPROC_OK)
      {
        uint8_least posIndex;
        uint8_least lenIndex;
        vStreamProc_StreamPositionType startPosition = inputPortInfo.StreamPositionInfo.AbsolutePositionStart;
        vStreamProc_LengthType totalLength = inputPortInfo.StreamPositionInfo.TotalLength;

        /* #50 Serialize data. */
        for (posIndex = 0; posIndex < VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE; posIndex++)
        {
          uint8 byteValue = (uint8)(startPosition & 0xFFu);
          startPosition >>= 8;
          outputPortInfo.WriteRequest.Buffer[VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE - posIndex - 1u] = byteValue;
        }

        for (lenIndex = 0; lenIndex < VSTREAMPROC_RANGESERIALIZATION_LENGTHSIZE; lenIndex++)
        {
          uint8 byteValue = (uint8)(totalLength & 0xFFu);
          totalLength >>= 8;
          outputPortInfo.WriteRequest.Buffer[VSTREAMPROC_RANGESERIALIZATION_LENGTHSIZE + VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE - lenIndex - 1u] = byteValue;
        }

        /* #60 Acknowledge data. */
        inputPortInfo.ReadRequest.StorageInfo.RequestLength = inputPortInfo.StreamPositionInfo.TotalLength;
        outputPortInfo.WriteRequest.StorageInfo.RequestLength =
          VSTREAMPROC_RANGESERIALIZATION_POSITIONSIZE
          + VSTREAMPROC_RANGESERIALIZATION_LENGTHSIZE;

        retVal = vStreamProc_AcknowledgePorts(
          ProcNodeInfo,
          &inputPortInfo,
          1u,
          &outputPortInfo,
          1u
        );
      }

      /* #70 End output port wave. */
      if (retVal == VSTREAMPROC_OK)
      {
        retVal = vStreamProc_EndOutputPortWave(
          ProcNodeInfo->ProcessingNodeId,
          vStreamProcConf_vStreamProcOutputPort_vStreamProc_RangeSerialization_OutputData,
          &waveInfo);
      }

      /* #80 Confirm the signal. */
      if (retVal == VSTREAMPROC_OK)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
      }
    }
  }

  return signalResponse;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_RANGESERIALIZATION == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_RangeSerialization.c
 *********************************************************************************************************************/
