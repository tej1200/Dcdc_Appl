/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_ProcessingNode.c
 *        \brief  vStreamProc processing node specific source code file
 *
 *      \details  Implementation of interfaces for the vStreamProc processing node modules.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_PROCESSING_NODE_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_Scheduler.h"

# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
#  include "Det.h"
# endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/*! Function pointer for input port operations (used by iterator). */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_InputPortOperationPtrType)(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);


/*! Function pointer for output port operations (used by iterator). */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_OutputPortOperationPtrType)(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/*! Configuration element for port operations (used by iterator). */
typedef struct
{
  vStreamProc_InputPortOperationPtrType   InputPortOperation;
  vStreamProc_OutputPortOperationPtrType  OutputPortOperation;
} vStreamProc_PortOperationConfigType;

/*! Symbolic names of port operations (used by iterator). */
typedef enum
{
  VSTREAMPROC_PORT_OPERATION_GET_INFO,
  VSTREAMPROC_PORT_OPERATION_REQUEST_DATA,
  VSTREAMPROC_PORT_OPERATION_ACKNOWLEDGE,
  VSTREAMPROC_PORT_OPERATION_COUNT
} vStreamProc_PortOperationType;

/*! Parameter structure for port iterator. */
typedef struct
{
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo;
  vStreamProc_PortOperationType   Operation;
  boolean                         AbortOnError;
} vStreamProc_PortIteratorParamType;

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vStreamProc_ProcessingNode_SetResultBySeverity()
 *********************************************************************************************************************/
/*! \brief          Set the result, taking severity into account.
 *  \details        Overwrite the output result by the input results, if the latter is more severe.
 *  \param[in]      InputResult   The current result value.
 *  \param[in,out]  OutputResult  In: The previous result value. Out: The new result value.
 *  \context        TASK
 *  \reentrant      TRUE
 *  \pre            -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SetResultBySeverity(
  vStreamProc_ReturnType InputResult,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputResult);

/**********************************************************************************************************************
 * vStreamProc_ProcessingNode_InitPortIteratorParam()
 *********************************************************************************************************************/
/*!
 * \brief          Initialize parameter structure for port iterator.
 * \details        -
 * \param[in]      ProcNodeInfo      The processing node information to operate on.
 * \param[in]      Operation         The operation to apply to all port structures.
 * \param[in]      AbortOnError      Abort the iteration in case any other result than VSTREAMPROC_OK is returned
 *                                   when applying the operation to an port structure.
 * \return         Initialized vStreamProc_PortIteratorParamType structure.
 * \pre            The pipe is open.
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_PortIteratorParamType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_InitPortIteratorParam(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_PortOperationType Operation,
  boolean AbortOnError);

/**********************************************************************************************************************
 * vStreamProc_ProcessingNode_IteratePortInfos()
 *********************************************************************************************************************/
/*!
 * \brief          Iterate over all passed port structures and apply the given operation.
 * \details        -
 * \param[in]      IteratorParam     The iterator parameter structure.
 * \param[in,out]  InputPortInfos    Input port information structures. NULL_PTR if not used.
 * \param[in]      InputPortCount    Number of input port information structures.
 * \param[in,out]  OutputPortInfos   Output port information structures. NULL_PTR if not used.
 * \param[in]      OutputPortCount   Number of output port information structures.
 * \return         VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return         VSTREAMPROC_INSUFFICIENT_OUTPUT   At least one port cannot provide the requested minimum storage.
 * \return         VSTREAMPROC_INSUFFICIENT_INPUT    At least one port cannot provide the requested minimum data.
 * \return         VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre            The pipe is open.
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_IteratePortInfos(
  vStreamProc_PortIteratorParamType IteratorParam,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ReleasePorts()
 *********************************************************************************************************************/
/*!
 * \brief          Iterate over all passed port structures and release each port.
 * \details        -
 * \param[in]      ProcNodeInfo      The processing node information to operate on.
 * \param[in,out]  InputPortInfos    Input port information structures. NULL_PTR if not used.
 * \param[in]      InputPortCount    Number of input port information structures.
 * \param[in,out]  OutputPortInfos   Output port information structures. NULL_PTR if not used.
 * \param[in]      OutputPortCount   Number of output port information structures.
 * \return         VSTREAMPROC_FAILED                Operation failed for at least one port.
 * \return         VSTREAMPROC_OK                    Operation was successful for all ports.
 * \pre            The pipe is open.
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ReleasePorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoConstPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoConstPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_SetResultBySeverity()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SetResultBySeverity(
  vStreamProc_ReturnType InputResult,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) OutputResult)
{
  /* #10 Overwrite the output result by the input results, if the latter is more severe. */
  if (InputResult > * OutputResult)
  {
    *OutputResult = InputResult;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_InitPortIteratorParam()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_PortIteratorParamType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_InitPortIteratorParam(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_PortOperationType Operation,
  boolean AbortOnError)
{
  vStreamProc_PortIteratorParamType iteratorParam;

  /* #10 Assign passed parameters to structure members and return by value. */
  iteratorParam.ProcNodeInfo = ProcNodeInfo;
  iteratorParam.Operation = Operation;
  iteratorParam.AbortOnError = AbortOnError;

  return iteratorParam;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_IteratePortInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_IteratePortInfos(
  vStreamProc_PortIteratorParamType IteratorParam,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Constants ---------------------------------------------- */
  /*! Lookup table for port operation functions. */
  CONST(vStreamProc_PortOperationConfigType, AUTOMATIC) vStreamProc_StreamOperationConfig[VSTREAMPROC_PORT_OPERATION_COUNT] =
  {
    { vStreamProc_Stream_GetInputPortInfo,        vStreamProc_Stream_GetOutputPortInfo },
    { vStreamProc_Stream_RequestInputPortData,    vStreamProc_Stream_RequestOutputPortData },
    { vStreamProc_Stream_AcknowledgeInputPort,    vStreamProc_Stream_AcknowledgeOutputPort },
  };

  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_OK;
  vStreamProc_ReturnType        opRetVal;
  vStreamProc_InputPortIdType   inputPortIndex;
  vStreamProc_OutputPortIdType  outputPortIndex;
  vStreamProc_ProcessingNodeIdType processingNodeId = IteratorParam.ProcNodeInfo->ProcessingNodeId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 For all input port infos */
  for (inputPortIndex = 0u; inputPortIndex < InputPortCount; inputPortIndex++)
  {
    vStreamProc_InputPortHandleType inputPortHandle;

    (void)vStreamProc_Stream_GetInputPortHandle(
      processingNodeId,
      InputPortInfos[inputPortIndex].SymbolicPortName,
      &inputPortHandle);

    /* #20 Execute input port operation. */
    opRetVal =
      vStreamProc_StreamOperationConfig[IteratorParam.Operation].InputPortOperation(
        &inputPortHandle,
        &InputPortInfos[inputPortIndex].ReadRequest,
        &InputPortInfos[inputPortIndex].MetaData,
        &InputPortInfos[inputPortIndex].StreamPositionInfo);

    /* #30 Write the port result to the processing node info. */
    if (IteratorParam.Operation == VSTREAMPROC_PORT_OPERATION_ACKNOWLEDGE)
    {
      vStreamProc_ProcessingNode_SetResultBySeverity(
        opRetVal,
        &IteratorParam.ProcNodeInfo->InputPortResults[inputPortIndex]);
    }
    else
    {
      IteratorParam.ProcNodeInfo->InputPortResults[inputPortIndex] = opRetVal;
    }

    /* Results with higher severity overwrite existing value. */
    if (opRetVal > retVal)
    {
      retVal = opRetVal;

      /* #40 Abort further processing if requested. */
      if (IteratorParam.AbortOnError == TRUE)
      {
        break;
      }
    }
  }

  if ((retVal == VSTREAMPROC_OK) || (IteratorParam.AbortOnError == FALSE))
  {
    /* #50 For all output port infos */
    for (outputPortIndex = 0u; outputPortIndex < OutputPortCount; outputPortIndex++)
    {
      vStreamProc_OutputPortHandleType outputPortHandle;

      (void)vStreamProc_Stream_GetOutputPortHandle(
        processingNodeId,
        OutputPortInfos[outputPortIndex].SymbolicPortName,
        &outputPortHandle);

      /* #60 Execute output port operation. */
      opRetVal =
        vStreamProc_StreamOperationConfig[IteratorParam.Operation].OutputPortOperation(
          &outputPortHandle,
          &OutputPortInfos[outputPortIndex].WriteRequest,
          &OutputPortInfos[outputPortIndex].MetaData,
          &OutputPortInfos[outputPortIndex].StreamPositionInfo);

      /* #70 Write the port result to the processing node info. */
      if (IteratorParam.Operation == VSTREAMPROC_PORT_OPERATION_ACKNOWLEDGE)
      {
        vStreamProc_ProcessingNode_SetResultBySeverity(
          opRetVal,
          &IteratorParam.ProcNodeInfo->OutputPortResults[outputPortIndex]);
      }
      else
      {
        IteratorParam.ProcNodeInfo->OutputPortResults[outputPortIndex] = opRetVal;
      }

      /* Results with higher severity overwrite existing value. */
      if (opRetVal > retVal)
      {
        retVal = opRetVal;

        /* #80 Abort further processing if requested. */
        if (IteratorParam.AbortOnError == TRUE)
        {
          break;
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ReleasePorts()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ReleasePorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoConstPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoConstPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;                                                                       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ReturnType opRetVal;
  vStreamProc_ProcessingNodeIdType processingNodeId = ProcNodeInfo->ProcessingNodeId;
  uint32_least inputPortInfoIdx;
  uint32_least outputPortInfoIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed port structures and call the release API. */
  for (inputPortInfoIdx = 0u; inputPortInfoIdx < InputPortCount; inputPortInfoIdx++)
  {
    vStreamProc_InputPortSymbolicNameType inputPortSymbolicName = InputPortInfos[inputPortInfoIdx].SymbolicPortName;
    vStreamProc_InputPortHandleType inputPortHandle;

    (void)vStreamProc_Stream_GetInputPortHandle(
      processingNodeId,
      inputPortSymbolicName,
      &inputPortHandle);

    opRetVal = vStreamProc_Stream_ReleaseInputPort(&inputPortHandle);

    vStreamProc_ProcessingNode_SetResultBySeverity(
      opRetVal,
      &ProcNodeInfo->InputPortResults[inputPortSymbolicName]);

    /* Results with higher severity overwrite existing value. */
    if (opRetVal > retVal)
    {
      retVal = opRetVal;
    }
  }

  for (outputPortInfoIdx = 0u; outputPortInfoIdx < OutputPortCount; outputPortInfoIdx++)
  {
    vStreamProc_OutputPortSymbolicNameType outputPortSymbolicName =
      OutputPortInfos[outputPortInfoIdx].SymbolicPortName;
    vStreamProc_OutputPortHandleType outputPortHandle;

    (void)vStreamProc_Stream_GetOutputPortHandle(
      processingNodeId,
      outputPortSymbolicName,
      &outputPortHandle);

    opRetVal = vStreamProc_Stream_ReleaseOutputPort(&outputPortHandle);

    vStreamProc_ProcessingNode_SetResultBySeverity(
      opRetVal,
      &ProcNodeInfo->OutputPortResults[outputPortSymbolicName]);

    /* Results with higher severity overwrite existing value. */
    if (opRetVal > retVal)
    {
      retVal = opRetVal;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  PROCESSING NODE API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_GetTypedWorkspaceOfProcessingNode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_GenericNodeWorkspaceType, VSTREAMPROC_CODE) vStreamProc_GetTypedWorkspaceOfProcessingNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_WorkspaceTypeIdType WorkspaceTypeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                                 errorID       = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_GenericNodeWorkspaceType  workspacePtr  = NULL_PTR;                                                       /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if ProcessingNodeId is an invalid ID */
  if (ProcNodeInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Compare workspace type of processing node against expected one. */
  else if (ProcNodeInfo->WorkspaceInfo.TypeId != WorkspaceTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Return workspace pointer of processing node */
    workspacePtr = ProcNodeInfo->WorkspaceInfo.Pointer;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_GET_TYPED_WORKSPACE_OF_PROCESSING_NODE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(WorkspaceTypeId);                                                                         /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return workspacePtr;
}

/**********************************************************************************************************************
 *  vStreamProc_GetTypedConfigOfProcessingNode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_GenericNodeConfigType, VSTREAMPROC_CODE) vStreamProc_GetTypedConfigOfProcessingNode(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_ConfigTypeIdType ConfigTypeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                             errorID   = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_GenericNodeConfigType configPtr = NULL_PTR;                                                               /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if ProcessingNodeId is an invalid ID */
  if (ProcNodeInfo == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  /* Compare configuration type of processing node definition against expected one. */
  else if (ProcNodeInfo->ConfigInfo.TypeId != ConfigTypeId)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Return configuration pointer of processing node definition */
    configPtr = ProcNodeInfo->ConfigInfo.Pointer;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_GET_TYPED_CONFIG_OF_PROCESSING_NODE, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(ConfigTypeId);                                                                            /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return configPtr;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareInputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareInputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_InputPortInfoPtrType InputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ProcessingNodeIdType processingNodeId = ProcNodeInfo->ProcessingNodeId;
  vStreamProc_NamedInputPortIdType namedInputPortCount =
    vStreamProc_GetNamedInputPortLengthOfProcessingNode(processingNodeId);
  vStreamProc_ReadRequestPtrType readRequest = &InputPortInfo->ReadRequest;
  vStreamProc_ReadRequestPtrType metaData = &InputPortInfo->MetaData;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set symbolic name in info structure.  */
  InputPortInfo->SymbolicPortName = InputPortSymbolicName;

  /* #20 Initialize read request with default values. */
  readRequest->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&readRequest->StorageInfo);

  /* #30 Initialize MetaData with default values. */
  metaData->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&metaData->StorageInfo);

  /* #40 Check if the provided symbolic name is valid. */
  if (InputPortSymbolicName < namedInputPortCount)
  {
    vStreamProc_NamedInputPortIdType namedInputPortId =
      vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(processingNodeId)
      + (vStreamProc_NamedInputPortIdType)InputPortSymbolicName;

    InputPortInfo->IsConnected = vStreamProc_Stream_IsInputPortConnected(processingNodeId, InputPortSymbolicName);

    /* #50 Set the virtual flag. */
    InputPortInfo->IsVirtual = vStreamProc_Stream_IsInputPortVirtual(namedInputPortId);

   retVal = VSTREAMPROC_OK;
  }
  /* #60 Otherwise: */
  else
  {
    /* #70 Mark the port as unconnected and non-virtual as default. */
    InputPortInfo->IsConnected = FALSE;
    InputPortInfo->IsVirtual = FALSE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetInputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetInputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_InputPortInfoPtrType InputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set data type in info structure. */
  InputPortInfo->ReadRequest.StorageInfo.DataTypeInfo.Id = DataTypeId;

  /* #20 Issue getter request to passed port. */
  (void)vStreamProc_Stream_GetInputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    InputPortInfo->SymbolicPortName,
    &inputPortHandle);

  retVal =
    vStreamProc_Stream_GetInputPortInfo(
      &inputPortHandle,
      &InputPortInfo->ReadRequest,
      &InputPortInfo->MetaData,
      &InputPortInfo->StreamPositionInfo);

  ProcNodeInfo->InputPortResults[InputPortInfo->SymbolicPortName] = retVal;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestInputPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestInputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_InputPortInfoPtrType InputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set data type and request length in info structure. */
  InputPortInfo->ReadRequest.StorageInfo.DataTypeInfo.Id  = DataTypeId;
  InputPortInfo->ReadRequest.StorageInfo.RequestLength    = RequestLength;

  /* #20 Issue request to passed port. */
  (void)vStreamProc_Stream_GetInputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    InputPortInfo->SymbolicPortName,
    &inputPortHandle);

  retVal =
    vStreamProc_Stream_RequestInputPortData(
      &inputPortHandle,
      &InputPortInfo->ReadRequest,
      &InputPortInfo->MetaData,
      &InputPortInfo->StreamPositionInfo);

  ProcNodeInfo->InputPortResults[InputPortInfo->SymbolicPortName] = retVal;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeInputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeInputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ConsumedLength,
  boolean ReleaseFlag,
  vStreamProc_InputPortInfoPtrType InputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set request length and release flag in info structure. */
  InputPortInfo->ReadRequest.StorageInfo.RequestLength  = ConsumedLength;
  InputPortInfo->ReadRequest.StorageInfo.ReleaseFlag    = ReleaseFlag;

  /* #20 Issue acknowledge request to passed port. */
  (void)vStreamProc_Stream_GetInputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    InputPortInfo->SymbolicPortName,
    &inputPortHandle);

  retVal =
    vStreamProc_Stream_AcknowledgeInputPort(
      &inputPortHandle,
      &InputPortInfo->ReadRequest,
      &InputPortInfo->MetaData,
      &InputPortInfo->StreamPositionInfo);

  vStreamProc_ProcessingNode_SetResultBySeverity(
    retVal,
    &ProcNodeInfo->InputPortResults[InputPortInfo->SymbolicPortName]);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareOutputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ProcessingNodeIdType processingNodeId = ProcNodeInfo->ProcessingNodeId;
  vStreamProc_NamedOutputPortIdType namedOutputPortCount =
    vStreamProc_GetNamedOutputPortLengthOfProcessingNode(processingNodeId);
  vStreamProc_WriteRequestPtrType writeRequest = &OutputPortInfo->WriteRequest;
  vStreamProc_ReadRequestPtrType metaData = &OutputPortInfo->MetaData;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set symbolic name in info structure. */
  OutputPortInfo->SymbolicPortName = OutputPortSymbolicName;

  /* #20 Initialize write request with default values. */
  writeRequest->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&writeRequest->StorageInfo);

  /* #30 Initialize MetaData with default values. */
  metaData->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&metaData->StorageInfo);

  /* #40 Check if the provided symbolic name is valid. */
  if (OutputPortSymbolicName < namedOutputPortCount)
  {
    vStreamProc_NamedOutputPortIdType namedOutputPortId =
      vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(processingNodeId)
      + (vStreamProc_NamedOutputPortIdType)OutputPortSymbolicName;

    /* #50 Check if the output port is connected. */
    OutputPortInfo->IsConnected = vStreamProc_Stream_IsOutputPortConnected(processingNodeId, OutputPortSymbolicName);

    /* #60 Check if the output port is virtual. */
    OutputPortInfo->IsVirtual = vStreamProc_Stream_IsOutputPortVirtual(namedOutputPortId);

    retVal = VSTREAMPROC_OK;
  }
  /* #70 Otherwise: */
  else
  {
    /* #80 Mark the port as unconnected and non-virtual as default. */
    OutputPortInfo->IsConnected = FALSE;
    OutputPortInfo->IsVirtual = FALSE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetOutputPortInfo(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set data type in info structure. */
  OutputPortInfo->WriteRequest.StorageInfo.DataTypeInfo.Id = DataTypeId;

  /* #20 Issue getter request to passed port. */
  (void)vStreamProc_Stream_GetOutputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    OutputPortInfo->SymbolicPortName,
    &outputPortHandle);

  retVal =
    vStreamProc_Stream_GetOutputPortInfo(
      &outputPortHandle,
      &OutputPortInfo->WriteRequest,
      &OutputPortInfo->MetaData,
      &OutputPortInfo->StreamPositionInfo);

  ProcNodeInfo->OutputPortResults[OutputPortInfo->SymbolicPortName] = retVal;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestOutputPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestOutputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_DataTypeIdType DataTypeId,
  vStreamProc_LengthType RequestLength,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set data type and request length in info structure. */
  OutputPortInfo->WriteRequest.StorageInfo.DataTypeInfo.Id  = DataTypeId;
  OutputPortInfo->WriteRequest.StorageInfo.RequestLength    = RequestLength;

  /* #20 Issue request to passed port. */
  (void)vStreamProc_Stream_GetOutputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    OutputPortInfo->SymbolicPortName,
    &outputPortHandle);

  retVal =
    vStreamProc_Stream_RequestOutputPortData(
      &outputPortHandle,
      &OutputPortInfo->WriteRequest,
      &OutputPortInfo->MetaData,
      &OutputPortInfo->StreamPositionInfo);

  ProcNodeInfo->OutputPortResults[OutputPortInfo->SymbolicPortName] = retVal;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgeOutputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgeOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProducedLength,
  boolean ReleaseFlag,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set request length and release flag in info structure. */
  OutputPortInfo->WriteRequest.StorageInfo.RequestLength  = ProducedLength;
  OutputPortInfo->WriteRequest.StorageInfo.ReleaseFlag    = ReleaseFlag;

  /* #20 Issue acknowledge request to passed port. */
  (void)vStreamProc_Stream_GetOutputPortHandle(
    ProcNodeInfo->ProcessingNodeId,
    OutputPortInfo->SymbolicPortName,
    &outputPortHandle);

  retVal = vStreamProc_Stream_AcknowledgeOutputPort(
    &outputPortHandle,
    &OutputPortInfo->WriteRequest,
    &OutputPortInfo->MetaData,
    &OutputPortInfo->StreamPositionInfo);

  vStreamProc_ProcessingNode_SetResultBySeverity(
    retVal,
    &ProcNodeInfo->OutputPortResults[OutputPortInfo->SymbolicPortName]);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PreparePortInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PreparePortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;                                                                       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ReturnType opRetVal;
  uint32_least inputPortInfoIdx;
  uint32_least outputPortInfoIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed port structures and apply prepare operation. */
  for (inputPortInfoIdx = 0u; inputPortInfoIdx < InputPortCount; inputPortInfoIdx++)
  {
    opRetVal = vStreamProc_PrepareInputPortInfo(
      ProcNodeInfo,
      InputPortInfos[inputPortInfoIdx].SymbolicPortName,
      &InputPortInfos[inputPortInfoIdx]);

    /* Results with higher severity overwrite existing value. */
    if (opRetVal > retVal)
    {
      retVal = opRetVal;
    }
  }
  if (retVal == VSTREAMPROC_OK)
  {
    for (outputPortInfoIdx = 0u; outputPortInfoIdx < OutputPortCount; outputPortInfoIdx++)
    {
      opRetVal = vStreamProc_PrepareOutputPortInfo(
        ProcNodeInfo,
        OutputPortInfos[outputPortInfoIdx].SymbolicPortName,
        &OutputPortInfos[outputPortInfoIdx]);

      /* Results with higher severity overwrite existing value. */
      if (opRetVal > retVal)
      {
        retVal = opRetVal;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_PrepareAllPortInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_PrepareAllPortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_FAILED;                                                            /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_InputPortIdType   inputPortIndex;
  vStreamProc_OutputPortIdType  outputPortIndex;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize symbolic names of passed port structures to be equal to the zero-based index. */
  for (inputPortIndex = 0u; inputPortIndex < InputPortCount; inputPortIndex++)
  {
    InputPortInfos[inputPortIndex].SymbolicPortName = inputPortIndex;
  }

  for (outputPortIndex = 0u; outputPortIndex < OutputPortCount; outputPortIndex++)
  {
    OutputPortInfos[outputPortIndex].SymbolicPortName = outputPortIndex;
  }

  /* #20 Prepare the port infos.*/
  retVal = vStreamProc_PreparePortInfos(ProcNodeInfo, InputPortInfos, InputPortCount, OutputPortInfos, OutputPortCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetPortInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_GetPortInfos(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed port infos and apply getter operation. */
  retVal = vStreamProc_ProcessingNode_IteratePortInfos(
    vStreamProc_ProcessingNode_InitPortIteratorParam(ProcNodeInfo, VSTREAMPROC_PORT_OPERATION_GET_INFO, FALSE),
    InputPortInfos, InputPortCount, OutputPortInfos, OutputPortCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_RequestPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_RequestPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed port infos and apply request operation. */
  retVal = vStreamProc_ProcessingNode_IteratePortInfos(
    vStreamProc_ProcessingNode_InitPortIteratorParam(ProcNodeInfo, VSTREAMPROC_PORT_OPERATION_REQUEST_DATA, FALSE),
    InputPortInfos, InputPortCount, OutputPortInfos, OutputPortCount);

  /* #20 If operation failed, iterate over all port point infos and apply release operation. */
  if (retVal != VSTREAMPROC_OK)
  {
    (void)vStreamProc_ProcessingNode_ReleasePorts(
      ProcNodeInfo,
      InputPortInfos, InputPortCount, OutputPortInfos, OutputPortCount);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgePorts()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_AcknowledgePorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfos,
  vStreamProc_InputPortIdType InputPortCount,
  vStreamProc_OutputPortInfoPtrType OutputPortInfos,
  vStreamProc_OutputPortIdType OutputPortCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed port infos and apply acknowledge operation. */
  retVal = vStreamProc_ProcessingNode_IteratePortInfos(
    vStreamProc_ProcessingNode_InitPortIteratorParam(ProcNodeInfo, VSTREAMPROC_PORT_OPERATION_ACKNOWLEDGE, FALSE),
    InputPortInfos, InputPortCount, OutputPortInfos, OutputPortCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_AcknowledgePorts()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ReleaseAllPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal      = VSTREAMPROC_FAILED;                                                 /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ProcessingNodeIdType    processingNodeId  = ProcNodeInfo->ProcessingNodeId;
  vStreamProc_InputPortInfoType       inputPortInfos[vStreamProcConf_MaxInputPortCount];
  vStreamProc_OutputPortInfoType      outputPortInfos[vStreamProcConf_MaxOutputPortCount];
  vStreamProc_InputPortIdType         inputPortCount  =
    (vStreamProc_InputPortIdType)vStreamProc_GetNamedInputPortLengthOfProcessingNode(processingNodeId);
  vStreamProc_OutputPortIdType        outputPortCount =
    (vStreamProc_OutputPortIdType)vStreamProc_GetNamedOutputPortLengthOfProcessingNode(processingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare all port infos, e.g. initialize symbolic names and port handles.  */
  retVal = vStreamProc_PrepareAllPortInfos(ProcNodeInfo, inputPortInfos, inputPortCount, outputPortInfos, outputPortCount);

  /* #20 Iterate over all passed port infos and apply release operation. */
  if (retVal == VSTREAMPROC_OK)
  {
    retVal = vStreamProc_ProcessingNode_ReleasePorts(ProcNodeInfo, inputPortInfos, inputPortCount, outputPortInfos, outputPortCount);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SetNodeSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetNodeSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the state of the node signal handler */
  retVal = vStreamProc_Scheduler_SetNodeSignalHandlerState(
    ProcessingNodeId,
    SignalId,
    NewSignalHandlerState);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SetInputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetInputPortSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_NamedInputPortIdType namedInputPortId =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedInputPortIdType)InputPortSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the state of the input port signal handler */
  retVal = vStreamProc_Scheduler_SetInputPortSignalHandlerState(
    namedInputPortId,
    SignalId,
    NewSignalHandlerState);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SetOutputPortSignalHandlerState
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC)  vStreamProc_SetOutputPortSignalHandlerState(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_NamedOutputPortIdType namedOutputPortId =
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedOutputPortIdType)OutputPortSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the state of the output port signal handler */
  retVal = vStreamProc_Scheduler_SetOutputPortSignalHandlerState(
    namedOutputPortId,
    SignalId,
    NewSignalHandlerState);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetInputPortWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_GetInputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the parameters are valid. */
  if ((ProcessingNodeId >= vStreamProc_GetSizeOfProcessingNode())
    || (InputPortSymbolicName >= vStreamProc_GetNamedInputPortLengthOfProcessingNode(ProcessingNodeId))
    || (WaveInfo == NULL_PTR))
  {
    /* Return failed if input parameters are invalid. */
  }
  else
  {
    /* #20 Set the registered wave type of the processing node. */
    /* Hint: Processing Nodes can only query their own wave type. */
    WaveInfo->WaveTypeSymbolicName =
      vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
        vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcessingNodeId));

    /* #30 Query the wave info at the input port. */
    retVal = vStreamProc_Stream_GetInputPortWaveInfo(ProcessingNodeId, InputPortSymbolicName, WaveInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_GetOutputPortWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_GetOutputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the parameters are valid. */
  if ( (ProcessingNodeId >= vStreamProc_GetSizeOfProcessingNode())
    || (OutputPortSymbolicName >= vStreamProc_GetNamedOutputPortLengthOfProcessingNode(ProcessingNodeId))
    || (WaveInfo == NULL_PTR))
  {
    /* Return failed if input parameters are invalid. */
  }
  else
  {
    WaveInfo->WaveTypeSymbolicName =
      vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
        vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcessingNodeId));

    /* #20 Query the wave info at the output port. */
    retVal = vStreamProc_Stream_GetOutputPortWaveInfo(ProcessingNodeId, OutputPortSymbolicName, WaveInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_StartOutputPortWave()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_StartOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType  retVal = VSTREAMPROC_FAILED;
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if output port is connected. */
  if (vStreamProc_Stream_GetOutputPortHandle(ProcessingNodeId, OutputPortSymbolicName, &outputPortHandle) == VSTREAMPROC_OK)
  {
    vStreamProc_WaveRoutingIdType waveRoutingId =
      vStreamProc_GetWaveRoutingIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfNamedOutputPort(
          vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId) + OutputPortSymbolicName));

    /* Processing Nodes shall only start waves if automatic propagation isn't configured. */
    if (!vStreamProc_IsAutomaticOfWaveRouting(waveRoutingId))
    {
      vStreamProc_WaveLevelType requestedWaveLevel =
        vStreamProc_Stream_GetWaveLevel(pipeId, WaveInfo->WaveTypeSymbolicName);

      /* #20 Check that the processing node starts its registered wave level. */
      if (requestedWaveLevel == outputPortHandle.WaveLevel)
      {
        vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(outputPortHandle.WaveId);

        /* #30 Check the wave state. */
        switch (waveState)
        {
          /* #40 If the wave state is (STARTING_)ENDING. */
          case(VSTREAMPROC_WAVESTATE_STARTING_ENDING):
          case(VSTREAMPROC_WAVESTATE_ENDING):
          {
            /* #50 Return INSUFFICIENT_OUTPUT to inform the user that the wave end is not yet completed. */
            retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
            break;
          }
          /* #60 If the wave state is UNINIT or IDLE. */
          case (VSTREAMPROC_WAVESTATE_UNINIT):
          case (VSTREAMPROC_WAVESTATE_IDLE):
          {
            /* #70 Proceed with the wave start. */
            retVal = vStreamProc_Stream_StartOutputPortWave(ProcessingNodeId, OutputPortSymbolicName, WaveInfo);
            break;
          }
          /* #80 Any other state: */
          default:
          {
            /* #90 Consider as error. */
            /* Nothing to do */
            break;
          }
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_EndOutputPortWave()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_EndOutputPortWave(                                                  /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType  retVal = VSTREAMPROC_FAILED;
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if output port is connected. */
  if (vStreamProc_Stream_GetOutputPortHandle(ProcessingNodeId, OutputPortSymbolicName, &outputPortHandle) == VSTREAMPROC_OK)
  {
    vStreamProc_WaveRoutingIdType waveRoutingId =
      vStreamProc_GetWaveRoutingIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfNamedOutputPort(outputPortHandle.NamedOutputPortId));

    /* #20 Check that the automatic wave routing is not configured */
    /* Hint: Processing Nodes shall only end waves if automatic propagation isn't configured. */
    if (!vStreamProc_IsAutomaticOfWaveRouting(waveRoutingId))
    {
      vStreamProc_WaveLevelType requestedWaveLevel =
        vStreamProc_Stream_GetWaveLevel(
          vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId), WaveInfo->WaveTypeSymbolicName);

      /* #30 Check that the processing node ends its registered wave level. */
      if (requestedWaveLevel == outputPortHandle.WaveLevel)
      {
        vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(outputPortHandle.WaveId);

        /* #40 Check that the wave is currently active. */
        if (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
        {
          vStreamProc_Stream_EndOutputPortWave(ProcessingNodeId, OutputPortSymbolicName, WaveInfo->WaveTypeSymbolicName);
          retVal = VSTREAMPROC_OK;
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_DiscardInputPortData()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_DiscardInputPortData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_StreamPositionType NewConsumePosition)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check the input parameters. */
  if ( (ProcessingNodeId < vStreamProc_GetSizeOfProcessingNode())
    && (InputPortSymbolicName < vStreamProc_GetNamedInputPortLengthOfProcessingNode(ProcessingNodeId)))
  {
    /* #20 Handle discard request. */
    (void)vStreamProc_Stream_GetInputPortHandle(ProcessingNodeId, InputPortSymbolicName, &inputPortHandle);

    retVal = vStreamProc_Stream_DiscardInputPortData(&inputPortHandle, NewConsumePosition);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_SignalHandler_DefaultHandler
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SignalHandler_DefaultHandler(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Return ACKNOWLEDGE unconditionally. */
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_SignalHandler_PropagateHighLevelWaveEnd
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_SignalHandler_PropagateHighLevelWaveEnd(/* PRQA S 6080 */ /* MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ProcessingNodeIdType processingNodeId  = ProcNodeInfo->ProcessingNodeId;
  vStreamProc_NamedInputPortIdType namedInputPortId =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(processingNodeId)
    + ProcNodeInfo->SignalInfo.InputPortId;
  vStreamProc_WaveRoutingIdType waveRoutingId =
    vStreamProc_GetWaveRoutingIdxOfNamedInputPort(namedInputPortId);

  /* ----- Implementation ----------------------------------------------- */
  if (waveRoutingId != VSTREAMPROC_NO_WAVEROUTINGIDXOFNAMEDINPUTPORT)
  {
    vStreamProc_NamedOutputPortIndIterType namedOutputPortIndIdx;
    vStreamProc_StreamIdType sourceStreamId;
    vStreamProc_InputPortHandleType inputPortHandle;

    /* #10 Get input port handle. */
    (void)vStreamProc_Stream_GetInputPortHandle(
      processingNodeId,
      ProcNodeInfo->SignalInfo.InputPortId,
      &inputPortHandle);

    sourceStreamId = vStreamProc_GetStreamIdxOfStreamOutputPort(inputPortHandle.StreamOutputPortId);

    for (namedOutputPortIndIdx = vStreamProc_GetNamedOutputPortIndStartIdxOfWaveRouting(waveRoutingId);
         namedOutputPortIndIdx < vStreamProc_GetNamedOutputPortIndEndIdxOfWaveRouting(waveRoutingId);
         namedOutputPortIndIdx++)
    {
      vStreamProc_OutputPortHandleType outputPortHandle;
      vStreamProc_OutputPortSymbolicNameType outputPortSymbolicName =
        vStreamProc_GetOutputPortSymbolicNameOfPortDef(
          vStreamProc_GetPortDefIdxOfNamedOutputPort(
            vStreamProc_GetNamedOutputPortInd(namedOutputPortIndIdx)));
      vStreamProc_WaveIterType sourceWaveIdx;
      vStreamProc_WaveIterType targetWaveIdx;

      /* #20 Get the output port handle */
      if (vStreamProc_Stream_GetOutputPortHandle(processingNodeId, outputPortSymbolicName, &outputPortHandle) == VSTREAMPROC_OK)
      {
        targetWaveIdx = (vStreamProc_WaveIterType)outputPortHandle.WaveId + 1u;

        /* #30 Iterate over all wave levels above the registered level depending on the source wave. */
        for (sourceWaveIdx = vStreamProc_GetWaveStartIdxOfStream(sourceStreamId) + (vStreamProc_WaveIterType)outputPortHandle.WaveLevel + 1u;
             sourceWaveIdx < vStreamProc_GetWaveEndIdxOfStream(sourceStreamId);
             sourceWaveIdx++)
        {
          if (!vStreamProc_IsWaveStateFlagSetOfWaveId(sourceWaveIdx, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)
            && vStreamProc_IsWaveStateFlagSetOfWaveId(targetWaveIdx, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
          {
            vStreamProc_WaveInfoType waveInfo;

            /* #40 End the wave for the wave level. */
            (void)vStreamProc_PrepareWaveInfo(
              vStreamProc_GetPipeIdxOfProcessingNode(processingNodeId),
              vStreamProc_GetWaveTypeDefIdxOfWave(sourceWaveIdx),
              &waveInfo);
            (void)vStreamProc_Stream_GetInputPortWaveInfo(
              processingNodeId,
              ProcNodeInfo->SignalInfo.InputPortId,
              &waveInfo);
            vStreamProc_Stream_EndOutputPortWave(
              processingNodeId,
              outputPortSymbolicName,
              waveInfo.WaveTypeSymbolicName);
          }

          targetWaveIdx++;
        }
      }
    }
  }

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Port.c
 *********************************************************************************************************************/
