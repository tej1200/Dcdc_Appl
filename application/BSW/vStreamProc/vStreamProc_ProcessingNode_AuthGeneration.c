/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_AuthGeneration.c
 *        \brief  vStreamProc Authentication Generation Processing Node Source Code File
 *      \details  Implementation of the Authentication Generation Processing Node for the vStreamProc framework
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_AUTH_GENERATION_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_AuthGeneration.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"


#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_AUTHGENERATION == STD_ON)
# include "Csm.h"
# include "Csm_Types.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_CheckState()
 *********************************************************************************************************************/
/*!
 *  \brief          This function ensures that the initialization is done and should be called in all signal handlers.
 *  \details        -
 *  \param[in]      ProcNodeInfo          The processing node information to operate on.
 *  \return         VSTREAMPROC_OK        Check or initialization was done successfully.
 *  \return         VSTREAMPROC_FAILED    Check or initialization failed.
 *
 *  \pre            vStreamProc_ProcessingNode_AuthGeneration_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_CheckState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_QueryPorts()
 *********************************************************************************************************************/
/*!
 *  \brief          Queries the input port and the progress output port (if connected) and calculates the processing
 *                  length.
 *  \details        If the progress output port is connected and has less buffer available than input data,
 *                  the processing length is limited.
 *  \param[in]      ProcNodeInfo            The processing node information to operate on.
 *  \param[in,out]  DataInputPortInfo       Pointer to the data input port info.
 *  \param[in,out]  ProgressOutputPortInfo  Pointer to the progress output port info.
 *  \param[out]     ProcessingLength        Pointer to the processing length.
 *  \param[out]     InputLimited            Pointer to the boolean value which describes if the input was limited.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType DataInputPortInfo,
  vStreamProc_OutputPortInfoPtrType ProgressOutputPortInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputLimited);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_FinishProcessing()
 *********************************************************************************************************************/
/*!
 *  \brief          Acknowledges the processed data and updates the signal handler states for the next cycle.
 *  \details        If the input data was limited, the node will wait for the StoragaAvailable at the progress output
 *                  port. Otherwise, DataAvailable at the data input port is the next awaited signal.
 *  \param[in]      ProcNodeInfo            The processing node information to operate on.
 *  \param[in]      DataInputPortInfo       Pointer to the data input port info.
 *  \param[in]      ProgressOutputPortInfo  Pointer to the progress output port info.
 *  \param[in]      ProcessingLength        The processing length.
 *  \param[in]      InputLimited            The boolean value which describes if the input was limited.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_FinishProcessing(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType DataInputPortInfo,
  vStreamProc_OutputPortInfoPtrType ProgressOutputPortInfo,
  vStreamProc_LengthType ProcessingLength,
  boolean InputLimited);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_CheckState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_CheckState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Check if internal state is initialized and perform initialization if not. */
  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    Std_ReturnType csmRetVal;

    retVal = VSTREAMPROC_FAILED;

    /* Initialize the processing node's CSM verification. */
    if (specializedCfgPtr->AuthGenerationModeOfProcessingNode_AuthGeneration_Config == VSTREAMPROC_MAC_AUTHGENERATIONTYPEOFPROCESSINGNODE_AUTHGENERATION_CONFIG)
    {
      csmRetVal =
        Csm_MacGenerate(
          specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
          CRYPTO_OPERATIONMODE_START,
          NULL_PTR, 0u,
          NULL_PTR, NULL_PTR);
    }
    else
    {
      /* Not supported yet. */
      csmRetVal = E_NOT_OK;
    }

    /* In case everything went fine set next state. */
    if (csmRetVal == E_OK)
    {
      workspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
      retVal = VSTREAMPROC_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_QueryPorts()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_QueryPorts(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType DataInputPortInfo,
  vStreamProc_OutputPortInfoPtrType ProgressOutputPortInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength,
  P2VAR(boolean, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputLimited)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_StorageInfoPtrType dataInputStorageInfo = &DataInputPortInfo->ReadRequest.StorageInfo;
  vStreamProc_StorageInfoConstPtrType progressStorageInfo = &ProgressOutputPortInfo->WriteRequest.StorageInfo;
  vStreamProc_LengthType processingLength = 0u;
  boolean inputLimited = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the data types. */
  DataInputPortInfo->ReadRequest.StorageInfo.DataTypeInfo.Id = vStreamProcConf_vStreamProcDataType_uint8;
  ProgressOutputPortInfo->WriteRequest.StorageInfo.DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;

  /* #20 If the progress port is connected, query the data input and the progress output port. */
  if (ProgressOutputPortInfo->IsConnected == TRUE)
  {
    dataInputStorageInfo->RequestLength = 1u;

    retVal = vStreamProc_RequestPortData(ProcNodeInfo, DataInputPortInfo, 1u, ProgressOutputPortInfo, 1u);

    /* #30 Limit the processingLength if the progress output length is limited. */
    if (retVal == VSTREAMPROC_OK)
    {
      processingLength = dataInputStorageInfo->AvailableLength;

      if (processingLength > progressStorageInfo->AvailableLength)
      {
        processingLength = progressStorageInfo->AvailableLength;
        inputLimited = TRUE;
      }
    }
  }
  /* #40 Otherwise, query only the data input port. */
  else
  {
    retVal = vStreamProc_RequestInputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      1u,
      DataInputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      processingLength = dataInputStorageInfo->AvailableLength;
    }
  }

  /* #50 Write the processing length and the limit flag. */
  *ProcessingLength = processingLength;
  *InputLimited = inputLimited;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_FinishProcessing()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_FinishProcessing(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType DataInputPortInfo,
  vStreamProc_OutputPortInfoPtrType ProgressOutputPortInfo,
  vStreamProc_LengthType ProcessingLength,
  boolean InputLimited)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Acknowledge the ports. */
  retVal = vStreamProc_AcknowledgeInputPort(ProcNodeInfo, ProcessingLength, TRUE, DataInputPortInfo);

  if ((retVal == VSTREAMPROC_OK) && (ProgressOutputPortInfo->IsConnected == TRUE))
  {
    retVal = vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, ProcessingLength, TRUE, ProgressOutputPortInfo);
  }

  /* #20 Update the signal handler states. */
  if (retVal == VSTREAMPROC_OK)
  {
    if (InputLimited == TRUE)
    {
      /* #30 Enable the StorageAvailable signal handler of the progress output port. */
      (void)vStreamProc_SetOutputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_AuthGeneration_Progress,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

      /* #40 Disable the DataAvailable signal handler of the input port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcInputPort_vStreamProc_AuthGeneration_InputData,
        vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);
    }
    else
    {
      /* #50 Disable the StorageAvailable signal handler of the progress output port. */
      (void)vStreamProc_SetOutputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_AuthGeneration_Progress,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

      /* #60 Enable the DataAvailable signal handler of the input port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcInputPort_vStreamProc_AuthGeneration_InputData,
        vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  VAR(Std_ReturnType, AUTOMATIC) retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's workspace. */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize state value of workspace in case of WaveStart. */
  workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_ProcessInputData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_ProcessInputData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_LengthType processingLength = 0u;
  boolean inputLimited = FALSE;
  vStreamProc_ReturnType localRetVal;
  vStreamProc_InputPortInfoType dataInputPortInfo;
  vStreamProc_OutputPortInfoType progressOutputPortInfo;
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Prepare the port infos. */
  (void)vStreamProc_PrepareInputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_AuthGeneration_InputData,
    &dataInputPortInfo);

  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_AuthGeneration_Progress,
    &progressOutputPortInfo);

  /* #20 Ensure that CSM is initialized. */
  localRetVal = vStreamProc_ProcessingNode_AuthGeneration_CheckState(ProcNodeInfo);

  if (localRetVal == VSTREAMPROC_OK)
  {
    /* #30 Read port data. */
    localRetVal = vStreamProc_ProcessingNode_AuthGeneration_QueryPorts(
      ProcNodeInfo,
      &dataInputPortInfo,
      &progressOutputPortInfo,
      &processingLength,
      &inputLimited);
  }

  /* #40 Update the mac with the available data. */
  if (localRetVal == VSTREAMPROC_OK)
  {
    Std_ReturnType csmResult = E_NOT_OK;

    if (specializedCfgPtr->AuthGenerationModeOfProcessingNode_AuthGeneration_Config == VSTREAMPROC_MAC_AUTHGENERATIONTYPEOFPROCESSINGNODE_AUTHGENERATION_CONFIG)
    {

      csmResult = Csm_MacGenerate(
        specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
        CRYPTO_OPERATIONMODE_UPDATE,
        dataInputPortInfo.ReadRequest.Buffer,
        processingLength,
        NULL_PTR, NULL_PTR);
    }

    if (csmResult != E_OK)
    {
      localRetVal = VSTREAMPROC_FAILED;
    }
  }

  if (localRetVal == VSTREAMPROC_OK)
  {
    localRetVal = vStreamProc_ProcessingNode_AuthGeneration_FinishProcessing(
      ProcNodeInfo,
      &dataInputPortInfo,
      &progressOutputPortInfo,
      processingLength,
      inputLimited);
  }

  if (localRetVal != VSTREAMPROC_FAILED)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_WriteAuthenticationTag
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_WriteAuthenticationTag(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  vStreamProc_OutputPortInfoType authenticationTagOutputPortInfo;
  vStreamProc_ReturnType localRetVal;
  Std_ReturnType csmResult = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Request the output port buffer for the authentication tag. */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_AuthGeneration_AuthenticationTag,
    &authenticationTagOutputPortInfo);

  localRetVal = vStreamProc_RequestOutputPortData(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcDataType_uint8,
    1u,
    &authenticationTagOutputPortInfo);

  if (localRetVal == VSTREAMPROC_OK)
  {
    /* #20 Ensure that CSM is initialized. */
    localRetVal = vStreamProc_ProcessingNode_AuthGeneration_CheckState(ProcNodeInfo);
  }

  /* #30 Finalize CSM Operation. */
  if (localRetVal == VSTREAMPROC_OK)
  {
    localRetVal = VSTREAMPROC_FAILED;

    specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

    /* #40 Prepare the request length with the available length. */
    authenticationTagOutputPortInfo.WriteRequest.StorageInfo.RequestLength =
      authenticationTagOutputPortInfo.WriteRequest.StorageInfo.AvailableLength;

    if (specializedCfgPtr->AuthGenerationModeOfProcessingNode_AuthGeneration_Config == VSTREAMPROC_MAC_AUTHGENERATIONTYPEOFPROCESSINGNODE_AUTHGENERATION_CONFIG)
    {
      /* #50 Finish the mac generation. */
      csmResult =
        Csm_MacGenerate(
          specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
          CRYPTO_OPERATIONMODE_FINISH,
          NULL_PTR, 0u,
          authenticationTagOutputPortInfo.WriteRequest.Buffer,
          &authenticationTagOutputPortInfo.WriteRequest.StorageInfo.RequestLength);
    }
    else
    {
      /* Not yet supported. */
    }

    /* #60 Reset the workspace to initialized. */
    if (csmResult == E_OK)
    {
      workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
      workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;
      localRetVal = VSTREAMPROC_OK;
    }
  }

  if (localRetVal == VSTREAMPROC_OK)
  {
    /* Write the authentication tag. */
    localRetVal = vStreamProc_AcknowledgeOutputPort(
      ProcNodeInfo,
      authenticationTagOutputPortInfo.WriteRequest.StorageInfo.RequestLength,
      TRUE,
      &authenticationTagOutputPortInfo);
  }
  else
  {
    /* Release ports, signalResponse shall be still set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
    (void)vStreamProc_ReleaseAllPorts(ProcNodeInfo);
  }

  /* #70 Set signal response if no internal error occurred. */
  if (localRetVal != VSTREAMPROC_FAILED)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#if  (VSTREAMPROC_PROCESSINGNODETYPE_AUTHGENERATION_STORE_CSM_CONTEXT == STD_ON)
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Hint: Actual size of context is written by CSM. */
  workspace->SizeOfStoredCsmContext = specializedCfgPtr->SizeOfContextOfProcessingNode_AuthGeneration_Config;

  /* #10 Check if there is a context buffer. */
  if (specializedCfgPtr->SizeOfContextOfProcessingNode_AuthGeneration_Config > 0u)
  {
    /* #20 Check if the node is in PROCESSING state. */
    if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
    {
      Std_ReturnType csmRetVal;

      /* #30 Store the CSM context in the workspace via the CSM. */
      csmRetVal = Csm_SaveContextJob(
        specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
        workspace->CsmContextBuffer,
        &workspace->SizeOfStoredCsmContext);

      switch (csmRetVal)
      {
        /* #40 If the storing was successful, confirm the signal. */
        case E_OK:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
          break;
        }
        /* #50 If the CSM is busy, return pending to try again in the next cycle. */
        case CRYPTO_E_BUSY:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
          break;
        }
        /* #60 If any other return value: Consider as error. */
        default:
        {
          /* Either E_NOT_OK or an unexpected return value was received. */
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
          break;
        }
      }
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
#endif

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Cancel job. */
    (void)Csm_CancelJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
      CRYPTO_OPERATIONMODE_FINISH);

    /* #30 Reset state. */
    workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;

    /* #40 Set signal response to confirm */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_AuthGeneration_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
#if  (VSTREAMPROC_PROCESSINGNODETYPE_AUTHGENERATION_STORE_CSM_CONTEXT == STD_ON)
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_AuthGeneration_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_AuthGeneration_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  Std_ReturnType csmRetVal;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_AuthGeneration_Config(ProcNodeInfo);                   /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_AuthGeneration_WorkspaceType(ProcNodeInfo);                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Restore the context. */
    csmRetVal = Csm_RestoreContextJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_AuthGeneration_Config,
      workspace->CsmContextBuffer,
      workspace->SizeOfStoredCsmContext);

    switch (csmRetVal)
    {
      /* #30 If the storing was successful, confirm the signal. */
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      /* #40 If the CSM is busy, return pending to try again in the next cycle. */
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      /* #50 If any other return value: Consider as error. */
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received. Leave signal response as failed. */
        break;
      }
    }
  }
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }
  return signalResponse;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#endif
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_AUTHGENERATION == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_AuthGeneration.c
 *********************************************************************************************************************/
