/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_CsmVerification.c
 *        \brief  vStreamProc CSM Verification Sub Module Source Code File
 *      \details  Implementation of the CSM verification sub module for the vStreamProc framework
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_CSM_VERIFICATION_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_CsmVerification.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_CSMVERIFICATION_CONFIG == STD_ON)
# include "Csm.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# define VSTREAMPROC_CSM_VERIFICATION_RESULT_LENGTH 1u

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_GetInputPortData()
 *********************************************************************************************************************/
/*!
 *  \brief          Helper function for collecting input data.
 *  \details        -
 *  \param[in]      ProcNodeInfo  The processing node information to operate on.
 *  \param[in,out]  InputPortInfo Input port information to operate on.
 *  \return         E_OK          Check or initialization was done successfully.
 *  \return         E_NOT_OK      Check or initialization failed.
 *
 *  \pre            vStreamProc_ProcessingNode_CsmVerification_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_GetInputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if the processing length is limited by the progress output port.
 *  \details        -
 *  \param[in]      ProcNodeInfo        The processing node information to operate on.
 *  \param[in,out]  ProcessingLength    IN:  The available input length.
 *                                      OUT: The resulting processing length.
 *  \return         VSTREAMPROC_OK      No internal errors occurred.
 *  \return         VSTREAMPROC_FAILED  Internal error occurred.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_GetComparePortData()
 *********************************************************************************************************************/
/*!
 *  \brief          Helper function for collecting port information needed for compare action.
 *  \details        -
 *  \param[in]      ProcNodeInfo   The processing node information to operate on.
 *  \param[in,out]  InputPortInfo  Input port information to operate on.
 *  \param[in,out]  OutputPortInfo Output port information to operate on.
 *  \return         E_OK           Check or initialization was done successfully.
 *  \return         E_NOT_OK       Check or initialization failed.
 *
 *  \pre            vStreamProc_ProcessingNode_CsmVerification_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_GetComparePortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfo,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_ReportVerifyResult()
 *********************************************************************************************************************/
/*!
 *  \brief          Helper function for reporting verify result.
 *  \details        -
 *  \param[in]      ProcNodeInfo          The processing node information to operate on.
 *  \param[in,out]  CsmResultPortInfo     Output port information to operate on.
 *  \param[in]      CsmResultValue        Result to report.
 *  \param[in,out]  CsmReferencePortInfo  Input port information to operate on.
 *  \param[in]      AvailableLength       Length of data.
 *  \return         VSTREAMPROC_OK        Check or initialization was done successfully.
 *  \return         VSTREAMPROC_FAILED    Check or initialization failed.
 *
 *  \pre            vStreamProc_ProcessingNode_CsmVerification_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_ReportVerifyResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortInfoPtrType CsmResultPortInfo,
  Crypto_VerifyResultType CsmResultValue,
  vStreamProc_InputPortInfoPtrType CsmReferencePortInfo,
  vStreamProc_LengthType AvailableLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_CheckState()
 *********************************************************************************************************************/
/*!
 *  \brief          This function ensures that the initialization is done and should be called in all signal handlers.
 *  \details        -
 *  \param[in]      ProcNodeInfo  The processing node information to operate on.
 *  \return         E_OK          Check or initialization was done successfully.
 *  \return         E_NOT_OK      Check or initialization failed.
 *
 *  \pre            vStreamProc_ProcessingNode_CsmVerification_Init() must be called before.
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-211279
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_CheckState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 *  \brief          Updates the progress output port with the processed data length if connected.
 *  \details        -
 *  \param[in]      ProcNodeInfo     The processing node information to operate on.
 *  \param[in]      ProcessedLength  The processed data length.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM     The update was successful or the progress port
 *                                                         is not connected.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED      The update was not successful.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_GetInputPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_GetInputPortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfo)
{
  Std_ReturnType retVal = E_NOT_OK;

  /* #10 Query input port data. */
  if (vStreamProc_PrepareInputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_InputData, InputPortInfo) == VSTREAMPROC_OK)
  {
    /* Request at least 1 Byte input data, which shall be available since we got a data available indication. */
    if (vStreamProc_RequestInputPortData(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, 1u, InputPortInfo) == VSTREAMPROC_OK)
    {
      vStreamProc_LengthType processingLength = InputPortInfo->ReadRequest.StorageInfo.AvailableLength;

      /* #20 Check if the input has to be limited. */
      if (vStreamProc_ProcessingNode_CsmVerification_CheckForInputLimitation(ProcNodeInfo, &processingLength) == VSTREAMPROC_OK)
      {
        InputPortInfo->ReadRequest.StorageInfo.AvailableLength = processingLength;

        retVal = E_OK;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;
  vStreamProc_LengthType limitedLength = *ProcessingLength;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_CsmVerification_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    /* #20 Update the progress. */
    retVal = vStreamProc_GetOutputPortInfo(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      &progressOutputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      if (progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength < limitedLength)
      {
        limitedLength = progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength;
      }
    }

  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  *ProcessingLength = limitedLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_GetComparePortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_GetComparePortData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_InputPortInfoPtrType InputPortInfo,
  vStreamProc_OutputPortInfoPtrType OutputPortInfo)
{
  Std_ReturnType retVal = E_NOT_OK;

  /* #10 Check state to ensure that CSM init was called even when wave end is the first received signal. */
  if (vStreamProc_ProcessingNode_CsmVerification_CheckState(ProcNodeInfo) == E_OK)
  {
    vStreamProc_StorageInfoPtrType  csmReferenceStorageInfo = &InputPortInfo->ReadRequest.StorageInfo;
    vStreamProc_StorageInfoPtrType  csmResultStorageInfo = &OutputPortInfo->WriteRequest.StorageInfo;

    /* #20 Check availability of reference value and if there is enough space for result. */
    InputPortInfo->SymbolicPortName = vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_CsmReferenceData;
    OutputPortInfo->SymbolicPortName = vStreamProcConf_vStreamProcOutputPort_vStreamProc_CsmVerification_ResultData;

    if (vStreamProc_PreparePortInfos(ProcNodeInfo, InputPortInfo, 1u, OutputPortInfo, 1u) == VSTREAMPROC_OK)
    {
      csmReferenceStorageInfo->RequestLength = 1u;
      csmReferenceStorageInfo->DataTypeInfo.Id = vStreamProcConf_vStreamProcDataType_uint8;
      csmResultStorageInfo->RequestLength = VSTREAMPROC_CSM_VERIFICATION_RESULT_LENGTH;
      csmResultStorageInfo->DataTypeInfo.Id = vStreamProcConf_vStreamProcDataType_Std_ReturnType;

      if (vStreamProc_RequestPortData(ProcNodeInfo, InputPortInfo, 1u, OutputPortInfo, 1u) == VSTREAMPROC_OK)
      {
        retVal = E_OK;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_ReportVerifyResult()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_ReportVerifyResult(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortInfoPtrType CsmResultPortInfo,
  Crypto_VerifyResultType CsmResultValue,
  vStreamProc_InputPortInfoPtrType CsmReferencePortInfo,
  vStreamProc_LengthType AvailableLength)
{
  vStreamProc_ReturnType spRetVal = VSTREAMPROC_FAILED;
  P2VAR(Std_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) outputBuffer =
    vStreamProc_GetTypedWriteRequestBuffer_Std_ReturnType(&CsmResultPortInfo->WriteRequest);
  Std_ReturnType csmVerifyResult;

  /* #10 Convert CSM result to StdResult. */
  if (CsmResultValue == CRYPTO_E_VER_OK)
  {
    csmVerifyResult = E_OK;
  }
  else
  {
    csmVerifyResult = E_NOT_OK;
  }

  /* #20 Set result and acknowledge open ports. */
  outputBuffer[0u] = csmVerifyResult;

  CsmReferencePortInfo->ReadRequest.StorageInfo.RequestLength = AvailableLength;

  spRetVal = vStreamProc_AcknowledgePorts(ProcNodeInfo, CsmReferencePortInfo, 1u, CsmResultPortInfo, 1u);

  return spRetVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_CheckState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_CheckState(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;

  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;
  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);                /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Check if internal state is initialized and perform initialization if not. */
  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    /* Initialize the processing node's CSM verification. */
    if (specializedCfgPtr->CsmVerificationModeOfProcessingNode_CsmVerification_Config == VSTREAMPROC_MAC_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG)
    {
      retVal = Csm_MacVerify(specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
                             CRYPTO_OPERATIONMODE_START, NULL_PTR, 0u,
                             NULL_PTR, 0u, NULL_PTR);
    }
    else /* == VSTREAMPROC_SIGNATURE_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG */
    {
      retVal = Csm_SignatureVerify(specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
                                   CRYPTO_OPERATIONMODE_START, NULL_PTR, 0u,
                                   NULL_PTR, 0u, NULL_PTR);
    }

    /* In case everything went fine set next state. */
    if (retVal == E_OK)
    {
      workspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType retVal = VSTREAMPROC_SIGNALRESPONSE_FAILED;                                            /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_CsmVerification_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    vStreamProc_ReturnType localRetVal;

    /* #20 Update the progress. */
    localRetVal = vStreamProc_RequestOutputPortData(
        ProcNodeInfo,
        VSTREAMPROC_NO_DATATYPE,
        ProcessedLength,
        &progressOutputPortInfo);

    if (localRetVal == VSTREAMPROC_OK)
    {
      localRetVal =
        vStreamProc_AcknowledgeOutputPort(
          ProcNodeInfo,
          ProcessedLength,
          TRUE,
          &progressOutputPortInfo);

      if (localRetVal == VSTREAMPROC_OK)
      {
        retVal = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
      }
    }
  }
  /* #30 Otherwise, just confirm the signal. */
  else
  {
    retVal = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  VAR(Std_ReturnType, AUTOMATIC) retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's workspace. */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);                /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize state value of workspace. */
  workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_ProcessInputData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_ProcessInputData(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;                                    /* PRQA S 2981 */ /* MD_MSR_RetVal */
  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Ensure that handler is configured correctly. */
  if (ProcNodeInfo->SignalInfo.SignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable)
  {
    /* #20 Ensure that CSM is initialized. */
    if (vStreamProc_ProcessingNode_CsmVerification_CheckState(ProcNodeInfo) == E_OK)
    {
      Std_ReturnType                    csmResult = E_NOT_OK;
      vStreamProc_InputPortInfoType     inputPortInfo;
      vStreamProc_LengthType            processingLength = 0u;

      /* #30 Read input port data. */
      if (vStreamProc_ProcessingNode_CsmVerification_GetInputPortData(ProcNodeInfo, &inputPortInfo) == E_OK)
      {
        processingLength = inputPortInfo.ReadRequest.StorageInfo.AvailableLength;

        /* #40 Update the CSM verification calculation based on the available data. */
        if (specializedCfgPtr->CsmVerificationModeOfProcessingNode_CsmVerification_Config == VSTREAMPROC_MAC_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG)
        {
          csmResult = Csm_MacVerify(specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
                                    CRYPTO_OPERATIONMODE_UPDATE, vStreamProc_GetTypedReadRequestBuffer_uint8(&inputPortInfo.ReadRequest), processingLength,
                                    NULL_PTR, 0u, NULL_PTR);
        }
        else /* == VSTREAMPROC_SIGNATURE_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG */
        {
          csmResult = Csm_SignatureVerify(specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
                                          CRYPTO_OPERATIONMODE_UPDATE, vStreamProc_GetTypedReadRequestBuffer_uint8(&inputPortInfo.ReadRequest), processingLength,
                                          NULL_PTR, 0u, NULL_PTR);
        }
      }

      if (csmResult == E_OK)
      {
        if (vStreamProc_AcknowledgeInputPort(ProcNodeInfo, processingLength, TRUE, &inputPortInfo) == VSTREAMPROC_OK)
        {
          signalResponse = vStreamProc_ProcessingNode_CsmVerification_WriteProgressOutputPort(ProcNodeInfo, processingLength);
        }
      }
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_TriggerVerification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_TriggerVerification(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ProcessingNodeIdType processingNodeId = ProcNodeInfo->ProcessingNodeId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Unblock the signal handler for WaveEnd at the reference data port to finish the verification. */
  (void)vStreamProc_SetInputPortSignalHandlerState(
    processingNodeId,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_CsmReferenceData,
    vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
    VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

  /* #20 Block the WaveStart signal at the input data port to suppress new waves until the verification is finished. */
  (void)vStreamProc_SetInputPortSignalHandlerState(
    processingNodeId,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_InputData,
    vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart,
    VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_FinishVerification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_FinishVerification(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;                                    /* PRQA S 2981 */ /* MD_MSR_RetVal */
  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);                /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* HINT: All input data shall be consumed (must not be checked), when wave end signal is indicated. */

  /* #10 Check for correct signal. */
  if (ProcNodeInfo->SignalInfo.SignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd)
  {
    vStreamProc_InputPortInfoType   csmReferencePortInfo;
    vStreamProc_OutputPortInfoType  csmResultPortInfo;

    /* #20 Collect port information. */
    if(vStreamProc_ProcessingNode_CsmVerification_GetComparePortData(ProcNodeInfo, &csmReferencePortInfo, &csmResultPortInfo) == E_OK)
    {
      Std_ReturnType          csmResult;
      Crypto_VerifyResultType csmResultValue  = CRYPTO_E_VER_NOT_OK;
      vStreamProc_LengthType  availableLength = csmReferencePortInfo.ReadRequest.StorageInfo.AvailableLength;

      /* #30 Finalize CSM Operation. */
      if (specializedCfgPtr->CsmVerificationModeOfProcessingNode_CsmVerification_Config == VSTREAMPROC_MAC_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG)
      {
        csmResult =
          Csm_MacVerify(
            specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
            CRYPTO_OPERATIONMODE_FINISH,
            NULL_PTR,
            0u,
            vStreamProc_GetTypedReadRequestBuffer_uint8(&csmReferencePortInfo.ReadRequest),
            availableLength,
            &csmResultValue);
      }
      else /* == VSTREAMPROC_SIGNATURE_CSMVERIFICATIONTYPEOFPROCESSINGNODE_CSMVERIFICATION_CONFIG */
      {
        csmResult =
          Csm_SignatureVerify(
            specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
            CRYPTO_OPERATIONMODE_FINISH,
            NULL_PTR,
            0u,
            vStreamProc_GetTypedReadRequestBuffer_uint8(&csmReferencePortInfo.ReadRequest),
            availableLength,
            &csmResultValue);
      }

      if (csmResult == E_OK)
      {
        /* #40 Propagate the result and acknowledge ports. */
        if (vStreamProc_ProcessingNode_CsmVerification_ReportVerifyResult(ProcNodeInfo, &csmResultPortInfo, csmResultValue, &csmReferencePortInfo, availableLength) == VSTREAMPROC_OK)
        {
          vStreamProc_ProcessingNodeIdType processingNodeId = ProcNodeInfo->ProcessingNodeId;

          /* #50 Block the signal handler for WaveEnd at the reference data port for the next cycle. */
          (void)vStreamProc_SetInputPortSignalHandlerState(
            processingNodeId,
            vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_CsmReferenceData,
            vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
            VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

          /* #60 Register the WaveStart signal at the input data port for the next cycle. */
          (void)vStreamProc_SetInputPortSignalHandlerState(
            processingNodeId,
            vStreamProcConf_vStreamProcInputPort_vStreamProc_CsmVerification_InputData,
            vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart,
            VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

          signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;

          workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;
        }
      }
      else
      {
        /* Release ports, signalResponse shall be still set to VSTREAMPROC_SIGNALRESPONSE_FAILED. */
        (void)vStreamProc_ReleaseAllPorts(ProcNodeInfo);
      }
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

#if  (VSTREAMPROC_PROCESSINGNODETYPE_CSMVERIFICATION_STORE_CSM_CONTEXT == STD_ON)
  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);                /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Hint: Actual size of context is written by CSM. */
  workspace->SizeOfStoredCsmContext = specializedCfgPtr->SizeOfContextOfProcessingNode_CsmVerification_Config;

  /* #10 Check if there is a context buffer. */
  if (specializedCfgPtr->SizeOfContextOfProcessingNode_CsmVerification_Config > 0u)
  {
    /* #20 Check if the node is in PROCESSING state. */
    if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
    {
      Std_ReturnType csmRetVal;

      /* #30 Store the CSM context in the workspace via the CSM. */
      csmRetVal = Csm_SaveContextJob(
        specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
        workspace->CsmContextBuffer,
        &workspace->SizeOfStoredCsmContext);

      switch (csmRetVal)
      {
        /* #40 If the storing was successful, confirm the signal. */
        case E_OK:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
          break;
        }
        /* #50 If the CSM is busy, return pending to try again in the next cycle. */
        case CRYPTO_E_BUSY:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
          break;
        }
        /* #60 If any other return value: Consider as error. */
        default:
        {
          /* Either E_NOT_OK or an unexpected return value was received. */
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
          break;
        }
      }
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
#endif

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Cancel job. */
    (void) Csm_CancelJob(specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config, CRYPTO_OPERATIONMODE_FINISH);

    /* #30 Reset state. */
    workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;

    /* #40 Set signal response to confirm */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_CsmVerification_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_CsmVerification_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
#if  (VSTREAMPROC_PROCESSINGNODETYPE_CSMVERIFICATION_STORE_CSM_CONTEXT == STD_ON)
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_CsmVerification_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_CsmVerification_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  Std_ReturnType csmRetVal;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_CsmVerification_Config(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_CsmVerification_WorkspaceType(ProcNodeInfo);                /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Restore the context. */
    csmRetVal = Csm_RestoreContextJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_CsmVerification_Config,
      workspace->CsmContextBuffer,
      workspace->SizeOfStoredCsmContext);

    switch (csmRetVal)
    {
      /* #30 If the storing was successful, confirm the signal. */
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      /* #40 If the CSM is busy, return pending to try again in the next cycle. */
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      /* #50 If any other return value: Consider as error. */
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received. Leave signal response as failed. */
        break;
      }
    }
  }
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }
  return signalResponse;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#endif
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_CSMVERIFICATION_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_CsmVerification.c
 *********************************************************************************************************************/
