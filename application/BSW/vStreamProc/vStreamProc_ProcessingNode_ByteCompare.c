/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!       \file   vStreamProc_ProcessingNode_ByteCompare.c
 *        \brief  vStreamProc Byte Compare Processing Node Source Code File
 *
 *      \details  Implementation of the vStreamProc byte compare processing node
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_BYTE_COMPARE_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_ByteCompare.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_BYTECOMPARE_CONFIG == STD_ON)
/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/* Local compare result */
#define VSTREAMPROC_PROCESSINGNODE_BYTECOMPARE_E_NO_ERROR  255u

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef struct
{
  vStreamProc_OutputPortInfoType    OutputPortInfos[vStreamProcConf_OutputPortCount_Internal_ByteCompareNode];
  vStreamProc_InputPortInfoType     InputPortInfos[vStreamProcConf_InputPortCount_Internal_ByteCompareNode];
  vStreamProc_LengthType            InputLength[vStreamProcConf_InputPortCount_Internal_ByteCompareNode];
  vStreamProc_OutputPortInfoPtrType ProgressPortInfo;
  vStreamProc_InputPortIdType       ConnectedInputPortCount;
  vStreamProc_InputPortIdType       FirstConnectedInputPortId;
  vStreamProc_InputPortIdType       MinInputPortId;
  vStreamProc_LengthType            MinInputLength;
  vStreamProc_LengthType            MaxInputLength;
  vStreamProc_LengthType            MaxOutputLength;
  vStreamProc_LengthType            MaxCompareLength;
  vStreamProc_LengthType            CompareElementLength;
} vStreamProc_ProcessingNode_ByteCompare_InfoType;

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace()
 *********************************************************************************************************************/
/*! \brief      Resets the provided workspace to default values.
 *  \details    -
 *  \param[in]  Workspace            Pointer to the workspace.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace(
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Workspace);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates()
 *********************************************************************************************************************/
/*! \brief      Set all input signal handlers for the provided signal to the provided signal handler state.
 *  \details    -
 *  \param[in]  ProcNodeInfo            The processing node information to operate on.
 *  \param[in]  Info                    Port information workspace.
 *  \param[in]  SignalId                Id of the signal.
 *  \param[in]  NewSignalHandlerState   The new signal handler state.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation()
 *********************************************************************************************************************/
/*! \brief      Helper function, which collects all port information.
 *  \details    -
 *  \param[in]  ProcNodeInfo            The processing node information to operate on.
 *  \param[out] Info                    Port information workspace.
 *  \return     VSTREAMPROC_OK          Operation succeeded.
 *  \return     VSTREAMPROC_FAILED      Operation failed.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_EvaluatePortInfos()
 *********************************************************************************************************************/
/*!
 *  \brief          Evaluates the port data lengths and calculates the max compare length based on the provided data.
 *  \details        -
 *  \param[in,out]  Info                The processing node information to operate on.
 *  \return         VSTREAMPROC_OK      Compare finished without internal errors.
 *  \return         VSTREAMPROC_FAILED  An error was reported from the port access APIs.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_EvaluatePortInfos(
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_IteratePorts()
 *********************************************************************************************************************/
/*!
 *  \brief          Iterates over all ports, calculates the length parameters and stores them in the
 *                  provided info structure.
 *  \details        -
 *  \param[in,out]  Info                The processing node information to operate on.
 *  \return         VSTREAMPROC_OK      Compare finished without internal errors.
 *  \return         VSTREAMPROC_FAILED  An error was reported from the port access APIs.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_IteratePorts(
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 *  \brief          Updates the progress output port with the processed data length if connected.
 *  \details        -
 *  \param[in]      ProcNodeInfo     The processing node information to operate on.
 *  \param[in]      ProgressPortInfo The prepared information structure of the progress output port.
 *  \param[in]      ProcessedLength  The processed data length.
 *  \return         VSTREAMPROC_OK      The update was successful or the progress port is not connected.
 *  \return         VSTREAMPROC_FAILED  The update was not successful.
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortInfoPtrType ProgressPortInfo,
  vStreamProc_LengthType ProcessedLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Report result of byte compare.
 * \details       -
 * \param[in]     ProcNodeInfo                      The processing node information to operate on.
 * \param[in]     Result                            The result to be reported.
 * \return        VSTREAMPROC_OK                    Operation succeeded.
 * \return        VSTREAMPROC_FAILED                Operation failed.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Not enough buffer available to report result.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  Std_ReturnType Result);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_Compare()
 *********************************************************************************************************************/
/*!
 * \brief         Handles the byte compare process and writes the result to the result output port if connected.
 * \details       The signal response is updated based on the compare result and the connection state of the
 *                result output port. If the port is connected, an error during compare is only reported via the
 *                result port. Otherwise, the signal response is set to FAILED in this case.
 * \param[in]     ProcNodeInfo        The processing node information to operate on.
 * \param[in]     Info                Port information workspace.
 * \param[out]    SignalResponse      Pointer to the signal response.
 * \return        VSTREAMPROC_OK      Compare finished without internal errors.
 * \return        VSTREAMPROC_FAILED  An error was reported from the port access APIs.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_Compare(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_CompareBuffers
 *********************************************************************************************************************/
/*!
 * \brief         Compares the input buffers of the connected input ports.
 * \details       -
 * \param[in]     ProcNodeInfo        The processing node information to operate on.
 * \param[in]     Info                Port information workspace.
 * \return        VSTREAMPROC_OK      Buffers were compared and the content is equal.
 * \return        VSTREAMPROC_FAILED  A mismatch in the buffer content was detected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_CompareBuffers(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData
 *********************************************************************************************************************/
/*!
 * \brief         Consumes any leftover data at the input ports.
 * \details       This is necessary to avoid triggering the clogged pipe detection in case an error occurred.
 * \param[in]     ProcNodeInfo        The processing node information to operate on.
 * \param[in]     Info                Port information workspace.
 * \return        VSTREAMPROC_OK      All data successfully consumed.
 * \return        VSTREAMPROC_FAILED  An error was reported by the port access API.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  boolean RequestData);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdatePortResults
 *********************************************************************************************************************/
/*!
 * \brief         Updates the port results based on the provided info structure.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     Info              Port information workspace.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdatePortResults(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdateSignalHandlers
 *********************************************************************************************************************/
/*!
 * \brief         Updates the signal handlers based on the provided info structure.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     Info              Port information workspace.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdateSignalHandlers(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_StartOutputPortWaves()
 *********************************************************************************************************************/
/*!
 * \brief         Request to start an output wave.
 * \details       -
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     Info              Port information workspace.
 * \return        VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Starting request was processed or stored correctly.
 * \return        VSTREAMPROC_SIGNALRESPONSE_FAILED       Bad state detected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_StartOutputPortWaves(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_EndOutputPortWaves
 *********************************************************************************************************************/
/*!
 * \brief         Ends the current wave at all (connected) output ports.
 * \details       -
 * \param[in]     ProcNodeInfo        The processing node information to operate on.
 * \param[in]     Info                Port information workspace.
 * \return        VSTREAMPROC_OK      EndWave successful.
 * \return        VSTREAMPROC_FAILED  An error occurred.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_EndOutputPortWaves(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_CheckRemainingInputLength
 *********************************************************************************************************************/
/*!
 * \brief         Checks if there is remaining input data at the current input port.
 * \details       If there is remaining data, either an error is written to the result output port if connected,
 *                or the signal response is set to FAILED otherwise. If an error is written to the result output port,
 *                all data from all input ports is consumed to avoid triggering the clogged pipe detection.
 * \param[in]     ProcNodeInfo      The processing node information to operate on.
 * \param[in]     Info              Port information workspace.
 * \param[in]     SignalResponse    Pointer to the signal response.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_CheckRemainingInputLength(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace(
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Workspace)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_InputPortIterType inputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Init the workspace */
  Workspace->WaveStartReceivedCount = 0u;
  Workspace->WaveEndReceivedCount = 0u;
  Workspace->OpenInputWaveCount = 0u;
  Workspace->ResultProvided = FALSE;

  for (inputPortIdx = 0u; inputPortIdx < vStreamProcConf_InputPortCount_Internal_ByteCompareNode; inputPortIdx++)
  {
    Workspace->WaveStartHandlerState[inputPortIdx] = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
    Workspace->WaveEndHandlerState[inputPortIdx] = VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalHandlerStateType NewSignalHandlerState)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  vStreamProc_InputPortIterType inputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Update the signal handler states. */
  for (inputPortIdx = 0u; inputPortIdx < ProcNodeInfo->InputPortCount; inputPortIdx++)
  {
    if (Info->InputPortInfos[inputPortIdx].IsConnected == TRUE)
    {
      (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
        (vStreamProc_InputPortSymbolicNameType)inputPortIdx,
        SignalId,
        NewSignalHandlerState);

      switch (SignalId)
      {
        case vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart:
        {
          workspace->WaveStartHandlerState[inputPortIdx] = NewSignalHandlerState;
          break;
        }
        case vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd:
        {
          workspace->WaveEndHandlerState[inputPortIdx] = NewSignalHandlerState;
          break;
        }
        default: /* vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable */
        /* No action necessary. */
        {
          break;
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal = VSTREAMPROC_FAILED;
  vStreamProc_InputPortIdType         inputPortCount  = ProcNodeInfo->InputPortCount;
  vStreamProc_OutputPortIdType        outputPortCount = ProcNodeInfo->OutputPortCount;
  vStreamProc_InputPortIterType       inputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  Info->ProgressPortInfo          = &Info->OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress];
  Info->MinInputPortId            = 0u;
  Info->FirstConnectedInputPortId = 0u;
  Info->MinInputLength            = 0u;
  Info->MaxInputLength            = 0u;
  Info->MaxCompareLength          = 0u;
  Info->CompareElementLength      = 0u;

  for (inputPortIdx = 0u; inputPortIdx < inputPortCount; inputPortIdx++)
  {
    Info->InputLength[inputPortIdx] = 0u;
  }

  /* #10 Initialize port info. */
  if (vStreamProc_PrepareAllPortInfos(ProcNodeInfo, Info->InputPortInfos, inputPortCount, Info->OutputPortInfos, outputPortCount) == VSTREAMPROC_OK)
  {
    /* #20 Get input port information. */
    if (vStreamProc_GetPortInfos(ProcNodeInfo, Info->InputPortInfos, inputPortCount, Info->OutputPortInfos, outputPortCount) == VSTREAMPROC_OK)
    {
      /* #30 Evaluate the port infos. */
      retVal = vStreamProc_ProcessingNode_ByteCompare_EvaluatePortInfos(Info);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_EvaluatePortInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_EvaluatePortInfos(
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_LengthType maxOutputLength = VSTREAMPROC_MAX_LENGTH;
  vStreamProc_LengthType maxCompareLength = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Loop over all connected input ports and detect minimum and maximum value. */
  retVal = vStreamProc_ProcessingNode_ByteCompare_IteratePorts(Info);

  /* #20 Check if there are enough ports connected. */
  if (retVal == VSTREAMPROC_OK)
  {
    if (Info->ConnectedInputPortCount < 2u)
    {
      retVal = VSTREAMPROC_FAILED;
    }
  }

  /* #30 Calculate the max compare value. */
  if (retVal == VSTREAMPROC_OK)
  {
    maxCompareLength = Info->MinInputLength;

    /* #40 Check if input is limited by process port. */
    if (Info->ProgressPortInfo->IsConnected == TRUE)
    {
      maxOutputLength = Info->ProgressPortInfo->WriteRequest.StorageInfo.AvailableLength;

      if (maxOutputLength < Info->MinInputLength)
      {
        maxCompareLength = maxOutputLength;
      }
    }

    /* #50 Set max output and compare value. */
    Info->MaxOutputLength = maxOutputLength;
    Info->MaxCompareLength = maxCompareLength;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_IteratePorts()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_IteratePorts(
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_OK;
  boolean                       firstConnectedPortFound = FALSE;
  vStreamProc_DataTypeIdType    inputDataType = VSTREAMPROC_NO_DATATYPE;
  vStreamProc_InputPortInfoConstPtrType inputPortInfos = Info->InputPortInfos;
  vStreamProc_InputPortIdType   inputInfoIndex;

  /* Info data */
  vStreamProc_LengthType        minInputLength = VSTREAMPROC_MAX_LENGTH;
  vStreamProc_LengthType        maxInputLength = 0u;
  vStreamProc_InputPortIdType   minInputPortId = 0u;
  vStreamProc_InputPortIdType   connectedInputPortCount = 0u;
  vStreamProc_InputPortIdType   firstConnectedPortId = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Loop over all connected input ports and detect minimum and maximum value. */
  for (inputInfoIndex = 0u; inputInfoIndex < vStreamProcConf_InputPortCount_Internal_ByteCompareNode; inputInfoIndex++)
  {
    if (Info->InputPortInfos[inputInfoIndex].IsConnected == TRUE)
    {
      vStreamProc_LengthType currentLength = inputPortInfos[inputInfoIndex].ReadRequest.StorageInfo.AvailableLength;

      Info->InputLength[inputInfoIndex] = currentLength;

      /* #20 Check if this is the first connected port. */
      if (firstConnectedPortFound == FALSE)
      {
        inputDataType = inputPortInfos[inputInfoIndex].ReadRequest.StorageInfo.DataTypeInfo.Id;

        firstConnectedPortId = inputInfoIndex;
        firstConnectedPortFound = TRUE;

        /* #30 Check that data type is in list of known literal types. */
        if ( (inputDataType != vStreamProcConf_vStreamProcDataType_uint8)
          && (inputDataType != vStreamProcConf_vStreamProcDataType_uint16)
          && (inputDataType != vStreamProcConf_vStreamProcDataType_uint32)
          && (inputDataType != vStreamProcConf_vStreamProcDataType_Std_ReturnType))
        {
          retVal = VSTREAMPROC_FAILED;
        }
      }

      if (currentLength < minInputLength)
      {
        minInputPortId = inputInfoIndex;
        minInputLength = currentLength;
      }

      if (currentLength > maxInputLength)
      {
        maxInputLength = currentLength;
      }

      /* #40 Check for data type consistency. */
      if (inputPortInfos[inputInfoIndex].ReadRequest.StorageInfo.DataTypeInfo.Id != inputDataType)
      {
        retVal = VSTREAMPROC_FAILED;
      }
      connectedInputPortCount++;

      if (retVal == VSTREAMPROC_FAILED)
      {
        break;
      }
    }
  }

  /* #50 Set the calculated info values. */
  Info->FirstConnectedInputPortId = firstConnectedPortId;
  Info->MinInputPortId = minInputPortId;
  Info->MinInputLength = minInputLength;
  Info->MaxInputLength = maxInputLength;
  Info->ConnectedInputPortCount = connectedInputPortCount;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_OutputPortInfoPtrType ProgressPortInfo,
  vStreamProc_LengthType ProcessedLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the Progress output port is connected. */
  if (ProgressPortInfo->IsConnected == TRUE)
  {

    /* #20 Update the progress. */
    retVal = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      ProcessedLength,
      ProgressPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      retVal =
        vStreamProc_AcknowledgeOutputPort(
          ProcNodeInfo,
          ProcessedLength,
          TRUE,
          ProgressPortInfo);
    }
  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  Std_ReturnType Result)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType         retVal = VSTREAMPROC_OK;
  vStreamProc_OutputPortInfoType resultPortInfo;
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If result is available: */
  if (workspace->ResultProvided == FALSE)
  {
    /* #20 Report result of byte compare. */
    (void)vStreamProc_PrepareOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData, &resultPortInfo);

    /* Report "insufficient data" when no data is available by requesting a single entry. */
    retVal = vStreamProc_RequestOutputPortData(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_Std_ReturnType, 1u, &resultPortInfo);

    /* Write result. */
    if (retVal == VSTREAMPROC_OK)
    {
      P2VAR(Std_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_VAR) resultBuffer =
        vStreamProc_GetTypedWriteRequestBuffer_Std_ReturnType(&resultPortInfo.WriteRequest);

      resultBuffer[0u] = Result;

      retVal = vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, 1u, TRUE, &resultPortInfo);

      if (retVal == VSTREAMPROC_OK)
      {
        workspace->ResultProvided = TRUE;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_Compare
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_Compare(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_InputPortIterType inputPortIdx;
  vStreamProc_LengthType maxCompareLength = Info->MaxCompareLength;
  vStreamProc_InputPortInfoPtrType inputPortInfos = Info->InputPortInfos;
  vStreamProc_InputPortIdType inputPortCount = ProcNodeInfo->InputPortCount;
  vStreamProc_SignalResponseType signalResponse = *SignalResponse;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Request the compare length at all connected input ports. */
  for (inputPortIdx = 0u; inputPortIdx < inputPortCount; inputPortIdx++)
  {
    if (inputPortInfos[inputPortIdx].IsConnected == TRUE)
    {
      inputPortInfos[inputPortIdx].ReadRequest.StorageInfo.RequestLength = maxCompareLength;
    }
  }

  /* #20 Request input buffers and compare the content. */
  retVal = vStreamProc_RequestPortData(
    ProcNodeInfo,
    inputPortInfos,
    inputPortCount,
    NULL_PTR,
    0u);

  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);

    /* #30 Compare the buffers and check if there is a mismatch. */
    if (vStreamProc_ProcessingNode_ByteCompare_CompareBuffers(ProcNodeInfo, Info) == VSTREAMPROC_FAILED)
    {
      vStreamProc_OutputPortInfoConstPtrType resultOutputPortInfo =
        &Info->OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData];

      /* #40 Check if the result output port is connected. */
      if (resultOutputPortInfo->IsConnected == TRUE)
      {
        /* #50 Write the error to the result output port. */
        retVal = vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort(
          ProcNodeInfo,
          VSTREAMPROC_PROCESSINGNODE_BYTECOMPARE_E_UNEQUAL);

        if (retVal == VSTREAMPROC_OK)
        {
          /* #60 Consume all data */
          retVal = vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData(ProcNodeInfo, Info, FALSE);
        }
      }
      /* #70 Otherwise, report the error via the signal response. */
      else
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
      }
    }
    /* #80 Otherwise: No differences detected, acknowledge the compared input data. */
    else
    {
      retVal = vStreamProc_AcknowledgePorts(ProcNodeInfo, inputPortInfos, inputPortCount, NULL_PTR, 0u);
    }
  }
  else
  {
    (void)vStreamProc_ReleaseAllPorts(ProcNodeInfo);
  }

  /* #90 Overwrite the signal response if any internal error occurred. */
  if (retVal != VSTREAMPROC_OK)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  }

  *SignalResponse = signalResponse;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_CompareBuffers
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_CompareBuffers( /* PRQA S 6080 */ /* MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_InputPortIterType inputPortIdx;
  vStreamProc_LengthType maxCompareLength = Info->MaxCompareLength;
  vStreamProc_LengthType compareElementLength = maxCompareLength;
  vStreamProc_InputPortInfoConstPtrType inputPortInfos = Info->InputPortInfos;
  vStreamProc_InputPortIdType inputPortCount = ProcNodeInfo->InputPortCount;

  /* ----- Implementation ----------------------------------------------- */
  vStreamProc_InputPortIdType firstConnectedInputPortId = Info->FirstConnectedInputPortId;
  vStreamProc_LengthType dataTypeSize =
    inputPortInfos[firstConnectedInputPortId].ReadRequest.StorageInfo.DataTypeInfo.Size;
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) referenceBuffer =
    inputPortInfos[firstConnectedInputPortId].ReadRequest.Buffer;
  vStreamProc_LengthType byteLength = maxCompareLength * dataTypeSize;

  /* #10 Compare all connected input ports to the first connected port. */
  for (inputPortIdx = 0u; inputPortIdx < inputPortCount; inputPortIdx++)
  {
    vStreamProc_StorageNodeBufferIterType bufferIdx;

    /* #20 Check if port is connected. */
    if (inputPortInfos[inputPortIdx].IsConnected == TRUE)
    {
      /* #30 Compare the buffer against the buffer of the first connected input port. */
      if (inputPortIdx != firstConnectedInputPortId)
      {
        P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) buffer =
          inputPortInfos[inputPortIdx].ReadRequest.Buffer;

        for (bufferIdx = 0u; bufferIdx < byteLength; bufferIdx++)
        {
          /* #40 Check for each byte if the buffer content is different. */
          if (referenceBuffer[bufferIdx] != buffer[bufferIdx])
          {
            /* #50 Break any operation when mismatch was detected */
            retVal = VSTREAMPROC_FAILED;

            /* #60 Update the actual processed length */
            compareElementLength = (byteLength / dataTypeSize);
            break;
          }
        }

        /* #70 Leave the loop if an error occurred. */
        if (retVal == VSTREAMPROC_FAILED)
        {
          break;
        }
      }
    }
  }

  Info->CompareElementLength = compareElementLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  boolean RequestData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;
  vStreamProc_InputPortIterType inputPortIdx;
  vStreamProc_InputPortInfoPtrType inputPortInfos = Info->InputPortInfos;
  vStreamProc_InputPortIdType inputPortCount = ProcNodeInfo->InputPortCount;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the all input port data. */
  if (RequestData == TRUE)
  {
    retVal = vStreamProc_RequestPortData(ProcNodeInfo, inputPortInfos, inputPortCount, NULL_PTR, 0u);
  }

  /* #20 Acknowledge all input ports with data. */
  for (inputPortIdx = 0u; inputPortIdx < inputPortCount; inputPortIdx++)
  {
    vStreamProc_LengthType availableLength = inputPortInfos[inputPortIdx].ReadRequest.StorageInfo.AvailableLength;

    if (availableLength > 0u)
    {
      retVal = vStreamProc_AcknowledgeInputPort(ProcNodeInfo, availableLength, TRUE, &inputPortInfos[inputPortIdx]);

      if (retVal != VSTREAMPROC_OK)
      {
        break;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdatePortResults
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdatePortResults(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_InputPortIdType inputPortCount = ProcNodeInfo->InputPortCount;
  vStreamProc_InputPortInfoConstPtrType inputPortInfos = Info->InputPortInfos;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there are different input data lengths. */
  if (Info->MaxInputLength > Info->MinInputLength)
  {
    vStreamProc_InputPortIterType inputPortIdx;

    /* #20 Report INSUFFICIENT_INPUT on all ports with fewer data than the maximum input length.*/
    for (inputPortIdx = 0u; inputPortIdx < inputPortCount; inputPortIdx++)
    {
      if ((inputPortInfos[inputPortIdx].IsConnected == TRUE)
        &&(Info->InputLength[inputPortIdx] < Info->MaxInputLength))
      {
        ProcNodeInfo->InputPortResults[inputPortIdx] = VSTREAMPROC_INSUFFICIENT_INPUT;
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_UpdateSignalHandlers
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_UpdateSignalHandlers(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Deregister all DataAvailable signal handler. */
  vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
    ProcNodeInfo,
    Info,
    vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
    VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

  /* #20 Check if the result was already provided. */
  if (workspace->ResultProvided == TRUE)
  {
    /* #30 Deregister the StorageAvailable handler at the progress output port */
    /* Result is already written, nothing to do. */
    (void)vStreamProc_SetOutputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);
  }
  /* #40 Otherwise */
  else
  {
    /* #50 Check if the processed length was restricted by the progress port. */
    if (Info->MinInputLength > Info->MaxOutputLength)
    {
      /* #60 Wait for storage available at the progress output port */
      (void)vStreamProc_SetOutputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

      /* #70 Block all WaveEnd handler. */
      /* Hint: Otherwise, the WaveEnd handler might be called before the output buffer is available again */
      vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
        ProcNodeInfo,
        Info,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
        VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);
    }
    /* #80 Otherwise: */
    else
    {
      /* #90 Deregister the StorageAvailable handler at the progress output port */
      (void)vStreamProc_SetOutputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

      /* #100 Check if there is more data available. */
      if (Info->MaxInputLength > Info->MinInputLength)
      {
        /* #110 Register only the DataAvailable signal handler of the min input port */
        (void)vStreamProc_SetInputPortSignalHandlerState(
          ProcNodeInfo->ProcessingNodeId,
          Info->MinInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
          VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
      }
      /* #120 Otherwise, register only the first connected input port */
      /* Hint: All data compared, but there might be still more data incoming. */
      else
      {
        (void)vStreamProc_SetInputPortSignalHandlerState(
          ProcNodeInfo->ProcessingNodeId,
          Info->FirstConnectedInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
          VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
      }

      /* #130 Register all WaveEnd handler. */
      vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
        ProcNodeInfo,
        Info,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_StartOutputPortWaves
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_StartOutputPortWaves(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType spRetVal = VSTREAMPROC_OK;
  vStreamProc_WaveInfoType waveInfo;
  vStreamProc_OutputPortInfoConstPtrType resultPortInfo =
    &Info->OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData];

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get wave information from reference port. */
  (void)vStreamProc_GetInputPortWaveInfo(ProcNodeInfo->ProcessingNodeId,
    Info->FirstConnectedInputPortId,
    &waveInfo);

  /* #20 Start wave at the result data port. */
  if (resultPortInfo->IsConnected == TRUE)
  {
    spRetVal = vStreamProc_StartOutputPortWave(
      ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData,
      &waveInfo);
  }

  /* #30 Check if the StartWave of the result output port was successful. */
  if (spRetVal == VSTREAMPROC_OK)
  {
    /* #40 Check if the progress output port is connected. */
    if (Info->ProgressPortInfo->IsConnected == TRUE)
    {
      /* #50 Start wave at the progress output port. */
      spRetVal = vStreamProc_StartOutputPortWave(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress,
        &waveInfo);
    }
  }

  /* #60 Check if the StartWave was successful. */
  if (spRetVal == VSTREAMPROC_OK)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_EndOutputPortWaves
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_EndOutputPortWaves(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_WaveInfoType waveInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get wave information from reference port. */
  (void)vStreamProc_GetInputPortWaveInfo(ProcNodeInfo->ProcessingNodeId,
    Info->FirstConnectedInputPortId,
    &waveInfo);

  /* #20 End wave at the result data port. */
  retVal = vStreamProc_EndOutputPortWave(
    ProcNodeInfo->ProcessingNodeId,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData,
    &waveInfo);

  /* #30 Check if the EndWave of the result output port was successful. */
  if (retVal == VSTREAMPROC_OK)
  {
    /* #40 Check if the progress output port is connected. */
    if (Info->ProgressPortInfo->IsConnected == TRUE)
    {
      /* #50 End wave at the progress output port. */
      retVal = vStreamProc_EndOutputPortWave(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_Progress,
        &waveInfo);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_CheckRemainingInputLength
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_CheckRemainingInputLength(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_ProcessingNode_ByteCompare_InfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Info,
  P2VAR(vStreamProc_SignalResponseType, AUTOMATIC, VSTREAMPROC_APPL_DATA) SignalResponse)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType currentInputPortLength =
    Info->InputPortInfos[ProcNodeInfo->SignalInfo.InputPortId].ReadRequest.StorageInfo.AvailableLength;
  vStreamProc_OutputPortInfoConstPtrType resultOutputPortInfo =
    &Info->OutputPortInfos[vStreamProcConf_vStreamProcOutputPort_vStreamProc_ByteCompare_ResultData];

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the current input port has less data than at least one other input port. */
  if (Info->MaxInputLength > currentInputPortLength)
  {
    /* #20 Check if the result output port is connected. */
    if (resultOutputPortInfo->IsConnected == TRUE)
    {
      /* #30 Write the error to the result output port. */
      (void)vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort(
        ProcNodeInfo,
        VSTREAMPROC_PROCESSINGNODE_BYTECOMPARE_E_INPUT_LENGTHS);

      /* #40 Consume all data. In case of an internal error set FAILED as signal response. */
      /* Hint: Otherwise, the signal response remains unchanged. */
      if (vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData(ProcNodeInfo, Info, TRUE) != VSTREAMPROC_OK)
      {
        *SignalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
      }
    }
    /* Otherwise: Set FAILED as signal response to failed. */
    else
    {
      *SignalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
    }
  }
}
/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize work space. */
  vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace(workspace);

  return E_OK;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  vStreamProc_ProcessingNode_ByteCompare_InfoType info;
  vStreamProc_ReturnType localRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Update the start wave count */
  workspace->OpenInputWaveCount++;
  workspace->WaveStartReceivedCount++;

  /* #20 Collect input information. */
  localRetVal = vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(ProcNodeInfo, &info);

  if (localRetVal == VSTREAMPROC_OK)
  {
    /* #30 Update wave counter. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

    /* #40 Start output port wave(s) when all wave start signals were received. */
    /* Implicitly, the DataAvailable signal on this port will trigger the start of the input port comparison.*/
    if (workspace->WaveStartReceivedCount == info.ConnectedInputPortCount)
    {
      signalResponse = vStreamProc_ProcessingNode_ByteCompare_StartOutputPortWaves(ProcNodeInfo, &info);
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_SignalHandler_CompareInput
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_SignalHandler_CompareInput(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  P2CONST(vStreamProc_ProcessingNode_ByteCompare_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr =
    vStreamProc_GetTypedConfigOfProcessingNode_ByteCompare_Config(ProcNodeInfo);                                        /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  vStreamProc_ProcessingNode_ByteCompare_InfoType info;
  vStreamProc_ReturnType localRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Collect input information. */
  localRetVal = vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(ProcNodeInfo, &info);

  if (localRetVal == VSTREAMPROC_OK)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

    /* #20 Check if the comparison is not failed already. */
    if (workspace->ResultProvided == FALSE)
    {
      /* #30 Check if there is at least one byte to compare. */
      if (info.MaxCompareLength > 0u)
      {
        /* #40 Restrict to maximum configured compare length if necessary. */
        if (specializedCfgPtr->BytesToCompareOfProcessingNode_ByteCompare_Config < info.MaxCompareLength)
        {
          info.MaxCompareLength = specializedCfgPtr->BytesToCompareOfProcessingNode_ByteCompare_Config;

          /* #50 Not all data can be consumed in this cycle, clear the acknowledge flag. */
          vStreamProc_ClrSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);
        }

        /* #60 Perform compare. */
        localRetVal = vStreamProc_ProcessingNode_ByteCompare_Compare(ProcNodeInfo, &info, &signalResponse);

        if (localRetVal == VSTREAMPROC_OK)
        {
          /* #70 Write the progress output port. */
          localRetVal = vStreamProc_ProcessingNode_ByteCompare_WriteProgressOutputPort(
            ProcNodeInfo,
            info.ProgressPortInfo,
            info.CompareElementLength);
        }
      }
    }
    /* #80 Otherwise: Just consume all data. */
    else
    {
      localRetVal = vStreamProc_ProcessingNode_ByteCompare_ConsumeAllInputData(ProcNodeInfo, &info, TRUE);
    }
  }

  /* #90 Set the signal response to failed if any internal error occurred. */
  if (localRetVal != VSTREAMPROC_OK)
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  }
  /* #100 Otherwise, update the Port results and signal handler states for the next cycle. */
  else
  {
    vStreamProc_ProcessingNode_ByteCompare_UpdatePortResults(ProcNodeInfo, &info);

    /* Hint: If the signal processing is not finished yet, the signal handlers must not be updated to avoid
             loosing the signal. */
    if (vStreamProc_IsSignalResponseFlagSet(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK))
    {
      vStreamProc_ProcessingNode_ByteCompare_UpdateSignalHandlers(ProcNodeInfo, &info);
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_SignalHandler_WriteResult
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_SignalHandler_WriteResult(/* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2VAR(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  vStreamProc_InputPortIdType inputPortId = ProcNodeInfo->SignalInfo.InputPortId;
  vStreamProc_ReturnType localRetVal;
  vStreamProc_ProcessingNode_ByteCompare_InfoType info;

  /* ----- Implementation ----------------------------------------------- */
  localRetVal = vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(ProcNodeInfo, &info);

  if (localRetVal == VSTREAMPROC_OK)
  {
    /* #10 Check if all WaveEnds were already received and the wave can be ended. */
    /* Hint: This is the case after all WaveEnds are unblocked again. */
    if (workspace->WaveEndReceivedCount == info.ConnectedInputPortCount)
    {
      workspace->OpenInputWaveCount--;

      /* #20 Check if this was the last WaveEnd */
      if (workspace->OpenInputWaveCount == 0u)
      {
        /* #30 Check if there was not yet a result provided. */
        /* Hint: If a result was already provided, the comparison was not successful. */
        if (workspace->ResultProvided == FALSE)
        {
          localRetVal= vStreamProc_ProcessingNode_ByteCompare_WriteResultOutputPort(ProcNodeInfo, E_OK);
        }

        /* #40 End the waves at the output port . */
        if (localRetVal == VSTREAMPROC_OK)
        {
          localRetVal = vStreamProc_ProcessingNode_ByteCompare_EndOutputPortWaves(ProcNodeInfo, &info);

          /* #50 Register all WaveStart handler for the next cycle. */
          vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
            ProcNodeInfo,
            &info,
            vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart,
            VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

          vStreamProc_ProcessingNode_ByteCompare_ResetWorkspace(workspace);
        }
      }

      /* #60 Acknowledge the WaveEnd. */
      if (localRetVal == VSTREAMPROC_OK)
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
      }
    }
    /* #70 Otherwise: */
    else
    {
      /* #80 Update the workspace. */
      workspace->WaveEndReceivedCount++;

      /* #90 Check if this is the last WaveEnd */
      if (workspace->WaveEndReceivedCount == info.ConnectedInputPortCount)
      {
        workspace->OpenInputWaveCount--;

        /* #100 Register all WaveEnd handler and block the WaveStart handler. */
        /* Hint: This is necessary to make sure that all waves are ended before a new wave can be started. */
        vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
          ProcNodeInfo,
          &info,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart,
          VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

        vStreamProc_ProcessingNode_ByteCompare_UpdateInputSignalHandlerStates(
          ProcNodeInfo,
          &info,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
          VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

        signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

      }
      /* #110 Otherwise: Block the WaveEnd and return BUSY. */
      /* Hint: The signal will be unblocked and acknowledged after all WaveEnds are received. */
      else
      {
        (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
          inputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
          VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

        workspace->WaveEndHandlerState[inputPortId] = VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED;

        signalResponse = VSTREAMPROC_SIGNALRESPONSE_BUSY;
      }

      /* #120 Check if the current input port has less data than the port with the most data. */
      /* Hint: This is not solvable as there will be no more data after the WaveEnd is received. */
      vStreamProc_ProcessingNode_ByteCompare_CheckRemainingInputLength(ProcNodeInfo, &info, &signalResponse);
    }
  }

  return signalResponse;
}

/***********************************************************************************************************************
 *  vStreamProc_ProcessingNode_ByteCompare_SignalHandler_Resume
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_ByteCompare_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ByteCompare_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_ByteCompare_WorkspaceType(ProcNodeInfo);                  /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  P2CONST(vStreamProc_SignalHandlerStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) waveStartHandlerState =
    workspace->WaveStartHandlerState;
  P2CONST(vStreamProc_SignalHandlerStateType, AUTOMATIC, VSTREAMPROC_APPL_VAR) waveEndHandlerState =
    workspace->WaveEndHandlerState;
  vStreamProc_InputPortIterType inputPortIdx;
  vStreamProc_ReturnType localRetVal;
  vStreamProc_ProcessingNode_ByteCompare_InfoType info;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query all ports. */
  localRetVal = vStreamProc_ProcessingNode_ByteCompare_GetAllPortInformation(ProcNodeInfo, &info);

  /* #20 Restore the WaveStart/WaveEnd signal handler states for all input ports. */
  if (localRetVal == VSTREAMPROC_OK)
  {
    for (inputPortIdx = 0u;
         inputPortIdx < ProcNodeInfo->InputPortCount;
         inputPortIdx++)
    {
      if (info.InputPortInfos[inputPortIdx].IsConnected == TRUE)
      {
        (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
          (vStreamProc_InputPortIdType)inputPortIdx,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart,
          waveStartHandlerState[inputPortIdx]);

        (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
          (vStreamProc_InputPortIdType)inputPortIdx,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
          waveEndHandlerState[inputPortIdx]);
      }
    }

    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_BYTECOMPARE_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_ByteCompare.c
 *********************************************************************************************************************/
