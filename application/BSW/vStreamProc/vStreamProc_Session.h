/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Session.h
 *        \brief  vStreamProc header file for the session handling.
 *
 *      \details  Declaration of the session related functionality.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_SESSION_H
#define VSTREAMPROC_SESSION_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Session_InitSession()
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the session related data.
 * \details       -
 * \param[in]     PipeId        Id of the desired pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Session_InitSession(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_CreateSession()
 *********************************************************************************************************************/
/*!
 * \brief         Create a snapshot session.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \return        VSTREAMPROC_FAILED                Session creation request was not accepted.
 * \return        VSTREAMPROC_OK                    Session created successfully.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Session could not be created because not enough snapshots are
 *                                                  available.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-310290
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_CreateSession(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetEntryPointInfoOfSession()
 *********************************************************************************************************************/
/*!
 * \brief         Query the stream position and meta data of the session at the entry point.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[in]     EntryPointId                      Id of the entry point.
 * \param[out]    StreamPositionInfo                Pointer to the stream position info.
 * \param[out]    MetaData                          Pointer to the meta data.
 * \return        VSTREAMPROC_FAILED                Query was not accepted.
 * \return        VSTREAMPROC_OK                    Query was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    No query possible due to insufficient snapshot data at the
 *                                                  entry point.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-310290
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetEntryPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo,
  vStreamProc_ReadRequestPtrType MetaData);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetExitPointInfoOfSession()
 *********************************************************************************************************************/
/*!
 * \brief         Query the stream position and meta data of the session at the exit point.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[in]     ExitPointId                       Id of the exit point.
 * \param[out]    StreamPositionInfo                Pointer to the stream position info.
 * \param[out]    MetaData                          Pointer to the meta data.
 * \return        VSTREAMPROC_FAILED                Query was not accepted.
 * \return        VSTREAMPROC_OK                    Query was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   No query possible due to insufficient snapshot data at the
 *                                                  exit point.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \trace         CREQ-310290
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetExitPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointId,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo,
  vStreamProc_ReadRequestPtrType MetaData);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetEntryPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * \brief         Query the wave information and meta data of the session at the entry point.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[in]     EntryPointId                      Id of the entry point.
 * \param[in,out] WaveInfo                          Pointer to the wave information structure.
 *                - WaveTypeSymbolicName            Defines the wave level the wave data shall be provided.
 *                - <Other members>                 Filled with wave data if successful.
 * \return        VSTREAMPROC_FAILED                Query was not accepted.
 * \return        VSTREAMPROC_OK                    Query was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    No query possible due to insufficient snapshot data at the
 *                                                  entry point.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetEntryPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointId,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetExitPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * \brief         Query the wave information and meta data of the session at the exit point.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[in]     ExitPointId                       Id of the exit point.
 * \param[in,out] WaveInfo                          Pointer to the wave information structure.
 *                - WaveTypeSymbolicName            Defines the wave level the wave data shall be provided.
 *                - <Other members>                 Filled with wave data if successful.
 * \return        VSTREAMPROC_FAILED                Query was not accepted.
 * \return        VSTREAMPROC_OK                    Query was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   No query possible due to insufficient snapshot data at the
 *                                                  exit point.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetExitPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType ExitPointId,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetMaxDataSizeOfSession
 *********************************************************************************************************************/
/*!
 * \brief         Returns the maximum size of serialized session data.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \return        0                                 No session data or unknown pipe ID.
 * \return        >0                                Max size of session data in Bytes.
 * \pre           The pipe is initialized.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_Session_GetMaxDataSizeOfSession(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Session_GetDataOfSession
 *********************************************************************************************************************/
/*!
 * \brief         Retrieve the serialized data of a previously created session.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[out]    SessionData                       Buffer for session data.
 * \param[in,out] Length                            IN:  Available length of SessionData buffer.
 *                                                  OUT: Used length of SessionData buffer.
 * \return        VSTREAMPROC_FAILED                Session data could not be created.
 * \return        VSTREAMPROC_OK                    Session data successfully serialized.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   SessionData buffer is to small to hold session data.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_GetDataOfSession(
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Length);

/**********************************************************************************************************************
 *  vStreamProc_Session_RestoreSession()
 *********************************************************************************************************************/
/*!
 * \brief         Restores the pipe with the provided session data.
 * \details       -
 * \param[in]     PipeId                            Id of the desired pipe.
 * \param[in]     SessionData                       Pointer to the session data.
 * \param[in]     SessionDataLength                 Length of the session data.
 * \return        VSTREAMPROC_FAILED                Session could not be restored.
 * \return        VSTREAMPROC_OK                    Session successfully restored.
 * \pre           vStreamProc is initialized.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Session_RestoreSession(
  vStreamProc_PipeIdType PipeId,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  vStreamProc_LengthType SessionDataLength);

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_SESSION_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Session.h
 *********************************************************************************************************************/
