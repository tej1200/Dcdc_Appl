/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Mode.c
 *        \brief  vStreamProc Mode Sub Module Source Code File
 *
 *      \details  Implementation of the vStreamProc Mode sub module.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_MODE_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Mode.h"
#include "vStreamProc_Stream.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vStreamProc_Mode_EvaluateMetaStates()
 *********************************************************************************************************************/
/*! \brief      Evaluate active meta states for current base state values.
 *  \details    -
 *  \param[in]  PipeId            The pipe to operate on.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_EvaluateMetaStates(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 * vStreamProc_Mode_UpdateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*! \brief      Updates the stream output port mapping of the edge based on the provided activation  state if the edge
 *              is a consumer edge.
 *  \details    If PendingActivation is TRUE, the named input port is mapped to the provided stream output port.
 *              Otherwise, the input port mapping is reset if it still maps to the provided stream output port.
 *  \param[in]  EdgeId                The edge id.
 *  \param[in]  StreamOutputPortId    The stream output port id.
 *  \param[in]  PendingActivation     The new activation state.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_UpdateStreamOutputPortMapping(
  vStreamProc_EdgeIdType EdgeId,
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  boolean PendingActivation);

/**********************************************************************************************************************
 * vStreamProc_Mode_SwitchPortActivations()
 *********************************************************************************************************************/
/*! \brief      Switch port activations based on active meta states.
 *  \details    -
 *  \param[in]  PipeId            The pipe to operate on.
 *  \return     E_OK - success
 *  \return     E_NOT_OK - error
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *  \note        Multiple exit point with limits connected to a single storage node not yet supported.
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_SwitchPortActivations(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Mode_IsProcessingNodeActiveProducer
 *********************************************************************************************************************/
/*!
 * \brief         Check if the provided node is an active producer.
 * \details       A processing node is considered active if any of its input ports is connected to
 *                an active storage output port.
 * \param[in]     ProcessingNodeId    The processing node instance id which shall be checked.
 * \return        TRUE                The node is an active producer.
 * \return        FALSE               The node is not an active producer.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Mode_IsProcessingNodeActiveProducer(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 * vStreamProc_Mode_ActivateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*!
 * \brief         Activates the stream output port mapping with the provided stream output port.
 * \details       Additionally, all affected runtime variables are updated as well.
 * \param[in]     StreamOutputPortMappingId   The stream output port mapping id.
 * \param[in]     StreamOutputPortId          The id of the new stream output port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_ActivateStreamOutputPortMapping(
  vStreamProc_StreamOutputPortMappingIdType StreamOutputPortMappingId,
  vStreamProc_StreamOutputPortIdType StreamOutputPortId);

/**********************************************************************************************************************
 * vStreamProc_Mode_DeactivateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*!
 * \brief         Deactivates the stream output port mapping by resetting of the mapped stream output port.
 * \details       Additionally, all affected runtime variables are updated as well.
 * \param[in]     StreamOutputPortMappingId   The stream output port mapping id.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_DeactivateStreamOutputPortMapping(
  vStreamProc_StreamOutputPortMappingIdType StreamOutputPortMappingId);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * vStreamProc_Mode_EvaluateMetaStates()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_EvaluateMetaStates(                                     /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasBaseStateInfo()
    && vStreamProc_HasMetaState()
    && vStreamProc_HasMetaStateInfo()
    && vStreamProc_HasMetaState2BaseState()
    && vStreamProc_HasMetaStateTruthTable())
  {
    vStreamProc_MetaStateIterType metaStateIdx;

    /* #10 Iterate all meta states of the pipe */
    for (metaStateIdx = vStreamProc_GetMetaStateStartIdxOfPipe(PipeId);
      metaStateIdx < vStreamProc_GetMetaStateEndIdxOfPipe(PipeId);
      metaStateIdx++)
    {
      vStreamProc_MetaStateTruthTableIterType truthTableRow;
      vStreamProc_MetaStateTruthTableIterType truthTableRowStartIdx =
        vStreamProc_GetMetaStateTruthTableStartIdxOfMetaState(metaStateIdx);
      boolean                                 truthTableMatch = FALSE;
      boolean                                 stateActivation = FALSE;

      /* #20 Iterate all rows of the truth table of the meta state */
      for (truthTableRow = 0u;
        truthTableRow < vStreamProc_GetTruthTableCountOfMetaState(metaStateIdx);
        truthTableRow++)
      {
        vStreamProc_MetaState2BaseStateIterType metaState2BaseStateIdx;
        vStreamProc_MetaStateTruthTableIterType truthTableIdx = truthTableRowStartIdx;

        truthTableMatch = TRUE;

        /* #30 Compare current activation of all base states referenced by meta state against expected values. */
        for (metaState2BaseStateIdx = vStreamProc_GetMetaState2BaseStateStartIdxOfMetaState(metaStateIdx);
          metaState2BaseStateIdx < vStreamProc_GetMetaState2BaseStateEndIdxOfMetaState(metaStateIdx);
          metaState2BaseStateIdx++)
        {
          vStreamProc_BaseStateIterType baseStateIdx = vStreamProc_GetBaseStateIdxOfMetaState2BaseState(metaState2BaseStateIdx);

          if (vStreamProc_IsExpectedStateActivationOfMetaStateTruthTable(truthTableIdx)
            != vStreamProc_IsActiveOfBaseStateInfo(baseStateIdx))
          {
            truthTableMatch = FALSE;

            break;
          }

          truthTableIdx++;
        }

        /* #40 End search if matching base state combination was found in truth table. */
        if (truthTableMatch == TRUE)
        {
          break;
        }

        truthTableRowStartIdx += vStreamProc_GetMetaState2BaseStateLengthOfMetaState(metaStateIdx);
      }

      /* #50 Activate meta state depending on result of truth table evaluation:
       *   + Normal truth table: Match was found.
       *   + Inverse truth table: No match was found. */
      if ((truthTableMatch == TRUE) == vStreamProc_IsMatchActivationOfMetaState(metaStateIdx))
      {
        stateActivation = TRUE;
      }

      vStreamProc_SetActiveOfMetaStateInfo(metaStateIdx, stateActivation);
    }
  }
}

/**********************************************************************************************************************
 * vStreamProc_Mode_UpdateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_UpdateStreamOutputPortMapping(
  vStreamProc_EdgeIdType EdgeId,
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  boolean PendingActivation)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortMappingIdType streamOutputPortMappingId =
    vStreamProc_GetStreamOutputPortMappingIdxOfEdge(EdgeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the edge is a consumer edge. */
  /* Only consumer edges reference a stream output port mapping entry. */
  if (streamOutputPortMappingId != VSTREAMPROC_NO_STREAMOUTPUTPORTMAPPINGIDXOFEDGE)
  {
    vStreamProc_ProcessingNodeIdType consumerNode = vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
      vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(StreamOutputPortId));
    vStreamProc_NamedOutputPortIterType namedOutputPortIdx;
    boolean isActiveProducer;

    /* #20 If pendingActivation is TRUE, map the provided stream output port. */
    if (PendingActivation == TRUE)
    {
      /* #30 Deactivate the previous stream output port mapping. */
      vStreamProc_Mode_DeactivateStreamOutputPortMapping(streamOutputPortMappingId);

      /* #40 Activate the new stream output port mapping. */
      vStreamProc_Mode_ActivateStreamOutputPortMapping(streamOutputPortMappingId, StreamOutputPortId);
    }
    /* #50 Otherwise: */
    else
    {
      /* #60 Check if the stream output port is still mapped to the provided stream output port. */
      /* Hint: If the input port is mapped to a different storage output port, it was already remapped in a previous
               step due to the activation of another edge and must not be changed. */
      if (vStreamProc_GetStreamOutputPortMapping(streamOutputPortMappingId) == StreamOutputPortId)
      {
        /* #70 Deactivate the stream output port mapping. */
        vStreamProc_Mode_DeactivateStreamOutputPortMapping(streamOutputPortMappingId);
      }
    }

    /* #80 Update active producer. */
    isActiveProducer = vStreamProc_Mode_IsProcessingNodeActiveProducer(consumerNode);

    for ( namedOutputPortIdx = vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(consumerNode);
          namedOutputPortIdx < vStreamProc_GetNamedOutputPortEndIdxOfProcessingNode(consumerNode);
          namedOutputPortIdx++)
    {
      vStreamProc_StreamIterType streamIdx = vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortIdx);

      if (isActiveProducer == TRUE)
      {
        if (streamIdx != VSTREAMPROC_NO_STREAMIDXOFNAMEDOUTPUTPORT)
        {
          vStreamProc_SetNamedOutputPortIdxOfStreamInfo(
            streamIdx,
            (vStreamProc_NamedOutputPortIdxOfStreamInfoType)namedOutputPortIdx);
        }
      }
      else
      {
        /* #90 Else: Check if the named output port is still set as active producer. */
        /* Hint: If already another named output port is set, the active producer was already
               set in a previous step during the activation of another edge. */
        if (vStreamProc_GetNamedOutputPortIdxOfStreamInfo(streamIdx) == namedOutputPortIdx)
        {
          /* #100 If true, reset the mapping as the named output port is no longer the active producer. */
          vStreamProc_SetNamedOutputPortIdxOfStreamInfo(
            streamIdx,
            (vStreamProc_NamedOutputPortIdxOfStreamInfoType)VSTREAMPROC_NO_NAMEDOUTPUTPORT);
        }
      }
    }
  }
}

/**********************************************************************************************************************
 * vStreamProc_Mode_SwitchPortActivations()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_SwitchPortActivations(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType            retVal = E_OK;                                                                              /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_EdgeIterType  edgeIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate all edges of the pipe */
  for ( edgeIdx = vStreamProc_GetEdgeStartIdxOfPipe(PipeId);
        edgeIdx < vStreamProc_GetEdgeEndIdxOfPipe(PipeId);
        edgeIdx++ )
  {
    vStreamProc_StreamOutputPortIdType streamOutputPortId = vStreamProc_GetStreamOutputPortIdxOfEdge(edgeIdx);

    /* #20 If a stream is referenced by the edge, it is either a consumer edge (from storage node output
     *   to processing node input) or an edge from storage node output to an exit point.
     *   Otherwise, it is an edge from an entry point to a storage node and is always active. */
    if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORTIDXOFEDGE)
    {
      vStreamProc_MetaStateIterType metaStateIdx = vStreamProc_GetMetaStateIdxOfEdge(edgeIdx);
      boolean pendingActivation = FALSE;

      /* #30 Edge is either always active (because it has no meta state dependency)
       *   or when meta state referenced by edge is active. */
      if ( (metaStateIdx == VSTREAMPROC_NO_METASTATEIDXOFEDGE)
        || (vStreamProc_HasMetaStateInfo() && vStreamProc_IsActiveOfMetaStateInfo(metaStateIdx)) )
      {
        pendingActivation = TRUE;
      }

      /* #40 Set the new port activation if it has changed. */
      if (vStreamProc_Stream_IsStreamOutputPortActive(streamOutputPortId) != pendingActivation)
      {
        /* #50 Update the stream output port mapping. */
        vStreamProc_Mode_UpdateStreamOutputPortMapping(
          (vStreamProc_EdgeIdType)edgeIdx,
          streamOutputPortId,
          pendingActivation);
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Mode_IsProcessingNodeActiveProducer
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Mode_IsProcessingNodeActiveProducer(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  vStreamProc_NamedInputPortIterType namedInputPortIdx;
  boolean retVal;

  if (vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId)
    < vStreamProc_GetNamedInputPortEndIdxOfProcessingNode(ProcessingNodeId))
  {
    retVal = FALSE;
  }
  else
  {
    /* Processing nodes without input ports may produce data from a source outside vStreamProc. */
    retVal = TRUE;
  }

  /* #10 Iterate over the named input ports and check if at least one is connected to a storage output port. */
  for (namedInputPortIdx = vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId);
       namedInputPortIdx < vStreamProc_GetNamedInputPortEndIdxOfProcessingNode(ProcessingNodeId);
       namedInputPortIdx++)
  {
    vStreamProc_StreamOutputPortMappingIterType streamOutputPortMappingIdx =
      vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortIdx);

    if (streamOutputPortMappingIdx != VSTREAMPROC_NO_STREAMOUTPUTPORTMAPPINGIDXOFNAMEDINPUTPORT)
    {
      vStreamProc_StreamOutputPortIdType streamOutputPortId =
        vStreamProc_GetStreamOutputPortMapping(streamOutputPortMappingIdx);

      /* #20 Return true if the input port is mapped to a stream. */
      /* Hint: Input port are only mapped to active streams. */
      if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
      {
        retVal = TRUE;
        break;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 * vStreamProc_Mode_ActivateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_ActivateStreamOutputPortMapping(
  vStreamProc_StreamOutputPortMappingIdType StreamOutputPortMappingId,
  vStreamProc_StreamOutputPortIdType StreamOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_NamedInputPortIdType namedInputPortId =
    vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(StreamOutputPortId);
  vStreamProc_ProcessingNodeIdType consumerNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortId);
  vStreamProc_ExitPointIdType exitPointId = vStreamProc_AccessNode_GetExitPointIdOfExitNode(consumerNodeId);
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Update the stream output port mapping. */
  vStreamProc_SetStreamOutputPortMapping(StreamOutputPortMappingId, StreamOutputPortId);

  (void)vStreamProc_Stream_GetInputPortHandle(
    consumerNodeId,
    vStreamProc_GetInputPortSymbolicNameOfPortDef(vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortId)),
    &inputPortHandle);

  /* #20 Update the consume position. */
  /* Hint: Mode switch is only possible if all produced data were consumed. Activate new port with latest position. */
  vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(
    StreamOutputPortId,
    vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(inputPortHandle.WaveId));
  vStreamProc_SetConsumePositionOfStreamOutputPortInfo(
    StreamOutputPortId,
    vStreamProc_GetProducePositionOfWaveVarInfo(inputPortHandle.WaveId));

  /* #30 Activate the storage output port. */
  if (inputPortHandle.StorageOutputPortId != VSTREAMPROC_NO_STORAGEOUTPUTPORT)
  {
    vStreamProc_GetSetActivationSignalOfStorageNodeTypeDef(
      vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(inputPortHandle.StorageNodeId))
      (inputPortHandle.StorageNodeId, inputPortHandle.StorageOutputPortId, TRUE);
  }

  /* #40 Update the limit on the producer stream. */
  if (exitPointId != VSTREAMPROC_NO_EXITPOINT)
  {
    vStreamProc_SetStreamLimitIdxOfStreamInfo(
      inputPortHandle.StreamId,
      vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId));
  }
}

/**********************************************************************************************************************
 * vStreamProc_Mode_DeactivateStreamOutputPortMapping()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Mode_DeactivateStreamOutputPortMapping(
  vStreamProc_StreamOutputPortMappingIdType StreamOutputPortMappingId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType previousStreamOutputPortId =
    vStreamProc_GetStreamOutputPortMapping(StreamOutputPortMappingId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Reset the stream output port mapping. */
  vStreamProc_SetStreamOutputPortMapping(StreamOutputPortMappingId, VSTREAMPROC_NO_STREAMOUTPUTPORT);

  if (previousStreamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
  {
    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(previousStreamOutputPortId);
    vStreamProc_StorageOutputPortIdType  storageOutputPortId =
      vStreamProc_GetStorageOutputPortIdxOfStreamOutputPort(previousStreamOutputPortId);

    /* #20 Reset the consume position. */
    vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(previousStreamOutputPortId, 0u);
    vStreamProc_SetConsumePositionOfStreamOutputPortInfo(previousStreamOutputPortId, 0u);

    /* #30 Deactivate the storage output port. */
    if (storageOutputPortId != VSTREAMPROC_NO_STORAGEOUTPUTPORT)
    {
      vStreamProc_StorageNodeIdType storageNodeId =
        vStreamProc_GetStorageNodeIdxOfStorageOutputPort(storageOutputPortId);

      vStreamProc_GetSetActivationSignalOfStorageNodeTypeDef(
        vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(storageNodeId))
        (storageNodeId, storageOutputPortId, FALSE);
    }

    /* #40 Deactivate the limit on the producer stream. */
    vStreamProc_SetStreamLimitIdxOfStreamInfo(
      streamId,
      (vStreamProc_StreamLimitIdxOfStreamInfoType)VSTREAMPROC_NO_STREAMLIMIT);
  }
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * vStreamProc_Mode_ApplyMode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_ApplyMode(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType PipeModeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasBaseState()
    && vStreamProc_HasBaseStateInfo()
    && vStreamProc_HasMode()
    && vStreamProc_HasMode2BaseState())
  {
    vStreamProc_BaseStateIterType baseStateIdx;

    /* #10 If NO_MODE is requested, set all base states to their default values. */
    if (PipeModeId == VSTREAMPROC_NO_MODE)
    {
      /* #20 Iterate over all base states. */
      for (baseStateIdx = vStreamProc_GetBaseStateStartIdxOfPipe(PipeId);
        baseStateIdx < vStreamProc_GetBaseStateEndIdxOfPipe(PipeId);
        baseStateIdx++)
      {
        vStreamProc_SetActiveOfBaseStateInfo(baseStateIdx, vStreamProc_IsDefaultActivationOfBaseState(baseStateIdx));
      }
    }
    /* #30 Otherwise (actual mode is requested) */
    else
    {
      vStreamProc_Mode2BaseStateIterType mode2BaseStateIdx;

      /* #40 Modify base states values as configured for given mode. */
      for (mode2BaseStateIdx = vStreamProc_GetMode2BaseStateStartIdxOfMode(PipeModeId);
        mode2BaseStateIdx < vStreamProc_GetMode2BaseStateEndIdxOfMode(PipeModeId);
        mode2BaseStateIdx++)
      {
        baseStateIdx = vStreamProc_GetBaseStateIdxOfMode2BaseState(mode2BaseStateIdx);

        vStreamProc_SetActiveOfBaseStateInfo(baseStateIdx, vStreamProc_IsBaseStateActivationOfMode2BaseState(mode2BaseStateIdx));
      }
    }

    retVal = vStreamProc_Mode_HandleMetaStateSwitching(PipeId);
  }
  else
  {
    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 * vStreamProc_Mode_HandleMetaStateSwitching()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Mode_HandleMetaStateSwitching(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Evaluate active meta states for new base state values. */
  vStreamProc_Mode_EvaluateMetaStates(PipeId);

  /* #20 Switch port activations based on active meta states. */
  retVal = vStreamProc_Mode_SwitchPortActivations(PipeId);

  return retVal;
}

/**********************************************************************************************************************
 * vStreamProc_Mode_IsActivationStateChanged()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Mode_IsActivationStateChanged(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType ModeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean baseStatesAreChanged = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasBaseState()
    && vStreamProc_HasBaseStateInfo()
    && vStreamProc_HasMode()
    && vStreamProc_HasMode2BaseState())
  {
    if (ModeId == VSTREAMPROC_NO_MODE)
    {
      vStreamProc_BaseStateIterType baseStateIdx;

      /* #10 Iterate over all base states of the pipe. */
      for (baseStateIdx = vStreamProc_GetBaseStateStartIdxOfPipe(PipeId);
           baseStateIdx < vStreamProc_GetBaseStateEndIdxOfPipe(PipeId);
           baseStateIdx++)
      {
        /* #20 Check if the current activation state differs from the default activation state. */
        if (vStreamProc_IsActiveOfBaseStateInfo(baseStateIdx)
          != vStreamProc_IsDefaultActivationOfBaseState(baseStateIdx))
        {
          baseStatesAreChanged = TRUE;
          break;
        }
      }
    }
    else
    {
      vStreamProc_Mode2BaseStateIterType modeToBaseStateIdx;

      /* #30 Iterate over affected base states. */
      for (modeToBaseStateIdx = vStreamProc_GetMode2BaseStateStartIdxOfMode(ModeId);
           modeToBaseStateIdx < vStreamProc_GetMode2BaseStateEndIdxOfMode(ModeId);
           modeToBaseStateIdx++)
      {
        vStreamProc_BaseStateIterType baseStateIdx = vStreamProc_GetBaseStateIdxOfMode2BaseState(modeToBaseStateIdx);

        /* #40 Check if the mode will change the base state. */
        if (vStreamProc_IsActiveOfBaseStateInfo(baseStateIdx)
          != vStreamProc_IsBaseStateActivationOfMode2BaseState(modeToBaseStateIdx))
        {
          baseStatesAreChanged = TRUE;
          break;
        }
      }
    }
  }

  return baseStatesAreChanged;
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Mode.c
 *********************************************************************************************************************/
