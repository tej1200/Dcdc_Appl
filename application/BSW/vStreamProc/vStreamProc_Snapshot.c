/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_Snapshot.c
 *        \brief  vStreamProc source code file for the snapshot handling
 *
 *      \details  Implementation of the snapshot related functionality.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_SNAPSHOT_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Snapshot.h"
#include "vStreamProc.h"
#include "vStreamProc_Queue.h"
#include "vStreamProc_Session.h"
#include "vStreamProc_Stream.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

 /*********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * \brief         Returns a pointer to the snapshot set queue of the provided processing node.
 * \details       -
 * \param[in]     ProcessingNodeId    Id of the processing node.
 * \return        Pointer to the snapshot set queue.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueuePtrType, AUTOMATIC) vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_DefragmentSnapshotQueue
 *********************************************************************************************************************/
/*!
 * \brief         Defragments the snapshot queue of the processing node.
 * \details       Every second entry is kept, beginning with the first entry.
 * \param[in]     Queue                   Pointer to snapshot queue which shall be defragmented.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_DefragmentSnapshotQueue(
  vStreamProc_QueuePtrType Queue);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_StoreInputPortContextData
 *********************************************************************************************************************/
/*!
 * \brief         Iterates over the input ports of the processing node and stores the context data.
 * \details       -
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \param[in]     SnapshotSlotId                    Id of the snapshot slot where the context shall be saved.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_StoreInputPortContextData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_StoreOutputPortContextData
 *********************************************************************************************************************/
/*!
 * \brief         Iterates over the output ports of the processing node and stores the context data.
 * \details       -
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \param[in]     SnapshotSlotId                    Id of the snapshot slot where the context shall be saved.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_StoreOutputPortContextData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId);

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshot
 *********************************************************************************************************************/
/*!
 * \brief         Creates a snapshot of the provided processing node.
 * \details       -
 * \param[in]     ProcessingNodeId                  Id of the processing node.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshot(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_QueuePtrType, AUTOMATIC) vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* #10 Return the snapshot set queue adress. */
  return vStreamProc_GetAddrSnapshotSetQueue(
    vStreamProc_GetSnapshotSetQueueIdxOfSnapshotSet(
      vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId)));
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_DefragmentSnapshotQueue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_DefragmentSnapshotQueue(
  vStreamProc_QueuePtrType Queue)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_QueueHandleType queueHandleToKeep = vStreamProc_Queue_GetFirstUsedHandle(Queue);
  vStreamProc_QueueHandleType queueHandleToDelete;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over queue until the end is reached. */
  /* Hint: At least one iteration is performed because this API is only called if the queue is full. */
  while (queueHandleToKeep != VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
  {
    /* #20 If there is a queue entry to keep, check the following queue entry. */
    queueHandleToDelete = vStreamProc_Queue_GetNextHandle(Queue, queueHandleToKeep);
    if (queueHandleToDelete != VSTREAMPROC_QUEUE_HANDLE_HEAD_USED)
    {
      /* #30 If there is a following queue entry, update the queue entry handle to keep for next iteration and
             then remove the queue entry. */
      queueHandleToKeep = vStreamProc_Queue_GetNextHandle(Queue, queueHandleToDelete);
      (void)vStreamProc_Queue_Remove(Queue, queueHandleToDelete);
    }
    else
    {
      /* #40 Otherwise, leave the loop. */
      break;
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_StoreInputPortContextData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_StoreInputPortContextData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SnapshotInputPortInfoIterType snapshotInputPortInfoIdx;
  vStreamProc_NamedInputPortIterType namedInputPortIdx =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the the input ports and store the relevant context data. */
  for (snapshotInputPortInfoIdx = vStreamProc_GetSnapshotInputPortInfoStartIdxOfSnapshotSlot(SnapshotSlotId);
       snapshotInputPortInfoIdx < vStreamProc_GetSnapshotInputPortInfoEndIdxOfSnapshotSlot(SnapshotSlotId);
       snapshotInputPortInfoIdx++)
  {
    vStreamProc_StreamOutputPortIterType streamOutputPortIdx =
      (vStreamProc_StreamOutputPortIterType)vStreamProc_GetStreamOutputPortMapping(
        vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortIdx));

    /* #20 Store the input data. */
    if (streamOutputPortIdx != VSTREAMPROC_NO_STREAMOUTPUTPORT)
    {
      vStreamProc_SetWaveHandleOfSnapshotInputPortInfo(
        snapshotInputPortInfoIdx,
        vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortIdx));

      vStreamProc_SetAbsolutePositionStartOfSnapshotInputPortInfo(
        snapshotInputPortInfoIdx,
        vStreamProc_GetAbsolutePositionStartOfStreamOutputPortInfo(streamOutputPortIdx));

      vStreamProc_SetConsumePositionOfSnapshotInputPortInfo(
        snapshotInputPortInfoIdx,
        vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortIdx));

      vStreamProc_SetWaveStateOfSnapshotInputPortInfo(
        snapshotInputPortInfoIdx,
        vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortIdx));
    }
    else
    {
      vStreamProc_SetConsumePositionOfSnapshotInputPortInfo(snapshotInputPortInfoIdx, 0u);
      vStreamProc_SetWaveHandleOfSnapshotInputPortInfo(snapshotInputPortInfoIdx, 0u);
    }

    namedInputPortIdx++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_StoreOutputPortContextData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_StoreOutputPortContextData(                                /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType SnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SnapshotOutputPortIterType snapshotOutputPortIdx;
  vStreamProc_NamedOutputPortIdType namedOutputPortIdx =
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the output ports and store the relevant context data. */
  for (snapshotOutputPortIdx = vStreamProc_GetSnapshotOutputPortStartIdxOfSnapshotSlot(SnapshotSlotId);
       snapshotOutputPortIdx < vStreamProc_GetSnapshotOutputPortEndIdxOfSnapshotSlot(SnapshotSlotId);
       snapshotOutputPortIdx++)
  {
    vStreamProc_StreamIterType streamIdx = vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortIdx);
    vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
    vStreamProc_SnapshotWaveIterType snapshotWaveIdx =
      vStreamProc_GetSnapshotWaveStartIdxOfSnapshotOutputPort(snapshotOutputPortIdx);
    vStreamProc_WaveLevelDefIterType waveLevelDefIdx;
    vStreamProc_WaveIterType waveIdx = 0u;

    if (streamIdx != VSTREAMPROC_NO_STREAMIDXOFNAMEDOUTPUTPORT)
    {
      waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamIdx);
    }

    /* #20 Store the wave data. */
    for (waveLevelDefIdx = vStreamProc_GetWaveLevelDefStartIdxOfPipe(pipeId);
         waveLevelDefIdx < vStreamProc_GetWaveLevelDefEndIdxOfPipe(pipeId);
         waveLevelDefIdx++)
    {
      /* #30 Check if the named output port has a valid stream. */
      /* Hint: If the stream index is invalid, the port is not connected. */
      if (streamIdx != VSTREAMPROC_NO_STREAMIDXOFNAMEDOUTPUTPORT)
      {
        vStreamProc_WaveTypeDefIterType waveTypeDefIdx = vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(waveLevelDefIdx);
        /* #40 Store runtime variables of wave. */
        vStreamProc_SetHandleOfSnapshotWaveInfo(
          snapshotWaveIdx,
          vStreamProc_GetHandleOfWaveVarInfo(waveIdx));

        vStreamProc_SetStateOfSnapshotWaveInfo(
          snapshotWaveIdx,
          vStreamProc_GetStateOfWaveVarInfo(waveIdx));

        vStreamProc_SetAbsolutePositionStartOfSnapshotWaveInfo(
          snapshotWaveIdx,
          vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveIdx));

        vStreamProc_SetProducePositionOfSnapshotWaveInfo(
          snapshotWaveIdx,
          vStreamProc_GetProducePositionOfWaveVarInfo(waveIdx));

        /* #50 Check if the wave has meta data. */
        if (vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(waveTypeDefIdx) != VSTREAMPROC_NO_DATATYPE)
        {
          vStreamProc_StorageNodeBufferType snapshotMetaData =
            vStreamProc_GetMetaDataPointerOfSnapshotWave(snapshotWaveIdx);
          P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) activeMetaData =
            (P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))vStreamProc_GetMetaDataPointerOfWave(waveIdx);
          vStreamProc_MetaDataElementSizeOfWaveTypeDefType byteIdx;

          /* #60 Perform a byte-wise copy of the wave meta data to the snapshot. */
          for (byteIdx = 0; byteIdx < vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(waveTypeDefIdx); byteIdx++)
          {
            snapshotMetaData[byteIdx] = activeMetaData[byteIdx];
          }
        }

        waveIdx++;
      }
      /* #70 Otherwise: */
      else
      {
        vStreamProc_SetHandleOfSnapshotWaveInfo(snapshotWaveIdx, 0u);
        vStreamProc_SetStateOfSnapshotWaveInfo(snapshotWaveIdx, VSTREAMPROC_WAVESTATE_UNINIT);
        vStreamProc_SetAbsolutePositionStartOfSnapshotWaveInfo(snapshotWaveIdx, 0u);
        vStreamProc_SetProducePositionOfSnapshotWaveInfo(snapshotWaveIdx, 0u);
      }

      snapshotWaveIdx++;
    }

    namedOutputPortIdx++;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshot(                                            /* PRQA S 6010 */ /* MD_MSR_STPTH */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* Snapshot data */
  vStreamProc_QueuePtrType queue = vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(ProcessingNodeId);
  vStreamProc_QueueHandleType queueHandle;
  vStreamProc_SnapshotSlotIdType snapshotSlotId;

  /* Workspace */
  P2CONST(void, AUTOMATIC, VSTREAMPROC_APPL_VAR) processingNodeWorkspace =
    vStreamProc_GetWorkspaceOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if snapshot queue is full. */
  if (vStreamProc_Queue_IsFull(queue))
  {
    /* #20 Defragment the snapshot queue. */
    vStreamProc_Snapshot_DefragmentSnapshotQueue(queue);
  }

  /* #30 Get the next free snapshot slot. */
  queueHandle = vStreamProc_Queue_GetFirstFreeHandle(queue);
  snapshotSlotId = vStreamProc_Queue_GetEntityHandle(queue, queueHandle);

  /* #40 Store the input port context data. */
  vStreamProc_Snapshot_StoreInputPortContextData(ProcessingNodeId, snapshotSlotId);

  /* #50 Store the output port context data. */
  vStreamProc_Snapshot_StoreOutputPortContextData(ProcessingNodeId, snapshotSlotId);

  /* #60 Store the processing node workspace, if one exists. */
  if (processingNodeWorkspace != NULL_PTR)
  {
    /* #70 Perform a byte-wise copy of the active workspace to the snapshot. */
    P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) snapshotWorkspace =
      (P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))vStreamProc_GetWorkspaceOfSnapshotSlot(snapshotSlotId);            /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) activeWorkspace =
      (P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR))vStreamProc_GetWorkspaceOfProcessingNode(ProcessingNodeId);      /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
    vStreamProc_WorkspaceSizeOfProcessingNodeClassDefType byteIdx;
    vStreamProc_WorkspaceSizeOfProcessingNodeClassDefType workspaceSize =
      vStreamProc_GetWorkspaceSizeOfProcessingNodeClassDef(
        vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
          vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)));

    for (byteIdx = 0; byteIdx < workspaceSize; byteIdx++)
    {
      snapshotWorkspace[byteIdx] = activeWorkspace[byteIdx];
    }
  }

  /* #80 Queue the snapshot entry. */
  (void)vStreamProc_Queue_PrioInsert(queue, 0u);
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_NamedOutputPortIdType NamedOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType registeredWaveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);
  vStreamProc_WaveIdType waveId =
    vStreamProc_GetWaveStartIdxOfStream(
      vStreamProc_GetStreamIdxOfNamedOutputPort(NamedOutputPortId))
    + registeredWaveLevel;
  vStreamProc_StreamPositionType producePosition  = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);
  vStreamProc_StreamPositionType snapshotInterval =
    vStreamProc_GetSnapshotIntervalOfPortDef(vStreamProc_GetPortDefIdxOfNamedOutputPort(NamedOutputPortId));
  vStreamProc_StreamPositionType newTriggerPosition;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Create the snapshot. */
  vStreamProc_Snapshot_CreateSnapshot(ProcessingNodeId);

  /* #20 Update the trigger position on the corresponding stream. */
  /* Hint: Disable snapshot trigger, if new trigger position exceeds maximum stream length. */
  if (snapshotInterval <= (VSTREAMPROC_MAX_LENGTH - producePosition))
  {
    newTriggerPosition = producePosition + snapshotInterval;
  }
  else
  {
    newTriggerPosition = 0u;
  }

  vStreamProc_SetSnapshotTriggerPositionOfStreamInfo(
    vStreamProc_GetStreamIdxOfNamedOutputPort(NamedOutputPortId), newTriggerPosition);
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateSnapshotWithInputTrigger
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_NamedInputPortIdType NamedInputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType streamOutputPortId =
    vStreamProc_GetStreamOutputPortMapping(vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(NamedInputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Create the snapshot. */
  vStreamProc_Snapshot_CreateSnapshot(ProcessingNodeId);

  /* #20 Check if the input port is active. */
  if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
  {
    vStreamProc_StreamPositionType consumePosition  =
      vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);
    vStreamProc_StreamPositionType snapshotInterval =
      vStreamProc_GetSnapshotIntervalOfPortDef(vStreamProc_GetPortDefIdxOfNamedInputPort(NamedInputPortId));
    vStreamProc_StreamPositionType newTriggerPosition;

    /* Hint: Disable snapshot trigger, if new trigger position exceeds maximum stream length. */
    if (snapshotInterval <= (VSTREAMPROC_MAX_LENGTH - consumePosition))
    {
      newTriggerPosition = consumePosition + snapshotInterval;
    }
    else
    {
      newTriggerPosition = 0u;
    }

    /* #30 If true, update the trigger position on the corresponding stream output port. */
    vStreamProc_SetSnapshotTriggerPositionOfStreamOutputPortInfo(
      streamOutputPortId, newTriggerPosition);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateEntryNodeSnapshot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateEntryNodeSnapshot(
  vStreamProc_ProcessingNodeIdType EntryNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_QueuePtrType queue = vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(EntryNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the snapshot queue is full and delete the newest one if necessary. */
  /* HINT: Oldest snapshot may belong to a previously restored session and shouldn't be discarded. */
  if (vStreamProc_Queue_IsFull(queue))
  {
    (void)vStreamProc_Queue_Remove(queue, vStreamProc_Queue_GetLastUsedHandle(queue));
  }

  /* #20 Create the snapshot. */
  vStreamProc_Snapshot_CreateSnapshot(EntryNodeId);
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateEntryNodeSnapshots
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateEntryNodeSnapshots(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_EntryPointIterType entryPointIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all entry points of the pipe. */
  for (entryPointIdx = vStreamProc_GetEntryPointStartIdxOfPipe(PipeId);
       entryPointIdx < vStreamProc_GetEntryPointEndIdxOfPipe(PipeId);
       entryPointIdx++)
  {
    vStreamProc_ProcessingNodeIdType entryNodeId =
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointIdx));

    vStreamProc_Snapshot_CreateEntryNodeSnapshot(entryNodeId);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_CreateExitNodeSnapshots
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_CreateExitNodeSnapshots(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ExitPointIterType exitPointIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all exit points of the pipe. */
  for (exitPointIdx = vStreamProc_GetExitPointStartIdxOfPipe(PipeId);
       exitPointIdx < vStreamProc_GetExitPointEndIdxOfPipe(PipeId);
       exitPointIdx++)
  {
    vStreamProc_ProcessingNodeIdType exitNodeId =
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
        vStreamProc_GetNamedInputPortIdxOfExitPoint(exitPointIdx));

    vStreamProc_QueuePtrType queue = vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(exitNodeId);

    /* #20 Check if the snapshot queue is full and clear it if necessary. */
    if (vStreamProc_Queue_IsFull(queue))
    {
      (void)vStreamProc_Queue_Remove(queue, vStreamProc_Queue_GetFirstUsedHandle(queue));
    }

    /* #30 Create the snapshot. */
    vStreamProc_Snapshot_CreateSnapshot(exitNodeId);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Snapshot_RemoveObsoleteSnapshots
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Snapshot_RemoveObsoleteSnapshots(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_SnapshotSlotIdType ReferenceSnapshotSlotId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_QueuePtrType queue =
    vStreamProc_Snapshot_GetSnapshotSetQueueByProcessingNodeId(ProcessingNodeId);
  vStreamProc_QueueHandleType queueHandle = vStreamProc_Queue_GetFirstUsedHandle(queue);
  vStreamProc_SnapshotSlotIterType snapshotSlotIdx =
    (vStreamProc_SnapshotSlotIterType)vStreamProc_Queue_GetEntityHandle(queue, queueHandle);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the snapshot queue of the processing node. */
  while (snapshotSlotIdx != ReferenceSnapshotSlotId)
  {
    vStreamProc_QueueHandleType nextQueueHandle = vStreamProc_Queue_GetNextHandle(queue, queueHandle);

    /* #20 Remove the snapshot slot. */
    (void)vStreamProc_Queue_Remove(queue, queueHandle);

    /* #30 Get next snapshot slot. */
    queueHandle = nextQueueHandle;
    snapshotSlotIdx =
      (vStreamProc_SnapshotSlotIterType)vStreamProc_Queue_GetEntityHandle(queue, queueHandle);
  }
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Snapshot.c
 *********************************************************************************************************************/
