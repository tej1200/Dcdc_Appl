/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file vStreamProc_Stream.c
 *        \brief  vStreamProc processing node specific source code file
 *
 *      \details  Implementation of helpers for the vStreamProc processing node modules.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the vStreamProc module. >> vStreamProc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_PORT_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Stream.h"
#include "vStreamProc_Scheduler.h"
#include "vStreamProc_Snapshot.h"
#include "vStreamProc_Limit.h"
#include "vStreamProc.h"

# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
#  include "Det.h"
# endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Stream_InitInputPortHandle
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the content of the provided input port handle with default values.
 * \details       -
 * \param[in]     InputPortHandle     The handle of the input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_InitInputPortHandle(
  vStreamProc_InputPortHandlePtrType InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_InitOutputPortHandle
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the content of the provided output port handle with default values.
 * \details       -
 * \param[in]     OutputPortHandle     The handle of the output port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_InitOutputPortHandle(
  vStreamProc_OutputPortHandlePtrType OutputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_SetDataTypeInfoByStream()
 *********************************************************************************************************************/
/*! \brief          Reads the data type infos from the storage node that belongs to the provided stream and
 *                  writes it to the provided data type info.
 *  \details        -
 *  \param[in]      StreamId  The stream from where the data shall be read.
 *  \param[out]     DataTypeInfo  The data type info structure where the data type infos shall be stored.
 *  \context        TASK
 *  \reentrant      TRUE
 *  \pre            -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SetDataTypeInfoByStream(
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_DataTypeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) DataTypeInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveInfo
 *********************************************************************************************************************/
/*!
 * \brief         Returns the wave info structure for the requested wave level.
 * \details       -
 * \param[in]     PipeId      The id of the pipe
 * \param[in]     StreamId    The stream the wave belongs to.
 * \param[in,out] WaveInfo    The wave info. The wave type symbolic name will be used to select the wave level.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_GetWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelRangeIdle()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the idle wave level range for the provided stream, starting from wave level 0.
 * \details       The wave state of the wave has to be either UNINIT or IDLE to count as idle.
 * \param[in]     StreamId    The stream for which the wave idle range shall be provided.
 * \return        The idle wave level range of the stream     If at least wave level 0 is idle.
 * \return        VSTREAMPROC_NO_WAVELEVEL                    If wave level 0 not idle.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelRangeIdle(
  vStreamProc_StreamIdType StreamId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelRangeStarting()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the starting wave level range for the provided stream, starting from wave level 0.
 * \details       The wave state has to be VSTREAMPROC_WAVESTATE_STARTING to count as starting.
 * \param[in]     StreamId    The stream for which the starting range shall be provided.
 * \return        The idle wave level range of the stream     If at least wave level 0 has wave state
 *                                                            VSTREAMPROC_WAVESTATE_STARTING.
 * \return        VSTREAMPROC_NO_WAVELEVEL                    If wave level 0 is not wave state
 *                                                            VSTREAMPROC_WAVESTATE_STARTING.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelRangeStarting(
  vStreamProc_StreamIdType StreamId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetSourceStreamOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the stream output port at the source input port of the producer node of the provided stream.
 * \details       If no stream output port is provided, there are several possible reasons:
 *                - There is no active producer for the stream.
 *                - There is no wave propagation defined for the producer named output port
 *                - The source input port is not active.
 * \param[in]     StreamId    The stream for which the source stream shall be provided.
 * \return        The stream output port id        If the backward search was successful.
 * \return        VSTREAMPROC_NO_STREAMOUTPUTPORT  Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamOutputPortIdType, AUTOMATIC) vStreamProc_Stream_GetSourceStreamOutputPort(
  vStreamProc_StreamIdType StreamId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortHandleByStreamOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Provides the input port handle of the input port which belongs to the provided stream output port.
 * \details       -
 * \param[in]     StreamOutputPortId     The id of the stream output port.
 * \param[out]    InputPortHandle        Pointer to the input port handle which shall be prepared.
 * \return        VSTREAMPROC_OK         The stream output port is currently mapped to the input port.
 * \return        VSTREAMPROC_FAILED     The stream output port is currently not mapped to the input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_InputPortHandlePtrType InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortHandleByStream()
 *********************************************************************************************************************/
/*!
 * \brief         Provides the output port handle of the output port which is the active producer of the provided stream.
 * \details       -
 * \param[in]     StreamId               The id of the stream.
 * \param[out]    OutputPortHandle       Pointer to the output port handle which shall be prepared.
 * \return        VSTREAMPROC_OK         The stream has an active producer.
 * \return        VSTREAMPROC_FAILED     The stream has no active producer.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortHandleByStream(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_OutputPortHandlePtrType OutputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetMetaDataOfInputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Write the meta data of the provided input port. If the producer wave is not in sync with the
 *                consumer wave, the meta data is filled with default values.
 * \details       -
 * \param[in]     InputPortHandle        Pointer to the input port handle.
 * \param[out]    MetaData               Pointer to the meta data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_GetMetaDataOfInputPort(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType MetaData);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsProducerWaveSync()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the wave at the stream output port of the provided input port is in sync
 *                with the producer wave at the corresponding stream.
 * \details       The waves are in sync if the wave handles are the same and the produce position is or equal than
 *                the consumer position.
 * \param[in]     InputPortHandle     The handle of the input port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsProducerWaveSync(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_UpdateConsumePosition()
 *********************************************************************************************************************/
/*!
 * \brief         Update consume position.
 * \details       -
 * \param[in]     StreamOutputPortId  The processing node's ID.
 * \param[in]     StorageInfo         Reference to storage information.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_UpdateConsumePosition(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_StorageInfoConstPtrType StorageInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostAcknowledgeOutputPortActions()
 *********************************************************************************************************************/
/*!
 * \brief         Triggers actions which shall be performed after the write request to the storage node is finished.
 * \details       -
 * \param[in]     OutputPortHandle    The handle of the output port.
 * \param[in]     WriteRequest        The write request for the output port.
 * \param[in]     LimitResult         The calculated limit result.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_TriggerPostAcknowledgeOutputPortActions(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestConstPtrType WriteRequest,
  vStreamProc_LimitResultType LimitResult);

/**********************************************************************************************************************
 *  vStreamProc_Stream_SyncConsumers()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if any consumers attached to the provided output port are not in sync with the producer wave
 *                and performs the synchronization if necessary.
 * \details       If synchronization is necessary, data which was already consumed before the session was restored
 *                will be discarded.
 * \param[in]     OutputPortHandle    The handle of the output port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SyncConsumers(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsStartWaveAllowed()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluates if a wave can be started in the provided stream and provides the possible wave level range.
 * \details       A wave level can be started if its wave state is either UNINIT or IDLE
 * \param[in]     ProcessingNodeId    The id of the processing node.
 * \param[in]     StreamId            The id of the stream.
 * \param[out]    WaveLevelRangeEnd   The wave level range starting from 0 which is ready to start.
 * \return        TRUE                At least wave level 0 is ready to start.
 * \return        FALSE               Wave level 0 is has a pending wave and starting is not possible.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsStartWaveAllowed(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_WaveLevelType, AUTOMATIC, VSTREAMPROC_APPL_DATA) WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_CopyWaveDataFromSourceWave
 *********************************************************************************************************************/
/*!
 * \brief         Copies the wave data from the source wave to the target wave.
 * \details       -
 * \param[in]     SourceWaveId        Id of the source wave.
 * \param[in]     TargetWaveId        Id of the target wave.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_CopyWaveDataFromSourceWave(
  vStreamProc_WaveIdType SourceWaveId,
  vStreamProc_WaveIdType TargetWaveId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_CopyWaveDataFromWaveInfo
 *********************************************************************************************************************/
/*!
 * \brief         Copies the wave data from the provided wave info structure to the target wave.
 * \details       -
 * \param[in]     TargetWaveId        Id of the target wave.
 * \param[in]     WaveInfo            Pointer to the wave info structure.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_CopyWaveDataFromWaveInfo(
  vStreamProc_WaveIdType TargetWaveId,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_UpdateWaveData()
 *********************************************************************************************************************/
/*!
 * \brief         Updates the wave data in the provided wave level range of the provided stream.
 * \details       The wave data of the registered wave level is updated from the provided wave info structure.
                  For all other levels in the range, the wave data is copied from the mapped source stream.
 * \param[in]     ProcessingNodeId    Id of the processing node.
 * \param[in]     StreamId            Id of the target stream.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range, starting from wave level 0.
 * \param[in]     WaveInfo            Pointer to the wave info structure.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_UpdateWaveData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsWavePropagationAllowed()
 *********************************************************************************************************************/
/*!
 * \brief         Check if all waves in the provided wave level range of the provided stream can be propagated to
 *                the consumer nodes.
 * \details       A wave level can be propagated, if the wave level is idle in the target stream of the
 *                mapped output port of all consumer nodes.
 * \param[in]     StreamId            Id of the stream.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range, starting from wave level 0.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsWavePropagationAllowed(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWaveStart()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the consumer node of the provided stream output port is registered within the
 *                provided wave level range. If true, the WaveStart signal is set at the named input port of
 *                the stream output port.
 * \details       -
 * \param[in]     InputPortHandle     Input port handle of the consumer input port.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range, starting from wave level 0.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWaveStart(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_WaveLevelType WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostWaveStartActions()
 *********************************************************************************************************************/
/*!
 * \brief         Triggers actions which have to be executed after a WaveStart signal is propagated to all
 *                consumer nodes of the provided stream.
 * \details       -
 * \param[in]     StreamId            Id of the stream.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range, starting from wave level 0.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_TriggerPostWaveStartActions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWaveEnd()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the consumer node of the provided stream output port is registered within the
 *                provided wave level range. If true, the WaveEnd signal is set at the named input port of
 *                the stream output port.
 *                If the wave level range contains wave levels above the registered wave level
 *                of the consumer node, the WaveEnd UpperLayer signal is set.
 * \details       -
 * \param[in]     InputPortHandle     Input port handle of the consumer input port.
 * \param[in]     WaveLevelRangeStart Bottom limit of the wave level range.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWaveEnd(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostWaveEndActions()
 *********************************************************************************************************************/
/*!
 * \brief         Triggers actions which have to be executed after a WaveEnd and WaveEndUpperLevel signals are
 *                propagated to all consumer nodes of the provided stream.
 * \details       -
 * \param[in]     StreamId            Id of the stream.
 * \param[in]     WaveLevelRangeStart Bottom limit of the wave level range.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_TriggerPostWaveEndActions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd);

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWave()
 *********************************************************************************************************************/
/*!
 * \brief         Propagates the provided wave signal to the stream output ports of the provided stream.
 *                After propagation, the post propagation actions for the provided signal are triggered.
 * \details       -
 * \param[in]     StreamId            Id of the stream.
 * \param[in]     WaveLevelRangeStart Bottom limit of the wave level range.
 * \param[in]     WaveLevelRangeEnd   Upper limit of the wave level range.
 * \param[in]     WaveSignalId        Id of the wave signal which shall be propagated.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWave(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd,
  vStreamProc_SignalIdType WaveSignalId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_CheckForBlockedWave
 *********************************************************************************************************************/
/*!
 * \brief         Checks the stream of the provided stream output port if there is a blocked wave pending.
 *                If true, the propagation of the blocked wave is tried.
 * \details       -
 * \param[in]     StreamOutputPortId  Id of the stream output port.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_CheckForBlockedWave(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Stream_SetDataTypeInfoByStream()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SetDataTypeInfoByStream(
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_DataTypeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) DataTypeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(StreamId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Read the data type infos from the storage node. */
  DataTypeInfo->Id = vStreamProc_GetDataElementTypeOfStorageNode(storageNodeId);
  DataTypeInfo->Size = vStreamProc_GetDataElementSizeOfStorageNode(storageNodeId);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_GetWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, WaveInfo->WaveTypeSymbolicName);
  vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(StreamId) + (vStreamProc_WaveIdType)waveLevel;
  vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);

  /* ----- Implementation ----------------------------------------------- */
  WaveInfo->State = waveState;

  /* #10 Check if the wave state is UNINIT. */
  if (waveState == VSTREAMPROC_WAVESTATE_UNINIT)
  {
    /* #20 Provide default values. */
    WaveInfo->AbsoluteStreamPosition = 0u;
    WaveInfo->RelativeStreamPosition = 0u;
    WaveInfo->Handle = 0u;
  }
  else
  {
    /* #30 Get wave information. */
    WaveInfo->AbsoluteStreamPosition = vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId);
    WaveInfo->RelativeStreamPosition = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);
    WaveInfo->Handle = vStreamProc_GetHandleOfWaveVarInfo(waveId);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelRangeIdle()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelRangeIdle(
  vStreamProc_StreamIdType StreamId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveIterType waveStartIdx = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveIterType waveIdx;
  vStreamProc_WaveLevelType waveLevelRangeEnd = VSTREAMPROC_NO_WAVELEVEL;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all wave levels to define the wave level range. */
  for (waveIdx = waveStartIdx;
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(StreamId);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Check if the wave level is UNINIT/IDLE. */
    if ((waveState == VSTREAMPROC_WAVESTATE_UNINIT) || (waveState == VSTREAMPROC_WAVESTATE_IDLE))
    {
      if (waveIdx == waveStartIdx)
      {
        waveLevelRangeEnd = 0u;
      }
      else
      {
        waveLevelRangeEnd++;
      }
    }
    else
    {
      break;
    }
  }

  return waveLevelRangeEnd;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelRangeStarting()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelRangeStarting(
  vStreamProc_StreamIdType StreamId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveIterType waveStartIdx = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveIterType waveIdx;
  vStreamProc_WaveLevelType waveLevelRangeEnd = VSTREAMPROC_NO_WAVELEVEL;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all wave levels to define the wave level range. */
  for (waveIdx = waveStartIdx;
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(StreamId);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Check if the wave level is STARTING. */
    if (waveState == VSTREAMPROC_WAVESTATE_STARTING)
    {
      if (waveIdx == waveStartIdx)
      {
        waveLevelRangeEnd = 0u;
      }
      else
      {
        waveLevelRangeEnd++;
      }
    }
    else
    {
      break;
    }
  }

  return waveLevelRangeEnd;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetSourceStreamOutputPort()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamOutputPortIdType, AUTOMATIC) vStreamProc_Stream_GetSourceStreamOutputPort(
  vStreamProc_StreamIdType StreamId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType sourceStreamOutputPortId = VSTREAMPROC_NO_STREAMOUTPUTPORT;
  vStreamProc_NamedOutputPortIdType producerNamedOutputPortId = vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is an active producer for the stream. */
  if (producerNamedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
  {
    vStreamProc_NamedInputPortIdType producerNamedInputPortId =
      vStreamProc_GetNamedInputPortIdxOfNamedOutputPort(producerNamedOutputPortId);

    /* #20 Check if there is a wave routing path defined. */
    if (producerNamedInputPortId != VSTREAMPROC_NO_NAMEDINPUTPORT)
    {
      /* #30 Return the source stream output port. */
      sourceStreamOutputPortId =
        vStreamProc_GetStreamOutputPortMapping(
          vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(
            producerNamedInputPortId));
    }
  }

  return sourceStreamOutputPortId;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortHandleByStreamOutputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_InputPortHandlePtrType InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_NamedInputPortIdType namedInputPortId =
    vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(StreamOutputPortId);
  vStreamProc_StreamOutputPortIdType mappedStreamOutputPortId =
    vStreamProc_GetStreamOutputPortMapping(
      vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the stream output port is connected to the named input port. */
  if (mappedStreamOutputPortId == StreamOutputPortId)
  {
    /* #20 Get the input port handle. */
    retVal = vStreamProc_Stream_GetInputPortHandle(
      vStreamProc_GetProcessingNodeIdxOfNamedInputPort(namedInputPortId),
      vStreamProc_GetInputPortSymbolicNameOfPortDef(vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortId)),
      InputPortHandle);
  }
  /* #30 Otherwise: */
  else
  {
    /* #40 Init the input port handle with default values. */
    vStreamProc_Stream_InitInputPortHandle(InputPortHandle);
    retVal = VSTREAMPROC_FAILED;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortHandleByStream()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortHandleByStream(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_OutputPortHandlePtrType OutputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_NamedOutputPortIdType namedOutputPortId =
    vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the stream has an active producer. */
  if (namedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
  {
    /* #20 Get the output port handle. */
    retVal = vStreamProc_Stream_GetOutputPortHandle(
      vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(namedOutputPortId),
      vStreamProc_GetOutputPortSymbolicNameOfPortDef(vStreamProc_GetPortDefIdxOfNamedOutputPort(namedOutputPortId)),
      OutputPortHandle);
  }
  /* #30 Otherwise: */
  else
  {
    /* #40 Init the output port handle with default values. */
    vStreamProc_Stream_InitOutputPortHandle(OutputPortHandle);
    retVal = VSTREAMPROC_FAILED;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetMetaDataOfInputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_GetMetaDataOfInputPort(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType MetaData)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(InputPortHandle->ProcessingNodeId);
  vStreamProc_WaveTypeDefIdType waveTypeDefId =
    vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
      vStreamProc_GetWaveLevelDefStartIdxOfPipe(pipeId) + InputPortHandle->WaveLevel);
  vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(InputPortHandle->WaveId);
  vStreamProc_WaveHandleType consumerWaveHandle =
    vStreamProc_GetWaveHandleOfStreamOutputPortInfo(InputPortHandle->StreamOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if valid meta data is available. */
  if ((producerWaveHandle == consumerWaveHandle) && (producerWaveHandle > 0u))
  {
    /* #20 Query the meta data. */
    vStreamProc_Stream_GetMetaData(pipeId, InputPortHandle->StreamId, waveTypeDefId, MetaData);
  }
  /* #30 Otherwise: */
  else
  {
    /* #40 Provide default values. */
    vStreamProc_Stream_InitStorageInfo(&MetaData->StorageInfo);
    MetaData->Buffer = NULL_PTR;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsProducerWaveSync()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsProducerWaveSync(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
  vStreamProc_WaveIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(waveId);
  vStreamProc_WaveHandleType consumerWaveHandle = vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId);
  vStreamProc_StreamPositionType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);
  vStreamProc_StreamPositionType consumePosition =
    vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the producer and consumer wave are in sync. */
  if (InputPortHandle->IsConnected)
  {
    if ((producerWaveHandle == consumerWaveHandle) && (producePosition >= consumePosition))
    {
      retVal = TRUE;
    }
  }
  else
  {
    retVal = TRUE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_UpdateConsumePosition()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_UpdateConsumePosition(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_StorageInfoConstPtrType StorageInfo)
{
  vStreamProc_ConsumePositionOfStreamOutputPortInfoType currentConsumePosition =
    vStreamProc_GetConsumePositionOfStreamOutputPortInfo(StreamOutputPortId);

  /* #10 Update consume position. */
  currentConsumePosition += StorageInfo->RequestLength;
  vStreamProc_SetConsumePositionOfStreamOutputPortInfo(StreamOutputPortId, currentConsumePosition);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_InitInputPortHandle
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_InitInputPortHandle(
  vStreamProc_InputPortHandlePtrType InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the input port handle with default values. */
  InputPortHandle->ProcessingNodeId = VSTREAMPROC_NO_PROCESSINGNODE;
  InputPortHandle->InputPortSymbolicName = 0u;
  InputPortHandle->NamedInputPortId = VSTREAMPROC_NO_NAMEDINPUTPORT;
  InputPortHandle->PortDefId = VSTREAMPROC_NO_PORTDEF;
  InputPortHandle->StreamOutputPortId = VSTREAMPROC_NO_STREAMOUTPUTPORT;
  InputPortHandle->StreamId = VSTREAMPROC_NO_STREAM;
  InputPortHandle->StorageOutputPortId = VSTREAMPROC_NO_STORAGEOUTPUTPORT;
  InputPortHandle->StorageNodeId = VSTREAMPROC_NO_STORAGENODE;
  InputPortHandle->WaveLevel = VSTREAMPROC_NO_WAVELEVEL;
  InputPortHandle->WaveId = VSTREAMPROC_NO_WAVE;
  InputPortHandle->IsConnected = FALSE;
  InputPortHandle->IsVirtual = FALSE;
  InputPortHandle->HasActiveProducer = FALSE;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_InitOutputPortHandle
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(void, AUTOMATIC) vStreamProc_Stream_InitOutputPortHandle(
  vStreamProc_OutputPortHandlePtrType OutputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the output port handle with default values. */
  OutputPortHandle->ProcessingNodeId = VSTREAMPROC_NO_PROCESSINGNODE;
  OutputPortHandle->OutputPortSymbolicName = 0u;
  OutputPortHandle->NamedOutputPortId = VSTREAMPROC_NO_NAMEDOUTPUTPORT;
  OutputPortHandle->PortDefId = VSTREAMPROC_NO_PORTDEF;
  OutputPortHandle->StorageNodeId = VSTREAMPROC_NO_STORAGENODE;
  OutputPortHandle->StreamId = VSTREAMPROC_NO_STREAM;
  OutputPortHandle->WaveId = VSTREAMPROC_NO_WAVE;
  OutputPortHandle->WaveLevel = VSTREAMPROC_NO_WAVELEVEL;
  OutputPortHandle->IsVirtual = FALSE;
  OutputPortHandle->IsConnected = FALSE;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostAcknowledgeOutputPortActions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_TriggerPostAcknowledgeOutputPortActions(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestConstPtrType WriteRequest,
  vStreamProc_LimitResultType LimitResult)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StorageInfoConstPtrType writeRequestStorageInfo = &WriteRequest->StorageInfo;
  vStreamProc_ProcessingNodeIdType    processingNodeId = OutputPortHandle->ProcessingNodeId;
  vStreamProc_NamedOutputPortIdType   namedOutputPortId = OutputPortHandle->NamedOutputPortId;
  vStreamProc_StreamIdType            streamId = OutputPortHandle->StreamId;
  vStreamProc_LengthType              producedLength = writeRequestStorageInfo->RequestLength;
  vStreamProc_StreamPositionType      triggerPosition = vStreamProc_GetSnapshotTriggerPositionOfStreamInfo(streamId);
  vStreamProc_StreamPositionType      producePosition;
  vStreamProc_WaveIterType            waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Track copy of write lock. */
  vStreamProc_SetWriteRequestLockOfStreamInfo(streamId, FALSE);

  /* #20 Iterate over all waves and update the produce position. */
  for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamId);
       waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamId);
       waveIdx++)
  {
    vStreamProc_ProducePositionOfWaveVarInfoType currentProducePosition =
      vStreamProc_GetProducePositionOfWaveVarInfo(waveIdx);

    /* #30 Update produce position. */
    currentProducePosition += producedLength;
    vStreamProc_SetProducePositionOfWaveVarInfo(waveIdx, currentProducePosition);
  }

  /* #40 Trigger DataAvailable signals */
  if (producedLength > 0u)
  {
    vStreamProc_Stream_HandleDataAvailable(streamId);
  }

  /* #50 Check if snapshot is triggered. */
  producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(OutputPortHandle->WaveId);

  if ((triggerPosition > 0u)
    && (producePosition >= triggerPosition))
  {
    /* #60 If true, set the snapshot signal at the output port. */
    if (vStreamProc_Scheduler_SetOutputPortSignal(
      namedOutputPortId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_OutputSnapshotRequested) == E_NOT_OK)
    {
      /* #70 If there is no signal handler or the signal handler is unregistered,
             start snapshot creation directly. */
      vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(processingNodeId, namedOutputPortId);
    }
  }

  /* #80 If soft limit was reached: Trigger limit. */
  vStreamProc_Limit_TriggerAcknowledgeLimit(streamId, LimitResult);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_SyncConsumers()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SyncConsumers(                                        /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamIdType streamId = OutputPortHandle->StreamId;
  vStreamProc_StreamOutputPortIterType streamOutputPortIdx;
  boolean allConsumersVirtual = TRUE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Loop over all stream output ports and check if there is any active data consumer present. */
  for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(streamId);
       streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(streamId);
       streamOutputPortIdx++)
  {
    vStreamProc_InputPortHandleType inputPortHandle;

    (void)vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
      (vStreamProc_StreamOutputPortIdType)streamOutputPortIdx,
      &inputPortHandle);

    /* #20 Check if the node has to be analyzed. */
    if (inputPortHandle.IsConnected == TRUE)
    {
      if (inputPortHandle.IsVirtual == FALSE)
      {
        vStreamProc_LengthType availableDataLength;
        vStreamProc_ReadRequestType readRequest;

        allConsumersVirtual = FALSE;

        /* #30 Query the available data from the storage node. */
        vStreamProc_Stream_InitStorageInfo(&readRequest.StorageInfo);

        (void)vStreamProc_CallReadInfoOfStorageNode(
          inputPortHandle.StorageNodeId,
          &readRequest.StorageInfo,
          inputPortHandle.StorageOutputPortId);

        availableDataLength = readRequest.StorageInfo.AvailableLength;

        /* #40 Check if there is data available. */
        if (availableDataLength > 0u)
        {
          vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(inputPortHandle.WaveId);
          vStreamProc_WaveHandleType consumerWaveHandle =
            vStreamProc_GetWaveHandleOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId);
          vStreamProc_LengthType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(inputPortHandle.WaveId);
          vStreamProc_LengthType consumePositon =
            vStreamProc_GetConsumePositionOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId);

          vStreamProc_LengthType discardDataLength;

          /* #50 Determine the discard length */
          /* Discard no data per default */
          discardDataLength = 0;

          /* Discard all data if the producer wave is still behind the consumer wave. */
          if ( (producerWaveHandle < consumerWaveHandle)
            || ((producerWaveHandle == consumerWaveHandle) && (producePosition <= consumePositon)))
          {
            discardDataLength = availableDataLength;
          }

          /* Discard data partly if position offset and available data are inconsistent. */
          /* Hint: This might be the case if the producer just reached the consume position. */
          if ((producerWaveHandle == consumerWaveHandle) && (producePosition > consumePositon))
          {
            vStreamProc_LengthType positionOffset = producePosition - consumePositon;

            if (positionOffset < availableDataLength)
            {
              discardDataLength = availableDataLength - positionOffset;
            }
          }

          /* #60 Discard data if necessary. */
          if (discardDataLength > 0u)
          {
            readRequest.StorageInfo.RequestLength = discardDataLength;

            (void)vStreamProc_CallReadRequestOfStorageNode(
              inputPortHandle.StorageNodeId,
              &readRequest,
              inputPortHandle.StorageOutputPortId);

            (void)vStreamProc_CallReadAckOfStorageNode(
              inputPortHandle.StorageNodeId,
              discardDataLength,
              inputPortHandle.StorageOutputPortId);
          }
        }
      }
    }
  }

  /* #70 Check if all data has to be discarded */
  if ( (allConsumersVirtual == TRUE)
    && (vStreamProc_GetStreamOutputPortStartIdxOfStream(streamId) != VSTREAMPROC_NO_STREAMOUTPUTPORT))
  {
    vStreamProc_CallDiscardAllDataOfStorageNode(OutputPortHandle->StorageNodeId);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsStartWaveAllowed()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsStartWaveAllowed(                                       /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_StreamIdType StreamId,
  P2VAR(vStreamProc_WaveLevelType, AUTOMATIC, VSTREAMPROC_APPL_DATA) WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType targetStreamIdleWaveLevelRange = 0u;
  boolean startWaveAllowed = FALSE;
  vStreamProc_WaveLevelType pipeWaveLevelRange =
    vStreamProc_GetWaveLevelDefLengthOfPipe(
      vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the pipe is a single wave level pipe. */
  if (pipeWaveLevelRange == 1u)
  {
    vStreamProc_WaveStateType waveState =
      vStreamProc_GetStateOfWaveVarInfo(
        vStreamProc_GetWaveStartIdxOfStream(StreamId));

    if ((waveState == VSTREAMPROC_WAVESTATE_UNINIT) || (waveState == VSTREAMPROC_WAVESTATE_IDLE))
    {
      startWaveAllowed = TRUE;
    }
  }
  /* #20 Otherwise: */
  else
  {
    boolean producerIsEntryNode = vStreamProc_AccessNode_IsEntryNode(ProcessingNodeId);

    /* #30 In case the node is not an entry node, check the stream to define wave level range. */
    if (producerIsEntryNode == FALSE)
    {
      /* Hint: Start wave is possible if
                - The wave levels equal and lower than the registered levels are idle.
                - If there are additional wave levels above the registered wave level to start,
                  these levels have to be idle in the target stream as well. */
      vStreamProc_WaveLevelType registeredWaveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);

      targetStreamIdleWaveLevelRange = vStreamProc_Stream_GetWaveLevelRangeIdle(StreamId);

      /* #40 Check if the wave range is higher than the registered level. */
      /* Hint: All level below the registered level need to be ready before a start wave can be performed. */
      if ((targetStreamIdleWaveLevelRange != VSTREAMPROC_NO_WAVELEVEL)
        && (targetStreamIdleWaveLevelRange >= registeredWaveLevel))
      {
        /* #50 Check if the registered wave level is the top wave level. */
        if (registeredWaveLevel == pipeWaveLevelRange)
        {
          startWaveAllowed = TRUE;
        }
        /* #60 Otherwise:*/
        else
        {
          vStreamProc_WaveIterType waveOffset = (vStreamProc_WaveIterType)registeredWaveLevel + 1u;
          vStreamProc_WaveIterType targetWaveIdx =
            vStreamProc_GetWaveStartIdxOfStream(StreamId) + waveOffset;
          /* Hint: In this context, the returned source stream output port is always valid. */
          vStreamProc_StreamOutputPortIdType sourceStreamOutputPort =
            vStreamProc_Stream_GetSourceStreamOutputPort(StreamId);
          vStreamProc_StreamIdType sourceStreamId = vStreamProc_GetStreamIdxOfStreamOutputPort(sourceStreamOutputPort);
          vStreamProc_WaveIterType sourceWaveUpperLevelStartIdx =
            vStreamProc_GetWaveStartIdxOfStream(sourceStreamId) + waveOffset;
          vStreamProc_WaveIterType sourceWaveIdx;
          vStreamProc_WaveLevelType startingWaveLevelRange = registeredWaveLevel;

          /* #70 Check the higher wave levels in the source stream. */
          for(sourceWaveIdx = sourceWaveUpperLevelStartIdx;
              sourceWaveIdx < vStreamProc_GetWaveEndIdxOfStream(sourceStreamId);
              sourceWaveIdx++)
          {
            /* #80 Compare the wave handle. */
            if (vStreamProc_GetHandleOfWaveVarInfo(sourceWaveIdx) == vStreamProc_GetHandleOfWaveVarInfo(targetWaveIdx))
            {
              break;
            }
            else
            {
              targetWaveIdx++;
              startingWaveLevelRange++;
            }
          }

          /* Hint: If the starting wave level is higher, it means the wave end on the target stream is not yet
                   finished. */
          if (startingWaveLevelRange == targetStreamIdleWaveLevelRange)
          {
            startWaveAllowed = TRUE;
          }
        }
      }
    }
    /* #90 For entry nodes, just define the wave level range. */
    /* Hint: Entry nodes call StartWave only the lowest level. For all higher wave level starts before,
             the meta data was already written and the state was set to STARTING. */
    else
    {
      /* #100 Set the STARTING flag on the lowest wave level to get the correct wave level range. */
      /* Hint: The level is started anyway in the next step. */
      vStreamProc_SetStateOfWaveVarInfo(
        vStreamProc_GetWaveStartIdxOfStream(StreamId),
        VSTREAMPROC_WAVESTATE_STARTING);
      targetStreamIdleWaveLevelRange = vStreamProc_Stream_GetWaveLevelRangeStarting(StreamId);
      startWaveAllowed = TRUE;
    }
  }

  *WaveLevelRangeEnd = targetStreamIdleWaveLevelRange;

  return startWaveAllowed;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_CopyWaveDataFromSourceWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_CopyWaveDataFromSourceWave(
  vStreamProc_WaveIdType SourceWaveId,
  vStreamProc_WaveIdType TargetWaveId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveTypeDefIdType sourceWaveTypeDef = vStreamProc_GetWaveTypeDefIdxOfWave(SourceWaveId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy the position info. */
  vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(
    TargetWaveId,
    vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(SourceWaveId));
  vStreamProc_SetProducePositionOfWaveVarInfo(TargetWaveId, 0u);

  /* #20 Copy the wave handle. */
  vStreamProc_SetHandleOfWaveVarInfo(
    TargetWaveId,
    vStreamProc_GetHandleOfWaveVarInfo(SourceWaveId));

  /* #30 Check if the wave type has meta data. */
  if (vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(sourceWaveTypeDef) != VSTREAMPROC_NO_DATATYPE)
  {
    P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_CONST) targetBuffer = vStreamProc_GetMetaDataPointerOfWave(TargetWaveId);
    P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_CONST) sourceBuffer = vStreamProc_GetMetaDataPointerOfWave(SourceWaveId);
    vStreamProc_StorageNodeBufferIterType bufferIdx;
    vStreamProc_LengthType metaDataLength = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(sourceWaveTypeDef);

    /* #40 Copy the meta data. */
    for (bufferIdx = 0u;
         bufferIdx < metaDataLength;
         bufferIdx++)
    {
      targetBuffer[bufferIdx] = sourceBuffer[bufferIdx];
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_CopyWaveDataFromWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_CopyWaveDataFromWaveInfo(
  vStreamProc_WaveIdType TargetWaveId,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy the position info. */
  vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(TargetWaveId, WaveInfo->AbsoluteStreamPosition);
  vStreamProc_SetProducePositionOfWaveVarInfo(TargetWaveId, 0u);

  /* #20 Check if the meta data has to be copied. */
  if (WaveInfo->MetaData.StorageInfo.RequestLength == 1u)
  {
    P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_CONST) targetBuffer = vStreamProc_GetMetaDataPointerOfWave(TargetWaveId);
    P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_CONST) sourceBuffer = WaveInfo->MetaData.Buffer;

    if ((sourceBuffer != NULL_PTR) && (targetBuffer != NULL_PTR))
    {
      vStreamProc_LengthType metaDataLength =
        vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(WaveInfo->WaveTypeSymbolicName);
      vStreamProc_StorageNodeBufferIterType bufferIdx;

      /* #30 Copy the meta data from the waveInfo to the target wave. */
      for (bufferIdx = 0u;
           bufferIdx < metaDataLength;
           bufferIdx++)
      {
        targetBuffer[bufferIdx] = sourceBuffer[bufferIdx];
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_UpdateWaveData()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_UpdateWaveData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType registeredWaveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);
  vStreamProc_WaveIterType registeredWaveId =
    vStreamProc_GetWaveStartIdxOfStream(StreamId) + (vStreamProc_WaveIterType)registeredWaveLevel;
  boolean producerIsEntryNode = vStreamProc_AccessNode_IsEntryNode(ProcessingNodeId);
  vStreamProc_WaveIterType waveStartIdx = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveIterType waveIdx;
  vStreamProc_WaveIterType sourceWaveIdx = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy the wave data from the wave info. */
  vStreamProc_Stream_CopyWaveDataFromWaveInfo((vStreamProc_WaveIdType)registeredWaveId, WaveInfo);

  /* #20 Update the wave handle. */
  /* Hint: If the producer is an entry node, the (external) handle must not be used.
           Instead, the current handle is incremented. */
  if (producerIsEntryNode == TRUE)
  {
    vStreamProc_IncHandleOfWaveVarInfo(registeredWaveId);
  }
  else
  {
    vStreamProc_SetHandleOfWaveVarInfo(registeredWaveId, WaveInfo->Handle);
  }

  /* #30 Set the wave state to STARTING */
  vStreamProc_SetWaveStateFlagOfWaveId(
    registeredWaveId,
    VSTREAMPROC_WAVESTATE_STARTING);

  /* #40 Update the other wave levels from the source stream. */
  /* Hint: The wave levels at entry nodes are updated explicitly from outside. For all other nodes, StartWave is only
           called once for the registered level and the data of the other wave levels has to be copied here from the
           source wave. */
  if (producerIsEntryNode == FALSE)
  {
    sourceWaveIdx =
      vStreamProc_GetWaveStartIdxOfStream(
        vStreamProc_GetStreamIdxOfStreamOutputPort(
          vStreamProc_GetStreamOutputPortMapping(
            vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(
              vStreamProc_GetNamedInputPortIdxOfNamedOutputPort(
                vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId))))));

    /* #50 Iterate over all wave levels to define the wave level range and prepare the wave data. */
    for (waveIdx = waveStartIdx;
         waveIdx <= (waveStartIdx + WaveLevelRangeEnd);
         waveIdx++)
    {
      /* #60 Check if the wave level has to be started. */
      /* Hint: The wave data of the registered wave level was already updated with the waveInfo. */
      if (waveIdx != registeredWaveId)
      {
        /* #70 Copy the wave data and set the starting flag. */
        vStreamProc_Stream_CopyWaveDataFromSourceWave(
          (vStreamProc_WaveIdType)sourceWaveIdx,
          (vStreamProc_WaveIdType)waveIdx);

        vStreamProc_SetWaveStateFlagOfWaveId(
          waveIdx,
          VSTREAMPROC_WAVESTATE_STARTING);

        /* #80 Overwrite the absolute position. */
        vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(waveIdx, WaveInfo->AbsoluteStreamPosition);
      }

      sourceWaveIdx++;
    }
  }
  else
  {
    /* #90 Iterate over all higher wave levels and overwrite the absolute position. */
    for (waveIdx = (waveStartIdx + 1u);
         waveIdx <= (waveStartIdx + WaveLevelRangeEnd);
         waveIdx++)
    {
      vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(waveIdx, WaveInfo->AbsoluteStreamPosition);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsWavePropagationAllowed()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsWavePropagationAllowed(                                 /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIterType streamOutputPortIdx;
  boolean wavePropagationAllowed = TRUE;

  /* ----- Implementation ----------------------------------------------- */
  for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(StreamId);
       streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(StreamId);
       streamOutputPortIdx++)
  {
    vStreamProc_InputPortHandleType inputPorthandle;

    (void)vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
      (vStreamProc_StreamOutputPortIdType)streamOutputPortIdx,
      &inputPorthandle);

    if (inputPorthandle.IsConnected == TRUE)
    {
      /* #10 Check if the wave level range is equal or higher than the level of the consumer node. */
      /* Hint: Otherwise, wave propagation is implicitly allowed as it won't lead to any signals for this node. */
      if (WaveLevelRangeEnd >= inputPorthandle.WaveLevel)
      {
        vStreamProc_WaveRoutingIterType waveRoutingIdx =
          vStreamProc_GetWaveRoutingIdxOfNamedInputPort(inputPorthandle.NamedInputPortId);

        /* #20 Check if there is a wave propagation defined for the named input port. */
        /* Hint: Otherwise, no follow stream has to be considered and the propagation is allowed. */
        if (waveRoutingIdx != VSTREAMPROC_NO_WAVEROUTINGIDXOFNAMEDINPUTPORT)
        {
          vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(inputPorthandle.WaveId);
          vStreamProc_WaveHandleType consumerWaveHandle =
            vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortIdx);

          /* #30 Check if producer wave and consumer wave are in sync */
          /* Hint: If the consumer wave handle is equal or higher, the waves are not in sync yet and the
                   wave propagation is generally allowed, as the synchronization will be performed during the actual
                   wave propagation. */
          if (producerWaveHandle > consumerWaveHandle)
          {
            vStreamProc_NamedOutputPortIndIterType namedOutputPortIndIdx;

            for (namedOutputPortIndIdx = vStreamProc_GetNamedOutputPortIndStartIdxOfWaveRouting(waveRoutingIdx);
                 namedOutputPortIndIdx < vStreamProc_GetNamedOutputPortIndEndIdxOfWaveRouting(waveRoutingIdx);
                 namedOutputPortIndIdx++)
            {
              vStreamProc_OutputPortSymbolicNameType outputPortSymbolicName =
                vStreamProc_GetOutputPortSymbolicNameOfPortDef(
                  vStreamProc_GetPortDefIdxOfNamedOutputPort(
                    vStreamProc_GetNamedOutputPortInd(namedOutputPortIndIdx)));
              vStreamProc_OutputPortHandleType outputPortHandle;

              (void)vStreamProc_Stream_GetOutputPortHandle(
                inputPorthandle.ProcessingNodeId,
                outputPortSymbolicName,
                &outputPortHandle);

              /* #40 Check if the output port is connected and active. */
              /* Hint: If the port is unconnected/inactive, it is not relevant for the wave propagation. */
              if (outputPortHandle.IsConnected == TRUE)
              {
                vStreamProc_WaveLevelType idleWaveLevelRange =
                  vStreamProc_Stream_GetWaveLevelRangeIdle(outputPortHandle.StreamId);

                /* #50 Check if the wave level range higher than the idle range in the target stream. */
                /* Hint: This means that it is not possible to start the wave range in the target stream. */
                if ((idleWaveLevelRange == VSTREAMPROC_NO_WAVELEVEL)
                  || (WaveLevelRangeEnd > idleWaveLevelRange))
                {
                  wavePropagationAllowed = FALSE;
                  break;
                }
              }
            }
          }
        }
      }

      if (wavePropagationAllowed == FALSE)
      {
        break;
      }
    }
  }

  return wavePropagationAllowed;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWaveStart()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWaveStart(                                          /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_WaveLevelType WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType consumerWaveLevel = InputPortHandle->WaveLevel;
  vStreamProc_WaveIdType registeredConsumerWaveId = InputPortHandle->WaveId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the consumer node is affected. */
  /* Hint: If the consumer wave level is equal or lower, the signal can be always set because
           the wave propagation is only started if all levels from the producer level to the lowest level
           are ready to start.
           If the consumer wave level is higher, there might be already an active wave, and the signal
           must not be set again. */
  if ( (consumerWaveLevel <= WaveLevelRangeEnd)
    || (!vStreamProc_IsWaveStateFlagSetOfWaveId(registeredConsumerWaveId, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE)))
  {
    vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
    vStreamProc_ProcessingNodeIdType consumerNodeId = InputPortHandle->ProcessingNodeId;
    vStreamProc_NamedOutputPortIdType consumerNamedInputPortId = InputPortHandle->NamedInputPortId;
    vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(registeredConsumerWaveId);
    vStreamProc_WaveHandleType consumerWaveHandle = vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId);
    vStreamProc_SnapshotIntervalOfPortDefType snapshotInterval;

    /* #20 Check if the producer wave handle is higher than the consumer wave handle. */
    /* Hint: Default use case, the wave handle was incremented on producer side but not yet on consumer side. */
    if (producerWaveHandle > consumerWaveHandle)
    {
      /* #30 Set wave start signal for the consumer node. */
      /* Hint: WaveStart is mandatory for all processing nodes, no need to consider the return value. */
      (void)vStreamProc_Scheduler_SetInputPortSignal(
        consumerNamedInputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart);

      /* #40 Set STARTING and ACTIVE wave state flag on consumer side. */
      vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortId, VSTREAMPROC_WAVESTATE_STARTING_ACTIVE);

      /* #50 Increase the pending wave end count on consumer wave level. */
      vStreamProc_IncPendingWaveStartCountOfWaveVarInfo(registeredConsumerWaveId);

      /* #60 Increase the overall pending wave count in the pipe. */
      vStreamProc_IncActiveWaveCountOfPipeInfo(vStreamProc_GetPipeIdxOfProcessingNode(consumerNodeId));

      /* #70 Set the consumer wave handle to the producer wave handle */
      vStreamProc_SetWaveHandleOfStreamOutputPortInfo(streamOutputPortId, producerWaveHandle);

      /* #80 Set the consumer wave absolute position to the producer absolute position */
      vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(
        streamOutputPortId,
        vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(registeredConsumerWaveId));

      /* #90 Reset the consume position of the stream output port */
      vStreamProc_SetConsumePositionOfStreamOutputPortInfo(streamOutputPortId, 0u);

      /* #100 Set a InputSnapshotRequest Signal if the consumer node has a defined snapshot interval. */
      snapshotInterval =
        vStreamProc_GetSnapshotIntervalOfPortDef(
          vStreamProc_GetPortDefIdxOfNamedInputPort(consumerNamedInputPortId));

      if (snapshotInterval != VSTREAMPROC_NO_SNAPSHOTINTERVALOFPORTDEF)
      {
        /* #110 If true, set the snapshot signal at the input port. */
        if (vStreamProc_Scheduler_SetInputPortSignal(
          consumerNamedInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_InputSnapshotRequested) == E_NOT_OK)
        {
          /* #120 If there is no signal handler or the signal handler is unregistered,
                 start snapshot creation directly. */
          vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(consumerNodeId, consumerNamedInputPortId);
        }
      }
    }
    /* #130 Otherwise: */
    /* Hint: If the wave handle of the producer is equal or lower, the pipe was restored. */
    else
    {
      /* #140 Check if the wave handles are equal. */
      /* Hint: If the wave handles are equal, the producer reached the consumer wave from the restored session.
               If the producer wave handle is still lower, the consumer must not be triggered at all. */
      if (producerWaveHandle == consumerWaveHandle)
      {
        vStreamProc_WaveStateType consumerWaveState = vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId);

        /* #150 Check if the consumer did not acknowledge the WaveStart before. */
        if (vStreamProc_IsWaveStateFlagSet(consumerWaveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING))
        {
          /* #160 Set WaveStart */
          (void)vStreamProc_Scheduler_SetInputPortSignal(
            consumerNamedInputPortId,
            vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart);
        }
        else
        {
          /* Hint: If the consumer wave state is IDLE, the wave was already finished and the consumer must not be
                   triggered again. */
          if (consumerWaveState != VSTREAMPROC_WAVESTATE_IDLE)
          {
            /* #170 Set WaveResume */
            (void)vStreamProc_Scheduler_SetInputPortSignal(
              consumerNamedInputPortId,
              vStreamProcConf_vStreamProcSignal_vStreamProc_WaveResume);
          }
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostWaveStartActions()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_TriggerPostWaveStartActions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_NamedOutputPortIdType namedOutputPortId =
    vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(namedOutputPortId);
  vStreamProc_SnapshotIntervalOfPortDefType snapshotInterval =
    vStreamProc_GetSnapshotIntervalOfPortDef(
      vStreamProc_GetPortDefIdxOfNamedOutputPort(namedOutputPortId));
  vStreamProc_WaveIterType waveStartIdx = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveIterType waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all waves and update wave state. */
  for (waveIdx = waveStartIdx;
       waveIdx <= (waveStartIdx + WaveLevelRangeEnd);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Set active flag */
    vStreamProc_SetWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);

    /* #30 Clear starting flag if there are no pending wave start signals. */
    if (vStreamProc_GetPendingWaveStartCountOfWaveVarInfo(waveIdx) == 0u)
    {
      vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING);
    }

    /* #40 Write wave state back */
    vStreamProc_SetStateOfWaveVarInfo(waveIdx, waveState);
  }

  /* #50 Set the OutputSnapshotRequest Signal if the node has a defined snapshot interval. */
  if (snapshotInterval != VSTREAMPROC_NO_SNAPSHOTINTERVALOFPORTDEF)
  {
    /* #60 If true, set the snapshot signal at the output port. */
    if (vStreamProc_Scheduler_SetOutputPortSignal(
      namedOutputPortId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_OutputSnapshotRequested) == E_NOT_OK)
    {
      /* #70 If there is no signal handler or the signal handler is unregistered,
             start snapshot creation directly. */
      vStreamProc_Snapshot_CreateSnapshotWithOutputTrigger(
        processingNodeId,
        namedOutputPortId);
    }
  }

  /* #80 Check if wave was started with active limit. */
  vStreamProc_Limit_EvaluateWaveStartLimit(StreamId);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWaveEnd()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWaveEnd(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_WaveIdType registeredConsumerWaveId = InputPortHandle->WaveId;
  vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(registeredConsumerWaveId);
  vStreamProc_WaveHandleType consumerWaveHandle = vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the wave handle is the same on producer and on consumer side */
  /* Hint: If the consumer wave handle is not the same, the pipe is getting restored, and the consumer snapshot already
           contained a future wave. In this case, the WaveEnd must not be propagated again. */
  if (producerWaveHandle == consumerWaveHandle)
  {
    vStreamProc_NamedOutputPortIdType consumerNamedInputPortId = InputPortHandle->NamedInputPortId;
    vStreamProc_WaveLevelType consumerWaveLevel = InputPortHandle->WaveLevel;

    /* #20 Check if the consumer node is registered on a wave level in the range: */
    if ((consumerWaveLevel >= WaveLevelRangeStart)
      && (consumerWaveLevel <= WaveLevelRangeEnd))
    {
      /* #30 Check if the consumer wave is in the correct state. */
      /* Hint: ENDING might be possible if the pipe was restored and the waves are not yet in sync.
               Is the consumer wave state already IDLE, no signal must be set. */
      if ( (vStreamProc_IsWaveStateFlagSetOfStreamOutputPortId(streamOutputPortId, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
        || (vStreamProc_IsWaveStateFlagSetOfStreamOutputPortId(streamOutputPortId, VSTREAMPROC_WAVESTATE_FLAG_ENDING)))
      {
        /* #40 Set wave end signal for the consumer node. */
        /* Hint: WaveEnd is mandatory for all processing nodes, no need to consider the return value. */
        (void)vStreamProc_Scheduler_SetInputPortSignal(
          consumerNamedInputPortId,
          vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd);

        /* #50 Clear the ACTIVE flag and set the ending flag of the wave state on consumer side. */
        vStreamProc_ClrWaveStateFlagOfStreamOutputPortId(streamOutputPortId, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);
        vStreamProc_SetWaveStateFlagOfStreamOutputPortId(streamOutputPortId, VSTREAMPROC_WAVESTATE_FLAG_ENDING);

        /* #60 Increase the pending wave end count on this wave level. */
        vStreamProc_IncPendingWaveEndCountOfWaveVarInfo(registeredConsumerWaveId);
      }
    }

    /* #70 If the consumer wave level is lower than the end of the wave level range: */
    if (consumerWaveLevel < WaveLevelRangeEnd)
    {
      /* #80 Trigger the internal wave propagation for higher wave levels. */
      /* Hint: If the consumer wave level is lower, it means the range contains wave levels which are out of scope
                for the node, which have to be propagated silently to other nodes behind the consumer node. */
      (void)vStreamProc_Scheduler_SetInputPortSignal(
        consumerNamedInputPortId,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEndUpperLevel);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_TriggerPostWaveEndActions()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_TriggerPostWaveEndActions(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveIterType waveStartIdx = vStreamProc_GetWaveStartIdxOfStream(StreamId);
  vStreamProc_WaveIterType waveIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the waves of the stream and update the wave state */
  for (waveIdx = (waveStartIdx + (vStreamProc_WaveIterType)WaveLevelRangeStart);
       waveIdx <= (waveStartIdx + (vStreamProc_WaveIterType)WaveLevelRangeEnd);
       waveIdx++)
  {
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveIdx);

    /* #20 Clear the ACTIVE flag */
    vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE);

    /* #30 Set ENDING flag if there are pending wave end signals for this wave level. */
    if (vStreamProc_GetPendingWaveEndCountOfWaveVarInfo(waveIdx) > 0u)
    {
      vStreamProc_SetWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
    }

    /* #40 Update the wave state. */
    vStreamProc_SetStateOfWaveVarInfo(waveIdx, waveState);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_PropagateWave()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, AUTOMATIC) vStreamProc_Stream_PropagateWave(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevelRangeStart,
  vStreamProc_WaveLevelType WaveLevelRangeEnd,
  vStreamProc_SignalIdType WaveSignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIterType streamOutputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(StreamId);
       streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(StreamId);
       streamOutputPortIdx++)
  {
    vStreamProc_InputPortHandleType inputPortHandle;

    (void)vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
      (vStreamProc_StreamOutputPortIdType)streamOutputPortIdx,
      &inputPortHandle);

    if (inputPortHandle.IsConnected == TRUE)
    {
      switch (WaveSignalId)
      {
        case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart):
        {
          vStreamProc_Stream_PropagateWaveStart(
            &inputPortHandle,
            WaveLevelRangeEnd);

          break;
        }
        case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd):
        {
          vStreamProc_Stream_PropagateWaveEnd(
            &inputPortHandle,
            WaveLevelRangeStart,
            WaveLevelRangeEnd);
          break;
        }
        default:
        {
          /* Nothing to do */
          break;
        }
      }
    }
  }

  /* #10 Trigger post propagation actions. */
  switch (WaveSignalId)
  {
    case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart):
    {
      vStreamProc_Stream_TriggerPostWaveStartActions(StreamId, WaveLevelRangeEnd);
      break;
    }
    case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd):
    {
      vStreamProc_Stream_TriggerPostWaveEndActions(StreamId, WaveLevelRangeStart, WaveLevelRangeEnd);
      break;
    }
    default:
    {
      /* Nothing to do */
      break;
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_CheckForBlockedWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_CheckForBlockedWave(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId)
{
    /* ----- Local Variables ---------------------------------------------- */
    vStreamProc_StreamIdType streamId =
      vStreamProc_GetStreamIdxOfStreamOutputPort(StreamOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if a blocked wave is pending. */
  if (vStreamProc_IsWaveBlockedOfStreamInfo(streamId))
  {
    vStreamProc_WaveLevelType waveLevelRangeEnd = vStreamProc_Stream_GetWaveLevelRangeStarting(streamId);

    if (vStreamProc_Stream_IsWavePropagationAllowed(streamId, waveLevelRangeEnd) == TRUE)
    {
      vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);
      vStreamProc_OutputPortIdType  storageOutputPortId =
        vStreamProc_GetStorageOutputPortIdxOfStreamOutputPort(StreamOutputPortId);
      vStreamProc_StorageInfoType storageInfo;

      /* #20 Propagate the wave blocked wave and clear the blocked flag */
      vStreamProc_SetWaveBlockedOfStreamInfo(streamId, FALSE);

      vStreamProc_Stream_PropagateWave(
        streamId,
        0u,
        waveLevelRangeEnd,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart);

      /* #30 Check for available data and issue DataAvailable if true. */
      vStreamProc_Stream_InitStorageInfo(&storageInfo);
      (void)vStreamProc_CallReadInfoOfStorageNode(storageNodeId, &storageInfo, storageOutputPortId);

      if (storageInfo.AvailableLength > 0u)
      {
        vStreamProc_Stream_HandleDataAvailable(streamId);
      }
    }
  }
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  CORE API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Stream_InitProcessingNodeInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_InitProcessingNodeInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortResults,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortResults,
  P2VAR(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  vStreamProc_ProcessingNodeTypeDefIdType    procNodeTypeId  =
    vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_ProcessingNodeClassDefIdType   procNodeClassId =
    vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(procNodeTypeId);
  vStreamProc_NamedInputPortIterType      inputPortIndex;
  vStreamProc_NamedOutputPortIterType     outputPortIndex;

  /* #10 Initialize node information attributes. */
  ProcNodeInfo->ProcessingNodeId        = ProcessingNodeId;
  ProcNodeInfo->SliceHandle             = vStreamProc_GetSliceOfProcessingNode(ProcessingNodeId);

  ProcNodeInfo->InputPortResults        = InputPortResults;
  ProcNodeInfo->OutputPortResults       = OutputPortResults;

  ProcNodeInfo->InputPortCount          =
    (vStreamProc_InputPortIdType)vStreamProc_GetRequiredInputPortsOfProcessingNodeTypeDef(procNodeTypeId)
    + (vStreamProc_InputPortIdType)vStreamProc_GetOptionalInputPortsOfProcessingNodeTypeDef(procNodeTypeId);
  ProcNodeInfo->OutputPortCount         =
    (vStreamProc_OutputPortIdType)vStreamProc_GetRequiredOutputPortsOfProcessingNodeTypeDef(procNodeTypeId)
    + (vStreamProc_OutputPortIdType)vStreamProc_GetOptionalOutputPortsOfProcessingNodeTypeDef(procNodeTypeId);

  ProcNodeInfo->WorkspaceInfo.Pointer   = vStreamProc_GetWorkspaceOfProcessingNode(ProcessingNodeId);
  ProcNodeInfo->WorkspaceInfo.TypeId    = vStreamProc_GetWorkspaceTypeOfProcessingNodeClassDef(procNodeClassId);

  ProcNodeInfo->ConfigInfo.Pointer      = vStreamProc_GetConfigOfProcessingNodeTypeDef(procNodeTypeId);
  ProcNodeInfo->ConfigInfo.TypeId       = vStreamProc_GetConfigTypeOfProcessingNodeClassDef(procNodeClassId);

  /* #20 Reset port specific results. */
  for (inputPortIndex = 0u; inputPortIndex < ProcNodeInfo->InputPortCount; inputPortIndex++)
  {
    InputPortResults[inputPortIndex] = VSTREAMPROC_OK;
  }

  for (outputPortIndex = 0u; outputPortIndex < ProcNodeInfo->OutputPortCount; outputPortIndex++)
  {
    OutputPortResults[outputPortIndex] = VSTREAMPROC_OK;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_InitStorageInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_InitStorageInfo(
  vStreamProc_StorageInfoPtrType StorageInfo)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize storage info structure with default values. */
  StorageInfo->DataTypeInfo.Id = vStreamProcConf_vStreamProcDataType_Undefined;
  StorageInfo->DataTypeInfo.Size = 0u;
  StorageInfo->AvailableLength = 0u;
  StorageInfo->RequestLength = 0u;
  StorageInfo->ReleaseFlag = TRUE;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_PrepareWaveInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_PrepareWaveInfo(
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Init the wave info structure. */
  WaveInfo->Handle = 0u;
  WaveInfo->AbsoluteStreamPosition = 0u;
  WaveInfo->RelativeStreamPosition = 0u;
  WaveInfo->State = VSTREAMPROC_WAVESTATE_UNINIT;
  WaveInfo->MetaData.Buffer = NULL_PTR;

  /* #20 Init the meta data storage info. */
  vStreamProc_Stream_InitStorageInfo(&WaveInfo->MetaData.StorageInfo);

  WaveInfo->WaveTypeSymbolicName = WaveTypeSymbolicName;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortHandle
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortHandle(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_InputPortHandlePtrType InputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);
  boolean isConnected = vStreamProc_Stream_IsInputPortConnected(ProcessingNodeId, InputPortSymbolicName);

  /* ----- Implementation ----------------------------------------------- */
  /* Init the handle with default values. */
  vStreamProc_Stream_InitInputPortHandle(InputPortHandle);

  /* #10 Write the unconditional handle values. */
  InputPortHandle->ProcessingNodeId = ProcessingNodeId;
  InputPortHandle->InputPortSymbolicName = InputPortSymbolicName;
  InputPortHandle->WaveLevel = waveLevel;
  InputPortHandle->IsConnected = isConnected;

  /* #20 Check if the input port is connected. */
  if (isConnected == TRUE)
  {
    /* #30 Write the conditional handle values. */
    vStreamProc_NamedInputPortIdType namedInputPortId =
      vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId)
      + (vStreamProc_NamedInputPortIdType)InputPortSymbolicName;

    vStreamProc_StreamOutputPortIdType streamOutputPortId =
      vStreamProc_GetStreamOutputPortMapping(
        vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortId));

    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId);

    vStreamProc_WaveIdType waveId =
      vStreamProc_GetWaveStartIdxOfStream(vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId))
      + (vStreamProc_WaveIdType)waveLevel;

    vStreamProc_StorageOutputPortIdType storageOutputPortId =
      vStreamProc_GetStorageOutputPortIdxOfStreamOutputPort(streamOutputPortId);

    if (storageOutputPortId != VSTREAMPROC_NO_STORAGEOUTPUTPORT)
    {
      InputPortHandle->StorageNodeId = vStreamProc_GetStorageNodeIdxOfStorageOutputPort(storageOutputPortId);
    }

    /* #40 Check whether the producer stream is active. */
    if (vStreamProc_GetNamedOutputPortIdxOfStreamInfo(streamId) != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
    {
      InputPortHandle->HasActiveProducer = TRUE;
    }

    InputPortHandle->NamedInputPortId = namedInputPortId;
    InputPortHandle->PortDefId = vStreamProc_GetPortDefIdxOfNamedInputPort(namedInputPortId);
    InputPortHandle->StreamOutputPortId = streamOutputPortId;
    InputPortHandle->StreamId = streamId;
    InputPortHandle->StorageOutputPortId = storageOutputPortId;
    InputPortHandle->WaveId = waveId;
    InputPortHandle->IsVirtual = vStreamProc_Stream_IsInputPortVirtual(namedInputPortId);

    retVal = VSTREAMPROC_OK;
  }


  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortHandle
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortHandle(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_OutputPortHandlePtrType OutputPortHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);
  boolean isConnected = vStreamProc_Stream_IsOutputPortConnected(ProcessingNodeId, OutputPortSymbolicName);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Init the output handle with default values. */
  vStreamProc_Stream_InitOutputPortHandle(OutputPortHandle);

  /* #20 Write the unconditional handle values. */
  OutputPortHandle->ProcessingNodeId = ProcessingNodeId;
  OutputPortHandle->OutputPortSymbolicName = OutputPortSymbolicName;
  OutputPortHandle->WaveLevel = waveLevel;
  OutputPortHandle->IsConnected = isConnected;

  /* #30 Check if the output port is connected. */
  if (isConnected == TRUE)
  {
    /* #40 Write the conditional handle data. */
    vStreamProc_NamedOutputPortIdType namedOutputPortId =
      vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId)
      + (vStreamProc_NamedOutputPortIdType)OutputPortSymbolicName;
    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortId);
    vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(streamId) + (vStreamProc_WaveIdType)waveLevel;

    OutputPortHandle->NamedOutputPortId = namedOutputPortId;
    OutputPortHandle->PortDefId = vStreamProc_GetPortDefIdxOfNamedOutputPort(namedOutputPortId);
    OutputPortHandle->StreamId = streamId;
    OutputPortHandle->StorageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);
    OutputPortHandle->WaveId = waveId;
    OutputPortHandle->IsVirtual = vStreamProc_Stream_IsOutputPortVirtual(namedOutputPortId);

    retVal = VSTREAMPROC_OK;
  }


  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsInputPortVirtual()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsInputPortVirtual(
  vStreamProc_NamedInputPortIdType NamedInputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal;
  vStreamProc_DataTypeIdType dataTypeId =
    vStreamProc_GetDataElementTypeOfPortDef(
      vStreamProc_GetPortDefIdxOfNamedInputPort(NamedInputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the input port is virtual. */
  if (dataTypeId == VSTREAMPROC_NO_DATATYPE)
  {
    retVal = TRUE;
  }
  else
  {
    retVal = FALSE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsOutputPortVirtual()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsOutputPortVirtual(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal;
  vStreamProc_DataTypeIdType dataTypeId =
    vStreamProc_GetDataElementTypeOfPortDef(
      vStreamProc_GetPortDefIdxOfNamedOutputPort(NamedOutputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the output port is virtual. */
  if (dataTypeId == VSTREAMPROC_NO_DATATYPE)
  {
    retVal = TRUE;
  }
  else
  {
    retVal = FALSE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsInputPortConnected()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsInputPortConnected(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_NamedInputPortIdType namedInputPortId =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedInputPortIdType)InputPortSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 None of the optional ports of input port groups may be mapped in the configuration at all. */
  if (namedInputPortId < vStreamProc_GetNamedInputPortEndIdxOfProcessingNode(ProcessingNodeId))
  {
    vStreamProc_StreamOutputPortMappingIdType streamOutputPortMappingId =
      vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(namedInputPortId);

    /* #20 Check whether port is currently connected:
      *  + Mapped in configuration (valid entry in indirection table).
      *  + Dynamic mapping contains valid stream output port. */
    if (streamOutputPortMappingId != VSTREAMPROC_NO_STREAMOUTPUTPORTMAPPINGIDXOFNAMEDINPUTPORT)
    {
      vStreamProc_StreamOutputPortIdType streamOutputPortId =
        vStreamProc_GetStreamOutputPortMapping(streamOutputPortMappingId);

      if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
      {
        retVal = TRUE;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsOutputPortConnected()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsOutputPortConnected(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = FALSE;
  vStreamProc_NamedOutputPortIdType namedOutputPortId =
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedOutputPortIdType)OutputPortSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 None of the optional ports may be mapped in the configuration at all. */
  if (namedOutputPortId < vStreamProc_GetNamedOutputPortEndIdxOfProcessingNode(ProcessingNodeId))
  {
    vStreamProc_StreamIdType streamId =
      vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortId);

    /* #20 Check whether port is currently connected:
      *  + Mapped in configuration (valid entry in indirection table).
      *  + The output port is the active producer of the connected stream. */
    if ((streamId != VSTREAMPROC_NO_STREAMIDXOFNAMEDOUTPUTPORT)
      && ((vStreamProc_GetNamedOutputPortIdxOfStreamInfo(streamId) == namedOutputPortId)))
    {
      retVal = TRUE;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsStreamOutputPortActive()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsStreamOutputPortActive(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean  retVal = FALSE;
  vStreamProc_NamedInputPortIdType consumerNamedInputPortId =
      vStreamProc_GetNamedInputPortIdxOfStreamOutputPort(StreamOutputPortId);
  vStreamProc_StreamOutputPortIdType mappedStreamOutputPortId =
    vStreamProc_GetStreamOutputPortMapping(
      vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(consumerNamedInputPortId));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the stream output port is active. */
  if (StreamOutputPortId == mappedStreamOutputPortId)
  {
    retVal = TRUE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetProducePosition()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetProducePosition(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevel,
  vStreamProc_LengthType AvailableLength,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(StreamId) + (vStreamProc_WaveIdType)WaveLevel;

  /* #10 Check if the wave was started at least once. */
  if (vStreamProc_IsWaveStateFlagSetOfWaveId(waveId, VSTREAMPROC_WAVESTATE_FLAG_INIT))
  {
    /* #20 Get produce position. */
    vStreamProc_LengthType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);

    StreamPositionInfo->RelativePosition = producePosition;
    StreamPositionInfo->AbsolutePositionStart = vStreamProc_GetAbsolutePositionStartOfWaveVarInfo(waveId);
    StreamPositionInfo->TotalLength = producePosition + AvailableLength;
  }
  /* #30 Otherwise: */
  else
  {
    /* #40 Provide default values. */
    StreamPositionInfo->RelativePosition = 0u;
    StreamPositionInfo->AbsolutePositionStart = 0u;
    StreamPositionInfo->TotalLength = 0u;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetConsumePosition()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetConsumePosition(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_LengthType AvailableLength,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* #10 Check if the wave was started at least once. */
  if (vStreamProc_IsWaveStateFlagSetOfStreamOutputPortId(StreamOutputPortId, VSTREAMPROC_WAVESTATE_FLAG_INIT))
  {
    /* #20 Get Consume position. */
    vStreamProc_LengthType consumePosition = vStreamProc_GetConsumePositionOfStreamOutputPortInfo(StreamOutputPortId);

    StreamPositionInfo->RelativePosition = consumePosition;
    StreamPositionInfo->AbsolutePositionStart =
      vStreamProc_GetAbsolutePositionStartOfStreamOutputPortInfo(StreamOutputPortId);
    StreamPositionInfo->TotalLength = consumePosition + AvailableLength;
  }
  /* #30 Otherwise: */
  else
  {
    /* #40 Provide default values. */
    StreamPositionInfo->RelativePosition = 0u;
    StreamPositionInfo->AbsolutePositionStart = 0u;
    StreamPositionInfo->TotalLength = 0u;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetMetaData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetMetaData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveTypeDefIdType WaveTypeDefId,
  vStreamProc_ReadRequestPtrType MetaData)
{
  vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevel(PipeId, WaveTypeDefId);
  vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(StreamId) + (vStreamProc_WaveIdType)waveLevel;
  vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);
  vStreamProc_DataTypeIdType dataTypeId = vStreamProc_GetMetaDataElementTypeOfWaveTypeDef(WaveTypeDefId);

  /* #10 Provide the data type info. */
  MetaData->StorageInfo.DataTypeInfo.Id = dataTypeId;
  MetaData->StorageInfo.DataTypeInfo.Size = vStreamProc_GetMetaDataElementSizeOfWaveTypeDef(WaveTypeDefId);

  /* #20 Check if there is meta data to provide.*/
  if ((dataTypeId != VSTREAMPROC_NO_DATATYPE) && (waveState != VSTREAMPROC_WAVESTATE_UNINIT))
  {
    /* #30 Provide the meta data. */
    MetaData->Buffer = vStreamProc_GetMetaDataPointerOfWave(waveId);
    MetaData->StorageInfo.AvailableLength = 1u;
  }
  /* #40 Otherwise:*/
  else
  {
    /* #50 Provide default values. */
    MetaData->Buffer = NULL_PTR;
    MetaData->StorageInfo.AvailableLength = 0u;
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevel(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveTypeDefIdType WaveTypeDefId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveLevelType waveLevel = VSTREAMPROC_NO_WAVELEVEL;
  vStreamProc_WaveLevelDefIterType waveLevelDefIdx;
  vStreamProc_WaveLevelDefIterType waveLevelDefOffset = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the wave levels of the pipe. */
  for (waveLevelDefIdx = vStreamProc_GetWaveLevelDefStartIdxOfPipe(PipeId);
       waveLevelDefIdx <= vStreamProc_GetWaveLevelDefEndIdxOfPipe(PipeId);
       waveLevelDefIdx++)
  {
    /* #20 If the provided wave level is found, set the wave level. */
    if (vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(waveLevelDefIdx) == WaveTypeDefId)
    {
      waveLevel = (vStreamProc_WaveLevelType)waveLevelDefOffset;
      break;
    }
    waveLevelDefOffset++;
  }

  return waveLevel;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelOfProcessingNode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelOfProcessingNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* #10 Calculate the registered wave level of the processing node. */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_WaveLevelType waveLevel =
    (vStreamProc_WaveLevelType)vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcessingNodeId)
    - (vStreamProc_WaveLevelType)vStreamProc_GetWaveLevelDefStartIdxOfPipe(pipeId);

  return waveLevel;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetAvailableStorageLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_LengthType, AUTOMATIC) vStreamProc_Stream_GetAvailableStorageLength(
  vStreamProc_StreamIdType StreamId)
{
  vStreamProc_LengthType availableLength = 0u;

  vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(StreamId);
  vStreamProc_NamedOutputPortIdType namedOutputPortId = vStreamProc_GetNamedOutputPortIdxOfStreamInfo(StreamId);

  /* #10 Check if there is an active producer for the stream. */
  if (namedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
  {
    vStreamProc_StorageInfoType storageInfo;

    /* #20 Issue write info request to associated storage node. */
    vStreamProc_Stream_InitStorageInfo(&storageInfo);
    (void)vStreamProc_CallWriteInfoOfStorageNode(storageNodeId, &storageInfo);

    /* #30 Return available storage length. */
    availableLength = storageInfo.AvailableLength;
  }

  return availableLength;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_SetEntryPointWaveData()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_SetEntryPointWaveData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfNamedOutputPort(
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId));
  vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevel(pipeId, WaveInfo->WaveTypeSymbolicName);
  vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(streamId) + (vStreamProc_WaveIdType)waveLevel;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy the wave data from the wave info. */
  vStreamProc_Stream_CopyWaveDataFromWaveInfo(waveId, WaveInfo);

  /* #20 Update the wave handle. */
  vStreamProc_IncHandleOfWaveVarInfo(waveId);

  /* #30 Set the wave state to STARTING */
  vStreamProc_SetStateOfWaveVarInfo(waveId, VSTREAMPROC_WAVESTATE_STARTING);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_DecreasePendingWaveSignalCount
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_DecreasePendingWaveSignalCount(                       /* PRQA S 6080 */ /* MD_MSR_STMIF */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIdType consumerNodeId = ProcessingNodeInfo->ProcessingNodeId;
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the producer stream. */
  (void)vStreamProc_Stream_GetInputPortHandle(
    consumerNodeId,
    ProcessingNodeInfo->SignalInfo.InputPortId,
    &inputPortHandle);

  if (inputPortHandle.IsConnected == TRUE)
  {
  /* #20 Check the signal. */
    switch (ProcessingNodeInfo->SignalInfo.SignalId)
    {
      /* #30 In case of WaveStart signal: */
      case vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart:
      {
        /* #40 Clear the STARTING  wave state flag on consumer side. */
        vStreamProc_ClrWaveStateFlagOfStreamOutputPortId(
          inputPortHandle.StreamOutputPortId,
          VSTREAMPROC_WAVESTATE_FLAG_STARTING);

        /* #50 Decrease the pending stream output port count of the wave. */
        vStreamProc_DecPendingWaveStartCountOfWaveVarInfo(inputPortHandle.WaveId);

        /* #60 Check if the pending count is zero and change the wave state if necessary. */
        if (vStreamProc_GetPendingWaveStartCountOfWaveVarInfo(inputPortHandle.WaveId) == 0u)
        {
          vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(inputPortHandle.WaveId);

          /* #70 Clear the STARTING flag in the wave state. */
          vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_STARTING);

          /* #80 Clear the ENDING flag as well if the pending EndWave count is zero. */
          /* Hint: This might be the case if there were no WaveEnd handlers but WaveStart handlers.*/
          if (vStreamProc_GetPendingWaveEndCountOfWaveVarInfo(inputPortHandle.WaveId) == 0u)
          {
            vStreamProc_ClrWaveStateFlag(waveState, VSTREAMPROC_WAVESTATE_FLAG_ENDING);
          }

          vStreamProc_SetStateOfWaveVarInfo(inputPortHandle.WaveId, waveState);
        }

        retVal = VSTREAMPROC_OK;

        break;
      }
      /* #90 In case of WaveEnd signal: */
      case vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd:
      {
        vStreamProc_StreamIdType streamId =
          vStreamProc_GetStreamIdxOfStreamOutputPort(inputPortHandle.StreamOutputPortId);

        retVal = VSTREAMPROC_OK;

        /* #100 Clear the ENDING  wave state flag on consumer side. */
        vStreamProc_ClrWaveStateFlagOfStreamOutputPortId(
          inputPortHandle.StreamOutputPortId,
          VSTREAMPROC_WAVESTATE_FLAG_ENDING);

        /* #110 Decrease the overall pending wave count. */
        vStreamProc_DecActiveWaveCountOfPipeInfo(vStreamProc_GetPipeIdxOfProcessingNode(consumerNodeId));

        /* #120 Decrease the pending stream output port count of the wave. */
        vStreamProc_DecPendingWaveEndCountOfWaveVarInfo(inputPortHandle.WaveId);

        /* #130 Check if the pending wave end count is zero and change the wave state if necessary. */
        if (vStreamProc_GetPendingWaveEndCountOfWaveVarInfo(inputPortHandle.WaveId) == 0u)
        {
          /* #140 Clear the ENDING flag in the wave state. */
          vStreamProc_ClrWaveStateFlagOfWaveId(inputPortHandle.WaveId, VSTREAMPROC_WAVESTATE_FLAG_ENDING);

          /* #150 Check if the pipe is clogged. */
          /* Hint: Check if all produced data was consumed. */
          if (vStreamProc_GetProducePositionOfWaveVarInfo(inputPortHandle.WaveId)
            == vStreamProc_GetConsumePositionOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId))
          {
            vStreamProc_StreamOutputPortIdType sourceStreamOutputPort = vStreamProc_Stream_GetSourceStreamOutputPort(streamId);

            if (sourceStreamOutputPort != VSTREAMPROC_NO_STREAMOUTPUTPORT)
            {
              /* #160 If true, update the source stream data. */
              vStreamProc_Stream_CheckForBlockedWave(sourceStreamOutputPort);
            }
          }
          else
          {
            retVal = VSTREAMPROC_FAILED;
          }
        }

        break;
      }
      /* #170 Otherwise: Do nothing */
      default:
      {
        /* Nothing to do */
        break;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_PerformWaveRouting
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_PerformWaveRouting(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_NamedInputPortIdType sourceNamedInputPortId =
    vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId) + InputPortSymbolicName;
  vStreamProc_WaveRoutingIdType waveRoutingId =
    vStreamProc_GetWaveRoutingIdxOfNamedInputPort(sourceNamedInputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is a wave propagation defined for the named input port. */
  if ( (waveRoutingId != VSTREAMPROC_NO_WAVEROUTINGIDXOFNAMEDINPUTPORT)
    && (vStreamProc_IsAutomaticOfWaveRouting(waveRoutingId)))
  {
    vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
    vStreamProc_WaveLevelDefIterType registeredWaveHierarchyIdx =
      vStreamProc_GetWaveLevelDefIdxOfProcessingNode(ProcessingNodeId);
    vStreamProc_WaveInfoType waveInfo;
    vStreamProc_NamedOutputPortIndIterType namedOutputPortIndIdx;

    /* #20 Query the wave info from the input port. */
    (void)vStreamProc_PrepareWaveInfo(
      pipeId,
      vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(registeredWaveHierarchyIdx),
      &waveInfo);

    /* Hint: In this context (automatic routing), the input port is always connected. */
    (void)vStreamProc_Stream_GetInputPortWaveInfo(ProcessingNodeId, InputPortSymbolicName, &waveInfo);

    /* #30 Iterate over the mapped named output ports */
    for (namedOutputPortIndIdx = vStreamProc_GetNamedOutputPortIndStartIdxOfWaveRouting(waveRoutingId);
         namedOutputPortIndIdx < vStreamProc_GetNamedOutputPortIndEndIdxOfWaveRouting(waveRoutingId);
         namedOutputPortIndIdx++)
    {
      vStreamProc_NamedOutputPortIdType targetNamedOutputPortId =
        vStreamProc_GetNamedOutputPortInd(namedOutputPortIndIdx);
      vStreamProc_OutputPortSymbolicNameType outputPortSymbolicName =
        vStreamProc_GetOutputPortSymbolicNameOfPortDef(
          vStreamProc_GetPortDefIdxOfNamedOutputPort(
            targetNamedOutputPortId));
      vStreamProc_OutputPortHandleType outputPortHandle;

      /* #40 Check if the output port is connected. */
      if (vStreamProc_Stream_GetOutputPortHandle(
            ProcessingNodeId, outputPortSymbolicName, &outputPortHandle) == VSTREAMPROC_OK)
      {
        switch (SignalId)
        {
          /* #50 In case of WaveStart */
          case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart):
          {
            /* #60 Try to propagate the wave start. */
            /* Hint: INSUFFICIENT_OUTPUT cannot happen here because it was checked before that the target stream
                     is free. Furthermore, there is a check which prevents processing nodes from starting
                     a wave on an output port with automatic wave routing. */
            (void)vStreamProc_Stream_StartOutputPortWave(
              ProcessingNodeId,
              outputPortSymbolicName,
              &waveInfo);
            break;
          }
          /* #70 In case of WaveEnd */
          case (vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd):
          {
            /* #80 Propagate the wave end. */
            vStreamProc_Stream_EndOutputPortWave(
              ProcessingNodeId,
              outputPortSymbolicName,
              waveInfo.WaveTypeSymbolicName);
            break;
          }
          /* #90 Otherwise: Do nothing */
          default:
          {
            /* Nothing to do */
            break;
          }
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_HandleDataAvailable
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_HandleDataAvailable(
  vStreamProc_StreamIdType StreamId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamOutputPortIterType streamOutputPortIdx;
  vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(StreamId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the wave is active. */
  if (vStreamProc_IsWaveStateFlagSetOfWaveId(waveId, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
  {
    /* #20 Iterate over all stream output ports. */
    for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(StreamId);
         streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(StreamId);
         streamOutputPortIdx++)
    {
      vStreamProc_InputPortHandleType inputPortHandle;

      /* #30 Check if the stream output port is connected. */
      (void)vStreamProc_Stream_GetInputPortHandleByStreamOutputPort(
        (vStreamProc_StreamOutputPortIdType)streamOutputPortIdx,
        &inputPortHandle);

      if (inputPortHandle.IsConnected == TRUE)
      {
        vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(inputPortHandle.WaveId);
        vStreamProc_WaveHandleType consumerWaveHandle =
          vStreamProc_GetWaveHandleOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId);
        vStreamProc_StreamPositionType producePosition =
          vStreamProc_GetProducePositionOfWaveVarInfo(inputPortHandle.WaveId);
        vStreamProc_StreamPositionType consumePosition =
          vStreamProc_GetConsumePositionOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId);

        /* #40 Check if the consumer wave handle is the same as the producer wave handle and the produce position
               is higher than the consume position. */
        /* Hint: If this is not the case, the pipe was restored and the waves are not synchronized yet.
                 In this case, the consumer must not be notified about new data. */
        if ( (consumerWaveHandle == producerWaveHandle)
          && (producePosition > consumePosition))
        {
          /* #50 Check if the produce position is higher than the consume position */
          /* #60 Set the DataAvailable signal at the named input port. */
          (void)vStreamProc_Scheduler_SetInputPortSignal(
            inputPortHandle.NamedInputPortId,
            (vStreamProc_SignalIdType)vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable);
        }
      }
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_HandleStorageAvailable
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_HandleStorageAvailable(
  vStreamProc_StorageNodeIdType StorageNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStorageNode(StorageNodeId);
  vStreamProc_NamedOutputPortIdType producerNamedOutputPortId =
    vStreamProc_GetNamedOutputPortIdxOfStreamInfo(streamId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the stream has an active producer. */
  if (producerNamedOutputPortId != VSTREAMPROC_NO_NAMEDOUTPUTPORT)
  {
    /* #20 Set the StorageAvailable signal at the producer output port. */
    (void)vStreamProc_Scheduler_SetOutputPortSignal(
      producerNamedOutputPortId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_SyncOutputPortsWaves
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SyncOutputPortsWaves(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_OutputPortIterType outputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the output ports. */
  for (outputPortIdx = 0u;
       outputPortIdx < vStreamProc_GetNamedOutputPortLengthOfProcessingNode(ProcessingNodeId);
       outputPortIdx++)
  {
    vStreamProc_OutputPortHandleType outputPortHandle;

    (void)vStreamProc_Stream_GetOutputPortHandle(
      ProcessingNodeId,
      (vStreamProc_OutputPortSymbolicNameType)outputPortIdx,
      &outputPortHandle);

    /* #20 Sync the consumers if the output port is connected. */
    if ((outputPortHandle.IsConnected == TRUE) && (outputPortHandle.IsVirtual == FALSE))
    {
      if (vStreamProc_IsDataProducedOfStreamInfo(outputPortHandle.StreamId))
      {
        vStreamProc_Stream_SyncConsumers(&outputPortHandle);
        vStreamProc_SetDataProducedOfStreamInfo(outputPortHandle.StreamId, FALSE);
      }
    }
  }
}

/**********************************************************************************************************************
 *  PORT ACCESS API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_GetInputPortInfo(                                     /* PRQA S 6030 */ /* MD_MSR_STCYC */
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType                retVal                  = VSTREAMPROC_FAILED;                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType        readRequestStorageInfo  = &ReadRequest->StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if (InputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId);
    vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
    vStreamProc_WaveStateType waveState =vStreamProc_GetStateOfWaveVarInfo(waveId);
    vStreamProc_DataTypeIdType expectedDataTypeId = readRequestStorageInfo->DataTypeInfo.Id;
    boolean isVirtual = InputPortHandle->IsVirtual;
    boolean producerWaveSync = vStreamProc_Stream_IsProducerWaveSync(InputPortHandle);

    /* #20 Set the data type if the input port is virtual. */
    if (isVirtual == TRUE)
    {
      readRequestStorageInfo->DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;
      readRequestStorageInfo->DataTypeInfo.Size = 0u;
    }

    /* #30 Check if the wave is in the correct state. */
    /* Hint: If the wave state is only starting (and not propagated yet),
             access to possibly available data shall be denied. */

    if ( (producerWaveSync == TRUE)
      &&   ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ACTIVE))
          || (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ENDING))))
    {
      /* #40 Check if a virtual stream is requested. */
      if (isVirtual == TRUE)
      {
        /* #50 Set the available length manually. */
        readRequestStorageInfo->AvailableLength =
          vStreamProc_GetProducePositionOfWaveVarInfo(waveId)
          - vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

        retVal = VSTREAMPROC_OK;
      }
      /* Otherwise: */
      else
      {
        /* #60 Issue stream request to associated storage node. */
        retVal = vStreamProc_CallReadInfoOfStorageNode(
          InputPortHandle->StorageNodeId,
          readRequestStorageInfo,
          InputPortHandle->StorageOutputPortId);
      }
    }
    /* #70 Otherwise: */
    else
    {
      /* #80 Report no available data */
      readRequestStorageInfo->AvailableLength = 0u;
      ReadRequest->Buffer = NULL_PTR;

      /* #90 Update the data type info if the input port is non-virtual */
      if (isVirtual == FALSE)
      {
        vStreamProc_Stream_SetDataTypeInfoByStream(streamId, &readRequestStorageInfo->DataTypeInfo);
      }

      if (readRequestStorageInfo->RequestLength > 0u)
      {
        retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
      }
      else
      {
        retVal = VSTREAMPROC_OK;
      }
    }

    if (retVal != VSTREAMPROC_FAILED)
    {
      /* #100 Get positions and meta data. */
      vStreamProc_Stream_GetConsumePosition(
        InputPortHandle->StreamOutputPortId,
        readRequestStorageInfo->AvailableLength,
        StreamPositionInfo);

      vStreamProc_Stream_GetMetaDataOfInputPort(InputPortHandle, MetaData);

      /* #110 Check the data type against the expected and overwrite the result if necessary. */
      if ( (readRequestStorageInfo->DataTypeInfo.Id != expectedDataTypeId)
        && (expectedDataTypeId != (vStreamProc_DataTypeIdType)vStreamProcConf_vStreamProcDataType_Undefined))
      {
        retVal = VSTREAMPROC_FAILED;
      }
    }
  }
  /* #120 Otherwise: */
  else
  {
    /* #130 Report insufficient input if any data was requested. Success otherwise. */
    if (readRequestStorageInfo->RequestLength > 0u)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
    }
    else
    {
      /* Failed result would interfere with batch requests for nodes with unconnected optional ports. */
      retVal = VSTREAMPROC_OK;
    }

    /* #140 Reset storage info to default values. */
    vStreamProc_Stream_InitStorageInfo(readRequestStorageInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_RequestInputPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_RequestInputPortData(                                 /* PRQA S 6030 */ /* MD_MSR_STCYC */
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal                  = VSTREAMPROC_FAILED;                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType      readRequestStorageInfo  = &ReadRequest->StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if (InputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId);
    vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
    vStreamProc_WaveStateType waveState =vStreamProc_GetStateOfWaveVarInfo(waveId);
    vStreamProc_DataTypeIdType expectedDataTypeId = readRequestStorageInfo->DataTypeInfo.Id;
    boolean isVirtual = InputPortHandle->IsVirtual;
    boolean producerWaveSync = vStreamProc_Stream_IsProducerWaveSync(InputPortHandle);

    /* #20 Set the data type if the input port is virtual. */
    if (isVirtual == TRUE)
    {
      readRequestStorageInfo->DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;
      readRequestStorageInfo->DataTypeInfo.Size = 0u;
    }

    /* #30 Check if the wave is in the correct state. */
    /* Hint: If the wave state is only starting (and not propagated yet),
             access to possibly available data shall be denied. */
    if ( (producerWaveSync == TRUE)
      && ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ACTIVE))
        || (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ENDING))))
    {
      /* #40 Check if a virtual stream is requested. */
      if (isVirtual == TRUE)
      {
        /* #50 Set the available length manually. */
        readRequestStorageInfo->AvailableLength =
          vStreamProc_GetProducePositionOfWaveVarInfo(waveId)
          - vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

        retVal = VSTREAMPROC_OK;
      }
      /* Otherwise: */
      else
      {
        /* #60 Issue stream request to associated storage node */
        retVal = vStreamProc_CallReadRequestOfStorageNode(
          InputPortHandle->StorageNodeId,
          ReadRequest,
          InputPortHandle->StorageOutputPortId);
      }
    }
    /* #70 Otherwise: */
    else
    {
      /* #80 Report no available data */
      readRequestStorageInfo->AvailableLength = 0u;
      ReadRequest->Buffer = NULL_PTR;

      /* #90 Update the data type info if the input port is non-virtual */
      if (isVirtual == FALSE)
      {
        vStreamProc_Stream_SetDataTypeInfoByStream(streamId, &readRequestStorageInfo->DataTypeInfo);
      }

      if (readRequestStorageInfo->RequestLength > 0u)
      {
        retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
      }
      else
      {
        retVal = VSTREAMPROC_OK;
      }
    }

    if (retVal != VSTREAMPROC_FAILED)
    {
      /* #100 Get positions and meta data. */
      vStreamProc_Stream_GetConsumePosition(
        streamOutputPortId,
        readRequestStorageInfo->AvailableLength,
        StreamPositionInfo);

      vStreamProc_Stream_GetMetaDataOfInputPort(InputPortHandle, MetaData);

      /* #110 Check the data type against the expected and overwrite the result if necessary. */
      if ( (readRequestStorageInfo->DataTypeInfo.Id != expectedDataTypeId)
        && (expectedDataTypeId != (vStreamProc_DataTypeIdType)vStreamProcConf_vStreamProcDataType_Undefined))
      {
        retVal = VSTREAMPROC_FAILED;

        /* #120 Release port in case of mismatch. */
        (void)vStreamProc_Stream_ReleaseInputPort(InputPortHandle);
      }
    }
  }
  /* #130 Otherwise: */
  else
  {
    /* #140 Report insufficient input if any data was requested. Success otherwise. */
    if (readRequestStorageInfo->RequestLength > 0u)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
    }
    else
    {
      /* Failed result would interfere with batch requests for nodes with unconnected optional ports. */
      retVal = VSTREAMPROC_OK;
    }

    /* #150 Reset storage info to default values. */
    vStreamProc_Stream_InitStorageInfo(readRequestStorageInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_AcknowledgeInputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_AcknowledgeInputPort(                                 /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal                  = VSTREAMPROC_FAILED;                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType      readRequestStorageInfo  = &ReadRequest->StorageInfo;
  vStreamProc_StorageInfoConstPtrType metaDataStorageInfo     = &MetaData->StorageInfo;
  vStreamProc_ProcessingNodeIdType    processingNodeId        = InputPortHandle->ProcessingNodeId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Report success if zero acknowledge length was requested, even when port isn't connected
   *      or wave state is only starting (and not propagated yet). */
  /* Hint: If the request length is > 0, FAILED will be reported, unless request is successful, which is the default. */
  if ((readRequestStorageInfo->RequestLength == 0u) && (metaDataStorageInfo->RequestLength == 0u))
  {
    retVal = VSTREAMPROC_OK;
  }

  /* #20 If port is connected */
  if (InputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
    vStreamProc_StreamIdType streamId =
      vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId);
    vStreamProc_WaveLevelType waveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(processingNodeId);
    vStreamProc_WaveIdType waveId = vStreamProc_GetWaveStartIdxOfStream(streamId) + (vStreamProc_WaveIdType)waveLevel;
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);
    boolean producerWaveSync = vStreamProc_Stream_IsProducerWaveSync(InputPortHandle);

    /* #30 Check if the wave is in the correct state. */
    /* Hint: If the wave state is only starting (and not propagated yet),
             access to possibly available data shall be denied. */
    if ( (producerWaveSync == TRUE)
      && ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ACTIVE))
        || (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_ENDING))))
    {

      vStreamProc_NamedInputPortIdType namedInputPortId = InputPortHandle->NamedInputPortId;

      /* #40 Check if a virtual stream is requested. */
      if (vStreamProc_Stream_IsInputPortVirtual(namedInputPortId) == TRUE)
      {
        /* #50 Set the data type. */
        readRequestStorageInfo->DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;
        readRequestStorageInfo->DataTypeInfo.Size = 0u;

        /* #60 Set the available length manually. */
        readRequestStorageInfo->AvailableLength =
          vStreamProc_GetProducePositionOfWaveVarInfo(waveId)
          - vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

        retVal = VSTREAMPROC_OK;
      }
      /* Otherwise: */
      else
      {
        /* #70 Issue stream request to associated storage node. */
        retVal = vStreamProc_CallReadAckOfStorageNode(
          InputPortHandle->StorageNodeId,
          readRequestStorageInfo->RequestLength,
          InputPortHandle->StorageOutputPortId);
      }

      /* #80 Update consume position. */
      if (retVal == VSTREAMPROC_OK)
      {
        vStreamProc_ConsumePositionOfStreamOutputPortInfoType consumePosition;
        vStreamProc_StreamPositionType triggerPosition =
          vStreamProc_GetSnapshotTriggerPositionOfStreamOutputPortInfo(streamOutputPortId);

        vStreamProc_Stream_UpdateConsumePosition(streamOutputPortId, readRequestStorageInfo);

        consumePosition = vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

        /* #90 Check if a snapshot has to be created. */
        if ( (triggerPosition > 0u)
          && (consumePosition >= triggerPosition))
        {
          /* #100 If true, set the snapshot signal at the input port. */
          if (vStreamProc_Scheduler_SetInputPortSignal(
            namedInputPortId,
            vStreamProcConf_vStreamProcSignal_vStreamProc_InputSnapshotRequested) == E_NOT_OK)
          {
            /* #110 If there is no signal handler or the signal handler is unregistered,
                   start snapshot creation directly. */
            vStreamProc_Snapshot_CreateSnapshotWithInputTrigger(processingNodeId, namedInputPortId);
          }
        }
      }

      /* #120 If storage release is requested, reset buffer and available length. */
      if (readRequestStorageInfo->ReleaseFlag == TRUE)
      {
        ReadRequest->Buffer = NULL_PTR;
        readRequestStorageInfo->AvailableLength = 0u;
      }
      /* #130 Otherwise, re-issue data request. */
      else
      {
        if (retVal == VSTREAMPROC_OK)
        {
          readRequestStorageInfo->RequestLength = 0u;

          retVal = vStreamProc_Stream_RequestInputPortData(InputPortHandle, ReadRequest, MetaData, StreamPositionInfo);
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_ReleaseInputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_ReleaseInputPort(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle)                                           /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_FAILED;                                                            /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if ( (InputPortHandle->IsConnected == TRUE)
    && (InputPortHandle->IsVirtual == FALSE))
  {
    /* #20 Issue stream request to passed pipe/port */
    retVal = vStreamProc_CallReadAckOfStorageNode(
      InputPortHandle->StorageNodeId,
      0u,
      InputPortHandle->StorageOutputPortId);
  }
  /* #30 Otherwise, report success. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_GetOutputPortInfo(                                    /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal                  = VSTREAMPROC_FAILED;                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType      writeRequestStorageInfo = &WriteRequest->StorageInfo;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if (OutputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_DataTypeIdType          expectedDataTypeId  = writeRequestStorageInfo->DataTypeInfo.Id;
    vStreamProc_ProcessingNodeIdType    processingNodeId    = OutputPortHandle->ProcessingNodeId;
    vStreamProc_StreamIdType            streamId            = OutputPortHandle->StreamId;
    vStreamProc_WaveLevelType waveLevel = OutputPortHandle->WaveLevel;
    vStreamProc_WaveIdType waveId = OutputPortHandle->WaveId;
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);
    boolean isVirtual = OutputPortHandle->IsVirtual;

    /* #20 Set the data type if the output port is virtual. */
    if (isVirtual == TRUE)
    {
      writeRequestStorageInfo->DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;
      writeRequestStorageInfo->DataTypeInfo.Size = 0u;
    }

    /* #30 Check if the wave is in the correct state. */
    if ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
      || (waveState == VSTREAMPROC_WAVESTATE_STARTING))
    {
      vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);

      /* #40 Evaluate the effect of a potential stream limit on the info request. */
      retVal = vStreamProc_Limit_EvaluateRequestLimit(streamId, writeRequestStorageInfo, FALSE);

      if (retVal == VSTREAMPROC_OK)
      {
        vStreamProc_LengthType limitedLength = writeRequestStorageInfo->AvailableLength;

        /* #50 Check if the output port non-virtual. */
        if (isVirtual == FALSE)
        {
          /* #60 Issue write requested to associated storage node. */
          retVal = vStreamProc_CallWriteInfoOfStorageNode(storageNodeId, writeRequestStorageInfo);

          /* #70 Limit available length if limit needs to be applied. */
          if (limitedLength < writeRequestStorageInfo->AvailableLength)
          {
            writeRequestStorageInfo->AvailableLength = limitedLength;
          }
        }
      }
    }
    /* #80 Otherwise: */
    else
    {
      /* #90 Report no available data */
      writeRequestStorageInfo->AvailableLength = 0u;
      WriteRequest->Buffer = NULL_PTR;

      /* #100 Update the data type info if the output port is non-virtual */
      if (isVirtual == FALSE)
      {
        vStreamProc_Stream_SetDataTypeInfoByStream(streamId, &writeRequestStorageInfo->DataTypeInfo);
      }

      /* #110 Set the return value depending on the requested length. */
      if (writeRequestStorageInfo->RequestLength > 0u)
      {
        retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
      }
      else
      {
        retVal = VSTREAMPROC_OK;
      }
    }

    /* #120 Get positions and meta data. */
    if (retVal != VSTREAMPROC_FAILED)
    {
      vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(processingNodeId);
      vStreamProc_WaveTypeDefIdType waveTypeDefId =
        vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
          vStreamProc_GetWaveLevelDefStartIdxOfPipe(pipeId) + waveLevel);

      vStreamProc_Stream_GetProducePosition(
        streamId,
        waveLevel,
        writeRequestStorageInfo->AvailableLength,
        StreamPositionInfo);
      vStreamProc_Stream_GetMetaData(pipeId, streamId, waveTypeDefId, MetaData);

      /* #130 Check the data type against the expected and overwrite the result if necessary. */
      if ( (writeRequestStorageInfo->DataTypeInfo.Id != expectedDataTypeId)
        && (expectedDataTypeId != (vStreamProc_DataTypeIdType)vStreamProcConf_vStreamProcDataType_Undefined))
      {
        retVal = VSTREAMPROC_FAILED;
      }
    }
  }
  /* #140 Otherwise: */
  else
  {
    /* #150 Report insufficient output if any storage was requested. Success otherwise. */
    if (writeRequestStorageInfo->RequestLength > 0u)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    }
    else
    {
      /* Failed result would interfere with batch requests for nodes with unconnected optional ports. */
      retVal = VSTREAMPROC_OK;
    }

    /* #160 Reset storage info to default values. */
    vStreamProc_Stream_InitStorageInfo(writeRequestStorageInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_RequestOutputPortData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_RequestOutputPortData(                                /* PRQA S 6030, 6050, 6080 */ /* MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal                  = VSTREAMPROC_FAILED;                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType      writeRequestStorageInfo = &WriteRequest->StorageInfo;
  vStreamProc_DataTypeIdType          expectedDataTypeId      = writeRequestStorageInfo->DataTypeInfo.Id;
  vStreamProc_ProcessingNodeIdType    processingNodeId        = OutputPortHandle->ProcessingNodeId;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if (OutputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_StreamIdType streamId = OutputPortHandle->StreamId;
    vStreamProc_WaveLevelType waveLevel = OutputPortHandle->WaveLevel;
    vStreamProc_WaveIdType waveId = OutputPortHandle->WaveId;
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);
    boolean isVirtual = OutputPortHandle->IsVirtual;

    /* #20 Set the data type if the output port is virtual. */
    if (isVirtual == TRUE)
    {
      writeRequestStorageInfo->DataTypeInfo.Id = VSTREAMPROC_NO_DATATYPE;
      writeRequestStorageInfo->DataTypeInfo.Size = 0u;
    }

    /* #30 Check if the wave is in the correct state. */
    if ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
      || (waveState == VSTREAMPROC_WAVESTATE_STARTING))
    {
      vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);

      /* #40 Evaluate the effect of a potential stream limit on the write request. */
      retVal = vStreamProc_Limit_EvaluateRequestLimit(streamId, writeRequestStorageInfo, TRUE);

      if (retVal == VSTREAMPROC_OK)
      {

        vStreamProc_LengthType limitedLength = writeRequestStorageInfo->AvailableLength;

        /* #50 Check if the output port non-virtual. */
        if (isVirtual == FALSE)
        {
          /* #60 Issue write requested to associated storage node. */
          retVal = vStreamProc_CallWriteRequestOfStorageNode(storageNodeId, WriteRequest);

          /* #70 Limit available length if limit needs to be applied. */
          if (limitedLength < writeRequestStorageInfo->AvailableLength)
          {
            writeRequestStorageInfo->AvailableLength = limitedLength;
          }
        }
      }

      if (retVal == VSTREAMPROC_OK)
      {
        /* #80 Track copy of write lock. */
        vStreamProc_SetWriteRequestLockOfStreamInfo(streamId, TRUE);
      }
    }
    /* #90 Otherwise: */
    else
    {
      /* #100 Report no available data */
      writeRequestStorageInfo->AvailableLength = 0u;
      WriteRequest->Buffer = NULL_PTR;

      /* #110 Update the data type info if the output port is non-virtual */
      if (isVirtual == FALSE)
      {
        vStreamProc_Stream_SetDataTypeInfoByStream(streamId, &writeRequestStorageInfo->DataTypeInfo);
      }

      /* #120 Set the return value depending on the requested length. */
      if (writeRequestStorageInfo->RequestLength > 0u)
      {
        retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
      }
      else
      {
        retVal = VSTREAMPROC_OK;
      }
    }

    /* #130 Get positions and meta data. */
    if (retVal != VSTREAMPROC_FAILED)
    {
      vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(processingNodeId);
      vStreamProc_WaveTypeDefIdType waveTypeDefId =
        vStreamProc_GetWaveTypeDefIdxOfWaveLevelDef(
          vStreamProc_GetWaveLevelDefStartIdxOfPipe(pipeId) + waveLevel);

      vStreamProc_Stream_GetProducePosition(
        streamId,
        waveLevel,
        writeRequestStorageInfo->AvailableLength,
        StreamPositionInfo);
      vStreamProc_Stream_GetMetaData(pipeId, streamId, waveTypeDefId, MetaData);

      /* #140 Check the data type against the expected and overwrite the result if necessary. */
      if (writeRequestStorageInfo->DataTypeInfo.Id != expectedDataTypeId)
      {

        retVal = VSTREAMPROC_FAILED;

        /* #150 Release port in case of mismatch. */
        (void)vStreamProc_Stream_ReleaseOutputPort(OutputPortHandle);
      }
    }
  }
  else
  {
    /* #160 Report insufficient output if any storage was requested. Success otherwise. */
    if (writeRequestStorageInfo->RequestLength > 0u)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    }
    else
    {
      /* Failed result would interfere with batch requests for nodes with unconnected optional ports. */
      retVal = VSTREAMPROC_OK;
    }

    /* #170 Reset storage info to default values. */
    vStreamProc_Stream_InitStorageInfo(writeRequestStorageInfo);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_AcknowledgeOutputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_AcknowledgeOutputPort(                                /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType              retVal                  = VSTREAMPROC_FAILED;                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageInfoPtrType      writeRequestStorageInfo = &WriteRequest->StorageInfo;
  vStreamProc_LengthType              producedLength = writeRequestStorageInfo->RequestLength;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Report success if zero acknowledge length was requested, even when port isn't connected
   *      or wave isn't in the correct state. */
  /* Hint: If the request length is > 0, FAILED will be reported, unless request is successful, which is the default. */
  if (producedLength == 0u)
  {
    retVal = VSTREAMPROC_OK;
  }

  /* #20 If port is connected. */
  if (OutputPortHandle->IsConnected == TRUE)
  {
    vStreamProc_StreamIdType streamId = OutputPortHandle->StreamId;
    vStreamProc_WaveIdType waveId = OutputPortHandle->WaveId;
    vStreamProc_WaveStateType waveState = vStreamProc_GetStateOfWaveVarInfo(waveId);

    /* #30 Check if the wave is in the correct state. */
    if ( (vStreamProc_IsWaveStateFlagSet(waveState, VSTREAMPROC_WAVESTATE_FLAG_ACTIVE))
      || (waveState == VSTREAMPROC_WAVESTATE_STARTING))
    {
      vStreamProc_LimitResultType limitResult;

      /* #40 Evaluate the effect of a potential soft stream limit on the write acknowledge. */
      /* Hint: FAILED is returned if more data is acknowledged than allowed. */
      retVal = vStreamProc_Limit_EvaluateAcknowledgeLimit(streamId, writeRequestStorageInfo, &limitResult);

      /* #50 Handle the write acknowledge request. */
      if (retVal == VSTREAMPROC_OK)
      {
        /* #60 Check if the output port is virtual. */
        if (OutputPortHandle->IsVirtual == FALSE)
        {
          retVal = vStreamProc_CallWriteAckOfStorageNode(OutputPortHandle->StorageNodeId, producedLength);

          if (producedLength > 0u)
          {
            vStreamProc_SetDataProducedOfStreamInfo(streamId, TRUE);
          }
        }
      }

      /* #70 Trigger post acknowledge actions. */
      if (retVal == VSTREAMPROC_OK)
      {
        vStreamProc_Stream_TriggerPostAcknowledgeOutputPortActions(OutputPortHandle, WriteRequest, limitResult);
      }

      /* #80 If storage release is requested, reset buffer and available length. */
      if (writeRequestStorageInfo->ReleaseFlag == TRUE)
      {
        WriteRequest->Buffer = NULL_PTR;
        writeRequestStorageInfo->AvailableLength = 0u;
      }
      /* #90 Otherwise, re-issue storage request. */
      else
      {
        if (retVal == VSTREAMPROC_OK)
        {
          writeRequestStorageInfo->RequestLength = 0u;

          retVal = vStreamProc_Stream_RequestOutputPortData(OutputPortHandle, WriteRequest, MetaData, StreamPositionInfo);
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_ReleaseOutputPort()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_ReleaseOutputPort(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle)                                           /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType        retVal = VSTREAMPROC_FAILED;                                                            /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If port is connected */
  if ( (OutputPortHandle->IsConnected == TRUE)
    && (OutputPortHandle->IsVirtual == FALSE))
  {
    vStreamProc_StreamIdType streamId = OutputPortHandle->StreamId;
    vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);

    /* #20 Issue write acknowledgment to passed pipe/port */
    retVal = vStreamProc_CallWriteAckOfStorageNode(storageNodeId, 0u);

    /* #30 Track copy of write lock. */
    if (retVal == VSTREAMPROC_OK)
    {
      vStreamProc_SetWriteRequestLockOfStreamInfo(streamId, FALSE);
    }
  }
  /* #40 Otherwise, report success. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the named input port is connected. */
  (void)vStreamProc_Stream_GetInputPortHandle(ProcessingNodeId, InputPortSymbolicName, &inputPortHandle);

  if ((inputPortHandle.IsConnected == TRUE) && (inputPortHandle.HasActiveProducer == TRUE))
  {
    vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
    vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(inputPortHandle.StreamOutputPortId);
    vStreamProc_WaveHandleType consumerWaveHandle =
      vStreamProc_GetWaveHandleOfStreamOutputPortInfo(inputPortHandle.StreamOutputPortId);
    vStreamProc_WaveHandleType producerWaveHandle =
      vStreamProc_GetHandleOfWaveVarInfo(inputPortHandle.WaveId);
    vStreamProc_WaveTypeSymbolicNameType waveTypeSymbolicName = WaveInfo->WaveTypeSymbolicName;

    /* #20 Check if the producer is still uninit. */
    if (producerWaveHandle == 0u)
    {
      /* #30 Provide default values. */
      vStreamProc_Stream_PrepareWaveInfo(waveTypeSymbolicName, WaveInfo);

      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
    }
    else
    {
      /* #40 Get the wave infos. */
      vStreamProc_Stream_GetWaveInfo(pipeId, streamId, WaveInfo);
      vStreamProc_Stream_GetMetaData(pipeId, streamId, waveTypeSymbolicName, &WaveInfo->MetaData);

      if (WaveInfo->MetaData.StorageInfo.AvailableLength > 0u)
      {
        /* Prepare request length for automatic meta data propagation to output port. */
        WaveInfo->MetaData.StorageInfo.RequestLength = 1u;
      }

      /* #50 Check if the wave handles on producer and consumer side are consistent. */
      if (producerWaveHandle == consumerWaveHandle)
      {
        retVal = VSTREAMPROC_OK;
      }
      /* Otherwise: Report pending */
      /* Hint: In this case, the pipe was restored and the nodes are not synchronized yet.
       *       Report the wave info regarless, as it can be useful for the caller. */
      else
      {
        retVal = VSTREAMPROC_PENDING;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortWaveInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the output port is connected. */
  retVal = vStreamProc_Stream_GetOutputPortHandle(ProcessingNodeId, OutputPortSymbolicName, &outputPortHandle);

  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);

    /* #20 Get the wave infos. */
    vStreamProc_Stream_GetWaveInfo(pipeId, outputPortHandle.StreamId, WaveInfo);
    vStreamProc_Stream_GetMetaData(pipeId, outputPortHandle.StreamId, WaveInfo->WaveTypeSymbolicName, &WaveInfo->MetaData);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_StartOutputPortWave()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_StartOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfNamedOutputPort(
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedOutputPortIterType)OutputPortSymbolicName);
  vStreamProc_WaveLevelType waveLevelRangeEnd = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if start wave is allowed */
  if (vStreamProc_Stream_IsStartWaveAllowed(ProcessingNodeId, streamId, &waveLevelRangeEnd) == TRUE)
  {
    retVal = VSTREAMPROC_OK;

    /* #20 Update the runtime data of the waves. */
    vStreamProc_Stream_UpdateWaveData(ProcessingNodeId, streamId, waveLevelRangeEnd, WaveInfo);

    /* #30 Check if wave propagation is allowed. */
    if (vStreamProc_Stream_IsWavePropagationAllowed(streamId, waveLevelRangeEnd) == TRUE)
    {
      /* #40 Propagate the wave start signal. */
      vStreamProc_Stream_PropagateWave(
        streamId,
        0u,
        waveLevelRangeEnd,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveStart);
    }
    /* #50 Otherwise, set wave blocked.*/
    else
    {
      vStreamProc_SetWaveBlockedOfStreamInfo(streamId, TRUE);
    }
  }
  /* #60 Otherwise, return INSUFFICIENT_OUTPUT. */
  else
  {
    retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_EndOutputPortWave()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, AUTOMATIC) vStreamProc_Stream_EndOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_PipeIdType pipeId = vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId);
  vStreamProc_NamedOutputPortIdType namedOutputPortId =
    vStreamProc_GetNamedOutputPortStartIdxOfProcessingNode(ProcessingNodeId)
    + (vStreamProc_NamedOutputPortIdType)OutputPortSymbolicName;
  vStreamProc_StreamIdType streamId =
    (vStreamProc_StreamIdType)vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortId);
  vStreamProc_WaveLevelType targetWaveLevel = vStreamProc_Stream_GetWaveLevel(pipeId, WaveTypeSymbolicName);
  vStreamProc_WaveLevelType producerWaveLevel = vStreamProc_Stream_GetWaveLevelOfProcessingNode(ProcessingNodeId);
  vStreamProc_WaveLevelType waveLevelRangeStart;
  vStreamProc_WaveLevelType waveLevelRangeEnd;

  /* ----- Implementation ----------------------------------------------- */

  /* #10 Check if the EndWave is called from an entry nodes or from the high level wave end propagation. */
  /* Hint: For entry nodes, only the target wave level is considered, as every wave level is started explicitly
           by the user. */
  if ( (vStreamProc_AccessNode_IsEntryNode(ProcessingNodeId) == TRUE)
    || (targetWaveLevel > producerWaveLevel))
  {
    waveLevelRangeStart = targetWaveLevel;
    waveLevelRangeEnd = targetWaveLevel;
  }
  else
  {
     waveLevelRangeStart = 0u;
     waveLevelRangeEnd = targetWaveLevel;
  }

  /* #20 Propagate the wave end signal. */
  vStreamProc_Stream_PropagateWave(
    streamId,
    waveLevelRangeStart,
    waveLevelRangeEnd,
    vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd);
}

/**********************************************************************************************************************
 *  vStreamProc_Stream_DiscardInputPortData()
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_DiscardInputPortData(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_StreamPositionType NewConsumePosition)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_StreamOutputPortIdType streamOutputPortId = InputPortHandle->StreamOutputPortId;
  vStreamProc_WaveStateType consumerWaveState = vStreamProc_GetWaveStateOfStreamOutputPortInfo(streamOutputPortId);
  vStreamProc_StreamPositionType oldConsumePosition = vStreamProc_GetConsumePositionOfStreamOutputPortInfo(streamOutputPortId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check the preconditions */
  if ( (InputPortHandle->IsConnected == TRUE)
    && (InputPortHandle->HasActiveProducer == TRUE)
    && (consumerWaveState != VSTREAMPROC_UNINIT)
    && (oldConsumePosition <= NewConsumePosition))
  {
    vStreamProc_WaveHandleType producerWaveHandle = vStreamProc_GetHandleOfWaveVarInfo(InputPortHandle->WaveId);
    vStreamProc_WaveHandleType consumerWaveHandle = vStreamProc_GetWaveHandleOfStreamOutputPortInfo(streamOutputPortId);

    /* #20 Set the new consume position. */
    vStreamProc_SetConsumePositionOfStreamOutputPortInfo(streamOutputPortId, NewConsumePosition);

    /* #30 Check if the producer and consumer wave are already sync */
    if (producerWaveHandle == consumerWaveHandle)
    {
      vStreamProc_WaveIdType waveId = InputPortHandle->WaveId;
      vStreamProc_StreamPositionType producePosition = vStreamProc_GetProducePositionOfWaveVarInfo(waveId);

      /* #40 Check if there is already data to discard. */
      if (producePosition > oldConsumePosition)
      {
        vStreamProc_OutputPortHandleType outputPortHandle;

        /* #50 Trigger the wave synchronization. */
        /* Hint: In this context, the stream has always an active producer. */
        (void)vStreamProc_Stream_GetOutputPortHandleByStream(InputPortHandle->StreamId, &outputPortHandle);

        vStreamProc_Stream_SyncConsumers(&outputPortHandle);

        /* #60 Check if the DataAvailable signal has to be discarded. */
        if (producePosition < NewConsumePosition)
        {
          vStreamProc_Scheduler_DiscardInputPortSignal(
            InputPortHandle->NamedInputPortId,
            vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable);
        }
      }
    }

    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Stream.c
 *********************************************************************************************************************/
