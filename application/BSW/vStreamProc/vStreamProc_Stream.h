/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Stream.h
 *        \brief  vStreamProc stream access interface header file
 *
 *      \details  Definition of interfaces for vStreamProc stream access
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_STREAM_H
#define VSTREAMPROC_STREAM_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallReadInfoOfStorageNode(inStorageNodeIdx, storageInfo, outputPortIdx) \
    vStreamProc_GetReadInfoOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(inStorageNodeIdx))(inStorageNodeIdx, storageInfo, outputPortIdx)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallReadRequestOfStorageNode(inStorageNodeIdx, readRequestPtr, outputPortIdx) \
    vStreamProc_GetReadRequestOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(inStorageNodeIdx))(inStorageNodeIdx, readRequestPtr, outputPortIdx)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallReadAckOfStorageNode(inStorageNodeIdx, consumedLen, outputPortIdx) \
    vStreamProc_GetReadAckOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(inStorageNodeIdx))(inStorageNodeIdx, consumedLen, outputPortIdx)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallWriteInfoOfStorageNode(outStorageNodeIdx, storageInfo) \
    vStreamProc_GetWriteInfoOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(outStorageNodeIdx))(outStorageNodeIdx, storageInfo)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallWriteRequestOfStorageNode(outStorageNodeIdx, writeRequestPtr) \
    vStreamProc_GetWriteRequestOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(outStorageNodeIdx))(outStorageNodeIdx, writeRequestPtr)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallWriteAckOfStorageNode(outStorageNodeIdx, requestedLen) \
    vStreamProc_GetWriteAckOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(outStorageNodeIdx))(outStorageNodeIdx, requestedLen)

/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define vStreamProc_CallDiscardAllDataOfStorageNode(storageNodeIdx) \
    vStreamProc_GetDiscardAllDataOfStorageNodeTypeDef(vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(storageNodeIdx))(storageNodeIdx)

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CORE API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Stream_InitProcessingNodeInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize node information structure.
 * \details       -
 * \param[in]     ProcessingNodeId  Id of a processing node.
 * \param[out]    InputPortResults  Array for input port specific results.
 * \param[out]    OuputPortResults  Array for output port specific results.
 * \param[in]     ProcNodeInfo      The processing node information to be initialized.
 * \pre           Arrays for port specific results must be big enough.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_InitProcessingNodeInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortResults,
  P2VAR(vStreamProc_ReturnType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortResults,
  P2VAR(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 * vStreamProc_Stream_InitStorageInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize storage info structure with default values.
 * \details       -
 * \param[out]    StorageInfo   The storage info structure to be initialized.
 * \pre        -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_InitStorageInfo(
  vStreamProc_StorageInfoPtrType StorageInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_PrepareWaveInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Initializes the provided wave info structure with default values and sets the provided
 *                wave type symbolic name.
 * \details       -
 * \param[in]     WaveTypeSymbolicName   The wave type symbolic name.
 * \param[out]    WaveInfo               Pointer to the wave info structure.
 * \pre        -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_PrepareWaveInfo(
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortHandle
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided input port is connected and provides the input port handle if true.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     InputPortSymbolicName     The named input port of the processing node.
 * \param[out]    InputPortHandle           Pointer to the input port handle.
 * \return        VSTREAMPROC_OK            The port is connected.
 * \return        VSTREAMPROC_FAILED        The port is not connected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortHandle(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_InputPortHandlePtrType InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortHandle
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided output port is connected and provides the input port handle if true.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     OutputPortSymbolicName    The named output port of the processing node.
 * \param[out]    OutputPortHandle          Pointer to the input port handle.
 * \return        VSTREAMPROC_OK            The port is connected.
 * \return        VSTREAMPROC_FAILED        The port is not connected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortHandle(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_OutputPortHandlePtrType OutputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsInputPortVirtual()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided named input port is virtual.
 * \details       -
 * \param[in]     NamedInputPortId   The id of the named input port.
 * \return        TRUE    The named input port is virtual.
 * \return        FALSE   The named input port is not virtual.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsInputPortVirtual(
  vStreamProc_NamedInputPortIdType NamedInputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsOutputPortVirtual()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided named output port is virtual.
 * \details       -
 * \param[in]     NamedInputPortId   The id of the named output port.
 * \return        TRUE    The named output port is virtual.
 * \return        FALSE   The named output port is not virtual.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsOutputPortVirtual(
  vStreamProc_NamedOutputPortIdType NamedOutputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsInputPortConnected()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided input port connected.
 * \details       The input port is considered as connected if it is configured in at least one consumer edge and
 *                if at least one edge is active.
 * \param[in]     ProcessingNodeId        The id of the processing node.
 * \param[in]     InputPortSymbolicName   The symbolic name of the input port.
 * \return        TRUE    The named output port is active.
 * \return        FALSE   The named output port is not active.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsInputPortConnected(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsOutputPortConnected()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided output port connected.
 * \details       The output port is considered as connected if it is configured in at least one producer edge and
 *                if the output port is the active producer of the corresponding stream.
 * \param[in]     ProcessingNodeId        The id of the processing node.
 * \param[in]     OutputPortSymbolicName  The symbolic name of the output port.
 * \return        TRUE    The named output port is active.
 * \return        FALSE   The named output port is not active.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Stream_IsOutputPortConnected(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName);

/**********************************************************************************************************************
 *  vStreamProc_Stream_IsStreamOutputPortActive()
 *********************************************************************************************************************/
/*!
 * \brief         Checks if the provided stream output port is active.
 * \details       The stream output port is active, if it is mapped to the input port of the consumer node.
 * \param[in]     StreamOutputPortId    The id of the stream output port.
 * \return        TRUE    The named output port is active.
 * \return        FALSE   The named output port is not active.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(boolean, AUTOMATIC) vStreamProc_Stream_IsStreamOutputPortActive(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetProducePosition()
 *********************************************************************************************************************/
/*!
 * \brief         Get produce position information.
 * \details       -
 * \param[in]     StreamId            Id of stream.
 * \param[in]     WaveLevel           Requested wave level.
 * \param[in]     AvailableLength     The length available for the current write request. 0 if not used.
 * \param[out]    StreamPositionInfo  Stream position info reference to provide data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetProducePosition(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveLevelType WaveLevel,
  vStreamProc_LengthType AvailableLength,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetConsumePosition()
 *********************************************************************************************************************/
/*!
 * \brief         Get consume position information.
 * \details       -
 * \param[in]     StreamOutputPortId  The processing node's ID.
 * \param[in]     AvailableLength     The length available for the current read request. 0 if not used.
 * \param[out]    StreamPositionInfo  Stream position info reference to provide data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetConsumePosition(
  vStreamProc_StreamOutputPortIdType StreamOutputPortId,
  vStreamProc_LengthType AvailableLength,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetMetaData()
 *********************************************************************************************************************/
/*!
 * \brief         Get meta data.
 * \details       -
 * \param[in]     PipeId              Id of pipe.
 * \param[in]     StreamId            Id of stream.
 * \param[in]     WaveTypeDefId       Id of the requested wave type
 * \param[out]    MetaData            Reference to meta data.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_GetMetaData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_StreamIdType StreamId,
  vStreamProc_WaveTypeDefIdType WaveTypeDefId,
  vStreamProc_ReadRequestPtrType MetaData);

/***********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevel
 **********************************************************************************************************************/
/*!
 * \brief         Returns the wave level of the provided wave type in the provided pipe.
 * \details       -
 * \param[in]     PipeId              Id of pipe.
 * \param[in]     WaveTypeDefId       Id of the requested wave type
 * \return        Wave level offset         If the wave type is used in the pipe wave hierarchy.
 * \return        VSTREAMPROC_NO_WAVELEVEL  Otherwise.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevel(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_WaveTypeDefIdType WaveTypeDefId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetWaveLevelOfProcessingNode
 *********************************************************************************************************************/
/*!
 * \brief         Returns the registered wave level of the provided node.
 * \details       -
 * \param[in]     ProcessingNodeId    The id of the processing node.
 * \return        The wave level of the processing node
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_WaveLevelType, AUTOMATIC) vStreamProc_Stream_GetWaveLevelOfProcessingNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetAvailableStorageLength()
 *********************************************************************************************************************/
/*!
 * \brief         Return available storage length at stream producers output port.
 * \details       -
 * \param[in]     StreamId             Id of stream.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_LengthType, AUTOMATIC) vStreamProc_Stream_GetAvailableStorageLength(
  vStreamProc_StreamIdType StreamId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_SetEntryPointWaveData()
 *********************************************************************************************************************/
/*!
 * \brief         Writes the provided wave data from the wave info to the target wave at the entry node output port.
 * \details       -
 * \param[in]     ProcessingNodeId    Id of processing node.
 * \param[in]     WaveInfo            Pointer to the wave info structure 
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_SetEntryPointWaveData(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_DecreasePendingWaveSignalCount
 *********************************************************************************************************************/
/*!
 * \brief         Decreases the pending count value in the corresponding wave var info of the named input port.
 * \details       If the count reaches zero, the wave state is updated. In case of WaveEnd signal, the clogged pipe
                  detection is triggered.
 * \param[in]     ProcessingNodeInfo    The processing node info structure.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_DecreasePendingWaveSignalCount(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_PerformWaveRouting
 *********************************************************************************************************************/
/*!
 * \brief         Performs the wave routing from the provided input port to the mapped output port.
 * \details       If no wave routing is defined for the input port, no routing will be performed.
 * \param[in]     ProcessingNodeId          The processing node's ID.
 * \param[in]     InputPortSymbolicName     The named input port of the processing node.
 * \param[in]     SignalId                  The wave signal which shall be routed. Supported signals:
 *                                          WaveStart...StartOutputPortWave will be called for all mapped output ports.
 *                                          WaveEnd...  EndOutputPortWave will be called for all mapped output ports.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_PerformWaveRouting(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_HandleDataAvailable()
 *********************************************************************************************************************/
/*!
 * \brief        Notifies all consumer nodes about new available data.
 * \details      The DataAvailable signal will be only set if the ACTIVE flag is set in the wave state.
 * \param[in]    StreamId   Id of the stream.
 * \pre          -
 * \note         The additional argument shall be a pointer to a scheduler.
 * \context      TASK|ISR
 * \reentrant    TRUE
 * \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_HandleDataAvailable(
  vStreamProc_StreamIdType StreamId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_HandleStorageAvailable()
 *********************************************************************************************************************/
/*!
 * \brief        Notifies the producer node about new available storage buffer.
 * \details      If there is no active producer, no signal will be set.
 * \param[in]    StorageNodeId   Id of the storage node with available buffer.
 * \pre          -
 * \note         The additional argument shall be a pointer to a scheduler.
 * \context      TASK|ISR
 * \reentrant    TRUE
 * \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_HandleStorageAvailable(
  vStreamProc_StorageNodeIdType StorageNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Stream_SyncOutputPortsWaves
 *********************************************************************************************************************/
/*!
 * \brief        Checks if data was produced at any output port of the provided processing node and tries to
 *               synchronize the consumer waves if necessary.
 * \details      Synchronization is only required after the pipe is restored and if the there are gaps in between the
 *               producer and the consumer wave handles/positions.
 * \param[in]    ProcessingNodeId   Id of the processing node.
 * \pre          -
 * \context      TASK|ISR
 * \reentrant    TRUE
 * \synchronous  TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Stream_SyncOutputPortsWaves(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  PORT ACCESS API
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Get information about a processing node input port.
 * \details       -
 * \param[in]     InputPortHandle     Pointer to the input port handle.
 * \param[in,out] ReadRequest         Pointer to the read request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                                                    StreamProcConf_vStreamProcDataType_Undefined if
 *                                                    no specific data type is expected.
 *                  - StorageInfo.RequestLength       Requested minimum buffer length.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.AvailableLength     Available buffer length.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_GetInputPortInfo(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_RequestInputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Requests a read action.
 * \details       This call does not read any data from the buffer. It rather provides pointer and length information.
 *                The application then has to do the data reading based on this information. If data is available, the
 *                provided pointer does not change until a read acknowledge is issued. Defragmentation may be blocked
 *                due to a pending read request. A read request that indicates data must always be acknowledged to release
 *                the defragmentation lock.
 * \param[in]     InputPortHandle     Pointer to the input port handle.
 * \param[in,out] ReadRequest         Pointer to the read request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.RequestLength       Requested minimum buffer length.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.AvailableLength     Available buffer length.
 *                  - Buffer                          Pointer to available buffer.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_RequestInputPortData(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_AcknowledgeInputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Acknowledges a read action.
 * \details       The processing node shall have issued a read request which then can be acknowledge by calling this
 *                function. Therefore, the amount of consumed data must be provided. By acknowledging a read request
 *                a possible defragmentation lock is being released.
 * \param[in]     InputPortHandle     Pointer to the input port handle.
 * \param[in,out] ReadRequest         Pointer to the read request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                                                    Only required when storage lock is kept.
 *                  - StorageInfo.RequestLength       The amount of consumed data which needs to be acknowledged.
 *                  - StorageInfo.ReleaseFlag         Flag to control release of storage lock.
 *                    - TRUE: Release storage lock.
 *                    - FALSE: Keep storage locked.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                                                    Only set when storage lock is kept.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                                                    Only set when storage lock is kept.
 *                  - StorageInfo.AvailableLength     Updated available buffer length.
 *                                                    Zero if storage lock was released.
 *                  - StorageInfo.RequestLength       Passed produced length value.
 *                                                    Zero if storage lock was released.
 *                  - Buffer                          Updated pointer to available buffer.
 *                                                    NULL_PTR if storage lock was released.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_AcknowledgeInputPort(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_ReadRequestPtrType ReadRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_ReleaseInputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Releases storage lock of an input port.
 * \details       -
 * \param[in]     InputPortHandle       Pointer to the input port handle.
 * \return        VSTREAMPROC_OK        Operation was successful.
 * \return        VSTREAMPROC_FAILED    Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_ReleaseInputPort(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Get information about a processing node output port.
 * \details       -
 * \param[in]     OutputPortHandle    Pointer to the output port handle.
 * \param[in,out] WriteRequest        Pointer to the write request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                                                    StreamProcConf_vStreamProcDataType_Undefined if
 *                                                    no specific data type is expected.
 *                  - StorageInfo.RequestLength       Requested minimum buffer length.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.AvailableLength     Available buffer length.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_GetOutputPortInfo(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_RequestOutputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Requests a write action.
 * \details       This call does not write any data to a buffer. It rather provides the pointer and length information
 *                to the application. The application can then write its data to the provided pointer while the
 *                maximum data amount is limited by the provided available length.
 * \param[in]     OutputPortHandle    Pointer to the output port handle.
 * \param[in,out] WriteRequest        Pointer to the write request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.RequestLength       Requested minimum buffer length.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                  - StorageInfo.AvailableLength     Available buffer length.
 *                  - Buffer                          Pointer to the available buffer.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Requested minimum buffer length isn't available.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_RequestOutputPortData(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_AcknowledgeOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Acknowledges a write action.
 * \details       The processing node shall have requested a write request which then can be acknowledge by calling
 *                this function. The write request only provides a maximum amount of data to be produced, but it does
 *                not specify a precise amount of data which shall be produced. Therefore, the amount of produced
 *                data must be provided by calling this function.
 * \param[in]     OutputPortHandle    Pointer to the output port handle.
 * \param[in,out] WriteRequest        Pointer to the write request.
 *                - Input
 *                  - StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                    connected to the port.
 *                                                    Only required when storage lock is kept.
 *                  - StorageInfo.RequestLength       The amount of produced data which needs to be acknowledged.
 *                  - StorageInfo.ReleaseFlag         Flag to control release of storage lock.
 *                    - TRUE: Release storage lock.
 *                    - FALSE: Keep storage locked.
 *                - Output
 *                  - StorageInfo.DataTypeInfo.Id     Id of the actual data type of the storage node
 *                                                    connected to the port.
 *                                                    Only set when storage lock is kept.
 *                  - StorageInfo.DataTypeInfo.Size   Byte size of data type of the storage node
 *                                                    connected to the port.
 *                                                    Only set when storage lock is kept.
 *                  - StorageInfo.AvailableLength     Updated available buffer length.
 *                                                    Zero if storage lock was released.
 *                  - StorageInfo.RequestLength       Passed produced length value.
 *                                                    Zero if storage lock was released.
 *                  - StorageInfo.ReleaseFlag         Passed release flag value.
 *                  - Buffer                          Updated pointer to available buffer.
 *                                                    NULL_PTR if storage lock was released.
 * \param[out]    MetaData            Pointer to the meta data.
 * \param[out]    StreamPositionInfo  Pointer to the stream position info.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_AcknowledgeOutputPort(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle,
  vStreamProc_WriteRequestPtrType WriteRequest,
  vStreamProc_ReadRequestPtrType MetaData,
  vStreamProc_StreamPositionInfoPtrType StreamPositionInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_ReleaseOutputPort()
 *********************************************************************************************************************/
/*!
 * \brief         Releases storage lock of an output port.
 * \details       -
 * \param[in]     OutputPortHandle  Pointer to the output port handle.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Stream_ReleaseOutputPort(
  vStreamProc_OutputPortHandleConstPtrType OutputPortHandle);

/***********************************************************************************************************************
 *  vStreamProc_Stream_GetInputPortWaveInfo
 **********************************************************************************************************************/
/*!
 * \brief         Reads the wave data from the provided input port and writes the data to the provided wave info
 *                structure.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     InputPortSymbolicName     The named input port of the processing node.
 * \param[out]    WaveInfo                  Pointer to the wave info structure.
 * \return        VSTREAMPROC_OK            Wave data was read successfully.
 * \return        VSTREAMPROC_FAILED        Wave data was not read successfully because the input port is
 *                                          not connected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetInputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortSymbolicNameType InputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_GetOutputPortWaveInfo
 *********************************************************************************************************************/
/*!
 * \brief         Reads the wave data from the provided output port and writes the data to the provided wave info
 *                structure.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     OutputPortSymbolicName    The named output port of the processing node.
 * \param[out]    WaveInfo                  Pointer to the wave info structure.
 * \return        VSTREAMPROC_OK            Wave data was read successfully.
 * \return        VSTREAMPROC_FAILED        Wave data was not read successfully because the output port is
 *                                          not connected.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_GetOutputPortWaveInfo(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_StartOutputPortWave()
 *********************************************************************************************************************/
/*!
 * \brief         Starts the wave with the data from the provided wave info structure and tries to propagate
 *                the WaveStart signal to the consumer nodes.
 * \details       If wave propagation is not possible due to pending streams at the consumer nodes, the wave will be
 *                marked as blocked and the wave propagation will be performed automatically as soon as the
 *                consumer streams are idle.
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     OutputPortSymbolicName    The named output port of the processing node.
 * \param[in]     WaveInfo                  Pointer to the wave info structure.
 * \return        VSTREAMPROC_OK                  Wave start was successfully .
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT Wave start was not successful because the wave at the output port
 *                                                is not idle.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_StartOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo);

/**********************************************************************************************************************
 *  vStreamProc_Stream_EndOutputPortWave()
 *********************************************************************************************************************/
/*!
 * \brief         Ends the wave with the data from the provided wave info structure and propagates
 *                the WaveEnd signal to the consumer nodes.
 * \details       -
 * \param[in]     ProcessingNodeId          Id of the processing node.
 * \param[in]     OutputPortSymbolicName    The named output port of the processing node.
 * \param[in]     WaveTypeSymbolicName      Symbolic name of the target wave type.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Stream_EndOutputPortWave(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_OutputPortSymbolicNameType OutputPortSymbolicName,
  vStreamProc_WaveTypeSymbolicNameType WaveTypeSymbolicName);

/**********************************************************************************************************************
 *  vStreamProc_Stream_DiscardInputPortData()
 *********************************************************************************************************************/
/*!
 * \brief         Discards existing and future data at the provided input port until the provided consume position.
 * \details       Pending DataAvailable signal will be discarded as well.
 * \param[in]     InputPortHandle           Handle of the input port.
 * \param[in]     NewConsumePosition        The consume position until the data shall be discarded.
 * \return        VSTREAMPROC_OK            Request was successful.
 * \return        VSTREAMPROC_FAILED        Request failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Stream_DiscardInputPortData(
  vStreamProc_InputPortHandleConstPtrType InputPortHandle,
  vStreamProc_StreamPositionType NewConsumePosition);

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_STREAM_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Stream.h
 *********************************************************************************************************************/

