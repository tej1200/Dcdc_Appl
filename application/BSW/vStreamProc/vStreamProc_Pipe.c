/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Pipe.c
 *        \brief  vStreamProc Pipe Sub Module Source Code File
 *
 *      \details  Implementation of the vStreamProc Pipe sub module.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
# define VSTREAMPROC_PIPE_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Pipe.h"
#include "vStreamProc_Scheduler.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_AccessNode.h"
#include "vStreamProc_Queue.h"
#include "vStreamProc_Session.h"
#include "vStreamProc_Snapshot.h"
#include "vStreamProc_Limit.h"
#include "vStreamProc_Mode.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

#if !defined (VSTREAMPROC_LOCAL_INLINE)
# define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/*! Function pointer for entry point operations (used by iterator). */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_EntryPointOperationPtrType)(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo);

/*! Function pointer for exit point operations (used by iterator). */
typedef P2FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE, vStreamProc_ExitPointOperationPtrType)(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo);

/*! Symbolic names of access point operations (used by iterator). */
typedef enum
{
#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  VSTREAMPROC_ACCESS_POINT_OPERATION_CHECK_INFO,
  VSTREAMPROC_ACCESS_POINT_OPERATION_CHECK_IS_LOCKED,
#endif
  VSTREAMPROC_ACCESS_POINT_OPERATION_PREPARE_INFO,
  VSTREAMPROC_ACCESS_POINT_OPERATION_GET_INFO,
  VSTREAMPROC_ACCESS_POINT_OPERATION_REQUEST_DATA,
  VSTREAMPROC_ACCESS_POINT_OPERATION_ACKNOWLEDGE,
  VSTREAMPROC_ACCESS_POINT_OPERATION_RELEASE,
  VSTREAMPROC_ACCESS_POINT_OPERATION_COUNT
} vStreamProc_AccessPointOperationType;

/*! Configuration element for access point operations (used by iterator). */
typedef struct
{
  vStreamProc_EntryPointOperationPtrType EntryPointOperation;
  vStreamProc_ExitPointOperationPtrType  ExitPointOperation;
} vStreamProc_AccessPointOperationConfigType;

/*! Parameter structure for access point iterator. */
typedef struct
{
  vStreamProc_PipeIdType                PipeId;
  vStreamProc_AccessPointOperationType  Operation;
  boolean                               AbortOnError;
} vStreamProc_AccessPointIteratorParamType;

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint()
 *********************************************************************************************************************/
/*! \brief      Returns the processing node id of the entry node attached to the provided entry point.
 *  \details    -
 *  \param[in]  PipeId                  The pipe id the entry point belongs to
 *  \param[in]  EntryPointSymbolicName  The symbolic name of the entry point
 *  \return     The processing node id of the entry node.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ProcessingNodeIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName);

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 * vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint()
 *********************************************************************************************************************/
/*! \brief      Returns the processing node id of the exit node attached to the provided exit point.
 *  \details    -
 *  \param[in]  PipeId                  The pipe id the exit point belongs to
 *  \param[in]  ExitPointSymbolicName  The symbolic name of the exit point
 *  \return     The processing node id of the exit node.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ProcessingNodeIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName);
#endif

/**********************************************************************************************************************
 * vStreamProc_Pipe_InitProcessingNode()
 *********************************************************************************************************************/
/*! \brief      Initialize the given processing node.
 *  \details    -
 *  \param[in]  ProcessingNodeId  The processing node's ID.
 *  \return     E_OK              Initialization was successful
 *  \return     E_NOT_OK          Initialization was not successful
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitProcessingNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InitNodes()
 *********************************************************************************************************************/
/*! \brief      Initialize the given processing and storage nodes of the provided pipe.
 *  \details    -
 *  \param[in]  PipeId            The pipe to operate on.
 *  \return     E_OK              Initialization was successful
 *  \return     E_NOT_OK          Initialization was not successful
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitNodes(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InitStreams()
 *********************************************************************************************************************/
/*! \brief      Initialize the stream related variables of the provided pipe.
 *  \details    -
 *  \param[in]  PipeId            The pipe to operate on.
 *  \context    TASK
 *  \reentrant  TRUE
 *  \pre        -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Pipe_InitStreams(
  vStreamProc_PipeIdType PipeId);

#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckEntryPointInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Check the entry point info attributes for plausibility.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     EntryPointInfo    Pointer to entry point information structure.
 *                - WriteRequest.StorageInfo.DataTypeInfo.Id    Id of the expected data type of the storage node
 *                                                              connected to the access point.
 *                                                              vStreamProcConf_vStreamProcDataType_Undefined if no
 *                                                              specific data type is expected.
 *                - EntryPointId                                Id of an entry point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckExitPointInfo()
 *********************************************************************************************************************/
/*!
 * \brief         Check the exit point info attributes for plausibility.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     ExitPointInfo     Pointer to exit point information structure.
 *                - ReadRequest.StorageInfo.DataTypeInfo.Id     Id of the expected data type of the storage node
 *                                                              connected to the access point.
 *                                                              vStreamProcConf_vStreamProcDataType_Undefined if no
 *                                                              specific data type is expected.
 *                - ExitPointId                                 Id of an entry point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckEntryPointIsLocked()
 *********************************************************************************************************************/
/*!
 * \brief         Checks the entry point is locked.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     EntryPointInfo    Pointer to entry point information structure.
 *                - EntryPointId                                Id of an entry point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckEntryPointIsLocked(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckExitPointIsLocked()
 *********************************************************************************************************************/
/*!
 * \brief         Checks the exit point is locked.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     ExitPointInfo     Pointer to exit point information structure.
 *                - ExitPointId                                 Id of an entry point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckExitPointIsLocked(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo);
#endif

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ReleaseEntryPoint()
 *********************************************************************************************************************/
/*!
 * \brief         Releases storage lock of an entry point.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     EntryPointInfo    Pointer to entry point information structure.
 *                - EntryPointId                                Id of an entry point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ReleaseEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ReleaseExitPoint()
 *********************************************************************************************************************/
/*!
 * \brief         Releases storage lock of an exit point.
 * \details       -
 * \param[in]     PipeId            Id of the desired pipe.
 * \param[in]     ExitPointInfo     Pointer to exit point information structure.
 *                - ExitPointId                                Id of an exit point of the desired pipe.
 * \return        VSTREAMPROC_OK                    Operation was successful.
 * \return        VSTREAMPROC_FAILED                Operation was unsuccessful.
 * \pre           The pipe is open.
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ReleaseExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InvertInsufficientRetVal()
 *********************************************************************************************************************/
/*!
 * \brief         Inverts the return value in case it is INSUFFICIENT_*.
 * \details       -
 * \param[in]     RetVal            The return value.
 * \return        VSTREAMPROC_INSUFFICIENT_INPUT    If RetVal is VSTREAMPROC_INSUFFICIENT_OUTPUT
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   If RetVal is VSTREAMPROC_INSUFFICIENT_INPUT
 * \return        retVal                            Otherwise, RetVal is returned
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InvertInsufficientRetVal(
  vStreamProc_ReturnType RetVal);

/*********************************************************************************************************************
 * vStreamProc_Pipe_InitAccessPointIteratorParam()
 *********************************************************************************************************************/
/*! \brief          Initialize parameter structure for access point iterator.
 *  \details        -
 *  \param[in]      PipeId            The pipe to operate on.
 *  \param[in]      Operation         The operation to apply to all access point structures.
 *  \param[in]      AbortOnError      Abort the iteration in case any other result than VSTREAMPROC_OK is returned
 *                                    when applying the operation to an access point structure.
 *  \return         Initialized vStreamProc_AccessPointIteratorParamType structure.
 *  \context        TASK
 *  \reentrant      TRUE
 *  \pre            -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_AccessPointIteratorParamType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitAccessPointIteratorParam(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_AccessPointOperationType Operation,
  boolean AbortOnError);

/*********************************************************************************************************************
 * vStreamProc_Pipe_IterateAccessPointInfos()
 *********************************************************************************************************************/
/*! \brief          Iterate over all passed access point structures and apply the given operation.
 *  \details        -
 *  \param[in]      IteratorParam     The iterator parameter structure.
 *  \param[in,out]  EntryPointInfos   Entry point information structures. NULL_PTR if not used.
 *  \param[in]      EntryPointCount   Number of entry point information structures.
 *  \param[in,out]  ExitPointInfos    Exit point information structures. NULL_PTR if not used.
 *  \param[in]      ExitPointCount    Number of exit point information structures.
 *  \return         VSTREAMPROC_FAILED                Operation failed for at least one access point.
 *  \return         VSTREAMPROC_INSUFFICIENT_OUTPUT   At least one access point cannot provide the
 *                                                    requested minimum storage.
 *  \return         VSTREAMPROC_INSUFFICIENT_INPUT    At least one access point cannot provide the
 *                                                    requested minimum data.
 *  \return         VSTREAMPROC_OK                    Operation was successful for all access points.
 *  \context        TASK
 *  \reentrant      TRUE
 *  \pre            -
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_IterateAccessPointInfos(
  vStreamProc_AccessPointIteratorParamType IteratorParam,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetBroadcastSignalDefId
 *********************************************************************************************************************/
/*!
 * \brief         Returns the broadcast signal definition id by the provided pipe id and signal id.
 * \details       -
 * \param[in]     PipeId          Id of the desired pipe.
 * \param[out]    SignalId        Id of the signal.
 * \return        Id of the broadcast signal definition.
 *                If no entry was found, VSTREAMPROC_NO_EXTERNALSIGNAL will be returned
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ExternalSignalDefIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetBroadcastSignalDefId(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_StartBroadcastSignalProcessing()
 *********************************************************************************************************************/
/*!
 * \brief         Queues the affected signal handlers of the broadcast signal and initializes the
 *                broadcast signal info.
 * \details       -
 * \param[in]     BroadcastSignalDefId    Id of the broadcast signal.
 * \return        VSTREAMPROC_PENDING     Processing was started and at least one signal handler is queued
 *                VSTREAMPROC_OK          Processing was completed immediately because no signal handler
 *                                        had to be queued.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_StartBroadcastSignalProcessing(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExternalSignalDefIdType BroadcastSignalDefId);

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the stream output port to which the exit node of the provided exit point is connected to.
 * \details       -
 * \param[in]     ExitPointId       Id of the exit point.
 * \return        The id of the stream output port.
 *                If the exit point is not active, VSTREAMPROC_NO_STREAMOUTPUTPORT will be reported.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamOutputPortIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint(
  vStreamProc_ExitPointIdType ExitPointId);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetStreamIdxOfExitPoint()
 *********************************************************************************************************************/
/*!
 * \brief         Returns the stream to which the exit node of the provided exit point is connected to.
 * \details       -
 * \param[in]     ExitPointId       Id of the exit point.
 * \return        The id of the stream.
 *                If the exit point is not active, VSTREAMPROC_NO_STREAM will be reported.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetStreamIdxOfExitPoint(
  vStreamProc_ExitPointIdType ExitPointId);

/**********************************************************************************************************************
 *  vStreamProc_Pipe_IsStreamLimitSignalPending()
 *********************************************************************************************************************/
/*!
 * \brief         Checks for all streams in a pipe if a stream limit is pending or not.
 * \details       -
 * \param[in]     PipeId            Id of the pipe.
 * \return        TRUE              There is a stream limit pending.
 *                FALSE             NO pending stream limit.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Pipe_IsStreamLimitSignalPending(
  vStreamProc_PipeIdType PipeId);
#endif

/**********************************************************************************************************************
 *  vStreamProc_Pipe_BroadcastInternal()
 *********************************************************************************************************************/
/*!
 * \brief         Internal broadcasting API without phase checks.
 * \details       -
 * \param[in]     PipeId            Id of the pipe.
 * \param[in]     SignalId          Id of the signal which shall broadcasted.
 * \return        TRUE              There is a stream limit pending.
 *                FALSE             NO pending stream limit.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_BroadcastInternal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ProcessingNodeIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ProcessingNodeIdType entryNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
      vStreamProc_GetNamedOutputPortIdxOfEntryPoint(
        vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the processing node id */

  return entryNodeId;
}

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 * vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_ProcessingNodeIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ProcessingNodeIdType exitNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
      vStreamProc_GetNamedInputPortIdxOfExitPoint(
        vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the processing node id */

  return exitNodeId;
}
#endif

/**********************************************************************************************************************
 * vStreamProc_Pipe_InitProcessingNode()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitProcessingNode(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;

  vStreamProc_NamedInputPortIterType  inputPortIdx;
  vStreamProc_ProcessingNodeInfoType  nodeInfo;
  vStreamProc_ReturnType              inputPortResults[vStreamProcConf_MaxInputPortCount];
  vStreamProc_ReturnType              outputPortResults[vStreamProcConf_MaxOutputPortCount];
  vStreamProc_SnapshotSetIterType     snapshotSetIdx = vStreamProc_GetSnapshotSetIdxOfProcessingNode(ProcessingNodeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Reset dynamic input port mapping of processing node. */
  for ( inputPortIdx = vStreamProc_GetNamedInputPortStartIdxOfProcessingNode(ProcessingNodeId);
        inputPortIdx < vStreamProc_GetNamedInputPortEndIdxOfProcessingNode(ProcessingNodeId);
        inputPortIdx++ )
  {
    vStreamProc_StreamOutputPortMappingIterType streamOutputPortMappingIdx =
      vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(inputPortIdx);

    if (streamOutputPortMappingIdx != VSTREAMPROC_NO_STREAMOUTPUTPORTMAPPINGIDXOFNAMEDINPUTPORT)
    {
      vStreamProc_SetStreamOutputPortMapping(streamOutputPortMappingIdx, VSTREAMPROC_NO_STREAMOUTPUTPORT);
    }
  }

  /* #20 Init the snapshot queue of the processing node. */
  if ((vStreamProc_GetSessionIdxOfPipe(vStreamProc_GetPipeIdxOfProcessingNode(ProcessingNodeId)) != VSTREAMPROC_NO_SESSIONIDXOFPIPE)
    && (snapshotSetIdx != VSTREAMPROC_NO_SNAPSHOTSETIDXOFPROCESSINGNODE))
  {
    vStreamProc_Queue_Init(
      vStreamProc_GetAddrSnapshotSetQueue(vStreamProc_GetSnapshotSetQueueIdxOfSnapshotSet(snapshotSetIdx)),
      vStreamProc_GetAddrSnapshotSetQueueEntry(vStreamProc_GetSnapshotSetQueueEntryStartIdxOfSnapshotSet(snapshotSetIdx)),
      vStreamProc_GetSnapshotSetQueueEntryLengthOfSnapshotSet(snapshotSetIdx),
      VSTREAMPROC_QUEUE_PRIO_ORDER_ASC,
      vStreamProc_GetSnapshotSlotStartIdxOfSnapshotSet(snapshotSetIdx),
      1u);
  }

  /* #30 Trigger the init function of the node. */
  vStreamProc_Stream_InitProcessingNodeInfo(ProcessingNodeId, inputPortResults, outputPortResults, &nodeInfo);

  retVal = vStreamProc_GetInitFctOfProcessingNodeClassDef(
    vStreamProc_GetProcessingNodeClassDefIdxOfProcessingNodeTypeDef(
      vStreamProc_GetProcessingNodeTypeDefIdxOfProcessingNode(ProcessingNodeId)))(&nodeInfo);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InitNodes()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitNodes(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;
  vStreamProc_ProcessingNodeIterType processingNodeIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Loop all processing nodes associated with the passed pipe. */
  for ( processingNodeIdx = vStreamProc_GetProcessingNodeStartIdxOfPipe(PipeId);
        processingNodeIdx < vStreamProc_GetProcessingNodeEndIdxOfPipe(PipeId);
        processingNodeIdx++)
  {
    /* #20 Initialize the processing node. */
    retVal = vStreamProc_Pipe_InitProcessingNode((vStreamProc_ProcessingNodeIdType)processingNodeIdx);

    /* Break loop if one init function from a node failed. */
    if (retVal != E_OK)
    {
      break;
    }
  }

  /* #30 Loop all streams associated with the passed pipe. */
  if (retVal == E_OK)
  {
    vStreamProc_StorageNodeIterType storageNodeIdx;
    vStreamProc_StreamIterType streamIdx;

    for ( streamIdx = vStreamProc_GetStreamStartIdxOfPipe(PipeId);
          streamIdx < vStreamProc_GetStreamEndIdxOfPipe(PipeId);
          streamIdx++)
    {
      storageNodeIdx = vStreamProc_GetStorageNodeIdxOfStream(streamIdx);

      /* #40 Check if there is a storage node attached. */
      if (storageNodeIdx != VSTREAMPROC_NO_STORAGENODE)
      {
        /* #50 Call the init function of the storage node. */
        retVal = vStreamProc_GetInitFctOfStorageNodeTypeDef(
          vStreamProc_GetStorageNodeTypeDefIdxOfStorageNode(storageNodeIdx))((vStreamProc_StorageNodeIdType)storageNodeIdx);

        /* Break loop if one init function from a node failed. */
        if (retVal != E_OK)
        {
          break;
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InitStreams()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_Pipe_InitStreams(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StreamIterType streamIdx;
  vStreamProc_EntryPointIterType entryPointIdx;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize vStreamProc_StreamInfo. */
  for (streamIdx = vStreamProc_GetStreamStartIdxOfPipe(PipeId);
       streamIdx < vStreamProc_GetStreamEndIdxOfPipe(PipeId);
       streamIdx++)
  {
    vStreamProc_StreamOutputPortIterType streamOutputPortIdx;
    vStreamProc_WaveIterType waveIdx;

    vStreamProc_SetSnapshotTriggerPositionOfStreamInfo(streamIdx, 0u);
    vStreamProc_SetStreamLimitIdxOfStreamInfo(streamIdx, VSTREAMPROC_NO_STREAMLIMITIDXOFSTREAMINFO);
    vStreamProc_SetNamedOutputPortIdxOfStreamInfo(
      streamIdx,
      (vStreamProc_NamedOutputPortIdxOfStreamInfoType)VSTREAMPROC_NO_NAMEDOUTPUTPORT);
    vStreamProc_SetWriteRequestLockOfStreamInfo(streamIdx, FALSE);
    vStreamProc_SetWaveBlockedOfStreamInfo(streamIdx, FALSE);
    vStreamProc_SetDataProducedOfStreamInfo(streamIdx, FALSE);

    /* #20 Initialize StreamOutputPortInfo. */
    for (streamOutputPortIdx = vStreamProc_GetStreamOutputPortStartIdxOfStream(streamIdx);
         streamOutputPortIdx < vStreamProc_GetStreamOutputPortEndIdxOfStream(streamIdx);
         streamOutputPortIdx++)
    {
      vStreamProc_SetWaveHandleOfStreamOutputPortInfo(streamOutputPortIdx, 0u);
      vStreamProc_SetAbsolutePositionStartOfStreamOutputPortInfo(streamOutputPortIdx, 0u);
      vStreamProc_SetConsumePositionOfStreamOutputPortInfo(streamOutputPortIdx, 0u);
      vStreamProc_SetWaveStateOfStreamOutputPortInfo(streamOutputPortIdx, VSTREAMPROC_WAVESTATE_UNINIT);
      vStreamProc_SetSnapshotTriggerPositionOfStreamOutputPortInfo(streamOutputPortIdx, 0u);
    }

    /* #30 Initialize the WaveVarInfo. */
    for (waveIdx = vStreamProc_GetWaveStartIdxOfStream(streamIdx);
         waveIdx < vStreamProc_GetWaveEndIdxOfStream(streamIdx);
         waveIdx++)
    {
      vStreamProc_SetStateOfWaveVarInfo(waveIdx, VSTREAMPROC_WAVESTATE_UNINIT);
      vStreamProc_SetHandleOfWaveVarInfo(waveIdx, 0u);
      vStreamProc_SetPendingWaveStartCountOfWaveVarInfo(waveIdx, 0u);
      vStreamProc_SetPendingWaveEndCountOfWaveVarInfo(waveIdx, 0u);
      vStreamProc_SetAbsolutePositionStartOfWaveVarInfo(waveIdx, 0u);
      vStreamProc_SetProducePositionOfWaveVarInfo(waveIdx, 0u);
    }
  }

  /* #40 Init active producer output port for entry nodes. */
  for (entryPointIdx = vStreamProc_GetEntryPointStartIdxOfPipe(PipeId);
       entryPointIdx < vStreamProc_GetEntryPointEndIdxOfPipe(PipeId);
       entryPointIdx++)
  {
    vStreamProc_NamedOutputPortIterType namedOutputPortIdx =
      vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointIdx);

    vStreamProc_SetNamedOutputPortIdxOfStreamInfo(
      vStreamProc_GetStreamIdxOfNamedOutputPort(namedOutputPortIdx),
      (vStreamProc_NamedOutputPortIdxOfStreamInfoType)namedOutputPortIdx);
  }
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InitAccessPointIteratorParam()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_AccessPointIteratorParamType, VSTREAMPROC_CODE) vStreamProc_Pipe_InitAccessPointIteratorParam(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_AccessPointOperationType Operation,
  boolean AbortOnError)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_AccessPointIteratorParamType iteratorParam;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Assign passed parameters to structure members and return by value. */
  iteratorParam.PipeId        = PipeId;
  iteratorParam.Operation     = Operation;
  iteratorParam.AbortOnError  = AbortOnError;

  return iteratorParam;
}

#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckEntryPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)                                                                     /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check plausibility of all input parameters */
  /* Verify that the passed entry point ID is valid. */
  if (EntryPointInfo->EntryPointSymbolicName >= vStreamProc_GetEntryPointLengthOfPipe(PipeId))
  {
  }
  /* Verify that the passed data type ID is valid. */
  else if (EntryPointInfo->WriteRequest.StorageInfo.DataTypeInfo.Id >= vStreamProcConf_DataTypeCount)
  {
  }
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
} /* vStreamProc_Pipe_CheckEntryPointInfo */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckExitPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check plausibility of all input parameters */
  /* Verify that the passed exit point ID is valid. */
  if (ExitPointInfo->ExitPointSymbolicName >= vStreamProc_GetExitPointLengthOfPipe(PipeId))
  {
  }
  /* Verify that the passed data type ID is valid. */
  else if (ExitPointInfo->ReadRequest.StorageInfo.DataTypeInfo.Id >= vStreamProcConf_DataTypeCount)
  {
  }
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
} /* vStreamProc_Pipe_CheckExitPointInfo */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckEntryPointIsLocked()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckEntryPointIsLocked(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)                                                                     /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_EntryPointIdType entryPointId =
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointInfo->EntryPointSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the entry node is locked. */
  if (vStreamProc_AccessNode_IsEntryNodeLocked(entryPointId) == TRUE)
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
} /* vStreamProc_Pipe_CheckEntryPointIsLocked */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CheckExitPointIsLocked()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CheckExitPointIsLocked(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ExitPointIdType exitPointId =
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointInfo->ExitPointSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the exit node is locked. */
  if (vStreamProc_AccessNode_IsExitNodeLocked(exitPointId) == TRUE)
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
} /* vStreamProc_Pipe_CheckExitPointIsLocked */
#endif

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ReleaseEntryPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ReleaseEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)                                                                     /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    vStreamProc_EntryPointIdType entryPointId =
      vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointInfo->EntryPointSymbolicName;

    /* #20 Check if there is no input stream active. */
    if (vStreamProc_AccessNode_IsInputStreamPending(entryPointId) == FALSE)
    {
      vStreamProc_StreamIdType streamId = vStreamProc_GetStreamIdxOfNamedOutputPort(
        vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointId));
      vStreamProc_StorageNodeIdType storageNodeId = vStreamProc_GetStorageNodeIdxOfStream(streamId);

      /* #30 Issue acknowledge request with zero length to storage node connected to entry point. */
      retVal = vStreamProc_CallWriteAckOfStorageNode(storageNodeId, 0u);

      /* #40 Set exit node lock. */
      if (retVal != VSTREAMPROC_FAILED)
      {
        vStreamProc_SetWriteRequestLockOfStreamInfo(streamId, FALSE);
        vStreamProc_AccessNode_SetEntryNodeLock(entryPointId, FALSE);
      }
    }
  }

  /* #50 Clear buffer and available length. */
  EntryPointInfo->WriteRequest.Buffer                      = NULL_PTR;
  EntryPointInfo->WriteRequest.StorageInfo.AvailableLength = 0u;

  return retVal;
} /* vStreamProc_Pipe_ReleaseEntryPoint */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ReleaseExitPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ReleaseExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_ExitPointIdType exitPointId =
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointInfo->ExitPointSymbolicName;
  vStreamProc_StreamOutputPortIdType streamOutputPortId =
    vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint(exitPointId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the exit point is active. */
  /* Hint: If inactive, the input port of the exit node is unmapped. */
  if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
  {
    /* #20 If true, check if there is no output stream pending. */
    if (vStreamProc_AccessNode_IsOutputStreamPending(exitPointId) == FALSE)
    {
      vStreamProc_OutputPortIdType storageOutputPortId =
        vStreamProc_GetStorageOutputPortIdxOfStreamOutputPort(streamOutputPortId);

      if (storageOutputPortId != VSTREAMPROC_NO_STORAGEOUTPUTPORTIDXOFSTREAMOUTPUTPORT)
      {
        vStreamProc_StorageNodeIdType storageNodeId =
          vStreamProc_GetStorageNodeIdxOfStorageOutputPort(storageOutputPortId);

        /* Hint: Exit points can also be evaluated when pipe is failed. */
        /* #30 Issue acknowledge request with zero length to storage node connected to exit point. */
        retVal = vStreamProc_CallReadAckOfStorageNode(storageNodeId, 0u, storageOutputPortId);
      }
      else
      {
        retVal = VSTREAMPROC_OK;
      }

      /* #40 Set exit node lock. */
      if (retVal != VSTREAMPROC_FAILED)
      {
        vStreamProc_AccessNode_SetExitNodeLock(exitPointId, FALSE);
      }
    }
  }
  else
  {
    /* #50 Return OK if exit point is inactive. */
    /* Hint: As nothing has to be done for an inactive exit point anyway (the node shouldn't be locked at this point),
             returning OK is fine here. */
    retVal = VSTREAMPROC_OK;
  }

  /* #60 Clear buffer and available length. */
  ExitPointInfo->ReadRequest.Buffer                      = NULL_PTR;
  ExitPointInfo->ReadRequest.StorageInfo.AvailableLength = 0u;
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointInfo);
#endif
  return retVal;
} /* vStreamProc_Pipe_ReleaseExitPoint */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_InvertInsufficientRetVal()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_InvertInsufficientRetVal(
  vStreamProc_ReturnType RetVal)
{
  vStreamProc_ReturnType retVal;

  /* #10 Exchange return codes for insufficient input and output. */
  switch (RetVal)
  {
    case VSTREAMPROC_INSUFFICIENT_INPUT:
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
      break;
    }
    case VSTREAMPROC_INSUFFICIENT_OUTPUT:
    {
      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
      break;
    }
    default:
    {
      /* #20 Other return codes remain unchanged. */
      retVal = RetVal;
      break;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_IterateAccessPointInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_IterateAccessPointInfos(
  vStreamProc_AccessPointIteratorParamType IteratorParam,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Constants ---------------------------------------------- */
  /*! Lookup table for access point operation functions. */
  CONST(vStreamProc_AccessPointOperationConfigType, AUTOMATIC) vStreamProc_AccessPointOperationConfig[VSTREAMPROC_ACCESS_POINT_OPERATION_COUNT] =
  {
#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
    { vStreamProc_Pipe_CheckEntryPointInfo,       vStreamProc_Pipe_CheckExitPointInfo },
    { vStreamProc_Pipe_CheckEntryPointIsLocked,   vStreamProc_Pipe_CheckExitPointIsLocked },
#endif
    { vStreamProc_Pipe_PrepareEntryPointInfo,     vStreamProc_Pipe_PrepareExitPointInfo },
    { vStreamProc_Pipe_GetEntryPointInfo,         vStreamProc_Pipe_GetExitPointInfo },
    { vStreamProc_Pipe_RequestEntryPointData,     vStreamProc_Pipe_RequestExitPointData },
    { vStreamProc_Pipe_AcknowledgeEntryPoint,     vStreamProc_Pipe_AcknowledgeExitPoint },
    { vStreamProc_Pipe_ReleaseEntryPoint,         vStreamProc_Pipe_ReleaseExitPoint }
  };

  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType          retVal = VSTREAMPROC_OK;                                                              /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_ReturnType          opRetVal;
  vStreamProc_EntryPointIterType  entryPointIndex;
  vStreamProc_ExitPointIterType   exitPointIndex;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 For all entry point infos */
  for (entryPointIndex = 0u; entryPointIndex < EntryPointCount; entryPointIndex++)
  {
    /* #20 Execute entry point operation. */
    opRetVal = vStreamProc_AccessPointOperationConfig[IteratorParam.Operation].EntryPointOperation(
      IteratorParam.PipeId,
      &EntryPointInfos[entryPointIndex]);

    /* Results with higher severity overwrite existing value. */
    if (opRetVal > retVal)
    {
      retVal = opRetVal;

      /* #30 Abort further processing if requested. */
      if (IteratorParam.AbortOnError == TRUE)
      {
        break;
      }
    }
  }

  if ( (retVal == VSTREAMPROC_OK) || (IteratorParam.AbortOnError == FALSE) )
  {
    /* #40 For all exit point infos */
    for (exitPointIndex = 0u; exitPointIndex < ExitPointCount; exitPointIndex++)
    {
      /* #50 Execute exit point operation. */
      opRetVal = vStreamProc_AccessPointOperationConfig[IteratorParam.Operation].ExitPointOperation(
        IteratorParam.PipeId,
        &ExitPointInfos[exitPointIndex]);

      /* Results with higher severity overwrite existing value. */
      if (opRetVal > retVal)
      {
        retVal = opRetVal;

        /* #60 Abort further processing if requested. */
        if (IteratorParam.AbortOnError == TRUE)
        {
          break;
        }
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetBroadcastSignalDefId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ExternalSignalDefIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetBroadcastSignalDefId(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ExternalSignalDefIterType    broadcastSignalDefIdx;
  vStreamProc_ExternalSignalDefIdType      broadcastSignalDefId = VSTREAMPROC_NO_EXTERNALSIGNAL;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasExternalSignalDef())
  {
    /* #10 Iterate over broadcast signals definitions. */
    for ( broadcastSignalDefIdx = vStreamProc_GetExternalSignalDefStartIdxOfPipe(PipeId);
          broadcastSignalDefIdx < vStreamProc_GetExternalSignalDefEndIdxOfPipe(PipeId);
          broadcastSignalDefIdx++)
    {
      if (vStreamProc_GetSignalIdxOfExternalSignalDef(broadcastSignalDefIdx) == SignalId)
      {
        /* #20 Broadcast signal definition entry found, update broadcast signal Id and leave loop. */
        broadcastSignalDefId = (vStreamProc_ExternalSignalDefIdType)broadcastSignalDefIdx;
        break;
      }
    }
  }

  return broadcastSignalDefId;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_StartBroadcastSignalProcessing()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_StartBroadcastSignalProcessing(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExternalSignalDefIdType BroadcastSignalDefId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalHandlerIndIterType signalHandlerIndIdx;
  vStreamProc_PendingCountOfExternalSignalInfoType pendingCount = 0;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasExternalSignalDef())
  {
    /* #10 Loop over the broadcast signal def to signal handler indirection table and trigger the signal handlers. */
    for ( signalHandlerIndIdx = vStreamProc_GetSignalHandlerIndStartIdxOfExternalSignalDef(BroadcastSignalDefId);
          signalHandlerIndIdx < vStreamProc_GetSignalHandlerIndEndIdxOfExternalSignalDef(BroadcastSignalDefId);
          signalHandlerIndIdx++)
    {
      Std_ReturnType stdRetVal;

      stdRetVal = vStreamProc_Scheduler_QueueSignalHandler(vStreamProc_GetSignalHandlerInd(signalHandlerIndIdx));

      /* Hint: If vStreamProc_Scheduler_QueueSignalHandler() returns E_NOT_OK, the signal handler is UNREGISTERED and
               must not be considered */
      if (stdRetVal == E_OK)
      {
        pendingCount++;
      }
    }

    /* #20 Init broadcast signal info. */
    vStreamProc_SetPendingCountOfExternalSignalInfo(
      BroadcastSignalDefId,
      pendingCount);

    /* #30 Check the pending count. */
    if (pendingCount > 0u)
    {
      /* #40 If there are pending signal handlers, set result to pending. */
      vStreamProc_SetResultOfExternalSignalInfo(BroadcastSignalDefId, VSTREAMPROC_PENDING);
      retVal = VSTREAMPROC_PENDING;
    }
    else
    {
      vStreamProc_SignalCallbackType signalCallbackFct =
        vStreamProc_GetSignalCallbackOfExternalSignalInfo(BroadcastSignalDefId);

      /* #50 Otherwise, set the result to OK. */
      /* Hint: In this case, all signal handlers are in unregistered state. Return VSTREAMPROC_OK as there is
               nothing to do. */
      vStreamProc_SetResultOfExternalSignalInfo(BroadcastSignalDefId, VSTREAMPROC_OK);
      retVal = VSTREAMPROC_OK;

      /* #60 Call signal callback if available. */
      if (signalCallbackFct != (vStreamProc_SignalCallbackType)0)
      {
        signalCallbackFct(
          PipeId,
          vStreamProc_GetSignalIdxOfExternalSignalDef(BroadcastSignalDefId),
          vStreamProc_GetResultOfExternalSignalInfo(BroadcastSignalDefId),
          vStreamProc_GetSignalCallbackHandleOfExternalSignalInfo(BroadcastSignalDefId));
      }
    }
  }

  return retVal;
}

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamOutputPortIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint(
  vStreamProc_ExitPointIdType ExitPointId)
{
  vStreamProc_StreamOutputPortIdType retVal = VSTREAMPROC_NO_STREAMOUTPUTPORT;
  vStreamProc_StreamOutputPortMappingIdxOfNamedInputPortType streamOutputPortMappingId =
    vStreamProc_GetStreamOutputPortMappingIdxOfNamedInputPort(vStreamProc_GetNamedInputPortIdxOfExitPoint(ExitPointId));

  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Return the mapped stream output port id. */
  if (streamOutputPortMappingId != VSTREAMPROC_NO_STREAMOUTPUTPORTMAPPINGIDXOFNAMEDINPUTPORT)
  {
    retVal = vStreamProc_GetStreamOutputPortMapping(streamOutputPortMappingId);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetStreamIdxOfExitPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(vStreamProc_StreamIdType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetStreamIdxOfExitPoint(
  vStreamProc_ExitPointIdType ExitPointId)
{
  vStreamProc_StreamIdType            streamId            = VSTREAMPROC_NO_STREAM;
  vStreamProc_StreamOutputPortIdType  streamOutputPortId  =
    vStreamProc_Pipe_GetStreamOutputPortIdxOfExitPoint(ExitPointId);

  /* #10 Return the stream ID of the mapped stream output port. */
  if (streamOutputPortId != VSTREAMPROC_NO_STREAMOUTPUTPORT)
  {
    streamId = vStreamProc_GetStreamIdxOfStreamOutputPort(streamOutputPortId);
  }

  return streamId;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_IsStreamLimitSignalPending()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL_INLINE FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_Pipe_IsStreamLimitSignalPending(                   /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId)
{
  boolean retVal = FALSE;

  /* #10 If limits are configured. */
  if ( (vStreamProc_HasStreamLimit())
    && (vStreamProc_HasExternalSignalDef()) )
  {
    vStreamProc_ExitPointIterType exitPointIdx;

    /* #20 Loop over all exit points of pipe. */
    for (exitPointIdx = vStreamProc_GetExitPointStartIdxOfPipe(PipeId);
         exitPointIdx < vStreamProc_GetExitPointEndIdxOfPipe(PipeId);
         exitPointIdx++)
    {
      vStreamProc_StreamIdType streamIdx =
        vStreamProc_Pipe_GetStreamIdxOfExitPoint((vStreamProc_ExitPointIdType)exitPointIdx);

      if (streamIdx != VSTREAMPROC_NO_STREAM)
      {
        vStreamProc_StreamLimitIdxOfStreamInfoType streamLimitIdx =
          vStreamProc_GetStreamLimitIdxOfStreamInfo(streamIdx);

        /* #30 If active stream limit is linked with stream connected to exit point. */
        if ((streamLimitIdx != VSTREAMPROC_NO_STREAMLIMITIDXOFEXITPOINT)
          && (vStreamProc_GetTypeOfStreamLimitInfo(streamLimitIdx) != VSTREAMPROC_NO_TYPEOFSTREAMLIMIT))
        {
          vStreamProc_ExternalSignalDefIdType externalSignalDefId =
            vStreamProc_GetExternalSignalDefIdxOfStreamLimit(streamLimitIdx);

          /* #40 Check for pending state. */
          if (vStreamProc_GetPendingCountOfExternalSignalInfo(externalSignalDefId) > 0u)
          {
            retVal = TRUE;
            break;
          }
        }
      }
    }
  }

  return retVal;
}
#endif

/**********************************************************************************************************************
 *  vStreamProc_Pipe_BroadcastInternal()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_BroadcastInternal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there are broadcast signals globally and if the pipe is in the processing phase. */
  if (vStreamProc_HasExternalSignalDef())
  {
    vStreamProc_ExternalSignalDefIdType broadcastSignalDefId =
      vStreamProc_Pipe_GetBroadcastSignalDefId(PipeId, SignalId);

    /* #20 Check if the broadcast signal is configured in the provided pipe. */
    if (broadcastSignalDefId != VSTREAMPROC_NO_EXTERNALSIGNAL)
    {
      /* #30 Check if the processing of the previous signal is completed. */
      if (vStreamProc_GetPendingCountOfExternalSignalInfo(broadcastSignalDefId) == 0u)
      {
        retVal = vStreamProc_Pipe_StartBroadcastSignalProcessing(PipeId, broadcastSignalDefId);
      }
    }
  }
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_Pipe_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_Init(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Init pipe runtime variables. */
  vStreamProc_SetActiveWaveCountOfPipeInfo(PipeId, 0u);

  /* #20 Initialize the pipe's scheduler. 1:1 relation between pipe and scheduler. */
  retVal = vStreamProc_Scheduler_Init(PipeId);

  if (retVal == E_OK)
  {
    /* #30 Pipe is closed after initialization. */
    vStreamProc_SetPipeState(PipeId, VSTREAMPROC_CLOSED_PIPESTATE);
    vStreamProc_SetPhaseOfPipeInfo(PipeId, VSTREAMPROC_IDLE_PHASEOFPIPEINFO);

    /* #40 Init the nodes of the pipe. */
    retVal = vStreamProc_Pipe_InitNodes(PipeId);
  }

  if (retVal == E_OK)
  {
    vStreamProc_ExternalSignalInfoIterType externalSignalInfoIdx;

    /* #50 Init streams and waves. */
    vStreamProc_Pipe_InitStreams(PipeId);

    /* #60 Init the session */
    if (vStreamProc_GetSessionIdxOfPipe(PipeId) != VSTREAMPROC_NO_SESSIONIDXOFPIPE)
    {
      vStreamProc_Session_InitSession(PipeId);
    }

  /* #70 Init the external signal info structures of the pipe. */
    for ( externalSignalInfoIdx = vStreamProc_GetExternalSignalDefStartIdxOfPipe(PipeId);
          externalSignalInfoIdx < vStreamProc_GetExternalSignalDefEndIdxOfPipe(PipeId);
          externalSignalInfoIdx++)
    {
      vStreamProc_SetPendingCountOfExternalSignalInfo(externalSignalInfoIdx, 0u);
      vStreamProc_SetSignalCallbackOfExternalSignalInfo(externalSignalInfoIdx, (vStreamProc_SignalCallbackType)0);
      vStreamProc_SetResultOfExternalSignalInfo(externalSignalInfoIdx, VSTREAMPROC_OK);
      vStreamProc_SetSignalCallbackHandleOfExternalSignalInfo(externalSignalInfoIdx, 0u);
    }

    /* #80 Init stream limits. */
    vStreamProc_Limit_InitStreamLimit(PipeId);

    /* #90 Set port activations to default configuration. */
    retVal = vStreamProc_Mode_ApplyMode(PipeId, VSTREAMPROC_NO_MODE);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_Open
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_Open(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Open the pipe. */
  vStreamProc_SetPipeState(PipeId, VSTREAMPROC_OPENED_PIPESTATE);
  retVal = E_OK;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_Close
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_Close(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Close the pipe. */
  vStreamProc_SetPipeState(PipeId, VSTREAMPROC_CLOSED_PIPESTATE);
  retVal = E_OK;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_Process
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_Process(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if (vStreamProc_GetPhaseOfPipeInfo(PipeId) != VSTREAMPROC_FAILED_PHASEOFPIPEINFO)
  {
    /* #20 Start the scheduler. 1:1 relation between pipe and scheduler. */
    retVal = vStreamProc_Scheduler_Process(PipeId);

    /* #30 Change pipe to "failed" state if operation didn't succeed. */
    if (retVal == VSTREAMPROC_FAILED)
    {
      vStreamProc_SetPhaseOfPipeInfo(PipeId, VSTREAMPROC_FAILED_PHASEOFPIPEINFO);
    }
    else
    {
      vStreamProc_SetPhaseOfPipeInfo(PipeId, VSTREAMPROC_PROCESS_PHASEOFPIPEINFO);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_Cancel(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_FAILED_PHASEOFPIPEINFO))
  {
    /* #20 Clear all pending signals. Re-init scheduling queue. */
    if (vStreamProc_Scheduler_Init(PipeId) == E_OK)
    {
      /* #30 Issue Cancel broadcast signal. */
      retVal = vStreamProc_Pipe_BroadcastInternal(PipeId, vStreamProcConf_vStreamProcSignal_vStreamProc_Cancel);

      /* #40 Set phase to "cancel". */
      vStreamProc_SetPhaseOfPipeInfo(PipeId, VSTREAMPROC_CANCEL_PHASEOFPIPEINFO);
    }

    /* #50 In case of failure set pipe to failed phase. */
    if (retVal == VSTREAMPROC_FAILED)
    {
      vStreamProc_SetPhaseOfPipeInfo(PipeId, VSTREAMPROC_FAILED_PHASEOFPIPEINFO);
    }
  }
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_SetMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_SetMode(                                                        /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ModeIdType PipeModeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  if (vStreamProc_HasBaseState()
    && vStreamProc_HasBaseStateInfo()
    && vStreamProc_HasMode()
    && vStreamProc_HasMode2BaseState())
  {
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
    {
      /* #20 Check if the mode leads to any base state changes. */
      if (vStreamProc_Mode_IsActivationStateChanged(PipeId, PipeModeId) == TRUE)
      {
        /* #30 Ensure that no limit trigger is pending and that no wave is pending. */
        if ( (vStreamProc_GetActiveWaveCountOfPipeInfo(PipeId) == 0u)
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
          && (vStreamProc_Pipe_IsStreamLimitSignalPending(PipeId) != TRUE)
#endif
           )
        {
          /* #40 Apply given mode, switching the affected base state values. */
          retVal = vStreamProc_Mode_ApplyMode(PipeId, PipeModeId);
        }
      }
      /* #50 Otherwise, directly return E_OK. */
      /* Hint: If no base states are affected, the mode switch does lead to any changes in the pipe.
                Therefore, no further actions have to be taken. */
      else
      {

        retVal = E_OK;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_AnnounceStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_AnnounceStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointSymbolicNameType EntryPointSymbolicName,
  vStreamProc_ProduceCallbackType ProduceCbk)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    vStreamProc_EntryPointIdType entryPointId = vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName;

    /* #20 Store callback function pointer in node workspace. Set signal handlers to correct state. */
    retVal = vStreamProc_AccessNode_AnnounceStream(entryPointId, ProduceCbk);

    /* #30 If stream was accepted, cause signal "announce stream". */
    if (retVal == E_OK)
    {
      (void)vStreamProc_Scheduler_SetNodeSignal(
        vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(vStreamProc_GetNamedOutputPortIdxOfEntryPoint(entryPointId)),
        vStreamProcConf_vStreamProcSignal_vStreamProc_StreamAnnounced);
    }
  }
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_PrepareEntryPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_PrepareEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;                                                                       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) writeRequest =
    &EntryPointInfo->WriteRequest;
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaData =
    &EntryPointInfo->MetaData;

  VSTREAMPROC_DUMMY_STATEMENT(PipeId);                                                                                  /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize write request with default values. */
  writeRequest->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&writeRequest->StorageInfo);

  /* #20 Initialize MetaData with default values. */
  metaData->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&metaData->StorageInfo);

  return retVal;
} /* vStreamProc_Pipe_PrepareEntryPointInfo */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetEntryPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetEntryPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortHandleType outputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the output port handle. */
  (void)vStreamProc_Stream_GetOutputPortHandle(
    vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint(PipeId, EntryPointInfo->EntryPointSymbolicName),
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
    &outputPortHandle);

  /* #20 Call output port API. */
  retVal = vStreamProc_Stream_GetOutputPortInfo(
    &outputPortHandle,
    &EntryPointInfo->WriteRequest,
    &EntryPointInfo->MetaData,
    &EntryPointInfo->StreamPositionInfo);

  retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

  return retVal;
} /* vStreamProc_Pipe_GetEntryPointInfo */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RequestEntryPointData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RequestEntryPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    vStreamProc_EntryPointIdType entryPointId =
      vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointInfo->EntryPointSymbolicName;

    /* #20 Check if there is no input stream active. */
    if (vStreamProc_AccessNode_IsInputStreamPending(entryPointId) == FALSE)
    {
      vStreamProc_StorageInfoConstPtrType writeRequestStorageInfo = &EntryPointInfo->WriteRequest.StorageInfo;
      vStreamProc_OutputPortHandleType outputPortHandle;

      /* #30 Get the output port handle. */
      (void)vStreamProc_Stream_GetOutputPortHandle(
        vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint(PipeId, EntryPointInfo->EntryPointSymbolicName),
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
        &outputPortHandle);

      /* #40 Call output port API. */
      retVal = vStreamProc_Stream_RequestOutputPortData(
        &outputPortHandle,
        &EntryPointInfo->WriteRequest,
        &EntryPointInfo->MetaData,
        &EntryPointInfo->StreamPositionInfo);

      retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

      /* #50 Lock the entry node if necessary. */
      if ((retVal == VSTREAMPROC_OK)
        && (writeRequestStorageInfo->AvailableLength > 0u))
      {
        vStreamProc_AccessNode_SetEntryNodeLock(entryPointId, TRUE);
      }
    }
  }

  return retVal;
} /* vStreamProc_Pipe_RequestEntryPointData */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_AcknowledgeEntryPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_AcknowledgeEntryPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    vStreamProc_EntryPointIdType entryPointId =
      vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointInfo->EntryPointSymbolicName;

  /* #20 Check if there is no input stream pending. */
    if (vStreamProc_AccessNode_IsInputStreamPending(entryPointId) == FALSE)
    {
      vStreamProc_StorageInfoConstPtrType writeRequestStorageInfo = &EntryPointInfo->WriteRequest.StorageInfo;
      vStreamProc_OutputPortHandleType outputPortHandle;

      /* #30 Get the output port handle. */
      (void)vStreamProc_Stream_GetOutputPortHandle(
        vStreamProc_Pipe_GetProcessingNodeIdOfEntryPoint(PipeId, EntryPointInfo->EntryPointSymbolicName),
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
        &outputPortHandle);

      /* #40 Call output port API. */
      retVal = vStreamProc_Stream_AcknowledgeOutputPort(
        &outputPortHandle,
        &EntryPointInfo->WriteRequest,
        &EntryPointInfo->MetaData,
        &EntryPointInfo->StreamPositionInfo);

      /* #50 Sync the output port waves. */
      vStreamProc_Stream_SyncOutputPortsWaves(outputPortHandle.ProcessingNodeId);

      retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

      /* #60 Release the entry node lock if necessary. */
      if (writeRequestStorageInfo->ReleaseFlag == TRUE)
      {
        vStreamProc_AccessNode_SetEntryNodeLock(entryPointId, FALSE);
      }
    }
  }

  return retVal;
} /* vStreamProc_Pipe_AcknowledgeEntryPoint */

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RequestStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RequestStream(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_ConsumeCallbackType ConsumeCbk)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;                                                                                     /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    vStreamProc_ExitPointIdType exitPointId =
      vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;

    /* #20 Store callback function pointer in node workspace. Set signal handlers to correct state. */
    retVal = vStreamProc_AccessNode_RequestStream(exitPointId, ConsumeCbk);

    /* #30 If stream was accepted, cause signal "request stream". */
    if (retVal == E_OK)
    {
      (void)vStreamProc_Scheduler_SetNodeSignal(
        vStreamProc_GetProcessingNodeIdxOfNamedInputPort(vStreamProc_GetNamedInputPortIdxOfExitPoint(exitPointId)),
        vStreamProcConf_vStreamProcSignal_vStreamProc_StreamRequested);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_PrepareExitPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_PrepareExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_OK;                                                                       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) readRequest =
    &ExitPointInfo->ReadRequest;
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_VAR) metaData =
    &ExitPointInfo->MetaData;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);                                                                                  /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */

  /* #10 Initialize read request with default values. */
  readRequest->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&readRequest->StorageInfo);

  /* #20 Initialize MetaData with default values. */
  metaData->Buffer = NULL_PTR;
  vStreamProc_Stream_InitStorageInfo(&metaData->StorageInfo);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetExitPointInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetExitPointInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_InputPortHandleType inputPortHandle;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the input port handle. */
  (void)vStreamProc_Stream_GetInputPortHandle(
    vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint(PipeId, ExitPointInfo->ExitPointSymbolicName),
    vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
    &inputPortHandle);

  /* #20 Call input port API. */
  retVal = vStreamProc_Stream_GetInputPortInfo(
    &inputPortHandle,
    &ExitPointInfo->ReadRequest,
    &ExitPointInfo->MetaData,
    &ExitPointInfo->StreamPositionInfo);

  retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointInfo);
#endif
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RequestExitPointData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RequestExitPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_ExitPointIdType exitPointId =
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointInfo->ExitPointSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
    /* #10 Check if there is no output stream active. */
  if (vStreamProc_AccessNode_IsOutputStreamPending(exitPointId) == FALSE)
  {
    vStreamProc_StorageInfoConstPtrType readRequestStorageInfo = &ExitPointInfo->ReadRequest.StorageInfo;
    vStreamProc_InputPortHandleType inputPortHandle;

    /* #20 Get the input port handle. */
    (void)vStreamProc_Stream_GetInputPortHandle(
      vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint(PipeId, ExitPointInfo->ExitPointSymbolicName),
      vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
      &inputPortHandle);

    /* #30 Call input port API. */
    retVal = vStreamProc_Stream_RequestInputPortData(
      &inputPortHandle,
      &ExitPointInfo->ReadRequest,
      &ExitPointInfo->MetaData,
      &ExitPointInfo->StreamPositionInfo);

    retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

    /* #40 Lock the exit node if necessary. */
    if ((retVal == VSTREAMPROC_OK)
      && (readRequestStorageInfo->AvailableLength > 0u))
    {
      vStreamProc_AccessNode_SetExitNodeLock(exitPointId, TRUE);
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointInfo);
#endif
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_AcknowledgeExitPoint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_AcknowledgeExitPoint(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)                                                                       /* PRQA S 3673 */ /* MD_vStreamProc_Rule8.13_3673_UnmodifiedPointerCouldBeConstant_UnifiedSignature */
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  vStreamProc_ExitPointIdType exitPointId =
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointInfo->ExitPointSymbolicName;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there is no output stream pending. */
  if (vStreamProc_AccessNode_IsOutputStreamPending(exitPointId) == FALSE)
  {
    vStreamProc_StorageInfoConstPtrType readRequestStorageInfo = &ExitPointInfo->ReadRequest.StorageInfo;
    vStreamProc_InputPortHandleType inputPortHandle;

    /* #20 Get the input port handle. */
    (void)vStreamProc_Stream_GetInputPortHandle(
      vStreamProc_Pipe_GetProcessingNodeIdOfExitPoint(PipeId, ExitPointInfo->ExitPointSymbolicName),
      vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
      &inputPortHandle);

    /* #30 Call input port API. */
    retVal = vStreamProc_Stream_AcknowledgeInputPort(
      &inputPortHandle,
      &ExitPointInfo->ReadRequest,
      &ExitPointInfo->MetaData,
      &ExitPointInfo->StreamPositionInfo);

    retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

    /* #40 Release the exit node lock if necessary. */
    if (readRequestStorageInfo->ReleaseFlag == TRUE)
    {
      vStreamProc_AccessNode_SetExitNodeLock(exitPointId, FALSE);
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointInfo);
#endif
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_PrepareAccessPointInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_PrepareAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed access point structures and apply prepare operation. */
  retVal = vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_PREPARE_INFO, FALSE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_PrepareAllAccessPointInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_PrepareAllAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType          retVal = VSTREAMPROC_FAILED;                                                          /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_EntryPointIterType  entryPointIndex;
  vStreamProc_ExitPointIterType   exitPointIndex;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize IDs of passed access point structures to be equal to the zero-based index. */
  for (entryPointIndex = 0u; entryPointIndex < EntryPointCount; entryPointIndex++)
  {
    EntryPointInfos[entryPointIndex].EntryPointSymbolicName = (vStreamProc_EntryPointSymbolicNameType)entryPointIndex;
  }

  for (exitPointIndex = 0u; exitPointIndex < ExitPointCount; exitPointIndex++)
  {
    ExitPointInfos[exitPointIndex].ExitPointSymbolicName = (vStreamProc_ExitPointSymbolicNameType)exitPointIndex;
  }

  /* #20 Iterate over all passed access point infos and apply prepare operation. */
  retVal = vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_PREPARE_INFO, FALSE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetAccessPointInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetAccessPointInfos(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed access point infos and apply getter operation. */
  retVal = vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_GET_INFO, FALSE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RequestAccessPointData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RequestAccessPointData(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed access point infos and apply request operation. */
  retVal = vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_REQUEST_DATA, FALSE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);

  /* #20 If operation failed, iterate over all passed access point infos and apply release operation. */
  if (retVal != VSTREAMPROC_OK)
  {
    (void)vStreamProc_Pipe_IterateAccessPointInfos(
      vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_RELEASE, FALSE),
      EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_AcknowledgeAccessPoints()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_AcknowledgeAccessPoints(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_ExitPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_EntryPointIdType ExitPointCount)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all passed access point infos and apply acknowledge operation. */
  retVal = vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_ACKNOWLEDGE, FALSE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ReleaseAllAccessPoints()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ReleaseAllAccessPoints(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  vStreamProc_EntryPointInfoType  entryPointInfos[vStreamProcConf_MaxEntryPointCount];
  vStreamProc_ExitPointInfoType   exitPointInfos[vStreamProcConf_MaxExitPointCount];
  vStreamProc_EntryPointIdType    entryPointCount = vStreamProc_GetEntryPointLengthOfPipe(PipeId);
  vStreamProc_ExitPointIdType     exitPointCount  = vStreamProc_GetExitPointLengthOfPipe(PipeId);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare all access point infos, e.g. initialize IDs.  */
  retVal = vStreamProc_Pipe_PrepareAllAccessPointInfos(
    PipeId,
    entryPointInfos, entryPointCount,
    exitPointInfos, exitPointCount);

  /* #20 Iterate over all access point infos and apply prepare operation. */
  if (retVal == VSTREAMPROC_OK)
  {
    retVal = vStreamProc_Pipe_IterateAccessPointInfos(
      vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_RELEASE, FALSE),
      entryPointInfos, entryPointCount, exitPointInfos, exitPointCount);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_BroadcastSignal()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_BroadcastSignal(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there are broadcast signals globally and if the pipe is in the processing phase. */
  if (  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    ||  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    retVal = vStreamProc_Pipe_BroadcastInternal(PipeId, SignalId);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetSignalStatus()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetSignalStatus(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId)
{
  /* ----- Local Variables ---------------------------------------------- */
    vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there are broadcast signals globally. */
  if (vStreamProc_HasExternalSignalDef())
  {
    vStreamProc_ExternalSignalDefIdType broadcastSignalDefId =
      vStreamProc_Pipe_GetBroadcastSignalDefId(PipeId, SignalId);

    /* #20 Check if the broadcast signal is configured in the provided pipe. */
    if (broadcastSignalDefId != VSTREAMPROC_NO_EXTERNALSIGNAL)
    {
      /* #30 Return the current result. */
      retVal = vStreamProc_GetResultOfExternalSignalInfo(broadcastSignalDefId);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RegisterSignalCallback()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RegisterSignalCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_SignalIdType SignalId,
  vStreamProc_SignalCallbackType SignalCbk,
  vStreamProc_CallbackHandleType CallbackHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if there are broadcast signals globally and if the pipe is in the processing phase. */
  if (vStreamProc_HasExternalSignalDef()
    && ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    ||  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO)))
  {
    vStreamProc_ExternalSignalDefIdType broadcastSignalDefId =
      vStreamProc_Pipe_GetBroadcastSignalDefId(PipeId, SignalId);

    /* #20 Check if the broadcast signal is configured in the provided pipe. */
    if (broadcastSignalDefId != VSTREAMPROC_NO_EXTERNALSIGNAL)
    {
      /* #30 Set the response callback of the broadcast signal. */
      vStreamProc_SetSignalCallbackOfExternalSignalInfo(broadcastSignalDefId, SignalCbk);

      /* #40 Store CallbackHandle for later use. */
      vStreamProc_SetSignalCallbackHandleOfExternalSignalInfo(broadcastSignalDefId, CallbackHandle);

      retVal = VSTREAMPROC_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_SetExitPointLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_SetExitPointLimit(                                      /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH , MD_MSR_STCYC , MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_LimitType LimitType,
  vStreamProc_StreamPositionType AbsoluteLimitPosition,
  vStreamProc_StreamPositionType LimitOffset)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  /* #10 Check state of pipe. */
  if (vStreamProc_HasStreamLimit()
    && ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    ||  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO)))
  {
    vStreamProc_ExitPointIdType   exitPointId     = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamIdType      streamId        = vStreamProc_Pipe_GetStreamIdxOfExitPoint(exitPointId);
    vStreamProc_StreamLimitIdType streamLimitIdx  = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);
    vStreamProc_LimitInfoType     limitInfo;

    /* #20 If the stream is active. */
    if (streamId != VSTREAMPROC_NO_STREAM)
    {
      limitInfo.Position  = AbsoluteLimitPosition;
      limitInfo.Offset    = LimitOffset;
      limitInfo.Type      = LimitType;

      /* #30 Set the stream limit. */
      retVal = vStreamProc_Limit_SetStreamLimit(streamId, streamLimitIdx, &limitInfo);
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointSymbolicName);
  VSTREAMPROC_DUMMY_STATEMENT(LimitType);
  VSTREAMPROC_DUMMY_STATEMENT(AbsoluteLimitPosition);
  VSTREAMPROC_DUMMY_STATEMENT(LimitOffset);
#endif

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_ClearExitPointLimit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_ClearExitPointLimit(                                    /* PRQA S 6080 */ /* MD_MSR_STMIF */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  /* #10 Check state of pipe. */
  if (vStreamProc_HasStreamLimit()
    && ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    ||  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO)))
  {
    vStreamProc_ExitPointIdType   exitPointId     = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamIdType      streamId        = vStreamProc_Pipe_GetStreamIdxOfExitPoint(exitPointId);
    vStreamProc_StreamLimitIdType streamLimitIdx  = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);

    /* #20 If the stream is active. */
    if (streamId != VSTREAMPROC_NO_STREAM)
    {
      /* #30 Clear the stream limit.*/
      retVal = vStreamProc_Limit_ClearStreamLimit(streamId, streamLimitIdx);
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointSymbolicName);
#endif

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetExitPointLimitStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetExitPointLimitStatus(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_EXITNODE == STD_ON)
  if (vStreamProc_HasStreamLimit())
  {
    vStreamProc_ExitPointIdType   exitPointId     = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamIdType      streamId        = vStreamProc_Pipe_GetStreamIdxOfExitPoint(exitPointId);
    vStreamProc_StreamLimitIdType streamLimitIdx  = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);

    /* #10 If the stream is active. */
    if (streamId != VSTREAMPROC_NO_STREAM)
    {
      /* #20 Get the status of the stream limit.*/
      retVal = vStreamProc_Limit_GetStatus(streamId, streamLimitIdx);
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(PipeId);
  VSTREAMPROC_DUMMY_STATEMENT(ExitPointSymbolicName);
#endif

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RegisterExitPointLimitCallback
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RegisterExitPointLimitCallback(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointSymbolicNameType ExitPointSymbolicName,
  vStreamProc_SignalCallbackType LimitCbk,
  vStreamProc_CallbackHandleType CallbackHandle)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType             retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check state of pipe. */
  if (vStreamProc_HasStreamLimit()
    && ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    ||  (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO)))
  {
    vStreamProc_ExitPointIdType   exitPointId = vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName;
    vStreamProc_StreamLimitIdType streamLimitIdx  = vStreamProc_GetStreamLimitIdxOfExitPoint(exitPointId);

    /* #20 Register the stream limit callback.*/
    retVal = vStreamProc_Limit_RegisterCallback(streamLimitIdx, LimitCbk, CallbackHandle);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_StartEntryPointWave()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_StartEntryPointWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
      vStreamProc_GetNamedOutputPortIdxOfEntryPoint(
        vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
    /* #20 Check if requested wave level is the lowest level. */
    if (vStreamProc_Stream_GetWaveLevel(PipeId, WaveInfo->WaveTypeSymbolicName) == 0u)
    {
      /* #30 Start wave with given information. */
      retVal = vStreamProc_Stream_StartOutputPortWave(
        processingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
        WaveInfo);

      /* #40 Create entry node snapshot if wave start was successful. */
      if ((vStreamProc_GetSessionIdxOfPipe(PipeId) != VSTREAMPROC_NO_SESSIONIDXOFPIPE)
        && (retVal == VSTREAMPROC_OK))
      {
        vStreamProc_Snapshot_CreateEntryNodeSnapshot(processingNodeId);
      }
    }
    /* Otherwise: */
    else
    {
      /*#50 Set the wave data for the provided level. */
      vStreamProc_Stream_SetEntryPointWaveData(processingNodeId, WaveInfo);
      retVal = VSTREAMPROC_OK;
    }

    /* #60 Remap the return value if necessary. */
    if (retVal == VSTREAMPROC_INSUFFICIENT_INPUT)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    }
    else if (retVal == VSTREAMPROC_INSUFFICIENT_OUTPUT)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_INPUT;
    }
    else
    {
      /* Nothing to do */
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_EndEntryPointWave()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_EndEntryPointWave(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoConstPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
      vStreamProc_GetNamedOutputPortIdxOfEntryPoint(
        vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the processing phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_IDLE_PHASEOFPIPEINFO)
    || (vStreamProc_GetPhaseOfPipeInfo(PipeId) == VSTREAMPROC_PROCESS_PHASEOFPIPEINFO))
  {
     /* #20 End wave with given information. */
     vStreamProc_Stream_EndOutputPortWave(
      processingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
      WaveInfo->WaveTypeSymbolicName);


     /* #30 Create entry node snapshot if wave start was successful and the wave level is the lowest level. */
     if ( (vStreamProc_Stream_GetWaveLevel(PipeId, WaveInfo->WaveTypeSymbolicName) == 0u)
       && (vStreamProc_GetSessionIdxOfPipe(PipeId) != VSTREAMPROC_NO_SESSIONIDXOFPIPE))
     {
       vStreamProc_Snapshot_CreateEntryNodeSnapshot(processingNodeId);
     }

     retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetEntryPointWaveInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetEntryPointWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedOutputPort(
      vStreamProc_GetNamedOutputPortIdxOfEntryPoint(
        vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + EntryPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query wave info from entry node output port. */
  retVal = vStreamProc_Stream_GetOutputPortWaveInfo(
    processingNodeId,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_EntryNode_Output,
    WaveInfo);

  retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetExitPointWaveInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetExitPointWaveInfo(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  vStreamProc_ProcessingNodeIdType processingNodeId =
    vStreamProc_GetProcessingNodeIdxOfNamedInputPort(
      vStreamProc_GetNamedInputPortIdxOfExitPoint(
        vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + ExitPointSymbolicName));

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query wave info from exit node input port. */
  retVal = vStreamProc_Stream_GetInputPortWaveInfo(
    processingNodeId,
    vStreamProcConf_vStreamProcInputPort_vStreamProc_ExitNode_Input,
    WaveInfo);

  retVal = vStreamProc_Pipe_InvertInsufficientRetVal(retVal);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_CreateSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_CreateSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the correct phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) != VSTREAMPROC_IDLE_PHASEOFPIPEINFO))
  {
    /* #20 Create the session. */
    if (vStreamProc_GetSessionIdxOfPipe(PipeId) != VSTREAMPROC_NO_SESSIONIDXOFPIPE)
    {
      retVal = vStreamProc_Session_CreateSession(PipeId);
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetEntryPointInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetEntryPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query the entry point session info. */
  retVal = vStreamProc_Session_GetEntryPointInfoOfSession(
    PipeId,
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId)
      + (vStreamProc_EntryPointIdType)EntryPointInfo->EntryPointSymbolicName,
    &EntryPointInfo->StreamPositionInfo,
    &EntryPointInfo->MetaData);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetExitPointInfoOfSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetExitPointInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointInfoPtrType ExitPointInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query the exit point session info. */
  retVal = vStreamProc_Session_GetExitPointInfoOfSession(
    PipeId,
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId)
      + (vStreamProc_ExitPointIdType)ExitPointInfo->ExitPointSymbolicName,
    &ExitPointInfo->StreamPositionInfo,
    &ExitPointInfo->MetaData);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetEntryPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetEntryPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointIdType EntryPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query wave info from session. */
  retVal = vStreamProc_Session_GetEntryPointWaveInfoOfSession(
    PipeId,
    vStreamProc_GetEntryPointStartIdxOfPipe(PipeId) + (vStreamProc_EntryPointIdType)EntryPointSymbolicName,
    WaveInfo);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetExitPointWaveInfoOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetExitPointWaveInfoOfSession(
  vStreamProc_PipeIdType PipeId,
  vStreamProc_ExitPointIdType ExitPointSymbolicName,
  vStreamProc_WaveInfoPtrType WaveInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query wave info from session. */
  retVal = vStreamProc_Session_GetExitPointWaveInfoOfSession(
    PipeId,
    vStreamProc_GetExitPointStartIdxOfPipe(PipeId) + (vStreamProc_ExitPointIdType)ExitPointSymbolicName,
    WaveInfo);

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetMaxDataSizeOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetMaxDataSizeOfSession(
  vStreamProc_PipeIdType PipeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType length = 0u;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query getting session data max size. */
  length = vStreamProc_Session_GetMaxDataSizeOfSession(PipeId);

  return length;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_GetDataOfSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_GetDataOfSession(
  vStreamProc_PipeIdType PipeId,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Ensure that pipe is in the correct phase. */
  if ((vStreamProc_GetPhaseOfPipeInfo(PipeId) != VSTREAMPROC_IDLE_PHASEOFPIPEINFO))
  {
    /* #20 Query getting session data. */
    retVal = vStreamProc_Session_GetDataOfSession(PipeId, SessionData, Length);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_RestoreSession()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Pipe_RestoreSession(
  vStreamProc_PipeIdType PipeId,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) SessionData,
  vStreamProc_LengthType SessionDataLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;
  Std_ReturnType localRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Init the pipe. */
  localRetVal = vStreamProc_Pipe_Init(PipeId);

  if (localRetVal == E_OK)
  {
  /* #20 Restore session data. */
    retVal = vStreamProc_Session_RestoreSession(PipeId, SessionData, SessionDataLength);
  }

  return retVal;
}

#if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
/**********************************************************************************************************************
 *  vStreamProc_Pipe_DetChecksAccessPointInfos()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_Pipe_DetChecksAccessPointInfos(                                               /* PRQA S 6060 */ /* MD_MSR_STPAR */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check plausibility of all input parameters. */
  /* Iterate over all passed access point infos and apply check operation. */
  if (vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_CHECK_INFO, TRUE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount) != VSTREAMPROC_OK)
  {
    errorID = VSTREAMPROC_E_ID_PARAM;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}

/**********************************************************************************************************************
 *  vStreamProc_Pipe_DetChecksAccessPointIsLocked()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(uint8, VSTREAMPROC_CODE) vStreamProc_Pipe_DetChecksAccessPointIsLocked(                                            /* PRQA S 6060 */ /* MD_MSR_STPAR */
  vStreamProc_PipeIdType PipeId,
  vStreamProc_EntryPointInfoPtrType EntryPointInfos,
  vStreamProc_EntryPointIdType EntryPointCount,
  vStreamProc_ExitPointInfoPtrType ExitPointInfos,
  vStreamProc_ExitPointIdType ExitPointCount,
  P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_DATA) ErrorId)
{
  uint8 errorID;

  /* #10 Check if all access points are locked. */
  if (vStreamProc_Pipe_IterateAccessPointInfos(
    vStreamProc_Pipe_InitAccessPointIteratorParam(PipeId, VSTREAMPROC_ACCESS_POINT_OPERATION_CHECK_IS_LOCKED, TRUE),
    EntryPointInfos, EntryPointCount, ExitPointInfos, ExitPointCount) != VSTREAMPROC_OK)
  {
    errorID = VSTREAMPROC_E_WRONG_SEQUENCE;
  }
  else
  {
    /* Default to no error. */
    errorID = VSTREAMPROC_E_NO_ERROR;
  }

  *ErrorId = errorID;

  return errorID;
}
#endif

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Pipe.c
 *********************************************************************************************************************/
