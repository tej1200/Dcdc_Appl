/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION  
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_RangeSerialization.h
 *        \brief  vStreamProc RangeSerialization Processing Node Header File
 *
 *      \details  Header file of the vStreamProc RangeSerialization processing node.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_PROCESSINGNODE_RANGESERIALIZATION_H)
# define VSTREAMPROC_PROCESSINGNODE_RANGESERIALIZATION_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_RangeSerialization_Init()
 *********************************************************************************************************************/
/*!
 *  \brief          Initializes the passed node.
 *  \details        -
 *  \param[in]      ProcNodeInfo              The processing node information to operate on.
 *  \return         E_OK                      Initialization was successful
 *  \return         E_NOT_OK                  Initialization was unsuccessful
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-313355
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_RangeSerialization_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_RangeSerialization_SignalHandler_DataAvailable()
 *********************************************************************************************************************/
/*!
 *  \brief          Serializes the absolute stream start position and the total length.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-313355
 *********************************************************************************************************************/
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_RangeSerialization_SignalHandler_DataAvailable(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo);

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_PROCESSINGNODE_RANGESERIALIZATION_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_RangeSerialization.h
 *********************************************************************************************************************/
