/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_SimpleBufferNode.c
 *        \brief  vStreamProc Pipe Simple Buffer Node Source Code File
 *
 *      \details  Implementation of the vStreamProc simple buffer storage node
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_SIMPLEBUFFERNODE_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc.h"
#include "vStreamProc_Stream.h"
#include "vStreamProc_SimpleBufferNode.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (VSTREAMPROC_LOCAL)
# define VSTREAMPROC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 2 */ /* MD_MSR_FctLikeMacro */
#define VSTREAMPROC_ADD_CONSUMED_AND_AVAILABLE_LENGTH(currentInfoId) \
  (vStreamProc_GetConsumedLengthOfStorageNodeBufferInfo(currentInfoId) + vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(currentInfoId))


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VSTREAMPROC_START_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_StorageNode_Defragment
 *********************************************************************************************************************/
/*! \brief          Copy data from the end and overwrite consumed data to increase the available length
 *  \details        -
 *  \param[in]      StorageNodeId     Id of a storage node
 *  \param[in]      ForceDefragment   TRUe enforces defragmentation, even if conditions aren't met
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \trace          CREQ-205148
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_StorageNode_Defragment(
  vStreamProc_StorageNodeIdType StorageNodeId,
  boolean ForceDefragment);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_Init(
  vStreamProc_StorageNodeIdType StorageNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StorageOutputPortIterType outputPortIdx;
  Std_ReturnType retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize consumed length and available length */
  vStreamProc_SetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId, 0);
  vStreamProc_SetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId,
    vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId));

  /* #20 Initialize lock count */
  vStreamProc_SetRequestLockCntOfStorageNodeBufferInfo(StorageNodeId, 0);

  /* #30 Initialize write lock */
  vStreamProc_SetWriteRequestLockOfStorageNodeBufferInfo(StorageNodeId, FALSE);

  /* #40 Iterate over all output ports and initialize associated information */
  for ( outputPortIdx = vStreamProc_GetStorageOutputPortStartIdxOfStorageNode(StorageNodeId);
        outputPortIdx < vStreamProc_GetStorageOutputPortEndIdxOfStorageNode(StorageNodeId);
        outputPortIdx++ )
  {
    vStreamProc_SetConsumeLenOfStorageOutputPortInfo(outputPortIdx, 0);
    vStreamProc_SetReadRequestLockOfStorageOutputPortInfo(outputPortIdx, FALSE);
    vStreamProc_SetActiveInformationOfStorageOutputPortInfo(outputPortIdx, FALSE);
  }

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_WriteInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_WriteInfo(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) WriteInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if RequestBufferPtr is a null pointer */
  if (WriteInfoPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    vStreamProc_LengthType  requestLength = WriteInfoPtr->RequestLength;
    vStreamProc_LengthType  availableLen;

    if (vStreamProc_GetRequestLockCntOfStorageNodeBufferInfo(StorageNodeId) == 0u)
    {
      availableLen  = VSTREAMPROC_ADD_CONSUMED_AND_AVAILABLE_LENGTH(StorageNodeId);
    }
    else
    {
      availableLen  = vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId);
    }

    /* #20 Update the user request with information */
    WriteInfoPtr->AvailableLength     = availableLen;
    WriteInfoPtr->DataTypeInfo.Id     = vStreamProc_GetDataElementTypeOfStorageNode(StorageNodeId);
    WriteInfoPtr->DataTypeInfo.Size   = vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

    /* #30 Report insufficient output if requested length cannot be provided. */
    if (availableLen >= requestLength)
    {
      retVal = VSTREAMPROC_OK;
    }
    else
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    }
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_WRITE_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_WriteRequest
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_WriteRequest(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_WriteRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA) RequestBufferPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if RequestBufferPtr is a null pointer */
  if (RequestBufferPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    vStreamProc_StorageInfoPtrType  storageInfo   = &RequestBufferPtr->StorageInfo;
    vStreamProc_LengthType          requestLength = storageInfo->RequestLength;
    vStreamProc_LengthType          availableLen  = vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId);

    retVal = VSTREAMPROC_OK;

    /* #20 Check if input buffer length fits into output buffer. */
    if (availableLen < requestLength)
    {
      /* #30 The buffer should only be defragmented if the resulting memory area is big enough to store the request */
      if (VSTREAMPROC_ADD_CONSUMED_AND_AVAILABLE_LENGTH(StorageNodeId) >= requestLength)
      {
        /* Defragment Buffer */
        vStreamProc_StorageNode_Defragment(StorageNodeId, TRUE);
        availableLen = vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId);
      }
    }

    if (availableLen < requestLength)
    {
      retVal = VSTREAMPROC_INSUFFICIENT_OUTPUT;
    }

    /* #40 Update the user request with information */
    storageInfo->AvailableLength    = availableLen;
    storageInfo->DataTypeInfo.Id    = vStreamProc_GetDataElementTypeOfStorageNode(StorageNodeId);
    storageInfo->DataTypeInfo.Size  = vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

    if ( (retVal == VSTREAMPROC_OK)
      && (availableLen != 0u) )
    {
      /* Use data element size to calculate byte offset into buffer. */
      vStreamProc_LengthType byteOffset = (vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId) - availableLen)
        * vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);
      RequestBufferPtr->Buffer = &vStreamProc_GetStorageNodeBufferOfStorageNode(StorageNodeId)[byteOffset];

      /* Engage write lock. */
      if (!vStreamProc_IsWriteRequestLockOfStorageNodeBufferInfo(StorageNodeId))
      {
        vStreamProc_SetWriteRequestLockOfStorageNodeBufferInfo(StorageNodeId, TRUE);
        vStreamProc_IncRequestLockCntOfStorageNodeBufferInfo(StorageNodeId);
      }
    }
    else
    {
      RequestBufferPtr->Buffer = NULL_PTR;
    }
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_WRITE_REQUEST, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
*  vStreamProc_SimpleBufferNode_WriteAck
**********************************************************************************************************************/
/*!
* Internal comment removed.
 *
 *
 *
*/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_WriteAck(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_LengthType ProducedLen)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if ProducedLen is greater than the TotalBufferLen */
  if (ProducedLen > vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId))
  {
    errorID = VSTREAMPROC_E_PARAM_SIZE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Update the workspace information. */
    vStreamProc_SetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId,
      vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId) - ProducedLen);

    /* Release write lock. */
    if (vStreamProc_IsWriteRequestLockOfStorageNodeBufferInfo(StorageNodeId))
    {
      vStreamProc_SetWriteRequestLockOfStorageNodeBufferInfo(StorageNodeId, FALSE);
      vStreamProc_DecRequestLockCntOfStorageNodeBufferInfo(StorageNodeId);
    }

    retVal = VSTREAMPROC_OK;
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_WRITE_ACK, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_ReadInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_ReadInfo(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_StorageInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ReadInfoPtr,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of input parameters */
  /* Check if RequestBufferPtr is a null pointer */
  if (ReadInfoPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    retVal = VSTREAMPROC_INSUFFICIENT_INPUT;

    /* #20 Check if the output port is active */
    if (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(StorageOutputPortId))
    {
      /* Only active consumers are allowed to read data  */
      vStreamProc_LengthType  requestLength     = ReadInfoPtr->RequestLength;
      vStreamProc_LengthType  availableDataLen  = vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
        - vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId)
        - vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId);

      /* #30 Fill the user request with appropriate information */
      ReadInfoPtr->AvailableLength    = availableDataLen;
      ReadInfoPtr->DataTypeInfo.Id    = vStreamProc_GetDataElementTypeOfStorageNode(StorageNodeId);
      ReadInfoPtr->DataTypeInfo.Size  = vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

      /* #40 Report insufficient input if requested length cannot be provided. */
      if (availableDataLen >= requestLength)
      {
        retVal = VSTREAMPROC_OK;
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_READ_INFO, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_ReadRequest
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_ReadRequest(
  vStreamProc_StorageNodeIdType StorageNodeId,
  P2VAR(vStreamProc_ReadRequestType, AUTOMATIC, VSTREAMPROC_APPL_DATA) RequestBufferPtr,
 vStreamProc_StorageOutputPortIdType StorageOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of input parameters. */
  /* Check if RequestBufferPtr is a null pointer */
  if (RequestBufferPtr == NULL_PTR)
  {
    errorID = VSTREAMPROC_E_PARAM_POINTER;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    vStreamProc_StorageInfoPtrType storageInfo = &RequestBufferPtr->StorageInfo;

    retVal = VSTREAMPROC_INSUFFICIENT_INPUT;

    RequestBufferPtr->Buffer        = NULL_PTR;
    storageInfo->AvailableLength    = 0u;
    storageInfo->DataTypeInfo.Id    = vStreamProc_GetDataElementTypeOfStorageNode(StorageNodeId);
    storageInfo->DataTypeInfo.Size  = vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

    /* #20 If the output port is active: */
    if (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(StorageOutputPortId))
    {
      /* Only active consumers are allowed to read data */

      /* #30 Fill the user request with available data length. */
      storageInfo->AvailableLength  = vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
        - vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId)
        - vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId);

      /* #40 Report insufficient input if requested length cannot be provided. Otherwise: */
      if (storageInfo->AvailableLength >= storageInfo->RequestLength)
      {
        /* Use data element size to calculate byte offset into buffer. */
        vStreamProc_LengthType byteOffset = vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId)
          * vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

        /* #50 Fill the user request with a pointer to the data buffer. */
        RequestBufferPtr->Buffer      = &vStreamProc_GetStorageNodeBufferOfStorageNode(StorageNodeId)[byteOffset];

        /* #60 Lock the buffer based on the current output port if it is not locked yet and data is available. */
        if ( (!vStreamProc_IsReadRequestLockOfStorageOutputPortInfo(StorageOutputPortId))
          && (storageInfo->AvailableLength != 0u) )
        {
          vStreamProc_SetReadRequestLockOfStorageOutputPortInfo(StorageOutputPortId, TRUE);
          vStreamProc_IncRequestLockCntOfStorageNodeBufferInfo(StorageNodeId);
        }

        retVal = VSTREAMPROC_OK;
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_READ_REQUEST, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
*  vStreamProc_SimpleBufferNode_ReadAck
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_ReadAck(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_LengthType ConsumedLen,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_StorageOutputPortIterType outputPortIdx;
  vStreamProc_LengthType minConsumedLen;
  uint8 errorID = VSTREAMPROC_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of all input parameters */
  /* Check if ConsumedLen is greater than the TotalBufferLen */
  if (ConsumedLen > (vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
        - vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId)
        - vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId)))
  {
    errorID = VSTREAMPROC_E_PARAM_SIZE;
  }
  else
# endif /* VSTREAMPROC_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    retVal = VSTREAMPROC_OK;

    /* #20 Update new consumed length for current consumer */
    vStreamProc_SetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId,
      vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId) + ConsumedLen);
    minConsumedLen = vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId);

    /* #30 Find the smallest consumer length over all consumers */
    for (outputPortIdx = vStreamProc_GetStorageOutputPortStartIdxOfStorageNode(StorageNodeId);
         outputPortIdx < vStreamProc_GetStorageOutputPortEndIdxOfStorageNode(StorageNodeId);
         outputPortIdx++)
    {
      if ( (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(outputPortIdx))
        && (vStreamProc_GetConsumeLenOfStorageOutputPortInfo(outputPortIdx) < minConsumedLen) )
      {
        minConsumedLen = vStreamProc_GetConsumeLenOfStorageOutputPortInfo(outputPortIdx);
      }
    }

    /* #40 Set smallest consumer length for Storage-Node */
    vStreamProc_SetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId, minConsumedLen);

    /* #50 Release the lock based on the current output port */
    if (vStreamProc_IsReadRequestLockOfStorageOutputPortInfo(StorageOutputPortId))
    {
      vStreamProc_SetReadRequestLockOfStorageOutputPortInfo(StorageOutputPortId, FALSE);
      vStreamProc_DecRequestLockCntOfStorageNodeBufferInfo(StorageNodeId);
    }

    /* #60 Defragment Buffer */
    vStreamProc_StorageNode_Defragment(StorageNodeId, FALSE);
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
  if (errorID != VSTREAMPROC_E_NO_ERROR)
  {
    (void)Det_ReportError(VSTREAMPROC_MODULE_ID, VSTREAMPROC_INSTANCE_ID_DET, VSTREAMPROC_API_ID_READ_ACK, errorID);
  }
# else
  VSTREAMPROC_DUMMY_STATEMENT(errorID);                                                                                 /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */
# endif /* VSTREAMPROC_DEV_ERROR_REPORT */

  return retVal;
}

/**********************************************************************************************************************
*  vStreamProc_SimpleBufferNode_SetActivationSignal
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_SetActivationSignal(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId,
  vStreamProc_OutputActivationType ActivationSignalValue)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(StorageNodeId);                                                                           /* PRQA S 2983,3112 */ /* MD_MSR_DummyStmt */

  /* #10 Set the new activation value. */
  vStreamProc_SetActiveInformationOfStorageOutputPortInfo(StorageOutputPortId, ActivationSignalValue);
}

/**********************************************************************************************************************
*  vStreamProc_SimpleBufferNode_DiscardAllData
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_DiscardAllData(
  vStreamProc_StorageNodeIdType StorageNodeId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StorageOutputPortIterType storageOutputPortIdx;

  /* ----- Implementation ----------------------------------------------- */
  vStreamProc_LengthType availableDataLength =
    vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
    - vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId);


  /* #10 Set the consume length of all storage output ports to the available length. */
  for (storageOutputPortIdx = vStreamProc_GetStorageOutputPortStartIdxOfStorageNode(StorageNodeId);
       storageOutputPortIdx < vStreamProc_GetStorageOutputPortEndIdxOfStorageNode(StorageNodeId);
       storageOutputPortIdx++)
  {
    if (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(storageOutputPortIdx))
    {
      vStreamProc_SetConsumeLenOfStorageOutputPortInfo(storageOutputPortIdx, availableDataLength);
    }
  }

  /* #20 Set the overall consume length to the available length */
  vStreamProc_SetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId, availableDataLength);

  /* #30 Defragment Buffer */
  vStreamProc_StorageNode_Defragment(StorageNodeId, TRUE);
}

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_StorageNode_Defragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_StorageNode_Defragment(
  vStreamProc_StorageNodeIdType StorageNodeId,
  boolean ForceDefragment)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_StorageOutputPortIterType outputPortIdx;
  vStreamProc_StorageNodeBufferIterType targetIdx;
  vStreamProc_StorageNodeBufferIterType upperBufferBoundary;

  /* ----- Implementation ----------------------------------------------- */

  /* #10 Defragmentation should be executed only if:
   *      No read operation is pending (buffer is locked) and
   *      Any data was consumed and
   *      the remaining amount of free memory is smaller than the configured threshold or size of the data elements
   *      or
   *      All data was consumed
   *      or
   *      Enforced by caller
   */
  if (vStreamProc_GetRequestLockCntOfStorageNodeBufferInfo(StorageNodeId) == 0u)
  {
    boolean performDefrag = FALSE;

    if (vStreamProc_GetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId) != 0u)
    {
      performDefrag = ForceDefragment;

      if (vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId) < vStreamProc_GetDefragmentThresholdOfStorageNode(StorageNodeId))
      {
        performDefrag = TRUE;
      }
      else
      {
        if (VSTREAMPROC_ADD_CONSUMED_AND_AVAILABLE_LENGTH(StorageNodeId) == vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId))
        {
          performDefrag = TRUE;
        }
      }
    }

    if (performDefrag == TRUE)
    {
      vStreamProc_StorageNodeBufferType     buffer    = vStreamProc_GetStorageNodeBufferOfStorageNode(StorageNodeId);
      vStreamProc_StorageNodeBufferIterType sourceIdx =
        (vStreamProc_StorageNodeBufferIterType)vStreamProc_GetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId);

      /* #20 Copy data from the end and overwrite consumed data */
      upperBufferBoundary = ((vStreamProc_StorageNodeBufferIterType)(vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
        - sourceIdx
        - (vStreamProc_StorageNodeBufferIterType)vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId))
        * (vStreamProc_StorageNodeBufferIterType)vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId));

      /* Convert element index to byte index. */
      sourceIdx *= (vStreamProc_StorageNodeBufferIterType)vStreamProc_GetDataElementSizeOfStorageNode(StorageNodeId);

      for (targetIdx = 0u; targetIdx < upperBufferBoundary; targetIdx++)
      {
        buffer[targetIdx] = buffer[sourceIdx];
        /* Delete copied data */
        buffer[sourceIdx] = 0u;

        sourceIdx++;
      }

      /* #30 Update storage Workspaces after defragment */
      for ( outputPortIdx = vStreamProc_GetStorageOutputPortStartIdxOfStorageNode(StorageNodeId);
            outputPortIdx < vStreamProc_GetStorageOutputPortEndIdxOfStorageNode(StorageNodeId);
            outputPortIdx++ )
      {
        if (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(outputPortIdx))
        {
          vStreamProc_SetConsumeLenOfStorageOutputPortInfo(outputPortIdx,
            vStreamProc_GetConsumeLenOfStorageOutputPortInfo(outputPortIdx)
            - vStreamProc_GetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId));
        }
      }

      vStreamProc_SetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId,
        vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId)
        + vStreamProc_GetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId));

      vStreamProc_SetConsumedLengthOfStorageNodeBufferInfo(StorageNodeId, 0u);

      /* #40 Inform subscribers of available storage. */
      vStreamProc_Stream_HandleStorageAvailable(StorageNodeId);
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_SimpleBufferNode_IsEmpty
 *********************************************************************************************************************/
/*!
* Internal comment removed.
 *
 *
 *
*/
FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_SimpleBufferNode_IsEmpty(
  vStreamProc_StorageNodeIdType StorageNodeId,
  vStreamProc_StorageOutputPortIdType StorageOutputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean retVal = TRUE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the output port is active */
  if (vStreamProc_IsActiveInformationOfStorageOutputPortInfo(StorageOutputPortId))
  {
    vStreamProc_LengthType  availableDataLen = vStreamProc_GetStorageNodeBufferLengthOfStorageNode(StorageNodeId)
      - vStreamProc_GetAvailableLengthOfStorageNodeBufferInfo(StorageNodeId)
      - vStreamProc_GetConsumeLenOfStorageOutputPortInfo(StorageOutputPortId);

    /* #20 Port is not empty if data is remaining. */
    if (availableDataLen > 0u)
    {
      retVal = FALSE;
    }
  }

  return retVal;
}

#define VSTREAMPROC_STOP_SEC_CODE
#include "MemMap.h"                                                                                                     /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_SimpleBufferNode.c
 *********************************************************************************************************************/
