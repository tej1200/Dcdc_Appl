/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_Hash.c
 *        \brief  vStreamProc Hash Sub Module Source Code File
 *      \details  Implementation of the Hash sub module for the vStreamProc framework
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_HASH_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_Hash.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_HASH_CONFIG == STD_ON)
# include "Csm.h"

/**********************************************************************************************************************
 *  DEFINES
 *********************************************************************************************************************/
# define MAXIMUM_HASH_LENGTH 64u

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if the processing length is limited by the progress output port.
 *  \details        -
 *  \param[in]      ProcNodeInfo        The processing node information to operate on.
 *  \param[in,out]  ProcessingLength    IN:  The available input length.
 *                                      OUT: The resulting processing length.
 *  \return         VSTREAMPROC_OK      No internal errors occurred.
 *  \return         VSTREAMPROC_FAILED  Internal error occurred.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 *  \brief          Updates the progress output port with the processed data length if connected.
 *  \details        -
 *  \param[in]      ProcNodeInfo     The processing node information to operate on.
 *  \param[in]      ProcessedLength  The processed data length.
 *  \return         VSTREAMPROC_OK      The update was successful or the progress port is not connected.
 *  \return         VSTREAMPROC_FAILED  The update was not successful.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;
  vStreamProc_LengthType limitedLength = *ProcessingLength;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_Hash_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    /* #20 Update the progress. */
    retVal = vStreamProc_GetOutputPortInfo(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      &progressOutputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      if (progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength < limitedLength)
      {
        limitedLength = progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength;
      }
    }

  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  *ProcessingLength = limitedLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_Hash_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    /* #20 Update the progress. */
    retVal = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      ProcessedLength,
      &progressOutputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      retVal =vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, ProcessedLength, TRUE, &progressOutputPortInfo);
    }
  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  VAR(Std_ReturnType, AUTOMATIC) retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's workspace */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize state value of workspace. */
  workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;

  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_UpdateHash
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_UpdateHash(        /* PRQA S 6010, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse  = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult   = VSTREAMPROC_FAILED;
  Std_ReturnType                  stdRetVal;

  P2CONST(vStreamProc_ProcessingNode_Hash_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Hash_Config(ProcNodeInfo);                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    /* #10 Initialize the processing node's hash calculation. */
    stdRetVal = Csm_Hash(specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config, CRYPTO_OPERATIONMODE_START,
      NULL_PTR, 0u, NULL_PTR, NULL_PTR);

    /* In case everything went fine set next state. */
    if (stdRetVal == E_OK)
    {
      workspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
    }
  }

  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    vStreamProc_LengthType processingLength = 0u;
    vStreamProc_InputPortInfoType inputPortInfo;

    /* #20 Query the input port. */
    if (vStreamProc_PrepareInputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcInputPort_vStreamProc_Hash_InputData, &inputPortInfo) == VSTREAMPROC_OK)
    {
      currentResult = vStreamProc_RequestInputPortData(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, 1u, &inputPortInfo);
    }

    /* #30 Check if the processing length has to be limited. */
    if (currentResult == VSTREAMPROC_OK)
    {
      processingLength = inputPortInfo.ReadRequest.StorageInfo.AvailableLength;
      currentResult = vStreamProc_ProcessingNode_Hash_CheckForInputLimitation(ProcNodeInfo, &processingLength);
    }

    if (currentResult == VSTREAMPROC_OK)
    {
      /* #40 Update the CSM with the available data. */
      stdRetVal = Csm_Hash(specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config,
        CRYPTO_OPERATIONMODE_UPDATE, vStreamProc_GetTypedReadRequestBuffer_uint8(&inputPortInfo.ReadRequest), processingLength,
        NULL_PTR, NULL_PTR);

      if (stdRetVal == E_OK)
      {
        /* #50 In case the hash update was successful, update the signal response and acknowledge the input port. */
        vStreamProc_SetSignalResponseFlag(
          signalResponse,
          VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

        currentResult = vStreamProc_AcknowledgeInputPort(ProcNodeInfo, processingLength, TRUE, &inputPortInfo);


      }
      else
      {
        /* #60 Otherwise, release all ports and set the overall result to failed. */
        (void)vStreamProc_ReleaseAllPorts(ProcNodeInfo);
        currentResult = VSTREAMPROC_FAILED;
      }

      if (currentResult == VSTREAMPROC_OK)
      {
        currentResult = vStreamProc_ProcessingNode_Hash_WriteProgressOutputPort(ProcNodeInfo, processingLength);
      }
    }
  }

  /* #70 Check overall result and set signal response accordingly. */
  if (currentResult != VSTREAMPROC_FAILED)
  {
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_WriteHash
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_WriteHash(         /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH , MD_MSR_STCYC , MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse  = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult   = VSTREAMPROC_FAILED;
  uint32  HashLength = MAXIMUM_HASH_LENGTH;
  uint8   hashValue[MAXIMUM_HASH_LENGTH];

  P2CONST(vStreamProc_ProcessingNode_Hash_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Hash_Config(ProcNodeInfo);                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  if (workspace->State == VSTREAMPROC_WS_STATE_INITIALIZED)
  {
    /* #10 Initialize the processing node's hash calculation if necessary. */
    if (Csm_Hash(
          specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config, CRYPTO_OPERATIONMODE_START,
          NULL_PTR, 0u,
          NULL_PTR, NULL_PTR) == E_OK)
    {
      /* In case everything went fine set next state. */
      workspace->State = VSTREAMPROC_WS_STATE_PROCESSING;
    }
  }

  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    vStreamProc_OutputPortInfoType  outputPortInfo;

  /* #20 Finalize the hash calculation. */
    if (Csm_Hash(
      specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config, CRYPTO_OPERATIONMODE_FINISH,
      NULL_PTR, 0u,
      &(hashValue[0u]), &HashLength) == E_OK)
    {
      /* #30 Request the output buffer. */
      if (vStreamProc_PrepareOutputPortInfo(ProcNodeInfo, vStreamProcConf_vStreamProcOutputPort_vStreamProc_Hash_OutputData, &outputPortInfo) == VSTREAMPROC_OK)
      {
        currentResult = vStreamProc_RequestOutputPortData(ProcNodeInfo, vStreamProcConf_vStreamProcDataType_uint8, HashLength, &outputPortInfo);
      }
    }

    /* #40 Write the passed hash value to the output port. */
    if (currentResult == VSTREAMPROC_OK)
    {
      P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) outputBuffer = vStreamProc_GetTypedWriteRequestBuffer_uint8(&outputPortInfo.WriteRequest);
      vStreamProc_StorageNodeBufferIterType index;

      /* Copy hash value to output buffer. */
      for (index = 0u; index < HashLength; index++)
      {
        outputBuffer[index] = hashValue[index];
      }

      vStreamProc_SetSignalResponseFlag(
        signalResponse,
        VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

      /* Acknowledge the write. */
      currentResult = vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, HashLength, TRUE, &outputPortInfo);

      workspace->State = VSTREAMPROC_WS_STATE_INITIALIZED;
    }
    else
    {
      /* #50 Otherwise, release all ports and set the overall result to failed. */
      (void)vStreamProc_ReleaseAllPorts(ProcNodeInfo);
      currentResult = VSTREAMPROC_FAILED;
    }
  }

  /* #60 Check overall result and set signal response accordingly */
  if (currentResult != VSTREAMPROC_FAILED)
  {
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_StoreCsmContext
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_StoreCsmContext(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

#if  (VSTREAMPROC_PROCESSINGNODETYPE_HASH_STORE_CSM_CONTEXT == STD_ON)
  P2CONST(vStreamProc_ProcessingNode_Hash_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Hash_Config(ProcNodeInfo);                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Hint: Actual size of context is written by CSM. */
  workspace->SizeOfStoredCsmContext = specializedCfgPtr->SizeOfContextOfProcessingNode_Hash_Config;

  /* #10 Check if there is a context buffer. */
  if (specializedCfgPtr->SizeOfContextOfProcessingNode_Hash_Config > 0u)
  {
    /* #20 Check if the node is in PROCESSING state. */
    if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
    {
      Std_ReturnType csmRetVal;

      /* #30 Store the CSM context in the workspace via the CSM. */
      csmRetVal = Csm_SaveContextJob(
        specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config,
        workspace->CsmContextBuffer,
        &workspace->SizeOfStoredCsmContext);

      switch (csmRetVal)
      {
        /* #40 If the storing was successful, confirm the signal. */
        case E_OK:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
          break;
        }
        /* #50 If the CSM is busy, return pending to try again in the next cycle. */
        case CRYPTO_E_BUSY:
        {
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
          break;
        }
        /* #60 If any other return value: Consider as error. */
        default:
        {
          /* Either E_NOT_OK or an unexpected return value was received. */
          signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
          break;
        }
      }
    }
  }
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
#endif

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_Cancel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_Cancel(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

  P2CONST(vStreamProc_ProcessingNode_Hash_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;

  /* ----- Implementation ----------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Hash_Config(ProcNodeInfo);
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Cancel job. */
    (void) Csm_CancelJob(specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config, CRYPTO_OPERATIONMODE_FINISH);

    /* #30 Reset state. */
    workspace->State = VSTREAMPROC_WS_STATE_UNDEFINED;

    /* #40 Set signal response to confirm. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Hash_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Hash_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
#if (VSTREAMPROC_PROCESSINGNODETYPE_HASH_STORE_CSM_CONTEXT == STD_ON)
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2CONST(vStreamProc_ProcessingNode_Hash_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Hash_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_CONST) workspace;
  Std_ReturnType csmRetVal;

  /* ----- Implementation ----------------------------------------------- */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Hash_Config(ProcNodeInfo);                             /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  workspace = vStreamProc_GetTypedWorkspaceOfProcessingNode_Hash_WorkspaceType(ProcNodeInfo);                           /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 If a CSM job is active. */
  if (workspace->State == VSTREAMPROC_WS_STATE_PROCESSING)
  {
    /* #20 Restore the context. */
    csmRetVal = Csm_RestoreContextJob(
      specializedCfgPtr->CsmJobIdOfProcessingNode_Hash_Config,
      workspace->CsmContextBuffer,
      workspace->SizeOfStoredCsmContext);

    switch (csmRetVal)
    {
      /* #30 If the storing was successful, confirm the signal. */
      case E_OK:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
        break;
      }
      /* #40 If the CSM is busy, return pending to try again in the next cycle. */
      case CRYPTO_E_BUSY:
      {
        signalResponse = VSTREAMPROC_SIGNALRESPONSE_PENDING;
        break;
      }
      /* #50 If any other return value: Consider as error. */
      default:
      {
        /* Either E_NOT_OK or an unexpected return value was received. Leave signal response as failed.*/
        break;
      }
    }
  }
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }
  return signalResponse;
#else
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);
  return VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
#endif
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_HASH_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_Hash.c
 *********************************************************************************************************************/
