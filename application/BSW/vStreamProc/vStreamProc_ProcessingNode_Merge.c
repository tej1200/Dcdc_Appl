/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!       \file   vStreamProc_ProcessingNode_Merge.c
 *        \brief  vStreamProc Merge Processing Node Source Code File
 *
 *      \details  Implementation of the vStreamProc merge processing node
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_MERGE_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_Merge.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc_Cfg.h"

#if (VSTREAMPROC_PROCESSINGNODECLASS_VSTREAMPROC_MERGE == STD_ON)
/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_CopyData()
 *********************************************************************************************************************/
/*!
 *  \brief          Copies the data from the input port to the output port.
 *  \details        Uses the relative stream position to determine start indices.
 *  \param[in]      InputPortInfo       The input port where to read the data from.
 *  \param[in]      OutputPortInfo      The output port where to produce the data.
 *  \param[in]      MinLength           Amount of bytes to copy.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_CopyData(
  P2CONST(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortInfo,
  P2CONST(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortInfo,
  vStreamProc_LengthType MinLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_IsPortWaveEnding()
 *********************************************************************************************************************/
/*!
 *  \brief          Returns whether the wave is ending for the given input port.
 *  \details        -
 *  \param[in]      ProcessingNodeId  The id of the processing node.
 *  \param[in]      InputPortId       The id of the input port to check.
 *  \return         TRUE              The given port is ending.
 *  \return         FALSE             The given port is not ending.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_IsPortWaveEnding(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortIdType InputPortId);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_HandleFirstPort()
 *********************************************************************************************************************/
/*!
 *  \brief          Returns whether the wave is ending for the given input port.
 *  \details        -
 *  \param[in]      ProcNodeInfo                            The processing node information to operate on.
 *  \param[in]      InputPortInfoPtr                        The input port to operate on.
 *  \param[in]      OutputPortInfoPtr                       The output port to operate on.
 *  \param[in]      MinLength                               Amount of bytes to copy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_BUSY         Signal couldn't be processed because node is busy.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_PENDING      Signal processing was started and is still ongoing.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE  Signal was accepted but there is nothing to do right now.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_CONFIRM      Signal was accepted and successfully processed.
 *  \return         VSTREAMPROC_SIGNALRESPONSE_FAILED       Failure in signal processing.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_HandleFirstPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortInfoPtr,
  P2CONST(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortInfoPtr,
  vStreamProc_LengthType MinLength);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_CopyData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_CopyData(
  P2CONST(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortInfo,
  P2CONST(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortInfo,
  vStreamProc_LengthType MinLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType index;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy as many bytes as possible from input to output. */
  for (index = 0; index < MinLength; index++)
  {
    OutputPortInfo->WriteRequest.Buffer[index] = InputPortInfo->ReadRequest.Buffer[index];
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_IsPortWaveEnding
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(boolean, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_IsPortWaveEnding(
  vStreamProc_ProcessingNodeIdType ProcessingNodeId,
  vStreamProc_InputPortIdType InputPortId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveInfoType waveInfo;
  boolean isWaveEnding = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get wave for input port. */
  vStreamProc_ReturnType spRetVal = vStreamProc_GetInputPortWaveInfo(
    ProcessingNodeId,
    InputPortId,
    &waveInfo);

  if (spRetVal == VSTREAMPROC_OK)
  {
    /* #20 Check if wave is in ending state. */
    if (waveInfo.State == VSTREAMPROC_WAVESTATE_ENDING)
    {
      isWaveEnding = TRUE;
    }
    else
    {
      isWaveEnding = FALSE;
    }
  }

  return isWaveEnding;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_HandleFirstPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_HandleFirstPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2CONST(vStreamProc_InputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) InputPortInfoPtr,
  P2CONST(vStreamProc_OutputPortInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) OutputPortInfoPtr,
  vStreamProc_LengthType MinLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  P2VAR(vStreamProc_Merge_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_Merge_WorkspaceType(ProcNodeInfo);                        /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Copy data from input to output. */
  vStreamProc_ProcessingNode_Merge_CopyData(
    InputPortInfoPtr,
    OutputPortInfoPtr,
    MinLength);

  /* #20 If the wave is ending: */
  if(vStreamProc_ProcessingNode_Merge_IsPortWaveEnding(
    ProcNodeInfo->ProcessingNodeId,
    InputPortInfoPtr->SymbolicPortName) == TRUE)
  {
    vStreamProc_InputPortIdType nextPortId = ((InputPortInfoPtr->SymbolicPortName + 1u) % ProcNodeInfo->InputPortCount);

    /* #30 Save the next port to be used in the workspace. */
    workspace->CurrentActiveInputPort = nextPortId;

    /* #40 Register DataAvailable handler of the next port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      nextPortId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

    /* #50 Register the WaveEnd handler of the next port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      nextPortId,
      vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

    /* #60 Confirm the signal. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(vStreamProc_Merge_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_Merge_WorkspaceType(ProcNodeInfo);                        /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Block the DataAvailable signal handler signal of the second port. */
  (void)vStreamProc_SetInputPortSignalHandlerState(
    ProcNodeInfo->ProcessingNodeId,
    1u,
    vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
    VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

  /* #20 Register the WaveEnd signal handler signal of the first port. */
  (void)vStreamProc_SetInputPortSignalHandlerState(
    ProcNodeInfo->ProcessingNodeId,
    0u,
    vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
    VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

  /* #30 Initialize the workspace. */
  workspace->CurrentActiveInputPort  = 0u;

  return E_OK;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_SignalHandler_StartWave(        /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_WaveInfoType waveInfo;
  vStreamProc_SignalResponseType signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If the wave started on the first port: */
  if (ProcNodeInfo->SignalInfo.InputPortId == 0u)
  {
    vStreamProc_ReturnType outputWaveResult;

    /* #20 Start the output port wave. */
    (void)vStreamProc_GetInputPortWaveInfo(ProcNodeInfo->ProcessingNodeId,
      ProcNodeInfo->SignalInfo.InputPortId,
      &waveInfo);

    outputWaveResult = vStreamProc_StartOutputPortWave(
      ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData,
      &waveInfo);

    if (outputWaveResult == VSTREAMPROC_OK)
    {
      /* #30 Confirm the signal. */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
    }
  }
  else
  {
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_SignalHandler_DataAvailable
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_SignalHandler_DataAvailable(    /* PRQA S 6010, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal;
  vStreamProc_InputPortInfoType   inputPortInfo;
  vStreamProc_OutputPortInfoType  outputPortInfo;
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  boolean setInsufficientOutput = FALSE;
  P2CONST(vStreamProc_Merge_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_Merge_WorkspaceType(ProcNodeInfo);                        /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Prepare the ports. */
  if (ProcNodeInfo->SignalInfo.SignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable)
  {
    inputPortInfo.SymbolicPortName = workspace->CurrentActiveInputPort;
  }
  else
  {
    inputPortInfo.SymbolicPortName = ProcNodeInfo->SignalInfo.InputPortId;
  }

  outputPortInfo.SymbolicPortName = vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData;

  retVal = vStreamProc_PreparePortInfos(
    ProcNodeInfo,
    &inputPortInfo,
    1u,
    &outputPortInfo,
    1u);

  if (retVal == VSTREAMPROC_OK)
  {
    /* #20 Request port data with length of 0. */
    inputPortInfo.ReadRequest.StorageInfo.RequestLength = 0u;
    outputPortInfo.WriteRequest.StorageInfo.RequestLength = 0u;

    retVal = vStreamProc_GetPortInfos(
      ProcNodeInfo,
      &inputPortInfo,
      1u,
      &outputPortInfo,
      1u);
  }

  /* #30 If there's not enough space in the output port buffer: */
  if (outputPortInfo.WriteRequest.StorageInfo.AvailableLength < inputPortInfo.ReadRequest.StorageInfo.AvailableLength)
  {
    /* #40 Unregister the DataAvailable handler of the current port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        inputPortInfo.SymbolicPortName,
        vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    /* #50 Register the StorageAvailable handler of the output port. */
    (void)vStreamProc_SetOutputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData,
        vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

    /* #60 Block the WaveEnd handler of the current port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
        ProcNodeInfo->ProcessingNodeId,
        inputPortInfo.SymbolicPortName,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
        VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

    setInsufficientOutput = TRUE;
  }
  /* #70 Otherwise: */
  else
  {
    /* #80 Register the DataAvailable handler of the current port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcNodeInfo->ProcessingNodeId,
      inputPortInfo.SymbolicPortName,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

    /* #90 Unregister the StorageAvailable handler of the output port. */
    (void)vStreamProc_SetOutputPortSignalHandlerState(
      ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData,
      vStreamProcConf_vStreamProcSignal_vStreamProc_StorageAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_UNREGISTERED);

    /* #100 Register the WaveEnd handler of the current port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(
      ProcNodeInfo->ProcessingNodeId,
      inputPortInfo.SymbolicPortName,
      vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
  }

  if (retVal == VSTREAMPROC_OK)
  {
    retVal = vStreamProc_RequestPortData(
      ProcNodeInfo,
      &inputPortInfo,
      1u,
      &outputPortInfo,
      1u);
  }

  if (retVal == VSTREAMPROC_OK)
  {
    vStreamProc_LengthType minLength;

    if (inputPortInfo.ReadRequest.StorageInfo.AvailableLength > outputPortInfo.WriteRequest.StorageInfo.AvailableLength)
    {
      minLength = outputPortInfo.WriteRequest.StorageInfo.AvailableLength;
    }
    else
    {
      minLength = inputPortInfo.ReadRequest.StorageInfo.AvailableLength;
    }

    /* #110 If the request is from the first port: */
    if (ProcNodeInfo->SignalInfo.InputPortId == 0u)
    {
      /* #120 Handle data reception for the first port. */
      signalResponse = vStreamProc_ProcessingNode_Merge_HandleFirstPort(
        ProcNodeInfo,
        &inputPortInfo,
        &outputPortInfo,
        minLength);
    }
    else
    {
      /* #130 Otherwise copy the data from the input to the output port. */
      vStreamProc_ProcessingNode_Merge_CopyData(
        &inputPortInfo,
        &outputPortInfo,
        minLength);

      /* #140 Confirm the signal. */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_CONFIRM;
    }

    inputPortInfo.ReadRequest.StorageInfo.RequestLength = minLength;
    outputPortInfo.WriteRequest.StorageInfo.RequestLength = minLength;

    /* #150 Acknowledge ports. */
    (void)vStreamProc_AcknowledgePorts(
      ProcNodeInfo,
      &inputPortInfo,
      1u,
      &outputPortInfo,
      1u);

  }

  if (setInsufficientOutput)
  {
    /* #160 If there was not enough space in the output port stream: Signalize INSUFFICIENT_OUTPUT to caller. */
    ProcNodeInfo->OutputPortResults[outputPortInfo.SymbolicPortName] = VSTREAMPROC_INSUFFICIENT_OUTPUT;
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_SignalHandler_WaveEnd
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_SignalHandler_WaveEnd(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType      signalResponse        = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_WaveInfoType waveInfo;

  P2VAR(vStreamProc_Merge_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
    vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_Merge_WorkspaceType(ProcNodeInfo);                        /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If the wave ended for the first port: */
  if (ProcNodeInfo->SignalInfo.InputPortId == 0u)
  {
    /* #20 Block the WaveEnd signal handler of the first port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
                                                  0u,
                                                  vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
                                                  VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);
    /* #30 Confirm the signal. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }
  /* #40 Otherwise: */
  else
  {
    vStreamProc_ReturnType outputWaveResult;

    (void)vStreamProc_GetOutputPortWaveInfo(ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData,
      &waveInfo);

    /* #50 End the output port wave. */
    outputWaveResult = vStreamProc_EndOutputPortWave(
      ProcNodeInfo->ProcessingNodeId,
      vStreamProcConf_vStreamProcOutputPort_vStreamProc_Merge_OutputData,
      &waveInfo
    );

    if(outputWaveResult == VSTREAMPROC_OK)
    {
      /* #60 Confirm the signal. */
      signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;

      /* #70 Reset the workspace. */
      workspace->CurrentActiveInputPort = 0u;

      /* #80 Block the DataAvailable handler of the second port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
                                                  1u,
                                                  vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
                                                  VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

      /* #90 Block the WaveEnd handler of the second port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
                                                  1u,
                                                  vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
                                                  VSTREAMPROC_SIGNALHANDLERSTATE_BLOCKED);

      /* #100 Register the WaveEnd handler of the first port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
                                                  0u,
                                                  vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
                                                  VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
    }
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Merge_SignalHandler_Resume
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Merge_SignalHandler_Resume(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType signalResponse        = VSTREAMPROC_SIGNALRESPONSE_FAILED;

  /* ----- Implementation ----------------------------------------------- */
  if (ProcNodeInfo->SignalInfo.SignalId == vStreamProcConf_vStreamProcSignal_vStreamProc_Resume)
  {
    P2CONST(vStreamProc_Merge_WorkspaceType, AUTOMATIC, VSTREAMPROC_APPL_VAR) workspace =
      vStreamProc_GetTypedWorkspaceOfProcessingNode_vStreamProc_Merge_WorkspaceType(ProcNodeInfo);                      /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

    /* #10 Register the DataAvailable signal handler of the currently active port. */
    (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
      workspace->CurrentActiveInputPort,
      vStreamProcConf_vStreamProcSignal_vStreamProc_DataAvailable,
      VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);

    if (workspace->CurrentActiveInputPort == 1u)
    {
      /* #20 Register the WaveEnd signal handler of the currently active port. */
      (void)vStreamProc_SetInputPortSignalHandlerState(ProcNodeInfo->ProcessingNodeId,
        workspace->CurrentActiveInputPort,
        vStreamProcConf_vStreamProcSignal_vStreamProc_WaveEnd,
        VSTREAMPROC_SIGNALHANDLERSTATE_REGISTERED);
    }

    /* #30 Continue with next signal handler. */
    signalResponse = VSTREAMPROC_SIGNALRESPONSE_ACKNOWLEDGE;
  }

  return signalResponse;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_MERGE_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_Merge.c
 *********************************************************************************************************************/
