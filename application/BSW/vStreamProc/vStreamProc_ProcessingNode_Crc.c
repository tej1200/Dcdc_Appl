/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_ProcessingNode_Crc.c
 *        \brief  vStreamProc CRC Sub Module Source Code File
 *      \details  Implementation of the CRC sub module for the vStreamProc framework
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#define VSTREAMPROC_PROCESSINGNODE_CRC_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_ProcessingNode_Crc.h"
#include "vStreamProc_ProcessingNode.h"
#include "vStreamProc.h"

#if (VSTREAMPROC_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

#if (VSTREAMPROC_PROCESSINGNODE_CRC_CONFIG == STD_ON)
# include "Crc.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
# if !defined (VSTREAMPROC_LOCAL)
#  define VSTREAMPROC_LOCAL static
# endif

# if !defined (VSTREAMPROC_LOCAL_INLINE)
#  define VSTREAMPROC_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_InitCrc()
 *********************************************************************************************************************/
/*! \brief        Initializes the workspace of the passed node.
 *  \details      -
 *  \param[in]    CfgPtr          Pointer to the config of the processing node.
 *  \param[out]   CrcValue        Pointer to the CRC value/workspace of the processing node.
 *  \pre          -
 *  \context      TASK
 *  \reentrant    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_InitCrc(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr,
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) CrcValue);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 *  \brief          Checks if the processing length is limited by the progress output port.
 *  \details        -
 *  \param[in]      ProcNodeInfo        The processing node information to operate on.
 *  \param[in,out]  ProcessingLength    IN:  The available input length.
 *                                      OUT: The resulting processing length.
 *  \return         VSTREAMPROC_OK      No internal errors occurred.
 *  \return         VSTREAMPROC_FAILED  Internal error occurred.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_CalculateCrc()
 *********************************************************************************************************************/
/*! \brief        Calculate CRC with the passed workspace of the passed node.
 *  \details      -
 *  \param[in]    CfgPtr          Pointer to the config of the processing node.
 *  \param[in,out] CrcValue       Pointer to the CRC value/workspace of the processing node.
 *  \param[in]    Buffer          Pointer to the Buffer of the input data.
 *  \param[in]    BufferLen       Length of the input data.
 *  \pre          -
 *  \context      TASK
 *  \reentrant    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_CalculateCrc(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr,
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) CrcValue,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) Buffer,
  CONST(uint32, AUTOMATIC) BufferLen);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_GetCrcSize
 *********************************************************************************************************************/
/*! \brief        Get the CRC size in bytes based on the passed workspace.
 *  \details      -
 *  \param[in]    CfgPtr          Pointer to the config of the processing node.
*   \return       The size of the CRC
 *  \pre          -
 *  \context      TASK
 *  \reentrant    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_GetCrcSize(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr);

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 *  \brief          Updates the progress output port with the processed data length if connected.
 *  \details        -
 *  \param[in]      ProcNodeInfo     The processing node information to operate on.
 *  \param[in]      ProcessedLength  The processed data length.
 *  \return         VSTREAMPROC_OK      The update was successful or the progress port is not connected.
 *  \return         VSTREAMPROC_FAILED  The update was not successful.
 *
 *  \pre            -
 *  \context        TASK|ISR
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_InitCrc
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_InitCrc(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr,
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) CrcValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the workspace based on the configured CRC type */
  switch (CfgPtr->CrcTypeOfProcessingNode_Crc_Config)
  {
    case VSTREAMPROC_CRC8_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC8(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    case VSTREAMPROC_CRC8H2F_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC8H2F(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    case VSTREAMPROC_CRC16_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC16(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    case VSTREAMPROC_CRC32_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC32(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    case VSTREAMPROC_CRC32P4_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC32P4(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    case VSTREAMPROC_CRC64_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC64(NULL_PTR, 0u, 0u, TRUE);
      break;
    }
    default:
    {
      /* Intentionally left empty */
      break;
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_CheckForInputLimitation
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_CheckForInputLimitation(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  P2VAR(vStreamProc_LengthType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcessingLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;
  vStreamProc_LengthType limitedLength = *ProcessingLength;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_Crc_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    /* #20 Update the progress. */
    retVal = vStreamProc_GetOutputPortInfo(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      &progressOutputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      if (progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength < limitedLength)
      {
        limitedLength = progressOutputPortInfo.WriteRequest.StorageInfo.AvailableLength;
      }
    }

  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  *ProcessingLength = limitedLength;

  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_CalculateCrc
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(void, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_CalculateCrc(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr,
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) CrcValue,
  P2CONST(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) Buffer,
  CONST(uint32, AUTOMATIC) BufferLen)
{
  /* ----- Local Variables ---------------------------------------------- */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Calculate the CRC value based on the configured CRC type */
  switch (CfgPtr->CrcTypeOfProcessingNode_Crc_Config)
  {
    case VSTREAMPROC_CRC8_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC8(Buffer, BufferLen,
          (uint8)(*CrcValue & 0x00000000000000FFuLL), FALSE);
      break;
    }
    case VSTREAMPROC_CRC8H2F_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC8H2F(Buffer, BufferLen,
          (uint8)(*CrcValue & 0x00000000000000FFuLL), FALSE);
      break;
    }
    case VSTREAMPROC_CRC16_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC16(Buffer, BufferLen,
          (uint16)(*CrcValue & 0x000000000000FFFFuLL), FALSE);
      break;
    }
    case VSTREAMPROC_CRC32_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC32(Buffer, BufferLen,
          (uint32)(*CrcValue & 0x00000000FFFFFFFFuLL), FALSE);
      break;
    }
    case VSTREAMPROC_CRC32P4_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC32P4(Buffer, BufferLen,
          (uint32)(*CrcValue & 0x00000000FFFFFFFFuLL), FALSE);
      break;
    }
    case VSTREAMPROC_CRC64_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      *CrcValue = Crc_CalculateCRC64(Buffer, BufferLen, *CrcValue, FALSE);
      break;
    }
    default:
    {
      /* Intentionally left empty */
      break;
    }
  }
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_GetCrcSize
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_LengthType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_GetCrcSize(
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) CfgPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_LengthType crcSize;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the CRC size based on the configured CRC type */
  switch (CfgPtr->CrcTypeOfProcessingNode_Crc_Config)
  {
    case VSTREAMPROC_CRC8_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 1u;
      break;
    }
    case VSTREAMPROC_CRC8H2F_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 1u;
      break;
    }
    case VSTREAMPROC_CRC16_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 2u;
      break;
    }
    case VSTREAMPROC_CRC32_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 4u;
      break;
    }
    case VSTREAMPROC_CRC32P4_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 4u;
      break;
    }
    case VSTREAMPROC_CRC64_CRCTYPEOFPROCESSINGNODE_CRC_CONFIG:
    {
      crcSize = 8u;
      break;
    }
    default:
    {
      crcSize = 0u;
      break;
    }
  }

  return crcSize;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_WriteProgressOutputPort
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
VSTREAMPROC_LOCAL FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_WriteProgressOutputPort(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo,
  vStreamProc_LengthType ProcessedLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_ReturnType retVal = VSTREAMPROC_FAILED;                                                                   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vStreamProc_OutputPortInfoType progressOutputPortInfo;

  /* ----- Implementation ----------------------------------------------- */
  (void)vStreamProc_PrepareOutputPortInfo(
    ProcNodeInfo,
    vStreamProcConf_vStreamProcOutputPort_vStreamProc_Crc_Progress,
    &progressOutputPortInfo);

  /* #10 Check if the Progress output port is connected. */
  if (progressOutputPortInfo.IsConnected == TRUE)
  {
    /* #20 Update the progress. */
    retVal = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      VSTREAMPROC_NO_DATATYPE,
      ProcessedLength,
      &progressOutputPortInfo);

    if (retVal == VSTREAMPROC_OK)
    {
      retVal = vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, ProcessedLength, TRUE, &progressOutputPortInfo);
    }
  }
  /* #30 Otherwise, just return OK. */
  else
  {
    retVal = VSTREAMPROC_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_Init(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_OK;

  /* ----- Implementation ----------------------------------------------- */
  VSTREAMPROC_DUMMY_STATEMENT(ProcNodeInfo);

  /* #10 Simply return E_OK. */
  return retVal;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_SignalHandler_StartWave
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_SignalHandler_StartWave(
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) crcValue;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace. */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Crc_Config(ProcNodeInfo);                              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  crcValue = vStreamProc_GetTypedWorkspaceOfProcessingNode_Crc_ValueType(ProcNodeInfo);                                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Initialize the associated workspace. */
  vStreamProc_ProcessingNode_Crc_InitCrc(specializedCfgPtr, crcValue);

  return VSTREAMPROC_SIGNALRESPONSE_CONFIRM;

}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_SignalHandler_UpdateCrc
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_SignalHandler_UpdateCrc(          /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse  = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult   = VSTREAMPROC_FAILED;
  vStreamProc_LengthType          processingLength = 0u;
  vStreamProc_InputPortInfoType   inputPortInfo;

  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2VAR(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) crcValue;

  /* ----- Implementation ----------------------------------------------- */
  /* Infer the processing node's configuration and workspace */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Crc_Config(ProcNodeInfo);                              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  crcValue = vStreamProc_GetTypedWorkspaceOfProcessingNode_Crc_ValueType(ProcNodeInfo);                                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* #10 Try to fetch input data and forward it to the CRC. */
  /* Issue a read request and if data is available then handle it. */
  if (vStreamProc_PrepareInputPortInfo(
        ProcNodeInfo,
        vStreamProcConf_vStreamProcInputPort_vStreamProc_Crc_InputData,
        &inputPortInfo) == VSTREAMPROC_OK)
  {
    currentResult = vStreamProc_RequestInputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      1u,
      &inputPortInfo);
  }

  /* #20 Check if the processing length has to be limited. */
  if (currentResult == VSTREAMPROC_OK)
  {
    processingLength = inputPortInfo.ReadRequest.StorageInfo.AvailableLength;
    currentResult = vStreamProc_ProcessingNode_Crc_CheckForInputLimitation(ProcNodeInfo, &processingLength);
  }

  if (currentResult == VSTREAMPROC_OK)
  {
    /* #30 Calculate the CRC and acknowledge the consumed data. */
    vStreamProc_ProcessingNode_Crc_CalculateCrc(
      specializedCfgPtr,
      crcValue,
      vStreamProc_GetTypedReadRequestBuffer_uint8(&inputPortInfo.ReadRequest),
      processingLength);

  /* #40 Update signal response */
    vStreamProc_SetSignalResponseFlag(signalResponse, VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE);

    currentResult = vStreamProc_AcknowledgeInputPort(ProcNodeInfo, processingLength, TRUE, &inputPortInfo);
  }

  /* #50 Write Progress output port. */
  if (currentResult == VSTREAMPROC_OK)
  {
    currentResult = vStreamProc_ProcessingNode_Crc_WriteProgressOutputPort(ProcNodeInfo, processingLength);
  }

  /* #60 Check overall result and set signal response accordingly */
  if (currentResult != VSTREAMPROC_FAILED)
  {
    vStreamProc_SetSignalResponseFlag(
      signalResponse,
      VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK | VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

/**********************************************************************************************************************
 *  vStreamProc_ProcessingNode_Crc_SignalHandler_WriteCrc
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(vStreamProc_SignalResponseType, VSTREAMPROC_CODE) vStreamProc_ProcessingNode_Crc_SignalHandler_WriteCrc(           /* PRQA S 6050 */ /* MD_MSR_STCAL */
  P2CONST(vStreamProc_ProcessingNodeInfoType, AUTOMATIC, VSTREAMPROC_APPL_DATA) ProcNodeInfo)
{
  /* ----- Local Variables ---------------------------------------------- */
  vStreamProc_SignalResponseType  signalResponse = VSTREAMPROC_SIGNALRESPONSE_FAILED;
  vStreamProc_ReturnType          currentResult = VSTREAMPROC_FAILED;
  vStreamProc_OutputPortInfoType  outputPortInfo;
  vStreamProc_LengthType          crcSize;

  P2CONST(vStreamProc_ProcessingNode_Crc_ConfigType, AUTOMATIC, VSTREAMPROC_APPL_CONST) specializedCfgPtr;
  P2CONST(vStreamProc_Crc_ValueType, AUTOMATIC, VSTREAMPROC_APPL_VAR) crcValue;

  /* ----- Implementation ----------------------------------------------- */
  /* Get the processing node's configuration and workspace */
  specializedCfgPtr = vStreamProc_GetTypedConfigOfProcessingNode_Crc_Config(ProcNodeInfo);                              /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */
  crcValue = vStreamProc_GetTypedWorkspaceOfProcessingNode_Crc_ValueType(ProcNodeInfo);                                 /* PRQA S 0316 */ /* MD_vStreamProc_Rule11.4_0316_CastPtrVoidPtrObj */

  /* Get the CRC's size in bytes */
  /* #10 Get the size of the CRC value in bytes. */
  crcSize = vStreamProc_ProcessingNode_Crc_GetCrcSize(specializedCfgPtr);


  /* #20 Request the output buffer. */
  if (vStreamProc_PrepareOutputPortInfo(
        ProcNodeInfo,
        vStreamProcConf_vStreamProcOutputPort_vStreamProc_Crc_OutputData,
        &outputPortInfo) == VSTREAMPROC_OK)
  {
    currentResult = vStreamProc_RequestOutputPortData(
      ProcNodeInfo,
      vStreamProcConf_vStreamProcDataType_uint8,
      crcSize,
      &outputPortInfo);
  }

  /* #30 Write the CRC value to the output port. */
  if (currentResult == VSTREAMPROC_OK)
  {
    P2VAR(uint8, AUTOMATIC, VSTREAMPROC_APPL_VAR) outputBuffer =
      vStreamProc_GetTypedWriteRequestBuffer_uint8(&outputPortInfo.WriteRequest);
    vStreamProc_StorageNodeBufferIterType index;
    uint64 crcOutputValue = *crcValue;

    /* Write CRC value to the output buffer  */
    for (index = 0; index < crcSize; index++)
    {
      /* Store CRC in little endian format (LSB first) */
      outputBuffer[index] = (uint8)(crcOutputValue & 0x00000000000000FFuLL);
      crcOutputValue >>= 8u;
    }

  /* #40 Update signal response */
    vStreamProc_SetSignalResponseFlag(
      signalResponse,
      VSTREAMPROC_SIGNALRESPONSE_FLAG_WORKDONE | VSTREAMPROC_SIGNALRESPONSE_FLAG_ACK);

    /* Acknowledge the write */
    currentResult = vStreamProc_AcknowledgeOutputPort(ProcNodeInfo, crcSize, TRUE, &outputPortInfo);
  }

  /* #50 Check overall result and set signal response accordingly */
  if (currentResult != VSTREAMPROC_FAILED)
  {
    vStreamProc_SetSignalResponseFlag(
      signalResponse,
      VSTREAMPROC_SIGNALRESPONSE_FLAG_SUCCESS);
  }

  return signalResponse;
}

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSTREAMPROC_PROCESSINGNODE_CRC_CONFIG == STD_ON) */

/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:
 */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_ProcessingNode_Crc.c
 *********************************************************************************************************************/
