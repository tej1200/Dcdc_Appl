/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vStreamProc_Limit.h
 *        \brief  vStreamProc header file for limit handling.
 *
 *      \details  Declaration of the limit related functionality.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file vStreamProc.h.
 *********************************************************************************************************************/
#ifndef VSTREAMPROC_LIMIT_H
#define VSTREAMPROC_LIMIT_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vStreamProc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef enum
{
  VSTREAMPROC_LIMIT_RESULT_NONE,
  VSTREAMPROC_LIMIT_RESULT_REACHED,
  VSTREAMPROC_LIMIT_RESULT_EXCEEDED,
  VSTREAMPROC_LIMIT_RESULT_TRUNCATED
} vStreamProc_LimitResultType;

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VSTREAMPROC_START_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vStreamProc_Limit_InitStreamLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Initialize all stream limits referenced by exit points.
 * \details       -
 * \param[in]     PipeId                  Id of the desired pipe.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, VSTREAMPROC_CODE) vStreamProc_Limit_InitStreamLimit(
  vStreamProc_PipeIdType PipeId);

/**********************************************************************************************************************
 *  vStreamProc_Limit_SetStreamLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Set limit of a stream.
 * \details       -
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \param[in]     LimitInfo               Description of the new limit.
 *                - Position              Absolute start position of the limit.
 *                                        Ignored for type VSTREAMPROC_LIMIT_PROMPT.
 *                - Offset                Relative offset of the effective limit position.
 *                                        Ignored for type VSTREAMPROC_LIMIT_PROMPT.
 *                - Type                  Type of limit.
 * \return        VSTREAMPROC_FAILED      Limit could not be set.
 * \return        VSTREAMPROC_OK          Limit was set successfully.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_SetStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_LimitInfoConstPtrType LimitInfo);

/**********************************************************************************************************************
 *  vStreamProc_Limit_ClearStreamLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Disable a stream limit.
 * \details       Will only have an effect on the effective limit of the stream, if the requested stream limit is
 *                currently the active one.
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \return        VSTREAMPROC_FAILED      Limit could not be cleared.
 * \return        VSTREAMPROC_OK          Limit was cleared successfully.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_ClearStreamLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId);

/**********************************************************************************************************************
 *  vStreamProc_Limit_GetStatus()
 *********************************************************************************************************************/
/*!
 * \brief         Queries the current status of a limit signal.
 * \details       -
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \return        VSTREAMPROC_OK          Limit was processed successfully or isn't active.
 * \return        VSTREAMPROC_PENDING     Limit processing is not yet finished or hasn't yet started.
 * \return        VSTREAMPROC_FAILED      Limit signal processing failed.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_GetStatus(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StreamLimitIdType StreamLimitId);

/**********************************************************************************************************************
 *  vStreamProc_Limit_RegisterCallback()
 *********************************************************************************************************************/
/*!
 * \brief         Register a callback function for the limit.
 * \details       -
 * \param[in]     StreamLimitId           Index of the stream limit structure.
 * \param[in]     LimitCbk                Pointer to the callback function.
 * \param[in]     CallbackHandle          This parameter will be forwarded to callback function.
 * \return        VSTREAMPROC_OK          Limit callback was registered successfully.
 * \return        VSTREAMPROC_FAILED      Limit callback could not be registered.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, VSTREAMPROC_CODE) vStreamProc_Limit_RegisterCallback(
  vStreamProc_StreamLimitIdType StreamLimitId,
  vStreamProc_SignalCallbackType LimitCbk,
  vStreamProc_CallbackHandleType CallbackHandle);

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateRequestLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluate the effect of a potential stream limit on a write request.
 * \details       The length is either limited by an active limit, or truncated to the maximum stream length.
 *                Triggers registered hard limit when request exceeds limit (available length == 0 and requested length
 *                > 0 or write buffer requested).
 * \param[in]     StreamId                          ID of the stream.
 * \param[in,out] StorageInfo                       Pointer to storage information structure.
 *                - Input
 *                  - RequestLength                 Requested minimum length.
 *                - Output
 *                  - AvailableLength               Available length.
 * \param[in]     WriteBufferRequested              Flag indicating, if write buffer is requested or just information.
 * \return        VSTREAMPROC_OK                    Requested minimum length is available.
 * \return        VSTREAMPROC_INSUFFICIENT_OUTPUT   Requested minimum length isn't available.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Limit_EvaluateRequestLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StorageInfoPtrType StorageInfo,
  boolean WriteBufferRequested);

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateAcknowledgeLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluate the effect of a potential stream limit on a write acknowledge.
 * \details       Detailed result to be forwarded to TriggerAcknowledgeLimit().
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     StorageInfo             Pointer to storage information structure.
 *                - Input
 *                  - RequestLength       The amount of produced data which needs to be acknowledged.
 * \param[out]    LimitResult             Detailed result of the limit evaluation.
 *                                          To be forwarded to TriggerAcknowledgeLimit()
 *                  VSTREAMPROC_LIMIT_RESULT_NONE       Stream doesn't exceed the limit.
 *                  VSTREAMPROC_LIMIT_RESULT_TRUNCATED  Stream doesn't exceed the limit, but length was truncated
 *                                                        to the maximum stream length.
 *                  VSTREAMPROC_LIMIT_RESULT_REACHED    Stream reached the limit.
 *                  VSTREAMPROC_LIMIT_RESULT_EXCEEDED   Stream exceeds the limit.
 * \return        VSTREAMPROC_OK          Requested length doesn't exceed available length.
 * \return        VSTREAMPROC_FAILED      Requested length exceeds available length.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(vStreamProc_ReturnType, AUTOMATIC) vStreamProc_Limit_EvaluateAcknowledgeLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_StorageInfoConstPtrType StorageInfo,
  P2VAR(vStreamProc_LimitResultType, AUTOMATIC, VSTREAMPROC_APPL_DATA) LimitResult);

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateAcknowledgeLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Triggers registered soft limit if reached or exceeded during previous evaluation.
 * \details       Pass detailed result returned by EvaluateAcknowledgeLimit().
 * \param[in]     StreamId                ID of the stream.
 * \param[in]     LimitResult             Detailed result of the previous limit evaluation.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Limit_TriggerAcknowledgeLimit(
  vStreamProc_StreamIdType StreamId,
  vStreamProc_LimitResultType LimitResult);

/**********************************************************************************************************************
 *  vStreamProc_Limit_EvaluateWaveStartLimit()
 *********************************************************************************************************************/
/*!
 * \brief         Evaluate the effect of a potential stream limit on a wave start.
 * \details       Triggers registered soft limit signal if stream start reached or exceeded limit (wave start position
 *                on or beyond effective limit position).
 * \param[in]     StreamId                ID of the stream.
 * \pre           -
 * \context       TASK|ISR
 * \reentrant     TRUE
 * \synchronous   TRUE
 *********************************************************************************************************************/
FUNC(void, AUTOMATIC) vStreamProc_Limit_EvaluateWaveStartLimit(
  vStreamProc_StreamIdType StreamId);

# define VSTREAMPROC_STOP_SEC_CODE
# include "MemMap.h"                                                                                                    /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VSTREAMPROC_LIMIT_H */

/**********************************************************************************************************************
 *  END OF FILE: vStreamProc_Limit.h
 *********************************************************************************************************************/
