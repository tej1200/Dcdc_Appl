/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  VX1000If_TimingHooks.h
 *        \brief  VX1000 Timing Hooks header file
 *
 *      \details  OS and Rte Vector Timing Hooks implementation file for timing measurements with VX1000.
 *
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#if !defined(VX1000IF_TIMING_HOOKS_H)
# define VX1000IF_TIMING_HOOKS_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"
# include "VX1000If_Trace.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
# define VX_OS_TASK_ACTIVATE            64u
# define VX_OS_TASK_SETEVENT            65u
# define VX_OS_TASK_RELEASE             66u
# define VX_OS_TASK_NOWAIT              67u
# define VX_OS_TASK_ACTIVATION_LIMIT    68u
# define VX_OS_ENABLEDINT               69u
# define VX_OS_DISABLEDINT              70u
# define VX_OS_SPINLOCK_GOT             71u
# define VX_OS_SPINLOCK_REQ             72u
# define VX_OS_SPINLOCK_REL             73u

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/
 
# define VX1000IF_START_SEC_VAR_NOINIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! VX1000If_Os_Trace endcoding:
 *
 *  00 00 00 00
 *  -- -- -- --
 *   |  |  |  |
 *   |  |  |  \ ToThreadId
 *   |  |  \ ToReasonId
 *   |  \ FromThreadId
 *   \ FromReasonId
 *
 **********************************************************************************************************************/
/* volatile VAR(uint32, VX1000IF_VAR_NOINIT) VX1000If_Os_Trace[VX1000IF_TRACE_NUM_CORES] declared by VX1000If_Trace.h */

/*! VX1000If_Rte_Trace endcoding:
 *
 *  00 00 00 00
 *  -- -- -- --
 *   |  |  |  |
 *   |  |  |  \ CoreId
 *   |  |  \ EventMask 
 *   |  \  EventMask 
 *   \ EventMask
 *
 **********************************************************************************************************************/
extern volatile VAR(uint32, VX1000IF_VAR_NOINIT) VX1000If_Rte_Trace[VX1000IF_TRACE_NUM_CORES];

#define VX1000IF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  OS_VTH_SCHEDULE()
 *********************************************************************************************************************/
/*! \brief          Trace thread-switches, where thread stands for task or ISR
 *  \details        This hook routine allows external tools to trace all context switches from task to ISR and back as
 *                  well as between tasks and between ISRs. So external tools may visualize the information or measure
 *                  the execution time of tasks and ISRs.
 *                  Mind that measured execution time values may not reflect the worst case, which would be necessary
 *                  for schedulability analysis.
 *
 *                  Callers:
 *                    - Os_TraceThreadSwitch()
 *                    - Os_TraceThreadResetAndResume()
 *                    - Os_TraceThreadCleanupAndResume()
 *                    - Os_TraceThreadSuspendAndStart()
 *
 *  \param[in]      FromThreadId     The ID of the thread which has run until the switch
 *  \param[in]      FromThreadReason Represents the reason why the thread is no longer running:
 *                                   - \ref OS_VTHP_TASK_TERMINATION    1
 *                                   - \ref OS_VTHP_ISR_END             2
 *                                   - \ref OS_VTHP_TASK_WAITEVENT      4
 *                                   - \ref OS_VTHP_TASK_WAITSEMA       8
 *                                   - \ref OS_VTHP_THREAD_PREEMPT      16
 *  \param[in]      ToThreadId       The ID of the thread which will run from now on
 *  \param[in]      ToThreadReason   Represents the reason why the thread runs from now on:
 *                                   - \ref OS_VTHP_TASK_ACTIVATION     1
 *                                   - \ref OS_VTHP_ISR_START           2
 *                                   - \ref OS_VTHP_TASK_SETEVENT       4
 *                                   - \ref OS_VTHP_TASK_GOTSEMA        8
 *                                   - \ref OS_VTHP_THREAD_RESUME       16
 *                                   - \ref OS_VTHP_THREAD_CLEANUP      32
 *  \param[in]      CallerCoreId     The ID of the core where the thread switch occurs
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called. 
 *  \trace          CREQ-278562
 *********************************************************************************************************************/
# define OS_VTH_SCHEDULE(FromThreadId, FromThreadReason, ToThreadId, ToThreadReason, CallerCoreId)                                                              \
{                                                                                                                                                               \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                                                                                                 \
      VX1000If_Os_Trace[CallerCoreId] = (((uint8)FromThreadId)<<24u) | (((uint8)FromThreadReason)<<16u) | (((uint8)ToThreadId)<<8u) | ((uint8)ToThreadReason);  \
      VX1000If_Os_Event(CallerCoreId);                                                                                                                          \
    } else VX1000If_ErrorCount++;                                                                                                                               \
}

/**********************************************************************************************************************
 *  OS_VTH_ACTIVATION()
 *********************************************************************************************************************/
/*! \brief          Trace the activation of a task.
 *  \details        This hook is called on the caller core when that core has successfully performed the activation of
 *                  TaskId on the destination core. As this OS implementation always performs task activation on the
 *                  destination core, DestCoreId and CallerCoreId are always identical.
 *
 *                  Callers:
 *                    - Os_TraceTaskActivate()
 *
 *  \param[in]      TaskId       The ID of the task which is activated
 *  \param[in]      DestCoreId   The ID of the core where the task will be executed
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called. 
 *  \trace          CREQ-278563
 *********************************************************************************************************************/
# define OS_VTH_ACTIVATION(TaskId, DestCoreId, CallerCoreId)                                            \
{                                                                                                       \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                                         \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)TaskId<<8u) | (VX_OS_TASK_ACTIVATE);                   \
      VX1000If_Os_Event(CallerCoreId);                                                                  \
    } else VX1000If_ErrorCount++;                                                                       \
}

/**********************************************************************************************************************
 *  OS_VTH_SETEVENT()
 *********************************************************************************************************************/
/*! \brief          Trace the event setting on a task.
 *  \details        This hook is called on the CallerCore when that core has successfully performed the event
 *                  setting on the destination core. As this OS implementation always performs event setting on the
 *                  destination core, DestCoreId and CallerCoreId are always identical.
 *
 *                  Callers:
 *                    - Os_TraceTaskSetEvent()
 *
 *  \param[in]      TaskId       The ID of the task which receives this event
 *  \param[in]      EventMask    A bit mask with the events which have been set
 *  \param[in]      StateChanged
 *                   - !0: The task state has changed from WAITING to READY
 *                   -  0: The task state has not changed
 *  \param[in]      DestCoreId   The ID of the core where the task will be executed
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called. 
 *  \trace          CREQ-278564
 *********************************************************************************************************************/
# define OS_VTH_SETEVENT(TaskId, EventMask, StateChanged, DestCoreId, CallerCoreId)         \
{                                                                                           \
    if(VX1000If_IsVX1000DriverAccessEnabled) {                                              \
        if((boolean)StateChanged != FALSE)                                                  \
        {                                                                                   \
            VX1000If_Os_Trace[CallerCoreId] = ((uint32)TaskId<<8u) | (VX_OS_TASK_RELEASE);  \
            VX1000If_Os_Event(CallerCoreId);                                                \
        }                                                                                   \
    } else VX1000If_ErrorCount++;                                                           \
}

/***********************************************************************************************************************
 *  OS_VTH_WAITEVENT_NOWAIT()
 **********************************************************************************************************************/
/*! \brief          Trace the event waiting on a task.
 *  \details        This hook is called on the CallerCore when that core has successfully performed the wait event 
 *                  on the destination core but the event is already set.
 *                  As the OS implementation always performs wait for event on the destination core, DestCoreId
 *                  and CallerCoreId are always identical.
 *
 *                  Callers:
 *                    - Os_TraceTaskWaitEventNoWait()
 *
 *  \param[in]      TaskId       The ID of the task which receives this event
 *  \param[in]      EventMask    A bit mask of the events for which the task is waiting for
 *  \param[in]      DestCoreId   The ID of the core where the task will be executed
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279707
 **********************************************************************************************************************/
# define OS_VTH_WAITEVENT_NOWAIT(TaskId, EventMask, DestCoreId, CallerCoreId)            \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)TaskId<<8u) | (VX_OS_TASK_NOWAIT);      \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_ACTIVATION_LIMIT()
 **********************************************************************************************************************/
/*! \brief          Trace the activation of a task.
 *  \details        This hook is called on the caller core when that core has failed to perform the activation of
 *                  TaskId on the destination core because the number of activations has reached the limit.
 *                  As the OS implementation always performs task activation on the destination core, DestCoreId
 *                  and CallerCoreId are always identical.
 *
 *                  Callers:
 *                    - Os_TraceTaskActivateLimit()
 *
 *  \param[in]      TaskId       The ID of the task which is activated
 *  \param[in]      DestCoreId   The ID of the core where the task will be executed
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279708
 **********************************************************************************************************************/
# define OS_VTH_ACTIVATION_LIMIT(TaskId, DestCoreId, CallerCoreId)                            \
{                                                                                             \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                               \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)TaskId<<8u) | (VX_OS_TASK_ACTIVATION_LIMIT); \
      VX1000If_Os_Event(CallerCoreId);                                                        \
    } else VX1000If_ErrorCount++;                                                             \
}

/***********************************************************************************************************************
 *  OS_VTH_ENABLEDINT()
 **********************************************************************************************************************/
/*! \brief          Trace interrupt locks (enable).
 *  \details        The OS calls this hook if the application has called an API function to enable interrupts.
 *                  Mind that the two types of interrupt locking (as described by the IntLockId) are independent from
 *                  each other so that interrupts may still be disabled by means of the other locking type after this
 *                  hook has returned.
 *
 *                  Callers:
 *                    - Os_TraceInterruptsGlobalEnabled()
 *                    - Os_TraceInterruptsLevelEnabled()
 *
 *  \param[in]      IntLockId:
 *                    - \ref OS_VTHP_CAT2INTERRUPTS
 *                    - \ref OS_VTHP_ALLINTERRUPTS
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279709
 **********************************************************************************************************************/
# define OS_VTH_ENABLEDINT(IntLockId, CallerCoreId)                                      \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)IntLockId<<8u) | (VX_OS_ENABLEDINT);    \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_DISABLEDINT()
 **********************************************************************************************************************/
/*! \brief          Trace interrupt locks (disable).
 *  \details        The OS calls this hook if the application has called an API function to disable interrupts. The
 *                  parameter IntLockId describes whether category 1 interrupts may still occur.
 *                  Mind that the two types of interrupt locking (as described by the IntLockId) are independent from
 *                  each other so that the hook may be called twice before the hook OS_VTH_ENABLEDINT is called,
 *                  dependent on the application.
 *
 *                  Callers:
 *                    - Os_TraceInterruptsGlobalDisabled()
 *                    - Os_TraceInterruptsLevelDisabled()
 *
 *  \param[in]      IntLockId:
 *                    - \ref OS_VTHP_CAT2INTERRUPTS
 *                    - \ref OS_VTHP_ALLINTERRUPTS
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279710
 **********************************************************************************************************************/
# define OS_VTH_DISABLEDINT(IntLockId, CallerCoreId)                                    \
{                                                                                       \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                         \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)IntLockId<<8u) | (VX_OS_DISABLEDINT);  \
      VX1000If_Os_Event(CallerCoreId);                                                  \
    } else VX1000If_ErrorCount++;                                                       \
}

/***********************************************************************************************************************
 *  OS_VTH_REQ_SPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace attempts to request a spinlock.
 *  \details        The OS calls this hook before it enters a busy waiting loop on a spinlock. Tasks or ISRs of lower
 *                  priority have to wait until this task or ISR has taken and released the spinlock.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockRequested()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock, the caller is waiting for
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279711
 **********************************************************************************************************************/
# define OS_VTH_REQ_SPINLOCK(SpinlockId, CallerCoreId)                                   \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_REQ); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_GOT_SPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace spinlock occupations.
 *  \details        The OS calls this hook whenever a spinlock has successfully been taken. If the task or ISR was not
 *                  successful immediately (entered busy waiting state), this hook means that it leaves the busy waiting
 *                  state. From now on no other task or ISR may get the spinlock until the current task or ISR has
 *                  released it.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockTaken()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock which was taken
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279712
 **********************************************************************************************************************/
# define OS_VTH_GOT_SPINLOCK(SpinlockId, CallerCoreId)                                   \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_GOT); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_REL_SPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace spinlock releases.
 *  \details        The OS calls this hook on a release of a spinlock. Other tasks and ISR may take the spinlock now.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockReleased()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock which was released
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279713
 **********************************************************************************************************************/
# define OS_VTH_REL_SPINLOCK(SpinlockId, CallerCoreId)                                   \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_REL); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_REQ_ISPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace attempts to request an internal spinlock.
 *  \details        The OS calls this hook before it enters a busy waiting loop on a internal spinlock. Tasks or ISRs of 
 *                  lower priority have to wait until this task or ISR has taken and released the spinlock.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockRequested()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock, the caller is waiting for
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279714
 **********************************************************************************************************************/
# define OS_VTH_REQ_ISPINLOCK(SpinlockId, CallerCoreId)                                  \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_REQ); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_GOT_ISPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace internal spinlock occupations.
 *  \details        The OS calls this hook whenever a internal spinlock has successfully been taken. If the task or ISR 
 *                  was not successful immediately (entered busy waiting state), this hook means that it leaves the busy 
 *                  waiting state. From now on no other task or ISR may get the spinlock until the current task or ISR 
 *                  has released it.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockTaken()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock which was taken
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279715
 **********************************************************************************************************************/
# define OS_VTH_GOT_ISPINLOCK(SpinlockId, CallerCoreId)                                  \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_GOT); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/***********************************************************************************************************************
 *  OS_VTH_REL_ISPINLOCK()
 **********************************************************************************************************************/
/*! \brief          Trace internal spinlock releases.
 *  \details        The OS calls this hook on a release of a internal spinlock. Other tasks and ISR may take the spinlock 
 *                  now.
 *
 *                  Callers:
 *                    - Os_TraceSpinlockReleased()
 *
 *  \param[in]      SpinlockId   The ID of the spinlock which was released
 *  \param[in]      CallerCoreId The ID of the core where this hook is called
 *  \context        TASK|ISR
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called.
 *  \trace          CREQ-279716
 **********************************************************************************************************************/
# define OS_VTH_REL_ISPINLOCK(SpinlockId, CallerCoreId)                                  \
{                                                                                        \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                          \
      VX1000If_Os_Trace[CallerCoreId] = ((uint32)SpinlockId<<8u) | (VX_OS_SPINLOCK_REL); \
      VX1000If_Os_Event(CallerCoreId);                                                   \
    } else VX1000If_ErrorCount++;                                                        \
}

/**********************************************************************************************************************
 *  VX1000If_RteVfbTraceHook()
 *********************************************************************************************************************/
/*! \brief          Trace the VFB event of the Rte.
 *  \details        This hook is called on the CallerCore when that core has successfully executed the VFB event such
 *                  as RunnableStart and RunnableStop.
 *
 *  \param[in]      CallerCoreId The ID of the core where this hook is called.
 *  \param[in]      EventMask    A bit mask with the events which have been set.
 *  \context        TASK
 *  \reentrant      TRUE for different caller cores.
 *  \synchronous    TRUE
 *  \pre            VX1000If_InitAsyncStart must have been called. 
 *  \trace          CREQ-278565
 *********************************************************************************************************************/
# define VX1000If_RteVfbTraceHook(CallerCoreId, EventMask)                            \
{                                                                                     \
    if (VX1000If_IsVX1000DriverAccessEnabled) {                                       \
        VX1000If_Rte_Trace[CallerCoreId] = ((uint32)EventMask<<8u) | (CallerCoreId);  \
        VX1000If_Rte_Event(CallerCoreId);                                             \
    } else VX1000If_ErrorCount++;                                                     \
} 

#endif /* VX1000IF_TIMING_HOOKS_H */

/**********************************************************************************************************************
 *  END OF FILE: VX1000If_TimingHooks.h
 *********************************************************************************************************************/
