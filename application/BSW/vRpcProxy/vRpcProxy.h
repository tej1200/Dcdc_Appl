/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy.h
 *      Project:  vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  vRpcProxy Header File
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2019-01-29  visssg viscpe visbbk 
 *                                STORYC-7360    InitialVersion_Step1
 *            2019-02-18  visssg viscpe visbbk
 *                                STORYC-7631    InitialVersion_Step2
 *            2019-03-18  visssg viscpe visbbk
 *                                STORYC-7635    InitialVersion_Step3
 *            2019-04-15  visssg viscpe visbbk
 *                                STORYC-8106    Beta Release
 *  01.01.00  2019-05-15  viscpe visbbk
 *                                STORYC-8312    Finalize Component
 *  01.01.01  2019-06-13  visbbk  ESCAN00103415  Missing include of Lcfg.h  in vRpcProxy_Cbk.h
 *  01.02.00  2019-07-25  visssg  COM-946        Support Repeated Transmission in case of negative TP Confirmation
 *            2019-08-01  visssg  COM-947        Support Achnowledged Communication with Timeout Supervision
 *            2019-08-08  visssg  COM-948        Secure Communication via CRC Check
 *  01.02.01  2019-08-15  visbbk  COM-944        Support Repeated Transmission in case of negative TP Confirmation 
 *            2019-08-15  visssg  COM-949        Support Achnowledged Communication with Timeout Supervision
 *  01.02.02  2019-08-28  visbbk  COM-994        Support Missing Acknowledges
 *            2019-09-09  visbbk  COM-1035       [SWDL]Refactor vRpcProxy MainStateMachine
 *  01.03.00  2019-09-18  visbbk  COM-1067       [SWDL]Refactor vRpcProxy MainStateMachine
 *            2019-09-18  visbbk  COM-1075       Support Missing Acknowledges
 *            2019-09-18  visbbk  ESCAN00104377  Compiler warning: Possible loss of data
 *  01.04.00  2020-12-20  visssg  COM-1281       Update ComStacklib and Minimmize the Number of SizeRelations
 *            2020-01-10  visssg  ESCAN00105319  AcknowledgeService State is overwritten
 *            2020-01-13  visssg  ESCAN00104493  State switch of TxServices happens at wrong time
 *            2020-01-14  visssg  ESCAN00104492  Retry counter is not reloaded
 *            2020-01-14  visssg  ESCAN00104187  Compiler warning: conversion from 'uint16' to 'vRpcProxy_TxProxyServiceCollectionIterType', possible loss of data
 *            2020-01-14  visssg  COM-1107       Missing invalid handle check for TxServices
 *  02.00.00  2020-05-04  visbbk viscpe
 *                                COM-1560       Implement ResponsePending
 *            2020-06-10  visssg  COM-1512       Support Helix QAC 2019-2
 *            2020-06-24  visbbk  ESCAN00106691  Wrong preprocessor encapsulation of serialize/deserialize functions
 *  02.01.00  2020-08-10  viscpe  COM-1766       Remove Target Group Limitation
 *            2020-08-13  visbbk  COM-1807       Report response timeout through ServiceStatusCallout
 *            2020-08-20  viscpe  ESCAN00106776  Compiler error: 'vRpcProxy_ServiceManager_SerializeData_boolean' undefined
 *            2020-08-21  viscpe  ESCAN00107180  Compiler error: 'vRpcProxy_ServiceManager_DeserializeArray_boolean' undefined
 *  02.02.00  2021-06-14  visbbk  ESCAN00109382  Incompatibility SoAd - vRpcProxy
 *            2021-06-14  visbbk  ESCAN00109359  DET Error Detect Prohibits Valid Use-Case of CopyTxData
 *            2021-06-14  visbbk  ESCAN00108982  Undefined reference to VRPCPROXY_REQUEST_SERVICETYPEOFCRXPROXYSERVICECOLLECTION  | VRPCPROXY_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION
 *            2021-06-17  visbbk  COM-2604       Remove serviceType ConfigurationConsistency
 *********************************************************************************************************************/

#if !defined (VRPCPROXY_H)
# define VRPCPROXY_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vRpcProxy_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

 /* Vendor and module identification */
# define VRPCPROXY_VENDOR_ID                       (30u)              /**< the vendor ID of this implementation. */
# define VRPCPROXY_MODULE_ID                       (255u)             /**< the module ID of this implementation. */

/* AUTOSAR Software specification version information */
# define VRPCPROXY_SW_MAJOR_VERSION                (2u)
# define VRPCPROXY_SW_MINOR_VERSION                (2u)
# define VRPCPROXY_SW_PATCH_VERSION                (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VRPCPROXY_AR_RELEASE_MAJOR_VERSION        (4u)
# define VRPCPROXY_AR_RELEASE_MINOR_VERSION        (0u)
# define VRPCPROXY_AR_RELEASE_REVISION_VERSION     (3u)

# define VRPCPROXY_INSTANCE_ID_DET                 (0u)

/* ----- API service IDs ----- */
# define VRPCPROXY_APIID_INIT                      (0x00u)        /* !< Service ID: vRpcProxy_Init() */
# define VRPCPROXY_APIID_VERSIONINFO               (0x01u)        /* !< Service ID: vRpcProxy_GetVersionInfo() */
# define VRPCPROXY_APIID_TPRXINDICATION            (0x02u)        /* !< Service ID: vRpcProxy_TpRxIndication() */
# define VRPCPROXY_APIID_TPTXCONFIRMATION          (0x03u)        /* !< Service ID: vRpcProxy_TpTxConfirmation() */
# define VRPCPROXY_APIID_COPYTXDATA                (0x04u)        /* !< Service ID: vRpcProxy_CopyTxData() */
# define VRPCPROXY_APIID_COPYRXDATA                (0x05u)        /* !< Service ID: vRpcProxy_CopyRxData() */
# define VRPCPROXY_APIID_STARTOFRECEPTION          (0x06u)        /* !< Service ID: vRpcProxy_StartOfReception()) */
# define VRPCPROXY_APIID_SERVICECALL               (0x07u)        /* !< Service ID: vRpcProxy_<ServiceName> */
# define VRPCPROXY_APIID_MAINFUNCTION              (0x08u)        /* !< Service ID: vRpcProxy_MainFunction()) */      

/* ----- Error codes ----- */
# define VRPCPROXY_E_NO_ERROR                      (0x00u)        /* !< used to check if no error occurred - use a value unequal to any error code */
# define VRPCPROXY_E_PARAM                         (0x0Au)        /* !< Error code: API service called with wrong parameter */
# define VRPCPROXY_E_PARAM_POINTER                 (0x0Bu)        /* !< Error code: API service used with invalid pointer parameter (NULL) */
# define VRPCPROXY_E_PARAM_CONFIG                  (0x0Cu)        /* !< Error code: API service vRpcProxy_Init() called with wrong parameter */
# define VRPCPROXY_E_UNINIT                        (0x10u)        /* !< Error code: API service used without module initialization */
# define VRPCPROXY_E_ALREADY_INITIALIZED           (0x11u)        /* !< Error code: The service vRpcProxy_Init() is called while the module is already initialized */

# define VRPCPROXY_E_TP_ERROR                      (0x21u)        /* !< Error code: Error during Tp transmitt or receive occured. */
# define VRPCPROXY_E_SERVICE_UNKNOWN               (0x22u)        /* !< Error code: The Service is unknown to the remote proxy. */
# define VRPCPROXY_E_CONFIGURATIONS_INCONSISTENT   (0x23u)        /* !< Error code: The caluclated Configuration Consistency hash and the deserialized Configuration consistency hash are not the same */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)                                     
# define vRpcProxy_Det_ReportError(ApiId, ErrorCode)    (void)Det_ReportError(VRPCPROXY_MODULE_ID, VRPCPROXY_INSTANCE_ID_DET, (ApiId), (ErrorCode))   /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
# define vRpcProxy_ReportRuntimeDetError(ApiId, ErrorCode) ((void) Det_ReportRuntimeError( VRPCPROXY_MODULE_ID, VRPCPROXY_INSTANCE_ID_DET, (ApiId), (ErrorCode))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#endif

#if !defined (VRPCPROXY_LOCAL)
# define VRPCPROXY_LOCAL static
#endif

#if !defined (VRPCPROXY_LOCAL_INLINE)
# define VRPCPROXY_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VRPCPROXY_START_SEC_CODE
# include "MemMap.h"   /* PRQA S 5087 */       /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vRpcProxy_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Function for *_INIT_*-variable initialization.
 *  \details     This function initializes the variables in the *_INIT_* sections. 
 *               Used in case they are not initialized by the startup code.
 *  \pre         Module is uninitialized. (vRpcProxy_Init() is not called yet.)
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE     
 *********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitMemory(void);

/**********************************************************************************************************************
 * vRpcProxy_Init()
 *********************************************************************************************************************/
/*! \brief       Initialization function.
 *  \details     This function initializes the module vRpcProxy. It initializes all variables and sets the module state to
 *               initialized.
 *  \param[in]   ConfigPtr     NULL_PTR if VRPCPROXY_USE_INIT_POINTER is STD_OFF
 *                             Pointer to the vRpcProxy configuration data if VRPCPROXY_USE_INIT_POINTER is STD_ON
 *  \pre         vRpcProxy_InitMemory() has to be executed previously, if the startup code does not initialize variables.
 *  \pre         vRpcProxy is not in initialized state.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_Init(P2CONST(vRpcProxy_ConfigType, AUTOMATIC, VRPCPROXY_PBCFG) ConfigPtr);

# if (VRPCPROXY_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief       Returns the version information.
 *  \details     vRpcProxy_GetVersionInfo() returns version information, vendor ID and module ID of the component.
 *  \param[out]  VersioninfoPtr    Pointer to where to store the version information. Parameter must not be NULL.
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VRPCPROXY_APPL_VAR) VersioninfoPtr);
# endif

# define VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h"   /* PRQA S 5087 */       /* MD_MSR_MemMap */

/*!
* \exclusivearea VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE
* Ensures consistency of global RAM variables.
* \protects Global RAM variables used of Rx path.
* \usedin vRpcProxy_ErrorManager_RxTpConnectionError, vRpcProxy_ErrorManager_RemoteProxyServiceUnknown, vRpcProxy_TpManager_InitRxTp, vRpcProxy_TpManager_TpRxIndication_Processing, 
* \exclude All functions provided by vRpcProxy.
* \length SHORT to LONG depending on the API, the call graph and configured thresholds.
* \endexclusivearea
*/

/*!
* \exclusivearea VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE
* Ensures consistency of global RAM variables.
* \protects Global RAM variables used of Rx path.
* \usedin vRpcProxy_ErrorManager_TxTpConnectionError, vRpcProxy_TpManager_InitTxTp, vRpcProxy_TpManager_TpTxConfirmation_Processing
* \exclude All functions provided by vRpcProxy.
* \length SHORT to LONG depending on the API, the call graph and configured thresholds.
* \endexclusivearea
*/

#endif /* VRPCPROXY_H */

/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy.h
 *********************************************************************************************************************/
