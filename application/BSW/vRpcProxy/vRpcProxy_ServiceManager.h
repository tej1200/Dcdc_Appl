/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_ServiceManager.h
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VRPCPROXYSERVICEMANAGER_H)
# define VRPCPROXYSERVICEMANAGER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VRPCPROXY_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */
 
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_TxAcknowledgeService()
 **********************************************************************************************************************/
/*! \brief     Send Acknowledge service
 *   \details   This function is called by RxIndication and Sends an Acknowledge service for the revceived tx service
 *   \param[in]   targetHandleId  Id of the target Pdu.
 *   \param[in]   txServiceId     Id of the service to be acknowledged.
 *   \param[in]   result          The status of the acknowledged service.
 *                                0   successfull reception of servie  
 *                                1   failure on reception 
 *   \return      Std_ReturnType
 *                     E_OK: Service was processed successfully
 *                     E_NOT_OK: Processing of service was not possible
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
FUNC(Std_ReturnType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_TxAcknowledgeService(vRpcProxy_CTargetsIterType targetHandleId, uint8 result, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId);

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer()
 **********************************************************************************************************************/
/*! \brief    This function is called by vRpcProxy_ServiceManager_ProcessTxServiceCall() to copy data from the ServiceQueueBuffer to the TxTpBuffer.
 *   \details      -
 *   \param[in]    serviceId      ID of the service. 
 *   \param[in]    targetId       ID of target Pdu. 
 *   \param[in]    serviceSize    Size of the service.
 *   \context      TASK|ISR
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize);

/**********************************************************************************************************************
  vRpcProxy_ServiceManager_CheckEachTpConnectionReady
**********************************************************************************************************************/
/*! \brief    This function checks if each target of the target group is in the ready state
 *   \details      -
 *   \param[in]    targetGroupHandleId       Id of the target
 *   \return       boolean
 *                     TRUE: each tp connection is ready
 *                     FALSE: atleast one tp connection is not ready
 *   \context      ISR2
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CheckEachTpConnectionReady(vRpcProxy_TargetGroupsIterType targetGroupHandleId);

/**********************************************************************************************************************
  vRpcProxy_ServiceManager_CheckEachServiceTargetReady
**********************************************************************************************************************/
/*! \brief    This function checks if each serviceTarget is in the ready state
 *   \details      -
 *   \param[in]    serviceId       Id of the service
 *   \return       boolean
 *                     TRUE: each serviceTarget is ready
 *                     FALSE: atleast one serviceTarget is not ready 
 *   \context      ISR2
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CheckEachServiceTargetReady(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId);

/**********************************************************************************************************************
  vRpcProxy_ServiceManager_SetAllServiceTargetStatesOfTargetGroup
**********************************************************************************************************************/
/*! \brief    This function sets all serviceTargets of the targetGroup to the passed state
 *   \details      -
 *   \param[in]    targetGroupHandleId       Id of the target
 *   \param[in]    serviceId                 Id of the service
 *   \param[in]    txServiceTargetState      The state to be set
 *   \context      ISR2
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SetAllServiceTargetStatesOfTargetGroup(vRpcProxy_TargetGroupsIterType targetGroupHandleId, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_TxServiceStateOfTxServiceTargetCollectionType txServiceTargetState);

#if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
/**********************************************************************************************************************
  vRpcProxy_ServiceManager_IsFilteringOfTxResponseRequired
**********************************************************************************************************************/
/*! \brief    This function checks if the transmit of a tx response should be dropped.
 *   \details      -
 *   \param[in]    responseOfServiceId       Id of the rx request
 *   \param[in]    serviceExecutionStatus    execution status of the the rx request
 *   \return       boolean
 *                     TRUE: response should be dropped
 *                     FALSE: response should be transmitted 
 *   \context      TASK
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_IsFilteringOfTxResponseRequired(uint16 responseOfServiceId, uint8 serviceExecutionStatus);
#endif


/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_ServiceStatusCallout()
 **********************************************************************************************************************/
/*! \brief    This function calls the ServiceStatusCallout at the upper layer related to the Service
 *   \details      -
 *                 
 *   \param[in]   targetIdx      Id of the target.
 *   \param[in]   serviceId      Id of the called service.
 *   \param[in]   result         TxService_StatusType
 *                                 E_POSITIVE_ACK       successfull transmission of service with positive acknowledge received 
 *                                 E_LOCAL_TX_TP_ERROR  failure on transmission (after retries if applicable) 
 *                                 E_NEGATIVE_ACK       the received acknowledge is negative 
 *                                 E_ACK_TIMEOUT        timeout for acknowledge of tx service occured (after retries if applicable) 
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ServiceStatusCallout(vRpcProxy_CTargetsIterType targetIdx, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, TxService_StatusType result);

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SetServiceBufferWriteIdxToStartIdxOfService()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to set the ServiceBufferWriteIdx to the BufferStartIdx of the called Service. 
 *   \details     -
 *   \param[in]   serviceId      Id of the called service.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SetServiceBufferWriteIdxToStartIdxOfService(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId);

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize uint8 data. 
 *   \details     This function copies the passed uint8 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint8 Data);

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize uint8 data. 
 *   \details     This function copies the uint8 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed uint8 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);

#if ((VRPCPROXY_EXISTS_BOOLEAN_ARRAYRXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_PRIMITIVERXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_boolean()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize boolean data. 
 *   \details     This function copies the boolean data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed boolean pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_boolean(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
#endif

#if ((VRPCPROXY_EXISTS_BOOLEAN_ARRAYRXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_PRIMITIVERXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_boolean()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize boolean data. 
 *   \details     This function copies the passed boolean value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_boolean(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, boolean Data);
#endif

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize uint16 data. 
 *   \details     This function copies the passed uint16 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint16 Data);

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize uint16 data. 
 *   \details     This function copies the uint16 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed uint16 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);

# if((VRPCPROXY_EXISTS_UINT32_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT32_ARRAYTXPARAMETERTYPE == STD_ON) || (VRPCPROXY_CRC_ENABLED == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize uint32 data. 
 *   \details     This function copies the passed uint32 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint32 Data);
# endif

# if((VRPCPROXY_EXISTS_UINT32_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT32_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize uint32 data. 
 *   \details     This function copies the uint32 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed uint32 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

# if((VRPCPROXY_EXISTS_UINT64_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT64_ARRAYTXPARAMETERTYPE))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize uint64 data. 
 *   \details     This function copies the passed uint64 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint64 Data);
# endif

# if((VRPCPROXY_EXISTS_UINT64_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT64_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize uint64 data. 
 *   \details     This function copies the uint64 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed uint64 pointer.
 *   \param[in]   sourceHandleId       Id of the source Pdu.
 *   \param[out]  parameter            Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

# if((VRPCPROXY_EXISTS_SINT8_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT8_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize sint8 data. 
 *   \details     This function copies the passed sint8 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint8 Data);
# endif

# if((VRPCPROXY_EXISTS_SINT8_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT8_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize sint8 data. 
 *   \details     This function copies the sint8 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed sint8 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

# if((VRPCPROXY_EXISTS_SINT16_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT16_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize sint16 data. 
 *   \details     This function copies the passed sint16 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint16 Data);
# endif

# if((VRPCPROXY_EXISTS_SINT16_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT16_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize sint16 data. 
 *   \details     This function copies the sint16 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed sint16 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

# if((VRPCPROXY_EXISTS_SINT32_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT32_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize sint32 data. 
 *   \details     This function copies the passed sint32 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint32 Data);
# endif

# if((VRPCPROXY_EXISTS_SINT32_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT32_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize sint32 data. 
 *   \details     This function copies the sint32 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed sint32 pointer.
 *   \param[in]   sourceHandleId    Id of the source Pdu.
 *   \param[out]  parameter         Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

# if((VRPCPROXY_EXISTS_SINT64_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT64_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize sint64 data. 
 *   \details     This function copies the passed sint64 value to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   Data           The Data to serialize.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint64 Data);
# endif

# if((VRPCPROXY_EXISTS_SINT64_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT64_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize sint64 data. 
 *   \details     This function copies the sint64 data from the RxTpBuffer at the current RxTpBufferReadIdx to the passed sint64 pointer.
 *   \param[in]   sourceHandleId       Id of the source Pdu.
 *   \param[out]  parameter            Pointer to parameter that shall be filled with values.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter);
# endif

#if((VRPCPROXY_EXISTS_UINT8_ARRAYTXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an uint8 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

#if(VRPCPROXY_EXISTS_UINT8_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an uint8 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId  Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

#if(VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_boolean()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an boolean array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_boolean(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
#endif

#if(VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_boolean()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an boolean array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId  Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_boolean(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);    
#endif

# if(VRPCPROXY_EXISTS_UINT16_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an uint16 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_UINT16_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an uint16 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId     Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_UINT32_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an uint32 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_UINT32_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an uint32 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId     Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_UINT64_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an uint64 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_UINT64_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an uint64 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId  Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT8_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an sint8 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT8_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint8()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an sint8 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId     Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT16_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an sint16 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[out]  array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT16_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint16()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an sint16 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId     Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT32_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an sint32 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT32_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint32()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an sint32 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId     Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT64_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to serialize an sint64 array. 
 *   \details     This function copies the array data to the ServiceBuffer at the current ServiceBufferWriteIndex.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   array          The array to serialize.
 *   \param[in]   arrayLength    The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

# if(VRPCPROXY_EXISTS_SINT64_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint64()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to deserialize an sint64 array. 
 *   \details     This function copies the array data from the RxTpBuffer at the current RxTpBufferReadIdx to the given array.
 *   \param[in]   sourceHandleId  Id of the source Pdu.
 *   \param[out]  array           The deserialized array.
 *   \param[in]   arrayLength     The length of the array.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength);
# endif

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_ProcessTxServiceCall()
 **********************************************************************************************************************/
/*! \brief    This function is called to further process the Tx ServiceCall.
 *   \details     This function copies the Data to the according TxTpBuffer for all target in the Target Group. And calls
 *                vRpcProxy_TpManager_TransmitTpFrame() for each target.
 *   \param[in]   serviceId      Id of the Service.
 *   \param[in]   targetGroupId  Id of the Target Group.
 *   \param[in]   serviceSize    Actual size of the Service.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         Serialization was performed before
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessTxServiceCall(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_TargetGroupsIterType targetGroupId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize);

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_RxIndication_ProcessPdu()
 **********************************************************************************************************************/
/*! \brief    This function is called by vRpcProxy_TpManager_TpRxIndication_Processing to process a successfully received Pdu
 *   \details     This function processes a received Rx Pdu and extracts the serviceId of the received Service from the data.
 *                Then the according RxService Function is called.
 *   \param[in]   sourceHandleId     Id of the Source Pdu
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_RxIndication_ProcessPdu(vRpcProxy_SourcesIterType sourceHandleId);

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection()
 **********************************************************************************************************************/
/*! \brief    This function gets the index of the service target collection.
 *   \details     -
 *   \param[in]   serviceId      Id of the service
 *   \param[in]   targetHandleId Id of the target
 *   \return      index of the service target collection
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE
 *   \pre         -
**********************************************************************************************************************/
FUNC(vRpcProxy_CTxServiceTargetCollectionIterType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetHandleId);

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection()
 **********************************************************************************************************************/
/*! \brief    This function gets the index of the service source collection.
 *   \details     -
 *   \param[in]   serviceId      Id of the service
 *   \param[in]   sourceHandleId Id of the source
 *   \return      index of the service source collection
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE
 *   \pre         -
**********************************************************************************************************************/
FUNC(vRpcProxy_CRxServiceSourceCollectionIterType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection(vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_SourcesIterType sourceHandleId);


# define VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */
#endif /* VRPCPROXYSERVICEMANAGER_H */
/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_ServiceManager.h
 *********************************************************************************************************************/
