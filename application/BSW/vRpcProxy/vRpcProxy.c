/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy.c
 *      Project:  vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  vRpcProxy Source File
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/
 
#define VRPCPROXY_SOURCE

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
**********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vstdlib.h"
#include "vRpcProxy.h"
#include "vRpcProxy_ServiceManager.h"
#include "vRpcProxy_TpManager.h"
#include "vRpcProxy_Cbk.h"
#include "SchM_vRpcProxy.h"

#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

/* Check the version of vRpcProxy header file */
#if ((VRPCPROXY_SW_MAJOR_VERSION != (2u)) || (VRPCPROXY_SW_MINOR_VERSION != (2u)) || (VRPCPROXY_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of vRpcProxy.c and vRpcProxy.h are inconsistent"
#endif
/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VRPCPROXY_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 */        /*  MD_MSR_MemMap */ 
 
/**********************************************************************************************************************
 *  vRpcProxy_InitTx
 **********************************************************************************************************************/
/*! \brief     This function is called to initalize Tx related Parameters
 *   \details  -
 *   \context  TASK|ISR2
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitTx(void);

/**********************************************************************************************************************
 *  vRpcProxy_InitRx
 **********************************************************************************************************************/
/*! \brief     This function is called to initalize Rx related Parameters
 *   \details  -
 *   \context  TASK|ISR2
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitRx(void); 

/**********************************************************************************************************************
 *  vRpcProxy_ManageAckTimeouts
 **********************************************************************************************************************/
/*! \brief      Manage active timeouts
 *   \details   This function is called by the main function to decrement the timeout counters and performs 
 *              retries or calls the status callouts upon timeout expiration
 *   \param[in] serviceTargetCollectionIdx ID of the service/target combination
 *   \context   TASK
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageAckTimeouts(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx);

/**********************************************************************************************************************
 *  vRpcProxy_ManageResponseTimeouts
 **********************************************************************************************************************/
/*! \brief      Manage response timeouts
 *   \details   This function is called by the main function to decrement the response timeout counters. On timeout expiration
 *              the tx service state is reset and the response service callout is called. 
 *   \param[in] serviceTargetCollectionIdx ID of the service/target combination
 *   \context   TASK
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageResponseTimeouts(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx);

/**********************************************************************************************************************
 *  vRpcProxy_ManageRetries
 **********************************************************************************************************************/
/*! \brief      Retransmit tx services
 *   \details   This function is called by the main function to retransmit tx services that are flagged for retransmission
 *   \param[in] serviceTargetCollectionIdx ID of the service/target combination
 *   \context   TASK
 *   \pre       -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageRetries(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx);

/*******************************************************************************************
 *  vRpcProxy_ManageAckService()
 *******************************************************************************************/
/*! \brief     Transmit acknowledge service
 *   \details  This function is called by the main function to transmit an acknowledge service to a target that is flagged for transmission
 *   \context  TASK
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageAckService(void);

/**********************************************************************************************************************
 *  vRpcProxy_ManageQueuedTxServices
 **********************************************************************************************************************/
/*! \brief     Transmit queued services
 *   \details  This function is called by the main function to transmit queued tx services
 *   \context  TASK
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageQueuedTxServices(void);

#if (VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ManageResponseFilterTimeWindow
 **********************************************************************************************************************/
/*! \brief     Update Response Filter related data
 *   \details  Update the timer and reset the Response Filter flags if the timer expired.
 *   \context  TASK
 *   \pre      -
 **********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageResponseFilterTimeWindow(void);
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  vRpcProxy_InitTx
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitTx(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_CTxProxyServiceCollectionIterType serviceIdx = 0u;
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
  
   /* Iterate over all Tx services and initialize all related parameters */
  for(; serviceIdx < vRpcProxy_GetSizeOfCTxProxyServiceCollection(); serviceIdx++)
  {
    vRpcProxy_SetServiceBufferWriteIdxOfVTxProxyServiceCollection(serviceIdx, 0u);     
    vRpcProxy_SetTxServiceDeferredProcessingOfVTxProxyServiceCollection(serviceIdx, FALSE);
    
    vRpcProxy_SetTxServiceDeferredTargetGroupOfVTxProxyServiceCollection(serviceIdx, VRPCPROXY_MAX_TXSERVICEDEFERREDTARGETGROUPOFVTXPROXYSERVICECOLLECTION);
    vRpcProxy_SetTxServiceSizeOfVTxProxyServiceCollection(serviceIdx, VRPCPROXY_MAX_TXSERVICESIZEOFVTXPROXYSERVICECOLLECTION);

    
    /* Iterate over service and target collection and initialize all related parameters */
    serviceTargetCollectionIdx = vRpcProxy_GetTxServiceTargetCollectionStartIdxOfCTxProxyServiceCollection(serviceIdx);
    for(; serviceTargetCollectionIdx < vRpcProxy_GetTxServiceTargetCollectionEndIdxOfCTxProxyServiceCollection(serviceIdx); serviceTargetCollectionIdx++)
    {
      vRpcProxy_SetTimeoutCounterAckOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
      vRpcProxy_SetTxServiceCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
      vRpcProxy_SetRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx, FALSE);
      vRpcProxy_SetTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxRetryCountOfCTxProxyServiceCollection(serviceIdx));
      vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
      vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
    }
  }
}
/* vRpcProxy_InitTx */

/**********************************************************************************************************************
 *  vRpcProxy_InitRx
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitRx(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_SourcesIterType sourceIdx = 0u;
  vRpcProxy_CRxProxyServiceCollectionIterType serviceIdx = 0u;
  vRpcProxy_CRxServiceSourceCollectionIterType serviceSourceCollectionIdx;
  
  /* Iterate over all Rx services and set all related parameters */
  for(;serviceIdx < vRpcProxy_GetSizeOfCRxProxyServiceCollection(); serviceIdx++)
  {  
#if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
    vRpcProxy_SetResponseSentInCurrentTimeWindowOfVRxProxyServiceCollection(serviceIdx, FALSE);
#endif  

    vRpcProxy_SetRxServiceStateOfVRxProxyServiceCollection(serviceIdx, VRPCPROXY_READY_RXSERVICESTATEOFVRXPROXYSERVICECOLLECTION);
    vRpcProxy_SetAckFlagOfVRxProxyServiceCollection(serviceIdx, FALSE);
    vRpcProxy_SetAckResultOfVRxProxyServiceCollection(serviceIdx, 0u);
    
    
    /* Iterate over service and source collection and initialize all related parameters */
    serviceSourceCollectionIdx = vRpcProxy_GetRxServiceSourceCollectionStartIdxOfCRxProxyServiceCollection(serviceIdx);
    for(; serviceSourceCollectionIdx < vRpcProxy_GetRxServiceSourceCollectionEndIdxOfCRxProxyServiceCollection(serviceIdx); serviceSourceCollectionIdx++)
    {    
      vRpcProxy_SetRxServiceCounterOfRxServiceSourceCollection(serviceSourceCollectionIdx, 0u);
    }
    
  }  
  /* Iterate over all Rx Pdus and set the read index for the TP buffer */
  for(; sourceIdx < vRpcProxy_GetSizeOfSources(); sourceIdx++)
  {
    vRpcProxy_SetRxTpBufferReadIdxOfRxTpInfo(sourceIdx, 0u);       
  }  
}
/* vRpcProxy_InitRx */

/*******************************************************************************************
 *  vRpcProxy_ManageAckTimeouts()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageAckTimeouts(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx)
{
  vRpcProxy_CTargetsIterType targetIdx;
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceIdx;
  
  /* If ServiceState is WAITING FOR ACK the ack timer is active */
  if(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx) == VRPCPROXY_WAITING_FOR_ACK_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION)
  {  
    /* Decrement timeout counter */
    vRpcProxy_DecTimeoutCounterAckOfTxServiceTargetCollection(serviceTargetCollectionIdx);
    
    /* If timeout counter has expired*/
    if(vRpcProxy_GetTimeoutCounterAckOfTxServiceTargetCollection(serviceTargetCollectionIdx) == 0u)
    {
      /* If retries remain */
      if(vRpcProxy_GetTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx) > 0u)
      {
        /* Set Retry Flag and set Service State back to PROCESSING */
        vRpcProxy_SetRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx, TRUE);
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_PROCESSING_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }
      /* No more retries remain */
      else
      {
        /* The ServiceStatusCallout is called with VRPCPROXY_E_ACK_TIMEOUT, the main statemachine state is set to READY */
        targetIdx = vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx);
        serviceIdx = vRpcProxy_GetTxProxyServiceCollectionIdxOfTxServiceTargetCollection(serviceTargetCollectionIdx);
        vRpcProxy_ServiceManager_ServiceStatusCallout(targetIdx, serviceIdx, VRPCPROXY_E_ACK_TIMEOUT);
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }

    } 
  }       
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_ManageAckTimeouts */

/*******************************************************************************************
 *  vRpcProxy_ManageResponseTimeouts()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageResponseTimeouts(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx)
{ 
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId = 0u;
  vRpcProxy_CTargetsIterType targetIdx = 0u;
  
  /* If ServiceState is WAITING FOR RESPONSE */
  if(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx) == VRPCPROXY_WAITING_FOR_RESPONSE_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION)
  {
    /* Decrement timeout counter */
    vRpcProxy_DecTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx);
  
    /* If timeout counter is 0 */
    if(vRpcProxy_GetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx) == 0u)
    {
      txServiceId = vRpcProxy_GetTxProxyServiceCollectionIdxOfTxServiceTargetCollection(serviceTargetCollectionIdx);
      targetIdx = vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx);
      
      /* Report timeout to the UpperLayer and set main statemachine to READY */
      vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      vRpcProxy_ServiceManager_ServiceStatusCallout(targetIdx, txServiceId, VRPCPROXY_E_RESPONSE_TIMEOUT);
    }
  }
  
  VRPCPROXY_DUMMY_STATEMENT(txServiceId);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(targetIdx);            /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}
/* vRpcProxy_ManageResponseTimeouts */

/*******************************************************************************************
 *  vRpcProxy_ManageRetries()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageRetries(vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx)
{ 
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_CTargetsIterType targetIdx;
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceIdx;

  /* If RetryTransmit Flag is set to TRUE and the Tx TP Statemachine is in READY state */
  if(vRpcProxy_IsRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx))
  {
    targetIdx = vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx);
    if(vRpcProxy_GetTxTpPduStateOfTxTpInfo(targetIdx) == VRPCPROXY_READY_TXTPPDUSTATEOFTXTPINFO)
    {
      /* Set the state variable to TRANSMISSION_STARTED  */
      vRpcProxy_SetTxTpPduStateOfTxTpInfo(targetIdx, VRPCPROXY_TRANSMISSION_STARTED_TXTPPDUSTATEOFTXTPINFO);
      
      /* Decrease RetryCounter and reset RetryFlag */
      vRpcProxy_DecTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx);
      vRpcProxy_SetRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx, FALSE);
      
      /* Copy data to TxTpBuffer and call vRpcProxy_TpManager_TransmitTpFrame() for target */
      serviceIdx = vRpcProxy_GetTxProxyServiceCollectionIdxOfTxServiceTargetCollection(serviceTargetCollectionIdx);
      vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer(serviceIdx, targetIdx, vRpcProxy_GetTxServiceSizeOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceIdx));
      vRpcProxy_TpManager_TransmitTpFrame(targetIdx);
    }
  }
}
/* vRpcProxy_ManageRetries */

/*******************************************************************************************
 *  vRpcProxy_ManageAckService()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageAckService(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_CRxProxyServiceCollectionIterType serviceIdx = 0u;
  vRpcProxy_CTargetsIterType targetIdx;
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId;
  vRpcProxy_CTxProxyServiceCollectionIterType ackServiceId = 0u; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
  Std_ReturnType retVal;

  
  /* Iterate over Rx service collection */
  for(; serviceIdx < vRpcProxy_GetSizeOfCRxProxyServiceCollection(); serviceIdx++)
  {       
     /* If Ack Flag is set to TRUE */
    if(vRpcProxy_IsAckFlagOfVRxProxyServiceCollection(serviceIdx))
    {
      targetIdx = vRpcProxy_GetTargetIdOfVRxProxyServiceCollection(serviceIdx);
      txServiceId = vRpcProxy_GetTxServiceIdToBeAcknowledgedOfVRxProxyServiceCollection(serviceIdx);
      
      /* Iterate over service target collection */
      serviceTargetCollectionIdx = vRpcProxy_GetTxServiceTargetCollectionStartIdxOfCTxProxyServiceCollection(ackServiceId);
      for(; serviceTargetCollectionIdx < vRpcProxy_GetTxServiceTargetCollectionEndIdxOfCTxProxyServiceCollection(ackServiceId); serviceTargetCollectionIdx++)
      {
        if(targetIdx == vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx))
        {
          vRpcProxy_SetAckFlagOfVRxProxyServiceCollection(serviceIdx, FALSE);
          
          /* Get saved result and try to send Acknowledge service */
          retVal = vRpcProxy_ServiceManager_TxAcknowledgeService(targetIdx, vRpcProxy_GetAckResultOfVRxProxyServiceCollection(serviceIdx), txServiceId);
          
          /* If the TxAcknowledge Service was busy, set the Ack Flag again */    
          if(retVal == E_NOT_OK)
          {
            vRpcProxy_SetAckFlagOfVRxProxyServiceCollection(serviceIdx, TRUE);
          }       
          break;
        }
      }       
    }
  }
  
  VRPCPROXY_DUMMY_STATEMENT(ackServiceId);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */  /* lint -e{438} */
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_ManageAckService */

/*******************************************************************************************
 *  vRpcProxy_ManageQueuedTxServices()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageQueuedTxServices(void)
{  
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceIdx = 0u;
  
  /* Iterate over all TxServices  */
  for(; txServiceIdx < vRpcProxy_GetSizeOfTxProxyServiceCollectionWithInvalidIndexes(); txServiceIdx++)
  {  
    /* If it is a valid serviceId */
#if(VRPCPROXY_INVALIDHNDOFTXPROXYSERVICECOLLECTIONWITHINVALIDINDEXES == STD_ON)
    if(!vRpcProxy_IsInvalidHndOfTxProxyServiceCollectionWithInvalidIndexes(txServiceIdx))
#endif
    {
      /* If deferred Flag is set to TRUE for a TxService call */
      if(vRpcProxy_IsTxServiceDeferredProcessingOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(txServiceIdx))
      {
        /* Iterate over all referenced targets and check if the correct state is set */
        vRpcProxy_TargetGroupsIterType targetGroupId = vRpcProxy_GetTxServiceDeferredTargetGroupOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(txServiceIdx);
        
        /* Process saved service call */
        if(vRpcProxy_ServiceManager_CheckEachTpConnectionReady(targetGroupId) == TRUE)
        {
          vRpcProxy_ServiceManager_ProcessTxServiceCall(txServiceIdx, targetGroupId, vRpcProxy_GetTxServiceSizeOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(txServiceIdx));
          vRpcProxy_SetTxServiceDeferredProcessingOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(txServiceIdx, FALSE);
        }
      }
    }
  }
}
/* vRpcProxy_ManageQueuedTxServices */


#if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
/*******************************************************************************************
 *  vRpcProxy_ManageResponseFilterTimeWindow()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ManageResponseFilterTimeWindow(void)
{  
  vRpcProxy_CRxProxyServiceCollectionIterType rxServiceIdx = 0u;

  vRpcProxy_DecResponseFilterTimerValue();
  if(vRpcProxy_GetResponseFilterTimerValue() == 0u)
  {  
    vRpcProxy_SetResponseFilterTimerValue(VRPCPROXY_RESPONSE_FILTER_TIMER_MAX);
  
    for(; rxServiceIdx < vRpcProxy_GetSizeOfCRxProxyServiceCollection(); rxServiceIdx++)
    { 
      vRpcProxy_SetResponseSentInCurrentTimeWindowOfVRxProxyServiceCollection(rxServiceIdx, FALSE); 
    }
  }
}
/* vRpcProxy_ManageResponseFilterTimeWindow */
#endif


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vRpcProxy_InitMemory()
 *********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_InitMemory(void)
{
  /* Initialize all component _INIT_ variables. */
  vRpcProxy_SetInitialized(FALSE);  
}  
/* vRpcProxy_InitMemory() */

/**********************************************************************************************************************
 *  vRpcProxy_Init()
 *********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_Init(P2CONST(vRpcProxy_ConfigType, AUTOMATIC, VRPCPROXY_PBCFG) ConfigPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_ALREADY_INITIALIZED;
  }
  /* Check plausibility of input parameters */
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON) 
  else if(ConfigPtr != NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_CONFIG;
  }
#endif
  else
  {
    /* ----- Implementation ----------------------------------------------- */   
    /* Initialize all Rx Services and related parameters */
    vRpcProxy_TpManager_InitRxTp();
    vRpcProxy_InitRx();
    
    /* Initialize all Tx Services and related parameters */
    vRpcProxy_TpManager_InitTxTp();
    vRpcProxy_InitTx();
    
#if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
  vRpcProxy_SetResponseFilterTimerValue(VRPCPROXY_RESPONSE_FILTER_TIMER_MAX);
#endif
    /* Set component state to INITIALIZED  */
    vRpcProxy_SetInitialized(TRUE); 
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR)      
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_INIT, (errorId));
  }
#else
  VRPCPROXY_DUMMY_STATEMENT(errorId);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */  /* lint -e{438} */
#endif
  VRPCPROXY_DUMMY_STATEMENT(ConfigPtr);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
} 
/* vRpcProxy_Init() */

#if (VRPCPROXY_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_GetVersionInfo()
 *********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VRPCPROXY_APPL_VAR) VersioninfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* ----- Development Error Checks ------------------------------------- */
  /* Check plausibility of input parameter */
# if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  if (VersioninfoPtr == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else
# endif
  {
    /* ----- Implementation ----------------------------------------------- */
    /* Prepare structure to return */
    VersioninfoPtr->vendorID = (uint16) (VRPCPROXY_VENDOR_ID);                 /* SBSW_VRPCPROXY_GETVERSION_PARAM */
    VersioninfoPtr->moduleID = (uint8) (VRPCPROXY_MODULE_ID);                  /* SBSW_VRPCPROXY_GETVERSION_PARAM */
    VersioninfoPtr->sw_major_version = (uint8) (VRPCPROXY_SW_MAJOR_VERSION);   /* SBSW_VRPCPROXY_GETVERSION_PARAM */
    VersioninfoPtr->sw_minor_version = (uint8) (VRPCPROXY_SW_MINOR_VERSION);   /* SBSW_VRPCPROXY_GETVERSION_PARAM */
    VersioninfoPtr->sw_patch_version = (uint8) (VRPCPROXY_SW_PATCH_VERSION);   /* SBSW_VRPCPROXY_GETVERSION_PARAM */
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VRPCPROXY_E_NO_ERROR)
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_VERSIONINFO, (errorId));
  }
# else
  VRPCPROXY_DUMMY_STATEMENT(errorId);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */  /* lint -e{438} */
# endif
} 
/* vRpcProxy_GetVersionInfo() */
#endif

/*******************************************************************************************
 *  vRpcProxy_MainFunction()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_MainFunction(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
  else
  {

    /* ----- Implementation ----------------------------------------------- */
    vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx = 0u;
    for(; serviceTargetCollectionIdx < vRpcProxy_GetSizeOfCTxServiceTargetCollection(); serviceTargetCollectionIdx++)
    {
      /* Call vRpcProxy_ManageAckTimeouts */
      vRpcProxy_ManageAckTimeouts(serviceTargetCollectionIdx);
        
      /* Call vRpcProxy_ManageResponseTimeouts */
      vRpcProxy_ManageResponseTimeouts(serviceTargetCollectionIdx);
    
      /* Call vRpcProxy_ManageRetries */
      vRpcProxy_ManageRetries(serviceTargetCollectionIdx);    
    }
    
    /* Call vRpcProxy_ManagAckService */
    vRpcProxy_ManageAckService();
      
    /* Call vRpcProxy_ManageQueuedTxServices */
    vRpcProxy_ManageQueuedTxServices();   
    
# if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
    vRpcProxy_ManageResponseFilterTimeWindow();
# endif
  }

  /* ----- Development Error Report --------------------------------------- */
# if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VRPCPROXY_E_NO_ERROR)
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_MAINFUNCTION, (errorId));
  }
# else
  VRPCPROXY_DUMMY_STATEMENT(errorId);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */  /* lint -e{438} */
# endif
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_MainFunction() */

/*******************************************************************************************
 *  vRpcProxy_TpTxConfirmation()
 *******************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpTxConfirmation(PduIdType PduId, Std_ReturnType result)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_CTargetsIterType targetHandleId = (vRpcProxy_CTargetsIterType)PduId;
  
  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON )
  /* Check plausibility of input parameters */
  else if(targetHandleId >= vRpcProxy_GetSizeOfCTargets())
  {
    errorId = VRPCPROXY_E_PARAM;
  }
#endif
  else
  {
    /* ----- Implementation ----------------------------------------------- */
    /* Call vRpcProxy_TpManager_TpTxConfirmation_Processing() */
    vRpcProxy_TpManager_TpTxConfirmation_Processing(targetHandleId, result);
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_TPTXCONFIRMATION, (errorId));
  }
#endif

  VRPCPROXY_DUMMY_STATEMENT(errorId);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(targetHandleId);   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(result);           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}   
/* vRpcProxy_TpTxConfirmation() */

/**********************************************************************************************************************
 *  vRpcProxy_CopyTxData
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_CopyTxData(PduIdType PduId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPtr, P2VAR(RetryInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) RetryInfoPtr, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_DATA) availableDataPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;         /* PRQA S 2981 */ /* MD_MSR_RetVal */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_CTargetsIterType targetHandleId = (vRpcProxy_CTargetsIterType)PduId;

  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON )
  /* Check plausibility of input parameters */
  else if(targetHandleId >= vRpcProxy_GetSizeOfCTargets())
  {
    errorId = VRPCPROXY_E_PARAM;
  }
  else if(PduInfoPtr == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else if((PduInfoPtr->SduDataPtr == NULL_PTR) && (PduInfoPtr->SduLength > 0u))
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else if(availableDataPtr == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
#endif
  else
  {
    /* ----- Implementation ----------------------------------------------- */
    /* Call vRpcProxy_TpManager_CopyTxData_Processing() */
    retVal = vRpcProxy_TpManager_CopyTxData_Processing(targetHandleId, PduInfoPtr, RetryInfoPtr, availableDataPtr); 
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_COPYTXDATA, (errorId));
  }
#endif

  VRPCPROXY_DUMMY_STATEMENT(errorId);         /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(targetHandleId);  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(PduInfoPtr);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(RetryInfoPtr);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(availableDataPtr);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_CopyTxData() */

/**********************************************************************************************************************
 *  vRpcProxy_StartOfReception
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_StartOfReception(PduIdType PduId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_VAR) TpSduInfoPtr, PduLengthType TpSduLength, /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
                                                       P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;                 /* PRQA S 2981 */ /* MD_MSR_RetVal */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;    /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_SourcesIterType sourceHandleId = (vRpcProxy_SourcesIterType)PduId;

  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON )
  /* Check plausibility of input parameters */
  else if(sourceHandleId >= vRpcProxy_GetSizeOfSources())
  {
    errorId = VRPCPROXY_E_PARAM;
  }
  else if(RxBufferSizePtr == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
#endif
  else
  {
      /* ----- Implementation ----------------------------------------------- */
      /* Process the passed Rx Tp Pdu */
      retVal = vRpcProxy_TpManager_StartOfReception_Processing(sourceHandleId, TpSduLength, RxBufferSizePtr);
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_STARTOFRECEPTION, errorId);
  }
#endif

  VRPCPROXY_DUMMY_STATEMENT(errorId);               /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);        /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(TpSduInfoPtr);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(TpSduLength);           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(RxBufferSizePtr);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
}
/* vRpcProxy_StartOfReception() */

/**********************************************************************************************************************
 *  vRpcProxy_TpRxIndication()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpRxIndication(PduIdType PduId, Std_ReturnType result)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;       /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_SourcesIterType sourceHandleId = (vRpcProxy_SourcesIterType)PduId;

  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON )
  /* Check plausibility of input parameters */
  else if(sourceHandleId >= vRpcProxy_GetSizeOfSources())
  {
    errorId = VRPCPROXY_E_PARAM;
  }
#endif
  else
  {
    /* ----- Implementation ----------------------------------------------- */
    /* Process the passed Rx Tp Pdu */
    vRpcProxy_TpManager_TpRxIndication_Processing(sourceHandleId, result);
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_TPRXINDICATION, errorId);
  }
#endif

  VRPCPROXY_DUMMY_STATEMENT(errorId);         /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(result);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}
/* vRpcProxy_TpRxIndication() */

/**********************************************************************************************************************
 *  vRpcProxy_CopyRxData
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 **********************************************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_CopyRxData(PduIdType PduId, CONSTP2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPointer, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr)      /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;                 /* PRQA S 2981 */ /* MD_MSR_RetVal */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;    /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_SourcesIterType sourceHandleId = (vRpcProxy_SourcesIterType)PduId;

  /* ----- Development Error Checks ------------------------------------- */
  /* Check if component is initialized */
  if(!vRpcProxy_IsInitialized())
  {
    errorId = VRPCPROXY_E_UNINIT;
  }
#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  /* Check plausibility of input parameters */
  else if(sourceHandleId >= vRpcProxy_GetSizeOfSources())
  {
    errorId = VRPCPROXY_E_PARAM;
  }
  else if(PduInfoPointer == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else if((PduInfoPointer->SduDataPtr == NULL_PTR) && (PduInfoPointer->SduLength > 0u))
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else if(RxBufferSizePtr == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
#endif
  else
  {

    /* ----- Implementation ----------------------------------------------- */
    /* Call  vRpcProxy_TpManager_CopyRxData_Processing() */
    retVal = vRpcProxy_TpManager_CopyRxData_Processing(sourceHandleId, PduInfoPointer, RxBufferSizePtr);  
  }

  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_COPYRXDATA, errorId);
  }
#endif

  VRPCPROXY_DUMMY_STATEMENT(errorId);                 /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(RxBufferSizePtr);         /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT_CONST(PduInfoPointer);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_CopyRxData() */

#define VRPCPROXY_STOP_SEC_CODE
#include "MemMap.h"     /* PRQA S 5087 */        /*  MD_MSR_MemMap */

/* Justification for module-specific MISRA deviations:
    
   MD_vRpcProxy_2985: Misra Rule 2.2
     Reason:      The value of the operands depends on the configuration.
     Risk:        No risk, because only warning that the operation is redundant.
     Prevention:  No prevention necessary.
 
   MD_vRpcProxy_2003:  Misra Rule 16.3
     Reason:     A break-statement is not required, the switch case needs to perform the remaining operations of the succeeding case.
     Risk:       No risk
     Prevention: No prevention necessary
 
   MD_vRpcProxy_4394:  Misra Rule 10.8
     Reason:     The composite expression has to be cast as a whole because of the shift or bitwise operation. Shift and bitwise operations should only be performed with esentially unsigned types. The cast is necessary, because the result of the shift or bitwise operation is signed.
     Risk:       No risk
     Prevention: No prevention necessary
     
   MD_vRpcProxy_2842_CSL_Index: Misra Rule 18.1, Misra Rule 21.18, Misra Dir 4.1
     Reason:     The ComStackLib validates that the Index doesn't exceed the table.
     Risk:       No risk.
     Prevention: Qualified use-case CSL01 of ComStackLib.
     
   MD_vRpcProxy_2842: Misra Rule 18.1, Misra Rule 21.18, Misra Dir 4.1
     Reason:     The pointer is checked for NULL_PTR before writing to the pointer. This function is only called by vRpcProxy. This function is not called with an invalid pointer value.
     Risk:       No risk.
     Prevention: No prevention necessary.
     
   MD_vRpcProxy_2991: Misra Rule 14.3
     Reason:     The value of the if-controlling expression depends on the configuration.
     Risk:       No risk.
     Prevention: No prevention necessary.
     
   MD_vRpcProxy_2986: Misra Rule 2.2
     Reason:     The value of the operands depends on the configuration.
     Risk:       No risk, because only warning that the operation is redundant.
     Prevention: No prevention necessary.
     
   MD_vRpcProxy_2995: Misra Rule 2.2
     Reason:     The value of the logical operation depends on the configuration.
     Risk:       No risk.
     Prevention: No prevention necessary.
*/

/* SBSW_JUSTIFICATION_BEGIN
  
  \ID SBSW_VRPCPROXY_GETVERSION_PARAM
    \DESCRIPTION    The vRpcProxy_GetVersionInfo writes to the 'versioninfo' pointer parameter. It is checked against NULL,
                    but this does not guarantee that the referenced memory is valid.
    \COUNTERMEASURE \N  Pointers passed to public vRpcProxy APIs point to a valid memory range. A general countermeasure is included in the safety manual.
    
SBSW_JUSTIFICATION_END 
*/

/* COV_JUSTIFICATION_BEGIN
    
  \ID COV_PROXY_TIMING_DEPENDENT_RXINDICATION
    \ACCEPT TX
    \REASON The condition checks if the mainstatemachine is set to ready which is done in the same context later on. Therefore this condition only evaluates to false if a second call to rxindication occurs before the first one is completed.    
    
COV_JUSTIFICATION_END
*/


/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy.c
 *********************************************************************************************************************/
