/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_TpManager.h
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy 
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VRPCPROXYTPMANAGER_H)
# define VRPCPROXYTPMANAGER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VRPCPROXY_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap.1 */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_InitTxTp
 **********************************************************************************************************************/
/*! \brief    This function is called to initalize Tx related Tp Parameters
 *  \details  -
 *  \context  TASK|ISR2
 *  \synchronous TRUE
 *  \reentrant   TRUE, for different Handles
 *  \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_InitTxTp(void);

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_InitRxTp
 **********************************************************************************************************************/
/*! \brief    This function is called to initalize Rx related Tp Parameters
 *  \details  -
 *  \context  TASK|ISR2
 *  \synchronous TRUE
 *  \reentrant   TRUE, for different Handles
 *  \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_InitRxTp(void);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_ResetTxTpConnection
 **********************************************************************************************************************/
/*! \brief    This function resets the Tx TP connection.
 *  \details  -
 *  \param[in]   targetHandleId    ID of PDU whose TP connection shall be reset.
 *  \context  TASK|ISR2
 *  \synchronous FALSE
 *  \reentrant   TRUE, for different Handles
 *  \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_ResetTxTpConnection(vRpcProxy_CTargetsIterType targetHandleId);

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_ResetRxTpConnection
 **********************************************************************************************************************/
/*! \brief       This function resets the Rx TP connection.
 *  \details     -
 *  \param[in]   sourceHandleId      ID of PDU whose TP connection shall be reset.
 *  \context     TASK|ISR2
 *  \synchronous FALSE
 *  \reentrant   TRUE, for different Handles
 *  \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_ResetRxTpConnection(vRpcProxy_SourcesIterType sourceHandleId);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_TransmitTpFrame()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy to transmit a Tp frame. 
 *   \details     The vRpcProxy calls the PduR_Transmit function.
 *   \param[in]   targetHandleId            ID of the transmitted target.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TransmitTpFrame(vRpcProxy_CTargetsIterType targetHandleId);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_CopyTxData_Processing()
 **********************************************************************************************************************/
/*! \brief    This function is called by the vRpcProxy_CopyTxData to hand a TP segment to the PduR TP buffer.
 *   \details     This function is called by the vRpcProxy_CopyTxData function to copy a segment of the Pdu to the Tp Buffer
 *                of the PduR.
 *   \param[in]   targetHandleId      ID of the transmitted PDU.
 *   \param[in]   PduInfoPtr          Pointer to a PduInfoType, which indicates the number of bytes to be copied (SduLength) and the 
 *                                    location where the data have to be copied to (SduDataPtr).
 *   \param[in]   RetryInfoPtr        This parameter is currently not used.
 *   \param[out]  availableDataPtr        Indicates the remaining number of bytes that are available in the vRpcProxy module's TpTx buffer. 
 *   \return      BufReq_ReturnType   BUFREQ_OK:       Data has been copied to the TpTx buffer completely as requested. 
 *                                    BUFREQ_E_BUSY:   Request could not be fulfilled, because the required amount of Tx data is not available. 
 *                                                     The lower layer module may retry this call later on. No data has been copied.
 *                                    BUFREQ_E_NOT_OK: Data has not been copied. Request failed. 
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_CopyTxData_Processing(vRpcProxy_CTargetsIterType targetHandleId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPtr, 
                P2VAR(RetryInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) RetryInfoPtr, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_DATA) availableDataPtr);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_TpTxConfirmation_Processing()
 **********************************************************************************************************************/
/*! \brief    This function is called by vRpcProxy_TpTxConfirmation after a TP PDU has been transmitted.
 *   \details      This function is called by the vRpcProxy_TpTxConfirmation function after the PDU has been transmitted, the 
 *                 result indicates whether the transmission was successful or not.
 *   \param[in]    targetHandleId      ID of PDU that has been transmitted. 
 *   \param[in]    result              Indicates whether the message was transmitted successfully.
 *   \context      TASK|ISR
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre         - 
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TpTxConfirmation_Processing(vRpcProxy_CTargetsIterType targetHandleId, Std_ReturnType result);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_CopyRxData_Processing()
 **********************************************************************************************************************/
/*! \brief    This function is called by the PduR to hand a received TP segment to vRpcProxy.
 *   \details     The vRpcProxy copies the received segment in his internal tp buffer.
 *   \param[in]   sourceHandleId     ID of PDU that has been received. Identifies the data that has been received.
 *   \param[in]   PduInfoPointer     Payload information of the received TP segment (pointer to data and data length).
 *   \param[out]  RxBufferSizePtr    The vRpcProxy returns in this value the remaining TP buffer size to the lower layer.
 *   \return      BufReq_ReturnType  BUFREQ_OK:       Connection has been accepted.
 *                                                    RxBufferSizePtr indicates the available receive buffer.
 *                                   BUFREQ_E_NOT_OK: Connection has been rejected.
 *                                                    RxBufferSizePtr remains unchanged.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
 FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_CopyRxData_Processing(vRpcProxy_SourcesIterType sourceHandleId, CONSTP2CONST(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPointer,
                                                                         P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr);

/**********************************************************************************************************************
 * vRpcProxy_TpManager_StartOfReception_Processing()
 **********************************************************************************************************************/
/*! \brief    This function is called by vRpcProxy_StartOfReception if the PduR has indicated the start of reception of a Pdu.
 *   \details     -
 *   \param[in]   sourceHandleId     ID of PDU that has been received. 
 *   \param[in]   TpSduLength        The length of the received Tp segment
 *   \param[out]  RxBufferSizePtr    The vRpcProxy returns in this value the remaining TP buffer size to the lower layer.
 *   \return      BufReq_ReturnType  BUFREQ_OK:       Connection has been accepted.
 *                                                    RxBufferSizePtr indicates the available receive buffer.
 *                                   BUFREQ_E_NOT_OK: Connection has been rejected.
 *                                                    RxBufferSizePtr remains unchanged.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_StartOfReception_Processing(vRpcProxy_SourcesIterType sourceHandleId, PduLengthType TpSduLength, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr);
                                                                      
/**********************************************************************************************************************
 * vRpcProxy_TpManager_TpRxIndication_Processing()
 **********************************************************************************************************************/
/*! \brief    This function is called by vRpcProxy_TpRxIndication after a TP I-PDU has been received.
 *   \details  -
 *   \param[in]   sourceHandleId   ID of PDU that has been received. Identifies the data that has been received.
 *                                 Range: 0..(maximum number of PDU IDs received by vRpcProxy) - 1
 *   \param[in]   result           Indicates whether the message was received successfully.
 *   \context  TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TpRxIndication_Processing(vRpcProxy_SourcesIterType sourceHandleId, Std_ReturnType result);
 
# define VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */

#endif /* VRPCPROXYTPMANAGER_H */
/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_TpManager.h
 *********************************************************************************************************************/
