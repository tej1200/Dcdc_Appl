/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_Cbk.h
 *      Project:  vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  vRpcProxy callback Header File
 *
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VRPCPROXY_CBK_H)
# define VRPCPROXY_CBK_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "vRpcProxy_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VRPCPROXY_START_SEC_CODE
# include "MemMap.h"   /* PRQA S 5087 */       /* MD_MSR_MemMap */

/*******************************************************************************************
 *  vRpcProxy_TpTxConfirmation()
 *******************************************************************************************/
/*! \brief        This function is called by the PduR after a PDU has been transmitted via the transport protocol on its network.
 *  \details      This function is called after the PDU has been transmitted on its network, the 
 *                result indicates whether the transmission was successful or not.
 *  \param[in]    PduId    ID of the PDU that has been transmitted.
 *  \param[in]    result   result of the transmission of the PDU
 *  \pre          -
 *  \context      TASK|ISR
 *  \reentrant    TRUE for different handle IDs
 *  \synchronous  TRUE      
 *******************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpTxConfirmation(PduIdType PduId, Std_ReturnType result);

/**********************************************************************************************************************
 * vRpcProxy_CopyTxData
 **********************************************************************************************************************/
/*! \brief       This function is called by the PduR to copy Data from the vRpcProxy TpTx buffer to the PduR TP buffer.
 *  \details     Each call to this function provides the next part of the PDU data.
 *               The size of the remaining data is written to the position indicated by availableDataPtr. 
 *  \param[in]   PduId            ID of the transmitted PDU.
 *  \param[in]   PduInfoPtr       Pointer to a PduInfoType, which indicates the number of bytes to be copied (SduLength) and the 
 *                                location where the data have to be copied to (SduDataPtr).
 *                                If not enough transmit data is available, no data is copied by the vRpcProxy module and 
 *                                BUFREQ_E_BUSY is returned. The lower layer module may retry the call. 
 *  \param[in]   RetryInfoPtr     Currently not in use.
 *  \param[out]  availableDataPtr     Indicates the remaining number of bytes that are available in the vRpcProxy module's TpTx buffer. 
 *                                availableDataPtr can be used by TP modules that support dynamic payload lengths 
 *                                (e.g. FrIsoTp) to determine the size of the following CFs.
 *  \return      BufReq_ReturnType
 *               BUFREQ_OK:       Data has been copied to the TpTx buffer completely as requested. 
 *               BUFREQ_E_BUSY:   Request could not be fulfilled, because the required amount of Tx data is not available. 
 *                                The lower layer module may retry this call later on. No data has been copied.
 *               BUFREQ_E_NOT_OK: Data has not been copied. Request failed. 
 *  \context     TASK|ISR2
 *  \synchronous TRUE
 *  \pre         -
 *  \reentrant   TRUE, for different PduIds.  
 **********************************************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_CopyTxData(PduIdType PduId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPtr, P2VAR(RetryInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) RetryInfoPtr, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_DATA) availableDataPtr);

/*******************************************************************************************
 *  vRpcProxy_StartOfReception()
 *******************************************************************************************/
/*! \brief        Starts the reception.
 *  \details      This function starts the reception of an Rx PDU and returns the available buffer size pointer.
 *  \param[in]    PduId         Id of the PDU to be received
 *  \param[in]    TpSduInfoPtr  Pointer to buffer containing PDU
 *  \param[in]    TpSduLength   Length of data
 *  \param[out]   RxBufferSizePtr Pointer to available buffer
 *  \context      TASK|ISR
 *  \reentrant    TRUE for different handle IDs
 *  \synchronous  TRUE
 *  \note         Called by PduR module
 *  \pre           -        
 *******************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_StartOfReception(PduIdType PduId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_VAR) TpSduInfoPtr, PduLengthType TpSduLength,
                                                       P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr);

/*******************************************************************************************
 *  vRpcProxy_TpRxIndication()
 *******************************************************************************************/
/*! \brief        Indicates complete reception of a PDU
 *  \details      This function indicates the end of reception of a rx PDU
 *  \param[in]    RxPduId   Id of the received PDU
 *  \param[in]    result    result of reception
 *  \context      TASK|ISR
 *  \reentrant    TRUE for different handle IDs
 *  \synchronous  TRUE
 *  \note         Called by PduR module
 *  \pre          -        
*******************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpRxIndication(PduIdType PduId, Std_ReturnType result);

/*******************************************************************************************
 *  vRpcProxy_CopyRxData()
 *******************************************************************************************/
/*! \brief        Indicates complete reception of a PDU
 *  \details      This function copies the PDU data into a local buffer. It either copies one single frame or segmented if not all data can be copied at once.
 *                The bufferSizePtr returns the size of the available buffer the PDU data is copied to.
 *  \param[in]    PduId             Id of the PDU
 *  \param[in]    PduInfoPointer    Pointer to buffer containing PDU data
 *  \param[out]   RxBufferSizePtr   Pointer to the remaining buffer size
 *  \context      TASK|ISR
 *  \reentrant    TRUE for different handle IDs
 *  \synchronous  TRUE
 *  \note         Called by PduR module
 *  \pre          -        
 *******************************************************************************************/
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_CopyRxData(PduIdType PduId, CONSTP2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPointer, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr);

# define VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h"   /* PRQA S 5087 */       /* MD_MSR_MemMap */

#endif  /* VRPCPROXY_CBK_H */
/**********************************************************************************************************************
  END OF FILE: vRpcProxy_Cbk.h
**********************************************************************************************************************/
