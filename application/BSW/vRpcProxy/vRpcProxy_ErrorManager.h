/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_ErrorManager.h
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VRPCPROXYERRORMANAGER_H)
# define VRPCPROXYERRORMANAGER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define VRPCPROXY_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_TxTpConnectionError()
 **********************************************************************************************************************/
/*!  \brief       This function is called by the vRpcProxy if an error occurs in the Tx Tp statemachine.
 *   \details     The vRpcProxy calls the vRpcProxy_ErrorManager_TxTpConnectionError() function if a Tp confirmation is negative 
 *                or PduR_vRpcProxyTransmit returns E_NOT_OK. Services are flagged for retransmission if retries remain. 
 *                Otherwise the Service State is reset and the ServiceStatusCallout is called.
 *   \param[in]   targetHandleId                 ID of the target PDU.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_TxTpConnectionError(vRpcProxy_CTargetsIterType targetHandleId);

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_RxTpConnectionError()
 **********************************************************************************************************************/
/*!  \brief       This function is called by the vRpcProxy if an error occurs in the Rx Tp statemachine.
 *   \details     The vRpcProxy calls the vRpcProxy_ErrorManager_RxTpConnectionError() function if a Tp StartOfReception or RxIndication 
 *                is neagtive or called in the wrong state or any other error occurs.
 *   \param[in]   sourceHandleId                   ID of the source PDU.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_RxTpConnectionError(vRpcProxy_SourcesIterType sourceHandleId);

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_RemoteProxyServiceUnknown()
 **********************************************************************************************************************/
/*!  \brief       This function is called by the vRpcProxy if an error occurs in the remote vRpcProxy.
 *   \details     The vRpcProxy calls the vRpcProxy_ErrorManager_RemoteProxyServiceUnknown() function if a service call is unknown by 
 *                the remote proxy.
 *   \param[in]   sourceHandleId  ID of the source PDU.
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \pre         -
 *   \reentrant   TRUE, for different Handles
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_RemoteProxyServiceUnknown(vRpcProxy_SourcesIterType sourceHandleId);

# define VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */
#endif /* VRPCPROXYERRORMANAGER_H */
/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_ErrorManager.h
 *********************************************************************************************************************/
