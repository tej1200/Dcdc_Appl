/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_TpManager.c
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL MISRA / PCLINT JUSTIFICATION
 *********************************************************************************************************************/
 
#define VRPCPROXYTPMANAGER_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vstdlib.h"
#include "SchM_vRpcProxy.h"
#include "PduR_vRpcProxy.h"
#include "vRpcProxy.h"
#include "vRpcProxy_TpManager.h"
#include "vRpcProxy_ErrorManager.h"
#include "vRpcProxy_ServiceManager.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
 
 /**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VRPCPROXY_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 */        /*  MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_ResetTxTpConnection
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_ResetTxTpConnection(vRpcProxy_CTargetsIterType targetHandleId)
{
  /* Reset WrittenBytesCounter, TpSduLength and TP Connection state */
  vRpcProxy_SetTxTpSduLengthOfTxTpInfo(targetHandleId, 0u);  
  vRpcProxy_SetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId, 0u);
  vRpcProxy_SetTxTpPduStateOfTxTpInfo(targetHandleId, VRPCPROXY_READY_TXTPPDUSTATEOFTXTPINFO); 
}
/* vRpcProxy_TpManager_ResetTxTpConnection() */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_ResetRxTpConnection
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_ResetRxTpConnection(vRpcProxy_SourcesIterType sourceHandleId)
{
  /* Reset WrittenBytesCounter, TpSduLength and TP Connection state */
  vRpcProxy_SetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId, 0u);
  vRpcProxy_SetRxTpSduLengthOfRxTpInfo(sourceHandleId, 0u);
  vRpcProxy_SetRxTpPduStateOfRxTpInfo(sourceHandleId, VRPCPROXY_READY_RXTPPDUSTATEOFRXTPINFO);

}
/* vRpcProxy_TpManager_ResetRxTpConnection() */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_InitTxTp
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_InitTxTp(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_CTargetsIterType targetIdx = 0u;
  
  /* Iterate over all Tx Tp Pdus and initialize all related parameters */
  for(; targetIdx < vRpcProxy_GetSizeOfCTargets(); targetIdx++)
  {
    SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
    vRpcProxy_TpManager_ResetTxTpConnection(targetIdx);    
    SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();    
  }  
}
/* vRpcProxy_InitTx() */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_InitRxTp
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_InitRxTp(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_SourcesIterType sourceIdx = 0u;
  
  /* Iterate over all Rx Tp Pdus and initialize all related parameters */
  for(; sourceIdx < vRpcProxy_GetSizeOfSources(); sourceIdx++)
  {
    SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
    vRpcProxy_TpManager_ResetRxTpConnection(sourceIdx);
    SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
  }
}
/* vRpcProxy_InitRx() */ 

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_TransmitTpFrame
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TransmitTpFrame(vRpcProxy_CTargetsIterType targetHandleId)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;     /* PRQA S 2981 */ /* MD_MSR_RetVal */
  PduInfoType PduInfo;

  /* Set the SduLength and SduDataPtr */
  PduInfo.SduDataPtr = vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetHandleId));
  PduInfo.SduLength = (PduLengthType) vRpcProxy_GetTxTpSduLengthOfTxTpInfo(targetHandleId);
      
  /* Initiate the transmit of a Pdu by a call of PduR_vRpcProxyTransmit */
  retVal = PduR_vRpcProxyTransmit(vRpcProxy_GetExternalIdOfTargets(targetHandleId), &PduInfo);
  
  if(retVal == E_NOT_OK)
  {
    /* If the return value is E_NOT_OK call vRpcProxy_ErrorManager_TxTpConnectionError */
    vRpcProxy_ErrorManager_TxTpConnectionError(targetHandleId);
  }
}
/* vRpcProxy_TpManager_TransmitTpFrame */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_CopyTxData_Processing
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_CopyTxData_Processing(vRpcProxy_CTargetsIterType targetHandleId, P2VAR(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPtr, /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
                P2VAR(RetryInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) RetryInfoPtr, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_DATA) availableDataPtr) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;   /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* If TxTpPduState is... */
  switch (vRpcProxy_GetTxTpPduStateOfTxTpInfo(targetHandleId))
  {
      /* ...TRANSMISSION_STARTED, switch to COPYING and Copy Tx Data */
    case VRPCPROXY_TRANSMISSION_STARTED_TXTPPDUSTATEOFTXTPINFO:
    {
      vRpcProxy_SetTxTpPduStateOfTxTpInfo(targetHandleId, VRPCPROXY_COPYING_TXTPPDUSTATEOFTXTPINFO);  
      /* No break required here. Copying operations shall be performed. */
    }
      /* ...COPYING */
    case VRPCPROXY_COPYING_TXTPPDUSTATEOFTXTPINFO:  /* PRQA S 2003 */ /* MD_vRpcProxy_2003 */
    {
      /* If enough data is present */
      if(PduInfoPtr->SduLength <= (vRpcProxy_GetTxTpSduLengthOfTxTpInfo(targetHandleId) - vRpcProxy_GetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId)))
      {
        uint16 remainingBytes;

        /* If data shall be copied */
        if((PduInfoPtr->SduLength > 0u) && (PduInfoPtr->SduDataPtr != NULL_PTR))
        {
          /* Copy the provided data segment to the Tx PDU buffer and increment the WrittenBytesCouter by the passed SduLength */
          VStdLib_MemCpy_s(PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength, vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetHandleId) + vRpcProxy_GetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId)), PduInfoPtr->SduLength); /* PRQA S 0315, 2986 */ /* MD_MSR_VStdLibCopy, MD_vRpcProxy_2986 */
          vRpcProxy_SetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId, (vRpcProxy_GetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId) + PduInfoPtr->SduLength));
        }     
        /* Set the passed availableDataPtr to number of remaining bytes in the Tx PDU buffer */
        remainingBytes = (vRpcProxy_GetTxTpSduLengthOfTxTpInfo(targetHandleId) - vRpcProxy_GetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId));  
        *availableDataPtr = remainingBytes; 

        /* If all bytes are transmitted set the state to WAITING_FOR_CONFIRMATION */
        if(0u == remainingBytes)
        {
          vRpcProxy_SetTxTpPduStateOfTxTpInfo(targetHandleId, VRPCPROXY_WAITING_FOR_CONFIRMATION_TXTPPDUSTATEOFTXTPINFO);      
        }
        
        retVal = BUFREQ_OK;         
      }
      /* Otherwise set the passed availableDataPtr to number of remaining bytes in the Tx PDU buffer */
      else
      {
        *availableDataPtr = (vRpcProxy_GetTxTpSduLengthOfTxTpInfo(targetHandleId) - vRpcProxy_GetTxTpWrittenBytesCounterOfTxTpInfo(targetHandleId)); 
        retVal = BUFREQ_E_BUSY;
      }
      break;
    }
      /* ...WAITING_FOR_CONFIRMATION, reset the connection */
    case VRPCPROXY_WAITING_FOR_CONFIRMATION_TXTPPDUSTATEOFTXTPINFO:
    {
      SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
      vRpcProxy_TpManager_ResetTxTpConnection(targetHandleId);    
      SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();       
      break;
    }
      /* ...READY, do nothing */
    default:
    {
      /* VRPCPROXY_READY_RXTPCONNECTIONSTATE */
      break;
    }
  }
  VRPCPROXY_DUMMY_STATEMENT(PduInfoPtr);          /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(RetryInfoPtr);        /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(availableDataPtr);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  
  return retVal;
}
/* vRpcProxy_TpManager_CopyTxData_Processing */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_TpTxConfirmation_Processing
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TpTxConfirmation_Processing(vRpcProxy_CTargetsIterType targetHandleId, Std_ReturnType result)
{
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
    
  /* If the state is WAITING_FOR_CONFIRMATION */
  if(vRpcProxy_GetTxTpPduStateOfTxTpInfo(targetHandleId) == VRPCPROXY_WAITING_FOR_CONFIRMATION_TXTPPDUSTATEOFTXTPINFO)
  {
    /* If the passed result is E_OK */
    if(result == E_OK)
    {
	    vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId = (vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType)vRpcProxy_GetLastTxServiceSentOfTargets(targetHandleId); 
		
      /* Process confirmation: prepare the TP parameters for the start of a new transmission */
      SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
      vRpcProxy_TpManager_ResetTxTpConnection(targetHandleId); 
      SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
  
      serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(serviceId, targetHandleId);
  
      /* If it is an acknowledge service set the main statemachine to READY */
      if(vRpcProxy_GetServiceTypeOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId) == VRPCPROXY_ACK_SERVICETYPEOFCTXPROXYSERVICECOLLECTION)
      {
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);   
      }
      /* Set TimeoutCounter, TimeoutFlag and set main statemachine state to WAITING_FOR_ACK */
      else
      {
        vRpcProxy_SetTimeoutCounterAckOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxTimeoutCountAckOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId));
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_WAITING_FOR_ACK_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION); 
      }
      
    }
    /* Tp error handling */
    else
    {
      vRpcProxy_ErrorManager_TxTpConnectionError(targetHandleId);
    }
  } 
  /* Confirmation is silently discarded in case the state is not WATING_FOR_CONFIRMATION */
  else 
  {   
    SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
    vRpcProxy_TpManager_ResetTxTpConnection(targetHandleId); 
    SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
  }
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_TpManager_TpTxConfirmation_Processing */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_StartOfReception_Processing
 **********************************************************************************************************************/
/*!  * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_StartOfReception_Processing(vRpcProxy_SourcesIterType sourceHandleId, PduLengthType TpSduLength, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;    /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  switch (vRpcProxy_GetRxTpPduStateOfRxTpInfo(sourceHandleId))
  {
      /* If vRpcProxy is ready for new TP connection */
    case VRPCPROXY_READY_RXTPPDUSTATEOFRXTPINFO:
    {
      /* If the passed TpSduLength fits into the current available buffer */
      if(TpSduLength <= vRpcProxy_GetRxTpBufferLengthOfSources(sourceHandleId))
      {
        /* Store the passed TpSduLength, set the passed RxBufferSizePtr to the available buffer size and set the Rx Tp vRpcProxyIPdu state to RECEPTION_STARTED */
        vRpcProxy_SetRxTpSduLengthOfRxTpInfo(sourceHandleId, TpSduLength);
        *RxBufferSizePtr = vRpcProxy_GetRxTpBufferLengthOfSources(sourceHandleId);

        vRpcProxy_SetRxTpPduStateOfRxTpInfo(sourceHandleId, VRPCPROXY_RECEPTION_STARTED_RXTPPDUSTATEOFRXTPINFO);
        retVal = BUFREQ_OK;
      }
      /* Otherwise return BUFREQ_E_OVFL. The bufferSizePtr remains unchanged */
      else
      {
        *RxBufferSizePtr = vRpcProxy_GetRxTpBufferLengthOfSources(sourceHandleId);     /* SBSW_VRPCPROXY_PTR_API */
        retVal = BUFREQ_E_OVFL;
      }
      break;
    }
      /* Else, silently discarded */
    default:
    { 
      SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
      SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      break;
    }
  }
 
  VRPCPROXY_DUMMY_STATEMENT(TpSduLength);      /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(RxBufferSizePtr);  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
}
/* vRpcProxy_TpManager_StartOfReception_Processing() */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_CopyRxData_Processing
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, VRPCPROXY_CODE) vRpcProxy_TpManager_CopyRxData_Processing(vRpcProxy_SourcesIterType sourceHandleId, CONSTP2CONST(PduInfoType, AUTOMATIC, VRPCPROXY_APPL_DATA) PduInfoPointer, P2VAR(PduLengthType, AUTOMATIC, VRPCPROXY_APPL_VAR) RxBufferSizePtr)      
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;    /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* If RxTpConnectionState is RECEPTION_STARTED, set RxTpConnectionState to COPYING */
  if(vRpcProxy_GetRxTpPduStateOfRxTpInfo(sourceHandleId) == VRPCPROXY_RECEPTION_STARTED_RXTPPDUSTATEOFRXTPINFO)
  {
    vRpcProxy_SetRxTpPduStateOfRxTpInfo(sourceHandleId, VRPCPROXY_COPYING_RXTPPDUSTATEOFRXTPINFO);
  }

  switch (vRpcProxy_GetRxTpPduStateOfRxTpInfo(sourceHandleId))
  {
      /* If RxTpConnectionState is COPYING */
    case VRPCPROXY_COPYING_RXTPPDUSTATEOFRXTPINFO:
    {
      /* If segment fits into buffer */
      if(PduInfoPointer->SduLength <= (vRpcProxy_GetRxTpSduLengthOfRxTpInfo(sourceHandleId) - vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId)))
      {                
        /* If data shall be copied */
        if((PduInfoPointer->SduLength > 0u) && (PduInfoPointer->SduDataPtr != NULL_PTR))
        {
          /* Copy the passed bytes to the buffer, update the written bytes counter and set the passed RxBufferSizePtr value to the remaining receive buffer size */
          PduLengthType remainingBufferLength = (PduLengthType)vRpcProxy_GetRxTpBufferLengthOfSources(sourceHandleId) - (PduLengthType)vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId);
          VStdLib_MemCpy_s(vRpcProxy_GetAddrRxTpBuffer(vRpcProxy_GetRxTpBufferStartIdxOfSources(sourceHandleId) + vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId)), remainingBufferLength, PduInfoPointer->SduDataPtr, PduInfoPointer->SduLength); /* PRQA S 0315, 2986 */ /* MD_MSR_VStdLibCopy, MD_vRpcProxy_2986 */  
          vRpcProxy_SetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId, (vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId) + PduInfoPointer->SduLength));
        } 
        *RxBufferSizePtr = (vRpcProxy_GetRxTpBufferLengthOfSources(sourceHandleId) - vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId));  

        /* If the expected amount of bytes is received, set Rx Tp vRpcProxyIPdu state to WAITING_FOR_INDICATION */
        if(vRpcProxy_GetRxTpSduLengthOfRxTpInfo(sourceHandleId) == vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId))
        {
          if(vRpcProxy_GetRxTpWrittenBytesCounterOfRxTpInfo(sourceHandleId) < 2u)
          {
            /* Reset Tx Tp state machine if less than 2 bytes were received as the received data shall at least contain the service Id */
            vRpcProxy_ErrorManager_RxTpConnectionError(sourceHandleId);
          }
          else
          {
            vRpcProxy_SetRxTpPduStateOfRxTpInfo(sourceHandleId, VRPCPROXY_WAITING_FOR_INDICATION_RXTPPDUSTATEOFRXTPINFO);
          }     
        }
        retVal = BUFREQ_OK;             
      }
      break;
    }
      /* Else-If RxTpConnectionState is WAITING_FOR_INDICATION */
    case VRPCPROXY_WAITING_FOR_INDICATION_RXTPPDUSTATEOFRXTPINFO:
    {
      /* Reset state machine */ 
      SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
      SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      break;
    }
      /* Else do nothing */
    default:
    {
      /* READY - Do nothing */
      break;
    }
  }
 
  VRPCPROXY_DUMMY_STATEMENT(RxBufferSizePtr); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
 
  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_TpManager_CopyRxData_Processing */

/**********************************************************************************************************************
 *  vRpcProxy_TpManager_TpRxIndication_Processing()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_TpManager_TpRxIndication_Processing(vRpcProxy_SourcesIterType sourceHandleId, Std_ReturnType result)
{
  switch (vRpcProxy_GetRxTpPduStateOfRxTpInfo(sourceHandleId))
  {
    /* If RXTPCONNECTIONSTATE is COPYING or RECEPTION_STARTED, reset Tp connection. */
    case VRPCPROXY_RECEPTION_STARTED_RXTPPDUSTATEOFRXTPINFO:
    case VRPCPROXY_COPYING_RXTPPDUSTATEOFRXTPINFO:
    {
      SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
      SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
      break;
    }
    /* If RXTPCONNECTIONSTATE is WAITING_FOR_INDICATION */
    case VRPCPROXY_WAITING_FOR_INDICATION_RXTPPDUSTATEOFRXTPINFO:
    {
      /* If the passed result is negative, reset TP connection. */
      if(result != E_OK)
      {
        vRpcProxy_ErrorManager_RxTpConnectionError(sourceHandleId);
      }
      /* Otherwise the Rx TP vRpcProxyIPdu reception was successful */
      else
      {
        /* Initiate processing and reset TP and main machine afterwards. */
        vRpcProxy_ServiceManager_RxIndication_ProcessPdu(sourceHandleId); 
        
        SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
        vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
        SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();     
      }
      break;
    }
      /* Do nothing if RxTpConnectionState is READY */
    default:
    {
      /* Do nothing. */
      break;
    }
  } 
  
  VRPCPROXY_DUMMY_STATEMENT(result); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}
/* vRpcProxy_TpManager_TpRxIndication_Processing() */

#define VRPCPROXY_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */   /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_TpManager.c
 *********************************************************************************************************************/
