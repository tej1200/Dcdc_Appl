/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_ServiceManager.c
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL MISRA / PCLINT JUSTIFICATION
 *********************************************************************************************************************/
 
#define VRPCPROXYSERVICEMANAGER_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "vstdlib.h"
#include "SchM_vRpcProxy.h"
#include "vRpcProxy.h"
#include "vRpcProxy_ServiceManager.h"
#include "vRpcProxy_ErrorManager.h"
#include "vRpcProxy_TpManager.h"

#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
# include "Det.h"
#endif
#if(VRPCPROXY_CRC_ENABLED == STD_ON)
# include "Crc.h"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
 
 /**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VRPCPROXY_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 */        /*  MD_MSR_MemMap */ 

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_RxAcknowledgeService()
 **********************************************************************************************************************/
/*! \brief    Receive Acknowledge service
 *   \details     This function is called by RxIndication if the received service is an ack service to
 *                acknowledge the corresponding tx service
 *   \param[in]   sourceHandleId  Id of the Source Pdu
 *   \param[in]   serviceId       Id of the txService to be acknowledged
 *   \context     TASK|ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_RxAcknowledgeService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId);

#if(VRPCPROXY_CRC_ENABLED == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_PerformCrcCheck()
 **********************************************************************************************************************/
/*! \brief    Perform CRC check
 *   \details     -
 *   \param[in]   sourceHandleId  Id of the Source Pdu
 *   \return      uint8
 *                     0: crc matched
 *                     1: crc did not match 
 *   \context     ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(uint8, VRPCPROXY_CODE) vRpcProxy_ServiceManager_PerformCrcCheck(vRpcProxy_SourcesIterType sourceHandleId);
#endif

#if (VRPCPROXY_EXISTS_REQUEST_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxService()
 **********************************************************************************************************************/
/*! \brief    Perform further RxService processing for Request or CCC Services
 *   \details     -
 *   \param[in]   sourceHandleId  Id of the Source Pdu
 *   \param[in]   serviceId       Id of the service
 *   \context     ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceId);
#endif

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessTxAcknowledgeService()
 **********************************************************************************************************************/
/*! \brief    Send or queue Ack Service
 *   \details     -
 *   \param[in]   targetIdx       Id of the target
 *   \param[in]   acknowledge     Value of CRC check
 *   \param[in]   serviceId       If of the service
 *   \context     ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessTxAcknowledgeService(vRpcProxy_CTargetsIterType targetIdx, uint8 acknowledge, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId);

#if (VRPCPROXY_EXISTS_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxResponseService()
 **********************************************************************************************************************/
/*! \brief    Perform further RxService processing for Response
 *   \details     -
 *   \param[in]   sourceHandleId  Id of the Source Pdu
 *   \param[in]   rxServiceId     Id of the Rx service
 *   \context     ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxResponseService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType rxServiceId);
#endif

#if (VRPCPROXY_EXISTS_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus()
 **********************************************************************************************************************/
/*! \brief    Evaluate service execution status
 *   \details     -
 *   \param[in]   txServiceId         Id of the TxService this response was sent for
 *   \param[in]   sourceHandleId      Id of the source Pdu
 *   \param[in]   rxServiceId         Id of the Rx Response Service
 *   \param[in]   targetHandleId      Id of the target Pdu
 *   \context     ISR2
 *   \synchronous TRUE
 *   \reentrant   TRUE, for different Handles
 *   \pre         -
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId, 
    vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType rxServiceId, vRpcProxy_CTargetsIterType targetHandleId);
#endif

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_SerializeTargetSpecificData()
 **********************************************************************************************************************/
/*! \brief    Overwrites Tx Tp Pdu specific data
 *   \details     Overwrites the service call counter in the tx tp Pdu since it differs for every target. 
 *                The Crc is then calculated based on the tx tp pdu and also sesrialized into the Pdu.
 *   \param[in]    serviceId      ID of the service. 
 *   \param[in]    targetId       ID of target Pdu. 
 *   \param[in]    serviceSize    Size of the service.
 *   \context      TASK|ISR
 *   \reentrant    TRUE for different handle IDs
 *   \synchronous  TRUE
 *   \pre          - 
**********************************************************************************************************************/
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeTargetSpecificData(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize);



/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#if (VRPCPROXY_EXISTS_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId, 
    vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType rxServiceId, vRpcProxy_CTargetsIterType targetHandleId)
{
/* ----- Local Variables ---------------------------------------------- */
  uint8 receivedExecutionStatus = VRPCPROXY_INVALID_INTERNALSERVICEEXECUTIONSTATUS;  

/* ----- Implementation ----------------------------------------------- */ 
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(txServiceId, targetHandleId);
  
  /* Deserialize service execution status */
  vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &receivedExecutionStatus);
  
  /* Tx Service State */
  switch(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx))
  {
    case VRPCPROXY_WAITING_FOR_RESPONSE_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION: 

      if(receivedExecutionStatus == VRPCPROXY_SERVICE_PENDING_INTERNALSERVICEEXECUTIONSTATUS)
      {
        /* Reload response timeout counter */
        vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxToutCntRespOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(txServiceId));
      }
      else if(receivedExecutionStatus == VRPCPROXY_SERVICE_FINISHED_INTERNALSERVICEEXECUTIONSTATUS)
      {
        /* Reset state of TxService */
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
        vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
        /* Forward status to upper layer */
        vRpcProxy_GetRxServiceFuncPtrOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(rxServiceId)(sourceHandleId, receivedExecutionStatus, serviceTargetCollectionIdx);
      }
      else
      {
        /* no other Execution state should be received */
      } 
      break;

    case VRPCPROXY_WAITING_FOR_ACK_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION:
    
      /* abort ack timeout tracking */
      vRpcProxy_SetTimeoutCounterAckOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
    
      if(receivedExecutionStatus == VRPCPROXY_SERVICE_PENDING_INTERNALSERVICEEXECUTIONSTATUS)
      {
        /* Reload response timeout counter and set state to WAITING FOR RESPONSE */
        vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxToutCntRespOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes( txServiceId));
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_WAITING_FOR_RESPONSE_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }
      else if(receivedExecutionStatus == VRPCPROXY_SERVICE_FINISHED_INTERNALSERVICEEXECUTIONSTATUS)
      {
        /* Reset state of Txservice */
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
        vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, 0u);
        /* Forward status to upper layer */
        vRpcProxy_GetRxServiceFuncPtrOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(rxServiceId)(sourceHandleId, receivedExecutionStatus, serviceTargetCollectionIdx);
      }
      else
      {
        /* no other Execution state should be received */ 
      } 
      break;
    default:
      /* Silently discard response */
      break;
  }
}
/* vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus */
#endif

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessTxAcknowledgeService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessTxAcknowledgeService(vRpcProxy_CTargetsIterType targetIdx, uint8 acknowledge, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId)
{
  Std_ReturnType retVal;
  
  /* Set values in case the TxAck service fails to be transmitted or receives a negative confirmation */
  vRpcProxy_SetTargetIdOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, (vRpcProxy_TargetIdOfVRxProxyServiceCollectionType) targetIdx);
  vRpcProxy_SetAckResultOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, acknowledge);
  vRpcProxy_SetTxServiceIdToBeAcknowledgedOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, (vRpcProxy_TxServiceIdToBeAcknowledgedOfVRxProxyServiceCollectionType)serviceId);
          
  /* Call TxAcknowledgeService */       
  retVal = vRpcProxy_ServiceManager_TxAcknowledgeService(targetIdx, acknowledge, serviceId);          
        
  /* If the TxAcknowledge Service was busy, save the acknowledge result to send the acknowledge back later */
  if(retVal == E_NOT_OK) 
  {
    vRpcProxy_SetAckFlagOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, TRUE);
  }
}
/* vRpcProxy_ServiceManager_ProcessTxAcknowledgeService */

#if (VRPCPROXY_EXISTS_REQUEST_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 txServiceCallCnt = 0u;
  uint8 acknowledge = 0u; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  vRpcProxy_CTargetsIterType targetIdx = vRpcProxy_GetTargetsIdxOfSources(sourceHandleId);
  vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceSourceIndex = 0u; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* Deserialize the txService call counter */
  vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &txServiceCallCnt);
        
# if(VRPCPROXY_CRC_ENABLED == STD_ON)
  /* Perform crc check */ 
  acknowledge = vRpcProxy_ServiceManager_PerformCrcCheck(sourceHandleId);
# endif

  vRpcProxy_ServiceManager_ProcessTxAcknowledgeService(targetIdx, acknowledge, (vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType) serviceId);
          
  /* Check if acknowledge is positive and tx service call counter differs from internal counter */        
# if(VRPCPROXY_CRC_ENABLED == STD_ON)        
  if(acknowledge == 0u)
# endif
  {
    serviceSourceIndex = vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection(serviceId, sourceHandleId);
    if(txServiceCallCnt != vRpcProxy_GetRxServiceCounterOfRxServiceSourceCollection(serviceSourceIndex))
    { 
      /* Call RxService and set the internal service call counter to the deserialized counter */
      /* parameter VRPCPROXY_INVALID_INTERNALSERVICEEXECUTIONSTATUS and 0u are dummy values. They are only evaluated if the service type is response */ 
      vRpcProxy_GetRxServiceFuncPtrOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId)(sourceHandleId, VRPCPROXY_INVALID_INTERNALSERVICEEXECUTIONSTATUS, 0u);
      vRpcProxy_SetRxServiceCounterOfRxServiceSourceCollection(serviceSourceIndex, txServiceCallCnt);
    }
  }
  VRPCPROXY_DUMMY_STATEMENT(acknowledge);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}
/* vRpcProxy_ServiceManager_ProcessRxService */
#endif

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_RxAcknowledgeService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_RxAcknowledgeService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 result = 0u;
  boolean reportStatus = FALSE;   
  TxService_StatusType resultStatusCallout = VRPCPROXY_E_NEGATIVE_ACK;
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
  
  /* ----- Implementation ----------------------------------------------- */
  vRpcProxy_CTargetsIterType targetHandleId = (vRpcProxy_CTargetsIterType)vRpcProxy_GetTargetsIdxOfSources(sourceHandleId);
  
  /* Get service target collection Id */
  serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(serviceId, targetHandleId);
  
  /* Perform deserialization */
  vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &result);
  
  /* If Acknowledge Service is expected */
  if(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx) == VRPCPROXY_WAITING_FOR_ACK_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION)
  {

    /* If result is E_OK, report via status callout */
    if(result == 0u)
    {
      reportStatus = TRUE;
      resultStatusCallout = VRPCPROXY_E_POSITIVE_ACK;

#if(VRPCPROXY_EXISTS_REQUEST_SERVICETYPEOFCTXPROXYSERVICECOLLECTION == STD_ON)
      if(vRpcProxy_GetServiceTypeOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId) == VRPCPROXY_REQUEST_SERVICETYPEOFCTXPROXYSERVICECOLLECTION)
      {
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_WAITING_FOR_RESPONSE_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
        vRpcProxy_SetTimeoutCounterResponseOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxToutCntRespOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId));
      }
      else
#endif
      {
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }
    }
    else 
    { 
      /* Check if retries are remaining */
      if(vRpcProxy_GetTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx) > 0u)
      {
        vRpcProxy_SetRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx, TRUE);
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_PROCESSING_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }
      else
      {
        reportStatus = TRUE;
        vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
      }
    } 
  }
  
  /* ServiceStatusCallout is called if required */
  if(reportStatus == TRUE)
  { 
    vRpcProxy_ServiceManager_ServiceStatusCallout(targetHandleId, serviceId, resultStatusCallout);
  }
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
/* vRpcProxy_ServiceManager_RxAcknowledgeService */

#if (VRPCPROXY_EXISTS_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_ProcessRxResponseService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessRxResponseService(vRpcProxy_SourcesIterType sourceHandleId, vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType rxServiceId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_CTargetsIterType targetHandleId;
  uint8 acknowledge = 0u;  /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint16 txServiceId = 0u;
  uint8 txServiceCallCnt = 0u;
  vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceSourceIndex = 0u; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* ----- Implementation ----------------------------------------------- */
  targetHandleId = vRpcProxy_GetTargetsIdxOfSources(sourceHandleId);
  
  /* Deserialize the txService call counter */
  vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &txServiceCallCnt);
  
  /* Deserialize TxServiceId this response was sent for */
  vRpcProxy_ServiceManager_DeserializeData_uint16(sourceHandleId, &txServiceId);
  
# if(VRPCPROXY_CRC_ENABLED == STD_ON)
  /* Perform crc check */ 
  acknowledge = vRpcProxy_ServiceManager_PerformCrcCheck(sourceHandleId);
# endif

  /* Send acknowledge for response service */
  vRpcProxy_ServiceManager_ProcessTxAcknowledgeService(targetHandleId, acknowledge, (vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType) rxServiceId);
      
  serviceSourceIndex = vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection(rxServiceId, sourceHandleId);    
  if((txServiceCallCnt != vRpcProxy_GetRxServiceCounterOfRxServiceSourceCollection(serviceSourceIndex))
# if(VRPCPROXY_CRC_ENABLED == STD_ON)  
    && (acknowledge == 0u)
# endif
  )
  {
    /* Evaluate service execution status */
    vRpcProxy_ServiceManager_ProcessRxResponseService_EvaluateStatus((vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType) txServiceId, sourceHandleId, rxServiceId, targetHandleId);
    vRpcProxy_SetRxServiceCounterOfRxServiceSourceCollection(serviceSourceIndex, txServiceCallCnt);
  }
}
/* vRpcProxy_ServiceManager_ProcessRxResponseService */
#endif

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vRpcProxy_CTxServiceTargetCollectionIterType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetHandleId)
{
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx = vRpcProxy_GetTxServiceTargetCollectionStartIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId);
  
  for(; serviceTargetCollectionIdx < vRpcProxy_GetTxServiceTargetCollectionEndIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId); serviceTargetCollectionIdx++)
  {
    if(vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx) == targetHandleId)
    {
      break;
    }
  }
  return serviceTargetCollectionIdx;
}
/* vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection */

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(vRpcProxy_CRxServiceSourceCollectionIterType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection(vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_SourcesIterType sourceHandleId)
{
  vRpcProxy_CRxServiceSourceCollectionIterType serviceSourceCollectionIdx = vRpcProxy_GetRxServiceSourceCollectionStartIdxOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId);
  
  for(; serviceSourceCollectionIdx < vRpcProxy_GetRxServiceSourceCollectionEndIdxOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId); serviceSourceCollectionIdx++)
  {
    if(vRpcProxy_GetSourceIdOfRxServiceSourceCollection(serviceSourceCollectionIdx) == sourceHandleId)
    {
      break;
    }
  }
  return serviceSourceCollectionIdx;
}
/* vRpcProxy_ServiceManager_GetIndexOfServiceSourceCollection */


# if(VRPCPROXY_CRC_ENABLED == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_PerformCrcCheck()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(uint8, VRPCPROXY_CODE) vRpcProxy_ServiceManager_PerformCrcCheck(vRpcProxy_SourcesIterType sourceHandleId)
{
  uint8 retVal = 0u;
  uint8 copyByte;
  uint8 iterVal = 0u;
  uint32 localCrcValue;
  uint32 remoteCrcValue = 0u;  
  
  vRpcProxy_RxTpBufferReadIdxOfRxTpInfoType crcIndex = vRpcProxy_GetRxTpBufferStartIdxOfSources(sourceHandleId)
                                                       + (vRpcProxy_RxTpBufferReadIdxOfRxTpInfoType)vRpcProxy_GetRxTpSduLengthOfRxTpInfo(sourceHandleId) /* PRQA S 2986 */ /* MD_vRpcProxy_2986 */
                                                       - (vRpcProxy_RxTpBufferReadIdxOfRxTpInfoType)VRPCPROXY_CRC_SIZE;
   
  localCrcValue = Crc_CalculateCRC32((Crc_DataRefType)vRpcProxy_GetAddrRxTpBuffer(vRpcProxy_GetRxTpBufferStartIdxOfSources(sourceHandleId)), 
                                     (uint32)vRpcProxy_GetRxTpSduLengthOfRxTpInfo(sourceHandleId) 
                                     - (uint32)VRPCPROXY_CRC_SIZE, 0u, TRUE);
   
  for(; iterVal < VRPCPROXY_CRC_SIZE; iterVal++)
  {
    copyByte = (uint8)vRpcProxy_GetRxTpBuffer(crcIndex + (vRpcProxy_RxTpBufferReadIdxOfRxTpInfoType)iterVal); 
    remoteCrcValue = (uint32)((remoteCrcValue << 8u) & 4294967040u); /* PRQA S 2985 */ /* MD_vRpcProxy_2985 */
    remoteCrcValue = remoteCrcValue + copyByte;
  }    
  if(localCrcValue != remoteCrcValue)
  {
    retVal = 1u;
  }
  return retVal;
}
/* vRpcProxy_ServiceManager_PerformCrcCheck */
#endif

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_TxAcknowledgeService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VRPCPROXY_CODE) vRpcProxy_ServiceManager_TxAcknowledgeService(vRpcProxy_CTargetsIterType targetHandleId, uint8 result, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType txServiceId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType ackServiceId = 0u;
  vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize = 5u;
  vRpcProxy_TargetGroupsIterType targetGroupId;
  Std_ReturnType retVal = E_NOT_OK;
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;

  /* ----- Implementation ----------------------------------------------- */
     
  serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(ackServiceId, targetHandleId);
     
  /* If txService is in READY state */
  if(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx) == VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION)
  { 
    /* Set TxService State to PROCESSING */
    vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_PROCESSING_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
    vRpcProxy_ServiceManager_SetServiceBufferWriteIdxToStartIdxOfService((vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType)ackServiceId);
    
    /* Serialize Data with txService Id that shall be acknowledged. */
    vRpcProxy_ServiceManager_SerializeData_uint16(ackServiceId, (uint16)ackServiceId);
    vRpcProxy_ServiceManager_SerializeData_uint16(ackServiceId, (uint16)txServiceId);
    vRpcProxy_ServiceManager_SerializeData_uint8(ackServiceId, result);
    
    targetGroupId = vRpcProxy_GetTargetGroupsIdxOfTargets(targetHandleId);
    vRpcProxy_SetTxServiceDeferredTargetGroupOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(ackServiceId, (vRpcProxy_TxServiceDeferredTargetGroupOfVTxProxyServiceCollectionType)targetGroupId);
    vRpcProxy_SetTxServiceSizeOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(ackServiceId, serviceSize);
    
    /* If target is in READY state */
    if(vRpcProxy_GetTxTpPduStateOfTxTpInfo(targetHandleId) == VRPCPROXY_READY_TXTPPDUSTATEOFTXTPINFO)
    {        
      /* Process service call */      
      vRpcProxy_ServiceManager_ProcessTxServiceCall(ackServiceId, (vRpcProxy_TxServiceDeferredTargetGroupOfVTxProxyServiceCollectionType)targetGroupId, serviceSize);
    }
    else
    {
      /* Else, Set Flag for deferred processsing */
      vRpcProxy_SetTxServiceDeferredProcessingOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(ackServiceId, TRUE);
    }
   
    retVal = E_OK;
  }

  return retVal;
}
/* vRpcProxy_ServiceManager_TxAcknowledgeService */

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize)
{
  
  /* Copy Data from the ServiceBuffer to the TxTpBuffer */
  VStdLib_MemCpy_s(vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetId)), vRpcProxy_GetPduLengthOfTargets(targetId), vRpcProxy_GetAddrServiceBuffer(vRpcProxy_GetServiceBufferStartIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId)), serviceSize); /* PRQA S 0315, 2842 */ /* MD_MSR_VStdLibCopy, MD_vRpcProxy_2842 */
  
  /* Overwrite the dummy values of service call counter and the crc since they are target/pdu specific */ 
  vRpcProxy_ServiceManager_SerializeTargetSpecificData(serviceId, targetId, serviceSize);

  /* Set TxTpSduLength */
  vRpcProxy_SetTxTpSduLengthOfTxTpInfo(targetId, (vRpcProxy_TxTpSduLengthOfTxTpInfoType) serviceSize);
}
/* vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer() */

/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_CheckEachTpConnectionReady
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CheckEachTpConnectionReady(vRpcProxy_TargetGroupsIterType targetGroupHandleId)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TargetsIndStartIdxOfTargetGroupsType targetStartIdx = vRpcProxy_GetTargetsIndStartIdxOfTargetGroups(targetGroupHandleId);
  boolean eachTargetReady = FALSE; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
  /* #10 Iterate over all referenced targets and check if the correct state is set */
  for(;targetStartIdx < vRpcProxy_GetTargetsIndEndIdxOfTargetGroups(targetGroupHandleId); targetStartIdx++)
  {
    if(vRpcProxy_GetTxTpPduStateOfTxTpInfo(vRpcProxy_GetTargetsInd(targetStartIdx)) == VRPCPROXY_READY_TXTPPDUSTATEOFTXTPINFO)
    {
      eachTargetReady = TRUE;
    }
    else
    {
      eachTargetReady = FALSE;
      break;
    }
  }
  VRPCPROXY_DUMMY_STATEMENT(targetGroupHandleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  
  return eachTargetReady;
} /* vRpcProxy_ServiceManager_CheckEachTpConnectionReady */


/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_CheckEachServiceTargetReady
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_CheckEachServiceTargetReady(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean eachServiceTargetReady = FALSE;
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx = vRpcProxy_GetTxServiceTargetCollectionStartIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId);
  
  for(; serviceTargetCollectionIdx < vRpcProxy_GetTxServiceTargetCollectionEndIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId); serviceTargetCollectionIdx++)
  {
    if(vRpcProxy_GetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx) == VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION)
    {
      eachServiceTargetReady = TRUE;
    }
    else
    {
      eachServiceTargetReady = FALSE;
      break;
    }
  }
  return eachServiceTargetReady;
} /* vRpcProxy_ServiceManager_CheckEachServiceTargetReady */


/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_SetAllServiceTargetStatesOfTargetGroup
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 **********************************************************************************************************************/
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SetAllServiceTargetStatesOfTargetGroup(vRpcProxy_TargetGroupsIterType targetGroupHandleId, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_TxServiceStateOfTxServiceTargetCollectionType txServiceTargetState)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TargetsIndStartIdxOfTargetGroupsType targetStartIdx = vRpcProxy_GetTargetsIndStartIdxOfTargetGroups(targetGroupHandleId);
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
  
  /* Iterate over all referenced targets and check if the correct state is set */
  for(;targetStartIdx < vRpcProxy_GetTargetsIndEndIdxOfTargetGroups(targetGroupHandleId); targetStartIdx++)
  {
    serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(serviceId, vRpcProxy_GetTargetsInd(targetStartIdx));
    vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, txServiceTargetState);
  }
  VRPCPROXY_DUMMY_STATEMENT(targetGroupHandleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
} /* vRpcProxy_ServiceManager_SetAllServiceTargetStatesOfTargetGroup */


# if(VRPCPROXY_RESPONSEFILTERTIMERVALUE == STD_ON)
/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_IsFilteringOfTxResponseRequired
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 **********************************************************************************************************************/
FUNC(boolean, VRPCPROXY_CODE) vRpcProxy_ServiceManager_IsFilteringOfTxResponseRequired(uint16 responseOfServiceId, uint8 serviceExecutionStatus)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean filteringRequired = FALSE;
  
  if((serviceExecutionStatus == VRPCPROXY_SERVICE_PENDING_INTERNALSERVICEEXECUTIONSTATUS) &&
     (vRpcProxy_IsResponseSentInCurrentTimeWindowOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(responseOfServiceId) == TRUE))
  {
    filteringRequired = TRUE;
  }       
  return filteringRequired;
} /* vRpcProxy_ServiceManager_IsFilteringOfTxResponseRequired */
#endif

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_ServiceStatusCallout()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ServiceStatusCallout(vRpcProxy_CTargetsIterType targetIdx, vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, TxService_StatusType result)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TargetGroupsIterType pseudoTargetGroupIdx = vRpcProxy_GetTargetGroupsIdxOfTargets(targetIdx);
  vRpcProxy_TargetGroupsIterType targetGroupId = vRpcProxy_GetTxServiceDeferredTargetGroupOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId);
  
  /* If the saved targetGroupId is a pseudoGroup, then set the targetGroupId to an unused value */
  if(targetGroupId == pseudoTargetGroupIdx)
  {
    targetGroupId = VRPCPROXY_UNUSED_TARGET_GROUP_ID;
  }
  
  /* Status callout is only called if it is no acknowledge service */
  if(vRpcProxy_IsStatusCoutFuncFromCTxProxyServiceCollectionUsedOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId))
  {
    vRpcProxy_GetStatusCoutFuncOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId)((uint16)serviceId, (uint8)targetIdx, (uint8)targetGroupId, result);
  }  
}
/* vRpcProxy_ServiceManager_ServiceStatusCallout() */

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SetServiceBufferWriteIdxToStartIdxOfService()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SetServiceBufferWriteIdxToStartIdxOfService(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId)
{
  /* Set ServiceQueueWriteIdx to the BufferStartIdx of the Service */
  vRpcProxy_SetServiceBufferWriteIdxOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId, vRpcProxy_GetServiceBufferStartIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId));
}

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint8 Data)
{
  /* Copy Data to the ServiceBuffer */
  if(vRpcProxy_GetServiceBufferWriteIdxOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId) < vRpcProxy_GetServiceBufferEndIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId))
  {
    vRpcProxy_SetServiceBuffer(vRpcProxy_GetServiceBufferWriteIdxOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId), Data);
    vRpcProxy_IncServiceBufferWriteIdxOfVTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId);
  }
}

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VRPCPROXY_E_NO_ERROR;                 /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (VRPCPROXY_DEV_ERROR_DETECT == STD_ON)  
  if(parameter == NULL_PTR)
  {
    errorId = VRPCPROXY_E_PARAM_POINTER;
  }
  else
#endif
  {
    /* Copy Data to the parameter */
    *parameter = (uint8)(vRpcProxy_GetRxTpBuffer(vRpcProxy_GetRxTpBufferReadIdxOfRxTpInfo(sourceHandleId))); /* PRQA S 2842 */ /* MD_vRpcProxy_2842 */
    vRpcProxy_IncRxTpBufferReadIdxOfRxTpInfo(sourceHandleId); /* PRQA S 2842 */ /* MD_vRpcProxy_2842_CSL_Index */
  }
  /* ----- Development Error Report --------------------------------------- */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if(errorId != VRPCPROXY_E_NO_ERROR) 
  {
    vRpcProxy_Det_ReportError(VRPCPROXY_APIID_TPRXINDICATION, errorId);
  }
#endif
  VRPCPROXY_DUMMY_STATEMENT(errorId);                 /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}

#if ((VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_PRIMITIVETXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_boolean()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_boolean(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, boolean Data)
{
  vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, (uint8) Data); /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
}
#endif

#if ((VRPCPROXY_EXISTS_BOOLEAN_ARRAYRXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_PRIMITIVERXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_boolean()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_boolean(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{   
  /* Copy Data to the parameter */
  *parameter = (uint8)(vRpcProxy_GetRxTpBuffer(vRpcProxy_GetRxTpBufferReadIdxOfRxTpInfo(sourceHandleId))); /* PRQA S 2842, 4440 */ /* MD_vRpcProxy_2842, MD_MSR_AutosarBoolean */
  vRpcProxy_IncRxTpBufferReadIdxOfRxTpInfo(sourceHandleId); /* PRQA S 2842 */ /* MD_vRpcProxy_2842_CSL_Index */
}
#endif
 
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint16 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;
 
  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 2u; iterVal++)
  {
    copyByte = (uint8)((Data >> ((1u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint16 init = 0u;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
    
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 2u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (init << 8u) & 0xFF00u;
    init = init + copyByte;
  }
  *parameter = init;
}

#if((VRPCPROXY_EXISTS_UINT32_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT32_ARRAYTXPARAMETERTYPE == STD_ON) || (VRPCPROXY_CRC_ENABLED == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint32 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;

  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 4u; iterVal++)
  {
    copyByte = (uint8)((Data >> ((3u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if((VRPCPROXY_EXISTS_UINT32_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT32_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint32 init = 0u;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
 
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 4u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (init << 8u) & 0xFFFFFF00u; /* PRQA S 2985 */ /* MD_vRpcProxy_2985 */
    init = init + copyByte;
  }
  *parameter = init;
}
#endif

#if((VRPCPROXY_EXISTS_UINT64_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT64_ARRAYTXPARAMETERTYPE))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_uint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_uint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, uint64 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;

  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 8u; iterVal++)
  {
    copyByte = (uint8)((Data >> ((7u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if((VRPCPROXY_EXISTS_UINT64_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_UINT64_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_uint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_uint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 init = 0u;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
  
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 8u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (init << 8u) & 0xFFFFFFFFFFFFFF00u; /* PRQA S 2985 */ /* MD_vRpcProxy_2985 */
    init = init + copyByte;
  }
  *parameter = init;
}
#endif

#if((VRPCPROXY_EXISTS_SINT8_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT8_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint8 Data)
{
  /* Copy Data bytewise to the buffer */  
  vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, (uint8) Data);
}
#endif

#if((VRPCPROXY_EXISTS_SINT8_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT8_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  /* Copy Data bytewise to the parameter */
  vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
  *parameter = (sint8) copyByte;
}
#endif

#if((VRPCPROXY_EXISTS_SINT16_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT16_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint16 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;
  
  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 2u; iterVal++)
  {
    copyByte = (uint8)(((uint16) Data >> ((1u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if((VRPCPROXY_EXISTS_SINT16_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT16_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint16 init = 0;
  uint8 copyByte;
  uint8 iterVal = 0u;
  
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 2u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (sint16) ((((uint16) init) << 8u) & 0xFF00u); /* PRQA S 2985, 4394 */ /* MD_vRpcProxy_2985, MD_vRpcProxy_4394 */
    init = init + (sint16) copyByte;
  }
  *parameter = init;
}
#endif

#if((VRPCPROXY_EXISTS_SINT32_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT32_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint32 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;
    
  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 4u; iterVal++)
  {
    copyByte = (uint8)(((uint32) Data >> ((3u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if((VRPCPROXY_EXISTS_SINT32_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT32_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint32 init = 0;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
  
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 4u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (sint32)(((uint32)(init) << 8u) & 0xFFFFFF00u); /* PRQA S 2985, 4394 */ /* MD_vRpcProxy_2985, MD_vRpcProxy_4394 */
    init = init + (sint32)copyByte;
  }
  *parameter = init;
}
#endif

#if((VRPCPROXY_EXISTS_SINT64_PRIMITIVETXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT64_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeData_sint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeData_sint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, sint64 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint8 iterVal = 0u;

  /* Copy Data bytewise to the buffer */    
  for(; iterVal < 8u; iterVal++)
  {
    copyByte = (uint8)(((uint64) Data >> ((7u-iterVal)*8u)) & 0xFFu);
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if((VRPCPROXY_EXISTS_SINT64_PRIMITIVERXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_SINT64_ARRAYRXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeData_sint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeData_sint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) parameter)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint64 init = 0;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
  
  /* Copy Data bytewise to the parameter */
  for(; iterVal < 8u; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    init = (sint64)(((uint64) (init) << 8u) & 0xFFFFFFFFFFFFFF00u); /* PRQA S 2985, 4394 */ /* MD_vRpcProxy_2985, MD_vRpcProxy_4394 */
    init = init + (sint64)copyByte;
  }
  *parameter = init;
}
#endif

#if((VRPCPROXY_EXISTS_UINT8_ARRAYTXPARAMETERTYPE == STD_ON) || (VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON))
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_uint8(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT8_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  uint8 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint8(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_BOOLEAN_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_boolean()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_boolean(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  vRpcProxy_ServiceManager_SerializeArray_uint8(serviceId, (P2CONST(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR)) array, arrayLength);
} 
#endif

#if(VRPCPROXY_EXISTS_BOOLEAN_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_boolean()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_boolean(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(boolean, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  boolean copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_boolean(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT16_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint16 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_uint16(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT16_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  uint16 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint16(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT32_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint32 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_uint32(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT32_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  uint32 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint32(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT64_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_uint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_uint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_uint64(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_UINT64_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_uint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_uint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(uint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  uint64 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_uint64(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT8_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint8(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint8 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_sint8(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT8_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint8()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint8(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint8, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  sint8 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_sint8(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT16_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint16(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint16 copyByte;
  uint64 iterVal = 0u;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_sint16(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT16_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint16()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint16(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint16, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  sint16 copyByte = 0;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_sint16(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT32_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint32(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint32 copyByte;
  uint64 iterVal = 0u;
  
  /* Iterate over all array elements and copy each array element bytewise */
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_sint32(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT32_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint32()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint32(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint32, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  sint32 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_sint32(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT64_ARRAYTXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeArray_sint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeArray_sint64(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, P2CONST(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  sint64 copyByte;
  uint64 iterVal = 0u;
 
  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    copyByte = array[iterVal]; 
    vRpcProxy_ServiceManager_SerializeData_sint64(serviceId, copyByte);
  }
}
#endif

#if(VRPCPROXY_EXISTS_SINT64_ARRAYRXPARAMETERTYPE == STD_ON)
/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_DeserializeArray_sint64()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_DeserializeArray_sint64(vRpcProxy_SourcesIterType sourceHandleId, P2VAR(sint64, AUTOMATIC, VRPCPROXY_APPL_VAR) array, uint64 arrayLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint64 iterVal = 0u;
  sint64 copyByte;

  /* Iterate over all array elements and copy each array element bytewise */  
  for(; iterVal < arrayLength; iterVal++)
  {
    vRpcProxy_ServiceManager_DeserializeData_sint64(sourceHandleId, &copyByte);
    array[iterVal] = copyByte;
  }
}
#endif

/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_ProcessTxServiceCall()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_ProcessTxServiceCall(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_TargetGroupsIterType targetGroupId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize)
{
  /* ----- Local Variables ---------------------------------------------- */
  vRpcProxy_TargetsIndIterType targetsIndIdx = vRpcProxy_GetTargetsIndStartIdxOfTargetGroups(targetGroupId);
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
   
  /* Iterate over all targets in the target group */
  for(; targetsIndIdx < vRpcProxy_GetTargetsIndEndIdxOfTargetGroups(targetGroupId); targetsIndIdx++)
  {
    vRpcProxy_CTargetsIterType targetIdx = vRpcProxy_GetTargetsInd(targetsIndIdx);
    
    /* Set the state of the service to TRANSMISSION_STARTED  */
    vRpcProxy_SetTxTpPduStateOfTxTpInfo(targetIdx, VRPCPROXY_TRANSMISSION_STARTED_TXTPPDUSTATEOFTXTPINFO);  
       
    /* Set last txService sent and reset retry counter */
    serviceTargetCollectionIdx = vRpcProxy_GetTxServiceTargetCollectionStartIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId);
    for(;serviceTargetCollectionIdx < vRpcProxy_GetTxServiceTargetCollectionEndIdxOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId); serviceTargetCollectionIdx++)
    {
      if(targetIdx == vRpcProxy_GetTargetIdOfTxServiceTargetCollection(serviceTargetCollectionIdx)) /* PRQA S 2991, 2995 */ /* MD_vRpcProxy_2991, MD_vRpcProxy_2995 */
      {
        vRpcProxy_SetLastTxServiceSentOfTargets(targetIdx, (vRpcProxy_LastTxServiceSentOfTargetsType) serviceId);
        vRpcProxy_SetTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx, vRpcProxy_GetMaxRetryCountOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId));
        break;
      }
    }
    /* Copy data to according TxTpBuffer */
    vRpcProxy_ServiceManager_CopyDataFromServiceBufferToTxTpBuffer(serviceId, targetIdx, serviceSize);
    
    /* Call vRpcProxy_TpManager_TransmitTpFrame() for each target */
    vRpcProxy_TpManager_TransmitTpFrame(targetIdx);
  }
  VRPCPROXY_DUMMY_STATEMENT(targetGroupId);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}
/* vRpcProxy_ServiceManager_ProcessTxServiceCall */


/**********************************************************************************************************************
 * vRpcProxy_ServiceManager_SerializeTargetSpecificData()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 */
VRPCPROXY_LOCAL_INLINE FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_SerializeTargetSpecificData(vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId, vRpcProxy_CTargetsIterType targetId, vRpcProxy_TxServiceSizeOfVTxProxyServiceCollectionType serviceSize)
{
  /* ----- Local Variables ---------------------------------------------- */

  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx = 0u;
#if(VRPCPROXY_CRC_ENABLED == STD_ON)
  uint32 crcValue = 0u;
  uint8 copyByte = 0u;
  uint8 iterVal = 0u;
#endif  

  /* Ack Pdu does not have a callcounter or crc */
  if(vRpcProxy_GetServiceTypeOfCTxProxyServiceCollectionOfTxProxyServiceCollectionWithInvalidIndexes(serviceId) != VRPCPROXY_ACK_SERVICETYPEOFCTXPROXYSERVICECOLLECTION)  /* PRQA S 2842  */ /* MD_vRpcProxy_2842_CSL_Index */ 
  {
    serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(serviceId, targetId);
    /* The callcounter is the second element of the pdu and is positioned after the service Id */
    /* Endianness is irrelevant since VRPCPROXY_SERVICECALLCOUNT_SIZE is 1 Byte*/
    VStdLib_MemCpy(vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetId) + VRPCPROXY_SERVICEID_SIZE), vRpcProxy_GetAddrTxServiceCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx), VRPCPROXY_SERVICECALLCOUNT_SIZE); /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */

#if(VRPCPROXY_CRC_ENABLED == STD_ON)
    crcValue = Crc_CalculateCRC32((Crc_DataRefType)vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetId)), (uint32)(serviceSize) - (uint32)(VRPCPROXY_CRC_SIZE), 0u, TRUE);
    /* The crc is the last element of the pdu. Do not change the Pdu content after crc calculation */
    /* Copy crcValue bytewise to the buffer */    
    for(; iterVal < 4u; iterVal++)  
    {
      copyByte = (uint8)(((uint32)crcValue >> ((3u-iterVal)*8u)) & 255u);
      VStdLib_MemCpy(vRpcProxy_GetAddrTxTpBuffer(vRpcProxy_GetTxTpBufferStartIdxOfTargets(targetId) + serviceSize - VRPCPROXY_CRC_SIZE + iterVal), &copyByte , 1u); /* PRQA S 0315, 2986 */ /* MD_MSR_VStdLibCopy, MD_vRpcProxy_2986 */
    }    
#endif   
  }   
  
  
  VRPCPROXY_DUMMY_STATEMENT(serviceTargetCollectionIdx);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(serviceSize);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(targetId);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
#if(VRPCPROXY_CRC_ENABLED == STD_ON)
  VRPCPROXY_DUMMY_STATEMENT(crcValue);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(copyByte);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VRPCPROXY_DUMMY_STATEMENT(iterVal);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
#endif 

}


/**********************************************************************************************************************
 *  vRpcProxy_ServiceManager_RxIndication_ProcessPdu()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ServiceManager_RxIndication_ProcessPdu(vRpcProxy_SourcesIterType sourceHandleId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint16 serviceId = 0u;
  uint16 txServiceId = 0u;
    
  vRpcProxy_SetRxTpBufferReadIdxOfRxTpInfo(sourceHandleId, vRpcProxy_GetRxTpBufferStartIdxOfSources(sourceHandleId));
  
  /* Deserialize the serviceId */
  vRpcProxy_ServiceManager_DeserializeData_uint16(sourceHandleId, &serviceId);
    
  if((serviceId < vRpcProxy_GetSizeOfRxProxyServiceCollectionWithInvalidIndexes())
#if(VRPCPROXY_INVALIDHNDOFRXPROXYSERVICECOLLECTIONWITHINVALIDINDEXES == STD_ON)
      && (!vRpcProxy_IsInvalidHndOfRxProxyServiceCollectionWithInvalidIndexes(serviceId))
#endif
    )
    {
      /* Check if the rx service state is READY */
      if(vRpcProxy_GetRxServiceStateOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId) == VRPCPROXY_READY_RXSERVICESTATEOFVRXPROXYSERVICECOLLECTION)
      {
        vRpcProxy_SetRxServiceStateOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, VRPCPROXY_PROCESSING_RXSERVICESTATEOFVRXPROXYSERVICECOLLECTION);
        
        /* Check type of received service and perform further processing */
        switch(vRpcProxy_GetServiceTypeOfCRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId))
        {
          case VRPCPROXY_ACK_SERVICETYPEOFCRXPROXYSERVICECOLLECTION:
          {
            vRpcProxy_ServiceManager_DeserializeData_uint16(sourceHandleId, &txServiceId);
            vRpcProxy_ServiceManager_RxAcknowledgeService(sourceHandleId, (vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType) txServiceId);
            vRpcProxy_SetRxServiceStateOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, VRPCPROXY_READY_RXSERVICESTATEOFVRXPROXYSERVICECOLLECTION);
            break;
          }
#if (VRPCPROXY_EXISTS_REQUEST_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
          case VRPCPROXY_REQUEST_SERVICETYPEOFCRXPROXYSERVICECOLLECTION:
          {
            vRpcProxy_ServiceManager_ProcessRxService(sourceHandleId, (vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType) serviceId);
            /* rx service state of request is reset to ready by the corresponding response  */
            break;
          }
#endif
#if (VRPCPROXY_EXISTS_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION == STD_ON)
          case VRPCPROXY_RESPONSE_SERVICETYPEOFCRXPROXYSERVICECOLLECTION:
          {
            vRpcProxy_ServiceManager_ProcessRxResponseService(sourceHandleId, (vRpcProxy_RxProxyServiceCollectionWithInvalidIndexesIterType) serviceId);
            vRpcProxy_SetRxServiceStateOfVRxProxyServiceCollectionOfRxProxyServiceCollectionWithInvalidIndexes(serviceId, VRPCPROXY_READY_RXSERVICESTATEOFVRXPROXYSERVICECOLLECTION);
            break;
          }
#endif
          default:
          {
            /* Do nothing */
            break;
          }
        }
      }
    }
    /* Unknown service error handling */
    else
    {
      vRpcProxy_ErrorManager_RemoteProxyServiceUnknown(sourceHandleId);
    }
    
  VRPCPROXY_DUMMY_STATEMENT(serviceId);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
 }/* PRQA S 6080 */ /* MD_MSR_STMIF */
 /* vRpcProxy_ServiceManager_RxIndication_ProcessPdu */ 


#define VRPCPROXY_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */   /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_ServiceManager.c
 *********************************************************************************************************************/
