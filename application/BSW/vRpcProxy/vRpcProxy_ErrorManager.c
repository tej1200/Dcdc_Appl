/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  vRpcProxy_ErrorManager.c
 *      Project:  Ipc_vRpcProxy
 *       Module:  MICROSAR vRpcProxy
 *    Generator:  CFG5
 *
 *  Description:  Vector implementation of MICROSAR vRpcProxy
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL MISRA / PCLINT JUSTIFICATION
 *********************************************************************************************************************/
 
#define VRPCPROXYERRORMANAGER_SOURCE
 
/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "SchM_vRpcProxy.h"
#include "vRpcProxy.h"
#include "vRpcProxy_ErrorManager.h"
#include "vRpcProxy_TpManager.h"
#include "vRpcProxy_ServiceManager.h"

#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
 
 /**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/
#define VRPCPROXY_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 */        /*  MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_TxTpConnectionError()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_TxTpConnectionError(vRpcProxy_CTargetsIterType targetHandleId)
{
    /* ----- Local Variables ---------------------------------------------- */
  boolean reportStatus = FALSE;   
  vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType serviceId = (vRpcProxy_TxProxyServiceCollectionWithInvalidIndexesIterType)vRpcProxy_GetLastTxServiceSentOfTargets(targetHandleId); 
  vRpcProxy_CTxServiceTargetCollectionIterType serviceTargetCollectionIdx;
  
  /* Set retry flag if there are retries remaining, otherwise set reportStatus to TRUE */
  serviceTargetCollectionIdx = vRpcProxy_ServiceManager_GetIndexOfServiceTargetCollection(serviceId, targetHandleId);

  if(vRpcProxy_GetTxRetryCounterOfTxServiceTargetCollection(serviceTargetCollectionIdx) > 0u)
  {
    vRpcProxy_SetRetryFlagOfTxServiceTargetCollection(serviceTargetCollectionIdx, TRUE);          
  }
  else
  {
    reportStatus = TRUE;
  }

  /* Reset TpConnection */
  SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
  vRpcProxy_TpManager_ResetTxTpConnection(targetHandleId); 
  SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_TXTPSTATEMACHINE();
  
  /* If reportStatus is set to TRUE, the ServiceStatusCallout is called with VRPCPROXY_E_LOCAL_TX_TP_ERROR */
  if(reportStatus == TRUE)
  {
    vRpcProxy_ServiceManager_ServiceStatusCallout(targetHandleId, serviceId, VRPCPROXY_E_LOCAL_TX_TP_ERROR);
    vRpcProxy_SetTxServiceStateOfTxServiceTargetCollection(serviceTargetCollectionIdx, VRPCPROXY_READY_TXSERVICESTATEOFTXSERVICETARGETCOLLECTION);
  }
}
/* vRpcProxy_ErrorManager_TxTpConnectionError() */

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_RxTpConnectionError()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_RxTpConnectionError(vRpcProxy_SourcesIterType sourceHandleId)
{
  /* Call DET and reset TpConnection */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  vRpcProxy_ReportRuntimeDetError(VRPCPROXY_APIID_SERVICECALL, (VRPCPROXY_E_TP_ERROR));
#endif
  
  SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
  vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
  SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
}
/* vRpcProxy_ErrorManager_RxTpConnectionError() */

/**********************************************************************************************************************
 * vRpcProxy_ErrorManager_RemoteProxyServiceUnknown()
 **********************************************************************************************************************/
/*! Internal comment removed.
 *
 *
 */
FUNC(void, VRPCPROXY_CODE) vRpcProxy_ErrorManager_RemoteProxyServiceUnknown(vRpcProxy_SourcesIterType sourceHandleId)
{
  /* Call DET and reset TpConnection */
#if (VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  vRpcProxy_Det_ReportError(VRPCPROXY_APIID_SERVICECALL, (VRPCPROXY_E_SERVICE_UNKNOWN));
#endif

  SchM_Enter_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
  vRpcProxy_TpManager_ResetRxTpConnection(sourceHandleId);
  SchM_Exit_vRpcProxy_VRPCPROXY_EXCLUSIVE_AREA_RXTPSTATEMACHINE();
}
/* vRpcProxy_ErrorManager_RemoteProxyServiceUnknown() */

#define VRPCPROXY_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */   /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vRpcProxy_ErrorManager.c
 *********************************************************************************************************************/
