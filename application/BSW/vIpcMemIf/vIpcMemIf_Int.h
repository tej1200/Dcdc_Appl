/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 * \file  vIpcMemIf_Int.h
 * \brief Internal header file.
 *
 * \details  Contains the extern declaration of global variables.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *********************************************************************************************************************/

#ifndef VIPCMEMIF_INT_H
# define VIPCMEMIF_INT_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Types.h"
# include "vIpcMemIf_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern P2CONST(vIpcMemIf_ConfigType, VIPCMEMIF_VAR_ZERO_INIT, VIPCMEMIF_PBCFG) vIpcMemIf_ConfigDataPtr;
extern VAR(vIpcMemIf_InitStatusType, VIPCMEMIF_VAR_ZERO_INIT) vIpcMemIf_InitStatus;

# define VIPCMEMIF_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMIF_INT_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Int.h
 *********************************************************************************************************************/
