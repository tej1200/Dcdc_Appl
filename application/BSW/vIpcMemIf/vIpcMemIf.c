/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 * \file  vIpcMemIf.c
 * \brief Implementation of the ServiceFunctions of the component.
 *
 * \details
 * The vIpcMemIf module is the generic interface between the upper layer module vIpc and the operating system dependent
 * lower layer modules to transfer data between two or more instances via shared memory.
 *
 * Instances are usually different CPU cores which can be controlled by different operating systems and may even have
 * different architectures. The requirement is that they operate on the same memory. vIpcMemIf forwards transmission
 * requests from the upper layer to the (operating system specific) lower module and supports different channel states
 * to be able to continue unfinished transmissions or receptions.
 *
 * Each communication channel is unidirectional and consists of a RX and a TX part. The channels are fully independent
 * from each other.
 *
 *
 * Wrapper API functions
 *
 * All API functions are implemented in the unit vIpcMemIf, but the functions implemented here only serve as wrapper
 * functions to the actual implementation. Their job is to call the internal API function and forward the returned error
 * code to the error handling. By that, the error handling can be encapsulated in this unit.
 *
 *
 * Error Reporting
 *
 * Each internal API method returns an vIpcMemIf error code. There are two types of error codes, internal errors and
 * development errors. Internal errors occure due to runtime issues, e.g. when the channel is full or empty. They are no
 * real errors but are treated as errors internally to simplify the call pattern of the internal APIs.
 *
 * The error codes follow a dedicated pattern to distinguish between development errors and internal errors during error
 * handling. The distinguishment is as follows:
 *
 * An error code always consists of 8 bit. The lowest 2 bits are only set for development errors, the bits 3 and 4 only
 * for internal errors. The error code for NO_ERROR is 0xFF. During error handling, it is first checked if an error
 * occured (compare to VIPCMEMIF_E_NO_ERROR). Afterwards it is checked whether the error code is a DET error or not.
 * If it is a DET error, it is forwarded to the Det module.
 *
 * \unit vIpcMemIf_Base
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#define VIPCMEMIF_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "vIpcMemIf.h"
#include "vIpcMemIf_Tx.h"
#include "vIpcMemIf_Rx.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

/* Check the version of vIpcMemIf header file */
#if (  (VIPCMEMIF_SW_MAJOR_VERSION != (1u)) \
    || (VIPCMEMIF_SW_MINOR_VERSION != (1u)) \
    || (VIPCMEMIF_SW_PATCH_VERSION != (0u)) )
# error "Vendor specific version numbers of vIpcMemIf.c and vIpcMemIf.h are inconsistent!"
#endif

/* Check the version of the configuration header file */
#if (  (VIPCMEMIF_CFG_MAJOR_VERSION != (1u)) \
    || (VIPCMEMIF_CFG_MINOR_VERSION != (1u)) )
# error "Version numbers of vIpcMemIf.c and vIpcMemIf_Cfg.h are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
/*! Value used to mark the module as uninitialized. */
# define VIPCMEMIF_NOT_INITIALIZED        (vIpcMemIf_InitStatusType)(FALSE)
/*! Value used to mark the module as initialized. */
# define VIPCMEMIF_INITIALIZED            (vIpcMemIf_InitStatusType)(TRUE)


/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
# if (VIPCMEMIF_DEV_ERROR_REPORT == STD_ON)
#  include "Det.h"
#  define vIpcMemIf_DetReportError(a, b, c, d) ((void) Det_ReportError(a, b, c, d))                                     /* PRQA S 3453 */ /* MD_MSR_19.7 */
# else
#  define vIpcMemIf_DetReportError(a, b, c, d)
# endif

# if (VIPCMEMIF_USE_INIT_POINTER == STD_ON)                                                                             /* COV_VIPCMEMIF_POSTBUILDUNSUPPORTED */
#  include "EcuM_Error.h"
# else
#  define EcuM_BswErrorHook(x, y)
# endif


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

P2CONST(vIpcMemIf_ConfigType, VIPCMEMIF_VAR_ZERO_INIT, VIPCMEMIF_PBCFG) vIpcMemIf_ConfigDataPtr = NULL_PTR;             /* PRQA S 1514 */ /* MD_vIpcMemIf_1514 */
VAR(vIpcMemIf_InitStatusType, VIPCMEMIF_VAR_ZERO_INIT) vIpcMemIf_InitStatus = VIPCMEMIF_NOT_INITIALIZED;

# define VIPCMEMIF_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_UseInitPointer()
 *********************************************************************************************************************/
/*! \brief       Returns whether the usage of init pointer is enabled.
 *  \details     -
 *  \return      TRUE if UseInitPointer is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_UseInitPointer
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_UseEcumBswErrorHook()
 *********************************************************************************************************************/
/*! \brief       Returns whether the usage EcuM Bsw ErrorHook is enabled.
 *  \details     -
 *  \return      TRUE if UseEcumBswErrorHook is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_UseEcumBswErrorHook
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_DevErrorDetect()
 *********************************************************************************************************************/
/*! \brief       Returns whether DevErrorDetect is enabled or not
 *  \details     -
 *  \return      TRUE if DevErrorDetect is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_DevErrorDetect
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_DevErrorReport()
 *********************************************************************************************************************/
/*! \brief       Returns whether DevErrorReport is enabled or not
 *  \details     -
 *  \return      TRUE if DevErrorReport is on, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_DevErrorReport
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_IsInitialized()
 *********************************************************************************************************************/
/*! \brief       Returns if the module was initialized.
 *  \details     -
 *  \return      TRUE if the module was initialized, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_IsInitialized
(
   void
);


/**********************************************************************************************************************
 * vIpcMemIf_ReportError()
 *********************************************************************************************************************/
/*! \brief         Reports an Error to the DET module, if DET Error Reporting is enabled.
 *  \details       -
 *  \param[in]     ApiId                  Id of the API service.
 *  \param[in]     ErrorId                Error code.
 *  \return        E_OK if there was no error, E_NOT_OK otherwise.
 *  \pre           -
 *  \context       ANY
 *  \reentrant     FALSE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(Std_ReturnType, VIPCMEMIF_CODE)
vIpcMemIf_ReportError
(
   vIpcMemIf_ServiceIdType ApiId,
   vIpcMemIf_ErrorCodeType ErrorId
);


/**********************************************************************************************************************
 *  vIpcMemIf_CheckFinalMagicNumber()
 *********************************************************************************************************************/
/*! \brief       Checks the final magic number of the selected post build configuration for validity.
 *  \details     -
 *  \return      TRUE if FinalMagicNumber is valid, FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_CheckFinalMagicNumber
(
   void
);

# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_InitMemory(void)
{
   vIpcMemIf_InitStatus = VIPCMEMIF_NOT_INITIALIZED;
} /* vIpcMemIf_InitMemory */


/**********************************************************************************************************************
 *  vIpcMemIf_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Init(P2CONST(vIpcMemIf_ConfigType, AUTOMATIC, VIPCMEMIF_INIT_DATA) ConfigPtr)
{
   vIpcMemIf_ErrorCodeType status = VIPCMEMIF_E_NO_ERROR;

   /* #10 If UseInitPointer is on: */
   if (vIpcMemIf_UseInitPointer() == TRUE)                                                                              /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemIf_2991, MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996 */
   {
      /* #20 Initialize the global configuration pointer. */
      vIpcMemIf_ConfigDataPtr = ConfigPtr;

      /* #30 Check that the pointer is not null. */
      if (vIpcMemIf_ConfigDataPtr == NULL_PTR)
      {
         if (vIpcMemIf_UseEcumBswErrorHook() == TRUE)
         {
            EcuM_BswErrorHook(VIPCMEMIF_MODULE_ID, ECUM_BSWERROR_NULLPTR);
         }
         status = VIPCMEMIF_E_INV_PTR;
      }
      /* #40 Check final magic number. */
      else if (vIpcMemIf_CheckFinalMagicNumber() == FALSE)
      {
         vIpcMemIf_ConfigDataPtr = NULL_PTR;
         if (vIpcMemIf_UseEcumBswErrorHook() == TRUE)
         {
            EcuM_BswErrorHook(VIPCMEMIF_MODULE_ID, ECUM_BSWERROR_MAGICNUMBER);
         }
         status = VIPCMEMIF_E_INV_PTR;
      }
      else
      {
         /* No action required in any other case. MISRA 14.10 */
      }
   }
   /* #50 Otherwise check that pointer is null. */
   else if (ConfigPtr != NULL_PTR)
   {
      status = VIPCMEMIF_E_INV_PTR;
   }
   else
   {
      /* No action required in any other case. MISRA 14.10 */
   }

   /* #60 If no error has occured: */
   if (status == VIPCMEMIF_E_NO_ERROR)
   {

      /* #70 Initialize the Tx module. */
      vIpcMemIf_Tx_Init();
      /* #80 Initialize the Rx module. */
      vIpcMemIf_Rx_Init();

      /* #90 Set status to initialized. */
      vIpcMemIf_InitStatus = VIPCMEMIF_INITIALIZED;
   }

   /* #100 Report any error. */
   (void) vIpcMemIf_ReportError(VIPCMEMIF_SID_INIT, status);
} /* vIpcMemIf_Init */


/**********************************************************************************************************************
 *  vIpcMemIf_GetVersionInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_GetVersionInfo
(
   P2VAR(Std_VersionInfoType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) VersionInfo
)
{
   vIpcMemIf_ErrorCodeType status = VIPCMEMIF_E_NO_ERROR;
   boolean isDetEnabled = vIpcMemIf_DevErrorDetect();

   /* #10 Check if the pointer is valid. */
   if ((isDetEnabled == TRUE) && (VersionInfo == NULL_PTR))                                                             /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996 */
   {
      status = VIPCMEMIF_E_PARAM;
   }
   else
   {
      /* #20 Set the version info to the values from the component header. */
      VersionInfo->vendorID         = (VIPCMEMIF_VENDOR_ID);
      VersionInfo->moduleID         = (VIPCMEMIF_MODULE_ID);
      VersionInfo->sw_major_version = (VIPCMEMIF_SW_MAJOR_VERSION);
      VersionInfo->sw_minor_version = (VIPCMEMIF_SW_MINOR_VERSION);
      VersionInfo->sw_patch_version = (VIPCMEMIF_SW_PATCH_VERSION);
   }

   /* #30 Report any error. */
   (void) vIpcMemIf_ReportError(VIPCMEMIF_SID_GET_VERSION_INFO, status);
} /* vIpcMemIf_GetVersionInfo */


/**********************************************************************************************************************
 *  vIpcMemIf_Transmit()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMIF_CODE) vIpcMemIf_Transmit(PduIdType ChannelId, const PduInfoType* PduInfoPtr)
{
   /* ----- Local Variables ---------------------------------------------- */
   Std_ReturnType retValue = E_NOT_OK;                                                                                  /* PRQA S 2981 */ /* MD_MSR_RetVal */
   vIpcMemIf_ErrorCodeType errId;
   boolean isDetEnabled = vIpcMemIf_DevErrorDetect();

   /* #10 Check if the ChannelId is valid. */
   if ((isDetEnabled == TRUE) && (ChannelId >= vIpcMemIf_Tx_GetNrOfChannels()))                                         /* PRQA S 2991, 2992, 2995, 2996, 3415 */ /* MD_vIpcMemIf_2991, MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_vIpcMemIf_3415 */
   {
      errId = VIPCMEMIF_E_PARAM;
   }
   /* #20 Check if the pointer is valid. */
   else if ((isDetEnabled == TRUE) && (PduInfoPtr == NULL_PTR))                                                         /* PRQA S 2992, 2995, 2996 */ /* MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996 */
   {
      errId = VIPCMEMIF_E_INV_PTR;
   }
   /* #30 Check if the module is initialized. */
   else if ((isDetEnabled == TRUE) && (vIpcMemIf_IsInitialized() == FALSE))                                             /* PRQA S 2992, 2995, 2996, 3415 */ /* MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_vIpcMemIf_3415 */
   {
      errId = VIPCMEMIF_E_UNINIT;
   }
   else
   {
      errId = VIPCMEMIF_E_NO_ERROR;

      /* #40 Initiate a transmission. */
      retValue = vIpcMemIf_Tx_StartTransmission(ChannelId, PduInfoPtr->SduLength);

      /* #50 If the transmission was initiated successfully: */
      if (retValue == E_OK)
      {
         /* #60 Process the channel. */
         vIpcMemIf_Tx_Process(ChannelId, FALSE);
      }
   }

   /* #70 Report any error and determine final return value. */
   /* The return value can be E_NOT_OK even though no DET error occured (if a transmit could not be started because the
      channel is full). Because of that, the return value is calculated from the return value of the internal transmit
      function and the error reporting function (or operation). */
   retValue |= vIpcMemIf_ReportError(VIPCMEMIF_SID_TRANSMIT, errId);                                                    /* PRQA S 2985 */ /* MD_vIpcMemIf_2985 */

   return retValue;
} /* vIpcMemIf_Transmit */


#if (VIPCMEMIF_RECEIVE_API == STD_ON)
/**********************************************************************************************************************
 *  vIpcMemIf_Receive()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMIF_CODE) vIpcMemIf_Receive(PduIdType ChannelId)
{
   Std_ReturnType retValue;
   vIpcMemIf_ErrorCodeType errId;
   boolean isDetEnabled = vIpcMemIf_DevErrorDetect();

   /* #10 Check if the ChannelId is valid. */
   if ((isDetEnabled == TRUE) && (ChannelId >= vIpcMemIf_Rx_GetNrOfChannels()))                                         /* PRQA S 2991, 2995, 2996, 3415 */ /* MD_vIpcMemIf_2991, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_vIpcMemIf_3415 */
   {
      errId = VIPCMEMIF_E_PARAM;
   }
   /* #20 Check if the module is initialized. */
   else if ((isDetEnabled == TRUE) && (vIpcMemIf_IsInitialized() == FALSE))                                             /* PRQA S 2995, 2996, 3415 */ /* MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_vIpcMemIf_3415 */
   {
      errId = VIPCMEMIF_E_UNINIT;
   }
   else
   {
      errId = VIPCMEMIF_E_NO_ERROR;

      /* #30 Process the channel. */
      vIpcMemIf_Rx_Process(ChannelId);
   }

   /* #40 Report any error. */
   retValue = vIpcMemIf_ReportError(VIPCMEMIF_SID_RECEIVE, errId);

   return retValue;
} /* vIpcMemIf_Receive */
#endif /* (VIPCMEMIF_RECEIVE_API == STD_ON) */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_MainFunction()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Tx_MainFunction(void)
{
   PduIdType channelId;

   /* #10 If the module is initialized: */
   if (vIpcMemIf_IsInitialized() == TRUE)
   {
      /* #20 For each Tx channel: */
      for (channelId = 0; channelId < vIpcMemIf_Tx_GetNrOfChannels(); channelId++)                                      /* PRQA S 2994, 2996 */ /* MD_vIpcMemIf_2994, MD_vIpcMemIf_2996 */
      {
         /* #30 Process the Tx channel. */
         vIpcMemIf_Tx_Process(channelId, TRUE);
      }
   }
} /* vIpcMemIf_Tx_MainFunction */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_MainFunction()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Rx_MainFunction(void)
{
   PduIdType channelId;

   /* #10 If the module is initialized: */
   if (vIpcMemIf_IsInitialized() == TRUE)
   {
      /* #20 For each Rx channel: */
      for (channelId = 0; channelId < vIpcMemIf_Rx_GetNrOfChannels(); channelId++)                                      /* PRQA S 2994, 2996 */ /* MD_vIpcMemIf_2994, MD_vIpcMemIf_2996 */
      {
         /* #30 Process the Rx channel. */
         vIpcMemIf_Rx_Process(channelId);
      }
   }
} /* vIpcMemIf_Rx_MainFunction */


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_UseInitPointer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_UseInitPointer
(
   void
)
{
   return (boolean)(VIPCMEMIF_USE_INIT_POINTER == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemIf_UseInitPointer */


/**********************************************************************************************************************
 *  vIpcMemIf_UseEcumBswErrorHook()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_UseEcumBswErrorHook
(
   void
)
{
   return (boolean)(VIPCMEMIF_USE_ECUM_BSW_ERROR_HOOK == STD_ON);                                                       /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemIf_UseEcumBswErrorHook */


/**********************************************************************************************************************
 *  vIpcMemIf_DevErrorDetect()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_DevErrorDetect
(
   void
)
{
   return (boolean)(VIPCMEMIF_DEV_ERROR_DETECT == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemIf_DevErrorDetect */


/**********************************************************************************************************************
 *  vIpcMemIf_DevErrorReport()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_DevErrorReport
(
   void
)
{
   return (boolean)(VIPCMEMIF_DEV_ERROR_REPORT == STD_ON);                                                              /* PRQA S 2995, 2996, 4304 */ /* MD_vIpcMemIf_2995, MD_vIpcMemIf_2996, MD_MSR_AutosarBoolean */
} /* vIpcMemIf_DevErrorReport */


/**********************************************************************************************************************
 *  vIpcMemIf_IsInitialized()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_IsInitialized
(
   void
)
{
   return (boolean)(vIpcMemIf_InitStatus == VIPCMEMIF_INITIALIZED);                                                     /* PRQA S 4304 */ /* MD_MSR_AutosarBoolean */
} /* vIpcMemIf_IsInitialized */


/**********************************************************************************************************************
 * vIpcMemIf_ReportError()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(Std_ReturnType, VIPCMEMIF_CODE)
vIpcMemIf_ReportError
(
   vIpcMemIf_ServiceIdType ApiId,
   vIpcMemIf_ErrorCodeType ErrorId
)
{
   Std_ReturnType result = E_OK;

   /* #10 If error reporting is enabled and an error occured: */
   if ((vIpcMemIf_DevErrorReport() == TRUE) && (ErrorId != VIPCMEMIF_E_NO_ERROR))                                       /* PRQA S 2991, 2992, 2995, 2996 */ /* MD_vIpcMemIf_2991, MD_vIpcMemIf_2992, MD_vIpcMemIf_2995, MD_vIpcMemIf_2996 */
   {
      /* #20 Report error to the DET. */
      vIpcMemIf_DetReportError(VIPCMEMIF_MODULE_ID, VIPCMEMIF_INSTANCE_ID, ApiId, ErrorId);

      result = E_NOT_OK;
   }
   else
   {
      VIPCMEMIF_DUMMY_STATEMENT(ApiId);
   }

   return result;
} /* vIpcMemIf_ReportError */



/**********************************************************************************************************************
 *  vIpcMemIf_CheckFinalMagicNumber()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_CheckFinalMagicNumber
(
   void
)
{
   /* #10 Return if the final magic number of the post-build loaded configuration matches the precompile constant. */
   return (boolean)(vIpcMemIf_GetFinalMagicNumber() == VIPCMEMIF_FINAL_MAGIC_NUMBER);                                   /* PRQA S 2995, 4304 */ /* MD_vIpcMemIf_2995, MD_MSR_AutosarBoolean */
} /* vIpcMemIf_CheckFinalMagicNumber */

#define VIPCMEMIF_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 *********************************************************************************************************************/
/* Justification for modules-specific MISRA deviations:

  MD_vIpcMemIf_3415: MISRA rule 12.4
      Reason:     The called function is pure reading function and has no side effects.
      Risk:       None.
      Prevention: None.

  MD_vIpcMemIf_1514:
      Reason:     Whether the object is referenced in other translation units, depends on a configuration parameter.
      Risk:       None, because in other configurations the object is referenced.
      Prevention: None.

  MD_vIpcMemIf_2880:
      Reason:     Reaching this code depends on a configuration parameter.
      Risk:       None, because in other configruations this code is reached.
      Prevention: None.

  MD_vIpcMemIf_2985:
      Reason:     The value of the result depends on a configuration parameter.
      Risk:       None, it is ensured that in other configuration the left-hand operand is not always used.
      Prevention: None.

  MD_vIpcMemIf_2991:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configuration it may be false.
      Prevention: None.

   MD_vIpcMemIf_2992:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configuration it may be true.
      Prevention: None.

   MD_vIpcMemIf_2994:
      Reason:     The controlling expression depends on a configuration parameter.
      Risk:       None, because in other configurations it may be true.
      Prevention: None.

   MD_vIpcMemIf_2995:
      Reason:     The result of the logical operation depends on a configuration parameter.
      Risk:       None, because in other configurations it may be false.
      Prevention: None.

   MD_vIpcMemIf_2996:
      Reason:     The result of the logical operation depends on a configuration parameter.
      Risk:       None, because in other configuration it may be true.
      Prevention: None.

  MD_vIpcMemIf_3219:
      Reason:     This function is inlined and therefore it has to be implemented here. The function is not used
                  by all implementation files which include this header file.
      Risk:       None.
      Prevention: None.
 */

/**********************************************************************************************************************
 *  SILENTBSW JUSTIFICATIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  COVERAGE JUSTIFICATIONS
 *********************************************************************************************************************/

/* START_COVERAGE_JUSTIFICATION
 *
\ID COV_VIPCMEMIF_POSTBUILDUNSUPPORTED
   \ACCEPT XX
   \ACCEPT TX
   \ACCEPT XF
   \REASON This functionality is related to the post build usecase which is not yet supported. Unit test and code
           inspection have not found any risk in keeping this code.

END_COVERAGE_JUSTIFICATION */

/* VCA_JUSTIFICATION_BEGIN

\ID VCA_VIPCMEMIF_POINTER2LOCAL
 \DESCRIPTION    Pass a reference to a local variable.
 \COUNTERMEASURE \N No measure required, as the passed pointer refers to a local variable and the called function
                    allows that the existence of the local variable ends on return.

\ID VCA_VIPCMEMIF_RXCHANNELDATA
 \DESCRIPTION The function is called with a pointer derived from a call of vIpcMemIf_Rx_GetChannel().
 \COUNTERMEASURE \N No countermeasure necessary as GenData will be checked by VCA. An inline VCA assertion ensures,
                    that the pointer is valid.

\ID VCA_VIPCMEMIF_TXCHANNELDATA
 \DESCRIPTION The function is called with a pointer derived from a call of vIpcMemIf_Tx_GetChannel().
 \COUNTERMEASURE \N No countermeasure necessary as GenData will be checked by VCA. An inline VCA assertion ensures,
                    that the pointer is valid.

VCA_JUSTIFICATION_END */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf.c
 *********************************************************************************************************************/
