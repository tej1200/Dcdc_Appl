/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 * \file   vIpcMemIf_Rx.h
 * \brief  Header file of the unit vIpcMemIf_Rx.
 * \details
 * The unit vIpcMemIf_Rx is responsible of receiving messages from all configured channels. For this, the unit provides
 * the function vIpcMemIf_Rx_Process, which takes care of processing a reception on a channel.
 * This function is called periodically from the Rx Main Function for all channels, as well as from vIpcMemIf_Receive,
 * which invocates processing for a specific channel.
 *
 * As a reception can not always be completed within one invocation, the internal state of a reception has to be
 * stored. That allows the unit to suspend a pending reception and continue it on the next call of the processing
 * function. This mechanism is implemented using an internal state machine. Each channel maintains its own instance
 * of the state machine. By that, channel independence is guaranteed.
 *
 * Description of the Rx State machine:
 *
 * States:
 * IDLE
 *
 * The channel is idle.
 * State execution: Poll for messages on the channel. If there is a message, initiate a reception, request the first
 * buffer segment and inform the upper layer about the initiated reception.
 *
 * COPY_PENDING
 *
 * A reception is active and shall be received by the upper layer.
 * State execution: Trigger the copy process at the upper layer. On success, request further buffer segments (if needed)
 * and trigger copying. Finalize reception when all buffer segments were copied successfully and inform the upper layer
 * about the successful reception. If the upper layer indicates an error, cancel the reception.
 *
 * WAIT_FOR_BUFFER
 *
 * The upper layer does currently not have enough space to receive the data.
 * Upon entering this state, processing is suspended for this channel and resumed on the next event.
 * State execution: Request the available space from the upper layer. Resume processing of the channel, if the upper
 * layer has enough space again.
 *
 *
 * State Transitions:
 *
 *
 * * --> IDLE
 *
 * IDLE --> COPY_PENDING : Successful initiation of a reception. Data has to be copied.
 *
 * IDLE --> WAIT_FOR_BUFFER : Reception was started, but upper layer does not have enough
 * space for the current buffer segment.
 *
 * WAIT_FOR_BUFFER --> IDLE : Reception was aborted by the upper layer.
 *
 * WAIT_FOR_BUFFER --> COPY_PENDING : Upper layer has enough space. Reception can be processed
 * further.
 *
 * COPY_PENDING --> IDLE : Message was completely received by the upper layer and finalized.
 *
 * COPY_PENDING --> IDLE : Reception was aborted by the upper layer.
 *
 * COPY_PENDING --> WAIT_FOR_BUFFER : The upper layer does not have enough space for the current buffer segment.
 *
 *
 * \trace DSGN-vIpcMemIf-PeriodicReception, DSGN-vIpcMemIf-OnDemandReception, DSGN-vIpcMemIf-CancelReception
 * \trace DSGN-vIpcMemIf-Initialization, DSGN-vIpcMemIf-ChannelIndependence, DSGN-vIpcMemIf-MultipleOsVariants
 *
 * \unit vIpcMemIf_Rx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *********************************************************************************************************************/

#ifndef VIPCMEMIF_RX_H
# define VIPCMEMIF_RX_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Int.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetNrOfChannels()
 *********************************************************************************************************************/
/*! \brief       Returns the number of the configured Rx channels.
 *  \details     -
 *  \return      Number of Rx channels.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetNrOfChannels
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_Init()
 *********************************************************************************************************************/
/*! \brief       Initializes all configured Rx channels.
 *  \details     -
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Rx_Init
(
   void
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_Process()
 *********************************************************************************************************************/
/*! \brief       Performs a full processing of a reception on the channel.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. Must be valid.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Rx_Process
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetNrOfChannels()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetNrOfChannels                                                                                            /* PRQA S 3219 */ /* MD_vIpcMemIf_3219 */
(
   void
)
{
   return vIpcMemIf_GetSizeOfRxConfig();
} /* vIpcMemIf_Rx_GetNrOfChannels */

# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMIF_RX_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Rx.h
 *********************************************************************************************************************/
