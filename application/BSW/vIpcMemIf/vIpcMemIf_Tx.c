/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*! \file     vIpcMemIf_Tx.c
 *  \brief    Contains the implementation of the unit vIpcMemIf_Tx.
 *  \details  -
 *
 *  \unit vIpcMemIf_Tx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.c
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Tx.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
/*! Mapping of the upper layer function CopyTxData. */
# define vIpcMemIf_UL_CopyTxData vIpc_CopyTxData
/*! Mapping of the upper layer function TxConfirmation. */
# define vIpcMemIf_UL_TxConfirmation vIpc_TxConfirmation


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the channel state data structure associated with the channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      Channel state data structure.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT), VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetChannel
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetLLId()
 *********************************************************************************************************************/
/*! \brief       Returns the mapping of the vIpcMemIf channel ID to the lower layer channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      Lower layer channel ID
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(vIpcMemIf_LL_ChannelIndexType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetLLId
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetULId()
 *********************************************************************************************************************/
/*! \brief       Returns the mapping of the vIpcMemIf channel ID to the vIpc channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      vIpc channel ID
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetULId
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_ProcessCopyState()
 *********************************************************************************************************************/
/*! \brief       Performs the step of the processing associated with the pending copy operation.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      TRUE                      Further processing is possible.
 *  \return      FALSE                     The result of the operation indicates that further processing should be
 *                                         postponed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Tx_ProcessCopyState
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_ProcessConfirmationState()
 *********************************************************************************************************************/
/*! \brief       Performs the step of the processing associated with the pending confirmation to the upper layer.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      TRUE                      Further processing is possible.
 *  \return      FALSE                     The result of the operation indicates that further processing should be
 *                                         postponed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Tx_ProcessConfirmationState
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetChannel()
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT), VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetChannel
(
   PduIdType ChannelId
)
{
   return &(vIpcMemIf_GetTxChannelData(ChannelId));
} /* vIpcMemIf_Tx_GetChannel */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetLLId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(vIpcMemIf_LL_ChannelIndexType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetLLId
(
   PduIdType ChannelId
)
{
   return vIpcMemIf_GetLowerLayerIdOfTxConfig(ChannelId);
} /* vIpcMemIf_Tx_GetLLId */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetULId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetULId
(
   PduIdType ChannelId
)
{
   return vIpcMemIf_GetUpperLayerIdOfTxConfig(ChannelId);
} /* vIpcMemIf_Tx_GetULId */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_ProcessCopyState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Tx_ProcessCopyState
(
   PduIdType ChannelId
)
{
   PduInfoType pduInfo;
   PduLengthType availableData;
   BufReq_ReturnType copyResult;
   boolean continueProcessing = TRUE;
   P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Tx_GetChannel(ChannelId);

   /* @ assert channel != NULL_PTR; */

   /* #10 Request the upper layer to copy the next segment. */
   pduInfo.SduDataPtr  = channel->BufferDesc.Ptr;
   pduInfo.SduLength   = (PduLengthType)channel->BufferDesc.Size;
   copyResult = vIpcMemIf_UL_CopyTxData(vIpcMemIf_Tx_GetULId(ChannelId), &pduInfo, NULL_PTR, &availableData);           /* VCA_VIPCMEMIF_POINTER2LOCAL */

   /* #20 If an error has occurred: */
   if ((copyResult != BUFREQ_OK) && (copyResult != BUFREQ_E_BUSY))
   {
      /* #30 Signal the transmission abort to the lower layer. */
      (void)vIpcMemIf_LL_Tx_Cancel(vIpcMemIf_Tx_GetLLId(ChannelId));

      /* #40 Change the channel state to idle and abort processing. */
      channel->State = VIPCMEMIF_TX_IDLE;
      continueProcessing = FALSE;
   }
   /* #50 If the upper layer is busy: */
   else if (copyResult == BUFREQ_E_BUSY)
   {
      /* #60 Suspend processing. */
      continueProcessing = FALSE;
   }
   /* #70 If the call was successful: */
   else
   {
      /* #80 And the message should have been copied completely by the upper layer: */
      if (channel->RemainingSize <= channel->BufferDesc.Size)
      {
         vIpcMemIf_UserTxNotificationCallbackType userCallback = vIpcMemIf_GetUserTxNotificationFctPtr();

         /* #90 Set remaining size to zero. */
         channel->RemainingSize = 0u;

         /* #100 Confirm the transmission to the lower layer. */
         (void)vIpcMemIf_LL_Tx_Confirm(vIpcMemIf_Tx_GetLLId(ChannelId));

         /* #110 Change the channel state to confirmation pending to queue the confirmation to the upper layer. */
         channel->State = VIPCMEMIF_TX_CONFIRMATION_PENDING;


         /* #120 Call the user's notification callback, if it is available. */
         if (userCallback != NULL_PTR)
         {
            userCallback(ChannelId);
         }
      }
      /* #130 Otherwise: */
      else
      {
         /* #140 Decrease the remaining size of the message. */
         channel->RemainingSize -= channel->BufferDesc.Size;

         /* #150 Request the next buffer segment from the lower layer. */
         (void)vIpcMemIf_LL_Tx_GetBuffer(vIpcMemIf_Tx_GetLLId(ChannelId), channel->RemainingSize,                       /* VCA_VIPCMEMIF_TXCHANNELDATA */
                                         &(channel->BufferDesc));
      }
   }

   return continueProcessing;
} /* vIpcMemIf_Tx_ProcessCopyState */ /* PRQA S 6050 */ /* MD_MSR_STCAL */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_ProcessConfirmationState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Tx_ProcessConfirmationState
(
   PduIdType ChannelId
)
{
   boolean continueProcessing = FALSE;
   P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Tx_GetChannel(ChannelId);

   /* #10 Change the channel state to idle. */
   channel->State = VIPCMEMIF_TX_IDLE;

   /* #20 Confirm the transmission to the upper layer. */
   vIpcMemIf_UL_TxConfirmation(vIpcMemIf_Tx_GetULId(ChannelId), E_OK);

   return continueProcessing;
} /* vIpcMemIf_Tx_ProcessConfirmationState */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Tx_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Tx_Init
(
    void
)
{
   PduIdType channelId;
   uint32 size = vIpcMemIf_GetSizeOfTxConfig();

   for (channelId = 0; channelId < size; channelId++)                                                                   /* PRQA S 2994, 2996 */ /* MD_vIpcMemIf_2994, MD_vIpcMemIf_2996 */
   {
      vIpcMemIf_Tx_GetChannel(channelId)->State = VIPCMEMIF_TX_IDLE;                                                    /* PRQA S 2880 */ /* MD_vIpcMemIf_2880 */
   }
} /* vIpcMemIf_Tx_Init */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_StartTransmission()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_StartTransmission
(
   PduIdType ChannelId,
   PduLengthType DataSize
)
{
   Std_ReturnType status = E_NOT_OK;
   P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Tx_GetChannel(ChannelId);

   /* @ assert channel != NULL_PTR; */

   /* #10 If the channel is idle: */
   if (channel->State == VIPCMEMIF_TX_IDLE)
   {
      /* #20 Initiate a transmission on the lower layer. */
      Std_ReturnType transmitResult = vIpcMemIf_LL_Tx_BeginTransmit(vIpcMemIf_Tx_GetLLId(ChannelId), DataSize);

      /* #30 If the transmission request was successful: */
      if (transmitResult == E_OK)
      {
         /* #40 Update the remaining size of the channel object. */
         channel->RemainingSize = DataSize;

         /* #50 Request a buffer segment from the lower layer. */
         (void)vIpcMemIf_LL_Tx_GetBuffer(vIpcMemIf_Tx_GetLLId(ChannelId), channel->RemainingSize,                       /* VCA_VIPCMEMIF_TXCHANNELDATA */
            &(channel->BufferDesc));

         /* #60 Set the channel state to copy pending to queue the copy request. */
         channel->State = VIPCMEMIF_TX_COPY_PENDING;

         status = E_OK;
      }
   }

   return status;
} /* vIpcMemIf_Tx_StartTransmission */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_Process()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Tx_Process
(
   PduIdType ChannelId,
   boolean MainFunctionContext
)
{
   P2VAR(vIpcMemIf_TxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Tx_GetChannel(ChannelId);
   boolean continueProcessing;

   do
   {
      switch(channel->State)
      {
      case VIPCMEMIF_TX_IDLE:
         /* #10 Nothing to do here for an idle channel. */
         continueProcessing = FALSE;
         break;

      case VIPCMEMIF_TX_COPY_PENDING:
         /* #20 Process the copy pending state. */
         continueProcessing = vIpcMemIf_Tx_ProcessCopyState(ChannelId);
         break;

      case VIPCMEMIF_TX_CONFIRMATION_PENDING:
         /* #30 If the function was called from MainFunction context: */
         if (MainFunctionContext == TRUE)
         {
            /* #40 Process the confirmation pending state. */
            (void)vIpcMemIf_Tx_ProcessConfirmationState(ChannelId);
         }

         continueProcessing = FALSE;
         break;

      default:
         /* Invalid state. Reset to idle state. */
         channel->State = VIPCMEMIF_TX_IDLE;
         continueProcessing = FALSE;
         break;
      }
   /* #50 Loop while further processing is possible. */
   } while (continueProcessing == TRUE);
} /* vIpcMemIf_Tx_Process */

# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Tx.c
 *********************************************************************************************************************/
