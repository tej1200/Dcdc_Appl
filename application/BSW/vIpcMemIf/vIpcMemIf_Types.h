/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file
 *        \brief  vIpcMemIf types header file
 *      \details  Contains the type definitions of the module.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *********************************************************************************************************************/

#ifndef VIPCMEMIF_TYPES_H
# define VIPCMEMIF_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "ComStack_Types.h"
# include "vIpcMemIf_LL_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ----- API service IDs ----- */
# define VIPCMEMIF_SID_INIT               (vIpcMemIf_ServiceIdType)(0u)    /*!< Service ID: vIpcMemIf_Init() */
# define VIPCMEMIF_SID_INIT_MEMORY        (vIpcMemIf_ServiceIdType)(1u)    /*!< Service ID: vIpcMemIf_InitMemory() */
# define VIPCMEMIF_SID_GET_VERSION_INFO   (vIpcMemIf_ServiceIdType)(2u)    /*!< Service ID: vIpcMemIf_GetVersionInfo() */
# define VIPCMEMIF_SID_TRANSMIT           (vIpcMemIf_ServiceIdType)(3u)    /*!< Service ID: vIpcMemIf_Transmit() */
# define VIPCMEMIF_SID_RECEIVE            (vIpcMemIf_ServiceIdType)(4u)    /*!< Service ID: vIpcMemIf_Receive() */

/* ----- Error codes ----- */
# define VIPCMEMIF_E_NO_ERROR             (vIpcMemIf_ErrorCodeType)(0xFFu) /*!< No error. */
# define VIPCMEMIF_E_INV_PTR              (vIpcMemIf_ErrorCodeType)(1u)    /*!< Error code: Invalid pointer. */
# define VIPCMEMIF_E_PARAM                (vIpcMemIf_ErrorCodeType)(2u)    /*!< Error code: Invalid parameter. */
# define VIPCMEMIF_E_UNINIT               (vIpcMemIf_ErrorCodeType)(3u)    /*!< Error code: API service used without module initialization. */


# ifndef VIPCMEMIF_LOCAL                                                                                                /* COV_VIPCMEMIF_COMPILERKEYWORD */
#  define VIPCMEMIF_LOCAL static
# endif

# ifndef VIPCMEMIF_LOCAL_INLINE                                                                                         /* COV_VIPCMEMIF_COMPILERKEYWORD */
#  define VIPCMEMIF_LOCAL_INLINE LOCAL_INLINE
# endif

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/*! Type for service IDs. */
typedef uint8 vIpcMemIf_ServiceIdType;

/*! Type for the error codes. */
typedef uint8 vIpcMemIf_ErrorCodeType;

/*! Type to store the initialization status of the module. */
typedef boolean vIpcMemIf_InitStatusType;

/*! Function pointer type for user's notification callback. */
typedef void (*vIpcMemIf_UserTxNotificationCallbackType)(PduIdType ChannelId);

/*! Type to store the processing state of a reception. */
typedef enum
{
   VIPCMEMIF_RX_IDLE,
   VIPCMEMIF_RX_COPY_PENDING,
   VIPCMEMIF_RX_WAIT_FOR_BUFFER
} vIpcMemIf_RxProcessingStateType;

/*! Type to describe the current state of a Rx channel. */
typedef struct
{
   /*! Channel state. */
   vIpcMemIf_RxProcessingStateType State;

   /*! Number of bytes which are yet to be copied by the upper layer. */
   uint32 RemainingSize;

   /*! The buffer description provided by the lower layer. */
   vIpcMemIf_LL_BufferDescriptionType BufferDesc;
} vIpcMemIf_RxChannelStateType;

/*! Type to store the processing state of a transmission. */
typedef enum
{
   VIPCMEMIF_TX_IDLE,
   VIPCMEMIF_TX_COPY_PENDING,
   VIPCMEMIF_TX_CONFIRMATION_PENDING
} vIpcMemIf_TxProcessingStateType;

/*! Type to describe the current state of a Tx channel. */
typedef struct
{
   /*! Channel state. */
   vIpcMemIf_TxProcessingStateType State;

   /*! Number of bytes which are yet to be copied by the upper layer. */
   uint32 RemainingSize;

   /*! The buffer description provided by the lower layer. */
   vIpcMemIf_LL_BufferDescriptionType BufferDesc;
} vIpcMemIf_TxChannelStateType;




/* START_COVERAGE_JUSTIFICATION
 *
\ID COV_VIPCMEMIF_COMPILERKEYWORD
   \ACCEPT TX
   \REASON [COV_MSR_COMPATIBILITY]

END_COVERAGE_JUSTIFICATION */

#endif /* VIPCMEMIF_TYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Types.h
 *********************************************************************************************************************/
