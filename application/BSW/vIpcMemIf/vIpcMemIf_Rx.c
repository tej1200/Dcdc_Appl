/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 * \file  vIpcMemIf_Rx.c
 * \brief Implementation of the unit vIpcMemIf_Rx.
 *
 * \details  Refer to vIpcMemIf_Rx.h.
 *
 * \unit vIpcMemIf_Rx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Rx.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
/*! Mapping of the upper layer function StartOfReception. */
# define vIpcMemIf_UL_StartOfReception vIpc_StartOfReception
/*! Mapping of the upper layer function CopyRxData. */
# define vIpcMemIf_UL_CopyRxData vIpc_CopyRxData
/*! Mapping of the upper layer function RxIndication. */
# define vIpcMemIf_UL_RxIndication vIpc_RxIndication


/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetChannel()
 *********************************************************************************************************************/
/*! \brief       Returns the channel state data structure associated with the channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      Channel state data structure.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT), VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetChannel
(
   PduIdType ChannelId
);


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetLLId()
 *********************************************************************************************************************/
/*! \brief       Returns the mapping of the vIpcMemIf channel ID to the lower layer channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      Lower layer channel ID
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(vIpcMemIf_LL_ChannelIndexType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetLLId
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetULId()
 *********************************************************************************************************************/
/*! \brief       Returns the mapping of the vIpcMemIf channel ID to the vIpc channel ID.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      vIpc channel ID
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetULId
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessIdleState()
 *********************************************************************************************************************/
/*! \brief       Performs the step of the processing associated with the idle state of the channel.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      TRUE                      Further processing is possible.
 *  \return      FALSE                     The result of the operation indicates that further processing should be
 *                                         postponed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessIdleState
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessWaitForBufferState()
 *********************************************************************************************************************/
/*! \brief       Performs the step of the processing associated with the waiting operation.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      TRUE                      Further processing is possible.
 *  \return      FALSE                     The result of the operation indicates that further processing should be
 *                                         postponed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessWaitForBufferState
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessCopyState()
 *********************************************************************************************************************/
/*! \brief       Performs the step of the processing associated with the pending copy operation.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \return      TRUE                      Further processing is possible.
 *  \return      FALSE                     The result of the operation indicates that further processing should be
 *                                         postponed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessCopyState
(
   PduIdType ChannelId
);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetChannel()
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT), VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetChannel
(
   PduIdType ChannelId
)
{
   return &(vIpcMemIf_GetRxChannelData(ChannelId));
} /* vIpcMemIf_Rx_GetChannel */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetLLId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(vIpcMemIf_LL_ChannelIndexType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetLLId
(
   PduIdType ChannelId
)
{
   return vIpcMemIf_GetLowerLayerIdOfRxConfig(ChannelId);
} /* vIpcMemIf_Rx_GetLLId */

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_GetULId()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Rx_GetULId
(
   PduIdType ChannelId
)
{
   return vIpcMemIf_GetUpperLayerIdOfRxConfig(ChannelId);
} /* vIpcMemIf_Rx_GetULId */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessIdleState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessIdleState
(
   PduIdType ChannelId
)
{
   boolean continueProcessing = TRUE;
   P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Rx_GetChannel(ChannelId);

   /* @ assert channel != NULL_PTR; */

   /* #10 Request a reception from the lower layer. */
   Std_ReturnType receiveResult = vIpcMemIf_LL_Rx_BeginReceive(vIpcMemIf_Rx_GetLLId(ChannelId),                         /* VCA_VIPCMEMIF_RXCHANNELDATA */
                                      &(channel->RemainingSize));

   /* #20 If no receivable message is available, abort processing. */
   if (receiveResult != E_OK)
   {
      continueProcessing = FALSE;
   }
   else
   {
      BufReq_ReturnType startOfReceptionResult;
      PduInfoType pduInfo;
      PduLengthType availableSize;

      /* #30 Request the buffer segment from the lower layer. */
      (void)vIpcMemIf_LL_Rx_GetBuffer(vIpcMemIf_Rx_GetLLId(ChannelId), channel->RemainingSize, &(channel->BufferDesc)); /* VCA_VIPCMEMIF_RXCHANNELDATA */

      /* #40 Signal the start of reception to the upper layer. */
      pduInfo.SduDataPtr  = channel->BufferDesc.Ptr;
      pduInfo.SduLength   = (PduLengthType)channel->BufferDesc.Size;
      startOfReceptionResult = vIpcMemIf_UL_StartOfReception(vIpcMemIf_Rx_GetULId(ChannelId), &pduInfo,                 /* VCA_VIPCMEMIF_POINTER2LOCAL */
                                   (PduLengthType)channel->RemainingSize, &availableSize);

      /* #50 If an error has occurred: */
      if (startOfReceptionResult != BUFREQ_OK)
      {
         /* #60 Signal the reception abort to the lower layer. */
         (void)vIpcMemIf_LL_Rx_Cancel(vIpcMemIf_Rx_GetLLId(ChannelId));

         /* #70 Abort processing. */
         continueProcessing = FALSE;
      }
      /* #80 If the call was successful: */
      else
      {
         /* #90 If the upper layer does not have enough space: */
         if (availableSize == 0u)
         {
            /* #100 Change the channel state to waiting for buffer and suspend processing. */
            channel->State = VIPCMEMIF_RX_WAIT_FOR_BUFFER;
            continueProcessing = FALSE;
         }
         /* #110 Otherwise: */
         else
         {
            /* #120 Change the channel state to queue copy request to the upper layer. */
            channel->State = VIPCMEMIF_RX_COPY_PENDING;
         }
      }
   }
   return continueProcessing;
} /* vIpcMemIf_Rx_ProcessIdleState */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessWaitForBufferState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessWaitForBufferState
(
   PduIdType ChannelId
)
{
   PduInfoType pduInfo;
   PduLengthType availableBufferSize;
   BufReq_ReturnType copyResult;
   boolean continueProcessing = TRUE;
   P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Rx_GetChannel(ChannelId);

   /* #10 Request the available space from the upper layer. */
   pduInfo.SduDataPtr  = NULL_PTR;
   pduInfo.SduLength   = 0;
   copyResult = vIpcMemIf_UL_CopyRxData(vIpcMemIf_Rx_GetULId(ChannelId), &pduInfo, &availableBufferSize);               /* VCA_VIPCMEMIF_POINTER2LOCAL */

   /* #20 If an error has occurred: */
   if (copyResult != BUFREQ_OK)
   {
      /* #30 Signal the reception abort to the lower layer. */
      (void)vIpcMemIf_LL_Rx_Cancel(vIpcMemIf_Rx_GetLLId(ChannelId));

      /* #40 Change the channel state to idle and abort processing. */
      channel->State = VIPCMEMIF_RX_IDLE;
      continueProcessing = FALSE;
   }
   /* #50 If the call was successful: */
   else
   {
      /* #60 If the upper layer does not have enough space for the next data segment: */
      if (availableBufferSize < channel->BufferDesc.Size)
      {
         /* #70 Suspend processing. */
         continueProcessing = FALSE;
      }
      /* #80 Otherwise: */
      else
      {
         /* #90 Change the channel state to copy pending to queue the copy request. */
         channel->State = VIPCMEMIF_RX_COPY_PENDING;
      }
   }

   return continueProcessing;
} /* vIpcMemIf_Rx_ProcessWaitForBufferState */

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_ProcessCopyState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(boolean, VIPCMEMIF_CODE)
vIpcMemIf_Rx_ProcessCopyState
(
   PduIdType ChannelId
)
{
   PduInfoType pduInfo;
   PduLengthType availableBufferSize;
   BufReq_ReturnType copyResult;
   boolean continueProcessing = TRUE;
   P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Rx_GetChannel(ChannelId);

   /* @ assert channel != NULL_PTR; */

   /* #10 Request the upper layer to copy the next segment. */
   pduInfo.SduDataPtr  = channel->BufferDesc.Ptr;
   pduInfo.SduLength   = (PduLengthType)channel->BufferDesc.Size;
   copyResult = vIpcMemIf_UL_CopyRxData(vIpcMemIf_Rx_GetULId(ChannelId), &pduInfo, &availableBufferSize);               /* VCA_VIPCMEMIF_POINTER2LOCAL */

   /* #20 If an error has occurred: */
   if (copyResult != BUFREQ_OK)
   {
      /* #30 Signal the reception abort to the lower layer. */
      (void)vIpcMemIf_LL_Rx_Cancel(vIpcMemIf_Rx_GetLLId(ChannelId));

      /* #40 Change the channel state to idle and abort processing. */
      channel->State = VIPCMEMIF_RX_IDLE;
      continueProcessing = FALSE;
   }
   /* #50 If the call was successful: */
   else
   {
      /* #60 Decrease the remaining size of the message. */
      channel->RemainingSize -= channel->BufferDesc.Size;

      /* #70 If the message was not fully copied yet: */
      if (channel->RemainingSize != 0u)
      {
         /* #80 Request the next buffer segment from the lower layer. */
         (void)vIpcMemIf_LL_Rx_GetBuffer(vIpcMemIf_Rx_GetLLId(ChannelId), channel->RemainingSize,                       /* VCA_VIPCMEMIF_RXCHANNELDATA */
                                            &(channel->BufferDesc));

         /* #90 If the upper layer has not sufficient space for the next segment: */
         if (availableBufferSize < channel->BufferDesc.Size)
         {
            /* #100 Change the channel state to waiting for buffer and suspend processing. */
            channel->State = VIPCMEMIF_RX_WAIT_FOR_BUFFER;
            continueProcessing = FALSE;
         }
      }
      /* #110 Otherwise: */
      else
      {
         /* #120 Confirm the reception to the lower layer. */
         (void)vIpcMemIf_LL_Rx_Confirm(vIpcMemIf_Rx_GetLLId(ChannelId));

         /* #130 Indicate the reception end to the upper layer. */
         vIpcMemIf_UL_RxIndication(vIpcMemIf_Rx_GetULId(ChannelId), E_OK);

         /* #140 Change the channel state to idle and finish processing. */
         channel->State = VIPCMEMIF_RX_IDLE;
         continueProcessing = FALSE;
      }
   }
   return continueProcessing;
} /* vIpcMemIf_Rx_ProcessCopyState */ /* PRQA S 6050 */ /* MD_MSR_STCAL */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Rx_Init
(
    void
)
{
   PduIdType channelId;
   uint32 size = vIpcMemIf_GetSizeOfRxConfig();

   for (channelId = 0; channelId < size; channelId++)                                                                   /* PRQA S 2994, 2996 */ /* MD_vIpcMemIf_2994, MD_vIpcMemIf_2996 */
   {
      vIpcMemIf_Rx_GetChannel(channelId)->State = VIPCMEMIF_RX_IDLE;                                                    /* PRQA S 2880 */ /* MD_vIpcMemIf_2880 */
   }
} /* vIpcMemIf_Rx_InitChannel */


/**********************************************************************************************************************
 *  vIpcMemIf_Rx_Process()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Rx_Process
(
   PduIdType ChannelId
)
{
   P2VAR(vIpcMemIf_RxChannelStateType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) channel = vIpcMemIf_Rx_GetChannel(ChannelId);
   boolean continueProcessing;

   /* #10 Enter critical section. */
   vIpcMemIf_EnterExclusiveArea();

   do
   {
      switch(channel->State)
      {
      case VIPCMEMIF_RX_IDLE:
         /* #20 Process the idle state. */
         continueProcessing = vIpcMemIf_Rx_ProcessIdleState(ChannelId);
         break;

      case VIPCMEMIF_RX_WAIT_FOR_BUFFER:
         /* #30 Process the waiting for buffer state. */
         continueProcessing = vIpcMemIf_Rx_ProcessWaitForBufferState(ChannelId);
         break;

      case VIPCMEMIF_RX_COPY_PENDING:
         /* #40 Process the copy pending state. */
         continueProcessing = vIpcMemIf_Rx_ProcessCopyState(ChannelId);
         break;

      default:
         /* Invalid state. Reset to idle state. */
         channel->State = VIPCMEMIF_RX_IDLE;
         continueProcessing = FALSE;
         break;
      }
   /* #50 Loop while further processing is possible. */
   } while (continueProcessing == TRUE);

   /* #60 Exit critical section. */
   vIpcMemIf_ExitExclusiveArea();

} /* vIpcMemIf_Rx_Process */


# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Rx.c
 *********************************************************************************************************************/
