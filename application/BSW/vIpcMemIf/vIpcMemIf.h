/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 *  \file  vIpcMemIf.h
 *  \brief External interface of the module vIpcMemIf.
 *
 *  \details  Contains the declaration of the module's API functions.
 *
 *  \unit vIpcMemIf_Base
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2019-11-04  virbse  OSC-5304      Initial version
 *            2020-08-14  visdqk  OSC-191       Refactored implementation
 *  01.01.00  2021-06-09  visdqk  OSC-6696      Fixed compiler error: Undefined references
 *            2021-07-28  visdqk  OSC-5044      Updates for SafeBSW
 *********************************************************************************************************************/

#ifndef VIPCMEMIF_H
# define VIPCMEMIF_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Cfg.h"
# include "vIpcMemIf_Lcfg.h"
# include "vIpcMemIf_PBcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Vendor and module identification */
# define VIPCMEMIF_VENDOR_ID                    (30u)
# define VIPCMEMIF_MODULE_ID                    (255u)
# define VIPCMEMIF_INSTANCE_ID                  (0u)

/* AUTOSAR Software specification version information */
# define VIPCMEMIF_AR_RELEASE_MAJOR_VERSION     (4u)
# define VIPCMEMIF_AR_RELEASE_MINOR_VERSION     (3u)
# define VIPCMEMIF_AR_RELEASE_REVISION_VERSION  (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VIPCMEMIF_SW_MAJOR_VERSION             (1u)
# define VIPCMEMIF_SW_MINOR_VERSION             (1u)
# define VIPCMEMIF_SW_PATCH_VERSION             (0u)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vIpcMemIf_InitMemory()
 *********************************************************************************************************************/
/*! \brief       Initialization for *_INIT_*-variables
 *  \details     Service to initialize module global variables at power up. This function initializes the
 *               variables in *_INIT_* sections. Used in case they are not initialized by the startup code.
 *  \pre         Module is uninitialized.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-Init
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_InitMemory(void);

/**********************************************************************************************************************
 * vIpcMemIf_Init()
 *********************************************************************************************************************/
/*! \brief       Initialization function.
 *  \details     Service to initialize the module vIpcMemIf. It initializes all variables and sets the module state to
 *               initialized.
 *
 *  \param[in]   ConfigPtr               Configuration structure for initializing the module
 *
 *  \pre         Module is uninitialized.
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-Init
 *  \trace       DSGN-vIpcMemIf-Initialization
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Init(P2CONST(vIpcMemIf_ConfigType, AUTOMATIC, VIPCMEMIF_INIT_DATA) ConfigPtr);

/**********************************************************************************************************************
 *  vIpcMemIf_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief       Returns the version information.
 *  \details     This function returns the version information, vendor ID and AUTOSAR module ID of the component.
 *
 *  \param[out]  VersionInfo             Pointer to where to store the version information. Parameter must not be NULL.
 *
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-GetVersionInfo
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_GetVersionInfo
(
    P2VAR(Std_VersionInfoType, AUTOMATIC, VIPCMEMIF_VAR_NOINIT) VersionInfo
);


/**********************************************************************************************************************
 * vIpcMemIf_Transmit()
 *********************************************************************************************************************/
/*! \brief       Requests a new transmission on the channel.
 *  \details     If the channel is idle, the transmission is accepted and processed.
 *
 *  \param[in]   ChannelId                 Id to identify the channel.
 *  \param[in]   PduInfoPtr                Pdu description.
 *
 *  \return      E_NOT_OK    Transmission request failed
 *  \return      E_OK        Transmission request accepted
 *
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-Transmit
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMIF_CODE) vIpcMemIf_Transmit
(
    PduIdType ChannelId,
    P2CONST(PduInfoType, AUTOMATIC, VIPCMEMIF_CONST) PduInfoPtr
);

/**********************************************************************************************************************
 *  vIpcMemIf_Receive()
 *********************************************************************************************************************/
/*! \brief       Triggers reception processing of a specific channel.
 *  \details     -
 *  \param[in]   ChannelId                 Id to identify the channel.
 *
 *  \return      E_NOT_OK    Processing request failed.
 *  \return      E_OK        Processing request accepted.
 *
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE for different channels.
 *  \config      VIPCMEMIF_RECEIVE_API
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-ReceiveOnDemand
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMIF_CODE) vIpcMemIf_Receive
(
    PduIdType ChannelId
);

/**********************************************************************************************************************
 *  vIpcMemIf_Tx_MainFunction()
 *********************************************************************************************************************/
/*! \brief       Controls the continuous processing of all transmission channels.
 *  \details     -
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-TxPeriodicProcessing
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Tx_MainFunction
(
    void
);

/**********************************************************************************************************************
 *  vIpcMemIf_Rx_MainFunction()
 *********************************************************************************************************************/
/*! \brief       Controls the periodic processing of all reception channels.
 *  \details     -
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *  \trace       CREQ-vIpcMemIf-RxPeriodicProcessing
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE) vIpcMemIf_Rx_MainFunction
(
    void
);

# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  EXCLUSIVE AREA DEFINITION
 *********************************************************************************************************************/
/*!
 * \exclusivearea VIPCMEMIF_EXCLUSIVE_AREA_0
 * Ensures consistency of internal data while processing a single channel,
 * i.e. if the vIpcMemIf_Receive is called while vIpcMemIf_Rx_MainFunction is active.
 * Entering/Leaving is wrapped by vIpcMemIf_EnterExclusiveArea, vIpcMemIf_ExitExclusiveArea.
 * \protects The internal channel state used during processing.
 * \usedin ALL functions called during reception processing.
 * \exclude N/A
 * \length LONG The exclusive area covers the whole channel processing of reception.
 * \endexclusivearea
 */

#endif /* VIPCMEMIF_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf.h
 *********************************************************************************************************************/
