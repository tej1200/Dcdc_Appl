/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!
 * \file   vIpcMemIf_Tx.h
 * \brief  Header file of the unit vIpcMemIf_Tx.
 *
 * \details
 * The unit vIpcMemIf_Tx is responsible of transmitting messages over the configured channels. The unit provides a
 * function to initiate a transmission (vIpcMemIf_Tx_StartTransmission) and a function to process a pending transmission
 * on a specific channel (vIpcMemIf_Tx_Process). During periodic processing of the TX channels, the processing function
 * shall be called for all configured Tx channels.
 *
 * As a transmission can not always be completed within one invocation of the process function, the internal state of a
 * transmission has to be stored. This allows the unit to suspend transmissions and continue them in the next call.
 * This mechanism is implemented using an internal state machine. Each channel maintains its own instance of the
 * Tx state machine. By this, channel independence is guaranteed.
 *
 * Description of the Tx State machine:
 *
 * States:
 * IDLE
 *
 * The channel is idle. New transmissions can be initiated on the channel.
 *
 * COPY_PENDING
 *
 * A transmission is pending and the upper layer shall copy data into the buffer.
 * State execution: Trigger the copy process at the upper layer. On success, request another buffer segment if needed.
 * Otherwise, finalize the transmission and notify the user. If the upper layer is busy, suspend execution for now.
 * If the upper layer indicates an error, cancel the transmission.
 *
 * CONFIRMATION_PENDING
 *
 * A transmission was finished and shall be confirmed to the upper layer.
 *
 *
 * State Transitions:
 *
 *
 * * --> IDLE
 *
 * IDLE --> COPY_PENDING : Successful initiation of a transmission. Data can be copied.
 *
 * COPY_PENDING --> IDLE : Transmission was aborted by the upper layer.
 *
 * COPY_PENDING --> CONFIRMATION_PENDING : Transmission has been finished and can be confirmed to the upper layer.
 *
 * CONFIRMATION_PENDING --> IDLE : Transmission was confirmed.
 *
 * \trace DSGN-vIpcMemIf-Transmit, DSGN-vIpcMemIf-PeriodicTransmission, DSGN-vIpcMemIf-CancelTransmission
 * \trace DSGN-vIpcMemIf-Initialization, DSGN-vIpcMemIf-ChannelIndependence, DSGN-vIpcMemIf-MultipleOsVariants
 *
 * \unit vIpcMemIf_Tx
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to vIpcMemIf.h.
 *********************************************************************************************************************/

#ifndef VIPCMEMIF_TX_H
# define VIPCMEMIF_TX_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vIpcMemIf_Int.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VIPCMEMIF_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetNrOfChannels()
 *********************************************************************************************************************/
/*! \brief       Returns the number of the configured Tx channels.
 *  \details     -
 *  \return      Number of Tx channels.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetNrOfChannels
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_Init()
 *********************************************************************************************************************/
/*! \brief       Initializes all configured Tx channels.
 *  \details     -
 *  \param[in]   Channel                  Channel to initialized. Must not be NULL_PTR.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Tx_Init
(
   void
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_StartTransmission()
 *********************************************************************************************************************/
/*! \brief       Attempts to start a transmission of a given size on the given channel.
 *  \details     -
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \param[in]   DataSize                  Message size in bytes.
 *  \return      E_OK                      Transmission request accepted.
 *  \return      E_NOT_OK                  Transmission request failed.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_StartTransmission
(
   PduIdType ChannelId,
   PduLengthType DataSize
);


/**********************************************************************************************************************
 *  vIpcMemIf_Tx_Process()
 *********************************************************************************************************************/
/*! \brief       Performs a full processing of a transmission on the channel.
 *  \details     If the function is NOT called from the Tx MainFunction, the confirmation to the upper layer will be
 *               delayed until next call from the Tx MainFunction.
 *  \param[in]   ChannelId                 Channel index. It has to be valid.
 *  \param[in]   MainFunctionContext       TRUE  if the function was called from the MainFunction.
 *                                         FALSE otherwise.
 *  \pre         -
 *  \context     ANY
 *  \reentrant   TRUE for different channels.
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, VIPCMEMIF_CODE)
vIpcMemIf_Tx_Process
(
   PduIdType ChannelId,
   boolean MainFunctionContext
);


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  vIpcMemIf_Tx_GetNrOfChannels()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VIPCMEMIF_LOCAL_INLINE FUNC(PduIdType, VIPCMEMIF_CODE)
vIpcMemIf_Tx_GetNrOfChannels                                                                                            /* PRQA S 3219 */ /* MD_vIpcMemIf_3219 */
(
   void
)
{
   return vIpcMemIf_GetSizeOfTxConfig();
} /* vIpcMemIf_Tx_GetNrOfChannels */


# define VIPCMEMIF_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* VIPCMEMIF_TX_H */

/**********************************************************************************************************************
 *  END OF FILE: vIpcMemIf_Tx.h
 *********************************************************************************************************************/
