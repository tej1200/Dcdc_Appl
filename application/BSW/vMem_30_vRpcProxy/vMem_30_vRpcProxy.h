/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vMem_30_vRpcProxy.h
 *        \brief  vMem_30_vRpcProxy header file
 *
 *      \details  This is the header file of the vMem_30_vRpcProxy. It declares the interfaces of the vMem_30_vRpcProxy. 
 *         \unit  Core
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author          Change Id                Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2021-10-11  vikaisi                                  Initial version
 *  01.01.00  2022-02-13  vikaisi                                  Refactor module to match vMem_30__core
 *  01.02.00  2022-02-14  vikaisi                                  Changed TargetHandleId to vRpcProxy config value
 *  02.00.00  2022-03-10  vikaisi         MCT-315                  Migration to latest vMem_30__core
 *  02.01.00  2022-03-25  vikaisi         MCT-304                  DET include in preproessor switch
 *********************************************************************************************************************/

#if !defined (VMEM_30_VRPCPROXY_H)
# define VMEM_30_VRPCPROXY_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "vMem_30_vRpcProxy_Cfg.h"
# include "vMem_30_vRpcProxy_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VMEM_30_VRPCPROXY_SW_MAJOR_VERSION                    (2u)
# define VMEM_30_VRPCPROXY_SW_MINOR_VERSION                    (1u)
# define VMEM_30_VRPCPROXY_SW_PATCH_VERSION                    (0u)

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define VMEM_30_VRPCPROXY_START_SEC_HEADER_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Global API pointer table */
extern CONST(vMemAccM_vMemApiType, AUTOMATIC) vMem_30_vRpcProxy_FunctionPointerTable;

#define VMEM_30_VRPCPROXY_STOP_SEC_HEADER_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define VMEM_30_VRPCPROXY_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief       Returns the version information. This service is always available.
 *  \details     vMem_30_vRpcProxy_GetVersionInfo() returns version information, vendor ID and AUTOSAR module ID of the component.
 *  \param[out]  versioninfo           Pointer to where to store the version information. Parameter must not be NULL.
 *  \pre         -
 *  \context     TASK
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *  \trace       CREQ-150071
 *********************************************************************************************************************/
FUNC(void, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, VMEM_30_VRPCPROXY_APPL_VAR) VersionInfo);

# define VMEM_30_VRPCPROXY_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */

#endif /* VMEM_30_VRPCPROXY_H */

/**********************************************************************************************************************
 *  END OF FILE: vMem_30_vRpcProxy.h
 *********************************************************************************************************************/

