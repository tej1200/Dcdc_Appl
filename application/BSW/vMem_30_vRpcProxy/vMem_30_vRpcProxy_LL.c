/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  vMem_30_vRpcProxy_LL.c
 *        \brief  vMem_30_vRpcProxy LowLevel source file
 *
 *      \details  See vMem_30_vRpcProxy_LL.h
 *         \unit  LL
 *********************************************************************************************************************/
 
 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#define VMEM_30_VRPCPROXY_LL_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "vstdlib.h"

#include "vRpcProxy_Types.h"
#include "vRpcProxy_Service.h"
#include "vRpcProxy_vMem_30_vRpcProxy.h"

#include "vMem_30_vRpcProxy_LL.h"
#include "vMem_30_vRpcProxy_IntShared.h"
#include "vMem_30_vRpcProxy_DetChecks.h"

#if (VMEM_30_VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
#include "Det.h"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

#if !defined (VMEM_30_VRPCPROXY_LOCAL) /* COV_VMEM_30_VRPCPROXY_COMPATIBILITY */
# define VMEM_30_VRPCPROXY_LOCAL static
#endif

#if !defined (VMEM_30_VRPCPROXY_LOCAL_INLINE) /* COV_VMEM_30_VRPCPROXY_COMPATIBILITY */
# define VMEM_30_VRPCPROXY_LOCAL_INLINE LOCAL_INLINE
#endif

/* vRpcProxyConf_vRpcProxy[Tx/Rx]Service_XXX IDs don't define a "NO_JOB". 0u can be used, as they start with one */
#define VMEM_30_VRPCPROXY_NO_JOB      0u    

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define VMEM_30_VRPCPROXY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Holds the current job type */
VMEM_30_VRPCPROXY_LOCAL VAR(uint8, VMEM_30_VRPCPROXY_VAR_NOINIT) vMem_30_vRpcProxy_CurrentJob;

/* Holds the curent data pointer for vMemAccM */
VMEM_30_VRPCPROXY_LOCAL VAR(vMem_30_vRpcProxy_DataPtrType, VMEM_30_VRPCPROXY_VAR_NOINIT)
vMem_30_vRpcProxy_CurrentDataPtr;

/* Holds the curent data length */
VMEM_30_VRPCPROXY_LOCAL VAR(vMem_30_vRpcProxy_LengthType, VMEM_30_VRPCPROXY_VAR_NOINIT)
vMem_30_vRpcProxy_CurrentDataLength;

/* Holds the current job status */
VMEM_30_VRPCPROXY_LOCAL VAR(vMem_30_vRpcProxy_JobResultType, VMEM_30_VRPCPROXY_VAR_NOINIT) vMem_30_vRpcProxy_JobResult;

#define VMEM_30_VRPCPROXY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define VMEM_30_VRPCPROXY_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VMEM_30_VRPCPROXY_LOCAL VAR(uint8, VMEM_30_VRPCPROXY_VAR_ZERO_INIT) vMem_30_vRpcProxy_ModuleInitialized;               /* PRQA S 3218 */ /* MD_vMem_30_vRpcProxy_Rule8.9_3218 */

#define VMEM_30_VRPCPROXY_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define VMEM_30_VRPCPROXY_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLRead
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLRead(
  vMem_30_vRpcProxy_InstanceIdType InstanceId,
  vMem_30_vRpcProxy_AddressType SourceAddress,
  vMem_30_vRpcProxy_DataPtrType TargetAddressPtr,
  vMem_30_vRpcProxy_LengthType Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;   
 
  {
    /* ----- Implementation ----------------------------------------------- */ 

    /* #10 Only accept job, if currently no job is executed */
    if(vMem_30_vRpcProxy_CurrentJob == VMEM_30_VRPCPROXY_NO_JOB)
    {
      /* Initialization is done BEFORE the vRpcProxy_RPC_Read_Target Call, because otherwise the ServiceStatusCallout could interrupt
       * between calling the vRpcProxy API and initalization of the states, leading to a endless PENDING state.
       */
      
      /* #20 Save TargetAddressPtr and length until the response arrives */
      vMem_30_vRpcProxy_CurrentDataPtr = TargetAddressPtr;
      vMem_30_vRpcProxy_CurrentDataLength = Length;
      /* #30 Set internal state */
      /* Initialize Job Result to PENDING */
      vMem_30_vRpcProxy_JobResult = VMEM_JOB_PENDING;
      /* Save current job type */
      vMem_30_vRpcProxy_CurrentJob = vRpcProxyConf_vRpcProxyRxService_RPC_Read;

      /* #40 Call vRPC_Proxy API */
      retVal = vRpcProxy_RPC_Read_Target(vRpcProxyConf_vRpcProxyTarget_vRpcProxy_Update_Target, SourceAddress, Length);

      /* #50 If vRpcProxy returns E_NOT_OK
               Reset internal state
               Set JobResult to JOB_Failed
               Return E_NOT_OK to vMemAccM */
      if(retVal == E_NOT_OK)
      {
        /* Rest data pointer */
        vMem_30_vRpcProxy_CurrentDataPtr = NULL_PTR;
        /* Set Job Result to JOB_FAILED */
        vMem_30_vRpcProxy_JobResult = VMEM_JOB_FAILED;
        /* Reset current job type */
        vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
      }
    }
  /* #60 Else if vMem_30_vRpcProxy is busy (VMEM_30_VRPCPROXY_NO_JOB != vMem_30_vRpcProxy_CurrentJob)
         return E_NOT_OK to vMemAccM */
  }
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(InstanceId);                                                                       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* vMem_30_vRpcProxy_LLRead() */

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLWrite
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLWrite(
  vMem_30_vRpcProxy_InstanceIdType InstanceId,
  vMem_30_vRpcProxy_AddressType TargetAddress,
  vMem_30_vRpcProxy_ConstDataPtrType SourceAddressPtr,
  vMem_30_vRpcProxy_LengthType Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK; 
 
  {
    /* ----- Implementation ----------------------------------------------- */ 

    /* #10 Only accept job, if currently no job is executed */
    if(vMem_30_vRpcProxy_CurrentJob == VMEM_30_VRPCPROXY_NO_JOB)
    {
      /* Initialization is done BEFORE the vRpcProxy Call, because otherwise the ServiceStatusCallout could interrupt
       * between calling the vRpcProxy API and initalization of the states, leading to a endless PENDING state.
       */

      /* #20 Set internal state */
      /* Initialize Job Result to PENDING */
      vMem_30_vRpcProxy_JobResult = VMEM_JOB_PENDING;
      /* Save current job type */
      vMem_30_vRpcProxy_CurrentJob = vRpcProxyConf_vRpcProxyRxService_RPC_Write;

      /* #30 Call vRPC_Proxy API */
      retVal = vRpcProxy_RPC_Write_Target(vRpcProxyConf_vRpcProxyTarget_vRpcProxy_Update_Target,
                                          TargetAddress,
                                          (const uint8*) SourceAddressPtr,                                             /* PRQA S 0316 */ /* MD_vMem_30_vRpcProxy_Rule11.5_0316 */
                                          (uint16)Length);                                                             

      /* #40 If vRpcProxy returns E_NOT_OK
               Reset internal state
               Set JobResult to JOB_Failed
               Return E_NOT_OK to vMemAccM */
      if(retVal == E_NOT_OK)
      {
        /* Set Job Result to JOB_FAILED */
        vMem_30_vRpcProxy_JobResult = VMEM_JOB_FAILED;
        /* Reset current job type */
        vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
      }    
    }
  /* #60 Else if vMem_30_vRpcProxy is busy (VMEM_30_VRPCPROXY_NO_JOB != vMem_30_vRpcProxy_CurrentJob)
         return E_NOT_OK to vMemAccM */
  }
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(InstanceId);                                                                       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* vMem_30_vRpcProxy_LLWrite() */

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLErase
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLErase(
  vMem_30_vRpcProxy_InstanceIdType InstanceId,
  vMem_30_vRpcProxy_AddressType TargetAddress,
  vMem_30_vRpcProxy_LengthType Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK; 
 
  {
    /* ----- Implementation ----------------------------------------------- */    

    /* #10 Only accept job, if currently no job is executed */
    if(vMem_30_vRpcProxy_CurrentJob == VMEM_30_VRPCPROXY_NO_JOB)
    {
      /* Initialization is done BEFORE the vRpcProxy Call, because otherwise the ServiceStatusCallout could interrupt
       * between calling the vRpcProxy API and initalization of the states, leading to a endless PENDING state.
       */

      /* #20 Set internal state */
      /* Initialize Job Result to PENDING */
      vMem_30_vRpcProxy_JobResult = VMEM_JOB_PENDING;
      /* Save current job type */
      vMem_30_vRpcProxy_CurrentJob = vRpcProxyConf_vRpcProxyRxService_RPC_Erase;

      /* #30 Call vRPC_Proxy API */
      retVal = vRpcProxy_RPC_Erase_Target(vRpcProxyConf_vRpcProxyTarget_vRpcProxy_Update_Target, TargetAddress, Length);

      /* #40 If vRpcProxy returns E_NOT_OK
               Reset internal state
               Set JobResult to JOB_Failed
               Return E_NOT_OK to vMemAccM */
      if(retVal == E_NOT_OK)
      {
        /* Set Job Result to JOB_FAILED */
        vMem_30_vRpcProxy_JobResult = VMEM_JOB_FAILED;
        /* Reset current job type */
        vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
      }
    }
  /* #60 Else if vMem_30_vRpcProxy is busy (VMEM_30_VRPCPROXY_NO_JOB != vMem_30_vRpcProxy_CurrentJob)
         return E_NOT_OK to vMemAccM */
  }
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(InstanceId);                                                                       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* vMem_30_vRpcProxy_LLErase() */

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLIsBlank
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLIsBlank(
  vMem_30_vRpcProxy_InstanceIdType InstanceId,
  vMem_30_vRpcProxy_AddressType TargetAddress,
  vMem_30_vRpcProxy_LengthType Length)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK; 
 
  {
    /* ----- Implementation ----------------------------------------------- */    

    /* #10 Only accept job, if currently no job is executed */
    if(vMem_30_vRpcProxy_CurrentJob == VMEM_30_VRPCPROXY_NO_JOB)
    {
      /* Initialization is done BEFORE the vRpcProxy Call, because otherwise the ServiceStatusCallout could interrupt
       * between calling the vRpcProxy API and initalization of the states, leading to a endless PENDING state.
       */

      /* #20 Set internal state */
      /* Initialize Job Result to PENDING */
      vMem_30_vRpcProxy_JobResult = VMEM_JOB_PENDING;
      /* Save current job type */
      vMem_30_vRpcProxy_CurrentJob = vRpcProxyConf_vRpcProxyRxService_RPC_IsBlank;

      /* #30 Call vRPC_Proxy API */
      retVal = vRpcProxy_RPC_IsBlank_Target(vRpcProxyConf_vRpcProxyTarget_vRpcProxy_Update_Target, TargetAddress, Length);

      
      /* #40 If vRpcProxy returns E_NOT_OK
               Reset internal state
               Set JobResult to JOB_Failed
               Return E_NOT_OK to vMemAccM */
      if(retVal == E_NOT_OK)
      {
        /* Set Job Result to JOB_FAILED */
        vMem_30_vRpcProxy_JobResult = VMEM_JOB_FAILED;
        /* Reset current job type */
        vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
      }      
    }
  /* #60 Else if vMem_30_vRpcProxy is busy (VMEM_30_VRPCPROXY_NO_JOB != vMem_30_vRpcProxy_CurrentJob)
         return E_NOT_OK to vMemAccM */
  }
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(InstanceId);                                                                       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
} /* vMem_30_vRpcProxy_LLIsBlank() */

/**********************************************************************************************************************
 * vMem_30_vRpcProxy_LLGetJobResult
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(vMem_30_vRpcProxy_JobResultType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLGetJobResult(
    vMem_30_vRpcProxy_InstanceIdType InstanceId)
{
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(InstanceId);                                                                       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  /* #10 Return current job result */
  return vMem_30_vRpcProxy_JobResult;
} /* vMem_30_vRpcProxy_LLGetJobResult() */

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLProcessing
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLProcessing(void)
{
}

/**********************************************************************************************************************
 *  vMem_30_vRpcProxy_LLInit
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, VMEM_30_VRPCPROXY_CODE) vMem_30_vRpcProxy_LLInit(void)
{
  /* #10 Initialize all variables with default values. */
  vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
  vMem_30_vRpcProxy_CurrentDataPtr = NULL_PTR;
  vMem_30_vRpcProxy_JobResult = VMEM_JOB_OK;

  vMem_30_vRpcProxy_ModuleInitialized = VMEM_30_VRPCPROXY_INIT;
  return E_OK;
}

/**********************************************************************************************************************
 vMem_30_vRpcProxy_ServiceStatusCallout
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_APPL_CODE) vMem_30_vRpcProxy_ServiceStatusCallout(uint16 ServiceId,
              uint8 TargetId,
              uint8 TargetGroupId,
              TxService_StatusType Result)
{
  /* This timeout handling is the same for all services, therefore ServiceId/TargetId/TargetGroupId is not evaluated. */
  /* #10 If the callout result is anthing else then VRPCPROXY_E_POSITIVE_ACK */
  if (Result != VRPCPROXY_E_POSITIVE_ACK)
  {
    /* #20 The transmission or reception failed
           Set JobResult to VMEM_JOB_FAILED
           Reset internal state */
    vMem_30_vRpcProxy_JobResult = VMEM_JOB_FAILED;
    vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
  }

  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(ServiceId);                                                                        /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(TargetId);                                                                         /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(TargetGroupId);                                                                    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

} /* vMem_30_vRpcProxy_ServiceStatusCallout() */

/**********************************************************************************************************************
 vMem_30_vRpcProxy_RPC_Read
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_APPL_CODE) vMem_30_vRpcProxy_RPC_Read(vRpcProxy_SourcesIterType sourceHandleId,                   /* PRQA S 6060 */ /* MD_MSR_STPAR */
              vRpcProxy_CTargetsIterType targetHandleId, 
              uint16 responseOfServiceId, 
              uint8 serviceExecutionStatus, 
              uint32 JobResult, 
              P2VAR(uint8, AUTOMATIC, VRPCPROXY_APPL_VAR) Data,                                                        /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
              uint16 Data_Length) 
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VMEM_30_VRPCPROXY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Verify that module is initialized */
  if (vMem_30_vRpcProxy_ModuleInitialized != VMEM_30_VRPCPROXY_INIT)
  {
    errorId = VMEM_30_VRPCPROXY_E_UNINIT;
  }
  else
#endif
  {
    /* ----- Implementation ----------------------------------------------- */    
    
    /* #20 Verify the response matches the current job */
    if(responseOfServiceId == vMem_30_vRpcProxy_CurrentJob)
    {
      /* #25 If Data_Length == vMem_30_vRpcProxy_CurrentDataLength */
      if(Data_Length == vMem_30_vRpcProxy_CurrentDataLength)
      { 
        /* #30 If JobResult is JOB_OK or READ_CORRECTED_ERRORS */
        if ( ((vMem_30_vRpcProxy_JobResultType)JobResult == VMEM_JOB_OK) ||                                            /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
            ((vMem_30_vRpcProxy_JobResultType)JobResult == VMEM_READ_CORRECTED_ERRORS)                                 /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
          )
        {          
          /* #40 Copy data from vRpcProxy memory to saved vMemAccM memory */
          VStdLib_MemCpy(vMem_30_vRpcProxy_CurrentDataPtr, Data, Data_Length);                                         /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */          
        }
      
        /* #50 Set Job Result */
        vMem_30_vRpcProxy_JobResult = (vMem_30_vRpcProxy_JobResultType)JobResult;                                      /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
        /* #60 Reset current job */
        vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
      }
    }    
  }
  /* ----- Development Error Report --------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VMEM_30_VRPCPROXY_E_NO_ERROR)
  {
    (void)Det_ReportError(VMEM_30_VRPCPROXY_MODULE_ID,
                          VMEM_30_VRPCPROXY_INSTANCE_ID_DET,
                          VMEM_30_VRPCPROXY_SID_RPC_READ,
                          errorId);
  }
#else
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(errorId);                                                                          /* PRQA S 2983 */ /* MD_MSR_DummyStmt */
#endif

  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(targetHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(serviceExecutionStatus);                                                           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

} /* vMem_30_vRpcProxy_RPC_Read() */

/**********************************************************************************************************************
 vMem_30_vRpcProxy_RPC_Write
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_APPL_CODE) vMem_30_vRpcProxy_RPC_Write(vRpcProxy_SourcesIterType sourceHandleId, 
              vRpcProxy_CTargetsIterType targetHandleId, 
              uint16 responseOfServiceId,
              uint8 serviceExecutionStatus,
              uint32 JobResult)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VMEM_30_VRPCPROXY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Verify that module is initialized */
  if (vMem_30_vRpcProxy_ModuleInitialized != VMEM_30_VRPCPROXY_INIT)
  {
    errorId = VMEM_30_VRPCPROXY_E_UNINIT;
  }
  else
#endif
  {
    /* ----- Implementation ----------------------------------------------- */    
    
    /* #20 Verify the response matches the current job */
    if (responseOfServiceId == vMem_30_vRpcProxy_CurrentJob)
    {
      /* #30 Set Job Result */
      vMem_30_vRpcProxy_JobResult = (vMem_30_vRpcProxy_JobResultType)JobResult;                                        /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
      /* #40 Reset current job */
      vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
    }    
  }
  /* ----- Development Error Report --------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VMEM_30_VRPCPROXY_E_NO_ERROR)
  {
    (void)Det_ReportError(VMEM_30_VRPCPROXY_MODULE_ID,
                          VMEM_30_VRPCPROXY_INSTANCE_ID_DET,
                          VMEM_30_VRPCPROXY_SID_RPC_WRITE,
                          errorId);
  }
#else
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(errorId);                                                                          /* PRQA S 2983 */ /* MD_MSR_DummyStmt */
#endif

  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(targetHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(serviceExecutionStatus);                                                           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

} /* vMem_30_vRpcProxy_RPC_Write() */

/**********************************************************************************************************************
 vMem_30_vRpcProxy_RPC_Erase
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_APPL_CODE) vMem_30_vRpcProxy_RPC_Erase(vRpcProxy_SourcesIterType sourceHandleId, 
              vRpcProxy_CTargetsIterType targetHandleId, 
              uint16 responseOfServiceId, 
              uint8 serviceExecutionStatus, 
              uint32 JobResult)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VMEM_30_VRPCPROXY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Verify that module is initialized */
  if (vMem_30_vRpcProxy_ModuleInitialized != VMEM_30_VRPCPROXY_INIT)
  {
    errorId = VMEM_30_VRPCPROXY_E_UNINIT;
  }
  else
#endif
  {
    /* ----- Implementation ----------------------------------------------- */    
    
    /* #20 Verify the response matches the current job */
    if (responseOfServiceId == vMem_30_vRpcProxy_CurrentJob)
    {
      /* #30 Set Job Result */
      vMem_30_vRpcProxy_JobResult = (vMem_30_vRpcProxy_JobResultType)JobResult;                                        /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
      /* #40 Reset current job */
      vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
    }    
  }
  /* ----- Development Error Report --------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VMEM_30_VRPCPROXY_E_NO_ERROR)
  {
    (void)Det_ReportError(VMEM_30_VRPCPROXY_MODULE_ID,
                          VMEM_30_VRPCPROXY_INSTANCE_ID_DET,
                          VMEM_30_VRPCPROXY_SID_RPC_ERASE,
                          errorId);
  }
#else
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(errorId);                                                                          /* PRQA S 2983 */ /* MD_MSR_DummyStmt */
#endif

  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(targetHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(serviceExecutionStatus);                                                           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

} /* vMem_30_vRpcProxy_RPC_Erase() */

/**********************************************************************************************************************
 vMem_30_vRpcProxy_RPC_IsBlank
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, VRPCPROXY_APPL_CODE) vMem_30_vRpcProxy_RPC_IsBlank(vRpcProxy_SourcesIterType sourceHandleId, 
              vRpcProxy_CTargetsIterType targetHandleId, 
              uint16 responseOfServiceId, 
              uint8 serviceExecutionStatus, 
              uint32 JobResult)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = VMEM_30_VRPCPROXY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Verify that module is initialized */
  if (vMem_30_vRpcProxy_ModuleInitialized != VMEM_30_VRPCPROXY_INIT)
  {
    errorId = VMEM_30_VRPCPROXY_E_UNINIT;
  }
  else
#endif
  {
    /* ----- Implementation ----------------------------------------------- */    
    
    /* #20 Verify the response matches the current job */
    if (responseOfServiceId == vMem_30_vRpcProxy_CurrentJob)
    {
      /* #30 Set Job Result */
      vMem_30_vRpcProxy_JobResult = (vMem_30_vRpcProxy_JobResultType)JobResult;                                        /* PRQA S 4342 */ /* MD_vMem_30_vRpcProxy_Rule10.5_4342 */
      /* #40 Reset current job */
      vMem_30_vRpcProxy_CurrentJob = VMEM_30_VRPCPROXY_NO_JOB;
    }    
  }
  /* ----- Development Error Report --------------------------------------- */
#if (VMEM_30_VRPCPROXY_DEV_ERROR_REPORT == STD_ON)
  if (errorId != VMEM_30_VRPCPROXY_E_NO_ERROR)
  {
    (void)Det_ReportError(VMEM_30_VRPCPROXY_MODULE_ID,
                          VMEM_30_VRPCPROXY_INSTANCE_ID_DET,
                          VMEM_30_VRPCPROXY_SID_RPC_IS_BLANK,
                          errorId);
  }
#else
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(errorId);                                                                          /* PRQA S 2983 */ /* MD_MSR_DummyStmt */
#endif

  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(sourceHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(targetHandleId);                                                                   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  VMEM_30_VRPCPROXY_DUMMY_STATEMENT(serviceExecutionStatus);                                                           /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

} /* vMem_30_vRpcProxy_RPC_IsBlank() */

#define VMEM_30_VRPCPROXY_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  MISRA JUSTIFICATIONS
 **********************************************************************************************************************/
/* Justification for module-specific MISRA deviations:

  MD_vMem_30_vRpcProxy_Rule10.5_4342:
    Reason:     The cast of 'essentially unsigned' type (unsigned int) to enum type 'vMemAccM_vMemJobResultType' is safe.
                There are no restrictions according to the alignment.
    Risk:       None.
    Prevention: None.
  
  MD_vMem_30_vRpcProxy_Rule11.5_0316:
    Reason:     The conversion from pointer to void into pointer to object is safe. There are no restrictions according 
                to the alignment.
    Risk:       None.
    Prevention: None.

  MD_vMem_30_vRpcProxy_Rule8.9_3218:
    Reason:     The variable vMem_30_vRpcProxy_ModuleInitialized is used for Det error checks and therefore has to be
                global.
    Risk:       None.
    Prevention: None.
*/

/**********************************************************************************************************************
 *  END OF FILE: vMem_30_vRpcProxy_LL.c
 *********************************************************************************************************************/
