/* ********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 */
/*!
 *  \addtogroup Dem_Satellite
 *  \{
 *  \file       Dem_Satellite_Implementation.h
 *  \brief      Implementation file for the MICROSAR Dem
 *  \details    Implementation of Satellite Logical Unit
 *********************************************************************************************************************/

/* ********************************************************************************************************************
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  REFER TO DEM.H
 *********************************************************************************************************************/

#if !defined (DEM_SATELLITE_IMPLEMENTATION_H)
#define DEM_SATELLITE_IMPLEMENTATION_H

/* ********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/

                          /* Include implementation belonging to child units */
/* ------------------------------------------------------------------------- */
#include "Dem_SatelliteData_Implementation.h"
#include "Dem_SatelliteInfo_Implementation.h"
#include "Dem_Debounce_Implementation.h"
#include "Dem_Monitor_Implementation.h"
#include "Dem_SatelliteIF_Implementation.h"
#include "Dem_SatelliteIF_SvcImplementation.h"

#endif /* DEM_SATELLITE_IMPLEMENTATION_H */

/*!
 * \}
 */
/* ********************************************************************************************************************
 *  END OF FILE: Dem_Satellite_Implementation.h
 *********************************************************************************************************************/
