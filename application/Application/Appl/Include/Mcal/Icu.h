/*created by nichte1*/
#ifndef ICU_ADAPT_H
#define ICU_ADAPT_H

#include "Icu_17_TimerIp.h"
#define Icu_ChannelType Icu_17_TimerIp_ChannelType
#define Icu_DisableNotification Icu_17_TimerIp_DisableNotification
#define Icu_EnableNotification Icu_17_TimerIp_EnableNotification
#define Icu_ConfigType Icu_17_TimerIp_ChannelConfigType


#endif
