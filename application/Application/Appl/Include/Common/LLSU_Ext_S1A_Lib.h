/*******************************************************************************/
/* CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)                                  */
/*                                                                             */
/* This is an unpublished work of authorship, which contains trade secrets,    */
/* created in 2009. Hella KGaA Hueck & Co. owns all rights to this work and    */
/* intends to maintain it in confidence to preserve its trade secret status.   */
/* Hella KGaA Hueck & Co. reserves the right, under the copyright laws of      */
/* Germany or those of any other country that may have jurisdiction, to        */
/* protect this work as an unpublished work, in the event of an inadvertent or */
/* deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves   */
/* its rights under all copyright laws to protect this work as a published     */
/* work, when appropriate. Those having access to this work may not copy it,   */
/* use it, modify it or disclose the information contained in it without the   */
/* written authorization of Hella KGaA Hueck & Co..                            */
/*                                                                             */
/*******************************************************************************/

/***********************************************************************************************/
/*  Filename    :   $Source: LLSU_Ext_S1A_Lib.h $                                               */
/*  Author      :   $Author: Kumar Anil kumaan8 (kumaan8) $                              */
/*  Date        :   $Date: 2021/02/03 15:17:28IST $                                           */
/*  Version     :   $Revision: 1.1 $                                                       */
/*  Status      :   $State: IN_WORK $                                                          */
/*  Description :   This file contains definitions for RAM-initialization       	           */
/***********************************************************************************************/
/*  Changes     :                                                                              */
/*  $Log        :   LLSU_Ext_S1A_Lib.h  $                                                       */
/***********************************************************************************************/
/*                                                                                             */
/*  Module                                                                                     */
/*  Release                                                                                    */
/*  Version     Date          Author    ChangeID Summary                                       */
/*                                                                                             */
/*  alpha     04.01.2021      schupe6                                                          */
/*                                                                                             */
/***********************************************************************************************/

#ifndef LLSU_EXT_S1A_LIB_H_
#define LLSU_EXT_S1A_LIB_H_

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "Std_Types.h"
//#include "regtc36x.sfr"

/**********************************************************************************************************************
  Global function prototypes
**********************************************************************************************************************/
void   Appl_UnlockInit(uint32 coreID);
void   Appl_LockInit(uint32 coreID);
void   LLSU_Ext_S1A_Lib_SafetyWdgSetReload(uint16 ReloadValue);

#define  APPLLIB_SAFETY_WATCHDOG_INDEX  (sint32) -1

#endif /*LLSU_EXT_S1A_LIB_H_*/
