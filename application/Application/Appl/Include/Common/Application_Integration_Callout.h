/**************************************************************************
**                                                                        *
**  FILE        :  Application_Integration_Callout.h                                               *
**                                                                        *
**  DESCRIPTION :                                                         *
**      modify this file untill there was a change in the start          *
**      up of the application.                                            *
**                                                                        *
**************************************************************************/
/***************************   Header Inclusions  ******************************/

#include "EcuM.h"
#include "Rte_Type.h"
#include "Rte_Main.h"



/***************************   Header Inclusions   ******************************/
extern void Mcal_SetModetoUser();
