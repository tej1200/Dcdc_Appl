
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        MemMap section definition to locate (link) data or code in memory
 *  \details      This file contains compiler specific section definition for every segment that can be mapped
 *                within the Flash Bootloader to a specific location.
 *
 *  \note
 *                Please note, that this file contains a collection of definitions to be used with the Flash Bootloader.
 *                This code may influence the behavior of the Bootloader in principle. Therefore, great care
 *                must be taken to verify the correctness of the implementation.
 *
 *                The contents of the originally delivered files are only examples resp. implementation proposals.
 *                With regard to the fact that these definitions are meant for demonstration purposes only,
 *                Vector Informatik's liability shall be expressly excluded in cases of ordinary negligence, to the
 *                extent admissible by law or statute.
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2023 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  THIS IS A Not A GENERATED FILE
 *  Generator version : X.X.X.XX
 *********************************************************************************************************************/

#if defined( UCBBM_HEADER_START_SEC_CONST )
#pragma protect 
#pragma section nearrom "BMHD0_UCB"
#pragma section farrom "BMHD0_UCB"
# undef UCBBM_HEADER_START_SEC_CONST
# undef FBL_MEMMAP_ERROR
#endif

#if defined( UCBBM_HEADER_STOP_SEC_CONST )
#pragma endprotect
#pragma section nearrom restore
#pragma section farrom restore
# undef UCBBM_HEADER_STOP_SEC_CONST
# undef FBL_MEMMAP_ERROR
#endif

#if defined( UCBBM_HEADER1_START_SEC_CONST )
#pragma protect 
#pragma section nearrom "BMHD1_UCB"
#pragma section farrom "BMHD1_UCB"
# undef UCBBM_HEADER_START_SEC_CONST
# undef FBL_MEMMAP_ERROR
#endif

#if defined( UCBBM_HEADER1_STOP_SEC_CONST )
#pragma endprotect
#pragma section nearrom restore
#pragma section farrom restore
# undef UCBBM_HEADER_STOP_SEC_CONST
# undef FBL_MEMMAP_ERROR
#endif