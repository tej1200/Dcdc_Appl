/***************************************************************************** 
 *  COPYRIGHT 
 *  ---------------------------------------------------------------------------- 
 *  \verbatim 
 * This software is copyright protected and proprietary to Netwalk GmbH. 
 * Netwalk GmbH grants to you only those rights as set out in the license         
	All other rights remain with Netwalk GmbH. 
 *  \endverbatim 
 *  --------------------------------------------------------------------------- 
 *  LICENSE 
 *  ---------------------------------------------------------------------------- 
 *  Module: - 
 *  Program: -
 *  Customer: Intenal 
 *  Expiry Date: 11.09.2023 
 *  Ordered Derivat.: - 
 *  License Scope: - 
 *  --------------------------------------------------------------------------- 
 *  FILE DESCRIPTION 
 *  ---------------------------------------------------------------------------- 
 *              File: SWC_MaxValEngDir.c 
 *            Author: Revanth Kumar B 
 *       Description: Max values and Energy direction  
 *              Date: 11.09.2023 
 *           Project: DCDC 
 *        Additional: - 
 *           Company: Netwalk GmbH. 
 ******************************************************************************

	Code Template Snippet 1: Start of File Tag containing Basic Information 	

	End of File Tag 
 ****************************************************************************** 
 *  END OF FILE: SWC_MaxValEngDir.c
 *****************************************************************************
 */

#include <math.h>
#include <stdio.h>
#include "Std_Types.h"


#define C_STDBYMODE_VOLT												0u
#define C_STDBYMODE_CUR													0u
#define C_BN12SWOFFLOW_VOLT           									8u
#define C_BN12DERATE1LOW_INTRCPT       									(-480u)
#define C_BN12DERATE1LOW_SLOPE        									60u

#define C_BN12DERATELOW2_VOLT           								9u
#define C_BN12DERATE2LOW_INTRCPT       									(-147u)
#define C_BN12DERATE2LOW_SLOPE        									23u

#define C_BN12MAXPWRSTRT_VOLT        									10u
#define C_BN12MAXPWREND_VOLT           									14u
#define C_BN12MAXALWDLMT_CUR           									83u

#define C_BN12DERATEHIGH1_VOLT          								15u
#define C_BN12DERATE1HIGH_INTRCPT       								405u
#define C_BN12DERATE1HIGH_SLOPE        									(-23u)

#define C_BN12SWOFFHIGH_VOLT           									16u
#define C_BN12DERATE2HIGH_INTRCPT       								960u
#define C_BN12DERATE2HIGH_SLOPE        									(-60u)

#define C_MAXPWRLMT_PWR													1040u

#define C_STDBYMODE_ST             										0u
#define C_DIAGMODE_ST              										1u
#define C_COMMANDMODE_ST           										2u

#define C_TMPLIMTSHUTDOWN_TMP      										84u
#define C_TMPLIMTDERATE_TMP      										74u
#define C_TEMPDERATE_INTRCPT       										8400u
#define C_TEMPDERATE_SLOPE        										(-100u)


#define C_INPROGRESS_ST            										2u

#define RUN            													2u
#define BUCK            												55u
#define NO_DIRECTION            										2u
#define par_F_BUCK_MODE_DEACTIVATION_CFG            					0u
#define par_U_ERROR_RESET_BUCK_UN12_OV_CFG            					16u
#define par_U_ERROR_RESET_BUCK_UN12_UV_CFG            					8u

#define NO_ERROR														0u
#define ERROR															1u






typedef struct Int_RequestValuesInfo_st
{
	uint8	control_b;
	uint16	voltage_u16;
	uint8	current_u8;
	uint8	mode_u8;
}Int_RequestValuesInfo_t;


typedef struct Diag_RoutineRequest_st
{
	uint8	RoutineRequest_u8;
	uint16	voltage_u16;
	uint8	current_u8;
	uint8	EnergyDir_u8;
}Diag_RoutineRequest_t;

Int_RequestValuesInfo_t readfeedback(void);