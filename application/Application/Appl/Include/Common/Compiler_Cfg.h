/*
 * This File is a template for the Compiler_Cfg.h
 *
 * Adapt it accordingly.
 *
 * For best compile time optimization, do it manually and add only the defines really required.
 *
 * Order the defines alphabetically to easily find them later on.
 *
 */

#ifndef COMPILER_CFG_H
#define COMPILER_CFG_H

#include "Rte_Compiler_Cfg.h"

#define AUTOSAR_COMSTACKDATA

#define REGSPACE
#define MSR_REGSPACE

/* due to compatibility to ASR 2.1 */
#define STATIC static
#define EXTERN	extern
#define Boolean boolean
#define _STATIC_   STATIC
#define _INLINE_   INLINE

#define _TASKING_C_TRICORE_ 1

#if !defined (ECUM_LOCAL)
# define ECUM_LOCAL static
#endif

/**************************************************
* ADC
**************************************************/

#define ADC_CODE
#define ADC_VAR_NOINIT
#define ADC_VAR_POWER_ON_INIT
#define ADC_VAR_FAST
#define ADC_VAR
#define ADC_CONST
#define ADC_APPL_DATA
#define ADC_APPL_CONST
#define ADC_APPL_CODE
#define ADC_APPL_REGSPACE

/**************************************************
* AIN_S
**************************************************/

#define AIN_S_VAR
#define AIN_S_CODE
#define Ain_S_DATA

/**************************************************
* AINH_S4
**************************************************/

#define AINH_S4_CODE
#define AINH_S4_CONST
#define AINH_S4_CONST_PBCFG
#define AINH_S4_VAR
#define AINH_S4_APPL_CONST
#define AINH_S4_PBCFG  AINH_S4_CONST_PBCFG

/**************************************************
* ACTTBL_S1
**************************************************/

#define ACTTBL_S1_CODE
#define ACTTBL_S1_CONST
#define ACTTBL_S1_CONST_PBCFG
#define ACTTBL_S1_VAR
#define ACTTBL_S1_APPL_CONST
#define ACTTBL_S1_APPL_DATA
#define ACTTBL_S1_PBCFG  ACTTBL_S1_CONST_PBCFG

/**************************************************
* AVTP
**************************************************/

#define AVTP_CODE
#define AVTP_CONST
#define AVTP_VAR_NO_INIT
#define AVTP_VAR_CLEARED
#define AVTP_APPL_DATA
#define AVTP_APPL_VAR

/**************************************************
* BSWM
**************************************************/

#define BSWM_CODE
#define BSWM_CONST
#define BSWM_PBCFG
#define BSWM_APPL_DATA
#define BSWM_VAR_NO_INIT
#define BSWM_VAR_PBCFG

/**************************************************
* CDD
**************************************************/

#define CDD_CODE
#define CDD_APPL_DATA
#define Cdd_Avb_CODE

/**************************************************
* CAN
**************************************************/

#define CAN_CODE
#define CAN_STATIC_CODE
#define CAN_CONST
#define CAN_CONST_PBCFG
#define CAN_VAR_NOINIT
#define CAN_VAR_INIT
#define CAN_VAR_PBCFG
#define CAN_INT_CTRL
#define CAN_REG_CANCELL
#define CAN_RX_TX_DATA
#define CAN_APPL_CODE
#define CAN_APPL_CONST
#define CAN_APPL_VAR
#define CAN_VAR_FAR
#define CAN_VAR_NEAR
#define CAN_PBCFG  CAN_CONST_PBCFG

/**************************************************
* CANIF
**************************************************/

#define CANIF_CODE
#define CANIF_CODE_FAST
#define CANIF_CODE_ISR
#define CANIF_CONST
#define CANIF_CONST_FAST
#define CANIF_PBCFG
#define CANIF_VAR_PBCFG
#define CANIF_VAR_INIT
#define CANIF_VAR_NOINIT
#define CANIF_VAR_ZERO_INIT
#define CANIF_VAR_INIT_FAST
#define CANIF_VAR_NOINIT_FAST
#define CANIF_VAR_ZERO_INIT_FAST
#define CANIF_VAR_INIT_NOCACHE
#define CANIF_VAR_NOINIT_NOCACHE
#define CANIF_VAR_ZERO_INIT_NOCACHE
#define CANIF_APPL_CODE
#define CANIF_APPL_VAR
#define CANIF_APPL_PBCFG
#define CANIF_VAR_STACK
#define CANIF_APPL_STATE_VAR   CANIF_APPL_VAR
#define CANIF_APPL_MSG_VAR     CANIF_APPL_VAR
#define CANIF_CBK_VAR          CANIF_APPL_VAR
#define CANIF_CBK_DRV_VAR      CANIF_CBK_VAR
#define CANIF_CBK_TRCV_CODE     CANIF_APPL_CODE
#define CANIF_CBK_CANDRV_CODE   CANIF_APPL_CODE
#define CANIF_APPL_STATE_CODE   CANIF_APPL_CODE
#define CANIF_APPL_MSG_CODE     CANIF_APPL_CODE
#define CANIF_UL_STANDARD_VAR    CANIF_APPL_VAR
#define CANIF_UL_ADVANCED_VAR    CANIF_APPL_VAR
#define CANIF_UL_OSEKNM_VAR      CANIF_APPL_VAR

/**************************************************
* CANNM
**************************************************/

#define CANNM_CODE
#define CANNM_CONST
#define CANNM_PBCFG
#define CANNM_VAR_NOINIT
#define CANNM_VAR_INIT
#define CANNM_VAR_ZERO_INIT
#define CANNM_VAR_PBCFG
#define CANNM_APPL_VAR

/**************************************************
* CANSM
**************************************************/

#define CANSM_CODE
#define CANSM_CONST
#define CANSM_APPL_CODE
#define CANSM_VAR_NOINIT
#define CANSM_VAR_ZERO_INIT
#define CANSM_APPL_VAR
#define CANSM_CONST_FAST
#define CANSM_VAR_NOINIT_FAST
#define CANSM_VAR_ZERO_INIT_FAST

/**************************************************
* CANTRCV_30_TJA1043
**************************************************/

#define CANTRCV_30_TJA1043_CODE
#define CANTRCV_30_TJA1043_CODE_FAST
#define CANTRCV_30_TJA1043_CODE_ISR
#define CANTRCV_30_TJA1043_CONST
#define CANTRCV_30_TJA1043_CONST_FAST
#define CANTRCV_30_TJA1043_PBCFG
#define CANTRCV_30_TJA1043_VAR_PBCFG
#define CANTRCV_30_TJA1043_VAR_INIT
#define CANTRCV_30_TJA1043_VAR_NOINIT
#define CANTRCV_30_TJA1043_APPL_VAR
#define CANTRCV_30_TJA1043_APPL_CODE
#define CANTRCV_30_TJA1043_VAR_ZERO_INIT
#define CANTRCV_30_TJA1043_VAR_INIT_FAST
#define CANTRCV_30_TJA1043_VAR_NOINIT_FAST
#define CANTRCV_30_TJA1043_VAR_ZERO_INIT_FAST
#define CANTRCV_30_TJA1043_VAR_INIT_NOCACHE
#define CANTRCV_30_TJA1043_VAR_NOINIT_NOCACHE
#define CANTRCV_30_TJA1043_VAR_ZERO_INIT_NOCACHE

/**************************************************
* CANTSYN
**************************************************/

#define CANTSYN_CONST
#define CANTSYN_CODE
#define CANTSYN_VAR_ZERO_INIT
#define CANTSYN_VAR_NOINIT
#define CANTSYN_APPL_VAR
#define CANTSYN_APPL_DATA

/**************************************************
* CANTSYN
**************************************************/

#define CANXCP_APPL_VAR

/**************************************************
* COM
**************************************************/

#define COM_CONST
#define COM_PBCFG
#define COM_VAR_PBCFG
#define COM_CODE
#define COM_VAR_NO_INIT
#define COM_VAR_CLEARED
#define COM_VAR_INIT
#define COM_APPL_CODE
#define COM_APPL_VAR
#define COM_APPL_DATA

/**************************************************
* COMM
**************************************************/

#define COMM_CODE
#define COMM_CONST
#define COMM_PBCFG
#define COMM_VAR_NO_INIT
#define COMM_VAR_PBCFG
#define COMM_VAR_CLEARED
#define COMM_APPL_VAR
#define COMM_NVM_DATA
#define COMM_NVM_DATA_NO_INIT

/**************************************************
* CRYIF
**************************************************/

#define CRYIF_CODE
#define CRYIF_CONST
#define CRYIF_VAR_ZERO_INIT
#define CRYIF_APPL_VAR
#define CRYIF_APPL_DATA

/**************************************************
* CSM
**************************************************/

#define CSM_CODE
#define CSM_APPL_CODE
#define CSM_RTE_CODE
#define CSM_CONST
#define CSM_VAR_NOINIT
#define CSM_VAR_ZERO_INIT
#define CSM_APPL_VAR
#define CSM_APPL_CONST

/**************************************************
* CRC
**************************************************/

#define CRC_APPL_DATA
#define CRC_PRIVATE_CODE
#define CRC_CODE
#define CRC_CONST

/**************************************************
* CRYPTO_30_LIBCV
**************************************************/

#define CRYPTO_30_LIBCV_CODE
#define CRYPTO_30_LIBCV_APPL_CODE
#define CRYPTO_30_LIBCV_CONST
#define CRYPTO_30_LIBCV_VAR_NOINIT
#define CRYPTO_30_LIBCV_VAR_ZERO_INIT
#define CRYPTO_30_LIBCV_APPL_VAR
#define CRYPTO_30_LIBCV_APPL_DATA
#define CRYPTO_30_LIBCV_CRYPTOCV_APPL_VAR

/**************************************************
* CRYPTO_30_VHSM
**************************************************/

#define CRYPTO_30_VHSM_CODE
#define CRYPTO_30_VHSM_APPL_CODE
#define CRYPTO_30_VHSM_CONST
#define CRYPTO_30_VHSM_VAR_NOINIT
#define CRYPTO_30_VHSM_VAR_INIT
#define CRYPTO_30_VHSM_VAR_ZERO_INIT
#define CRYPTO_30_VHSM_APPL_VAR
#define CRYPTO_30_VHSM_APPL_DATA

/**************************************************
* DBG
**************************************************/

#define DBG_CODE
#define DBG_CONST
#define DBG_VAR_INIT
#define DBG_VAR_NOINIT
#define DBG_VAR_ZERO_INIT
#define DBG_APPL_VAR
#define DBG_PBCFG

/**************************************************
* DCM
**************************************************/

#define DCM_CODE
#define DCM_CONST
#define DCM_CAL_PRM
#define DCM_APPL_CODE
#define DCM_APPL_CONST
#define DCM_APPL_DATA
#define DCM_CALLOUT_CODE
#define DCM_VAR_INIT
#define DCM_VAR_NOINIT
#define DCM_VAR_PBCFG
#define DCM_PBCFG

/**************************************************
* DEM
**************************************************/

#define DEM_CODE
#define DEM_VAR_INIT
#define DEM_VAR_ZERO_INIT
#define DEM_VAR_NOINIT
#define DEM_VAR_UNCACHED
#define DEM_CONST
#define DEM_CONST_ROOT
#define DEM_PBCFG
#define DEM_PBCFG_ROOT
#define DEM_VAR_PBCFG
#define DEM_DCM_DATA
#define DEM_J1939DCM_DATA
#define DEM_DLT_DATA
#define DEM_NVM_DATA
#define DEM_APPL_CODE
#define DEM_APPL_DATA
#define DEM_APPL_CONST
#define DEM_SHARED_DATA
#define DEM_CAL_PRM
#define DEM_NVM_DATA_NOINIT     DEM_NVM_DATA
#define DEM_VAR_SATELLITE

/**************************************************
* DET
**************************************************/

#define DET_CODE
#define DET_VAR
#define DET_APPL_DATA
#define DET_CONST

/**************************************************
* DIO
**************************************************/

#define DIO_CODE
#define DIO_VAR_NOINIT
#define DIO_VAR_POWER_ON_INIT
#define DIO_VAR_FAST
#define DIO_VAR
#define DIO_CONST
#define DIO_APPL_DATA
#define DIO_APPL_CONST
#define DIO_APPL_CODE
#define DIO_APPL_REGSPACE

/**************************************************
* DOUT
**************************************************/

#define DOUT_S_VAR
#define DOUT_S_CODE
#define DOUT_S_APPL_DATA

/**************************************************
* DLT
**************************************************/

#define DLT_CODE
#define DLT_VAR_INIT
#define DLT_VAR_NO_INIT
#define DLT_VAR_CLEARED
#define DLT_VAR
#define DLT_CONST
#define DLT_PBCFG
#define DLT_VAR_PBCFG
#define DLT_APPL_VAR
#define DLT_APPL_CODE

/**************************************************
* DOIP
**************************************************/

#define DOIP_CODE
#define DOIP_APPL_CONST
#define DOIP_APPL_DATA
#define DOIP_APPL_VAR
#define DOIP_CONST
#define DOIP_PBCFG
#define DOIP_VAR_NO_INIT
#define DOIP_VAR_CLEARED

/**************************************************
* DMAEXT_S1E
**************************************************/

#define DMAEXT_S1E_CODE
#define DMAEXT_S1E_CONST
#define DMAEXT_S1E_CONST_PBCFG
#define DMAEXT_S1E_VAR
#define DMAEXT_S1E_APPL_VAR
#define DMAEXT_S1E_APPL_DATA
#define DMAEXT_S1E_PBCFG  DMAEXT_S1E_CONST_PBCFG

/**************************************************
* E2E
**************************************************/

#define E2E_CODE
#define E2E_VAR_FAST
#define E2E_VAR
#define E2E_CONST
#define E2E_DATA
#define E2E_APPL_DATA
#define E2E_APPL_CONST
#define E2E_APPL_CODE

/**************************************************
* ECUM
**************************************************/

#define ECUM_APPL_DATA 
#define ECUM_CODE
#define ECUM_CONST
#define ECUM_PBCFG
#define ECUM_VAR_NO_INIT 

/**************************************************
* ECUMM
**************************************************/

#define ECUMM_VAR
#define ECUMM_CONST
#define ECUMM_CODE

/**************************************************
* ETH_30_TC3XX
**************************************************/

#define ETH_30_TC3XX_CODE
#define ETH_30_TC3XX_CODE_ISR
#define ETH_30_TC3XX_CONST
#define ETH_30_TC3XX_APPL_CONST
#define ETH_30_TC3XX_PBCFG
#define ETH_30_TC3XX_APPL_DATA
#define ETH_30_TC3XX_APPL_VAR
#define ETH_30_TC3XX_APPL_CODE
#define ETH_30_TC3XX_VAR_NOINIT
#define ETH_30_TC3XX_VAR_ZERO_INIT

/**************************************************
* ETHIF
**************************************************/

#define ETHIF_CODE
#define ETHIF_CODE_ISR
#define ETHIF_ETHCTRL_INLINE_CODE
#define ETHIF_ETHTRCV_INLINE_CODE
#define ETHIF_ETHSWT_INLINE_CODE
#define ETHIF_GW_INLINE_CODE
#define ETHIF_LINK_INLINE_CODE
#define ETHIF_MIRROR_INLINE_CODE
#define ETHIF_MODE_INLINE_CODE
#define ETHIF_RX_INLINE_CODE
#define ETHIF_STATS_INLINE_CODE
#define ETHIF_TX_INLINE_CODE
#define ETHIF_UTILS_INLINE_CODE
#define ETHIF_ZEROCOPY_INLINE_CODE
#define ETHIF_CONST
#define ETHIF_APPL_VAR
#define ETHIF_APPL_DATA
#define ETHIF_APPL_CODE
#define ETHIF_VAR_NO_INIT
#define ETHIF_VAR_NO_INIT_FAST
#define ETHIF_VAR_INIT

/**************************************************
* ETHSM
**************************************************/

#define ETHSM_CODE
#define ETHSM_CONST
#define ETHSM_APPL_CONST
#define ETHSM_PBCFG
#define ETHSM_APPL_DATA
#define ETHSM_VAR_NOINIT
#define ETHSM_VAR_ZERO_INIT

/**************************************************
* ETHTRCV_30_88Q1010
**************************************************/

#define ETHTRCV_30_88Q1010_CODE
#define ETHTRCV_30_88Q1010_CODE_INLINE
#define ETHTRCV_30_88Q1010_CODE_FAST
#define ETHTRCV_30_88Q1010_CODE_ISR
#define ETHTRCV_30_88Q1010_CONST
#define ETHTRCV_30_88Q1010_CONST_FAST
#define ETHTRCV_30_88Q1010_PBCFG
#define ETHTRCV_30_88Q1010_PBCFG_ROOT
#define ETHTRCV_30_88Q1010_VAR_PBCFG
#define ETHTRCV_30_88Q1010_APPL_VAR
#define ETHTRCV_30_88Q1010_APPL_CONST
#define ETHTRCV_30_88Q1010_APPL_CODE
#define ETHTRCV_30_88Q1010_APPL_DATA
#define ETHTRCV_30_88Q1010_VAR_NOINIT
#define ETHTRCV_30_88Q1010_VAR_ZERO_INIT
#define ETHTRCV_30_88Q1010_VAR_INIT_FAST
#define ETHTRCV_30_88Q1010_VAR_NOINIT_FAST
#define ETHTRCV_30_88Q1010_VAR_ZERO_INIT_FAST
#define ETHTRCV_30_88Q1010_VAR_INIT_NOCACHE
#define ETHTRCV_30_88Q1010_VAR_NOINIT_NOCACHE
#define ETHTRCV_30_88Q1010_VAR_ZERO_INIT_NOCACHE

/**************************************************
* ETHTSYN
**************************************************/

#define ETHTSYN_CODE
#define ETHTSYN_CONST
#define ETHTSYN_PBCFG
#define ETHTSYN_APPL_VAR
#define ETHTSYN_APPL_DATA
#define ETHTSYN_VAR_NOINIT
#define ETHTSYN_VAR_ZERO_INIT

/**************************************************
* FIM
**************************************************/

#define FIM_CODE
#define FIM_CONST
#define FIM_PBCFG
#define FIM_PBCFG_ROOT
#define FIM_VAR_NOINIT
#define FIM_VAR_INIT
#define FIM_VAR_ZERO_INIT
#define FIM_APPL_DATA
#define FIM_DEM_DATA

/**************************************************
* FLS_17_DMU
**************************************************/

#define FLS_17_DMU_CODE

/**************************************************
* FEE
**************************************************/

#define FEE_CODE

/**************************************************
* GPTEXT_S2E
**************************************************/
#define GPTEXT_S2E_CODE
#define GPTEXT_S2E_CONST
#define GPTEXT_S2E_CONST_PBCFG
#define GPTEXT_S2E_VAR
#define GPTEXT_S2E_APPL_DATA
#define GPTEXT_S2E_PBCFG  GPTEXT_S2E_CONST_PBCFG

/**************************************************
* ICU
**************************************************/

#define ICU_CODE
#define ICU_VAR_NOINIT
#define ICU_VAR_POWER_ON_INIT
#define ICU_VAR_FAST
#define ICU_VAR
#define ICU_CONST
#define ICU_APPL_DATA
#define ICU_APPL_CONST
#define ICU_APPL_CODE
#define ICU_APPL_REGSPACE

/**************************************************
* IODP
**************************************************/

#define IODP_S1_VAR
#define IODP_VAR
#define IODP_S1_CODE

/**************************************************
* IPBASE
**************************************************/

#define IPBASE_CODE
#define IPBASE_CONST
#define IPBASE_APPL_CONST
#define IPBASE_PBCFG
#define IPBASE_APPL_DATA
#define IPBASE_VAR_NOINIT

/**************************************************
* IPDUM
**************************************************/

#define IPDUM_CODE
#define IPDUM_CONST
#define IPDUM_PBCFG
#define IPDUM_VAR_PBCFG
#define IPDUM_VAR_INIT
#define IPDUM_VAR_CLEARED
#define IPDUM_VAR_NO_INIT
#define IPDUM_APPL_DATA

/**************************************************
* IRQ
**************************************************/

#define IRQ_CODE
#define IRQ_VAR_NOINIT
#define IRQ_VAR_POWER_ON_INIT
#define IRQ_VAR_FAST
#define IRQ_VAR
#define IRQ_CONST
#define IRQ_APPL_DATA
#define IRQ_APPL_CONST
#define IRQ_APPL_CODE
#define IRQ_APPL_REGSPACE

/**************************************************
* KEYM
**************************************************/

#define KEYM_CODE
#define KEYM_APPL_CODE
#define KEYM_RTE_CODE
#define KEYM_CONST
#define KEYM_PBCFG
#define KEYM_VAR_INIT
#define KEYM_VAR_NO_INIT
#define KEYM_VAR_ZERO_INIT
#define KEYM_APPL_VAR
#define KEYM_APPL_DATA

/**************************************************
* LDCOM
**************************************************/

#define LDCOM_CODE
#define LDCOM_CONST
#define LDCOM_PBCFG
#define LDCOM_VAR_PBCFG
#define LDCOM_VAR_INIT
#define LDCOM_VAR_NOINIT
#define LDCOM_VAR_ZERO_INIT
#define LDCOM_APPL_CODE
#define LDCOM_APPL_VAR
#define LDCOM_APPL_DATA

/**************************************************
* MCU
**************************************************/

#define MCU_CODE
#define MCU_VAR_NOINIT
#define MCU_VAR_POWER_ON_INIT
#define MCU_VAR_FAST
#define MCU_VAR
#define MCU_CONST
#define MCU_APPL_DATA
#define MCU_APPL_CONST
#define MCU_APPL_CODE
#define MCU_APPL_REGSPACE

/**************************************************
* MEMIF
**************************************************/

#define MEMIF_CODE
#define MEMIF_PRIVATE_CODE
#define MEMIF_CONST
#define MEMIF_APPL_DATA

/**************************************************
* NM
**************************************************/

#define NM_CODE
#define NM_APPL_CODE
#define NM_CONST
#define NM_VAR_NO_INIT
#define NM_VAR_INIT
#define NM_APPL_VAR
#define NM_VAR_CLEARED

/**************************************************
* NVM
**************************************************/

#define NVM_CODE
#define NVM_APPL_DATA
#define NVM_APPL_CODE
#define NVM_APPL_CONST
#define NVM_CRC_APPL_DATA
#define NVM_CONFIG_DATA
#define NVM_CONFIG_CONST
#define NVM_FAST_DATA
#define NVM_PRIVATE_CODE
#define NVM_PRIVATE_CONST
#define NVM_PRIVATE_DATA
#define NVM_PUBLIC_CODE
#define NVM_PUBLIC_CONST

/**************************************************
* NVMEMNV
**************************************************/

#define NvMemNvMCbk_S1_VAR
#define NvMemNvStatistics_S1_VAR
#define NvMemPBSelDef_S1_VAR
#define NvMemRAMMirrStat_S1_VAR


/*shilar1 : Define are required for NvMemQSDataH_S1A module */
/**********************************************************************************************************************
 *  NvMemQSDataH_S1A START
 *********************************************************************************************************************/

#define NvMemQSDataH_S1A_CONST

#define NvMemQSDataH_S1A_CONFIG_DATA

#define NvMemQSDataH_S1A_CODE

#define NvMemQSDataH_S1A_VAR

#define NvMemQSDataH_S1A_APPL_DATA

#define NvMemQSDataH_S1A_APPL_CONST


/**********************************************************************************************************************
 *  NvMemQSDataH_S1A END

**************************************************/

/**************************************************
* OS
**************************************************/

#define OS_CODE
#define OS_CODE_FAST
#define OS_CODE_SLOW
#define OS_CODE_ISR
#define OS_PANICHOOK_CODE
#define OS_PRETASKHOOK_CODE
#define OS_POSTTASKHOOK_CODE
#define OS_STARTUPHOOK_CODE
#define OS_ERRORHOOK_CODE
#define OS_PROTECTIONHOOK_CODE
#define OS_SHUTDOWNHOOK_CODE
#define OS_CONST
#define OS_CONST_FAST
#define OS_VAR_INIT
#define OS_VAR_NOINIT
#define OS_VAR_ZERO_INIT
#define OS_VAR_INIT_FAST
#define OS_VAR_NOINIT_FAST
#define OS_VAR_ZERO_INIT_FAST
#define OS_VAR_INIT_NOCACHE
#define OS_VAR_NOINIT_NOCACHE
#define OS_VAR_ZERO_INIT_NOCACHE
#define OS_APPL_VAR
#define OS_APPL_DATA

#include "Os_Compiler_Cfg.h"

/**************************************************
* PDUR
**************************************************/

#define PDUR_CODE
#define PDUR_VAR_NO_INIT
#define PDUR_VAR_CLEARED
#define PDUR_VAR
#define PDUR_CONST
#define PDUR_PBCFG
#define PDUR_VAR_PBCFG
#define PDUR_APPL_DATA
#define PDUR_APPL_CODE

/**************************************************
* PORT
**************************************************/

#define PORT_CODE
#define PORT_VAR_NOINIT
#define PORT_VAR_POWER_ON_INIT
#define PORT_VAR_FAST
#define PORT_VAR
#define PORT_CONST
#define PORT_APPL_DATA
#define PORT_APPL_CONST
#define PORT_APPL_CODE
#define PORT_APPL_REGSPACE

/**************************************************
* PWM
**************************************************/

#define PWM_CODE
#define PWM_VAR_NOINIT
#define PWM_VAR_POWER_ON_INIT
#define PWM_VAR_FAST
#define PWM_VAR
#define PWM_CONST
#define PWM_APPL_DATA
#define PWM_APPL_CONST
#define PWM_APPL_CODE
#define PWM_APPL_REGSPACE

/**************************************************
* POUT_S
**************************************************/

#define POUT_S_VAR
#define POUT_S_CODE
#define POUT_S_APPL_DATA

/**************************************************
* RTM
**************************************************/

#define RTM_CONST
#define RTM_CODE
#define RTM_VAR_INIT
#define RTM_VAR_NO_INIT
#define RTM_VAR_CLEARED
#define RTM_APPL_VAR
#define RTM_APPL_DATA

/**************************************************
* SECOC
**************************************************/

#define SECOC_CODE
#define SECOC_CONST
#define SECOC_PBCFG
#define SECOC_VAR_PBCFG
#define SECOC_VAR_INIT
#define SECOC_VAR_NO_INIT
#define SECOC_VAR_CLEARED
#define SECOC_APPL_CODE
#define SECOC_APPL_VAR
#define SECOC_APPL_DATA

/**************************************************
* SD
**************************************************/

#define SD_CODE
#define SD_APPL_DATA
#define SD_APPL_VAR
#define SD_CONST
#define SD_PBCFG
#define SD_VAR_NO_INIT
#define SD_VAR_INIT
#define SD_VAR_CLEARED
#define SD_VAR_PBCFG

/**************************************************
* SOAD
**************************************************/

#define SOAD_CODE
#define SOAD_APPL_DATA
#define SOAD_APPL_VAR
#define SOAD_CONST
#define SOAD_PBCFG
#define SOAD_VAR_NO_INIT
#define SOAD_VAR_CLEARED
#define SOAD_VAR_PBCFG

/**************************************************
* SPI
**************************************************/

#define SPI_CODE
#define SPI_VAR_NOINIT
#define SPI_VAR_POWER_ON_INIT
#define SPI_VAR_FAST
#define SPI_VAR
#define SPI_CONST
#define SPI_APPL_DATA
#define SPI_APPL_CONST
#define SPI_APPL_CODE
#define SPI_APPL_REGSPACE

/**************************************************
* SSA
**************************************************/

#define SSA_CONST
#define SSA_VAR_NOINIT
#define SSA_APPL_VAR

/**************************************************
* STBM
**************************************************/

#define STBM_CONST
#define STBM_PBCFG
#define STBM_CODE
#define STBM_VAR_CLEARED
#define STBM_VAR_NO_INIT
#define STBM_VAR_PBCFG
#define STBM_APPL_CODE
#define STBM_APPL_VAR
#define STBM_APPL_DATA

/**************************************************
* T1
**************************************************/

#define T1_CODE
#define T1_CDD_CODE
#define T1_CDD_APPL_DATA

/**************************************************
* TCPIP
**************************************************/

#define TCPIP_CODE
#define TCPIP_CODE_ISR
#define TCPIP_CONST
#define TCPIP_PBCFG
#define TCPIP_APPL_CODE
#define TCPIP_APPL_CONST
#define TCPIP_APPL_DATA
#define TCPIP_APPL_VAR
#define TCPIP_VAR_INIT
#define TCPIP_VAR_CLEARED
#define TCPIP_VAR_NO_INIT

/**************************************************
* SWCDIAG
**************************************************/

#define SWCDIAG_CODE
#define SWCDIAG_CONST
#define SWCDIAG_VAR_NOINIT
#define SWCDIAG_VAR_INIT

/**************************************************
* TCPIPXCP
**************************************************/

#define TCPIPXCP_VAR_NOINIT
#define TCPIPXCP_VAR_INIT
#define TCPIPXCP_CONST
#define TCPIPXCP_PBCFG
#define TCPIPXCP_CODE
#define TCPIPXCP_APPL_VAR
#define TCPIPXCP_APPL_DATA

/**************************************************
* UART
**************************************************/

#define UART_CODE

/**************************************************
* UDPNM
**************************************************/

#define UDPNM_CODE
#define UDPNM_CONST
#define UDPNM_APPL_VAR
#define UDPNM_PBCFG
#define UDPNM_VAR_NOINIT
#define UDPNM_VAR_INIT
#define UDPNM_VAR_ZERO_INIT
#define UDPNM_VAR_PBCFG

/**************************************************
* VSECPRIM
**************************************************/

#define VSECPRIM_CODE
#define VSECPRIM_CONST
#define VSECPRIM_APPL_CONST
#define VSECPRIM_APPL_VAR
#define VSECPRIM_APPL_DATA

/**************************************************
* VSTDLIB
**************************************************/

#define VSTDLIB_CODE
#define VSTDLIB_VAR_FAR
#define VSTDLIB_APPL_VAR
#define VSTDLIB_CONST

/**************************************************
* WDGIF
**************************************************/

#define WDGIF_CODE
#define WDGIF_CONST
#define WDGIF_VAR
#define WDGIF_APPL_DATA
#define WDGIF_APPL_CONST

/**************************************************
* WDGM
**************************************************/

#define WDGM_CODE
#define WDGM_APPL_CODE
#define WDGM_CONST
#define WDGM_VAR_INIT
#define WDGM_VAR_NOINIT
#define WDGM_VAR_ZERO_INIT
#define WDGM_APPL_DATA
#define WDGM_APPL_CONST

/**************************************************
* XCP
**************************************************/

#define XCP_CODE
#define XCP_CONST
#define XCP_PBCFG
#define XCP_VAR_INIT
#define XCP_VAR_NOINIT
#define XCP_VAR_ZERO_INIT
#define XCP_VAR_NOINIT_NOCACHE
#define XCP_APPL_DATA
#define XCP_APPL_VAR

/**********************************************************************************************************************
 *  BLEM_S1_SI START
 *********************************************************************************************************************/

#define BLEM_S1_SI_CODE
#define BLEM_S1_SI_VAR
#define BLEM_S1_SI_CONST
#define BLEM_S1_SI_APPL_DATA

/**********************************************************************************************************************
 *  BLEM_S1_SI END
 *********************************************************************************************************************/
 
 /**********************************************************************************************************************
 *  MCUEXT_S2B START
 *********************************************************************************************************************/

#define McuExt_S2B_CODE
#define McuExt_S2B_CONST
#define McuExt_S2B_APPL_DATA

/**********************************************************************************************************************
 *  MCUEXT_S2B END
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  McuExtLBist_S2B START
 *********************************************************************************************************************/

#define McuExtLBist_S2B_CODE
#define McuExtLBist_S2B_CONST
#define McuExtLBist_S2B_APPL_CONST
#define McuExtLBist_S2B_APPL_DATA

/**********************************************************************************************************************
 *  McuExtLBist_S2B END
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  MCUEXTMBIST_S2B START
 *********************************************************************************************************************/

#define McuExtMBist_S2B_CODE
#define McuExtMBist_S2B_VAR
#define McuExtMBist_S2B_CONST
#define McuExtMBist_S2B_APPL_DATA
#define McuExtMBist_S2B_APPL_CONST

/**********************************************************************************************************************
 *  MCUEXTMBIST_S2B END
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  MCUEXTTEMPMON_S2B START
 *********************************************************************************************************************/

#define McuExtTempMon_S2B_CODE
#define McuExtTempMon_S2B_APPL_DATA
#define McuExtTempMon_S2B_APPL_CONST

/**********************************************************************************************************************
 *  MCUEXTTEMPMON_S2B END
 *********************************************************************************************************************/
 
  /**********************************************************************************************************************
*  MCUEXTREGCHECK_S2B START
*********************************************************************************************************************/

#define McuExtRegCheck_S2B_CODE
#define McuExtRegCheck_S2B_VAR
#define McuExtRegCheck_S2B_CONST
#define McuExtRegCheck_S2B_APPL_DATA
#define McuExtRegCheck_S2B_APPL_CONST

/**********************************************************************************************************************
*  MCUEXTREGCHECK_S2B END
*********************************************************************************************************************/

/**********************************************************************************************************************
 *  MCUEXTCRC_S2B START
 *********************************************************************************************************************/

#define McuExtCrc_S2B_CODE
#define McuExtCrc_S2B_APPL_CONST

/**********************************************************************************************************************
 *  MCUEXTCRC_S2B END
 *********************************************************************************************************************/
 
  /**********************************************************************************************************************
 *  MCUEXTMONBIST_S2B START
 *********************************************************************************************************************/

#define McuExtMonBist_S2B_CODE
#define McuExtMonBist_S2B_APPL_DATA
#define McuExtMonBist_S2B_APPL_CONST

/**********************************************************************************************************************
 *  MCUEXTMONBIST_S2B END
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  FLSHTST_S1 START
 *********************************************************************************************************************/

#define FLSHTST_S1_CODE
#define FLSHTST_S1_CONST
#define FLSHTST_S1_VAR
#define FLSHTST_S1_APPL_DATA

/**********************************************************************************************************************
 *  FLSHTST_S1 END
 *********************************************************************************************************************/
 
#endif  /* COMPILER_CFG_H */
