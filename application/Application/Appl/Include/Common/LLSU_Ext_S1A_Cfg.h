/*******************************************************************************/
/* CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)                                  */
/*                                                                             */
/* This is an unpublished work of authorship, which contains trade secrets,    */
/* created in 2009. Hella KGaA Hueck & Co. owns all rights to this work and    */
/* intends to maintain it in confidence to preserve its trade secret status.   */
/* Hella KGaA Hueck & Co. reserves the right, under the copyright laws of      */
/* Germany or those of any other country that may have jurisdiction, to        */
/* protect this work as an unpublished work, in the event of an inadvertent or */
/* deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves   */
/* its rights under all copyright laws to protect this work as a published     */
/* work, when appropriate. Those having access to this work may not copy it,   */
/* use it, modify it or disclose the information contained in it without the   */
/* written authorization of Hella KGaA Hueck & Co..                            */
/*                                                                             */
/*******************************************************************************/

/***********************************************************************************************/
/*  Filename    :   $Source: LLSU_Ext_S1A_Cfg.h $                                               */
/*  Author      :   $Author: Kumar Anil kumaan8 (kumaan8) $                              */
/*  Date        :   $Date: 2021/02/15 17:16:55IST $                                           */
/*  Version     :   $Revision: 1.2 $                                                       */
/*  Status      :   $State: IN_WORK $                                                          */
/*  Description :   This file contains definitions for RAM-initialization       	           */
/***********************************************************************************************/
/*  Changes     :                                                                              */
/*  $Log        :   LLSU_Ext_S1A_Cfg.h  $                                                       */
/***********************************************************************************************/
/*                                                                                             */
/*  Module                                                                                     */
/*  Release                                                                                    */
/*  Version     Date          Author    ChangeID Summary                                       */
/*                                                                                             */
/*  1.0      04.02.2021      schupe6    1205742    LLSU integration code for LE-Porsche        */
/*                                                                                             */
/***********************************************************************************************/

#ifndef LLSU_EXT_S1A_CFG_H_
#define LLSU_EXT_S1A_CFG_H_


#include "Std_Types.h"
#include "cinit.h"
//#include "McuExt_S2B_Cfg.h"

#define ACTION_MWU_END    0u
#define ACTION_MWU_COPY   1u
#define ACTION_MWU_CLEAR  2u


/* Start/end address label declarations */
extern uint32 _DLMU0_NORMAL_RAM_START[];
extern uint32 _DLMU0_NORMAL_RAM_SIZE[];

extern uint32 _DLMU1_VAR_BSS_START[];
extern uint32 _DLMU1_VAR_BSS_SIZE[];

extern uint32 _DLMU1_VAR_DATA_START[];
extern uint32 _DLMU1_VAR_DATA_SIZE[];

extern uint32 _DLMU1_VAR_ROM_START[];
extern uint32 _DLMU1_VAR_ROM_SIZE[];

extern uint32 _DLMU1_VAR_INIT_START[];
extern uint32 _DLMU1_VAR_INIT_SIZE[];

extern uint32 _DLMU0_VAR_BSS_START[];
extern uint32 _DLMU0_VAR_BSS_SIZE[];

extern uint32 _DLMU0_VAR_DATA_START[];
extern uint32 _DLMU0_VAR_DATA_SIZE[];

extern uint32 _DLMU0_VAR_ROM_START[];
extern uint32 _DLMU0_VAR_ROM_SIZE[];

extern uint32 _DLMU0_VAR_INIT_START[];
extern uint32 _DLMU0_VAR_INIT_SIZE[];


extern uint32 _DSPR0_RAM_START[];
extern uint32 _DSPR0_RAM_SIZE[];
extern uint32 _DSPR1_RAM_START[];
extern uint32 _DSPR1_RAM_SIZE[];


extern uint32 _PSPR0_RAM_START[];
extern uint32 _PSPR0_RAM_SIZE[];
extern uint32 _PSPR1_RAM_START[];
extern uint32 _PSPR1_RAM_SIZE[];

extern uint32 _DSPR0_VAR_START[];
extern uint32 _DSPR0_VAR_ROM_START[];
extern uint32 _DSPR0_VAR_SIZE[];

extern uint32 _DSPR1_VAR_START[];
extern uint32 _DSPR1_VAR_ROM_START[];
extern uint32 _DSPR1_VAR_SIZE[];

extern uint32 _PSPR0_VAR_START[];
extern uint32 _PSPR0_VAR_ROM_START[];
extern uint32 _PSPR0_VAR_SIZE[];

extern uint32 _PSPR1_VAR_START[];
extern uint32 _PSPR1_VAR_ROM_START[];
extern uint32 _PSPR1_VAR_SIZE[];

/*todo : sharan5 RSTM*/
extern uint32 _BLEM_NCR_BSS_START[];
extern uint32 _BLEM_NCR_BSS_SIZE[];
extern uint32 _BLEM_NCR_DATA_SIZE[];
extern uint32 _BLEM_NCR_DATA_START[];
extern uint32 _BLEM_NCR_DATA_ROM_START[];

extern uint32 _OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_SIZE[];
extern uint32 _OS_OsApplication_Core0_QM_VAR_ZERO_INIT_SIZE[];

extern uint32 _OS_OsApplication_Core1_ASIL_VAR_ZERO_INIT_SIZE[];
extern uint32 _OS_OsApplication_Core1_QM_VAR_ZERO_INIT_SIZE[];

extern uint32 _OS_GLOBALSHARED_VAR_ZERO_INIT_SIZE[];
extern uint32 _bss_SIZE[];
extern uint32 _zbss_SIZE[];


extern uint32 _OS_OsApplication_Core0_ASIL_VAR_INIT_START[];
extern uint32 _OS_OsApplication_Core0_ASIL_VAR_INIT_ROM_START[];
extern uint32 _OS_OsApplication_Core0_QM_VAR_INIT_START[];
extern uint32 _OS_OsApplication_Core0_QM_VAR_INIT_ROM_START[];

extern uint32 _OS_OsApplication_Core1_ASIL_VAR_INIT_START[];
extern uint32 _OS_OsApplication_Core1_ASIL_VAR_INIT_ROM_START[];
extern uint32 _OS_OsApplication_Core1_QM_VAR_INIT_START[];
extern uint32 _OS_OsApplication_Core1_QM_VAR_INIT_ROM_START[];


extern uint32 _OS_DATA_CORE0_VAR_INIT_START[];
extern uint32 _OS_DATA_CORE0_VAR_INIT_ROM_START[];

extern uint32 _OS_DATA_CORE1_VAR_INIT_START[];
extern uint32 _OS_DATA_CORE1_VAR_INIT_ROM_START[];

extern uint32 _OS_DATA_SHARED_VAR_INIT_START[];
extern uint32 _OS_DATA_SHARED_VAR_INIT_ROM_START[];
extern uint32 _OS_GLOBALSHARED_VAR_INIT_START[];
extern uint32 _OS_GLOBALSHARED_VAR_INIT_ROM_START[];
extern uint32 _data_START[];
extern uint32 _data_ROM_START[];
extern uint32 _zdata_START[];
extern uint32 _zdata_ROM_START[];
extern uint32 _OS_OsApplication_Core0_ASIL_VAR_INIT_SIZE[];
extern uint32 _OS_OsApplication_Core0_QM_VAR_INIT_SIZE[];

extern uint32 _OS_OsApplication_Core1_ASIL_VAR_INIT_SIZE[];
extern uint32 _OS_OsApplication_Core1_QM_VAR_INIT_SIZE[];

extern uint32 _OS_DATA_CORE0_VAR_INIT_SIZE[];
extern uint32 _OS_DATA_CORE1_VAR_INIT_SIZE[];

extern uint32 _OS_DATA_SHARED_VAR_INIT_SIZE[];
extern uint32 _OS_GLOBALSHARED_VAR_INIT_SIZE[];
extern uint32 _data_SIZE[];
extern uint32 _zdata_SIZE[];


extern uint32 _OS_STACKS_CORE0_VAR_NOINIT_START[];
extern uint32 _OS_STACKS_CORE0_VAR_NOINIT_SIZE[];
extern uint32 _OS_STACKS_CORE1_VAR_NOINIT_START[];
extern uint32 _OS_STACKS_CORE1_VAR_NOINIT_SIZE[];

extern uint32 _bss_START[];
extern uint32 _bss_SIZE[];


extern uint32 _DSPR0_VAR_BSS_START[];
extern uint32 _DSPR0_VAR_BSS_SIZE[];

extern uint32 _DSPR1_VAR_BSS_START[];
extern uint32 _DSPR1_VAR_BSS_SIZE[];

extern uint32 _PSPR0_VAR_BSS_START[];
extern uint32 _PSPR0_VAR_BSS_SIZE[];
extern uint32 _PSPR1_VAR_BSS_START[];
extern uint32 _PSPR1_VAR_BSS_SIZE[];

/* Values for reset pattern */
#define LLSU_EXT_S1A_INTENDED_RESET_INDICATOR		 (uint32)(0xAA55AA55uL)
/* Reset pattern inversion value */
#define LLSU_EXT_S1A_INTENDED_RESET_INDICATOR_INVERSE (uint32)(0x55AA55AAuL)


/* number of section groups to be always initialized with zeros or with concrete data */
#define LLSU_EXT_S1A_RAM_INIT_BLOCK_COUNT_CORE0   10u
#define LLSU_EXT_S1A_RAM_INIT_BLOCK_COUNT_CORE1    9u

/* number of section groups to be initialized with zeros only after hard reset */
#define LLSU_EXT_S1A_NCRAM_INIT_BLOCK_COUNT 3u
#define BLEM_S1_SI_ENABLE_LBIST_DEBUG STD_OFF
#define BLEM_S1_SI_CLEAR_SRAM_CORE0_NO_OF_SSHID 12
#define SSIM_S1A_RSTSTAT_LBISTRESETVAL 0x40000000

#define Data_Cache_Invalidate 0x00040003
#define Program_Cache_Buffer_Invalidate 0x00000003
/* section groups to be always initialized with zeros or with concrete data */
extern const copytable_entry_t LLSU_Ext_S1A_RamInitTable_Core0[LLSU_EXT_S1A_RAM_INIT_BLOCK_COUNT_CORE0];
//extern const copytable_entry_t LLSU_Ext_S1A_RamInitTable_Core1[LLSU_EXT_S1A_RAM_INIT_BLOCK_COUNT_CORE1];

/* section groups to be initialized with zeros only after hard reset */
//extern const copytable_entry_t LLSU_Ext_S1A_NoClearRamInitTable[LLSU_EXT_S1A_NCRAM_INIT_BLOCK_COUNT];
//extern const McuExt_S2B_SshIdType BLEM_S1_SI_ClearSRAM_Core0_Cfg[BLEM_S1_SI_CLEAR_SRAM_CORE0_NO_OF_SSHID];

//extern void _endinit(void);             /* call a user function with protection switched off */
extern void _call_init(void);             /* call a user function */
//extern void _call_init_tc1(void);             /* call a user function */
extern void __prof_init(void);             /* call a user function */

//void Enable_Peripheral_Clc(void);
//void llsu_DLMU0_stdby_Enable(void);
#endif /*LLSU_EXT_S1A_CFG_H_ */


