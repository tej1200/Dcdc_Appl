/**************************************************************************
**                                                                        *
**  FILE        :  VoltConverter.h                                               *
**                                                                        *
**  DESCRIPTION :                                                         *
**      modify this file untill there was a change in the start          *
**      up of the application.                                            *
**                                                                        *
**************************************************************************/
/***************************   Header Inclusions  ******************************/

#include "SWC_MaxValEngDir.h"
#include "Pwm_17_GtmCcu6.h"

extern Int_RequestValuesInfo_t l_Int_RequestValuesInfo;

/***************************   Header Inclusions   ******************************/
void VoltConverter(void);
static void EnergyTransfer(void);
static void NoEnergyTransfer(void);

/***************************   Macro Definition   ******************************/

#define NO_ENERGY_TRANSFER                                          0U
#define MAX_VOLTAGE                                                 48U
#define HUNDRED_VALUE                                               100U
#define PWM_PERIOD                                                  62
#define DUTY_CYCLE                                                  33800

#define Rte_RP_MaxValEngDir_VoltConverter_Max_Value_MaxVal()      (l_Int_RequestValuesInfo.control_b)
#define Rte_RP_MaxValEngDir_VoltConverter_Eng_Dir_EngDir()        (l_Int_RequestValuesInfo.mode_u8)
#define Rte_Call_IoHWAbs_VoltConverter_AdcConv_Volt_Info()        (l_Int_RequestValuesInfo.voltage_u16)
#define Rte_Call_IoHWAbs_VoltConverter_AdcConv_Current_Info()     (l_Int_RequestValuesInfo.current_u8)


#define Rte_Call_IoHWAbs_VoltConverter_PWMDuty_SwitchingElement(a)     Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_0, PWM_PERIOD, a);


/***************************   Macro Definition   ******************************/