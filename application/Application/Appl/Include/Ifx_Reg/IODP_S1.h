/*###############################################################################
 CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)

 This is an unpublished work of authorship, which contains trade secrets,
 created in 2005. Hella KGaA Hueck & Co. owns all rights to this work and
 intends to maintain it in confidence to preserve its trade secret status.
 Hella KGaA Hueck & Co. reserves the right, under the copyright laws of
 Germany or those of any other country that may have jurisdiction, to
 protect this work as an unpublished work, in the event of an inadvertent or
 deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves
 its rights under all copyright laws to protect this work as a published
 work, when appropriate. Those having access to this work may not copy it,
 use it, modify it or disclose the information contained in it without the
 written authorization of Hella KGaA Hueck & Co..

###############################################################################*/
/*##############################################################################

Module
Release
Version   Date         Author    ChangeID   Summary
1.0.1     12.07.2018   furnad2   861883     SafeIO  : IODP New Development
1.0.2     16-10-2018   furnad2   870890     SafeIO 1: IODP_S1: Implementation update to include RTE use-cases
1.0.3     19-03-2019   licaco1   937201     SafeIO 1: IODP_S1: Update to include parameter range check or all necessary interfaces
1.0.4     12-08-2019   poendu1   989673     [Lighting LE-PF]IODP_S1 Parameter Check for IODP_S1_Write_VoltageType
1.0.5     29-11-2019   murtmi2   1019422    SafeIO 1: IODP_S1: Update IODP_S1 to support current calculation calibration
1.0.6     09-10-2020   licaco1   1183297    Safe IO: Create IODP_S1 release for Infineon Aurix
1.0.7     31-05-2021   licaco1   1265282    Safe IO: The IODP_S1 shall provide the possibility to calculate higher current that can also have negative values
1.0.8     31-01-2022   radoco8   1364799    IODP_S1: Input Output Data Pool update for AR 4.4 specifications
1.0.9     31-01-2022   radoco8   1380252    SafeIO: IODP_S1 - Issues found in generator
##############################################################################*/

/*
 * IODP_S1.h
 *
 * Created on: Sat Oct 15 17:27:01 EEST 2022
 * Author: catabo1
 * Generator version: 1.0.9.0
 */

#ifndef IODP_S1_H
#define IODP_S1_H

/* IODP_S1 Software version information */
#define IODP_S1_SW_MAJOR_VERSION (1U)
#define IODP_S1_SW_MINOR_VERSION (0U)
#define IODP_S1_SW_PATCH_VERSION (9U)

/* Autosar version information for IODP_S1.h */
#define IODP_S1_AR_RELEASE_MAJOR_VERSION    (4U)
#define IODP_S1_AR_RELEASE_MINOR_VERSION    (4U)
#define IODP_S1_AR_RELEASE_REVISION_VERSION (0U)

#ifndef PARAM_CHECK_ON
#define PARAM_CHECK_ON    STD_ON
#endif
#ifndef PARAM_CHECK_OFF
#define PARAM_CHECK_OFF   STD_OFF
#endif

/* Configuration Parameters */
#define IODP_S1_DEV_ERROR_DETECT    STD_ON
#define IODP_S1_PARAM_ERROR_DETECT  PARAM_CHECK_ON
#define IODP_S1_GETVERSION_INFO_API STD_ON

#include "IODP_S1_Data.h"
#include "IODP_S1_Cbk.h"

/* Module ID */
#define IODP_S1_MODULE_ID (0x2070U)

/* Vendor ID */
#define IODP_S1_VENDOR_ID (0x000DU)

/* Instance ID for IODP_S1 module */
#define IODP_S1_INSTANCE_ID           (0U)

/* Error codes for IODP_S1 module */
#define IODP_S1_E_PARAM_POINTER       (0x00U)

/* API IDs for IODP_S1 module */
#define IODP_S1_GETVERSIONINFO_API_ID (0x01U)



#if(STD_OFF == IODP_S1_DEV_ERROR_DETECT)

/* Data Pool write interfaces */

/* interface used for storing the HW_Variant value  */
#define IODP_S1_Write_HW_VariantType(address, value)             (*(address) = (value))
/* interface used for storing the PortLev value  */
#define IODP_S1_Write_PortLevType(address, value)                (*(address) = (value))
/* interface used for storing the Info value  */
#define IODP_S1_Write_InfoType(address, value)                   (*(address) = (value))
/* interface used for storing the ComModeClient value  */
#define IODP_S1_Write_ComModeClientType(address, value)          (*(address) = (value))
/* interface used for storing the Adc value  */
#define IODP_S1_Write_AdcType(address, value)                    (*(address) = (value))
/* interface used for storing the SigLev value  */
#define IODP_S1_Write_SigLevType(address, value)                 (*(address) = (value))
/* interface used for storing the DutyCycle value  */
#define IODP_S1_Write_DutyCycleType(address, value)              (*(address) = (value))
/* interface used for storing the Frequency value  */
#define IODP_S1_Write_FrequencyType(address, value)              (*(address) = (value))
/* interface used for storing the Period value  */
#define IODP_S1_Write_PeriodType(address, value)                 (*(address) = (value))
/* interface used for storing the Voltage value  */
#define IODP_S1_Write_VoltageType(address, value)                (*(address) = (value))
/* interface used for storing the kFactor value  */
#define IODP_S1_Write_kFactorType(address, value)                (*(address) = (value))
/* interface used for storing the kOffset value  */
#define IODP_S1_Write_kOffsetType(address, value)                (*(address) = (value))
/* interface used for storing the Resistance value  */
#define IODP_S1_Write_ResistanceType(address, value)             (*(address) = (value))


/* Data Pool read interfaces */

/* interface used for retrieving the HW_Variant value  */
#define IODP_S1_Read_HW_VariantType(address)             (*(address))
/* interface used for retrieving the PortLev value  */
#define IODP_S1_Read_PortLevType(address)                (*(address))
/* interface used for retrieving the Info value  */
#define IODP_S1_Read_InfoType(address)                   (*(address))
/* interface used for retrieving the ComModeClient value  */
#define IODP_S1_Read_ComModeClientType(address)          (*(address))
/* interface used for retrieving the Adc value  */
#define IODP_S1_Read_AdcType(address)                    (*(address))
/* interface used for retrieving the SigLev value  */
#define IODP_S1_Read_SigLevType(address)                 (*(address))
/* interface used for retrieving the DutyCycle value  */
#define IODP_S1_Read_DutyCycleType(address)              (*(address))
/* interface used for retrieving the Frequency value  */
#define IODP_S1_Read_FrequencyType(address)              (*(address))
/* interface used for retrieving the Period value  */
#define IODP_S1_Read_PeriodType(address)                 (*(address))
/* interface used for retrieving the Voltage value  */
#define IODP_S1_Read_VoltageType(address)                (*(address))
/* interface used for retrieving the kFactor value  */
#define IODP_S1_Read_kFactorType(address)                (*(address))
/* interface used for retrieving the kOffset value  */
#define IODP_S1_Read_kOffsetType(address)                (*(address))
/* interface used for retrieving the Resistance value  */
#define IODP_S1_Read_ResistanceType(address)             (*(address))


#else

/* Maximum value which can be measured on an ADC channel. */
#define ADC_MAX_MEASURED_VALUE 0xFFF0U
#define MAX_HW_VAR_VALUE       255U
#define MAX_BRDGH_SPEED_VALUE  100U
#define MAX_DUTYCYCLE_VALUE    0x8000U
#define MIN_RESISTANCE_VALUE   1U


/* Defines used as API IDs for read or write interfaces. */

#define IODP_S1_WRITE_ADC_VALUE_INTERFACE              0U
#define IODP_S1_WRITE_BRDGH_CMD_INTERFACE              1U
#define IODP_S1_WRITE_BRDGH_SPD_INTERFACE              2U
#define IODP_S1_WRITE_CURRENT_VALUE_INTERFACE          3U
#define IODP_S1_WRITE_HW_VAR_VALUE_INTERFACE           4U
#define IODP_S1_WRITE_INFO_VALUE_INTERFACE             5U
#define IODP_S1_WRITE_INSTCFG_VALUE_INTERFACE          6U
#define IODP_S1_WRITE_PORT_VALUE_INTERFACE             7U
#define IODP_S1_WRITE_SIGNAL_LEVEL_INTERFACE           8U
#define IODP_S1_WRITE_VOLTAGE_VALUE_INTERFACE          9U
#define IODP_S1_WRITE_COMMODECLIENT_VALUE_INTERFACE    10U
#define IODP_S1_WRITE_COM_MODE_VALUE_INTERFACE         11U
#define IODP_S1_WRITE_DUTY_CYCLE_VALUE_INTERFACE       12U
#define IODP_S1_WRITE_FREQUENCY_VALUE_INTERFACE        13U
#define IODP_S1_WRITE_CHANNEL_LOAD_VALUE_INTERFACE     14U
#define IODP_S1_WRITE_PERIOD_VALUE_INTERFACE           15U
#define IODP_S1_READ_SIGNAL_VALUE_INTERFACE            16U
#define IODP_S1_READ_ADC_VALUE_INTERFACE               17U
#define IODP_S1_READ_INFO_VALUE_INTERFACE              18U
#define IODP_S1_READ_VOLTAGE_VALUE_INTERFACE           19U
#define IODP_S1_READ_CURRENT_VALUE_INTERFACE           20U
#define IODP_S1_READ_BRDGH_CMD_VALUE_INTERFACE         21U
#define IODP_S1_READ_BRDGH_SPD_VALUE_INTERFACE         22U
#define IODP_S1_READ_INSTANCE_CFG_VALUE_INTERFACE      23U
#define IODP_S1_READ_PORT_VALUE_INTERFACE              24U
#define IODP_S1_READ_HW_VAR_VALUE_INTERFACE            25U
#define IODP_S1_READ_COMMODECLIENT_VALUE_INTERFACE     26U
#define IODP_S1_READ_COM_MODE_VALUE_INTERFACE          27U
#define IODP_S1_READ_DUTY_CYCLE_VALUE_INTERFACE        28U
#define IODP_S1_READ_FREQUENCY_VALUE_INTERFACE         29U
#define IODP_S1_READ_CHANNEL_LOAD_VALUE_INTERFACE      30U
#define IODP_S1_READ_PERIOD_VALUE_INTERFACE            31U
#define IODP_S1_READ_KFACTOR_VALUE_INTERFACE           32U
#define IODP_S1_READ_KOFFSET_VALUE_INTERFACE           33U
#define IODP_S1_READ_RESISTANCE_VALUE_INTERFACE        34U
#define IODP_S1_WRITE_KFACTOR_VALUE_INTERFACE          35U
#define IODP_S1_WRITE_KOFFSET_VALUE_INTERFACE          36U
#define IODP_S1_WRITE_RESISTANCE_VALUE_INTERFACE       37U
#define IODP_S1_WRITE_DEVICEPOWERSTATE_VALUE_INTERFACE 38U
#define IODP_S1_READ_DEVICEPOWERSTATE_VALUE_INTERFACE  39U


/* Defines used for reporting the error type. */

#define IODP_S1_E_INVALID_ADC_VALUE              1U
#define IODP_S1_E_INVALID_BRDGH_CMD_VALUE        2U
#define IODP_S1_E_INVALID_BRDGH_SPD_VALUE        3U
#define IODP_S1_E_INVALID_CURRENT_VALUE          4U
#define IODP_S1_E_INVALID_HWVAR_VALUE            5U
#define IODP_S1_E_INVALID_INFO_VALUE             6U
#define IODP_S1_E_INVALID_PORT_VALUE             7U
#define IODP_S1_E_INVALID_SIGLEV_VALUE           8U
#define IODP_S1_E_INVALID_VOLTAGE_VALUE          9U
#define IODP_S1_E_INVALID_COMMODECLIENT_VALUE    10U
#define IODP_S1_E_INVALID_COM_MODE_VALUE         11U
#define IODP_S1_E_INVALID_DUTY_CYCLE_VALUE       12U
#define IODP_S1_E_INVALID_FREQUENCY_VALUE        13U
#define IODP_S1_E_INVALID_CHANNEL_LOAD_VALUE     14U
#define IODP_S1_E_INVALID_PERIOD_VALUE           15U
#define IODP_S1_E_INVALID_KOFFSET_VALUE          16U
#define IODP_S1_E_INVALID_DEVICEPOWERSTATE_VALUE 17U


/* interface used for storing the HW_Variant value with parameter validation check */
#define IODP_S1_Write_HW_VariantType(address, value)			(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_HW_VAR_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
																(((value) > MAX_HW_VAR_VALUE) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_HW_VAR_VALUE_INTERFACE, IODP_S1_E_INVALID_HWVAR_VALUE)):((*(address) = (value)))))

/* interface used for storing the PortLev value with parameter validation check */
#define IODP_S1_Write_PortLevType(address, value)				(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_PORT_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the Info value with parameter validation check */
#define IODP_S1_Write_InfoType(address, value)					(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_INFO_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the ComModeClient value with parameter validation check */
#define IODP_S1_Write_ComModeClientType(address, value)		    (((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_COMMODECLIENT_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
																(((value) > SAFEIO_S_COMMODECLIENT_ACTIVE) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_COMMODECLIENT_VALUE_INTERFACE, IODP_S1_E_INVALID_COMMODECLIENT_VALUE)) : ((*(address) = (value)))))

/* interface used for storing the Adc value with parameter validation check */
#define IODP_S1_Write_AdcType(address, value)					(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_ADC_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
																(((value) > ADC_MAX_MEASURED_VALUE) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_ADC_VALUE_INTERFACE, IODP_S1_E_INVALID_ADC_VALUE)) : ((*(address) = (value)))))

/* interface used for storing the SigLev value with parameter validation check */
#define IODP_S1_Write_SigLevType(address, value)				(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_SIGNAL_LEVEL_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
																(((value) > SAFEIO_S_SL_INVALID) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_SIGNAL_LEVEL_INTERFACE, IODP_S1_E_INVALID_SIGLEV_VALUE)):((*(address) = (value)))))

/* interface used for storing the DutyCycle value with parameter validation check */
#define IODP_S1_Write_DutyCycleType(address, value)			    (((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_DUTY_CYCLE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
																(((value) > MAX_DUTYCYCLE_VALUE) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_DUTY_CYCLE_VALUE_INTERFACE, IODP_S1_E_INVALID_DUTY_CYCLE_VALUE)) : ((*(address) = (value)))))

/* interface used for storing the Frequency value with parameter validation check */
#define IODP_S1_Write_FrequencyType(address, value)             (((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_FREQUENCY_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the Period value with parameter validation check */
#define IODP_S1_Write_PeriodType(address, value)				(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_PERIOD_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the Voltage value with parameter validation check */
#define IODP_S1_Write_VoltageType(address, value)				(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_VOLTAGE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the kFactor value with parameter validation check */
#define IODP_S1_Write_kFactorType(address, value)             (((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_KFACTOR_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address) = (value)))

/* interface used for storing the kOffset value with parameter validation check */
#define IODP_S1_Write_kOffsetType(address, value)             (((NULL_PTR) == (address)) ? \
																((SafeIO_S_kOffsetType)IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_KOFFSET_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER))  : (*(address) = (value)))

/* interface used for storing the Resistance value with parameter validation check */
#define IODP_S1_Write_ResistanceType(address, value)				(((NULL_PTR) == (address)) ? \
																(IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_RESISTANCE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : \
															    (((value) < MIN_RESISTANCE_VALUE) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_WRITE_RESISTANCE_VALUE_INTERFACE, IODP_S1_WRITE_RESISTANCE_VALUE_INTERFACE)) : ((*(address) = (value)))))

/* interface used for retrieving the HW_Variant value with parameter validation check */
#define IODP_S1_Read_HW_VariantType(address)            (((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_HW_VAR_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the PortLev value with parameter validation check */
#define IODP_S1_Read_PortLevType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_PORT_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Info value with parameter validation check */
#define IODP_S1_Read_InfoType(address)					(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_INFO_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the ComModeClient value with parameter validation check */
#define IODP_S1_Read_ComModeClientType(address)			(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_COMMODECLIENT_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Adc value with parameter validation check */
#define IODP_S1_Read_AdcType(address)					(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_ADC_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the SigLev value with parameter validation check */
#define IODP_S1_Read_SigLevType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_SIGNAL_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the DutyCycle value with parameter validation check */
#define IODP_S1_Read_DutyCycleType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_DUTY_CYCLE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Frequency value with parameter validation check */
#define IODP_S1_Read_FrequencyType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_FREQUENCY_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Period value with parameter validation check */
#define IODP_S1_Read_PeriodType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_PERIOD_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Voltage value with parameter validation check */
#define IODP_S1_Read_VoltageType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_VOLTAGE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the kFactor value with parameter validation check */
#define IODP_S1_Read_kFactorType(address)				(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_KFACTOR_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the kOffset value with parameter validation check */
#define IODP_S1_Read_kOffsetType(address)				(((NULL_PTR) == (address)) ? ((SafeIO_S_kOffsetType)IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_KOFFSET_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

/* interface used for retrieving the Resistance value with parameter validation check */
#define IODP_S1_Read_ResistanceType(address)			(((NULL_PTR) == (address)) ? (IODP_S1_Report_Dev_Error(IODP_S1_MODULE_ID, IODP_S1_INSTANCE_ID, IODP_S1_READ_RESISTANCE_VALUE_INTERFACE, IODP_S1_E_PARAM_POINTER)) : (*(address)))

#endif

#define IODP_S1_START_SEC_CODE
#include "IODP_S1_MemMap.h"

extern FUNC(void, IODP_S1_CODE) IODP_S1_Init(void);

#if(STD_ON == IODP_S1_GETVERSION_INFO_API)
extern FUNC(void, IODP_S1_CODE) IODP_S1_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC,IODP_S1_CODE) VersionInfo);
#endif

#define IODP_S1_STOP_SEC_CODE
#include "IODP_S1_MemMap.h"

/* Hardware Variants */

#define PLU_HD_VARIANT 0U

#endif /* IODP_S1_H */
