/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Daimler_SLP11
 *          Customer: Mercedes-Benz AG
 *       Expiry Date: Not restricted
 *  Ordered Derivat.:  TC399
 *    License Scope : The usage is restricted to CBD2200333_D02
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Application_Cfg.h
 *   Generation Time: 2023-10-27 13:47:24
 *           Project: DCDC_Appl - Version 1.0
 *          Delivery: CBD2200333_D02
 *      Tool Version: DaVinci Configurator Classic 5.25.37 SP2
 *
 *
 *********************************************************************************************************************/

#ifndef OS_APPLICATION_CFG_H 
# define OS_APPLICATION_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Number of application objects: OsApplication_Core0_BSW */
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_ALARMS             (1u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_COUNTERS           (0u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_HOOKS              (0u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_ISRS               (0u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_CAT1ISRS           (0u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_SCHTS              (0u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_TASKS              (2u)
# define OS_CFG_NUM_APP_OSAPPLICATION_CORE0_BSW_SERVICES           (0u)

/* Number of application objects: SystemApplication_OsCore0 */
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ALARMS             (0u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_COUNTERS           (1u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_HOOKS              (0u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ISRS               (1u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_CAT1ISRS           (0u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SCHTS              (0u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_TASKS              (2u)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SERVICES           (0u)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_APPLICATION_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Application_Cfg.h
 *********************************************************************************************************************/
