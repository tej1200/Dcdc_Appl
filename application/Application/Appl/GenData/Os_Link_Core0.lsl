/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Daimler_SLP11
 *          Customer: Mercedes-Benz AG
 *       Expiry Date: Not restricted
 *  Ordered Derivat.:  TC399
 *    License Scope : The usage is restricted to CBD2200333_D02
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Link_Core0.lsl
 *   Generation Time: 2023-10-24 17:05:49
 *           Project: DCDC_Appl - Version 1.0
 *          Delivery: CBD2200333_D02
 *      Tool Version: DaVinci Configurator Classic 5.25.37 SP2
 *
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  CODE SECTIONS
 *********************************************************************************************************************/

#if defined ( OS_LINK_INTVEC_CODE )
if (exists(".text.OS_INTVEC_CORE0_CODE"))
{
  group OS_INTVEC_CORE0_CODE_GROUP(align=8192)
  {
    select "[.]text.OS_INTVEC_CORE0_CODE";
  }
  "_OS_INTVEC_CORE0_CODE_START" = "_lc_gb_OS_INTVEC_CORE0_CODE_GROUP";
  "_OS_INTVEC_CORE0_CODE_END" = "_lc_ge_OS_INTVEC_CORE0_CODE_GROUP" - 1;
  "_OS_INTVEC_CORE0_CODE_LIMIT" = "_lc_ge_OS_INTVEC_CORE0_CODE_GROUP";
}
else
{
  "_OS_INTVEC_CORE0_CODE_START" = 0;
  "_OS_INTVEC_CORE0_CODE_END" = 0;
  "_OS_INTVEC_CORE0_CODE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_EXCVEC_CODE )
if (exists(".text.OS_EXCVEC_CORE0_CODE"))
{
  group OS_EXCVEC_CORE0_CODE_GROUP(align=256)
  {
    select "[.]text.OS_EXCVEC_CORE0_CODE";
  }
  "_OS_EXCVEC_CORE0_CODE_START" = "_lc_gb_OS_EXCVEC_CORE0_CODE_GROUP";
  "_OS_EXCVEC_CORE0_CODE_END" = "_lc_ge_OS_EXCVEC_CORE0_CODE_GROUP" - 1;
  "_OS_EXCVEC_CORE0_CODE_LIMIT" = "_lc_ge_OS_EXCVEC_CORE0_CODE_GROUP";
}
else
{
  "_OS_EXCVEC_CORE0_CODE_START" = 0;
  "_OS_EXCVEC_CORE0_CODE_END" = 0;
  "_OS_EXCVEC_CORE0_CODE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CALLOUT_CODE )
if (exists(".text.OS_C0_INIT_BSW_TASK_CODE"))
{
  group OS_C0_INIT_BSW_TASK_CODE_GROUP(align=8)
  {
    select "[.]text.OS_C0_INIT_BSW_TASK_CODE";
  }
  "_OS_C0_INIT_BSW_TASK_CODE_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_CODE_GROUP";
  "_OS_C0_INIT_BSW_TASK_CODE_END" = "_lc_ge_OS_C0_INIT_BSW_TASK_CODE_GROUP" - 1;
  "_OS_C0_INIT_BSW_TASK_CODE_LIMIT" = "_lc_ge_OS_C0_INIT_BSW_TASK_CODE_GROUP";
}
else
{
  "_OS_C0_INIT_BSW_TASK_CODE_START" = 0;
  "_OS_C0_INIT_BSW_TASK_CODE_END" = 0;
  "_OS_C0_INIT_BSW_TASK_CODE_LIMIT" = 0;
}
if (exists(".text.OS_OsTask_Core0_Appl_CODE"))
{
  group OS_OsTask_Core0_Appl_CODE_GROUP(align=8)
  {
    select "[.]text.OS_OsTask_Core0_Appl_CODE";
  }
  "_OS_OsTask_Core0_Appl_CODE_START" = "_lc_gb_OS_OsTask_Core0_Appl_CODE_GROUP";
  "_OS_OsTask_Core0_Appl_CODE_END" = "_lc_ge_OS_OsTask_Core0_Appl_CODE_GROUP" - 1;
  "_OS_OsTask_Core0_Appl_CODE_LIMIT" = "_lc_ge_OS_OsTask_Core0_Appl_CODE_GROUP";
}
else
{
  "_OS_OsTask_Core0_Appl_CODE_START" = 0;
  "_OS_OsTask_Core0_Appl_CODE_END" = 0;
  "_OS_OsTask_Core0_Appl_CODE_LIMIT" = 0;
}
if (exists(".text.OS_OsTask_Core0_BSW_CODE"))
{
  group OS_OsTask_Core0_BSW_CODE_GROUP(align=8)
  {
    select "[.]text.OS_OsTask_Core0_BSW_CODE";
  }
  "_OS_OsTask_Core0_BSW_CODE_START" = "_lc_gb_OS_OsTask_Core0_BSW_CODE_GROUP";
  "_OS_OsTask_Core0_BSW_CODE_END" = "_lc_ge_OS_OsTask_Core0_BSW_CODE_GROUP" - 1;
  "_OS_OsTask_Core0_BSW_CODE_LIMIT" = "_lc_ge_OS_OsTask_Core0_BSW_CODE_GROUP";
}
else
{
  "_OS_OsTask_Core0_BSW_CODE_START" = 0;
  "_OS_OsTask_Core0_BSW_CODE_END" = 0;
  "_OS_OsTask_Core0_BSW_CODE_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_INTVEC_CODE
# undef OS_LINK_INTVEC_CODE
#endif

#ifdef OS_LINK_EXCVEC_CODE
# undef OS_LINK_EXCVEC_CODE
#endif

#ifdef OS_LINK_CALLOUT_CODE
# undef OS_LINK_CALLOUT_CODE
#endif


/**********************************************************************************************************************
 *  CONST SECTIONS
 *********************************************************************************************************************/

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_KERNEL ) || defined ( OS_LINK_CONST_KERNEL_FAR )
if (exists(".rodata.OS_CORE0_CONST"))
{
  group OS_CORE0_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_CORE0_CONST";
  }
  "_OS_CORE0_CONST_START" = "_lc_gb_OS_CORE0_CONST_GROUP";
  "_OS_CORE0_CONST_END" = "_lc_ge_OS_CORE0_CONST_GROUP" - 1;
  "_OS_CORE0_CONST_LIMIT" = "_lc_ge_OS_CORE0_CONST_GROUP";
}
else
{
  "_OS_CORE0_CONST_START" = 0;
  "_OS_CORE0_CONST_END" = 0;
  "_OS_CORE0_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_KERNEL ) || defined ( OS_LINK_CONST_KERNEL_NEAR )
if (exists(".zrodata.OS_CORE0_CONST_FAST"))
{
  group OS_CORE0_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_CORE0_CONST_FAST";
  }
  "_OS_CORE0_CONST_FAST_START" = "_lc_gb_OS_CORE0_CONST_FAST_GROUP";
  "_OS_CORE0_CONST_FAST_END" = "_lc_ge_OS_CORE0_CONST_FAST_GROUP" - 1;
  "_OS_CORE0_CONST_FAST_LIMIT" = "_lc_ge_OS_CORE0_CONST_FAST_GROUP";
}
else
{
  "_OS_CORE0_CONST_FAST_START" = 0;
  "_OS_CORE0_CONST_FAST_END" = 0;
  "_OS_CORE0_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_INTVEC_CONST )
if (exists(".rodata.OS_INTVEC_CORE0_CONST"))
{
  group OS_INTVEC_CORE0_CONST_GROUP(align=8192)
  {
    select "[.]rodata.OS_INTVEC_CORE0_CONST";
  }
  "_OS_INTVEC_CORE0_CONST_START" = "_lc_gb_OS_INTVEC_CORE0_CONST_GROUP";
  "_OS_INTVEC_CORE0_CONST_END" = "_lc_ge_OS_INTVEC_CORE0_CONST_GROUP" - 1;
  "_OS_INTVEC_CORE0_CONST_LIMIT" = "_lc_ge_OS_INTVEC_CORE0_CONST_GROUP";
}
else
{
  "_OS_INTVEC_CORE0_CONST_START" = 0;
  "_OS_INTVEC_CORE0_CONST_END" = 0;
  "_OS_INTVEC_CORE0_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_EXCVEC_CONST )
if (exists(".rodata.OS_EXCVEC_CORE0_CONST"))
{
  group OS_EXCVEC_CORE0_CONST_GROUP(align=256)
  {
    select "[.]rodata.OS_EXCVEC_CORE0_CONST";
  }
  "_OS_EXCVEC_CORE0_CONST_START" = "_lc_gb_OS_EXCVEC_CORE0_CONST_GROUP";
  "_OS_EXCVEC_CORE0_CONST_END" = "_lc_ge_OS_EXCVEC_CORE0_CONST_GROUP" - 1;
  "_OS_EXCVEC_CORE0_CONST_LIMIT" = "_lc_ge_OS_EXCVEC_CORE0_CONST_GROUP";
}
else
{
  "_OS_EXCVEC_CORE0_CONST_START" = 0;
  "_OS_EXCVEC_CORE0_CONST_END" = 0;
  "_OS_EXCVEC_CORE0_CONST_LIMIT" = 0;
}
#endif


#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_FAR )
if (exists(".rodata.OS_OsApplication_Core0_BSW_CONST"))
{
  group OS_OsApplication_Core0_BSW_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_OsApplication_Core0_BSW_CONST";
  }
  "_OS_OsApplication_Core0_BSW_CONST_START" = "_lc_gb_OS_OsApplication_Core0_BSW_CONST_GROUP";
  "_OS_OsApplication_Core0_BSW_CONST_END" = "_lc_ge_OS_OsApplication_Core0_BSW_CONST_GROUP" - 1;
  "_OS_OsApplication_Core0_BSW_CONST_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_CONST_GROUP";
}
else
{
  "_OS_OsApplication_Core0_BSW_CONST_START" = 0;
  "_OS_OsApplication_Core0_BSW_CONST_END" = 0;
  "_OS_OsApplication_Core0_BSW_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_NEAR )
if (exists(".zrodata.OS_OsApplication_Core0_BSW_CONST_FAST"))
{
  group OS_OsApplication_Core0_BSW_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_OsApplication_Core0_BSW_CONST_FAST";
  }
  "_OS_OsApplication_Core0_BSW_CONST_FAST_START" = "_lc_gb_OS_OsApplication_Core0_BSW_CONST_FAST_GROUP";
  "_OS_OsApplication_Core0_BSW_CONST_FAST_END" = "_lc_ge_OS_OsApplication_Core0_BSW_CONST_FAST_GROUP" - 1;
  "_OS_OsApplication_Core0_BSW_CONST_FAST_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_CONST_FAST_GROUP";
}
else
{
  "_OS_OsApplication_Core0_BSW_CONST_FAST_START" = 0;
  "_OS_OsApplication_Core0_BSW_CONST_FAST_END" = 0;
  "_OS_OsApplication_Core0_BSW_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_INIT )
















#endif

#ifdef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW
# undef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW
#endif

#ifdef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_FAR
# undef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_FAR
#endif

#ifdef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_NEAR
# undef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_NEAR
#endif

#ifdef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_INIT
# undef OS_LINK_CONST_APP_OSAPPLICATION_CORE0_BSW_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_FAR )
if (exists(".rodata.OS_SystemApplication_OsCore0_CONST"))
{
  group OS_SystemApplication_OsCore0_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_SystemApplication_OsCore0_CONST";
  }
  "_OS_SystemApplication_OsCore0_CONST_START" = "_lc_gb_OS_SystemApplication_OsCore0_CONST_GROUP";
  "_OS_SystemApplication_OsCore0_CONST_END" = "_lc_ge_OS_SystemApplication_OsCore0_CONST_GROUP" - 1;
  "_OS_SystemApplication_OsCore0_CONST_LIMIT" = "_lc_ge_OS_SystemApplication_OsCore0_CONST_GROUP";
}
else
{
  "_OS_SystemApplication_OsCore0_CONST_START" = 0;
  "_OS_SystemApplication_OsCore0_CONST_END" = 0;
  "_OS_SystemApplication_OsCore0_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_NEAR )
if (exists(".zrodata.OS_SystemApplication_OsCore0_CONST_FAST"))
{
  group OS_SystemApplication_OsCore0_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_SystemApplication_OsCore0_CONST_FAST";
  }
  "_OS_SystemApplication_OsCore0_CONST_FAST_START" = "_lc_gb_OS_SystemApplication_OsCore0_CONST_FAST_GROUP";
  "_OS_SystemApplication_OsCore0_CONST_FAST_END" = "_lc_ge_OS_SystemApplication_OsCore0_CONST_FAST_GROUP" - 1;
  "_OS_SystemApplication_OsCore0_CONST_FAST_LIMIT" = "_lc_ge_OS_SystemApplication_OsCore0_CONST_FAST_GROUP";
}
else
{
  "_OS_SystemApplication_OsCore0_CONST_FAST_START" = 0;
  "_OS_SystemApplication_OsCore0_CONST_FAST_END" = 0;
  "_OS_SystemApplication_OsCore0_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_APP ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_INIT )
















#endif

#ifdef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0
# undef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0
#endif

#ifdef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_FAR
# undef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_FAR
#endif

#ifdef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_NEAR
# undef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_NEAR
#endif

#ifdef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_INIT
# undef OS_LINK_CONST_APP_SYSTEMAPPLICATION_OSCORE0_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_FAR )
if (exists(".rodata.OS_C0_INIT_BSW_TASK_CONST"))
{
  group OS_C0_INIT_BSW_TASK_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_C0_INIT_BSW_TASK_CONST";
  }
  "_OS_C0_INIT_BSW_TASK_CONST_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_CONST_GROUP";
  "_OS_C0_INIT_BSW_TASK_CONST_END" = "_lc_ge_OS_C0_INIT_BSW_TASK_CONST_GROUP" - 1;
  "_OS_C0_INIT_BSW_TASK_CONST_LIMIT" = "_lc_ge_OS_C0_INIT_BSW_TASK_CONST_GROUP";
}
else
{
  "_OS_C0_INIT_BSW_TASK_CONST_START" = 0;
  "_OS_C0_INIT_BSW_TASK_CONST_END" = 0;
  "_OS_C0_INIT_BSW_TASK_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_NEAR )
if (exists(".zrodata.OS_C0_INIT_BSW_TASK_CONST_FAST"))
{
  group OS_C0_INIT_BSW_TASK_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_C0_INIT_BSW_TASK_CONST_FAST";
  }
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_CONST_FAST_GROUP";
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_END" = "_lc_ge_OS_C0_INIT_BSW_TASK_CONST_FAST_GROUP" - 1;
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_LIMIT" = "_lc_ge_OS_C0_INIT_BSW_TASK_CONST_FAST_GROUP";
}
else
{
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_START" = 0;
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_END" = 0;
  "_OS_C0_INIT_BSW_TASK_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_INIT )













#endif

#ifdef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK
# undef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK
#endif

#ifdef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_FAR
# undef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_FAR
#endif

#ifdef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_NEAR
# undef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_NEAR
#endif

#ifdef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_INIT
# undef OS_LINK_CONST_TASK_C0_INIT_BSW_TASK_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0_FAR )
if (exists(".rodata.OS_IdleTask_OsCore0_CONST"))
{
  group OS_IdleTask_OsCore0_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_IdleTask_OsCore0_CONST";
  }
  "_OS_IdleTask_OsCore0_CONST_START" = "_lc_gb_OS_IdleTask_OsCore0_CONST_GROUP";
  "_OS_IdleTask_OsCore0_CONST_END" = "_lc_ge_OS_IdleTask_OsCore0_CONST_GROUP" - 1;
  "_OS_IdleTask_OsCore0_CONST_LIMIT" = "_lc_ge_OS_IdleTask_OsCore0_CONST_GROUP";
}
else
{
  "_OS_IdleTask_OsCore0_CONST_START" = 0;
  "_OS_IdleTask_OsCore0_CONST_END" = 0;
  "_OS_IdleTask_OsCore0_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0_NEAR )
if (exists(".zrodata.OS_IdleTask_OsCore0_CONST_FAST"))
{
  group OS_IdleTask_OsCore0_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_IdleTask_OsCore0_CONST_FAST";
  }
  "_OS_IdleTask_OsCore0_CONST_FAST_START" = "_lc_gb_OS_IdleTask_OsCore0_CONST_FAST_GROUP";
  "_OS_IdleTask_OsCore0_CONST_FAST_END" = "_lc_ge_OS_IdleTask_OsCore0_CONST_FAST_GROUP" - 1;
  "_OS_IdleTask_OsCore0_CONST_FAST_LIMIT" = "_lc_ge_OS_IdleTask_OsCore0_CONST_FAST_GROUP";
}
else
{
  "_OS_IdleTask_OsCore0_CONST_FAST_START" = 0;
  "_OS_IdleTask_OsCore0_CONST_FAST_END" = 0;
  "_OS_IdleTask_OsCore0_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_CONST_TASK_IDLETASK_OSCORE0_INIT )













#endif

#ifdef OS_LINK_CONST_TASK_IDLETASK_OSCORE0
# undef OS_LINK_CONST_TASK_IDLETASK_OSCORE0
#endif

#ifdef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_FAR
# undef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_FAR
#endif

#ifdef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_NEAR
# undef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_NEAR
#endif

#ifdef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_INIT
# undef OS_LINK_CONST_TASK_IDLETASK_OSCORE0_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_FAR )
if (exists(".rodata.OS_OsTask_Core0_Appl_CONST"))
{
  group OS_OsTask_Core0_Appl_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_OsTask_Core0_Appl_CONST";
  }
  "_OS_OsTask_Core0_Appl_CONST_START" = "_lc_gb_OS_OsTask_Core0_Appl_CONST_GROUP";
  "_OS_OsTask_Core0_Appl_CONST_END" = "_lc_ge_OS_OsTask_Core0_Appl_CONST_GROUP" - 1;
  "_OS_OsTask_Core0_Appl_CONST_LIMIT" = "_lc_ge_OS_OsTask_Core0_Appl_CONST_GROUP";
}
else
{
  "_OS_OsTask_Core0_Appl_CONST_START" = 0;
  "_OS_OsTask_Core0_Appl_CONST_END" = 0;
  "_OS_OsTask_Core0_Appl_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_NEAR )
if (exists(".zrodata.OS_OsTask_Core0_Appl_CONST_FAST"))
{
  group OS_OsTask_Core0_Appl_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_OsTask_Core0_Appl_CONST_FAST";
  }
  "_OS_OsTask_Core0_Appl_CONST_FAST_START" = "_lc_gb_OS_OsTask_Core0_Appl_CONST_FAST_GROUP";
  "_OS_OsTask_Core0_Appl_CONST_FAST_END" = "_lc_ge_OS_OsTask_Core0_Appl_CONST_FAST_GROUP" - 1;
  "_OS_OsTask_Core0_Appl_CONST_FAST_LIMIT" = "_lc_ge_OS_OsTask_Core0_Appl_CONST_FAST_GROUP";
}
else
{
  "_OS_OsTask_Core0_Appl_CONST_FAST_START" = 0;
  "_OS_OsTask_Core0_Appl_CONST_FAST_END" = 0;
  "_OS_OsTask_Core0_Appl_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_INIT )













#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_FAR
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_FAR
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_NEAR
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_NEAR
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_INIT
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_APPL_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_FAR )
if (exists(".rodata.OS_OsTask_Core0_BSW_CONST"))
{
  group OS_OsTask_Core0_BSW_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_OsTask_Core0_BSW_CONST";
  }
  "_OS_OsTask_Core0_BSW_CONST_START" = "_lc_gb_OS_OsTask_Core0_BSW_CONST_GROUP";
  "_OS_OsTask_Core0_BSW_CONST_END" = "_lc_ge_OS_OsTask_Core0_BSW_CONST_GROUP" - 1;
  "_OS_OsTask_Core0_BSW_CONST_LIMIT" = "_lc_ge_OS_OsTask_Core0_BSW_CONST_GROUP";
}
else
{
  "_OS_OsTask_Core0_BSW_CONST_START" = 0;
  "_OS_OsTask_Core0_BSW_CONST_END" = 0;
  "_OS_OsTask_Core0_BSW_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_NEAR )
if (exists(".zrodata.OS_OsTask_Core0_BSW_CONST_FAST"))
{
  group OS_OsTask_Core0_BSW_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_OsTask_Core0_BSW_CONST_FAST";
  }
  "_OS_OsTask_Core0_BSW_CONST_FAST_START" = "_lc_gb_OS_OsTask_Core0_BSW_CONST_FAST_GROUP";
  "_OS_OsTask_Core0_BSW_CONST_FAST_END" = "_lc_ge_OS_OsTask_Core0_BSW_CONST_FAST_GROUP" - 1;
  "_OS_OsTask_Core0_BSW_CONST_FAST_LIMIT" = "_lc_ge_OS_OsTask_Core0_BSW_CONST_FAST_GROUP";
}
else
{
  "_OS_OsTask_Core0_BSW_CONST_FAST_START" = 0;
  "_OS_OsTask_Core0_BSW_CONST_FAST_END" = 0;
  "_OS_OsTask_Core0_BSW_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_TASK ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_INIT )













#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_FAR
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_FAR
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_NEAR
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_NEAR
#endif

#ifdef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_INIT
# undef OS_LINK_CONST_TASK_OSTASK_CORE0_BSW_INIT
#endif



#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_ISR ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_FAR )
if (exists(".rodata.OS_CounterIsr_SystemTimer_CONST"))
{
  group OS_CounterIsr_SystemTimer_CONST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]rodata.OS_CounterIsr_SystemTimer_CONST";
  }
  "_OS_CounterIsr_SystemTimer_CONST_START" = "_lc_gb_OS_CounterIsr_SystemTimer_CONST_GROUP";
  "_OS_CounterIsr_SystemTimer_CONST_END" = "_lc_ge_OS_CounterIsr_SystemTimer_CONST_GROUP" - 1;
  "_OS_CounterIsr_SystemTimer_CONST_LIMIT" = "_lc_ge_OS_CounterIsr_SystemTimer_CONST_GROUP";
}
else
{
  "_OS_CounterIsr_SystemTimer_CONST_START" = 0;
  "_OS_CounterIsr_SystemTimer_CONST_END" = 0;
  "_OS_CounterIsr_SystemTimer_CONST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_ISR ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_NEAR )
if (exists(".zrodata.OS_CounterIsr_SystemTimer_CONST_FAST"))
{
  group OS_CounterIsr_SystemTimer_CONST_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    select "[.]zrodata.OS_CounterIsr_SystemTimer_CONST_FAST";
  }
  "_OS_CounterIsr_SystemTimer_CONST_FAST_START" = "_lc_gb_OS_CounterIsr_SystemTimer_CONST_FAST_GROUP";
  "_OS_CounterIsr_SystemTimer_CONST_FAST_END" = "_lc_ge_OS_CounterIsr_SystemTimer_CONST_FAST_GROUP" - 1;
  "_OS_CounterIsr_SystemTimer_CONST_FAST_LIMIT" = "_lc_ge_OS_CounterIsr_SystemTimer_CONST_FAST_GROUP";
}
else
{
  "_OS_CounterIsr_SystemTimer_CONST_FAST_START" = 0;
  "_OS_CounterIsr_SystemTimer_CONST_FAST_END" = 0;
  "_OS_CounterIsr_SystemTimer_CONST_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_CONST ) || defined ( OS_LINK_CONST_ISR ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_INIT )













#endif

#ifdef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER
# undef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER
#endif

#ifdef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_FAR
# undef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_FAR
#endif

#ifdef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_NEAR
# undef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_NEAR
#endif

#ifdef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_INIT
# undef OS_LINK_CONST_ISR_COUNTERISR_SYSTEMTIMER_INIT
#endif



#ifdef OS_LINK_CONST
# undef OS_LINK_CONST
#endif

#ifdef OS_LINK_CONST_APP
# undef OS_LINK_CONST_APP
#endif

#ifdef OS_LINK_CONST_TASK
# undef OS_LINK_CONST_TASK
#endif

#ifdef OS_LINK_CONST_ISR
# undef OS_LINK_CONST_ISR
#endif

#ifdef OS_LINK_CONST_KERNEL
# undef OS_LINK_CONST_KERNEL
#endif

#ifdef OS_LINK_CONST_KERNEL_FAR
# undef OS_LINK_CONST_KERNEL_FAR
#endif

#ifdef OS_LINK_CONST_KERNEL_NEAR
# undef OS_LINK_CONST_KERNEL_NEAR
#endif

#ifdef OS_LINK_INTVEC_CONST
# undef OS_LINK_INTVEC_CONST
#endif

#ifdef OS_LINK_EXCVEC_CONST
# undef OS_LINK_EXCVEC_CONST
#endif


/**********************************************************************************************************************
 *  VAR SECTIONS
 *********************************************************************************************************************/

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_FAR ) || defined ( OS_LINK_VAR_KERNEL_FAR_CACHE ) || defined ( OS_LINK_VAR_KERNEL_FAR_CACHE_INIT )
if (exists(".data.OS_CORE0_VAR"))
{
  group OS_CORE0_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_CORE0_VAR";
    }
    reserved ".pad.OS_CORE0_VAR" (size=16);
  }
  "_OS_CORE0_VAR_START" = "_lc_gb_OS_CORE0_VAR_GROUP";
  "_OS_CORE0_VAR_END" = "_lc_ub__pad_OS_CORE0_VAR" - 1;
  "_OS_CORE0_VAR_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR";
}
else
{
  "_OS_CORE0_VAR_START" = 0;
  "_OS_CORE0_VAR_END" = 0;
  "_OS_CORE0_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_FAR ) || defined ( OS_LINK_VAR_KERNEL_FAR_CACHE ) || defined ( OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT )
if (exists(".bss.OS_CORE0_VAR_NOINIT"))
{
  group OS_CORE0_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CORE0_VAR_NOINIT";
    }
    reserved ".pad.OS_CORE0_VAR_NOINIT" (size=16);
  }
  "_OS_CORE0_VAR_NOINIT_START" = "_lc_gb_OS_CORE0_VAR_NOINIT_GROUP";
  "_OS_CORE0_VAR_NOINIT_END" = "_lc_ub__pad_OS_CORE0_VAR_NOINIT" - 1;
  "_OS_CORE0_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_NOINIT";
}
else
{
  "_OS_CORE0_VAR_NOINIT_START" = 0;
  "_OS_CORE0_VAR_NOINIT_END" = 0;
  "_OS_CORE0_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_NEAR ) || defined ( OS_LINK_VAR_KERNEL_NEAR_CACHE ) || defined ( OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT )
if (exists(".zdata.OS_CORE0_VAR_FAST"))
{
  group OS_CORE0_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_CORE0_VAR_FAST";
    }
    reserved ".pad.OS_CORE0_VAR_FAST" (size=16);
  }
  "_OS_CORE0_VAR_FAST_START" = "_lc_gb_OS_CORE0_VAR_FAST_GROUP";
  "_OS_CORE0_VAR_FAST_END" = "_lc_ub__pad_OS_CORE0_VAR_FAST" - 1;
  "_OS_CORE0_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_FAST";
}
else
{
  "_OS_CORE0_VAR_FAST_START" = 0;
  "_OS_CORE0_VAR_FAST_END" = 0;
  "_OS_CORE0_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_NEAR ) || defined ( OS_LINK_VAR_KERNEL_NEAR_CACHE ) || defined ( OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_CORE0_VAR_FAST_NOINIT"))
{
  group OS_CORE0_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CORE0_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_CORE0_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_CORE0_VAR_FAST_NOINIT_START" = "_lc_gb_OS_CORE0_VAR_FAST_NOINIT_GROUP";
  "_OS_CORE0_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOINIT" - 1;
  "_OS_CORE0_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOINIT";
}
else
{
  "_OS_CORE0_VAR_FAST_NOINIT_START" = 0;
  "_OS_CORE0_VAR_FAST_NOINIT_END" = 0;
  "_OS_CORE0_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_FAR ) || defined ( OS_LINK_VAR_KERNEL_FAR_NOCACHE ) || defined ( OS_LINK_VAR_KERNEL_FAR_NOCACHE_INIT )
if (exists(".data.OS_CORE0_VAR_NOCACHE"))
{
  group OS_CORE0_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_CORE0_VAR_NOCACHE";
    }
    reserved ".pad.OS_CORE0_VAR_NOCACHE" (size=16);
  }
  "_OS_CORE0_VAR_NOCACHE_START" = "_lc_gb_OS_CORE0_VAR_NOCACHE_GROUP";
  "_OS_CORE0_VAR_NOCACHE_END" = "_lc_ub__pad_OS_CORE0_VAR_NOCACHE" - 1;
  "_OS_CORE0_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_NOCACHE";
}
else
{
  "_OS_CORE0_VAR_NOCACHE_START" = 0;
  "_OS_CORE0_VAR_NOCACHE_END" = 0;
  "_OS_CORE0_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_FAR ) || defined ( OS_LINK_VAR_KERNEL_FAR_NOCACHE ) || defined ( OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_CORE0_VAR_NOCACHE_NOINIT"))
{
  group OS_CORE0_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CORE0_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CORE0_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CORE0_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_CORE0_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_CORE0_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CORE0_VAR_NOCACHE_NOINIT" - 1;
  "_OS_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_CORE0_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_CORE0_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_NEAR ) || defined ( OS_LINK_VAR_KERNEL_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_KERNEL_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_CORE0_VAR_FAST_NOCACHE"))
{
  group OS_CORE0_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_CORE0_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_CORE0_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_CORE0_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_CORE0_VAR_FAST_NOCACHE_GROUP";
  "_OS_CORE0_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOCACHE" - 1;
  "_OS_CORE0_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOCACHE";
}
else
{
  "_OS_CORE0_VAR_FAST_NOCACHE_START" = 0;
  "_OS_CORE0_VAR_FAST_NOCACHE_END" = 0;
  "_OS_CORE0_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_KERNEL ) || defined ( OS_LINK_VAR_KERNEL_NEAR ) || defined ( OS_LINK_VAR_KERNEL_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_KERNEL_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_CORE0_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORE0_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CORE0_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CORE0_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORE0_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_TRACE ) || defined ( OS_LINK_KERNEL_TRACE_FAR )
if (exists(".bss.OS_PUBLIC_CORE0_VAR_NOINIT"))
{
  group OS_PUBLIC_CORE0_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_PUBLIC_CORE0_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_PUBLIC_CORE0_VAR_NOINIT";
    }
    reserved ".pad.OS_PUBLIC_CORE0_VAR_NOINIT" (size=16);
  }
  "_OS_PUBLIC_CORE0_VAR_NOINIT_START" = "_lc_gb_OS_PUBLIC_CORE0_VAR_NOINIT_GROUP";
  "_OS_PUBLIC_CORE0_VAR_NOINIT_END" = "_lc_ub__pad_OS_PUBLIC_CORE0_VAR_NOINIT" - 1;
  "_OS_PUBLIC_CORE0_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_PUBLIC_CORE0_VAR_NOINIT";
}
else
{
  "_OS_PUBLIC_CORE0_VAR_NOINIT_START" = 0;
  "_OS_PUBLIC_CORE0_VAR_NOINIT_END" = 0;
  "_OS_PUBLIC_CORE0_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_TRACE ) || defined ( OS_LINK_KERNEL_TRACE_NEAR )
if (exists(".bss.OS_PUBLIC_CORE0_VAR_FAST_NOINIT"))
{
  group OS_PUBLIC_CORE0_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_PUBLIC_CORE0_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_PUBLIC_CORE0_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_PUBLIC_CORE0_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_START" = "_lc_gb_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_GROUP";
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_PUBLIC_CORE0_VAR_FAST_NOINIT" - 1;
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_PUBLIC_CORE0_VAR_FAST_NOINIT";
}
else
{
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_START" = 0;
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_END" = 0;
  "_OS_PUBLIC_CORE0_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_BARRIERS ) || defined ( OS_LINK_KERNEL_BARRIERS_FAR )
if (exists(".bss.OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT"))
{
  group OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT" - 1;
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_BARRIERS ) || defined ( OS_LINK_KERNEL_BARRIERS_NEAR )
if (exists(".bss.OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_CORESTATUS ) || defined ( OS_LINK_KERNEL_CORESTATUS_FAR )
if (exists(".bss.OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT"))
{
  group OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT" - 1;
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_KERNEL_CORESTATUS ) || defined ( OS_LINK_KERNEL_CORESTATUS_NEAR )
if (exists(".bss.OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif


#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW
group OS_OsApplication_Core0_BSW_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_INIT )
if (exists(".data.OS_OsApplication_Core0_BSW_VAR"))
{
  group OS_OsApplication_Core0_BSW_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsApplication_Core0_BSW_VAR";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_NOINIT )
if (exists(".bss.OS_OsApplication_Core0_BSW_VAR_NOINIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsApplication_Core0_BSW_VAR_NOINIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_NOINIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOINIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOINIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOINIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss") || exists(".data.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss";
      select "[.]data.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_INIT )
if (exists(".zdata.OS_OsApplication_Core0_BSW_VAR_FAST"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_INIT )
if (exists(".data.OS_OsApplication_Core0_BSW_VAR_NOCACHE"))
{
  group OS_OsApplication_Core0_BSW_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsApplication_Core0_BSW_VAR_NOCACHE";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_NOCACHE" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOCACHE_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW
} /* OS_OsApplication_Core0_BSW_VAR_ALL_GROUP */
"_OS_OsApplication_Core0_BSW_VAR_ALL_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ALL_GROUP";
"_OS_OsApplication_Core0_BSW_VAR_ALL_END" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_ALL_GROUP" - 1;
"_OS_OsApplication_Core0_BSW_VAR_ALL_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_ALL_GROUP";

# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_OSAPPLICATION_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0
group OS_SystemApplication_OsCore0_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_INIT )
if (exists(".data.OS_SystemApplication_OsCore0_VAR"))
{
  group OS_SystemApplication_OsCore0_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_SystemApplication_OsCore0_VAR";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR" - 1;
  "_OS_SystemApplication_OsCore0_VAR_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_NOINIT )
if (exists(".bss.OS_SystemApplication_OsCore0_VAR_NOINIT"))
{
  group OS_SystemApplication_OsCore0_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_SystemApplication_OsCore0_VAR_NOINIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_NOINIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_NOINIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOINIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOINIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_SystemApplication_OsCore0_VAR_ZERO_INIT_bss") || exists(".data.OS_SystemApplication_OsCore0_VAR_ZERO_INIT"))
{
  group OS_SystemApplication_OsCore0_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_SystemApplication_OsCore0_VAR_ZERO_INIT_bss";
      select "[.]data.OS_SystemApplication_OsCore0_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_ZERO_INIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_ZERO_INIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_ZERO_INIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_INIT )
if (exists(".zdata.OS_SystemApplication_OsCore0_VAR_FAST"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOINIT"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_INIT )
if (exists(".data.OS_SystemApplication_OsCore0_VAR_NOCACHE"))
{
  group OS_SystemApplication_OsCore0_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_SystemApplication_OsCore0_VAR_NOCACHE";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_NOCACHE" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_NOCACHE_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE" - 1;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT"))
{
  group OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_APP ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0 ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0
} /* OS_SystemApplication_OsCore0_VAR_ALL_GROUP */
"_OS_SystemApplication_OsCore0_VAR_ALL_START" = "_lc_gb_OS_SystemApplication_OsCore0_VAR_ALL_GROUP";
"_OS_SystemApplication_OsCore0_VAR_ALL_END" = "_lc_ge_OS_SystemApplication_OsCore0_VAR_ALL_GROUP" - 1;
"_OS_SystemApplication_OsCore0_VAR_ALL_LIMIT" = "_lc_ge_OS_SystemApplication_OsCore0_VAR_ALL_GROUP";

# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK
group OS_C0_INIT_BSW_TASK_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_INIT )
if (exists(".data.OS_C0_INIT_BSW_TASK_VAR"))
{
  group OS_C0_INIT_BSW_TASK_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_C0_INIT_BSW_TASK_VAR";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_NOINIT )
if (exists(".bss.OS_C0_INIT_BSW_TASK_VAR_NOINIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_C0_INIT_BSW_TASK_VAR_NOINIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_NOINIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_NOINIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOINIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOINIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_bss") || exists(".data.OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_bss";
      select "[.]data.OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_INIT )
if (exists(".zdata.OS_C0_INIT_BSW_TASK_VAR_FAST"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_C0_INIT_BSW_TASK_VAR_FAST";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_INIT )
if (exists(".data.OS_C0_INIT_BSW_TASK_VAR_NOCACHE"))
{
  group OS_C0_INIT_BSW_TASK_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_C0_INIT_BSW_TASK_VAR_NOCACHE";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_NOCACHE" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_C0_INIT_BSW_TASK_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK
} /* OS_C0_INIT_BSW_TASK_VAR_ALL_GROUP */
"_OS_C0_INIT_BSW_TASK_VAR_ALL_START" = "_lc_gb_OS_C0_INIT_BSW_TASK_VAR_ALL_GROUP";
"_OS_C0_INIT_BSW_TASK_VAR_ALL_END" = "_lc_ge_OS_C0_INIT_BSW_TASK_VAR_ALL_GROUP" - 1;
"_OS_C0_INIT_BSW_TASK_VAR_ALL_LIMIT" = "_lc_ge_OS_C0_INIT_BSW_TASK_VAR_ALL_GROUP";

# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_C0_INIT_BSW_TASK_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0
group OS_IdleTask_OsCore0_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_INIT )
if (exists(".data.OS_IdleTask_OsCore0_VAR"))
{
  group OS_IdleTask_OsCore0_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_IdleTask_OsCore0_VAR";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_GROUP";
  "_OS_IdleTask_OsCore0_VAR_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR" - 1;
  "_OS_IdleTask_OsCore0_VAR_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_NOINIT )
if (exists(".bss.OS_IdleTask_OsCore0_VAR_NOINIT"))
{
  group OS_IdleTask_OsCore0_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_IdleTask_OsCore0_VAR_NOINIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_NOINIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_NOINIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_NOINIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_NOINIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOINIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOINIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_NOINIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOINIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_IdleTask_OsCore0_VAR_ZERO_INIT_bss") || exists(".data.OS_IdleTask_OsCore0_VAR_ZERO_INIT"))
{
  group OS_IdleTask_OsCore0_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_IdleTask_OsCore0_VAR_ZERO_INIT_bss";
      select "[.]data.OS_IdleTask_OsCore0_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_ZERO_INIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_ZERO_INIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_ZERO_INIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_ZERO_INIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_INIT )
if (exists(".zdata.OS_IdleTask_OsCore0_VAR_FAST"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_IdleTask_OsCore0_VAR_FAST";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_IdleTask_OsCore0_VAR_FAST_NOINIT"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_IdleTask_OsCore0_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOINIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOINIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_INIT )
if (exists(".data.OS_IdleTask_OsCore0_VAR_NOCACHE"))
{
  group OS_IdleTask_OsCore0_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_IdleTask_OsCore0_VAR_NOCACHE";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_NOCACHE" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_NOCACHE_GROUP";
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE" - 1;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT"))
{
  group OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0 ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_IdleTask_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0
} /* OS_IdleTask_OsCore0_VAR_ALL_GROUP */
"_OS_IdleTask_OsCore0_VAR_ALL_START" = "_lc_gb_OS_IdleTask_OsCore0_VAR_ALL_GROUP";
"_OS_IdleTask_OsCore0_VAR_ALL_END" = "_lc_ge_OS_IdleTask_OsCore0_VAR_ALL_GROUP" - 1;
"_OS_IdleTask_OsCore0_VAR_ALL_LIMIT" = "_lc_ge_OS_IdleTask_OsCore0_VAR_ALL_GROUP";

# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_IDLETASK_OSCORE0_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL
group OS_OsTask_Core0_Appl_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_INIT )
if (exists(".data.OS_OsTask_Core0_Appl_VAR"))
{
  group OS_OsTask_Core0_Appl_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsTask_Core0_Appl_VAR";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR" - 1;
  "_OS_OsTask_Core0_Appl_VAR_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_NOINIT )
if (exists(".bss.OS_OsTask_Core0_Appl_VAR_NOINIT"))
{
  group OS_OsTask_Core0_Appl_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_Appl_VAR_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_NOINIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOINIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOINIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_OsTask_Core0_Appl_VAR_ZERO_INIT_bss") || exists(".data.OS_OsTask_Core0_Appl_VAR_ZERO_INIT"))
{
  group OS_OsTask_Core0_Appl_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_Appl_VAR_ZERO_INIT_bss";
      select "[.]data.OS_OsTask_Core0_Appl_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_INIT )
if (exists(".zdata.OS_OsTask_Core0_Appl_VAR_FAST"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsTask_Core0_Appl_VAR_FAST";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOINIT"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_INIT )
if (exists(".data.OS_OsTask_Core0_Appl_VAR_NOCACHE"))
{
  group OS_OsTask_Core0_Appl_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsTask_Core0_Appl_VAR_NOCACHE";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_NOCACHE" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_NOCACHE_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE" - 1;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT"))
{
  group OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_Appl_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL
} /* OS_OsTask_Core0_Appl_VAR_ALL_GROUP */
"_OS_OsTask_Core0_Appl_VAR_ALL_START" = "_lc_gb_OS_OsTask_Core0_Appl_VAR_ALL_GROUP";
"_OS_OsTask_Core0_Appl_VAR_ALL_END" = "_lc_ge_OS_OsTask_Core0_Appl_VAR_ALL_GROUP" - 1;
"_OS_OsTask_Core0_Appl_VAR_ALL_LIMIT" = "_lc_ge_OS_OsTask_Core0_Appl_VAR_ALL_GROUP";

# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_APPL_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW
group OS_OsTask_Core0_BSW_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_INIT )
if (exists(".data.OS_OsTask_Core0_BSW_VAR"))
{
  group OS_OsTask_Core0_BSW_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsTask_Core0_BSW_VAR";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR" - 1;
  "_OS_OsTask_Core0_BSW_VAR_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_NOINIT )
if (exists(".bss.OS_OsTask_Core0_BSW_VAR_NOINIT"))
{
  group OS_OsTask_Core0_BSW_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_BSW_VAR_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_NOINIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOINIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOINIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_OsTask_Core0_BSW_VAR_ZERO_INIT_bss") || exists(".data.OS_OsTask_Core0_BSW_VAR_ZERO_INIT"))
{
  group OS_OsTask_Core0_BSW_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_BSW_VAR_ZERO_INIT_bss";
      select "[.]data.OS_OsTask_Core0_BSW_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_INIT )
if (exists(".zdata.OS_OsTask_Core0_BSW_VAR_FAST"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsTask_Core0_BSW_VAR_FAST";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOINIT"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_INIT )
if (exists(".data.OS_OsTask_Core0_BSW_VAR_NOCACHE"))
{
  group OS_OsTask_Core0_BSW_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_OsTask_Core0_BSW_VAR_NOCACHE";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_NOCACHE" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_NOCACHE_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE" - 1;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT"))
{
  group OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_TASK ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_OsTask_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW
} /* OS_OsTask_Core0_BSW_VAR_ALL_GROUP */
"_OS_OsTask_Core0_BSW_VAR_ALL_START" = "_lc_gb_OS_OsTask_Core0_BSW_VAR_ALL_GROUP";
"_OS_OsTask_Core0_BSW_VAR_ALL_END" = "_lc_ge_OS_OsTask_Core0_BSW_VAR_ALL_GROUP" - 1;
"_OS_OsTask_Core0_BSW_VAR_ALL_LIMIT" = "_lc_ge_OS_OsTask_Core0_BSW_VAR_ALL_GROUP";

# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_TASK_OSTASK_CORE0_BSW_NEAR_NOCACHE_ZERO_INIT
#endif



#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER
group OS_CounterIsr_SystemTimer_VAR_ALL_GROUP(ordered, contiguous, fill, align=8)
{
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_INIT )
if (exists(".data.OS_CounterIsr_SystemTimer_VAR"))
{
  group OS_CounterIsr_SystemTimer_VAR_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_CounterIsr_SystemTimer_VAR";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_NOINIT )
if (exists(".bss.OS_CounterIsr_SystemTimer_VAR_NOINIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CounterIsr_SystemTimer_VAR_NOINIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_NOINIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_NOINIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOINIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOINIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_ZERO_INIT )
if (exists(".bss.OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_bss") || exists(".data.OS_CounterIsr_SystemTimer_VAR_ZERO_INIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_bss";
      select "[.]data.OS_CounterIsr_SystemTimer_VAR_ZERO_INIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_ZERO_INIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_INIT )
if (exists(".zdata.OS_CounterIsr_SystemTimer_VAR_FAST"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_CounterIsr_SystemTimer_VAR_FAST";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_NOINIT )
if (exists(".zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_ZERO_INIT )
if (exists(".zbss.OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_bss") || exists(".zdata.OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_bss";
      select "[.]zdata.OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_INIT )
if (exists(".data.OS_CounterIsr_SystemTimer_VAR_NOCACHE"))
{
  group OS_CounterIsr_SystemTimer_VAR_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]data.OS_CounterIsr_SystemTimer_VAR_NOCACHE";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_NOCACHE" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_NOCACHE_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_NOINIT )
if (exists(".bss.OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_ZERO_INIT )
if (exists(".bss.OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_bss") || exists(".data.OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]bss.OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_bss";
      select "[.]data.OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_INIT )
if (exists(".zdata.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zdata.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_NOINIT )
if (exists(".zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_NOINIT_LIMIT" = 0;
}
#endif

#if defined ( OS_LINK_VAR ) || defined ( OS_LINK_VAR_ISR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE ) || defined ( OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_ZERO_INIT )
if (exists(".zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_bss") || exists(".zdata.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT"))
{
  group OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_GROUP(ordered, contiguous, fill, align=8)
  {
    section "OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_SECTION" (blocksize=2, attributes=rw)
    {
      select "[.]zbss.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_bss";
      select "[.]zdata.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT";
    }
    reserved ".pad.OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT" (size=16);
  }
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_GROUP";
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_END" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT" - 1;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = "_lc_ub__pad_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT";
}
else
{
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_START" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_END" = 0;
  "_OS_CounterIsr_SystemTimer_VAR_FAST_NOCACHE_ZERO_INIT_LIMIT" = 0;
}
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER
} /* OS_CounterIsr_SystemTimer_VAR_ALL_GROUP */
"_OS_CounterIsr_SystemTimer_VAR_ALL_START" = "_lc_gb_OS_CounterIsr_SystemTimer_VAR_ALL_GROUP";
"_OS_CounterIsr_SystemTimer_VAR_ALL_END" = "_lc_ge_OS_CounterIsr_SystemTimer_VAR_ALL_GROUP" - 1;
"_OS_CounterIsr_SystemTimer_VAR_ALL_LIMIT" = "_lc_ge_OS_CounterIsr_SystemTimer_VAR_ALL_GROUP";

# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_FAR_NOCACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_ZERO_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_CACHE_ZERO_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_ZERO_INIT
# undef OS_LINK_VAR_ISR_COUNTERISR_SYSTEMTIMER_NEAR_NOCACHE_ZERO_INIT
#endif


#ifdef OS_LINK_VAR
# undef OS_LINK_VAR
#endif

#ifdef OS_LINK_VAR_APP
# undef OS_LINK_VAR_APP
#endif

#ifdef OS_LINK_VAR_TASK
# undef OS_LINK_VAR_TASK
#endif

#ifdef OS_LINK_VAR_ISR
# undef OS_LINK_VAR_ISR
#endif

#ifdef OS_LINK_KERNEL_TRACE
# undef OS_LINK_KERNEL_TRACE
#endif

#ifdef OS_LINK_KERNEL_TRACE_FAR
# undef OS_LINK_KERNEL_TRACE_FAR
#endif

#ifdef OS_LINK_KERNEL_TRACE_NEAR
# undef OS_LINK_KERNEL_TRACE_NEAR
#endif

#ifdef OS_LINK_KERNEL_BARRIERS
# undef OS_LINK_KERNEL_BARRIERS
#endif

#ifdef OS_LINK_KERNEL_BARRIERS_FAR
# undef OS_LINK_KERNEL_BARRIERS_FAR
#endif

#ifdef OS_LINK_KERNEL_BARRIERS_NEAR
# undef OS_LINK_KERNEL_BARRIERS_NEAR
#endif

#ifdef OS_LINK_KERNEL_CORESTATUS
# undef OS_LINK_KERNEL_CORESTATUS
#endif

#ifdef OS_LINK_KERNEL_CORESTATUS_FAR
# undef OS_LINK_KERNEL_CORESTATUS_FAR
#endif

#ifdef OS_LINK_KERNEL_CORESTATUS_NEAR
# undef OS_LINK_KERNEL_CORESTATUS_NEAR
#endif

#ifdef OS_LINK_VAR_KERNEL
# undef OS_LINK_VAR_KERNEL
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR
# undef OS_LINK_VAR_KERNEL_FAR
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_CACHE
# undef OS_LINK_VAR_KERNEL_FAR_CACHE
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_CACHE_INIT
# undef OS_LINK_VAR_KERNEL_FAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT
# undef OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_NOCACHE
# undef OS_LINK_VAR_KERNEL_FAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_NOCACHE_INIT
# undef OS_LINK_VAR_KERNEL_FAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR
# undef OS_LINK_VAR_KERNEL_NEAR
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_CACHE
# undef OS_LINK_VAR_KERNEL_NEAR_CACHE
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
# undef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT
# undef OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_NOCACHE
# undef OS_LINK_VAR_KERNEL_NEAR_NOCACHE
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_NOCACHE_INIT
# undef OS_LINK_VAR_KERNEL_NEAR_NOCACHE_INIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_NOCACHE_NOINIT
# undef OS_LINK_VAR_KERNEL_NEAR_NOCACHE_NOINIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
# undef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
#endif

#ifdef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
# undef OS_LINK_VAR_KERNEL_NEAR_CACHE_INIT
#endif


