/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Hook.h
 *           Config:  DCDC_Appl.dpa
 *      ECU-Project:  DCDC_Appl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  Header file containing definitions for VFB tracing
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * Names of available VFB-Trace-Hooks
 **********************************************************************************************************************
 *
 * Not configured:
 *
 *  Rte_Runnable_BswM_BswM_MainFunction_Return
 *  Rte_Runnable_BswM_BswM_MainFunction_Start
 *  Rte_Runnable_EcuM_EcuM_MainFunction_Return
 *  Rte_Runnable_EcuM_EcuM_MainFunction_Start
 *  Rte_Runnable_EcuM_GetBootTarget_Return
 *  Rte_Runnable_EcuM_GetBootTarget_Start
 *  Rte_Runnable_EcuM_GetLastShutdownTarget_Return
 *  Rte_Runnable_EcuM_GetLastShutdownTarget_Start
 *  Rte_Runnable_EcuM_GetShutdownCause_Return
 *  Rte_Runnable_EcuM_GetShutdownCause_Start
 *  Rte_Runnable_EcuM_GetShutdownTarget_Return
 *  Rte_Runnable_EcuM_GetShutdownTarget_Start
 *  Rte_Runnable_EcuM_SelectBootTarget_Return
 *  Rte_Runnable_EcuM_SelectBootTarget_Start
 *  Rte_Runnable_EcuM_SelectShutdownCause_Return
 *  Rte_Runnable_EcuM_SelectShutdownCause_Start
 *  Rte_Runnable_EcuM_SelectShutdownTarget_Return
 *  Rte_Runnable_EcuM_SelectShutdownTarget_Start
 *  Rte_Runnable_Os_OsCore0_swc_GetCounterValue_Return
 *  Rte_Runnable_Os_OsCore0_swc_GetCounterValue_Start
 *  Rte_Runnable_Os_OsCore0_swc_GetElapsedValue_Return
 *  Rte_Runnable_Os_OsCore0_swc_GetElapsedValue_Start
 *  Rte_Task_Activate
 *  Rte_Task_Dispatch
 *  Rte_Task_Terminate
 *  Rte_Task_WaitEvent
 *  Rte_Task_WaitEventRet
 *  SchM_EnterHook_Adc_KernelData_Return
 *  SchM_EnterHook_Adc_KernelData_Start
 *  SchM_EnterHook_Adc_SrcRegAccess_Return
 *  SchM_EnterHook_Adc_SrcRegAccess_Start
 *  SchM_EnterHook_BswM_BSWM_EXCLUSIVE_AREA_0_Return
 *  SchM_EnterHook_BswM_BSWM_EXCLUSIVE_AREA_0_Start
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_0_Return
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_0_Start
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_1_Return
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_1_Start
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_2_Return
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_2_Start
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_3_Return
 *  SchM_EnterHook_EcuM_ECUM_EXCLUSIVE_AREA_3_Start
 *  SchM_EnterHook_Fls_17_Dmu_Erase_Return
 *  SchM_EnterHook_Fls_17_Dmu_Erase_Start
 *  SchM_EnterHook_Fls_17_Dmu_Init_Return
 *  SchM_EnterHook_Fls_17_Dmu_Init_Start
 *  SchM_EnterHook_Fls_17_Dmu_Main_Return
 *  SchM_EnterHook_Fls_17_Dmu_Main_Start
 *  SchM_EnterHook_Fls_17_Dmu_ResumeErase_Return
 *  SchM_EnterHook_Fls_17_Dmu_ResumeErase_Start
 *  SchM_EnterHook_Fls_17_Dmu_UserContentCount_Return
 *  SchM_EnterHook_Fls_17_Dmu_UserContentCount_Start
 *  SchM_EnterHook_Fls_17_Dmu_Write_Return
 *  SchM_EnterHook_Fls_17_Dmu_Write_Start
 *  SchM_EnterHook_Gpt_Get100UsPredefTimerValue_Return
 *  SchM_EnterHook_Gpt_Get100UsPredefTimerValue_Start
 *  SchM_EnterHook_Gpt_Get1UsPredefTimerValue_Return
 *  SchM_EnterHook_Gpt_Get1UsPredefTimerValue_Start
 *  SchM_EnterHook_Gpt_Gpt12StartTimer_Return
 *  SchM_EnterHook_Gpt_Gpt12StartTimer_Start
 *  SchM_EnterHook_Gpt_GtmStartTimer_Return
 *  SchM_EnterHook_Gpt_GtmStartTimer_Start
 *  SchM_EnterHook_Icu_17_TimerIp_CcuInterruptHandle_Return
 *  SchM_EnterHook_Icu_17_TimerIp_CcuInterruptHandle_Start
 *  SchM_EnterHook_Icu_17_TimerIp_CcuVariableupdate_Return
 *  SchM_EnterHook_Icu_17_TimerIp_CcuVariableupdate_Start
 *  SchM_EnterHook_Icu_17_TimerIp_EnableNotification_Return
 *  SchM_EnterHook_Icu_17_TimerIp_EnableNotification_Start
 *  SchM_EnterHook_Icu_17_TimerIp_EnableWakeup_Return
 *  SchM_EnterHook_Icu_17_TimerIp_EnableWakeup_Start
 *  SchM_EnterHook_Icu_17_TimerIp_GtmEnableEdgeCount_Return
 *  SchM_EnterHook_Icu_17_TimerIp_GtmEnableEdgeCount_Start
 *  SchM_EnterHook_Icu_17_TimerIp_GtmGetDutyCycle_Return
 *  SchM_EnterHook_Icu_17_TimerIp_GtmGetDutyCycle_Start
 *  SchM_EnterHook_Icu_17_TimerIp_ResetEdgeCount_Return
 *  SchM_EnterHook_Icu_17_TimerIp_ResetEdgeCount_Start
 *  SchM_EnterHook_Icu_17_TimerIp_SetActivationCondition_Return
 *  SchM_EnterHook_Icu_17_TimerIp_SetActivationCondition_Start
 *  SchM_EnterHook_Mcu_AtomAgcReg_Return
 *  SchM_EnterHook_Mcu_AtomAgcReg_Start
 *  SchM_EnterHook_Mcu_TomTgcReg_Return
 *  SchM_EnterHook_Mcu_TomTgcReg_Start
 *  SchM_EnterHook_Pwm_17_GtmCcu6_HandleNotification_Return
 *  SchM_EnterHook_Pwm_17_GtmCcu6_HandleNotification_Start
 *  SchM_EnterHook_Pwm_17_GtmCcu6_PeriodAndDutyUpdate_Return
 *  SchM_EnterHook_Pwm_17_GtmCcu6_PeriodAndDutyUpdate_Start
 *  SchM_ExitHook_Adc_KernelData_Return
 *  SchM_ExitHook_Adc_KernelData_Start
 *  SchM_ExitHook_Adc_SrcRegAccess_Return
 *  SchM_ExitHook_Adc_SrcRegAccess_Start
 *  SchM_ExitHook_BswM_BSWM_EXCLUSIVE_AREA_0_Return
 *  SchM_ExitHook_BswM_BSWM_EXCLUSIVE_AREA_0_Start
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_0_Return
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_0_Start
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_1_Return
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_1_Start
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_2_Return
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_2_Start
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_3_Return
 *  SchM_ExitHook_EcuM_ECUM_EXCLUSIVE_AREA_3_Start
 *  SchM_ExitHook_Fls_17_Dmu_Erase_Return
 *  SchM_ExitHook_Fls_17_Dmu_Erase_Start
 *  SchM_ExitHook_Fls_17_Dmu_Init_Return
 *  SchM_ExitHook_Fls_17_Dmu_Init_Start
 *  SchM_ExitHook_Fls_17_Dmu_Main_Return
 *  SchM_ExitHook_Fls_17_Dmu_Main_Start
 *  SchM_ExitHook_Fls_17_Dmu_ResumeErase_Return
 *  SchM_ExitHook_Fls_17_Dmu_ResumeErase_Start
 *  SchM_ExitHook_Fls_17_Dmu_UserContentCount_Return
 *  SchM_ExitHook_Fls_17_Dmu_UserContentCount_Start
 *  SchM_ExitHook_Fls_17_Dmu_Write_Return
 *  SchM_ExitHook_Fls_17_Dmu_Write_Start
 *  SchM_ExitHook_Gpt_Get100UsPredefTimerValue_Return
 *  SchM_ExitHook_Gpt_Get100UsPredefTimerValue_Start
 *  SchM_ExitHook_Gpt_Get1UsPredefTimerValue_Return
 *  SchM_ExitHook_Gpt_Get1UsPredefTimerValue_Start
 *  SchM_ExitHook_Gpt_Gpt12StartTimer_Return
 *  SchM_ExitHook_Gpt_Gpt12StartTimer_Start
 *  SchM_ExitHook_Gpt_GtmStartTimer_Return
 *  SchM_ExitHook_Gpt_GtmStartTimer_Start
 *  SchM_ExitHook_Icu_17_TimerIp_CcuInterruptHandle_Return
 *  SchM_ExitHook_Icu_17_TimerIp_CcuInterruptHandle_Start
 *  SchM_ExitHook_Icu_17_TimerIp_CcuVariableupdate_Return
 *  SchM_ExitHook_Icu_17_TimerIp_CcuVariableupdate_Start
 *  SchM_ExitHook_Icu_17_TimerIp_EnableNotification_Return
 *  SchM_ExitHook_Icu_17_TimerIp_EnableNotification_Start
 *  SchM_ExitHook_Icu_17_TimerIp_EnableWakeup_Return
 *  SchM_ExitHook_Icu_17_TimerIp_EnableWakeup_Start
 *  SchM_ExitHook_Icu_17_TimerIp_GtmEnableEdgeCount_Return
 *  SchM_ExitHook_Icu_17_TimerIp_GtmEnableEdgeCount_Start
 *  SchM_ExitHook_Icu_17_TimerIp_GtmGetDutyCycle_Return
 *  SchM_ExitHook_Icu_17_TimerIp_GtmGetDutyCycle_Start
 *  SchM_ExitHook_Icu_17_TimerIp_ResetEdgeCount_Return
 *  SchM_ExitHook_Icu_17_TimerIp_ResetEdgeCount_Start
 *  SchM_ExitHook_Icu_17_TimerIp_SetActivationCondition_Return
 *  SchM_ExitHook_Icu_17_TimerIp_SetActivationCondition_Start
 *  SchM_ExitHook_Mcu_AtomAgcReg_Return
 *  SchM_ExitHook_Mcu_AtomAgcReg_Start
 *  SchM_ExitHook_Mcu_TomTgcReg_Return
 *  SchM_ExitHook_Mcu_TomTgcReg_Start
 *  SchM_ExitHook_Pwm_17_GtmCcu6_HandleNotification_Return
 *  SchM_ExitHook_Pwm_17_GtmCcu6_HandleNotification_Start
 *  SchM_ExitHook_Pwm_17_GtmCcu6_PeriodAndDutyUpdate_Return
 *  SchM_ExitHook_Pwm_17_GtmCcu6_PeriodAndDutyUpdate_Start
 *  SchM_Schedulable_BswM_BswM_MainFunction_Return
 *  SchM_Schedulable_BswM_BswM_MainFunction_Start
 *  SchM_Schedulable_EcuM_EcuM_MainFunction_Return
 *  SchM_Schedulable_EcuM_EcuM_MainFunction_Start
 *  SchM_Schedulable_Fls_17_Dmu_Fls_17_Dmu_MainFunction_Return
 *  SchM_Schedulable_Fls_17_Dmu_Fls_17_Dmu_MainFunction_Start
 *
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_HOOK_H
# define RTE_HOOK_H

# include "Os.h" /* PRQA S 0828, 0883 */ /* MD_MSR_Dir1.1, MD_Rte_Os */

# include "Rte_Type.h"
# include "Rte_Cfg.h"

# ifndef RTE_VFB_TRACE
#  define RTE_VFB_TRACE (0)
# endif

#endif /* RTE_HOOK_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
