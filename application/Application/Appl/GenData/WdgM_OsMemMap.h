/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: WdgM
 *           Program: MSR_Daimler_SLP11
 *          Customer: Mercedes-Benz AG
 *       Expiry Date: Not restricted
 *  Ordered Derivat.:  TC399
 *    License Scope : The usage is restricted to CBD2200333_D02
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: WdgM_OsMemMap.h
 *   Generation Time: 2023-03-01 01:20:38
 *           Project: PP_DemoECU - Version 1.0
 *          Delivery: CBD2200333_D02
 *      Tool Version: DaVinci Configurator Classic 5.25.37 SP2
 *
 *
 *********************************************************************************************************************/


/* PRQA S 0883 EOF */ /* MD_WdgM_0883 */

/********************************************************************************************************************** 
 *  Memory sections for core 0 
 *********************************************************************************************************************/ 
/* Memory section(s) for supervised entity 'SE_C0_ASIL_EXTENDED_TASK1' */ 
#ifdef WDGM_SE0_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE0_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE0_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE0_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE0_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE0_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
/* Supervised entity 'SE_C0_ASIL_EXTENDED_TASK1' has an alive counter */ 
#ifdef WDGM_SE0_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE0_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE0_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE0_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE0_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE0_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C0_ASIL_BASIC_CYCLIC_TASK' */ 
#ifdef WDGM_SE1_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE1_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE1_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE1_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE1_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE1_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
/* Supervised entity 'SE_C0_ASIL_BASIC_CYCLIC_TASK' has an alive counter */ 
#ifdef WDGM_SE1_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE1_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE1_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE1_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE1_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE1_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C0_ASIL_EXTENDED_TASK2' */ 
#ifdef WDGM_SE2_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE2_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE2_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE2_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE2_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE2_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
/* Supervised entity 'SE_C0_ASIL_EXTENDED_TASK2' has an alive counter */ 
#ifdef WDGM_SE2_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE2_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE2_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE2_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE2_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE2_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C0_ASIL_BASIC_OUTPUT_TASK' */ 
#ifdef WDGM_SE3_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE3_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE3_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE3_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE3_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE3_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
/* Supervised entity 'SE_C0_ASIL_BASIC_OUTPUT_TASK' has an alive counter */ 
#ifdef WDGM_SE3_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE3_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE3_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE3_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE3_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE3_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C0_ASIL_EXTENDED_TASK3' */ 
#ifdef WDGM_SE4_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE4_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE4_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE4_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_SE4_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE4_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
/* Supervised entity 'SE_C0_ASIL_EXTENDED_TASK3' has an alive counter */ 
#ifdef WDGM_SE4_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE4_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE4_START_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_SE4_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE 
# undef WDGM_SE4_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE4_STOP_SEC_VAR_NOINIT_32BIT_CORE0_PRIVATE */ 
 
 
/********************************************************************************************************************** 
 *  Memory section(s) for core specific global data - core 0 
 *********************************************************************************************************************/ 
/* 
 * Read by all modules (tasks), written by WdgM_MainFunction() and WdgM_Init() only 
 */ 
#ifdef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE 
# undef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE0_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE0_PRIVATE 
# undef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_32BIT 
#endif /* ifdef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE0_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE0_PRIVATE 
# undef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE0_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_32BIT 
#endif /* ifdef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE0_PRIVATE */ 
 
 
 
/********************************************************************************************************************** 
 *  Memory sections for core 1 
 *********************************************************************************************************************/ 
/* Memory section(s) for supervised entity 'SE_C1_ASIL_BASIC_INPUT_TASK' */ 
#ifdef WDGM_SE5_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE5_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE5_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE5_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE5_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE5_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
/* Supervised entity 'SE_C1_ASIL_BASIC_INPUT_TASK' has an alive counter */ 
#ifdef WDGM_SE5_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE5_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE5_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE5_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE5_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE5_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C1_ASIL_BASIC_ALGO_TASK' */ 
#ifdef WDGM_SE6_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE6_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE6_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE6_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE6_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE6_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
/* Supervised entity 'SE_C1_ASIL_BASIC_ALGO_TASK' has an alive counter */ 
#ifdef WDGM_SE6_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE6_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE6_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE6_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE6_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE6_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C1_ASIL_EXTENDED_TASK1' */ 
#ifdef WDGM_SE7_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE7_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE7_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE7_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE7_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE7_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
/* Supervised entity 'SE_C1_ASIL_EXTENDED_TASK1' has an alive counter */ 
#ifdef WDGM_SE7_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE7_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE7_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE7_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE7_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE7_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
 
/* Memory section(s) for supervised entity 'SE_C1_ASIL_BASIC_OUTPUT_TASK' */ 
#ifdef WDGM_SE8_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE8_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE8_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE8_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_SE8_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_SE8_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
/* Supervised entity 'SE_C1_ASIL_BASIC_OUTPUT_TASK' has an alive counter */ 
#ifdef WDGM_SE8_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE8_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE8_START_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
#ifdef WDGM_SE8_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE 
# undef WDGM_SE8_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_32BIT 
#endif /* ifdef WDGM_SE8_STOP_SEC_VAR_NOINIT_32BIT_CORE1_PRIVATE */ 
 
 
/********************************************************************************************************************** 
 *  Memory section(s) for core specific global data - core 1 
 *********************************************************************************************************************/ 
/* 
 * Read by all modules (tasks), written by WdgM_MainFunction() and WdgM_Init() only 
 */ 
#ifdef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_START_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE 
# undef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_STOP_SEC_VAR_NOINIT_UNSPECIFIED_CORE1_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE1_PRIVATE 
# undef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_OsApplication_Core1_ASIL_VAR_32BIT 
#endif /* ifdef WDGM_GLOBAL_START_SEC_VAR_32BIT_CORE1_PRIVATE */ 
 
#ifdef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE1_PRIVATE 
# undef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE1_PRIVATE /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_OsApplication_Core1_ASIL_VAR_32BIT 
#endif /* ifdef WDGM_GLOBAL_STOP_SEC_VAR_32BIT_CORE1_PRIVATE */ 
 
 
 
/********************************************************************************************************************** 
 *  Memory section for WdgM global shared data 
 *********************************************************************************************************************/ 
/* 
 * Read and write by all modules (tasks) 
 * WdgM read:  WdgM_CheckpointReached() and WdgM_MainFunction() 
 * WdgM write: WdgM_CheckpointReached() and WdgM_Init() 
 * Variables which are accessed by multiple cores must not be cached. 
 */ 
#ifdef WDGM_GLOBAL_SHARED_START_SEC_VAR_NOINIT_UNSPECIFIED 
# undef WDGM_GLOBAL_SHARED_START_SEC_VAR_NOINIT_UNSPECIFIED /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_START_SEC_GLOBALSHARED_VAR_NOCACHE_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_SHARED_START_SEC_VAR_NOINIT_UNSPECIFIED */ 
#ifdef WDGM_GLOBAL_SHARED_STOP_SEC_VAR_NOINIT_UNSPECIFIED 
# undef WDGM_GLOBAL_SHARED_STOP_SEC_VAR_NOINIT_UNSPECIFIED /* PRQA S 0841 */ /* MD_MSR_Undef */ 
# define OS_STOP_SEC_GLOBALSHARED_VAR_NOCACHE_NOINIT_UNSPECIFIED 
#endif /* ifdef WDGM_GLOBAL_SHARED_STOP_SEC_VAR_NOINIT_UNSPECIFIED */ 


/**********************************************************************************************************************
 *  END OF FILE: WdgM_OsMemMap.h
 *********************************************************************************************************************/

