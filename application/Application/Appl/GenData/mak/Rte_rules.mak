# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
#   \verbatim
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   \endverbatim
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#              File:  Rte_rules.mak
#            Config:  DCDC_Appl.dpa
#       ECU-Project:  DCDC_Appl
# 
#         Generator:  MICROSAR RTE Generator Version 4.29.0
#                     RTE Core Version 1.29.0
#           License:  CBD2200333
# 
#       Description:  GNU MAKEFILE (rules)
# *********************************************************************************************************************


GENERATED_SOURCE_FILES       += $(GENDATA_DIR)\Rte.c
