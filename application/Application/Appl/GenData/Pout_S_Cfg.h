
/**
 * CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)
 *
 * This is an unpublished work of authorship, which contains trade secrets,
 * created in 2018. Hella KGaA Hueck & Co. owns all rights to this work and
 * intends to maintain it in confidence to preserve its trade secret status.
 * Hella KGaA Hueck & Co. reserves the right, under the copyright laws of
 * Germany or those of any other country that may have jurisdiction, to
 * protect this work as an unpublished work, in the event of an inadvertent or
 * deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves
 * its rights under all copyright laws to protect this work as a published
 * work, when appropriate. Those having access to this work may not copy it,
 * use it, modify it or disclose the information contained in it without the
 * written authorization of Hella KGaA Hueck & Co..
 */

/*******************************************************************************
 * History
 * created by: catabo1 Sat Oct 15 17:27:04 EEST 2022
 * generator version:  1.2.3.0
 *
*******************************************************************************/

/**
    Filename          :   Pout_S_Cfg.h
    Author            :   $Author: (catabo1)$
    Date              :   $Date: Sat Oct 15 17:27:04 EEST 2022$
    Version           :   $Revision: 1.2 $
    Status            :   $State: ACCEPTED $
    Company           :   HRO
 */

/*################################################################################################################### */
/*    Project     :   SafeIO                                                                                          */
/*    Module      :   Pout_S                                                                                          */
/*    Filename    :   $Source: CFG_H_file.java $                                                                      */
/*    Author      :   $Author: Spataru Simona Camelia spatsi1 (spatsi1) $                                               */
/*    Version     :   $Revision: 1.18 $                                                                                */
/*    Status      :   $State: IN_WORK $                                                                               */
/*    Company     :   HRO                                                                                             */
/*    Department  :   HRO-E-GSH12                                                                                    */
/*    Limitations :   NA                                                                                              */
/*    Description :   This file contains the precompile configuration of the Pout_S module.                           */
/*################################################################################################################### */
/*    Changes     :                                                                                                   */
/*    $Log        :   Pout_S_Cfg.h $                                                                                  */
/*####################################################################################################################*/
/*  Module                                                                                                            */
/*  Release                                                                                                           */
/*  Version      Date          Author       ChangeID    Summary                                                       */
/*                                                                                                                    */
/*  1.0.0        13.08.2018    catabo1      853589      Pout_S New Development                                        */
/*  1.1.0        10.01.2019    mocaio1      871424      SafeIO 2: Pout_S : Update for variable frequency              */
/*                                                      and delete SafeIO_S_PeriodType from Pout_S_swc.arxml          */
/*  1.1.1        30.04.2019    mocaio1      830496      SafeIO 2: Qualify Pout_S in ASIL B quality                    */
/*  1.2.0        31.07.2019    catabo1      981771      SafeIO 2: Pout_S immediate write functionality follow-up      */
/*  1.2.1        20.12.2019    catabo1      1019198     SafeIO 2: Pout_S improve resources performance                */
/*  1.2.2        09.10.2020    spatsi1      1183295     Safe IO: Create Pout_S release for Infineon Aurix             */
/*  1.2.3        22.10.2021    spatsi1      1364797     Pout_S: PWM Output Handling support for variable period       */
/*                                                              functionality and update for AR 4.4                   */
/*####################################################################################################################*/

#ifndef POUT_S_CFG_H
#define POUT_S_CFG_H


/* Software version */
#define POUT_S_CFG_SW_MAJOR_VERSION (1U)
#define POUT_S_CFG_SW_MINOR_VERSION (2U)
#define POUT_S_CFG_SW_PATCH_VERSION (3U)


/* AUTOSAR version */
#define POUT_S_CFG_AR_RELEASE_MAJOR_VERSION    (4U)
#define POUT_S_CFG_AR_RELEASE_MINOR_VERSION    (4U)
#define POUT_S_CFG_AR_RELEASE_REVISION_VERSION (0U)


/*******************************************************************************************************************************************************/
/*                                                        CHANNELS RELATED DEFINES                                                                     */
/*******************************************************************************************************************************************************/

/* Logical Signals ID'S */
#define OUT_PWM_HSD1  (uint8)0x00U
#define OUT_PWM_HSD2  (uint8)0x01U
#define OUT_PWM_HSD3  (uint8)0x02U
#define OUT_PWM_HSD4  (uint8)0x03U

/*******************************************************************************************************************************************************/


/* Define for invalid PeriodId */
#define POUT_S_INVALID_PERIOD_ID	(Pwm_S1_ClockSourceType)PWM_INVALID_CLOCK_SOURCE

/* Pout_S channel which have variable frequency functionality selected for PLU_HD_Variant*/
/* Defines for period timer ticks and microseconds for OUT_PWM_HSD1*/
#define POUT_S_REF_PERIOD_TIMERTICKS_OUT_PWM_HSD1_PLU_HD_Variant      (SafeIO_S_FrequencyType)25UL
#define POUT_S_REF_PERIOD_TIME_US_OUT_PWM_HSD1_PLU_HD_Variant		   (SafeIO_S_FrequencyType)8UL

/* Defines for period timer ticks and microseconds for OUT_PWM_HSD2*/
#define POUT_S_REF_PERIOD_TIMERTICKS_OUT_PWM_HSD2_PLU_HD_Variant      (SafeIO_S_FrequencyType)25UL
#define POUT_S_REF_PERIOD_TIME_US_OUT_PWM_HSD2_PLU_HD_Variant		   (SafeIO_S_FrequencyType)8UL

/* Defines for period timer ticks and microseconds for OUT_PWM_HSD3*/
#define POUT_S_REF_PERIOD_TIMERTICKS_OUT_PWM_HSD3_PLU_HD_Variant      (SafeIO_S_FrequencyType)25UL
#define POUT_S_REF_PERIOD_TIME_US_OUT_PWM_HSD3_PLU_HD_Variant		   (SafeIO_S_FrequencyType)8UL

/* Defines for period timer ticks and microseconds for OUT_PWM_HSD4*/
#define POUT_S_REF_PERIOD_TIMERTICKS_OUT_PWM_HSD4_PLU_HD_Variant      (SafeIO_S_FrequencyType)25UL
#define POUT_S_REF_PERIOD_TIME_US_OUT_PWM_HSD4_PLU_HD_Variant		   (SafeIO_S_FrequencyType)8UL


/* Switch for including/not including version info API */
#define POUT_S_VERSION_INFO_API STD_ON
/* Parameter checking switch for the Pout_S module */
#define POUT_S_PARAM_ERROR_DETECT PARAM_CHECK_ON
/* Development error report switch for the Pout_S module */
#define POUT_S_DEV_ERROR_DETECT STD_ON
/* Switch for including/not including RTE header */
#define POUT_S_INCLUDE_RTE  STD_OFF

//#include "Pout_S_Cbk.h"

/* Pout_S module state typedef */
typedef uint8 Pout_S_StateType;

/* Constants declaration */

#endif
