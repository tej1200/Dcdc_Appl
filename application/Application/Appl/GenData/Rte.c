/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte.c
 *           Config:  DCDC_Appl.dpa
 *      ECU-Project:  DCDC_Appl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  RTE implementation file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0857 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define RTE_CORE
#include "Os.h" /* PRQA S 0828, 0883 */ /* MD_MSR_Dir1.1, MD_Rte_Os */
#include "Rte_Type.h"
#include "Rte_Main.h"

#include "Rte_BswM.h"
#include "Rte_EcuM.h"
#include "Rte_Os_OsCore0_swc.h"
#include "SchM_Adc.h"
#include "SchM_BswM.h"
#include "SchM_Dio.h"
#include "SchM_EcuM.h"
#include "SchM_Fls_17_Dmu.h"
#include "SchM_Gpt.h"
#include "SchM_Icu_17_TimerIp.h"
#include "SchM_Mcu.h"
#include "SchM_Port.h"
#include "SchM_Pwm_17_GtmCcu6.h"

#include "Rte_Hook.h"

/* AUTOSAR 3.x compatibility */
#if !defined (RTE_LOCAL)
# define RTE_LOCAL static
#endif


/**********************************************************************************************************************
 * API for enable / disable interrupts global
 *********************************************************************************************************************/

#if defined(osDisableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableAllInterrupts() osDisableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_DisableAllInterrupts() DisableAllInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableAllInterrupts() osEnableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_EnableAllInterrupts() EnableAllInterrupts()   /* AUTOSAR OS */
#endif

/**********************************************************************************************************************
 * API for enable / disable interrupts up to the systemLevel
 *********************************************************************************************************************/

#if defined(osDisableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableOSInterrupts() osDisableLevelKM()   /* MICROSAR OS */
#else
# define Rte_DisableOSInterrupts() SuspendOSInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableOSInterrupts() osEnableLevelKM()   /* MICROSAR OS */
#else
# define Rte_EnableOSInterrupts() ResumeOSInterrupts()   /* AUTOSAR OS */
#endif


/**********************************************************************************************************************
 * Timer handling
 *********************************************************************************************************************/

#if defined OS_US2TICKS_SystemTimer
# define RTE_USEC_SystemTimer OS_US2TICKS_SystemTimer
#else
# define RTE_USEC_SystemTimer(val) ((TickType)RTE_CONST_USEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_MS2TICKS_SystemTimer
# define RTE_MSEC_SystemTimer OS_MS2TICKS_SystemTimer
#else
# define RTE_MSEC_SystemTimer(val) ((TickType)RTE_CONST_MSEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_SEC2TICKS_SystemTimer
# define RTE_SEC_SystemTimer OS_SEC2TICKS_SystemTimer
#else
# define RTE_SEC_SystemTimer(val)  ((TickType)RTE_CONST_SEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#define RTE_CONST_MSEC_SystemTimer_0U (0UL)
#define RTE_CONST_MSEC_SystemTimer_10U (10UL)


/**********************************************************************************************************************
 * Internal definitions
 *********************************************************************************************************************/

#define RTE_TASK_TIMEOUT_EVENT_MASK   ((EventMaskType)0x01)
#define RTE_TASK_WAITPOINT_EVENT_MASK ((EventMaskType)0x02)

/**********************************************************************************************************************
 * RTE life cycle API
 *********************************************************************************************************************/

#define RTE_START_SEC_CODE
#include "Rte_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) SchM_Init(void)
{
  /* activate the tasks */
  (void)ActivateTask(OsTask_Core0_BSW); /* PRQA S 3417 */ /* MD_Rte_Os */

  /* activate the alarms used for TimingEvents */
  (void)SetRelAlarm(Rte_Al_TE2_OsTask_Core0_BSW_0_10ms, RTE_MSEC_SystemTimer(0U) + (TickType)1U, RTE_MSEC_SystemTimer(10U)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */

}

FUNC(Std_ReturnType, RTE_CODE) Rte_Start(void)
{
  return RTE_E_OK;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Stop(void)
{
  return RTE_E_OK;
}

FUNC(void, RTE_CODE) SchM_Deinit(void)
{
  /* deactivate alarms */
  (void)CancelAlarm(Rte_Al_TE2_OsTask_Core0_BSW_0_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */

}

FUNC(void, RTE_CODE) Rte_InitMemory(void)
{
}


/**********************************************************************************************************************
 * Exclusive area access
 *********************************************************************************************************************/

FUNC(void, RTE_CODE) SchM_Enter_Adc_KernelData(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_KernelData(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_SrcRegAccess(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_SrcRegAccess(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_Erase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_Erase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_Main(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_Main(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_ResumeErase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_ResumeErase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_UserContentCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_UserContentCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Dmu_Write(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Dmu_Write(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Gpt_Get100UsPredefTimerValue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Gpt_Get100UsPredefTimerValue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Gpt_Get1UsPredefTimerValue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Gpt_Get1UsPredefTimerValue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Gpt_Gpt12StartTimer(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Gpt_Gpt12StartTimer(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Gpt_GtmStartTimer(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Gpt_GtmStartTimer(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_CcuInterruptHandle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_CcuInterruptHandle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_CcuVariableupdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_CcuVariableupdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_EnableNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_EnableNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_EnableWakeup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_EnableWakeup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_GtmEnableEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_GtmEnableEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_GtmGetDutyCycle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_GtmGetDutyCycle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_ResetEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_ResetEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_TimerIp_SetActivationCondition(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_TimerIp_SetActivationCondition(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Mcu_AtomAgcReg(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Mcu_AtomAgcReg(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Mcu_TomTgcReg(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Mcu_TomTgcReg(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_GtmCcu6_HandleNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_GtmCcu6_HandleNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_GtmCcu6_PeriodAndDutyUpdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_GtmCcu6_PeriodAndDutyUpdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


#define RTE_STOP_SEC_CODE
#include "Rte_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Task bodies for RTE controlled tasks
 *********************************************************************************************************************/
#define RTE_START_SEC_OSTASK_CORE0_BSW_CODE
#include "Rte_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#include "Gpt.h"
#include "Adc.h"
#include "Dio.h"
#include "IfxEvadc_reg.h"
#include "Pwm_17_GtmCcu6.h"

/** \brief Group ${x} Result Register ${y} */
typedef struct _ADCVoltageValue
{
    Ifx_UReg_32Bit v12TempRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
	Ifx_UReg_32Bit v48TempRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
	Ifx_UReg_32Bit v48SplyCurrentRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
	Ifx_UReg_32Bit v12SplyCurrentRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
	Ifx_UReg_32Bit v48SplyVoltageRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
	Ifx_UReg_32Bit v12SplyVoltageRESULT:16;         /**< \brief [15:0] Result of Most Recent Conversion - RESULT (rwh) */
} ADCVoltageValue;
ADCVoltageValue sADInputValue;

static void Ain_S_StartUp_12VTemperatureMonitoring(void);
static void Ain_S_StartUp_12VTemperatureMonitoring(void)
{ 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES0.B.VF);
 
    sADInputValue.v12TempRESULT = EVADC_G1RES0.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}

static void Ain_S_StartUp_48VTemperatureMonitoring(void);
static void Ain_S_StartUp_48VTemperatureMonitoring(void)
{
    
 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES1.B.VF);
 
    sADInputValue.v48TempRESULT = EVADC_G1RES1.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}


static void Ain_S_StartUp_48VSupplyCurrentValue(void);
static void Ain_S_StartUp_48VSupplyCurrentValue(void)
{
    
 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES3.B.VF);
 
    sADInputValue.v48SplyCurrentRESULT = EVADC_G1RES3.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}

static void Ain_S_StartUp_12VSupplyCurrentValue(void);
static void Ain_S_StartUp_12VSupplyCurrentValue(void)
{
    
 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES4.B.VF);
 
    sADInputValue.v12SplyCurrentRESULT = EVADC_G1RES4.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}


static void Ain_S_StartUp_48VSupplyVoltageValue(void);
static void Ain_S_StartUp_48VSupplyVoltageValue(void)
{
    
 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES5.B.VF);
 
    sADInputValue.v48SplyVoltageRESULT = EVADC_G1RES5.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}

static void Ain_S_StartUp_12VSupplyVoltageValue(void);
static void Ain_S_StartUp_12VSupplyVoltageValue(void)
{
    
 
    /* Starts a conversion with SW trigger source for Adc3_StartUp group */
    Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
 
    /* Wait until the measurement is completed - KL30 */
    do{
 
    }while ((uint32)1 != EVADC_G1RES6.B.VF);
 
    sADInputValue.v12SplyVoltageRESULT = EVADC_G1RES6.B.RESULT; 
    
    /* Stop conversion with SW trigger source for Adc3_StartUp group */
    Adc_StopGroupConversion(AdcConf_AdcGroup_AdcGroup_0);
}

#include "Dio_Cfg.h"
volatile static Std_ReturnType timerReturnValue;
volatile static Std_ReturnType timerPreviousReturnValue;
volatile static uint32 BSWMCyclicTask;
volatile static SetPWMCycle;
volatile static ucCount;

/* Macros */
#define PWM_PERIOD                    (0x138U)
#define PWM_DUTY                      (0x3D09U)
/**********************************************************************************************************************
 * Task:     OsTask_Core0_BSW
 * Priority: 60
 * Schedule: FULL
 *********************************************************************************************************************/
TASK(OsTask_Core0_BSW) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
  EventMaskType ev;

  for(;;)
  {
    (void)WaitEvent(Rte_Ev_Cyclic2_OsTask_Core0_BSW_0_10ms | Rte_Ev_Run1_EcuM_EcuM_MainFunction | Rte_Ev_RunS1_EcuM_EcuM_MainFunction); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)GetEvent(OsTask_Core0_BSW, &ev); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)ClearEvent(ev & (Rte_Ev_Cyclic2_OsTask_Core0_BSW_0_10ms | Rte_Ev_Run1_EcuM_EcuM_MainFunction | Rte_Ev_RunS1_EcuM_EcuM_MainFunction)); /* PRQA S 3417 */ /* MD_Rte_Os */

    if ((ev & Rte_Ev_Cyclic2_OsTask_Core0_BSW_0_10ms) != (EventMaskType)0)
    {
			
			
			/*Read ADC Value*/		
			Ain_S_StartUp_12VTemperatureMonitoring();
			Ain_S_StartUp_48VTemperatureMonitoring();
			
			//if(SetPWMCycle==1)
			//{
				/* Start PWM */
				/*Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_0, PWM_PERIOD, PWM_DUTY);
				Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_1, PWM_PERIOD, PWM_DUTY);
				Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_2, PWM_PERIOD, PWM_DUTY);
				Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_3, PWM_PERIOD, PWM_DUTY);
				Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_4, PWM_PERIOD, PWM_DUTY);
				Pwm_17_GtmCcu6_SetPeriodAndDuty(Pwm_17_GtmCcu6Conf_PwmChannel_PwmChannel_5, PWM_PERIOD, PWM_DUTY);*/
				
				/*Donot Remove Manual chagnes*/
				SysSysFunc_ENERGY_DIRECTION();
				VoltConverter();
			//}		
			if(ucCount%2==0)
			{
				Dio_WriteChannel(DioConf_DioChannel_DioChannel_P15_00,1);
				Dio_WriteChannel(DioConf_DioChannel_DioChannel_P15_01,1);
			}
			else
			{
				Dio_WriteChannel(DioConf_DioChannel_DioChannel_P15_00,0);
				Dio_WriteChannel(DioConf_DioChannel_DioChannel_P15_01,0);
				
			}
			ucCount++;
			//Ain_S_StartUp_48VSupplyCurrentValue();
			//Ain_S_StartUp_12VSupplyCurrentValue();
			//Ain_S_StartUp_48VSupplyVoltageValue();
			//Ain_S_StartUp_12VSupplyVoltageValue();
			
			/* call runnable */
			BswM_MainFunction(); /* PRQA S 2987 */ /* MD_Rte_2987 */			
		
			//timerReturnValue = Gpt_lGtmGetTimeElapsed(0);
		  
			if(timerPreviousReturnValue !=0)
			{
				BSWMCyclicTask = timerReturnValue - timerPreviousReturnValue;
			}
		  
		  timerPreviousReturnValue = timerReturnValue;
    }

    if ((ev & Rte_Ev_Run1_EcuM_EcuM_MainFunction) != (EventMaskType)0)
    {
      /* call runnable */
      EcuM_MainFunction(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }
  }
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

#define RTE_STOP_SEC_OSTASK_CORE0_BSW_CODE
#include "Rte_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3408:  MISRA rule: Rule8.4
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
