/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Fls_17_Dmu.c
 *           Config:  DCDC_Appl.dpa
 *        SW-C Type:  Fls_17_Dmu
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Fls_17_Dmu.h"
#include "TSC_SchM_Fls_17_Dmu.h"
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_Erase(void)
{
  SchM_Enter_Fls_17_Dmu_Erase();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_Erase(void)
{
  SchM_Exit_Fls_17_Dmu_Erase();
}
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_Init(void)
{
  SchM_Enter_Fls_17_Dmu_Init();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_Init(void)
{
  SchM_Exit_Fls_17_Dmu_Init();
}
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_Main(void)
{
  SchM_Enter_Fls_17_Dmu_Main();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_Main(void)
{
  SchM_Exit_Fls_17_Dmu_Main();
}
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_ResumeErase(void)
{
  SchM_Enter_Fls_17_Dmu_ResumeErase();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_ResumeErase(void)
{
  SchM_Exit_Fls_17_Dmu_ResumeErase();
}
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_UserContentCount(void)
{
  SchM_Enter_Fls_17_Dmu_UserContentCount();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_UserContentCount(void)
{
  SchM_Exit_Fls_17_Dmu_UserContentCount();
}
void TSC_Fls_17_Dmu_SchM_Enter_Fls_17_Dmu_Write(void)
{
  SchM_Enter_Fls_17_Dmu_Write();
}
void TSC_Fls_17_Dmu_SchM_Exit_Fls_17_Dmu_Write(void)
{
  SchM_Exit_Fls_17_Dmu_Write();
}
