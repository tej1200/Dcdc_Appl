/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Icu_17_TimerIp.h
 *           Config:  DCDC_Appl.dpa
 *        SW-C Type:  Icu_17_TimerIp
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_CcuInterruptHandle();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_CcuInterruptHandle();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_CcuVariableupdate();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_CcuVariableupdate();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_EnableNotification();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_EnableNotification();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_EnableWakeup();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_EnableWakeup();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_GtmEnableEdgeCount();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_GtmEnableEdgeCount();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_GtmGetDutyCycle();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_GtmGetDutyCycle();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_ResetEdgeCount();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_ResetEdgeCount();
void TSC_Icu_17_TimerIp_SchM_Enter_Icu_17_TimerIp_SetActivationCondition();
void TSC_Icu_17_TimerIp_SchM_Exit_Icu_17_TimerIp_SetActivationCondition();
