/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Pwm_17_GtmCcu6.h
 *           Config:  DCDC_Appl.dpa
 *        SW-C Type:  Pwm_17_GtmCcu6
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
void TSC_Pwm_17_GtmCcu6_SchM_Enter_Pwm_17_GtmCcu6_HandleNotification();
void TSC_Pwm_17_GtmCcu6_SchM_Exit_Pwm_17_GtmCcu6_HandleNotification();
void TSC_Pwm_17_GtmCcu6_SchM_Enter_Pwm_17_GtmCcu6_PeriodAndDutyUpdate();
void TSC_Pwm_17_GtmCcu6_SchM_Exit_Pwm_17_GtmCcu6_PeriodAndDutyUpdate();
