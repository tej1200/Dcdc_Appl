/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Gpt.c
 *           Config:  DCDC_Appl.dpa
 *        SW-C Type:  Gpt
 *
 *        Generator:  MICROSAR RTE Generator Version 4.29.0
 *                    RTE Core Version 1.29.0
 *          License:  CBD2200333
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Gpt.h"
#include "TSC_SchM_Gpt.h"
void TSC_Gpt_SchM_Enter_Gpt_Get100UsPredefTimerValue(void)
{
  SchM_Enter_Gpt_Get100UsPredefTimerValue();
}
void TSC_Gpt_SchM_Exit_Gpt_Get100UsPredefTimerValue(void)
{
  SchM_Exit_Gpt_Get100UsPredefTimerValue();
}
void TSC_Gpt_SchM_Enter_Gpt_Get1UsPredefTimerValue(void)
{
  SchM_Enter_Gpt_Get1UsPredefTimerValue();
}
void TSC_Gpt_SchM_Exit_Gpt_Get1UsPredefTimerValue(void)
{
  SchM_Exit_Gpt_Get1UsPredefTimerValue();
}
void TSC_Gpt_SchM_Enter_Gpt_Gpt12StartTimer(void)
{
  SchM_Enter_Gpt_Gpt12StartTimer();
}
void TSC_Gpt_SchM_Exit_Gpt_Gpt12StartTimer(void)
{
  SchM_Exit_Gpt_Gpt12StartTimer();
}
void TSC_Gpt_SchM_Enter_Gpt_GtmStartTimer(void)
{
  SchM_Enter_Gpt_GtmStartTimer();
}
void TSC_Gpt_SchM_Exit_Gpt_GtmStartTimer(void)
{
  SchM_Exit_Gpt_GtmStartTimer();
}
