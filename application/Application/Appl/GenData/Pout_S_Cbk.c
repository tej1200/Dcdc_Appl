
/**
 * CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)
 *
 * This is an unpublished work of authorship, which contains trade secrets,
 * created in 2018. Hella KGaA Hueck & Co. owns all rights to this work and
 * intends to maintain it in confidence to preserve its trade secret status.
 * Hella KGaA Hueck & Co. reserves the right, under the copyright laws of
 * Germany or those of any other country that may have jurisdiction, to
 * protect this work as an unpublished work, in the event of an inadvertent or
 * deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves
 * its rights under all copyright laws to protect this work as a published
 * work, when appropriate. Those having access to this work may not copy it,
 * use it, modify it or disclose the information contained in it without the
 * written authorization of Hella KGaA Hueck & Co..
 */

/*******************************************************************************
 * History
 * created by: catabo1 Sat Oct 15 17:27:04 EEST 2022
 * generator version:  1.2.3.0
 *
*******************************************************************************************************/

/**
    Filename          :   Pout_S_Cbk.c
    Author            :   $Author: (catabo1)$
    Date              :   $Date: Sat Oct 15 17:27:04 EEST 2022$
    Version           :   $Revision: 1.2 $
    Status            :   $State: ACCEPTED $
    Company           :   HRO
 */

/*################################################################################################################### */
/*    Project     :   SafeIO                                                                                          */
/*    Module      :   Pout_S                                                                                          */
/*    Filename    :   $Source: CBK_C_file.java $                                                                      */
/*    Author      :   $Author: Spataru Simona Camelia spatsi1 (spatsi1) $                                               */
/*    Version     :   $Revision: 1.13 $                                                                                */
/*    Status      :   $State: IN_WORK $                                                                               */
/*    Company     :   HRO                                                                                             */
/*    Department  :   HRO-E-GSH12                                                                                    */
/*    Limitations :   NA                                                                                              */
/*    Description :   This file contains the callbacks of the Pout_S module.                                          */
/*################################################################################################################### */
/*    Changes     :                                                                                                   */
/*    $Log        :   Pout_S_Cbk.c $                                                                                  */
/*####################################################################################################################*/
/*  Module                                                                                                            */
/*  Release                                                                                                           */
/*  Version      Date          Author       ChangeID    Summary                                                       */
/*                                                                                                                    */
/*  1.0.0        13.08.2018    catabo1      853589      Pout_S New Development                                        */
/*  1.1.0        10.01.2019    mocaio1      871424      SafeIO 2: Pout_S : Update for variable frequency              */
/*                                                      and delete SafeIO_S_PeriodType from Pout_S_swc.arxml          */
/*  1.1.1        30.04.2019    mocaio1      830496      SafeIO 2: Qualify Pout_S in ASIL B quality                    */
/*  1.2.0        31.07.2019    catabo1      981771      SafeIO 2: Pout_S immediate write functionality follow-up      */
/*  1.2.1        20.12.2019    catabo1      1019198     SafeIO 2: Pout_S improve resources performance                */
/*  1.2.2        09.10.2020    spatsi1      1183295     Safe IO: Create Pout_S release for Infineon Aurix             */
/*  1.2.3        22.10.2021    spatsi1      1364797     Pout_S: PWM Output Handling support for variable period       */
/*                                                              functionality and update for AR 4.4                   */
/*####################################################################################################################*/

#include "Pout_S_GC.h"

#define POUT_S_START_SEC_CODE
#include "Pout_S_MemMap.h"

/* Dummy runnable */
FUNC(void, POUT_S_CODE) Pout_S_Dummy(void)
{
	/* User's code */
}


/* Wrapper used to read the duty cycle for the specific Pout_S channel */
SafeIO_S_InfoType Pout_S_ImmediateRead (uint8 ChannelId, SafeIO_S_DutyCycleIdType DutyCycle)
{
	SafeIO_S_InfoType RetVal = SAFEIO_S_E_NOK;

	switch (ChannelId)
	{
		case OUT_PWM_HSD1:
		{
			/* Read the duty cycle from IODP for the specific Pout_S channel */
			*DutyCycle = IODP_S1_Read_DutyCycleType(IODP_S1_OUT_PWM_HSD1_DUTYCYCLE_ID);
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD2:
		{
			/* Read the duty cycle from IODP for the specific Pout_S channel */
			*DutyCycle = IODP_S1_Read_DutyCycleType(IODP_S1_OUT_PWM_HSD2_DUTYCYCLE_ID);
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD3:
		{
			/* Read the duty cycle from IODP for the specific Pout_S channel */
			*DutyCycle = IODP_S1_Read_DutyCycleType(IODP_S1_OUT_PWM_HSD3_DUTYCYCLE_ID);
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD4:
		{
			/* Read the duty cycle from IODP for the specific Pout_S channel */
			*DutyCycle = IODP_S1_Read_DutyCycleType(IODP_S1_OUT_PWM_HSD4_DUTYCYCLE_ID);
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		default:
		{
			/* Do nothing */
		}
		break;
	}

	return RetVal;

}


/* Wrapper used to set the duty cycle for the specific Pout_S channel */
SafeIO_S_InfoType Pout_S_ImmediateWrite (uint8 ChannelId, SafeIO_S_DutyCycleType DutyCycle)
{
	SafeIO_S_InfoType RetVal = SAFEIO_S_E_NOK;

	switch (ChannelId)
	{
		case OUT_PWM_HSD1:
		{
			/* Set the duty cycle for the specific Pout_S channel */
			IODP_S1_Write_DutyCycleType(IODP_S1_OUT_PWM_HSD1_DUTYCYCLE_ID, DutyCycle);
			/* Call immediate write functionality for the specific Pout_S channel */
			Pout_S_ImmediateWrite_OUT_PWM_HSD1();
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD2:
		{
			/* Set the duty cycle for the specific Pout_S channel */
			IODP_S1_Write_DutyCycleType(IODP_S1_OUT_PWM_HSD2_DUTYCYCLE_ID, DutyCycle);
			/* Call immediate write functionality for the specific Pout_S channel */
			Pout_S_ImmediateWrite_OUT_PWM_HSD2();
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD3:
		{
			/* Set the duty cycle for the specific Pout_S channel */
			IODP_S1_Write_DutyCycleType(IODP_S1_OUT_PWM_HSD3_DUTYCYCLE_ID, DutyCycle);
			/* Call immediate write functionality for the specific Pout_S channel */
			Pout_S_ImmediateWrite_OUT_PWM_HSD3();
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;

		case OUT_PWM_HSD4:
		{
			/* Set the duty cycle for the specific Pout_S channel */
			IODP_S1_Write_DutyCycleType(IODP_S1_OUT_PWM_HSD4_DUTYCYCLE_ID, DutyCycle);
			/* Call immediate write functionality for the specific Pout_S channel */
			Pout_S_ImmediateWrite_OUT_PWM_HSD4();
			/* Execution is OK */
			RetVal = SAFEIO_S_E_OK;
		}
		break;


		default:
		{
			/* Do nothing */
		}
		break;
	}

	return RetVal;
}

#define POUT_S_STOP_SEC_CODE
#include "Pout_S_MemMap.h"
