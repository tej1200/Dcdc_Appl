/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: WdgIf
 *           Program: MSR_Daimler_SLP11
 *          Customer: Mercedes-Benz AG
 *       Expiry Date: Not restricted
 *  Ordered Derivat.:  TC399
 *    License Scope : The usage is restricted to CBD2200333_D02
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: WdgIf_MemMap.h
 *   Generation Time: 2023-03-01 01:20:40
 *           Project: PP_DemoECU - Version 1.0
 *          Delivery: CBD2200333_D02
 *      Tool Version: DaVinci Configurator Classic 5.25.37 SP2
 *
 *
 *********************************************************************************************************************/


/********************************************************************************************************************** 
 *  Memory sections for the state combiner master 
 *********************************************************************************************************************/ 
#ifdef WDGIF_START_SEC_VAR_INIT_8BIT 
# undef WDGIF_START_SEC_VAR_INIT_8BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_INIT_8BIT 
#endif /* ifdef WDGIF_START_SEC_VAR_INIT_8BIT */ 
 
#ifdef WDGIF_STOP_SEC_VAR_INIT_8BIT 
# undef WDGIF_STOP_SEC_VAR_INIT_8BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_INIT_8BIT 
#endif /* ifdef WDGIF_STOP_SEC_VAR_INIT_8BIT */ 
 
 
#ifdef WDGIF_START_SEC_VAR_INIT_16BIT 
# undef WDGIF_START_SEC_VAR_INIT_16BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_INIT_16BIT 
#endif /* ifdef WDGIF_START_SEC_VAR_INIT_16BIT */ 
 
#ifdef WDGIF_STOP_SEC_VAR_INIT_16BIT 
# undef WDGIF_STOP_SEC_VAR_INIT_16BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_INIT_16BIT 
#endif /* ifdef WDGIF_STOP_SEC_VAR_INIT_16BIT */ 
 
 
#ifdef WDGIF_START_SEC_VAR_INIT_32BIT 
# undef WDGIF_START_SEC_VAR_INIT_32BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_START_SEC_OsApplication_Core0_ASIL_VAR_INIT_32BIT 
#endif /* ifdef WDGIF_START_SEC_VAR_INIT_32BIT */ 
 
#ifdef WDGIF_STOP_SEC_VAR_INIT_32BIT 
# undef WDGIF_STOP_SEC_VAR_INIT_32BIT /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_STOP_SEC_OsApplication_Core0_ASIL_VAR_INIT_32BIT 
#endif /* ifdef WDGIF_STOP_SEC_VAR_INIT_32BIT */ 
 
 
/********************************************************************************************************************** 
 *  Memory sections for global shared data section, read and writen by all modules 
 *********************************************************************************************************************/ 
#ifdef WDGIF_GLOBAL_SHARED_START_SEC_VAR_UNSPECIFIED 
# undef WDGIF_GLOBAL_SHARED_START_SEC_VAR_UNSPECIFIED /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_START_SEC_GLOBALSHARED_VAR_NOCACHE_UNSPECIFIED /* Variables which are accessed by multiple cores must not be cached. */ 
#endif /* ifdef WDGIF_GLOBAL_SHARED_START_SEC_VAR_UNSPECIFIED */ 
#ifdef WDGIF_GLOBAL_SHARED_STOP_SEC_VAR_UNSPECIFIED 
# undef WDGIF_GLOBAL_SHARED_STOP_SEC_VAR_UNSPECIFIED /* PRQA S 0841 */ /* MD_MSR_19.6 */ 
# define OS_STOP_SEC_GLOBALSHARED_VAR_NOCACHE_UNSPECIFIED /* Variables which are accessed by multiple cores must not be cached. */ 
#endif /* ifdef WDGIF_GLOBAL_SHARED_STOP_SEC_VAR_UNSPECIFIED */ 


/**********************************************************************************************************************
 *  END OF FILE: WdgIf_MemMap.h
 *********************************************************************************************************************/
