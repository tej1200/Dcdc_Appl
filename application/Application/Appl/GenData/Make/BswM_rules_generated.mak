######################################################################################################################
# #  COPYRIGHT
# #  -------------------------------------------------------------------------------------------------------------------
# #  \verbatim
# #
# #                 This software is copyright protected and proprietary to Vector Informatik GmbH.
# #                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
# #                 All other rights remain with Vector Informatik GmbH.
# #  \endverbatim
# #  -------------------------------------------------------------------------------------------------------------------
# #  LICENSE
# #  -------------------------------------------------------------------------------------------------------------------
# #            Module: BswM
# #           Program: MSR_Daimler_SLP11
# #          Customer: Mercedes-Benz AG
# #       Expiry Date: Not restricted
# #  Ordered Derivat.:  TC399
# #    License Scope : The usage is restricted to CBD2200333_D02
# #
# #  -------------------------------------------------------------------------------------------------------------------
# #  FILE DESCRIPTION
# #  -------------------------------------------------------------------------------------------------------------------
# #              File: BswM_rules_generated.mak
# #   Generation Time: 2023-10-24 17:49:11
# #           Project: DCDC_Appl - Version 1.0
# #          Delivery: CBD2200333_D02
# #      Tool Version: DaVinci Configurator Classic 5.25.37 SP2
# #
# #       Description: This makefile is a template.  
# #                    It conatains the generated part of the <MSM>_rules.mak file. 
# #                    This file has to be included by the static <MSN>_rules.mak file.
# #
# #####################################################################################################################
#
#
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\BswM_Lcfg.c

