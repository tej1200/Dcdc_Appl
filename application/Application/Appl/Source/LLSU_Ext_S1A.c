/*******************************************************************************/
/* CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)                                  */
/*                                                                             */
/* This is an unpublished work of authorship, which contains trade secrets,    */
/* created in 2009. Hella KGaA Hueck & Co. owns all rights to this work and    */
/* intends to maintain it in confidence to preserve its trade secret status.   */
/* Hella KGaA Hueck & Co. reserves the right, under the copyright laws of      */
/* Germany or those of any other country that may have jurisdiction, to        */
/* protect this work as an unpublished work, in the event of an inadvertent or */
/* deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves   */
/* its rights under all copyright laws to protect this work as a published     */
/* work, when appropriate. Those having access to this work may not copy it,   */
/* use it, modify it or disclose the information contained in it without the   */
/* written authorization of Hella KGaA Hueck & Co..                            */
/*                                                                             */
/*******************************************************************************/

/***********************************************************************************************/
/*  Filename    :   $Source: LLSU_Ext_S1A.c $                                               *//* PRQA S 0292 # Hella naming convetion for file names */
/*  Author      :   $Author: Kumar Anil kumaan8 (kumaan8) $                              */
/*  Date        :   $Date: 2021/02/15 15:19:39IST $                                           */
/*  Version     :   $Revision: 1.4 $                                                       */
/*  Status      :   $State: IN_WORK $                                                          */
/*  Description :   This file contains definitions of UCB's and LLSU-callout '_call_init()'    */
/***********************************************************************************************/
/*  Changes     :                                                                              */
/*  $Log        :   LLSU_Ext_S1A.c  $                                                          */
/***********************************************************************************************/
/*                                                                                             */
/*  Module                                                                                     */
/*  Release                                                                                    */
/*  Version     Date          Author    ChangeID       Summary                                 */
/*                                                                                             */
/*  1.0      04.02.2021      schupe6    1205742    LLSU integration code for LE-Porsche        */
/*                                                                                             */
/***********************************************************************************************/

#include "Std_Types.h"
#include "LLSU_Ext_S1A_Cfg.h"
#include "LLSU_Ext_S1A_Lib.h"
//#include "BLEM_S1_RSTM.h" /* PRQA S 0380 # TASKING compiler supports more than 4095 macro definitions */

#include "IfxScu_Reg.h"
#include "IfxCpu_Reg.h"
#include "IfxMtu_reg.h"
#include "IfxGtm_reg.h"
#include "IfxCan_reg.h"
#include "IfxPsi5_reg.h"
#include "IfxGeth_reg.h"
#include "IfxIom_reg.h"
//#include "McuExt_S2B.h"
#include "IfxPms_reg.h"
//#include "BLEM_S1_SI.h"




/* PRQA S 4700 1 # (STAV1 = 0, STST3 = 0) The function is kept empty for future extensions. */
void __prof_init(void) /* PRQA S 0602 # Intended implementation, system entry point - enabling user function code */
{
	/* nothing to do */
}
/* PRQA S 4700 1 # (STAV1 = 10, STPTH = 512 ) Complexity is needed for the given functionality. */

/*
 * The function '_call_init()' will be called from cstart.c if "__CALL_INIT" is defined
 *
 */
void _call_init(void) /* PRQA S 0602 #Intended implementation */
{


	/* clearing an initializing the different RAM-Areas */
	_c_init_entry(LLSU_Ext_S1A_RamInitTable_Core0); /* PRQA S 0431 # Intended implementation, used to initialize the memory according to the given table */

}
/* PRQA S 4700 1 # ( STAV1 = 10 ) The high size of statements in function does not influence code execution */

