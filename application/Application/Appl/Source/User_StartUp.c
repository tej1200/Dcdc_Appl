/**************************************************************************
**                                                                        *
**  FILE        :  User_StartUp.c                                                *
**                                                                        *
**  DESCRIPTION :                                                         *
**      Donot modify this file untill there was a change in the start     *
**      Address of the application.                                       *
**      Without Knowledege on the BMHD this will Brick the ECU            *
**                                                                        *
**************************************************************************/
#include "Platform_Types.h" 
/* Boot Mode Headers - use boot from internal flash              */
/* At least 1 of these headers has to be present on the hardware */
/* TODO: Check if this configuration suits your setup.           */


/*+++++++++++++++++++++++++++++++++++++Attention!++++++++++++++++++++++++++++++++++++++++++++++++++++++
++ If you have to adjust the bmiField you have to change the bmi header value and CRC calculation.    +
++ Otherwise you could be locked out of your device! Read the manual for more information.            +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#define UCBBM_HEADER_START_SEC_CONST
#include "User_MemMap.h" 

/*@@@@@@@@@@@ As of now used BMHD0 for Application Standalone Software @@@@@@@@@@@*/
/*@@@@@@@@@@@ Without the correct configuration it will lock the ECU@@@@@@@@@@@*/

volatile const uint32 bmiField0[128] =
{
  0xB359000Eul, 0x800A0100ul, 0xCDF31E84ul, 0x320CE17Bul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x43211234ul, 0x00000000ul, 0x00000000ul, 0x00000000ul  /* UCB CONFIRMATION - 32-bit CODE = UNLOCKED */
};

#define UCBBM_HEADER_STOP_SEC_CONST
#include "User_MemMap.h"  
/*
#define UCBBM_HEADER1_START_SEC_CONST
#include "User_MemMap.h" 

volatile const uint32 bmiField1[128] =
{
  0xB359000Eul, 0xA0000100ul, 0x6056346Cul, 0x9FA9CB93ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x00000000ul, 0x00000000ul, 0x00000000ul, 0x00000000ul,
  0x43211234ul, 0x00000000ul, 0x00000000ul, 0x00000000ul  
};

#define UCBBM_HEADER1_STOP_SEC_CONST
#include "User_MemMap.h" /* UCB CONFIRMATION - 32-bit CODE = UNLOCKED */



