/***************************************************************************** 
 *  COPYRIGHT 
 *  ---------------------------------------------------------------------------- 
 *  \verbatim 
 * This software is copyright protected and proprietary to Netwalk GmbH. 
 * Netwalk GmbH grants to you only those rights as set out in the license
 *	All other rights remain with Netwalk GmbH. 
 *  \endverbatim 
 *  --------------------------------------------------------------------------- 
 *  LICENSE 
 *  ---------------------------------------------------------------------------- 
 *  Module: - 
 *  Program: -
 *  Customer: Intenal 
 *  Expiry Date: 11.09.2023 
 *  Ordered Derivat.: - 
 *  License Scope: - 
 *  --------------------------------------------------------------------------- 
 *  FILE DESCRIPTION 
 *  ---------------------------------------------------------------------------- 
 *              File: SWC_MaxValEngDir.c 
 *            Author: Revanth Kumar B 
 *       Description: Max values and Energy direction  
 *              Date: 11.09.2023 
 *           Project: DCDC 
 *        Additional: - 
 *           Company: Netwalk GmbH. 
 ******************************************************************************
 *
 *	Code Template Snippet 1: Start of File Tag containing Basic Information
 *
 *	End of File Tag 
 ******************************************************************************
 *  END OF FILE: SWC_MaxValEngDir.c
 *****************************************************************************
 */


#include "SWC_MaxValEngDir.h"

volatile uint16	Rte_read_tempinfo,int_stateinfo;
volatile Diag_RoutineRequest_t Rte_Routine_request_data;
volatile uint16	Rte_Cansignal_NW_DCDC_U12VoltReq_Rx;
volatile uint16   Rte_Cansignal_NW_DCDC_OperatingMode_Rx;
volatile uint8	Rte_Cansignal_NW_DCDC_U12CurrReq_Rx,System_state,System_error_info,int_EnergydirectionInfo;
volatile uint8	Rte_DerateFlag_u8;

Int_RequestValuesInfo_t l_Int_RequestValuesInfo = {0U,0U,0U,0U};
Int_RequestValuesInfo_t feedback_RequestValuesInfo = {0U,0U,0U,0U};

uint8 Dcdc_TemperatureDegeneration(Int_RequestValuesInfo_t *l_fa_Int_RequestValuesInfo)
{
	static uint8	l_rtval_u8 = FALSE;
	static uint8 	l_Int_Temp_Info_u8;
	static uint32	l_calcpwr_u32,l_calcpwrderate_u32,l_currentload_u32;
	
	l_Int_Temp_Info_u8 = Rte_read_tempinfo;
	
	if (l_Int_Temp_Info_u8 > C_TMPLIMTDERATE_TMP)
	{
		if (l_Int_Temp_Info_u8 <= C_TMPLIMTSHUTDOWN_TMP)
		{
			l_calcpwr_u32 = (l_fa_Int_RequestValuesInfo->voltage_u16 * l_fa_Int_RequestValuesInfo->current_u8);
			l_calcpwrderate_u32 = ((l_calcpwr_u32 * (uint16)C_TEMPDERATE_SLOPE) + (uint16)C_TEMPDERATE_INTRCPT);
			
			l_currentload_u32 = (l_fa_Int_RequestValuesInfo->voltage_u16 / l_fa_Int_RequestValuesInfo->current_u8);
			
			l_fa_Int_RequestValuesInfo->current_u8 = (l_fa_Int_RequestValuesInfo->current_u8 * (l_calcpwrderate_u32/l_currentload_u32));
			
			l_fa_Int_RequestValuesInfo->voltage_u16 = (l_fa_Int_RequestValuesInfo->current_u8 * l_currentload_u32);
			l_rtval_u8 = TRUE;
		}
		else
		{
			l_fa_Int_RequestValuesInfo->control_b = FALSE;
			l_fa_Int_RequestValuesInfo->voltage_u16 = C_STDBYMODE_VOLT;
			l_fa_Int_RequestValuesInfo->current_u8 = C_STDBYMODE_CUR;
			l_fa_Int_RequestValuesInfo->mode_u8 = C_STDBYMODE_ST;
		}
	}
	else
	{
		/* do nothing */
	}
	return l_rtval_u8;
}


void Dcdc_MaxRequestVal_CanSignalProcessing(Int_RequestValuesInfo_t *l_fa_Int_RequestValuesInfo)
{
	static Diag_RoutineRequest_t l_reqRoutine;
	static uint8 l_CurrReq_u8,l_mode_u8;
	static uint16	l_VoltReq_u16;
	l_reqRoutine = Rte_Routine_request_data;
	
	if (l_reqRoutine.RoutineRequest_u8 == C_INPROGRESS_ST)
	{
		l_VoltReq_u16 = l_reqRoutine.voltage_u16;
		l_CurrReq_u8 = l_reqRoutine.current_u8;
		l_mode_u8 = C_DIAGMODE_ST;
	}
	else if(Rte_Cansignal_NW_DCDC_OperatingMode_Rx == 1U  )
	{
		l_VoltReq_u16 = Rte_Cansignal_NW_DCDC_U12VoltReq_Rx;
		l_CurrReq_u8 = Rte_Cansignal_NW_DCDC_U12CurrReq_Rx;
		l_mode_u8 = C_COMMANDMODE_ST;
	}
	else
	{
		l_VoltReq_u16 = 0u;
		l_CurrReq_u8 = 0u;
		l_mode_u8 = C_STDBYMODE_ST;
	}
	
	if ((l_VoltReq_u16 > C_BN12SWOFFLOW_VOLT) && (l_VoltReq_u16 < C_BN12SWOFFHIGH_VOLT) )
	{
		if ((uint32)(l_VoltReq_u16 * l_CurrReq_u8) <= (uint32)C_MAXPWRLMT_PWR )
		{
			l_fa_Int_RequestValuesInfo->voltage_u16 = l_VoltReq_u16;
			l_fa_Int_RequestValuesInfo->current_u8 = l_CurrReq_u8;
			l_fa_Int_RequestValuesInfo->mode_u8 = l_mode_u8;
			
		}
		else
		{
			l_fa_Int_RequestValuesInfo->voltage_u16 = C_STDBYMODE_VOLT;
			l_fa_Int_RequestValuesInfo->current_u8 = C_STDBYMODE_CUR;
			l_fa_Int_RequestValuesInfo->mode_u8 = C_STDBYMODE_ST;			
		}
	}
	else {
			l_fa_Int_RequestValuesInfo->voltage_u16 = C_STDBYMODE_VOLT;
			l_fa_Int_RequestValuesInfo->current_u8 = C_STDBYMODE_CUR;
			l_fa_Int_RequestValuesInfo->mode_u8 = C_STDBYMODE_ST;		
	}
}


Int_RequestValuesInfo_t Dcdc_MaxRequestVal_calc()
{
	uint16	l_voltage_u16;
	uint8	l_current_u8;
	static uint8	l_NW_DCDC_MvLimnIndcn_b;
	
	(void)Dcdc_MaxRequestVal_CanSignalProcessing(&l_Int_RequestValuesInfo);
	l_voltage_u16 = l_Int_RequestValuesInfo.voltage_u16;
	l_current_u8 = l_Int_RequestValuesInfo.current_u8;
	
	
	if ((l_voltage_u16 >= C_BN12SWOFFLOW_VOLT) && (l_voltage_u16 < C_BN12DERATELOW2_VOLT))
	{
		l_Int_RequestValuesInfo.control_b = TRUE;
		l_Int_RequestValuesInfo.voltage_u16 = l_voltage_u16;
		l_Int_RequestValuesInfo.current_u8 = ((l_Int_RequestValuesInfo.current_u8 * C_BN12DERATE1LOW_SLOPE) + (C_BN12DERATE1LOW_INTRCPT));
		l_NW_DCDC_MvLimnIndcn_b = 1;	
	}
	else if ((l_voltage_u16 >= C_BN12DERATELOW2_VOLT) && (l_voltage_u16 < C_BN12MAXPWRSTRT_VOLT))
	{
		l_Int_RequestValuesInfo.control_b = TRUE;
		l_Int_RequestValuesInfo.voltage_u16 = l_voltage_u16;
		l_Int_RequestValuesInfo.current_u8 = ((l_Int_RequestValuesInfo.current_u8 * C_BN12DERATE2LOW_SLOPE) + (C_BN12DERATE2LOW_INTRCPT));
		l_NW_DCDC_MvLimnIndcn_b = 1;	
	}
	else if ((l_voltage_u16 >= C_BN12MAXPWRSTRT_VOLT) && (l_voltage_u16 < C_BN12MAXPWREND_VOLT))
	{
		l_Int_RequestValuesInfo.control_b = TRUE;
		l_Int_RequestValuesInfo.voltage_u16 = l_voltage_u16;
		l_NW_DCDC_MvLimnIndcn_b = 0;		
		if (l_current_u8 <= C_BN12MAXALWDLMT_CUR)
		{
			l_Int_RequestValuesInfo.current_u8 = l_current_u8;
		}
		else
		{
			l_Int_RequestValuesInfo.current_u8 = C_BN12MAXALWDLMT_CUR;
		}
	}
	else if ((l_voltage_u16 >= C_BN12MAXPWREND_VOLT) && (l_voltage_u16 < C_BN12DERATEHIGH1_VOLT))
	{
		l_Int_RequestValuesInfo.control_b = TRUE;
		l_Int_RequestValuesInfo.voltage_u16 = l_voltage_u16;
		l_Int_RequestValuesInfo.current_u8 = ((l_Int_RequestValuesInfo.current_u8 * C_BN12DERATE1HIGH_SLOPE) + (C_BN12DERATE1HIGH_INTRCPT));
		l_NW_DCDC_MvLimnIndcn_b = 1;	
	}
	else if ((l_voltage_u16 >= C_BN12DERATEHIGH1_VOLT) && (l_voltage_u16 <= C_BN12SWOFFHIGH_VOLT))
	{
		l_Int_RequestValuesInfo.control_b = TRUE;
		l_Int_RequestValuesInfo.voltage_u16 = l_voltage_u16;
		l_Int_RequestValuesInfo.current_u8 = ((l_Int_RequestValuesInfo.current_u8 * C_BN12DERATE2HIGH_SLOPE) + (C_BN12DERATE2HIGH_INTRCPT));
		l_NW_DCDC_MvLimnIndcn_b = 1;	
	}	
	else
	{
		l_Int_RequestValuesInfo.control_b = FALSE;
		l_Int_RequestValuesInfo.voltage_u16 = 0u;
		l_Int_RequestValuesInfo.current_u8 = 0u;
		l_Int_RequestValuesInfo.mode_u8 = C_STDBYMODE_ST;
	}
	
	
	if (l_Int_RequestValuesInfo.control_b == TRUE )
	{
		l_NW_DCDC_MvLimnIndcn_b = Dcdc_TemperatureDegeneration(&l_Int_RequestValuesInfo);
	}
	else
	{
		l_Int_RequestValuesInfo.mode_u8 = C_STDBYMODE_ST;
	}
	Rte_DerateFlag_u8 = l_NW_DCDC_MvLimnIndcn_b;
	return l_Int_RequestValuesInfo;
}



void func_STANDBY(){

    //states should be diag, standby
    l_Int_RequestValuesInfo.mode_u8 = NO_DIRECTION;
    int_stateinfo = C_STDBYMODE_ST;

	l_Int_RequestValuesInfo.current_u8 = C_STDBYMODE_VOLT;
	l_Int_RequestValuesInfo.voltage_u16 = C_STDBYMODE_CUR;
    //update to port

}


void func_BUCK(){
    //states should be diag, command
	Int_RequestValuesInfo_t struct_reqvalues = Dcdc_MaxRequestVal_calc();
	 if((struct_reqvalues.voltage_u16 <= par_U_ERROR_RESET_BUCK_UN12_UV_CFG) || (struct_reqvalues.voltage_u16 >= par_U_ERROR_RESET_BUCK_UN12_OV_CFG)){
          func_STANDBY();
    }
	else{
     l_Int_RequestValuesInfo.mode_u8 = BUCK;
     int_stateinfo = BUCK;

	/* Feedback to be checked in the Diag Protection*/

	 //feedback_RequestValuesInfo = readfeedback();
	 //Read and check the feedback and compare with uv and ov

	/*  if((feedback_RequestValuesInfo.voltage_u16 <= par_U_ERROR_RESET_BUCK_UN12_UV_CFG) || (feedback_RequestValuesInfo.voltage_u16 >= par_U_ERROR_RESET_BUCK_UN12_OV_CFG)){
          func_STANDBY();
    	}*/
    //update to port

	}

}


void func_STATE_CONTROL(){

    Int_RequestValuesInfo_t struct_reqvalues = Dcdc_MaxRequestVal_calc();

    if (struct_reqvalues.mode_u8 == C_DIAGMODE_ST ||struct_reqvalues.mode_u8 == C_COMMANDMODE_ST){
        func_BUCK();
    } 

    else if(struct_reqvalues.mode_u8 == C_STDBYMODE_ST ){
        func_STANDBY();
    }
}


void SysSysFunc_ENERGY_DIRECTION(){

    if ((System_state == RUN) && (System_error_info == NO_ERROR) && (par_F_BUCK_MODE_DEACTIVATION_CFG == FALSE )){
        func_STATE_CONTROL();
    }
	
    else if(System_error_info == ERROR || par_F_BUCK_MODE_DEACTIVATION_CFG == TRUE){
        func_STANDBY();
    }
}

Int_RequestValuesInfo_t readfeedback(){

	//Int_RequestValuesInfo_t feedback_RequestValuesInfo = {0U,0U,0U,0U};

	//Rte_Call_IoHWAbs_MaxValEngDir_AdcConv_Volt_Info(feedback_RequestValuesInfo.voltage_u16);
	//Rte_Call_IoHWAbs_MaxValEngDir_AdcConv_Current_Info(feedback_RequestValuesInfo.current_u8);

	return feedback_RequestValuesInfo;

}

