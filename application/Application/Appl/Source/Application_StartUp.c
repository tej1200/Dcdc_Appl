/**************************************************************************
**                                                                        *
**  FILE        :  Application_StartUp.c                                         *
**                                                                        *
**     DESCRIPTION :                                                      *
**       modify this file Based on the user start up                      *
**************************************************************************/

/*+++++++++++++++++++++++++++++++++++++Attention!++++++++++++++++++++++++++++++++++++++++++++++++++++++
++                                          HeaderFile Inclusion                                     ++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#include "EcuM.h"
#include "Os.h" 
//#include "Integration_Callout.h"
//#include "Gpt.h"
//#include "Spi.h"
//#include "Pwm_17_GtmCcu6.h"
//#include "Pout_S_GC.h"
//#include "Dout_S_GC.h"
//#include "IODP_S1.h"


int main ()
{

Os_InitMemory();

Os_Init();
	
EcuM_Init();

while(1);

return 0;
}

TASK(C0_INIT_BSW_TASK) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
    EcuM_StartupTwo();
  (void)TerminateTask();
}


TASK(IdleTask_OsCore0) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
  for(;;)
  {
    (void)Schedule();
  }
}
#if 1
TASK(OsTask_Core0_Appl) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
  (void)TerminateTask();
}
#endif

