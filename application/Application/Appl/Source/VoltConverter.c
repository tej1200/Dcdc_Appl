/**************************************************************************
**                                                                        *
**            FILE        : VoltConverter.c
**            Author      : Malik Vindhani
**            Description : To transfer the energy as per direction and
**                          requsted value.
**            Date        : 11.09.2023
**            Project     : DCDC
**            Additional  : NA
**            Company     : Netwalk GmbH                                  *
**                                                                        *
**  DESCRIPTION :                                                         *
**      modify this file if there was a change Algorithm to trasnfer the  *
**      energy.                                                           *
**                                                                        *
**************************************************************************/
/***************************   Header Inclusions  ******************************/

#include "VoltConverter.h"

/***************************   Header Inclusions   ******************************/

/***************************   Function Banner   ******************************
* Function Name     :	
* Description       :	
* Parameters        :	None
* Inputs			:	None
* Outputs 			:	None
* In/Out 			:	None
* Return values     :	None
* Exceptions	    :	None
*******************************************************************************/

void VoltConverter()
{
    uint8 l_MaxVal_u8;
    uint8 l_EngDir_u8;

    l_MaxVal_u8 = Rte_RP_MaxValEngDir_VoltConverter_Max_Value_MaxVal(); /* Reading the Max Value Sent by MaxValEngDir SWC */
    l_EngDir_u8 = Rte_RP_MaxValEngDir_VoltConverter_Eng_Dir_EngDir(); /* Reading the Energy Direction Sent by MaxValEngDir SWC */

    if((TRUE == l_MaxVal_u8) && (BUCK == l_EngDir_u8))
    {
        EnergyTransfer();
    }
    else
    {
        NoEnergyTransfer();
    }
}

static void EnergyTransfer()
{
    uint16 l_Volt_Info_u16;
    uint16 l_SwitchingElement_u16;
    uint16 l_remainder_u16;
    uint8 offset_u8;

    l_Volt_Info_u16 = Rte_Call_IoHWAbs_VoltConverter_AdcConv_Volt_Info();  /* Reading the Voltage information Sent by IoHWAbs */

    /*Algo Calculation to be done once Algo is finalized */
    l_remainder_u16 = ((l_Volt_Info_u16 * DUTY_CYCLE)%MAX_VOLTAGE);
    if( l_remainder_u16 >= 24u )
    {
        offset_u8 = 1u;
    }
    else
    {
        offset_u8 = 0u;
    }

    l_SwitchingElement_u16 = (uint16)(((l_Volt_Info_u16 * DUTY_CYCLE)/MAX_VOLTAGE)+ offset_u8);
    

    (void)Rte_Call_IoHWAbs_VoltConverter_PWMDuty_SwitchingElement(l_SwitchingElement_u16 );  /* Writing the PWM Duty Cycle information Sent by IoHWAbs */
}

static void NoEnergyTransfer()
{
    /*Need to Pass the duty cycle as 0 */

    (void)Rte_Call_IoHWAbs_VoltConverter_PWMDuty_SwitchingElement(NO_ENERGY_TRANSFER);  /* Writing the PWM Duty Cycle information Sent by IoHWAbs */
}