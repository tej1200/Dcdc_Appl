/* Following labels were manually added in the linker because they are used in the startup*/

section_layout mpe:vtc:linear
{
	/* Following labels were manually added in the linker because they are used in the startup*/
	
	
	"_DLMU0_VAR_BSS_START" = "_VarGroup_DLMU0_NonCached_BSS_ALL_START";
	"_DLMU0_VAR_BSS_SIZE" = "_VarGroup_DLMU0_NonCached_BSS_ALL_END" - "_VarGroup_DLMU0_NonCached_BSS_ALL_START"; 
	
	"_DLMU0_VAR_DATA_START" = "_VarGroup_DLMU0_NonCached_DATA_ALL_START";
	"_DLMU0_VAR_DATA_SIZE" = "_VarGroup_DLMU0_NonCached_DATA_ALL_END" - "_VarGroup_DLMU0_NonCached_DATA_ALL_START";
	
	"_DLMU0_VAR_ROM_START" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_START";
	"_DLMU0_VAR_ROM_SIZE" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_LIMIT" - "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_START";
	
	"_DLMU0_VAR_INIT_START" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_START";
	"_DLMU0_VAR_INIT_SIZE" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_LIMIT" - "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_START";

	"_DSPR0_VAR_BSS_START" = "_VarSectionGroup_0_DSPR0_VAR_BSS_START";	

	"_DSPR0_VAR_BSS_SIZE" = "_VarSectionGroup_0_DSPR0_VAR_BSS_LIMIT" - "_VarSectionGroup_0_DSPR0_VAR_BSS_START";

	"_DSPR0_VAR_START" = "_VarSectionGroup_0_DSPR0_VAR_START";

	"_DSPR0_VAR_SIZE" = "_VarSectionGroup_0_DSPR0_VAR_LIMIT" - "_VarSectionGroup_0_DSPR0_VAR_START";

	"_DSPR0_VAR_ROM_START" = "_VarSectionGroup_0_DSPR0_VAR_ROM_START";

	"_DSPR0_VAR_ROM_SIZE" = "_VarSectionGroup_0_DSPR0_VAR_ROM_LIMIT" - "_VarSectionGroup_0_DSPR0_VAR_ROM_START";

	"_PSPR0_VAR_BSS_START" = "_VarSectionGroup_0_PSPR0_VAR_BSS_START";

	"_PSPR0_VAR_BSS_SIZE" = "_VarSectionGroup_0_PSPR0_VAR_BSS_LIMIT" - "_VarSectionGroup_0_PSPR0_VAR_BSS_START";

	"_PSPR0_VAR_START" = "_VarSectionGroup_0_PSPR0_VAR_START";

	"_PSPR0_VAR_SIZE" = "_VarSectionGroup_0_PSPR0_VAR_LIMIT" - "_VarSectionGroup_0_PSPR0_VAR_START";

	"_PSPR0_VAR_ROM_START" = "_VarSectionGroup_0_PSPR0_VAR_ROM_START";

	"_PSPR0_VAR_ROM_SIZE" = "_VarSectionGroup_0_PSPR0_VAR_ROM_LIMIT" - "_VarSectionGroup_0_PSPR0_VAR_ROM_START";

	"_OS_STACKS_CORE0_VAR_NOINIT_SIZE" = "_OS_STACKS_CORE0_VAR_NOINIT_LIMIT" - "_OS_STACKS_CORE0_VAR_NOINIT_START";

	"_OS_OsApplication_Core0_ASIL_VAR_INIT_SIZE" = "_OS_OsApplication_Core0_ASIL_VAR_INIT_LIMIT" - "_OS_OsApplication_Core0_ASIL_VAR_INIT_START";

	"_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_SIZE" = "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_LIMIT" - "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_START";

	"_OS_OsApplication_Core0_QM_VAR_INIT_SIZE" = "_OS_OsApplication_Core0_QM_VAR_INIT_LIMIT" - "_OS_OsApplication_Core0_QM_VAR_INIT_START";

	"_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_SIZE" = "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_LIMIT" -"_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_START";

	"_OS_DATA_CORE0_VAR_INIT_SIZE" = "_OS_DATA_CORE0_VAR_INIT_LIMIT" - "_OS_DATA_CORE0_VAR_INIT_START";
		
	"_OS_DATA_CORE0_VAR_ZERO_INIT_SIZE" = "_OS_DATA_CORE0_VAR_ZERO_INIT_LIMIT" - "_OS_DATA_CORE0_VAR_ZERO_INIT_START";

	"_OS_DATA_SHARED_VAR_INIT_SIZE" = "_OS_DATA_SHARED_VAR_INIT_LIMIT" - "_OS_DATA_SHARED_VAR_INIT_START";
	"_OS_GLOBALSHARED_VAR_INIT_SIZE" = "_OS_GLOBALSHARED_VAR_INIT_LIMIT" - "_OS_GLOBALSHARED_VAR_INIT_START";
	"_OS_GLOBALSHARED_VAR_ZERO_INIT_SIZE" = "_OS_GLOBALSHARED_VAR_ZERO_INIT_LIMIT" - "_OS_GLOBALSHARED_VAR_ZERO_INIT_START";

	"_data_START" = "_VarSectionGroup_0_data_START";
	"_zdata_START" = "_VarSectionGroup_1_zdata_START";
	"_bss_START" = "_VarSectionGroup_2_bss_START";
	"_zbss_START" = "_VarSectionGroup_3_zbss_START";

	"_data_ROM_START" = "_VarSectionGroup_0_data_ROM_START";
	"_zdata_ROM_START" = "_VarSectionGroup_1_zdata_ROM_START";

	"_data_SIZE" = "_VarSectionGroup_0_data_LIMIT"- "_VarSectionGroup_0_data_START";
	"_zdata_SIZE" = "_VarSectionGroup_1_zdata_LIMIT" - "_VarSectionGroup_1_zdata_START";
	"_bss_SIZE" = "_VarSectionGroup_2_bss_LIMIT" - "_VarSectionGroup_2_bss_START";
	"_zbss_SIZE" = "_VarSectionGroup_3_zbss_LIMIT" - "_VarSectionGroup_3_zbss_START";
	
	"_lc_u_int_tab_tc0" = "_OS_INTVEC_CORE0_CODE_START";	
	"_lc_u_int_tab" = "_lc_u_int_tab_tc0";

	"_lc_u_trap_tab_tc0" = "_OS_EXCVEC_CORE0_CODE_START";
	"_lc_u_trap_tab" = "_lc_u_trap_tab_tc0";

	"_lc_ub_csa_01" := "_lc_gb_VarSectionGroup_0_CSA0";
	"_lc_ue_csa_01" := "_lc_ge_VarSectionGroup_0_CSA0";

	"__CSA_BEGIN_CPU0_" = "_VarSectionGroup_0_CSA0_START";
	"__CSA_END_CPU0_" = "_VarSectionGroup_0_CSA0_LIMIT";

	"_lc_ub_ustack" = "_VarSectionGroup_0_Stack0_START";
	"_lc_ue_ustack" = "_VarSectionGroup_0_Stack0_LIMIT";

	"__STARTUPSTACK_CORE0" = "_VarSectionGroup_0_Stack0_LIMIT";

	/*Do not change these lables these are the definition of RAM area that are required by LLSU */

	"_DSPR0_RAM_START" = 0x70000180;
	"_DSPR0_RAM_SIZE" = 0x2FE80;
	"_DSPR1_RAM_START" = 0x60000000;
	"_DSPR1_RAM_SIZE" = 0x30000;	


	"_PSPR0_RAM_START" = 0x70100000;
	"_PSPR0_RAM_SIZE" = 0x8000;
	"_PSPR1_RAM_START" = 0x60100000;
	"_PSPR1_RAM_SIZE" = 0x8000;


	"_DLMU0_NORMAL_RAM_START" = 0xB0001C00;
	"_DLMU0_NORMAL_RAM_SIZE" = 0x0400;


	#ifndef __REDEFINE_BASE_ADDRESS_GROUPS
	#include        "base_address_groups.lsl"
	#endif

	"_SMALL_DATA_TC0" := "_SMALL_DATA_";
	"_SMALL_DATA_TC1" := "_SMALL_DATA_";
	"_SMALL_DATA_TC2" := "_SMALL_DATA_";
	"_SMALL_DATA_TC3" := "_SMALL_DATA_";
	"_SMALL_DATA_TC4" := "_SMALL_DATA_";
	"_SMALL_DATA_TC5" := "_SMALL_DATA_";

	"_LITERAL_DATA_TC0" := "_LITERAL_DATA_";
	"_LITERAL_DATA_TC1" := "_LITERAL_DATA_";
	"_LITERAL_DATA_TC2" := "_LITERAL_DATA_";
	"_LITERAL_DATA_TC3" := "_LITERAL_DATA_";
	"_LITERAL_DATA_TC4" := "_LITERAL_DATA_";
	"_LITERAL_DATA_TC5" := "_LITERAL_DATA_";

	"_A8_DATA_TC0" := "_A8_DATA_";
	"_A8_DATA_TC1" := "_A8_DATA_";
	"_A8_DATA_TC2" := "_A8_DATA_";
	"_A8_DATA_TC3" := "_A8_DATA_";
	"_A8_DATA_TC4" := "_A8_DATA_";
	"_A8_DATA_TC5" := "_A8_DATA_";

	"_A9_DATA_TC0" := "_A9_DATA_";
	"_A9_DATA_TC1" := "_A9_DATA_";
	"_A9_DATA_TC2" := "_A9_DATA_";
	"_A9_DATA_TC3" := "_A9_DATA_";
	"_A9_DATA_TC4" := "_A9_DATA_";
	"_A9_DATA_TC5" := "_A9_DATA_";
}