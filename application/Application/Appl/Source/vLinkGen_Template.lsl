/**********************************************************************************************************************
 *  EXAMPLE CODE ONLY
 *  -------------------------------------------------------------------------------------------------------------------
 *  This Example Code is only intended for illustrating an example of a possible BSW integration and BSW configuration.
 *  The Example Code has not passed any quality control measures and may be incomplete. The Example Code is neither
 *  intended nor qualified for use in series production. The Example Code as well as any of its modifications and/or
 *  implementations must be tested with diligent care and must comply with all quality requirements which are necessary
 *  according to the state of the art before their use.
 *********************************************************************************************************************/


architecture TC1V1.6.2
{
  endianness
  {
    little;
  }
  space linear
  {
    id = 1;
    mau = 8;
    map (dest = bus:local_bus, size = 4G);
    copytable (align = 4, copy_unit = 1, dest = linear);
  }
  bus local_bus
  {
    mau = 8;
    width = 32;
  }
}

/**********************************************************************************************************************
 * CAUTION - DO NOT EDIT
 * -------------------------------------------------------------------------------------------------------------------
 * Never manually edit the following memory definitions in this file. Only change them in the vLinkGen configuration
 * (/MICROSAR/vLinkGen/vLinkGenMemLayout/vLinkGenMemoryRegion/vLinkGenMemoryRegionBlock) and regenerate the files.
 *********************************************************************************************************************/
derivative mpe
{
  core vtc
  {
    architecture = TC1V1.6.2;
  }
  bus local_bus
  {
    mau = 8;
    width = 32;
    map (dest = bus:vtc:local_bus, dest_offset = 0, size = 4G);
  }
  section_setup :vtc:linear
  {
    start_address (symbol = "_START");
  }
  memory RegionBlock_DSPR0_0
  {
    mau = 8;
    type = ram;
    size = 196608; /* 192 KiB */
    map (dest=bus:local_bus, dest_offset = 0x70000000, size = 196608);
  }
  memory RegionBlock_PSPR0_0
  {
    mau = 8;
    type = ram;
    size = 8192; /* 8 KiB */
    map (dest=bus:local_bus, dest_offset = 0x70100000, size = 8192);
  }
  memory vLinkGenMemoryRegionBlock
  {
    mau = 8;
    type = reserved rom;
    size = 655360; /* 640 KiB */
    map (dest=bus:local_bus, dest_offset = 0x80000000, size = 655360);
  }
  memory RegionBlock_PFlash0_ResetStartup
  {
    mau = 8;
    type = rom;
    size = 256; /* 256 Byte */
    map (dest=bus:local_bus, dest_offset = 0x800A0100, size = 256);
  }
  memory RegionBlock_PFlash0_Application
  {
    mau = 8;
    type = rom;
    size = 1408928; /* 1 MiB */
    map (dest=bus:local_bus, dest_offset = 0x800A0200, size = 1408928);
  }
  memory RegionBlock_DLMU0_Normal_RAM
  {
    mau = 8;
    type = ram;
    size = 8192; /* 8 KiB */
    map (dest=bus:local_bus, dest_offset = 0x90000000, size = 8192);
  }
  memory RegionBlock_0_BMHD0
  {
    mau = 8;
    type = rom;
    size = 512; /* 512 Byte */
    map (dest=bus:local_bus, dest_offset = 0xAF400000, size = 512);
  }
}

section_layout mpe:vtc:linear
{
  group ConstGroup_BMHD0_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_0_BMHD0)
  {
    group ConstSectionGroup_0_BMHD0 (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_0_BMHD0_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select ".rodata.BMHD0_UCB";
      }
    }
    "_ConstSectionGroup_0_BMHD0_START" = "_lc_gb_ConstSectionGroup_0_BMHD0";
    "_ConstSectionGroup_0_BMHD0_END" = ("_lc_ge_ConstSectionGroup_0_BMHD0" == 0) ? 0 : "_lc_ge_ConstSectionGroup_0_BMHD0" - 1;
    "_ConstSectionGroup_0_BMHD0_LIMIT" = "_lc_ge_ConstSectionGroup_0_BMHD0";

    "_ConstGroup_BMHD0_ALL_START" = "_ConstSectionGroup_0_BMHD0_START";
    "_ConstGroup_BMHD0_ALL_END" = "_ConstSectionGroup_0_BMHD0_END";
    "_ConstGroup_BMHD0_ALL_LIMIT" = "_ConstSectionGroup_0_BMHD0_LIMIT";
  }

  group ConstGroup_Startup_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_ResetStartup)
  {
    group ConstSectionGroup_0_Reset (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_0_Reset_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.libc.reset";
      }
    }
    "_ConstSectionGroup_0_Reset_START" = "_lc_gb_ConstSectionGroup_0_Reset";
    "_ConstSectionGroup_0_Reset_END" = ("_lc_ge_ConstSectionGroup_0_Reset" == 0) ? 0 : "_lc_ge_ConstSectionGroup_0_Reset" - 1;
    "_ConstSectionGroup_0_Reset_LIMIT" = "_lc_ge_ConstSectionGroup_0_Reset";

    group ConstSectionGroup_1_Startup (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_1_Startup_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.cstart";
      }
    }
    "_ConstSectionGroup_1_Startup_START" = "_lc_gb_ConstSectionGroup_1_Startup";
    "_ConstSectionGroup_1_Startup_END" = ("_lc_ge_ConstSectionGroup_1_Startup" == 0) ? 0 : "_lc_ge_ConstSectionGroup_1_Startup" - 1;
    "_ConstSectionGroup_1_Startup_LIMIT" = "_lc_ge_ConstSectionGroup_1_Startup";

    "_ConstGroup_Startup_ALL_START" = "_ConstSectionGroup_0_Reset_START";
    "_ConstGroup_Startup_ALL_END" = "_ConstSectionGroup_1_Startup_END";
    "_ConstGroup_Startup_ALL_LIMIT" = "_ConstSectionGroup_1_Startup_LIMIT";
  }

  group OS_OsApplication_Core0_ASIL_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_ASIL_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_INIT_ROM";
    "_OS_OsApplication_Core0_ASIL_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_ASIL_VAR_INIT_ROM";

  }

  group OS_OsApplication_Core0_ASIL_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_ASIL_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_INIT_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_INIT";
    "_OS_OsApplication_Core0_ASIL_VAR_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_INIT_PAD";

    group OS_OsApplication_Core0_ASIL_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_NOINIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_NOINIT";
    "_OS_OsApplication_Core0_ASIL_VAR_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_NOINIT_PAD";

    group OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT";
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_PAD";

    group OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss";
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_PAD";

    "_OS_OsApplication_Core0_ASIL_VAR_ALL_START" = "_OS_OsApplication_Core0_ASIL_VAR_INIT_START";
    "_OS_OsApplication_Core0_ASIL_VAR_ALL_END" = "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_END";
    "_OS_OsApplication_Core0_ASIL_VAR_ALL_LIMIT" = "_OS_OsApplication_Core0_ASIL_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_OsApplication_Core0_QM_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM";
    "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_QM_VAR_FAST_INIT_ROM";

    group OS_OsApplication_Core0_QM_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_QM_VAR_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_INIT_ROM";
    "_OS_OsApplication_Core0_QM_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_QM_VAR_INIT_ROM";

  }

  group OS_OsApplication_Core0_QM_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_QM_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_INIT";
    "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT";
    "_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_NOINIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT";
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss";
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_FAST_ZERO_INIT_bss_PAD";

    group OS_OsApplication_Core0_QM_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_INIT";
    "_OS_OsApplication_Core0_QM_VAR_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_NOINIT";
    "_OS_OsApplication_Core0_QM_VAR_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_NOINIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_ZERO_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT";
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss";
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_PAD";

    "_OS_OsApplication_Core0_QM_VAR_ALL_START" = "_OS_OsApplication_Core0_QM_VAR_FAST_INIT_START";
    "_OS_OsApplication_Core0_QM_VAR_ALL_END" = "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_END";
    "_OS_OsApplication_Core0_QM_VAR_ALL_LIMIT" = "_OS_OsApplication_Core0_QM_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_STACKS_CORE0_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_STACKS_CORE0_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_STACKS_CORE0_VAR_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_STACK_OSCORE0_ERROR_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_INIT_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_ISR_CORE_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_KERNEL_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_TASK_PRIO110_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_TASK_PRIO4294967295_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSCORE0_TASK_PRIO60_VAR_NOINIT";
        select "[.]bss.OS_STACK_OSTASK_CORE0_APPL_VAR_NOINIT";
      }
    }
    group OS_STACKS_CORE0_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_STACKS_CORE0_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_STACKS_CORE0_VAR_NOINIT_START" = "_lc_gb_OS_STACKS_CORE0_VAR_NOINIT";
    "_OS_STACKS_CORE0_VAR_NOINIT_END" = ("_lc_gb_OS_STACKS_CORE0_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_STACKS_CORE0_VAR_NOINIT_PAD" - 1;
    "_OS_STACKS_CORE0_VAR_NOINIT_LIMIT" = "_lc_gb_OS_STACKS_CORE0_VAR_NOINIT_PAD";

    "_OS_STACKS_CORE0_VAR_ALL_START" = "_OS_STACKS_CORE0_VAR_NOINIT_START";
    "_OS_STACKS_CORE0_VAR_ALL_END" = "_OS_STACKS_CORE0_VAR_NOINIT_END";
    "_OS_STACKS_CORE0_VAR_ALL_LIMIT" = "_OS_STACKS_CORE0_VAR_NOINIT_LIMIT";
  }

  "_RESET" = "_START";
  "_start" = "_START";

  "__CSA_BEGIN_CPU0_" = "_VarSectionGroup_0_CSA0_START";
  "__CSA_END_CPU0_" = "_VarSectionGroup_0_CSA0_LIMIT";
  "_lc_u_int_tab" = "_OS_INTVEC_CORE0_CODE_START";
  "_lc_u_trap_tab" = "_OS_EXCVEC_CORE0_CODE_START";
  "_lc_ub_csa_01" = "_lc_gb_VarSectionGroup_0_CSA0";
  "_lc_ub_ustack" = "_VarSectionGroup_0_Stack0_START";
  "_lc_ue_csa_01" = "_lc_ge_VarSectionGroup_0_CSA0";
  "_lc_ue_ustack" = "_VarSectionGroup_0_Stack0_LIMIT";
  "__STARTUPSTACK_CORE0" = "_VarSectionGroup_0_Stack0_LIMIT";

  group VarGroup_iStack0_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_iStack0 (ordered, contiguous, fill, align = 16)
    {
      reserved "VarSectionGroup_0_iStack0" (size = 1024);
    }
    "_VarSectionGroup_0_iStack0_START" = "_lc_gb_VarSectionGroup_0_iStack0";
    "_VarSectionGroup_0_iStack0_END" = ("_lc_ge_VarSectionGroup_0_iStack0" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_iStack0" - 1;
    "_VarSectionGroup_0_iStack0_LIMIT" = "_lc_ge_VarSectionGroup_0_iStack0";

    group VarSectionGroup_1_iStack0_ALIGN (ordered, contiguous, fill, align = 8)
    {
      reserved "VarSectionGroup_1_iStack0_ALIGN" (size = 8);
    }
    "_VarSectionGroup_1_iStack0_ALIGN_START" = "_lc_gb_VarSectionGroup_1_iStack0_ALIGN";
    "_VarSectionGroup_1_iStack0_ALIGN_END" = ("_lc_ge_VarSectionGroup_1_iStack0_ALIGN" == 0) ? 0 : "_lc_ge_VarSectionGroup_1_iStack0_ALIGN" - 1;
    "_VarSectionGroup_1_iStack0_ALIGN_LIMIT" = "_lc_ge_VarSectionGroup_1_iStack0_ALIGN";

    group VarSectionGroup_2_iStack0_PAD (ordered, contiguous, fill, align = 8)
    {
      reserved "VarSectionGroup_2_iStack0_PAD" (size = 16);
    }
    "_VarSectionGroup_2_iStack0_PAD_START" = "_lc_gb_VarSectionGroup_2_iStack0_PAD";
    "_VarSectionGroup_2_iStack0_PAD_END" = ("_lc_ge_VarSectionGroup_2_iStack0_PAD" == 0) ? 0 : "_lc_ge_VarSectionGroup_2_iStack0_PAD" - 1;
    "_VarSectionGroup_2_iStack0_PAD_LIMIT" = "_lc_ge_VarSectionGroup_2_iStack0_PAD";

    "_VarGroup_iStack0_ALL_START" = "_VarSectionGroup_0_iStack0_START";
    "_VarGroup_iStack0_ALL_END" = "_VarSectionGroup_2_iStack0_PAD_END";
    "_VarGroup_iStack0_ALL_LIMIT" = "_VarSectionGroup_2_iStack0_PAD_LIMIT";
  }

  group VarGroup_PSPR0_BSS_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PSPR0_0)
  {
    group VarSectionGroup_0_PSPR0_VAR_BSS (ordered, contiguous, fill, align = 4)
    {
      reserved "VarSectionGroup_0_PSPR0_VAR_BSS" (size = 0);
    }
    "_VarSectionGroup_0_PSPR0_VAR_BSS_START" = "_lc_gb_VarSectionGroup_0_PSPR0_VAR_BSS";
    "_VarSectionGroup_0_PSPR0_VAR_BSS_END" = ("_lc_ge_VarSectionGroup_0_PSPR0_VAR_BSS" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_PSPR0_VAR_BSS" - 1;
    "_VarSectionGroup_0_PSPR0_VAR_BSS_LIMIT" = "_lc_ge_VarSectionGroup_0_PSPR0_VAR_BSS";

    "_VarGroup_PSPR0_BSS_ALL_START" = "_VarSectionGroup_0_PSPR0_VAR_BSS_START";
    "_VarGroup_PSPR0_BSS_ALL_END" = "_VarSectionGroup_0_PSPR0_VAR_BSS_END";
    "_VarGroup_PSPR0_BSS_ALL_LIMIT" = "_VarSectionGroup_0_PSPR0_VAR_BSS_LIMIT";
  }

  group VarGroup_PSPR0_DATA_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group VarSectionGroup_0_PSPR0_VAR_ROM (ordered, contiguous, fill, align = 4)
    {
      reserved "VarSectionGroup_0_PSPR0_VAR_ROM" (size = 0);
    }
    "_VarSectionGroup_0_PSPR0_VAR_ROM_START" = "_lc_gb_VarSectionGroup_0_PSPR0_VAR_ROM";
    "_VarSectionGroup_0_PSPR0_VAR_ROM_LIMIT" = "_lc_ge_VarSectionGroup_0_PSPR0_VAR_ROM";

  }

  group VarGroup_PSPR0_DATA_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PSPR0_0)
  {
    group VarSectionGroup_0_PSPR0_VAR (ordered, contiguous, fill, align = 4)
    {
      reserved "VarSectionGroup_0_PSPR0_VAR" (size = 0);
    }
    group VarSectionGroup_0_PSPR0_VAR_PAD (align = 4)
    {
      reserved "VarSectionGroup_0_PSPR0_VAR_PAD" (size = 0);
    }
    "_VarSectionGroup_0_PSPR0_VAR_START" = "_lc_gb_VarSectionGroup_0_PSPR0_VAR";
    "_VarSectionGroup_0_PSPR0_VAR_END" = ("_lc_ge_VarSectionGroup_0_PSPR0_VAR" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_PSPR0_VAR" - 1;
    "_VarSectionGroup_0_PSPR0_VAR_LIMIT" = "_lc_ge_VarSectionGroup_0_PSPR0_VAR";

    "_VarGroup_PSPR0_DATA_ALL_START" = "_VarSectionGroup_0_PSPR0_VAR_START";
    "_VarGroup_PSPR0_DATA_ALL_END" = "_VarSectionGroup_0_PSPR0_VAR_END";
    "_VarGroup_PSPR0_DATA_ALL_LIMIT" = "_VarSectionGroup_0_PSPR0_VAR_LIMIT";
  }

  group VarGroup_Stack0_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_Stack0 (ordered, contiguous, fill, align = 4)
    {
      reserved "VarSectionGroup_0_Stack0" (size = 1024);
    }
    group VarSectionGroup_0_Stack0_PAD (align = 4)
    {
      reserved "VarSectionGroup_0_Stack0_PAD" (size = 0);
    }
    "_VarSectionGroup_0_Stack0_START" = "_lc_gb_VarSectionGroup_0_Stack0";
    "_VarSectionGroup_0_Stack0_END" = ("_lc_ge_VarSectionGroup_0_Stack0" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_Stack0" - 1;
    "_VarSectionGroup_0_Stack0_LIMIT" = "_lc_ge_VarSectionGroup_0_Stack0";

    "_VarGroup_Stack0_ALL_START" = "_VarSectionGroup_0_Stack0_START";
    "_VarGroup_Stack0_ALL_END" = "_VarSectionGroup_0_Stack0_END";
    "_VarGroup_Stack0_ALL_LIMIT" = "_VarSectionGroup_0_Stack0_LIMIT";
  }

  group OS_INTVEC_CODE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_INTVEC_CODE (ordered, contiguous, fill, align = 8192)
    {
      section "OS_INTVEC_CODE_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.OS_INTVEC_CODE";
      }
    }
    group OS_INTVEC_CODE_PAD (align = 8)
    {
      reserved "OS_INTVEC_CODE_PAD" (size = 16);
    }
    "_OS_INTVEC_CODE_START" = "_lc_gb_OS_INTVEC_CODE";
    "_OS_INTVEC_CODE_END" = ("_lc_gb_OS_INTVEC_CODE_PAD" == 0) ? 0 : "_lc_gb_OS_INTVEC_CODE_PAD" - 1;
    "_OS_INTVEC_CODE_LIMIT" = "_lc_gb_OS_INTVEC_CODE_PAD";

    "_OS_INTVEC_CODE_ALL_START" = "_OS_INTVEC_CODE_START";
    "_OS_INTVEC_CODE_ALL_END" = "_OS_INTVEC_CODE_END";
    "_OS_INTVEC_CODE_ALL_LIMIT" = "_OS_INTVEC_CODE_LIMIT";
  }

  group OS_INTVEC_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_INTVEC_CONST (ordered, contiguous, fill, align = 8192)
    {
      section "OS_INTVEC_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_INTVEC_CONST";
      }
    }
    group OS_INTVEC_CONST_PAD (align = 8)
    {
      reserved "OS_INTVEC_CONST_PAD" (size = 16);
    }
    "_OS_INTVEC_CONST_START" = "_lc_gb_OS_INTVEC_CONST";
    "_OS_INTVEC_CONST_END" = ("_lc_gb_OS_INTVEC_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_INTVEC_CONST_PAD" - 1;
    "_OS_INTVEC_CONST_LIMIT" = "_lc_gb_OS_INTVEC_CONST_PAD";

    "_OS_INTVEC_CONST_ALL_START" = "_OS_INTVEC_CONST_START";
    "_OS_INTVEC_CONST_ALL_END" = "_OS_INTVEC_CONST_END";
    "_OS_INTVEC_CONST_ALL_LIMIT" = "_OS_INTVEC_CONST_LIMIT";
  }

  group VarGroup_DSPR0_BSS_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_DSPR0_VAR_BSS (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DSPR0_VAR_BSS_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.BswM*";
        select "[.]bss.Fee*";
        select "[.]bss.Fls*";
        select "[.]bss.NvBlocks*";
        select "[.]bss.NvM*";
        select "[.]bss.NvMem*";
      }
    }
    "_VarSectionGroup_0_DSPR0_VAR_BSS_START" = "_lc_gb_VarSectionGroup_0_DSPR0_VAR_BSS";
    "_VarSectionGroup_0_DSPR0_VAR_BSS_END" = ("_lc_ge_VarSectionGroup_0_DSPR0_VAR_BSS" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_DSPR0_VAR_BSS" - 1;
    "_VarSectionGroup_0_DSPR0_VAR_BSS_LIMIT" = "_lc_ge_VarSectionGroup_0_DSPR0_VAR_BSS";

    "_VarGroup_DSPR0_BSS_ALL_START" = "_VarSectionGroup_0_DSPR0_VAR_BSS_START";
    "_VarGroup_DSPR0_BSS_ALL_END" = "_VarSectionGroup_0_DSPR0_VAR_BSS_END";
    "_VarGroup_DSPR0_BSS_ALL_LIMIT" = "_VarSectionGroup_0_DSPR0_VAR_BSS_LIMIT";
  }

  group VarGroup_DSPR0_DATA_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group VarSectionGroup_0_DSPR0_VAR_ROM (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DSPR0_VAR_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.BswM*";
          select "[.]data.Fee*";
          select "[.]data.Fls*";
          select "[.]data.NvBlocks*";
          select "[.]data.NvM*";
          select "[.]data.NvMem*";
        }
      }
    }
    "_VarSectionGroup_0_DSPR0_VAR_ROM_START" = "_lc_gb_VarSectionGroup_0_DSPR0_VAR_ROM";
    "_VarSectionGroup_0_DSPR0_VAR_ROM_LIMIT" = "_lc_ge_VarSectionGroup_0_DSPR0_VAR_ROM";

  }

  group VarGroup_DSPR0_DATA_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_DSPR0_VAR (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DSPR0_VAR_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.BswM*";
        select "[.]data.Fee*";
        select "[.]data.Fls*";
        select "[.]data.NvBlocks*";
        select "[.]data.NvM*";
        select "[.]data.NvMem*";
      }
    }
    group VarSectionGroup_0_DSPR0_VAR_PAD (align = 16)
    {
      reserved "VarSectionGroup_0_DSPR0_VAR_PAD" (size = 0);
    }
    "_VarSectionGroup_0_DSPR0_VAR_START" = "_lc_gb_VarSectionGroup_0_DSPR0_VAR";
    "_VarSectionGroup_0_DSPR0_VAR_END" = ("_lc_ge_VarSectionGroup_0_DSPR0_VAR" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_DSPR0_VAR" - 1;
    "_VarSectionGroup_0_DSPR0_VAR_LIMIT" = "_lc_ge_VarSectionGroup_0_DSPR0_VAR";

    "_VarGroup_DSPR0_DATA_ALL_START" = "_VarSectionGroup_0_DSPR0_VAR_START";
    "_VarGroup_DSPR0_DATA_ALL_END" = "_VarSectionGroup_0_DSPR0_VAR_END";
    "_VarGroup_DSPR0_DATA_ALL_LIMIT" = "_VarSectionGroup_0_DSPR0_VAR_LIMIT";
  }

  group OS_EXCVEC_CORE0_CODE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_EXCVEC_CORE0_CODE (ordered, contiguous, fill, align = 256)
    {
      section "OS_EXCVEC_CORE0_CODE_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.OS_EXCVEC_CORE0_CODE";
      }
    }
    group OS_EXCVEC_CORE0_CODE_PAD (align = 8)
    {
      reserved "OS_EXCVEC_CORE0_CODE_PAD" (size = 16);
    }
    "_OS_EXCVEC_CORE0_CODE_START" = "_lc_gb_OS_EXCVEC_CORE0_CODE";
    "_OS_EXCVEC_CORE0_CODE_END" = ("_lc_gb_OS_EXCVEC_CORE0_CODE_PAD" == 0) ? 0 : "_lc_gb_OS_EXCVEC_CORE0_CODE_PAD" - 1;
    "_OS_EXCVEC_CORE0_CODE_LIMIT" = "_lc_gb_OS_EXCVEC_CORE0_CODE_PAD";

    "_OS_EXCVEC_CORE0_CODE_ALL_START" = "_OS_EXCVEC_CORE0_CODE_START";
    "_OS_EXCVEC_CORE0_CODE_ALL_END" = "_OS_EXCVEC_CORE0_CODE_END";
    "_OS_EXCVEC_CORE0_CODE_ALL_LIMIT" = "_OS_EXCVEC_CORE0_CODE_LIMIT";
  }

  group OS_EXCVEC_CORE0_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_EXCVEC_CORE0_CONST (ordered, contiguous, fill, align = 256)
    {
      section "OS_EXCVEC_CORE0_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_EXCVEC_CORE0_CONST";
      }
    }
    group OS_EXCVEC_CORE0_CONST_PAD (align = 8)
    {
      reserved "OS_EXCVEC_CORE0_CONST_PAD" (size = 16);
    }
    "_OS_EXCVEC_CORE0_CONST_START" = "_lc_gb_OS_EXCVEC_CORE0_CONST";
    "_OS_EXCVEC_CORE0_CONST_END" = ("_lc_gb_OS_EXCVEC_CORE0_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_EXCVEC_CORE0_CONST_PAD" - 1;
    "_OS_EXCVEC_CORE0_CONST_LIMIT" = "_lc_gb_OS_EXCVEC_CORE0_CONST_PAD";

    "_OS_EXCVEC_CORE0_CONST_ALL_START" = "_OS_EXCVEC_CORE0_CONST_START";
    "_OS_EXCVEC_CORE0_CONST_ALL_END" = "_OS_EXCVEC_CORE0_CONST_END";
    "_OS_EXCVEC_CORE0_CONST_ALL_LIMIT" = "_OS_EXCVEC_CORE0_CONST_LIMIT";
  }

  group OS_INTVEC_CORE0_CODE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_INTVEC_CORE0_CODE (ordered, contiguous, fill, align = 8192)
    {
      section "OS_INTVEC_CORE0_CODE_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.OS_INTVEC_CORE0_CODE";
      }
    }
    group OS_INTVEC_CORE0_CODE_PAD (align = 8)
    {
      reserved "OS_INTVEC_CORE0_CODE_PAD" (size = 16);
    }
    "_OS_INTVEC_CORE0_CODE_START" = "_lc_gb_OS_INTVEC_CORE0_CODE";
    "_OS_INTVEC_CORE0_CODE_END" = ("_lc_gb_OS_INTVEC_CORE0_CODE_PAD" == 0) ? 0 : "_lc_gb_OS_INTVEC_CORE0_CODE_PAD" - 1;
    "_OS_INTVEC_CORE0_CODE_LIMIT" = "_lc_gb_OS_INTVEC_CORE0_CODE_PAD";

    "_OS_INTVEC_CORE0_CODE_ALL_START" = "_OS_INTVEC_CORE0_CODE_START";
    "_OS_INTVEC_CORE0_CODE_ALL_END" = "_OS_INTVEC_CORE0_CODE_END";
    "_OS_INTVEC_CORE0_CODE_ALL_LIMIT" = "_OS_INTVEC_CORE0_CODE_LIMIT";
  }

  group OS_INTVEC_CORE0_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_INTVEC_CORE0_CONST (ordered, contiguous, fill, align = 8192)
    {
      section "OS_INTVEC_CORE0_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_INTVEC_CORE0_CONST";
      }
    }
    group OS_INTVEC_CORE0_CONST_PAD (align = 8)
    {
      reserved "OS_INTVEC_CORE0_CONST_PAD" (size = 16);
    }
    "_OS_INTVEC_CORE0_CONST_START" = "_lc_gb_OS_INTVEC_CORE0_CONST";
    "_OS_INTVEC_CORE0_CONST_END" = ("_lc_gb_OS_INTVEC_CORE0_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_INTVEC_CORE0_CONST_PAD" - 1;
    "_OS_INTVEC_CORE0_CONST_LIMIT" = "_lc_gb_OS_INTVEC_CORE0_CONST_PAD";

    "_OS_INTVEC_CORE0_CONST_ALL_START" = "_OS_INTVEC_CORE0_CONST_START";
    "_OS_INTVEC_CORE0_CONST_ALL_END" = "_OS_INTVEC_CORE0_CONST_END";
    "_OS_INTVEC_CORE0_CONST_ALL_LIMIT" = "_OS_INTVEC_CORE0_CONST_LIMIT";
  }

  group VarGroup_DLMU0_NonCached_BSS_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DLMU0_Normal_RAM)
  {
    group VarSectionGroup_0_DLMU0_NonCached_VAR_BSS (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.Dcm*";
        select "[.]bss.Dem*";
        select "[.]bss.Det*";
        select "[.]bss.Diag*";
        select "[.]bss.EcuM*";
        select "[.]bss.EcuMM*";
        select "[.]bss.Mcu*";
        select "[.]bss.MemIf*";
        select "[.]bss.SwcDiag*";
        select "[.]bss.Wdg_17_Scu*";
        select "[.]bss.WdgIf*";
        select "[.]bss.WdgM*";
      }
    }
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_START" = "_lc_gb_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS";
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_END" = ("_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS" - 1;
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_LIMIT" = "_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS";

    "_VarGroup_DLMU0_NonCached_BSS_ALL_START" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_START";
    "_VarGroup_DLMU0_NonCached_BSS_ALL_END" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_END";
    "_VarGroup_DLMU0_NonCached_BSS_ALL_LIMIT" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_BSS_LIMIT";
  }

  group VarGroup_DLMU0_NonCached_DATA_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.Dcm*";
          select "[.]data.Dem*";
          select "[.]data.Det*";
          select "[.]data.Diag*";
          select "[.]data.EcuM*";
          select "[.]data.EcuMM*";
          select "[.]data.Mcu*";
          select "[.]data.MemIf*";
          select "[.]data.SwcDiag*";
          select "[.]data.Wdg_17_Scu*";
          select "[.]data.WdgIf*";
        }
      }
    }
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_START" = "_lc_gb_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM";
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM_LIMIT" = "_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_ROM";

  }

  group VarGroup_DLMU0_NonCached_DATA_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DLMU0_Normal_RAM)
  {
    group VarSectionGroup_0_DLMU0_NonCached_VAR_DATA (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.Dcm*";
        select "[.]data.Dem*";
        select "[.]data.Det*";
        select "[.]data.Diag*";
        select "[.]data.EcuM*";
        select "[.]data.EcuMM*";
        select "[.]data.Mcu*";
        select "[.]data.MemIf*";
        select "[.]data.SwcDiag*";
        select "[.]data.Wdg_17_Scu*";
        select "[.]data.WdgIf*";
      }
    }
    group VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_PAD (align = 16)
    {
      reserved "VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_PAD" (size = 0);
    }
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_START" = "_lc_gb_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA";
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_END" = ("_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA" - 1;
    "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_LIMIT" = "_lc_ge_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA";

    "_VarGroup_DLMU0_NonCached_DATA_ALL_START" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_START";
    "_VarGroup_DLMU0_NonCached_DATA_ALL_END" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_END";
    "_VarGroup_DLMU0_NonCached_DATA_ALL_LIMIT" = "_VarSectionGroup_0_DLMU0_NonCached_VAR_DATA_LIMIT";
  }

  group ConstGroup_PFlash0Appl_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group ConstSectionGroup_3_text (ordered, contiguous, fill, align = 4)
    {
      reserved "ConstSectionGroup_3_text" (size = 0);
    }
    "_ConstSectionGroup_3_text_START" = "_lc_gb_ConstSectionGroup_3_text";
    "_ConstSectionGroup_3_text_END" = ("_lc_ge_ConstSectionGroup_3_text" == 0) ? 0 : "_lc_ge_ConstSectionGroup_3_text" - 1;
    "_ConstSectionGroup_3_text_LIMIT" = "_lc_ge_ConstSectionGroup_3_text";

    group ConstSectionGroup_4_rodata (ordered, contiguous, fill, align = 4)
    {
      reserved "ConstSectionGroup_4_rodata" (size = 0);
    }
    "_ConstSectionGroup_4_rodata_START" = "_lc_gb_ConstSectionGroup_4_rodata";
    "_ConstSectionGroup_4_rodata_END" = ("_lc_ge_ConstSectionGroup_4_rodata" == 0) ? 0 : "_lc_ge_ConstSectionGroup_4_rodata" - 1;
    "_ConstSectionGroup_4_rodata_LIMIT" = "_lc_ge_ConstSectionGroup_4_rodata";

    "_ConstGroup_PFlash0Appl_ALL_START" = "_ConstSectionGroup_3_text_START";
    "_ConstGroup_PFlash0Appl_ALL_END" = "_ConstSectionGroup_4_rodata_END";
    "_ConstGroup_PFlash0Appl_ALL_LIMIT" = "_ConstSectionGroup_4_rodata_LIMIT";
  }

  "_A8_DATA_" = 0;
  "_A9_DATA_" = 0;
  "_LITERAL_DATA_" = 0;
  "_SMALL_DATA_" = 0;

  group OS_DATA_CORE0_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_DATA_CORE0_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_CORE0_VAR_FAST";
          select "[.]zdata.OS_CORE0_VAR_FAST_NOCACHE";
          select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST";
          select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE";
          select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_SAVED_ZONE";
        }
      }
    }
    "_OS_DATA_CORE0_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_INIT_ROM";
    "_OS_DATA_CORE0_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_DATA_CORE0_VAR_FAST_INIT_ROM";

    group OS_DATA_CORE0_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_CORE0_VAR";
          select "[.]data.OS_CORE0_VAR_NOCACHE";
          select "[.]data.OS_SystemApplication_OsCore0_VAR";
          select "[.]data.OS_SystemApplication_OsCore0_VAR_NOCACHE";
          select "[.]data.OS_SystemApplication_OsCore0_VAR_SAVED_ZONE";
        }
      }
    }
    "_OS_DATA_CORE0_VAR_INIT_ROM_START" = "_lc_gb_OS_DATA_CORE0_VAR_INIT_ROM";
    "_OS_DATA_CORE0_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_DATA_CORE0_VAR_INIT_ROM";

  }

  group OS_DATA_CORE0_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_DATA_CORE0_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_CORE0_VAR_FAST";
        select "[.]zdata.OS_CORE0_VAR_FAST_NOCACHE";
        select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST";
        select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE";
        select "[.]zdata.OS_SystemApplication_OsCore0_VAR_FAST_SAVED_ZONE";
      }
    }
    group OS_DATA_CORE0_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_FAST_INIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_INIT";
    "_OS_DATA_CORE0_VAR_FAST_INIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_FAST_INIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_INIT_PAD";

    group OS_DATA_CORE0_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_CORE0_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_CORE0_VAR_FAST_NOINIT";
        select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_NOINIT";
        select "[.]zbss.OS_SystemApplication_OsCore0_VAR_FAST_SAVED_ZONE_NOINIT";
      }
    }
    group OS_DATA_CORE0_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_DATA_CORE0_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_FAST_NOINIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_NOINIT";
    "_OS_DATA_CORE0_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_NOINIT_PAD";

    group OS_DATA_CORE0_VAR_FAST_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_FAST_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT";
        select "[.]*.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT";
      }
    }
    group OS_DATA_CORE0_VAR_FAST_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_FAST_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT";
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_PAD";

    group OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_SystemApplication_OsCore0_VAR_FAST_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_SystemApplication_OsCore0_VAR_FAST_ZERO_INIT_bss";
      }
    }
    group OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_START" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss";
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_END" = ("_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_PAD" - 1;
    "_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_FAST_ZERO_INIT_bss_PAD";

    group OS_DATA_CORE0_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_CORE0_VAR";
        select "[.]data.OS_CORE0_VAR_NOCACHE";
        select "[.]data.OS_SystemApplication_OsCore0_VAR";
        select "[.]data.OS_SystemApplication_OsCore0_VAR_NOCACHE";
        select "[.]data.OS_SystemApplication_OsCore0_VAR_SAVED_ZONE";
      }
    }
    group OS_DATA_CORE0_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_INIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_INIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_INIT";
    "_OS_DATA_CORE0_VAR_INIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_INIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_INIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_INIT_PAD";

    group OS_DATA_CORE0_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_CORE0_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_CORE0_VAR_NOINIT";
        select "[.]bss.OS_SystemApplication_OsCore0_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_SystemApplication_OsCore0_VAR_NOINIT";
        select "[.]bss.OS_SystemApplication_OsCore0_VAR_SAVED_ZONE_NOINIT";
      }
    }
    group OS_DATA_CORE0_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_DATA_CORE0_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_NOINIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_NOINIT";
    "_OS_DATA_CORE0_VAR_NOINIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_NOINIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_NOINIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_NOINIT_PAD";

    group OS_DATA_CORE0_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT";
        select "[.]*.OS_SystemApplication_OsCore0_VAR_ZERO_INIT";
      }
    }
    group OS_DATA_CORE0_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_ZERO_INIT_START" = "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT";
    "_OS_DATA_CORE0_VAR_ZERO_INIT_END" = ("_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_PAD" - 1;
    "_OS_DATA_CORE0_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_PAD";

    group OS_DATA_CORE0_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_CORE0_VAR_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_SystemApplication_OsCore0_VAR_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_SystemApplication_OsCore0_VAR_ZERO_INIT_bss";
      }
    }
    group OS_DATA_CORE0_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_DATA_CORE0_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_DATA_CORE0_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_bss";
    "_OS_DATA_CORE0_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_DATA_CORE0_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_DATA_CORE0_VAR_ZERO_INIT_bss_PAD";

    "_OS_DATA_CORE0_VAR_ALL_START" = "_OS_DATA_CORE0_VAR_FAST_INIT_START";
    "_OS_DATA_CORE0_VAR_ALL_END" = "_OS_DATA_CORE0_VAR_ZERO_INIT_bss_END";
    "_OS_DATA_CORE0_VAR_ALL_LIMIT" = "_OS_DATA_CORE0_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_DATA_SHARED_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_DATA_SHARED_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_VAR_FAST_NOCACHE";
        }
      }
    }
    "_OS_DATA_SHARED_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_DATA_SHARED_VAR_FAST_INIT_ROM";
    "_OS_DATA_SHARED_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_DATA_SHARED_VAR_FAST_INIT_ROM";

    group OS_DATA_SHARED_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_VAR_NOCACHE";
        }
      }
    }
    "_OS_DATA_SHARED_VAR_INIT_ROM_START" = "_lc_gb_OS_DATA_SHARED_VAR_INIT_ROM";
    "_OS_DATA_SHARED_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_DATA_SHARED_VAR_INIT_ROM";

  }

  group OS_DATA_SHARED_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DLMU0_Normal_RAM)
  {
    group OS_DATA_SHARED_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_VAR_FAST_NOCACHE";
      }
    }
    group OS_DATA_SHARED_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_SHARED_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_DATA_SHARED_VAR_FAST_INIT_START" = "_lc_gb_OS_DATA_SHARED_VAR_FAST_INIT";
    "_OS_DATA_SHARED_VAR_FAST_INIT_END" = ("_lc_gb_OS_DATA_SHARED_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_SHARED_VAR_FAST_INIT_PAD" - 1;
    "_OS_DATA_SHARED_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_DATA_SHARED_VAR_FAST_INIT_PAD";

    group OS_DATA_SHARED_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_BARRIER_CORE0_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_CORESTATUS_CORE0_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_PUBLIC_CORE0_VAR_FAST_NOINIT";
        select "[.]zbss.OS_VAR_FAST_NOCACHE_NOINIT";
      }
    }
    group OS_DATA_SHARED_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_DATA_SHARED_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_DATA_SHARED_VAR_FAST_NOINIT_START" = "_lc_gb_OS_DATA_SHARED_VAR_FAST_NOINIT";
    "_OS_DATA_SHARED_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_DATA_SHARED_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_SHARED_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_DATA_SHARED_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_DATA_SHARED_VAR_FAST_NOINIT_PAD";

    group OS_DATA_SHARED_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_VAR_NOCACHE";
      }
    }
    group OS_DATA_SHARED_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_DATA_SHARED_VAR_INIT_PAD" (size = 16);
    }
    "_OS_DATA_SHARED_VAR_INIT_START" = "_lc_gb_OS_DATA_SHARED_VAR_INIT";
    "_OS_DATA_SHARED_VAR_INIT_END" = ("_lc_gb_OS_DATA_SHARED_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_SHARED_VAR_INIT_PAD" - 1;
    "_OS_DATA_SHARED_VAR_INIT_LIMIT" = "_lc_gb_OS_DATA_SHARED_VAR_INIT_PAD";

    group OS_DATA_SHARED_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_DATA_SHARED_VAR_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_BARRIER_CORE0_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_CORESTATUS_CORE0_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_PUBLIC_CORE0_VAR_NOINIT";
        select "[.]bss.OS_VAR_NOCACHE_NOINIT";
      }
    }
    group OS_DATA_SHARED_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_DATA_SHARED_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_DATA_SHARED_VAR_NOINIT_START" = "_lc_gb_OS_DATA_SHARED_VAR_NOINIT";
    "_OS_DATA_SHARED_VAR_NOINIT_END" = ("_lc_gb_OS_DATA_SHARED_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_DATA_SHARED_VAR_NOINIT_PAD" - 1;
    "_OS_DATA_SHARED_VAR_NOINIT_LIMIT" = "_lc_gb_OS_DATA_SHARED_VAR_NOINIT_PAD";

    "_OS_DATA_SHARED_VAR_ALL_START" = "_OS_DATA_SHARED_VAR_FAST_INIT_START";
    "_OS_DATA_SHARED_VAR_ALL_END" = "_OS_DATA_SHARED_VAR_NOINIT_END";
    "_OS_DATA_SHARED_VAR_ALL_LIMIT" = "_OS_DATA_SHARED_VAR_NOINIT_LIMIT";
  }

  group OS_GLOBALSHARED_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_GLOBALSHARED_CONST (ordered, contiguous, fill)
    {
      section "OS_GLOBALSHARED_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_GLOBALSHARED_CONST";
        select "[.]zrodata.OS_GLOBALSHARED_CONST_FAST";
      }
    }
    group OS_GLOBALSHARED_CONST_PAD (align = 1)
    {
      reserved "OS_GLOBALSHARED_CONST_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_CONST_START" = "_lc_gb_OS_GLOBALSHARED_CONST";
    "_OS_GLOBALSHARED_CONST_END" = ("_lc_gb_OS_GLOBALSHARED_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_CONST_PAD" - 1;
    "_OS_GLOBALSHARED_CONST_LIMIT" = "_lc_gb_OS_GLOBALSHARED_CONST_PAD";

    "_OS_GLOBALSHARED_CONST_ALL_START" = "_OS_GLOBALSHARED_CONST_START";
    "_OS_GLOBALSHARED_CONST_ALL_END" = "_OS_GLOBALSHARED_CONST_END";
    "_OS_GLOBALSHARED_CONST_ALL_LIMIT" = "_OS_GLOBALSHARED_CONST_LIMIT";
  }

  group OS_GLOBALSHARED_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_GLOBALSHARED_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_GLOBALSHARED_VAR_FAST";
          select "[.]zdata.OS_GLOBALSHARED_VAR_FAST_NOCACHE";
        }
      }
    }
    "_OS_GLOBALSHARED_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_INIT_ROM";
    "_OS_GLOBALSHARED_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_GLOBALSHARED_VAR_FAST_INIT_ROM";

    group OS_GLOBALSHARED_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_GLOBALSHARED_VAR";
          select "[.]data.OS_GLOBALSHARED_VAR_NOCACHE";
        }
      }
    }
    "_OS_GLOBALSHARED_VAR_INIT_ROM_START" = "_lc_gb_OS_GLOBALSHARED_VAR_INIT_ROM";
    "_OS_GLOBALSHARED_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_GLOBALSHARED_VAR_INIT_ROM";

  }

  group OS_GLOBALSHARED_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DLMU0_Normal_RAM)
  {
    group OS_GLOBALSHARED_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_GLOBALSHARED_VAR_FAST";
        select "[.]zdata.OS_GLOBALSHARED_VAR_FAST_NOCACHE";
      }
    }
    group OS_GLOBALSHARED_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_FAST_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_INIT";
    "_OS_GLOBALSHARED_VAR_FAST_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_FAST_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_INIT_PAD";

    group OS_GLOBALSHARED_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_GLOBALSHARED_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_GLOBALSHARED_VAR_FAST_NOINIT";
      }
    }
    group OS_GLOBALSHARED_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_GLOBALSHARED_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_FAST_NOINIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_NOINIT";
    "_OS_GLOBALSHARED_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_NOINIT_PAD";

    group OS_GLOBALSHARED_VAR_FAST_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_GLOBALSHARED_VAR_FAST_NOCACHE_ZERO_INIT";
        select "[.]*.OS_GLOBALSHARED_VAR_FAST_ZERO_INIT";
      }
    }
    group OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT";
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_PAD";

    group OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_GLOBALSHARED_VAR_FAST_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss";
      }
    }
    group OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_START" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss";
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_FAST_ZERO_INIT_bss_PAD";

    group OS_GLOBALSHARED_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_GLOBALSHARED_VAR";
        select "[.]data.OS_GLOBALSHARED_VAR_NOCACHE";
      }
    }
    group OS_GLOBALSHARED_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_INIT";
    "_OS_GLOBALSHARED_VAR_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_INIT_PAD";

    group OS_GLOBALSHARED_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_GLOBALSHARED_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_GLOBALSHARED_VAR_NOINIT";
      }
    }
    group OS_GLOBALSHARED_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_GLOBALSHARED_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_NOINIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_NOINIT";
    "_OS_GLOBALSHARED_VAR_NOINIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_NOINIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_NOINIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_NOINIT_PAD";

    group OS_GLOBALSHARED_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_GLOBALSHARED_VAR_NOCACHE_ZERO_INIT";
        select "[.]*.OS_GLOBALSHARED_VAR_ZERO_INIT";
      }
    }
    group OS_GLOBALSHARED_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT";
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_PAD";

    group OS_GLOBALSHARED_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_GLOBALSHARED_VAR_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_GLOBALSHARED_VAR_ZERO_INIT_bss";
      }
    }
    group OS_GLOBALSHARED_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_bss";
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_PAD";

    "_OS_GLOBALSHARED_VAR_ALL_START" = "_OS_GLOBALSHARED_VAR_FAST_INIT_START";
    "_OS_GLOBALSHARED_VAR_ALL_END" = "_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_END";
    "_OS_GLOBALSHARED_VAR_ALL_LIMIT" = "_OS_GLOBALSHARED_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_GLOBALSHARED_VAR_SAVED_ZONE_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_GLOBALSHARED_VAR_FAST_SAVED_ZONE";
        }
      }
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_ROM";

    group OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_GLOBALSHARED_VAR_SAVED_ZONE";
        }
      }
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM_LIMIT" = "_lc_ge_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_ROM";

  }

  group OS_GLOBALSHARED_VAR_SAVED_ZONE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DLMU0_Normal_RAM)
  {
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_GLOBALSHARED_VAR_FAST_SAVED_ZONE";
      }
    }
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_PAD";

    group OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_GLOBALSHARED_VAR_FAST_SAVED_ZONE_NOINIT";
      }
    }
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_NOINIT_PAD";

    group OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_GLOBALSHARED_VAR_SAVED_ZONE";
      }
    }
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_PAD (align = 4)
    {
      reserved "OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_INIT_PAD";

    group OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT";
      }
    }
    group OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_PAD (align = 1)
    {
      reserved "OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_PAD" (size = 16);
    }
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_START" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_END" = ("_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_PAD" - 1;
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_LIMIT" = "_lc_gb_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_PAD";

    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_ALL_START" = "_OS_GLOBALSHARED_VAR_SAVED_ZONE_FAST_INIT_START";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_ALL_END" = "_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_END";
    "_OS_GLOBALSHARED_VAR_SAVED_ZONE_ALL_LIMIT" = "_OS_GLOBALSHARED_VAR_SAVED_ZONE_NOINIT_LIMIT";
  }

  group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM";
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_ROM";

  }

  group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT";
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_PAD";

    group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT";
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_PAD";

    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_ALL_START" = "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_INIT_START";
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_ALL_END" = "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_END";
    "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_ALL_LIMIT" = "_OS_OsApplication_Core0_ASIL_VAR_SAVED_ZONE_NOINIT_LIMIT";
  }

  group OS_OsApplication_Core0_BSW_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST";
          select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE";
        }
      }
    }
    "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM";
    "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_ROM";

    group OS_OsApplication_Core0_BSW_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_OsApplication_Core0_BSW_VAR";
          select "[.]data.OS_OsApplication_Core0_BSW_VAR_NOCACHE";
        }
      }
    }
    "_OS_OsApplication_Core0_BSW_VAR_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_INIT_ROM";
    "_OS_OsApplication_Core0_BSW_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_INIT_ROM";

  }

  group OS_OsApplication_Core0_BSW_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_BSW_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST";
        select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_NOINIT";
        select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT";
    "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_NOINIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT";
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_FAST_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss";
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_FAST_ZERO_INIT_bss_PAD";

    group OS_OsApplication_Core0_BSW_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_OsApplication_Core0_BSW_VAR";
        select "[.]data.OS_OsApplication_Core0_BSW_VAR_NOCACHE";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_OsApplication_Core0_BSW_VAR_NOCACHE_NOINIT";
        select "[.]bss.OS_OsApplication_Core0_BSW_VAR_NOINIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOINIT";
    "_OS_OsApplication_Core0_BSW_VAR_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_NOINIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT";
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_NOCACHE_ZERO_INIT_bss";
        select "[.]*.OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss";
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_PAD";

    "_OS_OsApplication_Core0_BSW_VAR_ALL_START" = "_OS_OsApplication_Core0_BSW_VAR_FAST_INIT_START";
    "_OS_OsApplication_Core0_BSW_VAR_ALL_END" = "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_END";
    "_OS_OsApplication_Core0_BSW_VAR_ALL_LIMIT" = "_OS_OsApplication_Core0_BSW_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_SAVED_ZONE";
        }
      }
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_ROM";

    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data.OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE";
        }
      }
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_ROM";

  }

  group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata.OS_OsApplication_Core0_BSW_VAR_FAST_SAVED_ZONE";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss.OS_OsApplication_Core0_BSW_VAR_FAST_SAVED_ZONE_NOINIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_NOINIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data.OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_INIT_PAD";

    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT (ordered, contiguous, fill, align = 16)
    {
      section "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss.OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT";
      }
    }
    group OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_PAD";

    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_ALL_START" = "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_FAST_INIT_START";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_ALL_END" = "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_END";
    "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_ALL_LIMIT" = "_OS_OsApplication_Core0_BSW_VAR_SAVED_ZONE_NOINIT_LIMIT";
  }

  group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_ROM";

    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_ROM";

  }

  group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_NOINIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_INIT_PAD";

    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT" (size = 0);
    }
    group OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_START" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_END" = ("_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_PAD" - 1;
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_PAD";

    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_ALL_START" = "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_FAST_INIT_START";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_ALL_END" = "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_END";
    "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_ALL_LIMIT" = "_OS_OsApplication_Core0_QM_VAR_SAVED_ZONE_NOINIT_LIMIT";
  }

  group OS_OsApplication_VAR_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_VAR_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_FAST_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_VAR_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_VAR_FAST_INIT_ROM";
    "_OS_OsApplication_VAR_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_VAR_FAST_INIT_ROM";

    group OS_OsApplication_VAR_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_VAR_INIT_ROM_START" = "_lc_gb_OS_OsApplication_VAR_INIT_ROM";
    "_OS_OsApplication_VAR_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_VAR_INIT_ROM";

  }

  group OS_OsApplication_VAR_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_VAR_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_FAST_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_FAST_INIT_START" = "_lc_gb_OS_OsApplication_VAR_FAST_INIT";
    "_OS_OsApplication_VAR_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_FAST_INIT_PAD";

    group OS_OsApplication_VAR_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_FAST_NOINIT" (size = 0);
    }
    group OS_OsApplication_VAR_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_VAR_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_VAR_FAST_NOINIT";
    "_OS_OsApplication_VAR_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_VAR_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_VAR_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_FAST_NOINIT_PAD";

    group OS_OsApplication_VAR_FAST_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_FAST_ZERO_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_FAST_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_FAST_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT";
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_PAD";

    group OS_OsApplication_VAR_FAST_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_FAST_ZERO_INIT_bss" (size = 0);
    }
    group OS_OsApplication_VAR_FAST_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_FAST_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_bss";
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_VAR_FAST_ZERO_INIT_bss_PAD";

    group OS_OsApplication_VAR_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_INIT_START" = "_lc_gb_OS_OsApplication_VAR_INIT";
    "_OS_OsApplication_VAR_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_INIT_PAD";

    group OS_OsApplication_VAR_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_NOINIT" (size = 0);
    }
    group OS_OsApplication_VAR_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_VAR_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_NOINIT_START" = "_lc_gb_OS_OsApplication_VAR_NOINIT";
    "_OS_OsApplication_VAR_NOINIT_END" = ("_lc_gb_OS_OsApplication_VAR_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_NOINIT_PAD" - 1;
    "_OS_OsApplication_VAR_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_NOINIT_PAD";

    group OS_OsApplication_VAR_ZERO_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_ZERO_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_ZERO_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_ZERO_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_ZERO_INIT_START" = "_lc_gb_OS_OsApplication_VAR_ZERO_INIT";
    "_OS_OsApplication_VAR_ZERO_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_ZERO_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_ZERO_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_ZERO_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_ZERO_INIT_PAD";

    group OS_OsApplication_VAR_ZERO_INIT_bss (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_ZERO_INIT_bss" (size = 0);
    }
    group OS_OsApplication_VAR_ZERO_INIT_bss_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_ZERO_INIT_bss_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_ZERO_INIT_bss_START" = "_lc_gb_OS_OsApplication_VAR_ZERO_INIT_bss";
    "_OS_OsApplication_VAR_ZERO_INIT_bss_END" = ("_lc_gb_OS_OsApplication_VAR_ZERO_INIT_bss_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_ZERO_INIT_bss_PAD" - 1;
    "_OS_OsApplication_VAR_ZERO_INIT_bss_LIMIT" = "_lc_gb_OS_OsApplication_VAR_ZERO_INIT_bss_PAD";

    "_OS_OsApplication_VAR_ALL_START" = "_OS_OsApplication_VAR_FAST_INIT_START";
    "_OS_OsApplication_VAR_ALL_END" = "_OS_OsApplication_VAR_ZERO_INIT_bss_END";
    "_OS_OsApplication_VAR_ALL_LIMIT" = "_OS_OsApplication_VAR_ZERO_INIT_bss_LIMIT";
  }

  group OS_OsApplication_VAR_SAVED_ZONE_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM";
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_ROM";

    group OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM" (size = 0);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM";
    "_OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM_LIMIT" = "_lc_ge_OS_OsApplication_VAR_SAVED_ZONE_INIT_ROM";

  }

  group OS_OsApplication_VAR_SAVED_ZONE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT";
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_PAD";

    group OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT" (size = 0);
    }
    group OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT";
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_END" = ("_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_PAD" - 1;
    "_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_FAST_NOINIT_PAD";

    group OS_OsApplication_VAR_SAVED_ZONE_INIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_INIT" (size = 0);
    }
    group OS_OsApplication_VAR_SAVED_ZONE_INIT_PAD (align = 4)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_INIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_INIT_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_INIT";
    "_OS_OsApplication_VAR_SAVED_ZONE_INIT_END" = ("_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_INIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_INIT_PAD" - 1;
    "_OS_OsApplication_VAR_SAVED_ZONE_INIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_INIT_PAD";

    group OS_OsApplication_VAR_SAVED_ZONE_NOINIT (ordered, contiguous, fill, align = 16)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_NOINIT" (size = 0);
    }
    group OS_OsApplication_VAR_SAVED_ZONE_NOINIT_PAD (align = 1)
    {
      reserved "OS_OsApplication_VAR_SAVED_ZONE_NOINIT_PAD" (size = 16);
    }
    "_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_START" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_NOINIT";
    "_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_END" = ("_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_PAD" == 0) ? 0 : "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_PAD" - 1;
    "_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_LIMIT" = "_lc_gb_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_PAD";

    "_OS_OsApplication_VAR_SAVED_ZONE_ALL_START" = "_OS_OsApplication_VAR_SAVED_ZONE_FAST_INIT_START";
    "_OS_OsApplication_VAR_SAVED_ZONE_ALL_END" = "_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_END";
    "_OS_OsApplication_VAR_SAVED_ZONE_ALL_LIMIT" = "_OS_OsApplication_VAR_SAVED_ZONE_NOINIT_LIMIT";
  }

  group OS_USER_CODE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_USER_CODE (ordered, contiguous, fill)
    {
      section "OS_USER_CODE_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.OS_C0_INIT_BSW_TASK_CODE";
        select "[.]text.OS_ERRORHOOK_CODE";
        select "[.]text.OS_OsTask_Core0_BSW_CODE";
      }
    }
    group OS_USER_CODE_PAD (align = 1)
    {
      reserved "OS_USER_CODE_PAD" (size = 16);
    }
    "_OS_USER_CODE_START" = "_lc_gb_OS_USER_CODE";
    "_OS_USER_CODE_END" = ("_lc_gb_OS_USER_CODE_PAD" == 0) ? 0 : "_lc_gb_OS_USER_CODE_PAD" - 1;
    "_OS_USER_CODE_LIMIT" = "_lc_gb_OS_USER_CODE_PAD";

    "_OS_USER_CODE_ALL_START" = "_OS_USER_CODE_START";
    "_OS_USER_CODE_ALL_END" = "_OS_USER_CODE_END";
    "_OS_USER_CODE_ALL_LIMIT" = "_OS_USER_CODE_LIMIT";
  }

  group OS_USER_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_USER_CONST (ordered, contiguous, fill)
    {
      section "OS_USER_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_OsApplication_Core0_BSW_CONST";
        select "[.]zrodata.OS_OsApplication_Core0_BSW_CONST_FAST";
      }
    }
    group OS_USER_CONST_PAD (align = 1)
    {
      reserved "OS_USER_CONST_PAD" (size = 16);
    }
    "_OS_USER_CONST_START" = "_lc_gb_OS_USER_CONST";
    "_OS_USER_CONST_END" = ("_lc_gb_OS_USER_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_USER_CONST_PAD" - 1;
    "_OS_USER_CONST_LIMIT" = "_lc_gb_OS_USER_CONST_PAD";

    "_OS_USER_CONST_ALL_START" = "_OS_USER_CONST_START";
    "_OS_USER_CONST_ALL_END" = "_OS_USER_CONST_END";
    "_OS_USER_CONST_ALL_LIMIT" = "_OS_USER_CONST_LIMIT";
  }

  group VarGroup_CSA0_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_CSA0 (ordered, contiguous, fill, align = 64)
    {
      reserved "VarSectionGroup_0_CSA0" (size = 8192);
    }
    group VarSectionGroup_0_CSA0_PAD (align = 4)
    {
      reserved "VarSectionGroup_0_CSA0_PAD" (size = 0);
    }
    "_VarSectionGroup_0_CSA0_START" = "_lc_gb_VarSectionGroup_0_CSA0";
    "_VarSectionGroup_0_CSA0_END" = ("_lc_ge_VarSectionGroup_0_CSA0" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_CSA0" - 1;
    "_VarSectionGroup_0_CSA0_LIMIT" = "_lc_ge_VarSectionGroup_0_CSA0";

    "_VarGroup_CSA0_ALL_START" = "_VarSectionGroup_0_CSA0_START";
    "_VarGroup_CSA0_ALL_END" = "_VarSectionGroup_0_CSA0_END";
    "_VarGroup_CSA0_ALL_LIMIT" = "_VarSectionGroup_0_CSA0_LIMIT";
  }

  group OS_CODE_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_CODE (ordered, contiguous, fill)
    {
      section "OS_CODE_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text.OS_CODE";
        select "[.]text.OS_OS_COREINITHOOK_CODE";
      }
    }
    group OS_CODE_PAD (align = 1)
    {
      reserved "OS_CODE_PAD" (size = 16);
    }
    "_OS_CODE_START" = "_lc_gb_OS_CODE";
    "_OS_CODE_END" = ("_lc_gb_OS_CODE_PAD" == 0) ? 0 : "_lc_gb_OS_CODE_PAD" - 1;
    "_OS_CODE_LIMIT" = "_lc_gb_OS_CODE_PAD";

    "_OS_CODE_ALL_START" = "_OS_CODE_START";
    "_OS_CODE_ALL_END" = "_OS_CODE_END";
    "_OS_CODE_ALL_LIMIT" = "_OS_CODE_LIMIT";
  }

  group OS_CONST_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group OS_CONST (ordered, contiguous, fill)
    {
      section "OS_CONST_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata.OS_CONST";
        select "[.]zrodata.OS_CONST_FAST";
        select "[.]rodata.OS_CORE0_CONST";
        select "[.]zrodata.OS_CORE0_CONST_FAST";
        select "[.]rodata.OS_SystemApplication_OsCore0_CONST";
        select "[.]zrodata.OS_SystemApplication_OsCore0_CONST_FAST";
      }
    }
    group OS_CONST_PAD (align = 1)
    {
      reserved "OS_CONST_PAD" (size = 16);
    }
    "_OS_CONST_START" = "_lc_gb_OS_CONST";
    "_OS_CONST_END" = ("_lc_gb_OS_CONST_PAD" == 0) ? 0 : "_lc_gb_OS_CONST_PAD" - 1;
    "_OS_CONST_LIMIT" = "_lc_gb_OS_CONST_PAD";

    "_OS_CONST_ALL_START" = "_OS_CONST_START";
    "_OS_CONST_ALL_END" = "_OS_CONST_END";
    "_OS_CONST_ALL_LIMIT" = "_OS_CONST_LIMIT";
  }

  group ConstGroup_Default_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group ConstSectionGroup_0_text (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_0_text_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]text*";
      }
    }
    "_ConstSectionGroup_0_text_START" = "_lc_gb_ConstSectionGroup_0_text";
    "_ConstSectionGroup_0_text_END" = ("_lc_ge_ConstSectionGroup_0_text" == 0) ? 0 : "_lc_ge_ConstSectionGroup_0_text" - 1;
    "_ConstSectionGroup_0_text_LIMIT" = "_lc_ge_ConstSectionGroup_0_text";

    group ConstSectionGroup_1_rodata (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_1_rodata_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]rodata*";
      }
    }
    "_ConstSectionGroup_1_rodata_START" = "_lc_gb_ConstSectionGroup_1_rodata";
    "_ConstSectionGroup_1_rodata_END" = ("_lc_ge_ConstSectionGroup_1_rodata" == 0) ? 0 : "_lc_ge_ConstSectionGroup_1_rodata" - 1;
    "_ConstSectionGroup_1_rodata_LIMIT" = "_lc_ge_ConstSectionGroup_1_rodata";

    group ConstSectionGroup_2_zrodata (ordered, contiguous, fill, align = 4)
    {
      section "ConstSectionGroup_2_zrodata_SEC" (fill, blocksize = 2, attributes = rx)
      {
        select "[.]zrodata*";
      }
    }
    "_ConstSectionGroup_2_zrodata_START" = "_lc_gb_ConstSectionGroup_2_zrodata";
    "_ConstSectionGroup_2_zrodata_END" = ("_lc_ge_ConstSectionGroup_2_zrodata" == 0) ? 0 : "_lc_ge_ConstSectionGroup_2_zrodata" - 1;
    "_ConstSectionGroup_2_zrodata_LIMIT" = "_lc_ge_ConstSectionGroup_2_zrodata";

    "_ConstGroup_Default_ALL_START" = "_ConstSectionGroup_0_text_START";
    "_ConstGroup_Default_ALL_END" = "_ConstSectionGroup_2_zrodata_END";
    "_ConstGroup_Default_ALL_LIMIT" = "_ConstSectionGroup_2_zrodata_LIMIT";
  }

  group VarGroup_Default_ROM_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_PFlash0_Application)
  {
    group VarSectionGroup_0_data_ROM (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_data_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]data*";
        }
      }
    }
    "_VarSectionGroup_0_data_ROM_START" = "_lc_gb_VarSectionGroup_0_data_ROM";
    "_VarSectionGroup_0_data_ROM_LIMIT" = "_lc_ge_VarSectionGroup_0_data_ROM";

    group VarSectionGroup_1_zdata_ROM (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_1_zdata_ROM_SEC" (fill, blocksize = 2, attributes = rx)
      {
        group (load_addr)
        {
          select "[.]zdata*";
        }
      }
    }
    "_VarSectionGroup_1_zdata_ROM_START" = "_lc_gb_VarSectionGroup_1_zdata_ROM";
    "_VarSectionGroup_1_zdata_ROM_LIMIT" = "_lc_ge_VarSectionGroup_1_zdata_ROM";

  }

  group VarGroup_Default_GROUP (ordered, contiguous, fill, run_addr = mem:mpe:RegionBlock_DSPR0_0)
  {
    group VarSectionGroup_0_data (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_0_data_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]data*";
      }
    }
    group VarSectionGroup_0_data_PAD (align = 16)
    {
      reserved "VarSectionGroup_0_data_PAD" (size = 0);
    }
    "_VarSectionGroup_0_data_START" = "_lc_gb_VarSectionGroup_0_data";
    "_VarSectionGroup_0_data_END" = ("_lc_ge_VarSectionGroup_0_data" == 0) ? 0 : "_lc_ge_VarSectionGroup_0_data" - 1;
    "_VarSectionGroup_0_data_LIMIT" = "_lc_ge_VarSectionGroup_0_data";

    group VarSectionGroup_1_zdata (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_1_zdata_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zdata*";
      }
    }
    group VarSectionGroup_1_zdata_PAD (align = 16)
    {
      reserved "VarSectionGroup_1_zdata_PAD" (size = 0);
    }
    "_VarSectionGroup_1_zdata_START" = "_lc_gb_VarSectionGroup_1_zdata";
    "_VarSectionGroup_1_zdata_END" = ("_lc_ge_VarSectionGroup_1_zdata" == 0) ? 0 : "_lc_ge_VarSectionGroup_1_zdata" - 1;
    "_VarSectionGroup_1_zdata_LIMIT" = "_lc_ge_VarSectionGroup_1_zdata";

    group VarSectionGroup_2_bss (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_2_bss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]bss*";
      }
    }
    group VarSectionGroup_2_bss_PAD (align = 16)
    {
      reserved "VarSectionGroup_2_bss_PAD" (size = 0);
    }
    "_VarSectionGroup_2_bss_START" = "_lc_gb_VarSectionGroup_2_bss";
    "_VarSectionGroup_2_bss_END" = ("_lc_ge_VarSectionGroup_2_bss" == 0) ? 0 : "_lc_ge_VarSectionGroup_2_bss" - 1;
    "_VarSectionGroup_2_bss_LIMIT" = "_lc_ge_VarSectionGroup_2_bss";

    group VarSectionGroup_3_zbss (ordered, contiguous, fill, align = 16)
    {
      section "VarSectionGroup_3_zbss_SEC" (blocksize = 2, attributes = rw)
      {
        select "[.]zbss*";
      }
    }
    group VarSectionGroup_3_zbss_PAD (align = 16)
    {
      reserved "VarSectionGroup_3_zbss_PAD" (size = 0);
    }
    "_VarSectionGroup_3_zbss_START" = "_lc_gb_VarSectionGroup_3_zbss";
    "_VarSectionGroup_3_zbss_END" = ("_lc_ge_VarSectionGroup_3_zbss" == 0) ? 0 : "_lc_ge_VarSectionGroup_3_zbss" - 1;
    "_VarSectionGroup_3_zbss_LIMIT" = "_lc_ge_VarSectionGroup_3_zbss";

    "_VarGroup_Default_ALL_START" = "_VarSectionGroup_0_data_START";
    "_VarGroup_Default_ALL_END" = "_VarSectionGroup_3_zbss_END";
    "_VarGroup_Default_ALL_LIMIT" = "_VarSectionGroup_3_zbss_LIMIT";
  }

}
/* These labels must be maintained up-to-date depending on the Linker generation */
#include "UserLabels.lsl"

