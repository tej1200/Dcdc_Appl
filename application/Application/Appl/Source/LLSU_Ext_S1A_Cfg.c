/*******************************************************************************/
/* CONFIDENTIAL HELLA  KGaA HUECK & CO. (HKG)                                  */
/*                                                                             */
/* This is an unpublished work of authorship, which contains trade secrets,    */
/* created in 2009. Hella KGaA Hueck & Co. owns all rights to this work and    */
/* intends to maintain it in confidence to preserve its trade secret status.   */
/* Hella KGaA Hueck & Co. reserves the right, under the copyright laws of      */
/* Germany or those of any other country that may have jurisdiction, to        */
/* protect this work as an unpublished work, in the event of an inadvertent or */
/* deliberate unauthorized publication. Hella KGaA Hueck & Co. also reserves   */
/* its rights under all copyright laws to protect this work as a published     */
/* work, when appropriate. Those having access to this work may not copy it,   */
/* use it, modify it or disclose the information contained in it without the   */
/* written authorization of Hella KGaA Hueck & Co..                            */
/*                                                                             */
/*******************************************************************************/

/***********************************************************************************************/
/*  Filename    :   $Source: LLSU_Ext_S1A_Cfg.c $                                               *//* PRQA S 0292 # Hella naming convetion for file names */
/*  Author      :   $Author: Kumar Anil kumaan8 (kumaan8) $                              */
/*  Date        :   $Date: 2021/02/15 17:40:51IST $                                                                  */
/*  Version     :   $Revision: 1.2 $                                                               */
/*  Status      :   $State: IN_WORK $                                                          */
/*  Description :   This file contains definitions for RAM-initialization       	           */
/***********************************************************************************************/
/*  Changes     :                                                                              */
/*  $Log        :   LLSU_Ext_S1A_Cfg.c  $                                                      */
/***********************************************************************************************/
/*                                                                                             */
/*  Module                                                                                     */
/*  Release                                                                                    */
/*  Version     Date          Author    ChangeID Summary                                       */
/*                                                                                             */
/*  1.0      04.02.2021      schupe6    1205742    LLSU integration code for LE-Porsche        */
/*                                                                                             */
/***********************************************************************************************/

#include "Std_Types.h"
#include "LLSU_Ext_S1A_Cfg.h"

const copytable_entry_t LLSU_Ext_S1A_RamInitTable_Core0[LLSU_EXT_S1A_RAM_INIT_BLOCK_COUNT_CORE0] = /* PRQA S 2053, 0315, 3004, 0306, 1257 ++ # Intended implementation for memory init table */
{  
 
        { /* DLMU0_VAR_BSS */
                /* .action_mwu = */ ACTION_MWU_CLEAR,
                /* .*dst       = */ _DLMU0_VAR_BSS_START,
                /* .*src       = */ 0x0uL,
                /* .size       = */ (uint32) _DLMU0_VAR_BSS_SIZE
        },
                { /* DLMU0_VAR_DATA */
                /* .action_mwu = */ ACTION_MWU_CLEAR,
                /* .*dst       = */ _DLMU0_VAR_DATA_START,
                /* .*src       = */ 0x0uL,
                /* .size       = */ (uint32) _DLMU0_VAR_DATA_SIZE
        },
        { /* OS_STACKS_CORE0_VAR_NOINIT */
                /* .action_mwu = */ ACTION_MWU_CLEAR,
                /* .*dst       = */ _OS_STACKS_CORE0_VAR_NOINIT_START,
                /* .*src       = */ 0x0uL,
                /* .size       = */ (uint32) _OS_STACKS_CORE0_VAR_NOINIT_SIZE
        },
    { /* DLMU0_VAR */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _DLMU0_VAR_INIT_START,
                /* .*src       = */ _DLMU0_VAR_ROM_START,
                /* .size       = */ (uint32) _DLMU0_VAR_INIT_SIZE
        },
               
        { /* DSPR0_VAR */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _DSPR0_VAR_START,
                /* .*src       = */ _DSPR0_VAR_ROM_START,
                /* .size       = */ (uint32) _DSPR0_VAR_SIZE
        },
        { /* data */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _data_START,
                /* .*src       = */ _data_ROM_START,
                /* .size       = */ (uint32) _data_SIZE
        },
        { /* zdata */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _zdata_START,
                /* .*src       = */ _zdata_ROM_START,
                /* .size       = */ (uint32) _zdata_SIZE
        },
                { /* OS_GLOBALSHARED_VAR_INIT */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _OS_GLOBALSHARED_VAR_INIT_START,
                /* .*src       = */ _OS_GLOBALSHARED_VAR_INIT_ROM_START,
                /* .size       = */ (uint32) _OS_GLOBALSHARED_VAR_INIT_SIZE
        },
 
        { /* OS_DATA_CORE0_VAR_INIT */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _OS_DATA_CORE0_VAR_INIT_START,
                /* .*src       = */ _OS_DATA_CORE0_VAR_INIT_ROM_START,
                /* .size       = */ (uint32) _OS_DATA_CORE0_VAR_INIT_SIZE
        },
 
 
        { /* OS_DATA_SHARED_VAR_INIT */
                /* .action_mwu = */ ACTION_MWU_COPY,
                /* .*dst       = */ _OS_DATA_SHARED_VAR_INIT_START,
                /* .*src       = */ _OS_DATA_SHARED_VAR_INIT_ROM_START,
                /* .size       = */ (uint32) _OS_DATA_SHARED_VAR_INIT_SIZE
        },
 
}; /* PRQA S 2053, 0315, 3004, 0306, 1257 -- */



