/**************************************************************************
**                                                                        *
**  FILE        :  Application_Integration_Callout.c                                                *
**                                                                        *
**  DESCRIPTION :                                                         *
**      modify this file untill there was a change in the start          *
**      up of the application.                                            *
**                                                                        *
**************************************************************************/
/***************************   Header Inclusions  ******************************/

#include "EcuM.h"
#include "Rte_Type.h"
#include "Rte_Main.h"
#include "Application_Integration_Callout.h"
#include "Os_Lcfg.h"
#include "Os.h"
/***************************   Header Inclusions   ******************************/
#if 0
/***************************   Function Banner   ******************************
* Function Name     :	TASK(C0_INIT_BSW_TASK)
* Description       :	This task C0_INIT_BSW_TASK will initialize
* 						the Master core.
* Parameters        :	None
* Inputs			      :	None
* Outputs 			    :	None
* In/Out 			      :	None
* Return values     :	None
* Exceptions	      :	None
*******************************************************************************/
TASK(C0_INIT_BSW_TASK)
{
		/*call EcuM startup two */
		EcuM_StartupTwo();
		/* Start RTE */
		(void)Rte_Start();
  	/*terminate the task*/
    (void)TerminateTask();
}
#endif 
void Mcal_SetModetoUser()/*TODO : sharan5*/
{

}
