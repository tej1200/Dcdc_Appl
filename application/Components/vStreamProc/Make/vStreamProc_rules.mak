############################################################################### 
# File Name  : vStreamProc_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2022 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2019-08-02  visphh  Initial Version
# 1.01.00   2019-10-25  visfsn  Added cipher and copy node, improved usability
# 1.02.00   2019-12-20  visrr   Added trigger node
# 2.00.00   2021-03-12  visjhg  Renamed vStreamProc_ProcessingNodesHeaderTypes.c
#                               to vStreamProc_Port.c.
# 3.00.00   2021-08-16  visshk  Added vStreamProc_Queue.c.
#           2021-08-27  visfsn  Removed Copy node
# 3.01.00   2021-12-17  visfsn  Added vStreamProc_Snapshot.c
# 3.02.00   2021-02-11  visfsn  Removed Trigger node
#                               Added vStreamProc_Session.c
# 3.03.00   2022-03-11  visrr   Added vStreamProc_Limit.c
#           2022-06-03  visfsn  Removed vStreamProc_Port.c
#                               Added vStreamProc_Stream.c
#                               Added vStreamProc_ProcessingNode.c
#           2022-06-03  virljs  Added vStreamProc_CipherAead.c
#           2022-06-03  visfsn  Added vStreamProc_Mode.c
#           2022-06-03  visfsn  Added vStreamProc_ProcessingNode_AuthGeneration.c      
#           2022-06-03  virljs  Added vStreamProc_ProcessingNode_Merge.c     
#           									  Added vStreamProc_ProcessingNode_RangeSerialization.c     
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_AccessNode.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Limit.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Mode.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Pipe.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_AuthGeneration.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_ByteCompare.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Cipher.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_CipherAead.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Crc.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_CsmVerification.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Hash.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Merge.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_RangeSerialization.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Queue.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Scheduler.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Session.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_SimpleBufferNode.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Snapshot.c
CC_FILES_TO_BUILD       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Stream.c
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\vStreamProc_Lcfg.c

# Library Settings
LIBRARIES_TO_BUILD      += vStreamProc
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_AccessNode.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Limit.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Mode.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Pipe.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_ByteCompare.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Cipher.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_CipherAead.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Crc.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_CsmVerification.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Hash.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_Merge.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_ProcessingNode_RangeSerialization.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Queue.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Scheduler.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Session.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_SimpleBufferNode.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Snapshot.c
vStreamProc_FILES       += vStreamProc$(BSW_SRC_DIR)\vStreamProc_Stream.c
