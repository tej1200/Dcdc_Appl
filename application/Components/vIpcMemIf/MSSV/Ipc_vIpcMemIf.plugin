/**********************************************************************************************************************
*  COPYRIGHT
*  -------------------------------------------------------------------------------------------------------------------
*  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
*
*                This software is copyright protected and proprietary to Vector Informatik GmbH.
*                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
*                All other rights remain with Vector Informatik GmbH.
*  -------------------------------------------------------------------------------------------------------------------
*  FILE DESCRIPTION
*  -------------------------------------------------------------------------------------------------------------------
*  File       :  Ipc_vIpcMemIf.plugin
*  Module     :  MSSV
*  Description:  Entry point of MSSV Core.
*  -------------------------------------------------------------------------------------------------------------------
*  REVISION HISTORY
*  -------------------------------------------------------------------------------------------------------------------
*  Version   Date        Author        Change Id     Description
*  -------------------------------------------------------------------------------------------------------------------
*  01.00.00  2021-09-08  virsmn                      First version
**********************************************************************************************************************/

/**********************************************************************************************************************
* Mandatory Functions
**********************************************************************************************************************/

/**********************************************************************************************************************
* Name         : RegisterPlugin
* Return value : Reference to a structure which contains the registration information about the plugin
* Description  : MSSV_core calls this function to query necessary information about the plugin.
*                This function is mandatory.
**********************************************************************************************************************/
def RegisterPlugin()
{
  var reg = ModulePluginRegistration()
  reg.SetVersion(0x010000)
  reg.SetPackageName("Ipc_vIpcMemIf")
  reg.SetInputFiles(["vIpcMemIf.c", "vIpcMemIf_Lcfg.c", "vIpcMemIf_PBcfg.c"])
  return reg
}

/**********************************************************************************************************************
* Name         : CheckVersions
* Return value : -
* Description  : MSSV_core calls this function to allow the plugin a version check against the BSW sources.
*                Check against the major and minor version of the source code.
*                Note: The version check is not necessary if only rule CheckQMDefines is implemented in this plugin.
**********************************************************************************************************************/
def CheckVersions()
{
  AssertDefineEquals("VIPCMEMIF_SW_MAJOR_VERSION", "(1u)")
  AssertDefineEquals("VIPCMEMIF_SW_MINOR_VERSION", "(1u)")
}

/**********************************************************************************************************************
* Name         : main
* Parameter    : None
* Return value : None
* Description  : This is the entry point of the MSSV plugin. Main calls all rule functions to check the configuration.
*                This function is mandatory.
* Requirements : N/A
**********************************************************************************************************************/
def main()
{
  InvokeRule("Check Dev Error Detection", CheckDevErrorDetect)
}

/**********************************************************************************************************************
* Rules
**********************************************************************************************************************/
/**********************************************************************************************************************
* Name         : CheckDevErrorDetect
* Parameter    : None
* Return value : None
* Description  : This rule checks the setting of VIPCMEMIF_DEV_ERROR_DETECT. In saftey requierements,
*                development error detection needs to be turned on.
* Requirements : N/A
**********************************************************************************************************************/
def CheckDevErrorDetect()
{
  AssertDefineIsStdOn("VIPCMEMIF_DEV_ERROR_DETECT")
}
