###############################################################################
# File Name  : vIpcMemIf_rules.mak
# Description: Rules makefile
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2020 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2007-06-13  visaba  Initial Version of Template (1.0)
# 1.01.00   2017-05-30  vismas  Clean-up
# 1.02.00   2020-09-11  virbse  Added new source files
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD       += vIpcMemIf$(BSW_SRC_DIR)\vIpcMemIf.c    \
                           vIpcMemIf$(BSW_SRC_DIR)\vIpcMemIf_Tx.c \
                           vIpcMemIf$(BSW_SRC_DIR)\vIpcMemIf_Rx.c
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\vIpcMemIf_Lcfg.c        \
                           $(GENDATA_DIR)\vIpcMemIf_PBcfg.c

# Library Settings
LIBRARIES_TO_BUILD      +=
vIpcMemIf_FILES         +=

