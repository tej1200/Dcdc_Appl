###############################################################################
# File Name  : vIpcMemCp_rules.mak
# Description: Rules makefile
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2020 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2017-06-07  Bse     Initial Version
# 1.01.00   2019-02-06  vircbl  Added support of component-based SIP structure
# 1.02.00   2020-27-07  visdqk  Added new source files
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD       += vIpcMemCp$(BSW_SRC_DIR)\vIpcMemCp.c    \
                           vIpcMemCp$(BSW_SRC_DIR)\vIpcMemCp_Tx.c \
                           vIpcMemCp$(BSW_SRC_DIR)\vIpcMemCp_Rx.c

GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\vIpcMemCp_Cfg.c         \
                           $(GENDATA_DIR)\vIpcMemCp_PBcfg.c

# Library Settings
LIBRARIES_TO_BUILD      +=
VIPCMEMCP_FILES          =
