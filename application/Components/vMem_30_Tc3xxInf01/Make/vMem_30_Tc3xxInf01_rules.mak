############################################################################### 
# File Name  : vMem_30_Tc3xxInf01_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2021 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author         Description
#------------------------------------------------------------------------------
# 1.00.00   2018-04-24  Chl, Oto       Initial creation
# 1.01.00   2018-05-23  Cre            STORYC-5221 resulted in a new source file for
#                                      LowLevel specific functionality
# 1.02.00   2019-02-06  vircbl         Added support of component-based SIP structure
# 1.03.00   2021-10-06  coberpriller   Uprade to new core version 3.00.00
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD        += vMem_30_Tc3xxInf01$(BSW_SRC_DIR)\vMem_30_Tc3xxInf01.c \
 			    vMem_30_Tc3xxInf01$(BSW_SRC_DIR)\vMem_30_Tc3xxInf01_DetChecks.c \
                            vMem_30_Tc3xxInf01$(BSW_SRC_DIR)\vMem_30_Tc3xxInf01_LL.c
GENERATED_SOURCE_FILES   += $(GENDATA_DIR)\vMem_30_Tc3xxInf01_Lcfg.c

