############################################################################### 
# File Name  : vMem_30_Tc3xxInf01_check.mak 
# Description: Configuration check makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2021 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Refer to the vMem_30_Tc3xxInf01_rules.mak file.
############################################################################### 
