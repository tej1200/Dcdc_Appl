/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  
 *        \brief  Float Header for RTE Analyzer
 *
 *      \details  This header provides the float.h defines
 *                that are required for the static analysis with RTE Analyzer.
 *
 *********************************************************************************************************************/
 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2015-07-31  visso                 Initial creation
 *  01.01.00  2021-02-18  visso   RTE-4007      Rework TechRef according to templates and remove full name from sources
 *********************************************************************************************************************/
#ifndef _FLOAT_H
# define _FLOAT_H
# define FLT_MIN                 1.17549435E-38F
# define FLT_MAX                 3.40282347E38F
# define DBL_MIN                 2.2250738585072014E-308
# define DBL_MAX                 1.7976931348623157E308

#endif
