############################################################################### 
# File Name  : vMem_30_vRpcProxy.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2021 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD       += vMem_30_vRpcProxy$(BSW_SRC_DIR)\vMem_30_vRpcProxy.c
CC_FILES_TO_BUILD       += vMem_30_vRpcProxy$(BSW_SRC_DIR)\vMem_30_vRpcProxy_DetChecks.c
CC_FILES_TO_BUILD       += vMem_30_vRpcProxy$(BSW_SRC_DIR)\vMem_30_vRpcProxy_LL.c

GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\vMem_30_vRpcProxy_Lcfg.c
