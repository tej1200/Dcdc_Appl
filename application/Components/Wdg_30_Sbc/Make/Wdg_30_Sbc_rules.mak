############################################################################### 
# File Name  : Wdg_30_Sbc_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2015-07-10  mid     Added Wdg_30_Sbc specific content
# 2.00.00   2016-02-05  mid     Adapted to implementation version 2.x
# 2.00.01   2017-10-12  mid     Removed SSC_ROOT
# 2.01.00   2019-02-06  vircbl  Added support of component-based SIP structure
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD       += Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc.c Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_LL.c \
                           Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_Mode.c Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_Timer.c \
                           Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_TrgCnd.c
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\Wdg_30_Sbc_Lcfg.c

# Library Settings
LIBRARIES_TO_BUILD      += Wdg_30_Sbc
Wdg_30_Sbc_FILES        += Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc.c Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_LL.c \
                           Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_Mode.c Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_Timer.c \
                           Wdg_30_Sbc$(BSW_SRC_DIR)\Wdg_30_Sbc_TrgCnd.c

