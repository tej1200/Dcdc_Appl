############################################################################### 
# File Name  : vStreamProcCmprLz77_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2021 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2021-05-21  visfsn  Initial Version
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################

# Component Files
CC_FILES_TO_BUILD                  += vStreamProcCmprLz77$(BSW_SRC_DIR)\vStreamProcCmprLz77.c
GENERATED_SOURCE_FILES             += 

# Library Settings
LIBRARIES_TO_BUILD                 += vStreamProcCmprLz77
vStreamProcCmprLz77_FILES  += vStreamProcCmprLz77$(BSW_SRC_DIR)\vStreamProcCmprLz77.c

