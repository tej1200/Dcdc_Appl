/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2021 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  wdgm_verifier_version.h
 *        \brief  wdgm_verifier_version header file
 *
 *      \details  This is the header file of WdgMVerifier version
 *
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the Verifier's header file.
 *********************************************************************************************************************/
 
#ifndef WDGM_VERIFIER_VERSION_H
#define WDGM_VERIFIER_VERSION_H

#define WDGM_VERIFIER_MAJOR_VERSION 3
#define WDGM_VERIFIER_MINOR_VERSION 0
#define WDGM_VERIFIER_PATCH_VERSION 2

#endif /* WDGM_VERIFIER_VERSION_H */

