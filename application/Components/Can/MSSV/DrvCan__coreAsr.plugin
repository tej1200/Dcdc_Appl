/**********************************************************************************************************************
*  COPYRIGHT
*  -------------------------------------------------------------------------------------------------------------------
*  Copyright (c) 2015 by Vector Informatik GmbH.                                                  All rights reserved.
*
*                This software is copyright protected and proprietary to Vector Informatik GmbH.
*                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
*                All other rights remain with Vector Informatik GmbH.
*  -------------------------------------------------------------------------------------------------------------------
*  FILE DESCRIPTION
*  -------------------------------------------------------------------------------------------------------------------
*  File       :  DrvCan__coreAsr.plugin
*  Module     :  MSSV
*
*  Description:  Checking rule for DrvCan__coreAsr
*
*  -------------------------------------------------------------------------------------------------------------------
*  AUTHOR IDENTITY
*  -------------------------------------------------------------------------------------------------------------------
*  Name                          Initials      Company
*  -------------------------------------------------------------------------------------------------------------------
*  Holger Birke                   visbir       Vector Informatik GmbH
*  -------------------------------------------------------------------------------------------------------------------
*  REVISION HISTORY
*  -------------------------------------------------------------------------------------------------------------------
*  Version    Date          Author   Change Id        Description
*  -------------------------------------------------------------------------------------------------------------------
*  01.00.00   2012-08-07    visbir                    First version (based on template 1.00.00)
*  01.01.00   2012-12-11    visbir                    add SBSW_CM_CAN_HL05, 23, 27, 62, 63, 64
*  01.02.00   2014-07-03    visbir                    ESCAN00074843: AR3-2636: new MSSV / version check moved from RegisterPlugin() to CheckVersions()
*  01.03.00   2015-10-23    visbir                    AR4-R13 Silent Check secure deactivated Feature only
*  01.04.00   2015-05-11    visbir                    AR4-R15 remove version check
*  01.04.01   2018-01-23    visbir                    ESCAN00098094: change include file to suppress check when module is not active
*  
**********************************************************************************************************************/

/**********************************************************************************************************************
* Mandatory Functions
**********************************************************************************************************************/

/**********************************************************************************************************************
* Name         : RegisterPlugin
* Return value : Reference to a structure which contains the registration information about the plugin
* Description  : Elisa_core calls this function to query necessary information about the plugin.
*                This function is mandatory.
**********************************************************************************************************************/
def RegisterPlugin()
{
  var reg = ModulePluginRegistration()
  reg.SetVersion(0x010401)                 /* version number of the plugin */
  reg.SetPackageName("DrvCan__coreAsr")    /* package name of the module (as in ALM). */
  reg.SetInputFiles(["Can_Lcfg.c"])        /* list of all required C-files to be checked */
  return reg // Mandatory
}

def CheckVersions()
{
  /* do not check version because the checked switch: CAN_SAFE_BSW is version independent */
  /* AssertDefineEquals("CAN_COREVERSION","0x0502u")   or later    check major/minor and release version of the BSW module */
}

/**********************************************************************************************************************
* Name         : main
* Parameter    : None
* Return value : None
* Description  : This is the entry point of the MSSV plugin. main calls all rule functions to check the configuration.
*                This function is mandatory.
* Requirements : ADAPT: Enter here the references to the corresponding requirements
**********************************************************************************************************************/
def main()
{
  InvokeRule("Check QM inactive", CheckQMDefines)
}

/**********************************************************************************************************************
* Name         : CheckQMDefines
* Parameter    : None
* Return value : None
* Description  : This rule checks the setting of QM-related preprocessor defines.
*                Typically it checks that QM features are inactive in safety context.
* Requirements : N/A
**********************************************************************************************************************/
def CheckQMDefines()
{
  AssertDefineIsStdOn("CAN_SAFE_BSW")
}
